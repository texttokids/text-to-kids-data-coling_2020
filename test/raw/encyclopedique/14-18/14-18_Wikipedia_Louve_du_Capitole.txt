
La sculpture en bronze de 75 cm de hauteur et 114 cm de longueur,
représente un épisode de l'histoire légendaire de Romulus et
Rémus : déposés sur le Tibre dans un panier d'osier, les jumeaux
sont recueillis par une louve au pied du Mont Palatin, sous un
figuier sauvage (le Ficus Ruminalis) situé devant l'entrée de
la grotte du Lupercal. L'animal les aurait nourris et protégés.
Devenus adultes, les deux frères retournent à l'endroit de leur abandon




Denier d'argent de Sextus Pompée (vers 137 avant notre ère) : avers

Sur l'autel de Mars et Vénus d'Ostie (musée du Palazzo
Massimo), la louve a la tête tournée vers les jumeaux.
Sur un buste romain de l'Agora d'Athènes, la louve a aussi la tête


En juillet 2008, les résultats des analyses de datation au
carbone 14, effectuées depuis 2006 par les universitaires de la
région du Salento, ainsi que l'utilisation de la technique de la
prouvent que l'œuvre ne date pas du V^e siècle av. J.-C., comme on
l'a longtemps prétendu depuis les études de Johann Joachim
Winckelmann au XVIII^e siècle, mais remonterait en fait au milieu
du Moyen Âge : elle n'était donc pas étrusque^. On
pourrait donc supposer que la statue signalée devant le palais du
Latran dès le X^e siècle ne serait pas celle-ci.

Cependant, ces résultats n'ont toujours pas fait l'objet, en 2012,
d'une véritable publication scientifique et il n'existe pas encore
un consensus clair de la communauté scientifique sur cette question.


Cette œuvre fut utilisée comme fontaine aux XII^e et
XIII^e siècles. Offerte par le pape Sixte IV à la ville de Rome
en 1471, elle est placée dans l'église Saint-Théodore-au-Palatin,
des jumeaux romains, œuvres attribuées à Antonio Pollaiuolo (1432 -
1498). La statue est placée au Capitole vers 1544. L'ensemble fut
ensuite transféré après 1876 au palais des Conservateurs.

Placée au centre de la salle – où Aldrovani la
mentionne, au XVIe siècle "dans une loggia couverte qui regarde vers la
partie plane de la ville" (les traces des colonnes de la loggia sont
visibles sur le mur entre les deux fenêtres) – la Louve est le symbole


Parvenue au Capitole avec la donation de Sixte IV, elle fut
initialement placée sur la façade du palais du XVe siècle et fut
ensuite transportée à l'intérieur, à la faveur des travaux de



Réplique installée en 1962 dans le square Paul-Painlevé, à Paris.

Une réplique est offerte par la ville de Rome en 1962 à la ville de
Paris, à l'occasion du jumelage des deux capitales. Elle se trouve
dans le square Paul-Painlevé (V^e arrondissement). La ville de
Rome a également offert une réplique de la louve à la ville de
Martigny, ancienne cité romaine située en Valais ; elle se
trouve d'ailleurs sur la bien nommée place de Rome. Une autre fut
donnée en 1921 par la municipalité de Rome à la ville de
Cluj-Napoca (Roumanie), comme un symbole de la romanité des


Les 5 et 6 juin 1982 furent célébrés les 2100 ans d'existence de
Narbonne. A cette occasion le docteur Alberto Benzoni, maire-adjoint de
la ville de Rome, offrit à Narbonne une magnifique réplique de la
Louve ; Narbonne fut la première colonie romaine et devint de ce fait


Une histoire analogue (celle des fils de Rémus) donne lieu à une
légende comparable pour la fondation de la ville de Sienne, avec la
lupa senese qui orne tous les édifices notables de la ville



Les textes anciens nous ont conservé le souvenir de deux groupes
sculptés qui représentaient à Rome la louve et les jumeaux :
Ruminal^, à l'initiative des frères Ogulnii, Cnaeus et

d'Halicarnasse^ et datant vraisemblablement de l'époque de




Un troisième groupe figurant la louve avec les jumeaux se trouvait au
Capitole et fut renversé, avec d'autres statues, lors d'un violent
orage qui s'abattit sur Rome en 65 av. J.-C.^. C'est à ce
groupe (jumeaux non compris) qu'a généralement été identifiée la statue
de la louve aujourd'hui conservée au musée du Capitole, dès 1544 par
Marliani, puis par Winckelmann, Petersen, Carcopino et d'autres.

Sur la plupart des représentations antiques, la louve a la tête tournée



La louve romaine comique et galopante, emblème des éditions

La louve romaine de Chişinău en Moldavie, symbole des luttes


Benito Mussolini, qui se voulait le promoteur d'une « Nouvelle
Rome », a utilisé la représentation de la Louve du Capitole comme outil
de propagande. Il en envoya à plusieurs reprises des répliques aux
Le monument érigé à la mémoire de Nicolas Pietkin à


La Louve du Capitole fut choisie comme emblème des Jeux olympiques
de Rome (1960). Elle figure aussi sur le logo du club de football


La Louve du Capitole, sans les jumeaux (justifiant cela dans la FAQ par
le fait que les jumeaux sont des ajouts tardifs), est aussi présente
sur la couverture des livres de la série latine de la Collection
des universités de France « Budé », à couverture rouge^.

Le facétieux éditeur romain Angelo Fortunato Formiggini a adopté
comme emblème de sa maison d'édition une louve romaine comique et


du Capitole, symbole de la civilisation latine, a été inauguré en 1926,


Enfin la louve romaine de Chişinău, capitale de la république de
Moldavie, est devenue, depuis l'indépendance en 1991 de ce
pays jadis roumain, puis soviétique, un symbole des
luttes politiques entre autochtones moldaves (de langue
romane) et colons russophones : régulièrement ces derniers la
vandalisent ou subtilisent, tandis qu'une „Ligue culturelle pour
l'unité des roumains de tous lieux” organise des souscriptions, fait
faire des copies et la remet en place. La première statue a été fondue
en 1940 au moment de l'annexion soviétique, puis une réplique a été
remise en place lors de l'indépendance devant le Musée


