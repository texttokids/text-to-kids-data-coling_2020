

Né le 20 décembre 1998 dans le 19^e arrondissement de



issu d'une famille de sportifs. Son père, Wilfrid Mbappé Lottin,
d'origine camerounaise, est un ancien footballeur de niveau
régional devenu entraîneur des moins de 15 ans à l'Association
sportive de Bondy^. Sa mère, Fayza Lamari, d'origine
algérienne, a été handballeuse de D1 dans ce même club

Jirès Kembo, est joueur de football professionnel depuis 2006,
après une formation suivie au Stade rennais^, et son petit
frère Ethan de sept ans, compte suivre leurs traces^.

Kylian est scolarisé dans un établissement privé catholique à


Il commence le football à l'AS Bondy, club de 750 licenciés dont, sont
issus des joueurs professionnels dont Sébastien Corchia,


En 2011, il est sélectionné dans la génération 1998 du centre de
l'INF Clairefontaine, avec Arnaud Nordin notamment, dont la
formation est suivie par une journaliste du Monde^. Il
quitte alors Bondy et entre au collège Catherine-de-Vivonne à
Rambouillet^. À l'issue de la formation à Clairefontaine,
en 2013, il est en contact avec plusieurs clubs, en France, dont
les Girondins de Bordeaux, où ses parents rencontrent Yannick
Stopyra, et le Stade Malherbe de Caen, avec lequel est noué un
accord de principe^^,^, et à l'étranger (le Real
Madrid, Chelsea, etc.). Il rejoint finalement le centre de
formation de l'AS Monaco, dont l'équipe première fait son retour


Mbappé y poursuit sa progression : en septembre 2014, il est
sélectionné à deux reprises en équipe de France des moins de 17
ans^. Il commence la saison 2015-2016 en marquant trois
doublés consécutifs avec l'équipe des moins de 19 ans de l'AS
Monaco^. Il fait peu après ses débuts en équipe réserve, en
CFA, avec laquelle il marque ses premiers buts en novembre


En septembre 2016, il obtient le baccalauréat STMG (sciences et
technologies du management et de la gestion)^. À ce sujet, il
déclare : « J’avais besoin de ce diplôme pour mes projets
d’après-carrière, dont celui de devenir entraîneur. »^.



Kylian Mbappé, déborde avant de délivrer une passe décisive à
Thomas Lemar sur un centre au second poteau, le 6 mai 2017 en


Kylian Mbappé fait ses débuts en Ligue 1 le 2 décembre
2015 lors d'un match nul 1-1 à domicile contre le Stade
Malherbe de Caen, alors qu'il n'a pas encore 17 ans (16 ans et 11
mois), il remplace Fábio Coentrão à la 88^e minute et devient le
plus jeune joueur à porter le maillot professionnel de l'AS Monaco
en L1, battant l'ancien record, qui appartenait à Thierry


2016, Kylian Mbappé marque son premier but en tant que
professionnel, lors du temps additionnel face à ES Troyes AC
plus jeune buteur de l'histoire de l'AS Monaco, en battant le


après une négociation médiatisée^, Mbappé signe son premier
contrat professionnel d'une durée de trois ans, le liant à l'ASM


Avec les U19 de Monaco, il remporte la Coupe Gambardella à
l'été 2016, inscrivant un doublé en finale face au RC
Lens^. Quelques semaines plus tard, Kylian Mbappé remporte
avec l'équipe de France des moins de 19 ans le Championnat
d'Europe de football des moins de 19 ans 2016, lors duquel il inscrit

Mbappé à l'échauffement avec l'AS Monaco face à Nancy au


Lors du marché des transferts estival 2016, l'AS Monaco
refuse une offre de 40 millions d'euros du club anglais de

inscrit son premier triplé en Ligue 1 face au FC Metz (victoire
5-0). Dix jours plus tard, Mbappé inscrit son premier but en Ligue
des champions contre Manchester City et devient alors le deuxième
plus jeune buteur français dans cette compétition derrière Karim


Il enchaîne par la suite les records de précocité : le 5 mars
2017, il atteint la barre des dix buts en Ligue 1 après un doublé
face au FC Nantes^. Le 12 avril, il inscrit son premier
doublé en Ligue des champions en quart de finale aller, sur la
pelouse du Borussia Dortmund. Lors du match retour, il marque
son cinquième but dans la compétition et devient à la fois le plus
jeune joueur à atteindre ce total et le premier à marquer un but dans
chacun de ses quatre premiers matchs à élimination directe en
Ligue des champions^. Il est encore le plus jeune buteur
de l'histoire de la compétition en demi-finale, face à la
Juventus, meilleure défense de la compétition en 2016-2017,


Le 17 mai 2017, Mbappé et l'AS Monaco remportent la Ligue 1 devant
le Paris SG, quadruple champion en titre. Mbappé termine au
cinquième rang du classement des buteurs avec quinze buts, malgré un


Au cours de l'intersaison, le jeune Français est annoncé sur les
tablettes de grands clubs de football d'Europe comme le Paris SG,
le Real Madrid et Manchester City. Le club monégasque semble
cependant vouloir le garder et lui réserve le numéro 10, libéré par le




Après plusieurs semaines d'annonces contradictoires dans les médias,
Mbappé rejoint le Paris Saint-Germain Football Club au dernier
jour du marché des transferts, le 31 août 2017, dans le cadre d'un prêt
d'une saison avec option d’achat de 180 millions d'euros. L'option sera
automatiquement levée si le PSG se maintient en Ligue 1^.
pour un salaire de base net d'impôt sur cinq ans de 10 millions d'euros
par saison^ et devient le deuxième joueur le plus cher de
l'histoire du football derrière son nouveau coéquipier Neymar,
recruté quelques semaines plus tôt^. Il garde au Paris
Saint-Germain le numéro 29 qu'il a choisi en clin d’œil à la date
d'anniversaire de son petit frère Ethan^. Il joue son premier
match le 8 septembre contre le FC Metz et inscrit à cette
occasion son tout premier but avec le club parisien (victoire 5 à
1)^. Quatre jours plus tard, il participe à son premier match
sur la scène européenne avec le maillot parisien sur le terrain du
Celtic Glasgow pour une nouvelle victoire (5-0). Il inscrit à
cette occasion le deuxième but du PSG, ce qui constitue son premier but
en Ligue des champions sous ses nouvelles couleurs. À 18 ans et
neuf mois il devient ainsi le plus jeune buteur du PSG en coupe

Marquinhos^. Le 27 septembre, Mbappé réalise une
performance saluée par les observateurs lors du choc face au
Bayern Munich, en Ligue des champions (victoire 3-0). Il
offre deux passes décisives et est élu homme du match^. À la
suite de ce match, plusieurs personnalités footballistiques se disent
impressionnées par son talent, comme Carlo Ancelotti ou son
coéquipier Neymar, qui déclare notamment le voir un jour « briguer le

pourrait être un nouveau Pelé^. Le 23 octobre 2017, il
est élu Golden Boy, trophée récompensant le meilleur footballeur
de moins de 21 ans évoluant en Europe^. Mbappé traverse
ensuite, pendant quelques semaines, une période de méforme qui conduit
les médias à remettre en question sa place de titulaire dans un
effectif aussi riche que celui du Paris SG^. Il retrouve le
chemin des filets, à Angers (doublé pour une victoire 0-5) et
Strasbourg (1 but et défaite 2-1) en Ligue 1, mais également
en Ligue des champions face au Celtic (1 but pour une
victoire 7-1) ainsi que contre le Bayern Munich (1 but pour une
défaite 3-1). Le 7 décembre 2017, il est classé 7^e et meilleur
français au Ballon d'or à 18 ans et 11 mois, ce qui en fait le
plus jeune joueur de l'histoire à entrer dans le top 10^. Le
20 décembre 2017, jour de ses 19 ans, il est à nouveau
titulaire et marque un but lors de la 19^e journée de Ligue 1 au
Parc des Princes face à Caen, ce qui en fait le meilleur
buteur français de l'année 2017 avec 33 buts, devant Alexandre
Lacazette, évoluant lui cependant en Premier League^.
Lors de la défaite parisienne face à l'Olympique lyonnais, il s'en
va violemment percuter Anthony Lopes lors d'une sortie de
celui-ci, et est obligé de sortir sur blessure. Le 30 janvier

demi-finale de la Coupe de la Ligue, il reçoit le premier
carton rouge de sa carrière pour une faute sur Ismaïla Sarr,
sorti sur civière. Le 21 février 2018, invité par Emmanuel Macron,
il participe avec de nombreuses autres personnalités du monde sportif à
un « déjeuner de travail » à l'Élysée en l'honneur de l'ancien
footballeur George Weah, nouvellement élu président du


Le 25 février 2018, Kylian Mbappé ouvre le score lors de
la victoire (3-0) des Parisiens face à Marseille et devient le
plus jeune buteur lors du Classico en Ligue 1 à 19 ans, 2 mois et
8 jours, détrônant l'ancien Marseillais Samir Nasri, qui avait
marqué contre Paris à 19 ans, deux mois et dix-sept jours, le 10

son premier titre depuis son arrivée au Paris-Saint-Germain face à
l'AS Monaco (victoire 3-0) en finale de Coupe de la Ligue, où il
est élu homme du match. Il est ensuite sacré champion de France
avec le PSG. Il termine la saison en tant que treizième meilleur buteur
de Ligue 1 avec treize réalisations en championnat^,
confirmant son statut de grand espoir du football. Il est surnommé





Auréolé d'une deuxième étoile acquise avec l'équipe de France,
Mbappé manque la préparation, le Trophée des champions remporté
par ses coéquipiers face à l'AS Monaco ainsi que la première
journée de championnat. Il commence la saison lors de la deuxième
journée de championnat, en entrant en jeu à la mi-temps face à
Guimgamp qui mène d'un but des Parisiens en grande difficulté. Son
entrée permet de redynamiser le jeu du PSG qui égalise sur penalty,
avant que Mbappé ne marque un doublé, synonyme de victoire et de trois
points supplémentaires (3-1). Lors de la troisième journée du
championnat de France, Mbappé marque un but et fait une passe
décisive pour son coéquipier Neymar contre Angers. Il marque
son quatrième but de la saison face à Nîmes, match durant lequel
il écope d'une suspension de trois matchs. Le 7 octobre
2018 contre Lyon, il réalise un quadruplé en l'espace de
treize minutes (victoire 5-0)^. Il est alors le plus jeune
joueur (19 ans et 9 mois) à inscrire un quadruplé en championnat

Robert Lewandowski avait lui aussi réalisé un quintuplé en
seulement 9 minutes lors d'un match opposant le Bayern Munich à

Marseille le 28 octobre, Thomas Tuchel le met sur le banc,
une sanction consécutive à son arrivée en retard à la causerie
d'avant-match^. À peine entré sur le terrain à l'heure de jeu,
Mbappé débloque le match en marquant d'un tir croisé face à Steve
Mandanda (victoire finale 0-2)^. Il poursuit dans sa lancée
cinq jours plus tard, en ouvrant le score face à Lille, devenant
ainsi le plus jeune joueur à dépasser les 40 buts en Ligue


Le 4 décembre 2018, alors qu'il se classe quatrième du
ballon d'or, il remporte le premier trophée Kopa,
récompensant le meilleur joueur mondial de moins de 21 ans.
Désigné par un cortège composé de 22 anciens ballons d'or, il est
systématiquement classé premier par chacun d'entre-eux, et récolte donc
le maximum des suffrages (110 points)^. Le début d'année 2019
reste dans la continuité de sa première partie de saison, avec 6 buts


Le 12 février 2019, il marque le deuxième but parisien
face à Manchester United en huitième de finale aller de la
Ligue des champions (0-2). En marquant ce but, il inscrit son 14^e
but en 24 matches de C1 et égale Zinédine Zidane (14 buts en 80
matchs) en nombre de buts dans la compétition^. Le 17 février,
il inscrit l'unique but du PSG face à l'AS Saint-Étienne et
devient le premier joueur français à marquer 19 buts en 18 matchs sur
une saison de Ligue 1 sur les 45 dernières années^. Le 21
février, il marque son 20^e but de la saison lors de la victoire (5-1)
contre Montpellier et devient le meilleur buteur français de
l'histoire du PSG sur une saison de championnat, battant l'ancien
record de Dominique Rocheteau qui avait inscrit 19 buts lors de la

Kylian Mbappé à l'echauffement avec le Paris Saint-Germain le 17


Le 23 février, Kylian Mbappé marque un doublé lors de la victoire (3-0)
des Parisiens contre Nîmes, grâce à ses deux buts, il devient :
88 matches dans l'élite depuis Jacky Vergnes en 1971 (50 buts

sur les 45 dernières saisons à 20 ans et 2 mois, détrônant
Yannick Stopyra (21 ans et 12 mois en 1982) et Djibril

marquer 50 buts à 20 ans et 2 mois, détrônant Rachid Mekhloufi

disputés sur une saison en L1/D1 sans tirer un seul penalty depuis

matches disputés sur une saison en L1/D1 depuis 56 ans est


Le 2 mars, il s'offre deux buts et donne la victoire (2-1) au Paris SG
face à Caen en inscrivant notamment son premier penalty en Ligue
1. Il devient également à 20 ans le plus jeune joueur à marquer 50 buts
sous les couleurs parisienne, détrônant ainsi Mustapha Dahleb qui
avait 25 ans quand il avait atteint la barre des 50 buts^.

En mai, il est élu meilleur joueur et meilleur espoir de Ligue 1 de la


meilleur buteur du championnat de France avec 33 buts^.


Après le retour de Leonardo à la tête du secteur sportif du PSG,
le début de saison commence par une victoire en Trophée des
champions contre Rennes (2-1), au cours duquel Mbappé ouvre déjà


Le 11 août 2019, Mbappé commence la première journée du
championnat par un but et une passe décisive contre Nîmes

Le 23 octobre 2019, il devient à 20 ans et 306 jours le
plus jeune joueur de l'histoire de la Ligue des champions à inscrire 15
buts dans cette compétition, devant Lionel Messi et


Le 26 novembre 2019, il joue son 100^e match avec le PSG face au




En 2016, Kylian Mbappé est un des joueurs majeurs de l'équipe de
France des moins de 19 ans qui remporte le championnat
d'Europe^. Il marque notamment un doublé décisif en
demi-finale face au Portugal^. Il compte finalement sept
buts en onze sélections dans cette catégorie d'âge^.

Mbappé en action avec l'équipe de France face à la Russie le


Jamais passé par l'équipe de France espoirs, il est convoqué pour
la première fois en équipe de France par Didier Deschamps le
16 mars 2017, lors des éliminatoires de la Coupe du
monde 2018 face au Luxembourg et pour un match amical face à
l'Espagne^. Le 25 mars 2017, il rentre en jeu à la
78^e minute pour sa première sélection contre le Luxembourg

Le 13 juin 2017, face à l'Angleterre, il délivre sa première passe
décisive en bleu à Ousmane Dembélé (victoire 3-2)^. Le
31 août, en match éliminatoire de la Coupe du monde contre les
Pays-Bas, il marque son premier but en bleu, le quatrième de la
victoire 4-0. Le 27 mars 2018, il marque deux buts de plus en match
amical en Russie, lors de sa douzième sélection, devenant à 19 ans
et 97 jours le plus jeune buteur à l'extérieur en équipe de France
depuis René Gérard en mars 1933, et le plus jeune depuis 1945 à
réaliser un doublé^. À noter que ce jour là, après avoir porté
les maillots numéros 12 et 20 de la sélection, il porte pour la


Présent sur la liste de 23 joueurs retenus pour le Mondial
2018^, où lors du premier match contre l'Australie, il
devient le plus jeune joueur à prendre part à une rencontre d'un grand
tournoi majeur avec l'équipe de France, à 19 ans et 6 mois, détrônant
ainsi Bruno Bellone (20 ans et 118 jours contre la Pologne en
1982)^. Il inscrit son premier but contre le Pérou le 21
juin lors du second match de poule. À 19 ans, 6 mois et un jour,
il devient ainsi le plus jeune joueur français à marquer dans un
tournoi majeur, détrônant David Trezeguet (20 ans et 246 jours
contre l'Arabie saoudite lors de la Coupe du monde 1998), le premier
joueur à marquer un but en Coupe du monde en étant né après la victoire
de la France en 1998 et le quatrième joueur évoluant au PSG à marquer
pour l'équipe de France en Coupe du monde après Dominique
Rocheteau (1982), Luiz Fernandez (1986) et Blaise Matuidi

Le 30 juin, lors de la victoire de la France (4-3) contre
l'Argentine en huitièmes de finale de la Coupe du monde, Il
provoque trois coup-francs dangereux et un penalty transformé par
Antoine Griezmann en première mi-temps, après avoir été mis à
terre dans la surface par Marcos Rojo à la suite d'un démarrage
depuis son camp où il est chronométré à 37 km/h^^,^.
En deuxième mi-temps, il inscrit un doublé et devient le plus jeune
joueur à inscrire deux buts dans un match à élimination directe de
Coupe du monde depuis Pelé (17 ans et 8 mois en demi-finale du
Mondial 1958 contre la France)^. Il devient aussi le deuxième
plus jeune joueur de l'histoire de la Coupe du monde à marquer 3 buts
joueur français à inscrire au moins deux buts en phase à élimination
directe d'un match de Coupe de monde depuis Zinédine Zidane en


Le 6 juillet, malgré plus de difficulté à pénétrer la défense adverse,
il contribue à la victoire française lors du quart de finale face
face aux Belges^. Lors de la victoire contre la Belgique
Diables rouges. Aucun joueur français n'a réussi autant de dribbles sur
un match de Coupe du monde depuis l'apparition des statistiques, en

Mbappé avec la Coupe du monde, le 15 juillet 2018 à Moscou.

Le 15 juillet 2018, il devient champion du monde après la victoire
française en finale face à la Croatie (4-2)^. Sur
une passe de Lucas Hernandez, il inscrit d'un frappe sèche aux
25 m le quatrième but français et devient le deuxième plus jeune buteur
de l'histoire en finale de Coupe du monde après Pelé (buteur en
finale du Mondial 1958 contre la Suède à 17 ans)^.
Grâce à ce but, il devient également le plus jeune buteur français en
finale d'une compétition majeure, dépassant Bruno Bellone contre
l'Espagne à l'Euro 1984 (22 ans et 105 jours), le deuxième joueur
de la Ligue 1 française à marquer en finale de Coupe du monde après
l'Argentin Jorge Burruchaga en 1986 (il jouait alors au FC

Saint-Germain à inscrire un but en finale d'une Coupe du monde et le
premier joueur parisien à marquer dans une finale internationale depuis
le Nigérian Jay-Jay Okocha lors de la Coupe d'Afrique des
Nations 2000^. Kylian Mbappé n'est par ailleurs que le
deuxième teenager (mot anglais ne correspondant pas tout à fait à
adolescent puisqu'il recouvre une période de 13 à 19 ans), à
marquer en finale de la Coupe du monde, soixante ans après Pelé.

quatre buts, il est désigné « Meilleur jeune joueur » de la compétition
par la FIFA^. À l'âge de 19 ans et 207 jours, il devient
le troisième plus jeune champion du monde de l'histoire, derrière le
Brésilien Pelé en 1958 (17 ans, 8 mois et 6 jours) et l'Italien
Giuseppe Bergomi en 1982 (18 ans, 6 mois et 17 jours)^.
Il réussit 32 dribbles dans cette Coupe du monde, un record pour un
joueur français^. Il est par ailleurs adoubé par Pelé en
personne, qui tweete le 15 juillet « Si Kylian continue d’égaler mes
records comme ça, je vais devoir dépoussiérer mes crampons », à quoi
Mbappé répond : « Le Roi restera toujours le Roi »^. Par
ailleurs, Miroslav Klose, recordman du nombre de buts marqués en
Coupe du monde (seize, en quatre participations entre 2002 et 2014) est
persuadé que Kylian Mbappé peut égaler ou améliorer son record : « Pour
y parvenir, il faudra que la France reste compétitive sur la durée. À
l’heure actuelle, il y a de quoi être optimiste », dit-il^.

Le 11 octobre 2018, en inscrivant son dixième but en sélection face à
l'Islande (2-2), Kylian Mbappé devient le premier joueur de
l'équipe de France à passer la barre des dix buts en équipe de France
avant l'âge de vingt ans^. Lors des matchs de qualification à
l'Euro 2020, il se distingue en marquant deux fois en deux matchs,
rencontres au cours desquelles il est titulaire. Sa première
réalisation intervient en fin de rencontre face à la modeste équipe de
Moldavie, qui permet à l'équipe de France de l'emporter 4-1. Enfin, au
stade de France, il participe également à la large victoire des Bleus
avec un but face à l'Islande (4-0). Il s'agit de son douzième but en
trente sélections, un total qu'il est le premier à atteindre en équipe


Lors de la reprise de ces éliminatoires de l'Euro 2020, très critiqué
après la défaite des Bleus en Turquie (0-2), Kylian Mbappé se reprend
quatre jours plus tard, le 11 juin, et ouvre le score face à l'Andorre
but de sa carrière professionnelle (27 avec l'AS Monaco, 60 avec le
PSG, 13 en équipe de France), mieux que Lionel Messi et



Kylian Mbappé peut jouer à tous les postes de l'attaque (gauche,
droite, pointe ou soutien) alliant vitesse et technicité. Il sait
particulièrement bien exploiter l'espace dans le dos des adversaires


défenseurs, mais aussi grâce à la qualité technique de ses dribbles, y
compris en sprintant^. Capable d'accélérations fulgurantes,
avec une pointe de vitesse à 38 km/h, il est l'un des joueurs les plus
rapides du monde^ et même le plus rapide balle aux pieds
selon certaines sources^. Il est régulièrement remarqué pour
la qualité de ses gestes techniques^. C'est un attaquant qui
participe très activement au travail défensif de récupération de la



Ronaldo^ ou encore, lors de la Coupe du monde 2018,
au « roi Pelé », notamment pour les records de précocité qui le
rapprochent de ce dernier. En 2019, sa tendance à se replacer de
plus en plus comme un attaquant axial au PSG comme en équipe de France,
lui vaut notamment d'être comparé à l'avant-centre emblématique du club




CAPTION: Statistiques de Kylian Mbappé au 11 mars 2020^

Saison Club Championnat Coupe nationale Coupe de la Ligue Supercoupe


Division M B Pd M B Pd M B Pd M B Pd C M B Pd M B Pd
2015-2016 Drapeau de la France AS Monaco Ligue 1 11

2016-2017 Drapeau de la France AS Monaco Ligue 1 29

2017-2018 Drapeau de la France AS Monaco Ligue 1 1

Sous-total 41 16 9 4 2 4 4 3 0 1 0 0 - 10 6 1 60 27 14
2017-2018 Drapeau de la France Paris Saint-Germain
2018-2019 Drapeau de la France Paris Saint-Germain
Ligue 1 29 33 6 4 2 3 2 0 0 - - - C1 8 4 4 43 39 13
2019-2020 Drapeau de la France Paris Saint-Germain
Ligue 1 20 18 5 2 4 1 3 2 3 1 1 0 C1 7 5 4 33 30 13
Sous-total 76 64 18 11 10 7 9 2 5 1 1 0 - 23 13 11 120 90 41
Total sur la carrière 117 80 27 15 12 11 13 5 5 2 1 0 - 33 19 12 180



CAPTION: Statistiques de Kylian Mbappé au 14 novembre 2019^

Saison Sélection Campagne Phases finales Éliminatoires Matchs amicaux


2016-2017 Drapeau : France France Coupe du monde 2018 -


2018-2019 Drapeau : France France Ligue des nations 4 1




CAPTION: Liste des buts en sélection de Kylian Mbappé



Saint-Denis, France Éliminatoires Coupe du monde 2018 V
4-0 Drapeau : Pays-Bas Pays-Bas But inscrit après 90+1



Drapeau : Russie Russie But inscrit après 40 minutes 40^e du

3^e But inscrit après 83 minutes 83^e du pied droit 1-3

Décines-Charpieu, France N 1-1 Drapeau : États-Unis



Drapeau : Pérou Pérou But inscrit après 34 minutes 34^e du



inscrit après 64 minutes 64^e du pied gauche 3-2 19^e
7^e But inscrit après 68 minutes 68^e du pied droit 4-2

Russie V 4-2 Drapeau : Croatie Croatie But inscrit



2018-2019 V 2-1 Drapeau : Pays-Bas Pays-Bas But inscrit



Islande Islande But inscrit après 90 minutes 90^e s.p du pied



Moldavie Moldavie But inscrit après 87 minutes 87^e du pied droit



Islande But inscrit après 78 minutes 78^e du pied droit 3-0 30^e

Andorre-la-Vieille, Andorre V 1-4 Drapeau : Andorre
Andorre But inscrit après 11 minutes 11^e du pied droit 0-1 33^e




Luxembourg Drapeau : Luxembourg Luxembourg V 3 - 1



amical Titulaire, remplacé par Olivier Giroud à la 64^e minute de


Drapeau : Suède Suède D 1 - 2 Éliminatoires de la Coupe
du monde 2018 Entre en jeu à la place d'Antoine Griezmann à la






Olivier Giroud à la 75^e minute de jeu, puis buteur.

Toulouse, France Drapeau : Luxembourg Luxembourg N
0 - 0 Titulaire, remplacé par Kingsley Coman à la 59^e minute de


Sofia, Bulgarie Drapeau : Bulgarie Bulgarie V 1 - 0
Titulaire, remplacé par Olivier Giroud à la 84^e minute de jeu.


Biélorussie V 2 - 1 Entre en jeu à la place de Kingsley Coman


de Galles V 2 - 0 Match amical Titulaire, remplacé par


Cologne, Allemagne Drapeau : Allemagne Allemagne N


France Drapeau : Colombie Colombie D 2 - 3 Titulaire,
remplacé par Ousmane Dembélé à la 66^e minute de jeu.

Saint-Pétersbourg, Russie Drapeau : Russie Russie V
1 - 3 Titulaire et double buteur, remplacé par Thomas Lemar à la


France Drapeau : Irlande Irlande V 2 - 0 Titulaire,
remplacé par Ousmane Dembélé à la 77^e minute de jeu.


remplacé par Florian Thauvin à la 83^e minute de jeu.

France Drapeau : États-Unis États-Unis N 1 - 1 Titulaire
et buteur, remplacé par Thomas Lemar à la 88^e minute de jeu.

Russie Drapeau : Australie Australie V 2 - 1 Coupe


Iekaterinbourg, Russie Drapeau : Pérou Pérou V 1 -
0 Titulaire et buteur, remplacé par Ousmane Dembélé à la 74^e


Russie Drapeau : Danemark Danemark N 0 - 0 Entre en jeu

Russie Drapeau : Argentine Argentine V 4 - 3 Titulaire
et double buteur, remplacé par Florian Thauvin à la 89^e minute de


Nijni Novgorod, Russie Drapeau : Uruguay Uruguay V
2 - 0 Titulaire, remplacé par Ousmane Dembélé à la 88^e minute de





Russie Drapeau : Croatie Croatie V 4 - 2 Titulaire et





Saint-Denis, France Drapeau : Pays-Bas Pays-Bas V 2

Guingamp, France Drapeau : Islande Islande N 2 - 2
Match amical Entre en jeu à la place d'Antoine Griezmann à la


Saint-Denis, France Drapeau : Allemagne Allemagne V
2 - 1 Ligue des nations 2018-2019 Titulaire, remplacé par


Rotterdam, Pays-Bas Drapeau : Pays-Bas Pays-Bas D 0

Saint-Denis, France Drapeau : Uruguay Uruguay V 1 -
0 Match amical Titulaire, remplacé par Florian Thauvin à la




France Drapeau : Islande Islande V 4 - 0 Titulaire et



amical Titulaire, remplacé par Wissam Ben Yedder à la 45^e minute

32 8 juin 2019 Konya Büyükşehir Belediye Stadyumu,
Konya, Turquie Drapeau : Turquie Turquie D 0 – 2




Saint-Denis, France Drapeau : Moldavie Moldavie V 2


Drapeau : France AS Monaco (1) Drapeau : France PSG















insignes lui sont remises en juin 2019 à l'Élysée par le
président de la République, Emmanuel Macron, en même
temps qu'aux autres membres de l'équipe de France championne



















Années Classements % des suffrages Clubs Vainqueurs



2018 4^e 12,04 % Drapeau : France Paris Saint-Germain

2019 6^e 3,16 % Drapeau : France Paris Saint-Germain

















Le lendemain de la victoire de l'équipe de France de football
lors de la finale de la Coupe du monde 2018, le groupe LEJ
a publié une chanson intitulée Liberté, Égalité^.
Spécialement écrite pour l'occasion, elle rend notamment hommage à


Le 15 août 2018, Fyre, un rappeur bulgare de 22 ans^,
publie sur la plateforme YouTube une chanson intitulée Kylian

footballeur^. Ce morceau, entièrement consacré à l'attaquant
français, passe relativement inaperçu jusqu'au 9 octobre 2018, date à
partir de laquelle il est massivement relayé dans les médias français.

Les rappeurs français Médine et Booba lui consacrent le
morceau Kyll la même année^. Le titre est un jeu de mot sur
son prénom et le verbe tuer (« to kill ») en anglais, et
plusieurs passages de la chanson font directement référence à son
métissage comme : « Du nègre, de l'algérien font du
Kylian Mbappé » ou encore « Mbappé Kylian (Mbappé), Alger,

tourné dans les rues d'Alger, est publié sur la chaîne YouTube de


En 2018 toujours, Suprême NTM et Sofiane le citent dans le
titre Sur le drapeau de l'album 93 Empire : « 9.3 c’est l’amour,
la peine ; 9.3 c’est la haine, la paix Ça fabrique des mecs à part, ça


Depuis l'été 2018, les médias et entreprises du monde entier se


Afrique Moyen-Orient le 10 octobre 2018, soulignant la renommée
internationale qu'il a acquise à 19 ans, au-delà des connaisseurs du



Il est le parrain de l'association Premiers de cordée^
qui propose des initiations sportives aux enfants hospitalisés.






Superstars du foot, éditions Albin Michel, 6 février 2019,


interdire de rêver", éditions Solar, 16 mai 2019, 125 p.

