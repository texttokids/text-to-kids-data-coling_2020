Épais manteau
Les adultes n'hésiteront pas à charger si des loups attaquent les bouvillons!
La fourrure du boeuf musqué est très épaisse :
une première couche de duvet recouverte de très longs poils, qu'on appelle les jards, et qui mesurent jusqu'à 60 centimètres de long ! Grâce à ce gros manteau, le boeuf musqué
est le seul mammifère capable d'affronter les tempêtes de neige en altitude. Tout son corps est hyperadapté au froid : silhouette massive, pattes courtes, sabots larges pour ne pas s'enfoncer, petites oreilles cachées sous les poils. Un vrai costaud des neiges !
Gare aux loups!
On reconnaît le boeuf musqué à ses longs poils et à ses cornes, qui forment un bandeau sur son front.

Si le boeuf musqué ne craint pas le froid, il redoute quand même son unique prédateur, le loup. Les jeunes bouvillons font partie de ses proies favorites, mais les boeufs musqués ont une tactiqu En cas d'attaque, ils choisissent un terrain surélevé, où la neige est peu profonde, et ils forment un cercle de défense autour de jeunes. Ainsi, les petits sont bier à l'abri entre les flancs des adul et les loups ne peuvent pas approcher !
Pour trouver des végétaux, le boeuf musqué gratte la neige avec ses sabots. Si la glace est trop épaisse, bing !, il frappe sa tête contre le sol !
