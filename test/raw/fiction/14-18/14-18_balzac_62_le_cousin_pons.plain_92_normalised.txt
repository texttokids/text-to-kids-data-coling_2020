
La Cibot sentit ses cheveux lui brûler le crâne, et un froid glacial l'enveloppa.
- Comment ?... dit-elle hébétée.
- Voilà l'affaire criminelle toute trouvée !... Vous pouvez être accusée de soustraction de testament, répondit froidement Fraisier.

La Cibot fit un mouvement d'horreur.
- Rassurez-vous, je suis votre conseil, reprit-il. Je n'ai voulu que vous prouver combien il est facile, d'une manière ou d'une autre, de réaliser ce que je vous disais.  Voyons ! qu'avez-vous fait pour que cet Allemand si naïf se soit caché dans la chambre à votre insu ?...
- Rien, c'est la scène de l'autre jour, quand j'ai soutenu à monsieur Pons qu'il avait eu la berlue.  Depuis ce jour-là, ces deux messieurs ont changé du tout au tout à mon égard. Ainsi vous êtes la cause de tous mes malheurs, car si j'avais perdu de mon empire sur monsieur Pons, j'étais sûre de l'Allemand qui parlait déjà de m'épouser, ou de me prendre avec lui, c'est tout un !

Cette raison était si plausible, que Fraisier fut obligé de s'en contenter.
- Rassurez-vous, reprit-il, je vous ai promis des rentes, je tiendrai ma parole. Jusqu'à présent, tout, dans cette affaire, était hypothétique ; maintenant, elle vaut des billets de Banque... Vous n'aurez pas moins de douze cents francs de rente viagère... Mais il faudra, ma chère dame Cibot, obéir à mes ordres, et les exécuter avec intelligence.
- Oui, mon cher monsieur Fraisier, dit avec une servile souplesse la portière entièrement matée.
- Eh bien ! adieu, repartit Fraisier en quittant la loge et emportant le dangereux testament.

Il revint chez lui tout joyeux, car ce testament était une arme terrible.
- J'aurai, pensait-il, une bonne garantie contre la bonne foi de madame la présidente de Marville. Si elle s'avisait de ne pas tenir sa parole, elle perdrait la succession.

Au petit jour, Rémonencq, après avoir ouvert sa boutique et l'avoir laissée sous la garde de sa sœur, vint, selon une habitude prise depuis quelques jours, voir comment allait son bon ami Cibot, et trouva la portière qui contemplait le tableau de Metzu en se demandant comment une petite planche peinte pouvait valoir tant d'argent.
- Ah ! ah ! c'est le seul, dit-il en regardant par-dessus l'épaule de la Cibot, que monsieur Magus regrettait de ne pas avoir, il dit qu'avec cette petite chose-là, il ne manquerait rien à son bonheur.
- Qu'en donnerait-il ? demanda la Cibot.
- Mais si vous me promettez de m'épouser dans l'année de votre veuvage, répondit Rémonencq, je me charge d'avoir vingt mille francs d'Élie Magus, et si vous ne m'épousez pas, vous ne pourrez jamais vendre ce tableau plus de mille francs.
- Et pourquoi ?
- Mais vous seriez obligée de signer une quittance comme propriétaire, et vous auriez alors un procès avec les héritiers. Si vous êtes ma femme, c'est moi qui le vendrai à monsieur Magus, et on ne demande rien à un marchand que l'inscription sur son livre d'achats, et j'écrirai que monsieur Schmucke me l'a vendu. Allez, mettez cette planche chez moi...  si votre mari mourait, vous pourriez être bien tracassée, et personne ne trouvera drôle que j'aie chez moi un tableau... Vous me connaissez bien. D'ailleurs, si vous voulez, je vous en ferai une reconnaissance.

Dans la situation criminelle où elle était surprise, l'avide portière souscrivit à cette proposition, qui la liait pour toujours au brocanteur.
- Vous avez raison, apportez-moi votre écriture, dit elle en serrant le tableau dans sa commode.
- Voisine, dit le brocanteur à voix basse en entraînant la Cibot sur le pas de la porte, je vois bien que nous ne sauverons pas notre pauvre ami Cibot ; le docteur Poulain désespérait de lui hier soir, et disait qu'il ne passerait pas la journée... C'est un grand malheur ! Mais après tout, vous n'étiez pas à votre place ici... Votre place, c'est dans un beau magasin de curiosités sur le boulevard des Capucines. Savez-vous que j'ai gagné bien près de cent mille francs depuis dix ans, et que si vous en avez un jour autant, je me charge de vous faire une belle fortune... si vous êtes ma femme... Vous seriez bourgeoise... bien servie par ma sœur qui ferait le ménage, et...

Le séducteur fut interrompu par les plaintes déchirantes du petit tailleur dont l'agonie commençait.
- Allez-vous-en, dit la Cibot, vous êtes un monstre de me parler de ces choses-là, quand mon pauvre homme se meurt dans de pareils états...
- Ah ! c'est que je vous aime, dit Rémonencq, à tout confondre pour vous avoir...
- Si vous m'aimiez, vous ne me diriez rien en ce moment, répondit-elle.

Et Rémonencq rentra chez lui, sûr d'épouser la Cibot.

Sur les dix heures, il y eut à la porte de la maison une sorte d'émeute, car on administra les sacrements à monsieur Cibot. Tous les amis des Cibot, les concierges, les portières de la rue de Normandie et des rues adjacentes occupaient la loge, le dessous de la porte cochère et le devant sur la rue. On ne fit alors aucune attention à monsieur Léopold Hannequin, qui vint avec un de ses confrères, ni à Schwab et à Brunner, qui purent arriver chez Pons sans être vus de madame Cibot. La portière de la maison voisine, à qui le notaire s'adressa pour savoir à quel étage demeurait Pons, lui désigna l'appartement. Quant à Brunner, qui vint avec Schwab, il était déjà venu voir le musée Pons, il passa sans rien dire, et montra le chemin à son associé... Pons annula formellement son testament de la veille, et institua Schmucke son légataire universel. Une fois cette cérémonie accomplie, Pons, après avoir remercié Schwab et Brunner, et avoir recommandé vivement à monsieur Léopold Hannequin les intérêts de Schmucke, tomba dans une faiblesse telle, par suite de l'énergie qu'il avait déployée, et dans la scène nocturne avec la Cibot et dans ce dernier acte de la vie sociale, que Schmucke pria Schwab d'aller prévenir l'abbé Duplanty, car il ne voulut pas quitter le chevet de son ami, et Pons réclamait les sacrements.
