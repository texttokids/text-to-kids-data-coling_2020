Wendy rêve de devenir déléguée de la classe. Elle adore l'école, mais elle n'est pas la préférée des élèves. C'est Laura.
Wendy est triste car elle aimerait gagner les élections de la classe mais elle ne sait pas comment faire.
Laura a déjà un programme précis : elle veut rallonger les récréations, supprimer les dictées et l'école le lundi. Alors tout le monde veut voter pour elle.
Wendy, elle, ne sait pas vraiment ce qu'elle veut proposer. Elle voudrait en discuter avec tout le monde et choisir ensemble les bonnes idées.
Le jour de l'élection est arrivé. C'est Laura qui commence. Elle fait son discours et donne ses idées.
Toute la classe applaudit Laura. C'est sûr, Wendy n'a aucune chance.
C'est le tour de Wendy. Elle va au tableau et regarde ses camarades. Elle tremble, elle a très peur, mais elle se lance.
"Je n'ai pas écrit de programme. Si je suis élue déléguée de la classe, nous allons écrire le programme ensemble." Toute la classe regarde Wendy d'un drôle d'oeil. Elle est bizarre Wendy !
"Je vous propose que chaque vendredi, nous nous retrouvions dans la cour, tous ensemble. Chaque semaine, nous parlerons de ce qui ne va pas, des disputes, des devoirs, et aussi des choses qui vont bien."
"Chacun donnera ses idées pour résoudre les problèmes et on choisira tous ensemble les solutions que l'on veut pour la classe. Moi, j'organiserai les débats pour que chacun puisse parler."
Toute la classe reste silencieuse. Mais quelle bonne idée, on pourra tous s'exprimer et partager nos idées ! Même Laura aime cette idée ! Pas de doute, la nouvelle déléguée de la classe, c'est Wendy !
