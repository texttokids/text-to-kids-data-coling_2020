N°41 - 19 au 25 janvier 2018
les théories du complot
Depuis trois ans, de plus en plus de théories du complot sont diffusées en France. Il s'agit d'histoires censées prouver que les personnes puissantes nous mentent pour pouvoir nous faire faire ce qu’elles veulent. Les gens qui y croient sont appelés des complotistes.


Lesly, 13 ans, ne croit pas aux théories du complot

Lesly est en 4e et habite à Rennes, en Bretagne. Quand il était en CM2, un copain lui a raconté que Neil Armstrong, le premier homme à avoir marché sur la Lune, n’y était en fait jamais allé. Que tout était inventé, et que c’était d’ailleurs simple de s’en rendre compte : sur les photos faites à l’époque, on voit que le drapeau flotte au vent, alors qu’il n’y a pas de vent sur la Lune.
«Il avait gobé tout ce qu’il avait pu écouter», se souvient Lesly. Lui n’y a pas cru une seconde. «Vu que ça a été prouvé que Neil Armstrong a marché sur la Lune, pour moi c’était logique de ne pas croire à son histoire», résume l’adolescent. Alors pourquoi le drapeau flotte-t-il ? «Il y avait une barre qui tenait le drapeau, c’était juste pour que ça fasse une bonne photo», explique-t-il.
Ce que lui a raconté son copain est une théorie du complot. C’est-à-dire une histoire servant à montrer que les personnes qui ont le plus de pouvoir dans le monde nous mentent et nous manipulent.
Lesly fait particulièrement attention à ce qu’il lit, parce qu’il est le rédacteur en chef d’une revue scolaire appelée Mediaparks. Son rôle est de veiller à ce qu’elle soit de bonne qualité. Il doit être sûr que ses camarades n’écrivent pas n’importe quoi.
Comment fait-il pour vérifier une information ? «Je regarde ce qui est officiel, ce qui vient de l’Etat, par exemple. Je me pose des questions qui peuvent aller contre les informations officielles, et si je trouve des réponses sûres, j’essaye de modifier l’article. Je fais aussi confiance aux journaux. J’essaye d’aller le plus près possible de la source, détaille Lesly. Les complotistes ont un autre avis que moi sur les médias, mais chacun pense ce qu’il veut.»
Rennes, la ville où habite Lesly


Qu’est-ce qu’une théorie du complot ?

Depuis tout-petits à l’école, on apprend que la Terre est ronde. Mais certaines personnes sont sûres que c’est faux et disent que la Terre est plate. Elles refusent de croire ce qu’on nous raconte officiellement, autrement dit les informations qui viennent des personnes qui ont du pouvoir (les présidents et ministres, les journalistes…). On appelle ces personnes des complotistes.
Elles pensent que ceux qui nous dirigent font tout le temps des complots, c’est-à-dire qu’ils mentent et cachent des choses pour avoir plus de pouvoir et pousser le peuple à faire ce qu’ils veulent, sans que ça se sache. «Comme si, par exemple, les parents manigançaient avec les professeurs pour que les enfants aient des mauvaises notes et qu’ils soient punis», explique Thomas Huchon, journaliste et réalisateur d’un documentaire appelé Comment nous avons piégé les complotistes.
«Les théories du complot sont vieilles comme le monde», explique Pascal Froissart, professeur de sciences de l’information et de communication à l’université Paris-VIII (8). L’expression «théorie du complot» a été utilisée pour la première fois au XIXe (19e) siècle.
Mais ces fausses informations se sont beaucoup développées dans les années 2000, surtout après les attentats du 11 septembre 2001, où des terroristes ont pris le contrôle de plusieurs avions pour tuer de nombreuses personnes à New York, aux Etats-Unis. C’est arrivé à un moment où Internet se développait, ce qui a facilité la diffusion de théories du complot.
En France, elles se sont multipliées après les attentats de janvier 2015 contre le journal Charlie Hebdo et le magasin Hyper Cacher, à Paris. Certaines personnes racontent que les victimes n’ont pas été tuées par des terroristes mais par des espions proches du président de la République. Et donc qu’on nous cache la vérité.
Ce qui est compliqué, pour séparer le vrai du faux, c’est qu’il y a des complots qui ont vraiment existé, comme celui de grands fabricants de tabac, aux Etats-Unis. Pendant des dizaines d’années, ils ont fait croire que la cigarette n’était pas mauvaise pour la santé alors qu’ils savaient que c’était faux : des études scientifiques l’avaient prouvé. Ils ont menti pour que les gens continuent à acheter des cigarettes et pouvoir ainsi gagner de l’argent. Mais ça ne veut pas dire pour autant que tout le monde nous manipule tout le temps.


Comment les complotistes arrivent-ils à nous convaincre ?

Bizarre, bizarre, ce triangle dessiné dans le champ. Bizarre, bizarre, ce nuage en forme de visage. Les humains voient des bizarreries partout. Et c’est normal : notre cerveau cherche à donner du sens à tous les détails.
Avec les théories du complot, c’est pareil. Les complotistes utilisent de vrais éléments, des faits réels, mais leur donnent une mauvaise signification : ça donne l’impression qu’on nous ment. Puisque le vrai et le faux sont mélangés, les théories du complot nous paraissent parfois possibles.
Certains pensent que l’Américain Neil Armstrong, le premier astronaute à avoir marché sur la Lune, n’y a en fait jamais mis le petit orteil. Selon les complotistes, l’absence d’étoiles dans le ciel ou le fait que le drapeau flotte sur les photos alors qu’il n’y a pas de vent sur la Lune prouvent que tout a été mis en scène dans un studio de cinéma.
Des scientifiques ont pourtant expliqué que tout était normal. Si on ne voit pas les étoiles, c’est parce que les caméras avaient été réglées de sorte que l’on voie bien les humains. Elles n’étaient pas assez puissantes pour capter l’image des étoiles derrière en même temps. Quant au drapeau américain, il ne flotte pas, il est simplement froissé et accroché à une tige horizontale, afin de rester droit.
«Les complotistes jouent aussi sur l’émotion», raconte Guillaume Brossard, chasseur de théories du complot et fondateur du site internet HoaxBuster. Dans leurs vidéos ou dans leurs articles, les complotistes mettent souvent des images choquantes ou des musiques angoissantes. «C’est pour nous faire peur ou nous mettre en colère, comme ça on croit plus facilement à leurs théories», explique-t-il.
En France, certaines personnes pensent que les attentats qui ont eu lieu à Paris en novembre 2015 sont un complot préparé par l’ancien président François Hollande et des espions. «Si on croit à ces théories, c’est avant tout parce que c’est pratique, analyse le journaliste Thomas Huchon. Il est plus rassurant de croire que des groupes secrets ont organisé des attentats plutôt que de penser que n’importe qui peut être un terroriste.»
Selon les complotistes, il ne faut pas croire ce que disent les journalistes. Et quand les journalistes démontrent que les histoires des complotistes sont fausses, les complotistes répondent que les médias sont complices du complot.


Pourquoi cela pose-t-il problème ?

Tout le monde est libre de penser ou de croire ce qu’il veut. Généralement, les complotistes cherchent des informations sur ce qu’ils pensent être la vérité, et ça s’arrête là. Mais parfois, les théories du complot peuvent être dangereuses.
En décembre 2016, par exemple, un Américain a débarqué avec un fusil dans une pizzeria de Washington, la capitale des Etats-Unis. Après avoir lu plusieurs articles sur des sites complotistes, il était persuadé que le restaurant abritait des criminels soutenus par certains hommes politiques proches de l’ancienne candidate à l’élection présidentielle, Hillary Clinton. En vérité, il n’y avait rien du tout.
Les théories du complot «sont l’une des principales causes de radicalisation politique et religieuse», souligne le journaliste Thomas Huchon. A force de penser qu’on nous ment tout le temps, on se dit que la société est injuste, qu’on ne peut faire confiance à personne. On développe alors des idées de plus en plus dures.
Ça peut aussi créer de la haine envers un groupe de gens. Depuis très longtemps, certains croient à un «complot juif». Selon eux, les personnes de religion juive feraient tout pour dominer le monde, empêchant les autres d’avoir du pouvoir. En croyant à ça, ils en veulent aux juifs et se disent : pourquoi ont-ils tout et pas moi ? Parfois, des personnes ont alors envie de leur faire du mal, pour se venger.
«Certains terroristes étaient des adeptes de théories du complot», rappelle le journaliste Thomas Huchon. A force d’entendre, dans des vidéos complotistes, que les pays les plus riches (dont la France et les Etats-Unis) manigancent contre les musulmans et choisissent leurs lois exprès pour les punir, certains ont eu envie de se venger et de commettre des attentats.


Comment reconnaître une théorie du complot ?

Sur les sites internet et les réseaux sociaux, n’importe qui peut publier des informations. Dur dur dans ce cas de savoir qui dit vrai… Voici quelques conseils du P’tit Libé et du chasseur de théories du complot Guillaume Brossard pour repérer des informations complotistes.
Les complotistes ne disent pas qu’ils partagent un complot. D’ailleurs, ils détestent qu’on les traite de complotistes. Ils insistent sur le fait que, contrairement aux autres, ils disent la vérité. Mais les personnes qui disent la vérité ne prennent généralement pas la peine de l’affirmer, elles donnent juste les informations.
En plus, si quelqu’un assure avoir enfin trouvé la vérité et la révèle sur Internet, pourquoi cette information n’est-elle pas connue de tout le monde ? C’est louche !
Les complotistes disent souvent qu’ils ont les preuves de ce qu’ils disent. Mais quand un journaliste révèle une information, il explique ce qui se passe, prouve ce qu’il dit et n’a pas besoin d’insister en disant «J’ai les preuves», le lecteur le comprend.
Et puis les complotistes font les choses à l’envers : ils sont sûrs de quelque chose et vont utiliser des informations pour prouver qu’ils disent vrai. Or quand on enquête, on cherche d'abord des informations, puis à la fin on regarde à quelle conclusion on arrive.
Les complotistes ne croient pas au hasard : si quelque chose s’est passé, il y a forcément une explication. Mais dans la vie, le hasard et les coïncidences existent. Si une journaliste à la télévision porte une robe rouge, ça peut être parce qu’elle aime cette couleur ou parce que ça rend bien à l’écran, pas pour envoyer un message caché à notre cerveau…
Pour prouver ce qu’ils disent, les complotistes invitent des scientifiques ou citent des études. Ça fait plus sérieux. Dans ce cas, il faut chercher qui est la personne invitée et/ou retrouver l’étude dont ils parlent. Il faut toujours chercher la source d’une information, c’est-à-dire d’où elle vient. Puis voir si cette source a l’air sérieuse ou si elle a l’habitude de partager des théories du complot. Et il faut croiser les sources, c’est-à-dire vérifier à différents endroits si la personne dit vrai.


