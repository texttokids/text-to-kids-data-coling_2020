N°89 - 18 au 24 janvier 2019
les impôts
Depuis le 1er janvier, on ne paie plus les impôts comme avant. Maintenant, l’Etat prend directement de l’argent sur les salaires. Les impôts, c’est une somme d’argent payée sous différentes formes pour que le pays fonctionne bien. C’est grâce à ça que tous les enfants peuvent aller à l’école et être en sécurité, par exemple. Qui paye des impôts et à quoi ça sert dans notre quotidien exactement ?Cette semaine, je t’explique comment tout ça fonctionne.


Martine travaille dans les finances publiques

Martine, 58 ans, a un rôle important : elle veille à ce que le pays reçoive bien l’argent qu’on est censé lui donner. Cet argent, ce sont les impôts. Ils sont versés à un organisme appelé le Trésor public. «A l’époque, le roi avait un trésor. Aujourd’hui, ce trésor c’est celui de l’Etat, c’est-à-dire de tous les citoyens», explique Martine, qui travaille à la direction générale des finances publiques, à Paris.
Les citoyens donnent de l’argent à leur ville ou à l’Etat, par exemple. On les appelle des contribuables, car ils contribuent au fonctionnement du pays : ils financent différents services, comme l’école ou les routes. Des services qui peuvent être utilisés par tous, et souvent gratuitement. «Si on veut vivre dans un pays qui fonctionne de façon équitable et qui nous donne la liberté et la paix, ça a un prix, remarque Martine. Il faut des enseignants pour apprendre aux jeunes à lire et à écrire ou des policiers pour notre sécurité. C’est pour ça qu’on paie des impôts.»
Martine travaille depuis 25 ans dans ce domaine. Elle est déjà intervenue dans des classes pour expliquer à quoi servent les impôts. Quand les enfants vont à l’école publique, ils ne se rendent pas compte que tout a été payé avant. Il faut bien construire l’école, l’entretenir, payer le gymnase, les ordinateurs, les livres, les chaises et les tables, mais aussi tout le personnel, comme les instituteurs.
C’est à ça que sert l’argent des impôts. «Ce n’est pas parce qu’on ne paie pas l’école que c’est gratuit. La grande majorité des choses ont été payées par l'Etat ou la ville. C’est important de ne pas dégrader le matériel scolaire, ça coûte de l’argent», dit Martine.
Paris, la ville où travaille Martine


Pourquoi paye-t-on des impôts ?

Un impôt est une somme d’argent payée par les citoyens et les entreprises à l’Etat ou aux communes, par exemple, pour que le pays fonctionne bien. On en parle beaucoup dans l’actualité parce qu’il y a cette année un grand changement dans la façon dont on récupère cet argent. Par exemple, avant, les travailleurs recevaient un salaire et mettaient une partie de côté pour payer leurs impôts à l’Etat l’année d’après. Depuis le 1er janvier, c’est l’Etat qui prend directement cet argent sur le salaire. On parle de prélèvement à la source.
L’Etat doit en effet rendre aux citoyens tout un tas de services : assurer l’éducation des enfants, garantir la sécurité, rendre la justice, construire des logements, s’occuper des grandes routes… Et ça demande beaucoup d’argent ! En France, cet argent vient de différents impôts.
Pour bien comprendre, les impôts fonctionnent un peu comme quand les parents donnent tous un peu d’argent pour une sortie scolaire, par exemple. Comme ça, tous les enfants peuvent y aller. C’est le même principe pour les impôts : les citoyens versent de l’argent à l’Etat, qui met tout dans un pot commun et utilise tous les sous récoltés pour rendre différents services aux citoyens.
C’est grâce aux impôts que des écoles, des universités, des parcs, des forêts ou des routes sont construits ou entretenus. Ça permet aussi de payer l’armée, la police, la justice… Les impôts servent également à payer les fonctionnaires, c’est-à-dire les personnes qui travaillent au service de l’Etat : les enseignants, les infirmiers, les pompiers professionnels, les juges…
Chaque année au mois de mai, les citoyens doivent déclarer à l’Etat combien ils ont gagné l’année d’avant et payer des impôts en fonction de l’argent qu’ils ont reçu. On appelle ça l’impôt sur le revenu. Payer ses impôts est un acte citoyen. Si quelqu’un triche, ce sont les autres citoyens qui vont payer pour lui. Voilà pourquoi un fraudeur (tricheur) risque de payer une grosse amende, et même aller en prison.
Les impôts, ce n’est pas nouveau Les impôts existent depuis plus de 830 ans en France et ils ont bien changé. Pendant longtemps, ils servaient surtout à financer les guerres. Par exemple, il existait la gabelle, un impôt qu’il fallait payer sur le sel.


Qui paye quoi exactement ?

Les impôts sont payés en très grande majorité par les citoyens, et aussi par les entreprises. Tout le monde ne paie pas la même chose, ça dépend parfois de l’argent qu’on gagne ou que l’on a.
C’est une somme d’argent payée directement par des citoyens et des entreprises à l’Etat. Ce sont les impôts qui se voient le plus parce que les citoyens (salariés, retraités…) doivent chaque année déclarer l’argent qu’ils ont gagné.
Quand on a au moins 18 ans, qu’on travaille et vit en France, on doit payer des impôts sur le revenu, c’est-à-dire sur l’argent qu’on gagne. Plus on touche d’argent, plus on paye d’impôts. Si on ne gagne pas beaucoup, on ne paie rien. Comme ça, tout le monde peut vivre un peu plus correctement. Ça s’appelle la solidarité. C’est la même chose pour les entreprises : elles doivent payer un impôt en fonction de l’argent qu’elles gagnent.
Quand on habite un logement, on doit aussi verser chaque année un impôt pour ça. On en paye également un autre si on est propriétaire de son habitation. On donne cet argent à la commune, au département ou à la région.
C’est une somme d’argent payée par tout le monde à ceux qui vendent des produits ou des services. Ceux-ci la reversent ensuite à l’Etat.
En France, ça ne se voit pas mais on paye aussi un impôt dès qu’on achète quelque chose : des bonbons, du pain, des livres, une place pour le ciné ou encore un téléphone. Ça s’appelle la TVA : la taxe sur la valeur ajoutée. Les produits dont on a besoin pour vivre comme le pain ou les tickets de transports ont une petite TVA, alors que les meubles ou les jouets en ont une plus grande parce que ce n’est pas indispensable. Les commerçants reversent ensuite cette somme à l’Etat.
Les automobilistes payent une taxe, c’est-à-dire de l’argent, à chaque fois qu’ils achètent du carburant par exemple. Il faut aussi payer une taxe sur l’alcool, le tabac ou le loto.
En plus de ces impôts, les employeurs et les salariés versent de l’argent pour aider les citoyens à différentes périodes de leur vie : en cas de maladie, d’accident grave, de congé maternité, de chômage ou quand ils sont à la retraite, parce qu’il faut bien qu’on continue à avoir de l’argent quand on ne travaille plus. On appelle ça la protection sociale.


Qui décide où va l’argent des impôts ?

C’est le gouvernement qui propose comment l’argent des impôts va être dépensé. Il ne prend pas cette décision tout seul dans son coin : c’est très contrôlé, parce qu’il n’a pas le droit de dépenser l’argent des Français n’importe comment. Il y a plusieurs étapes à suivre.
Il faut d’abord préparer le budget de l’Etat. Pour faire ça, le Premier ministre regarde dans quel état se trouve le pays : combien de personnes n’ont pas de travail, si les Français dépensent assez pour que les entreprises gagnent de l’argent… Ensuite, il faut calculer ce qui entre dans les caisses de l’Etat, comme les impôts, et ce qui sort des caisses pour payer les salaires des fonctionnaires, la construction de routes, l’entretien des parcs ou des écoles… Ce sont des calculs compliqués, d’autant que l’Etat dépense plus d’argent qu’il n’en reçoit. C’est tellement compliqué qu’il faut un an pour préparer le budget.
Ensuite, au mois d’avril, le ministre du Budget négocie avec tous les autres ministres la somme que chacun aura dans son domaine : celui de la culture, du sport, de l’environnement, de l’agriculture, et ça demande beaucoup d’argent.
Pour 2019, par exemple, le ministère de l’Education nationale, qui s’occupe de l’école, a droit à plus de 51 milliards d’euros. Avec tout cet argent, il doit payer les enseignants (ils sont plus de 955 000), tout le personnel qui encadre les enfants, l’entretien des bâtiments, le mobilier, le téléphone ou encore le matériel informatique. Le gouvernement ne peut pas verser la même somme d’argent à chaque ministère, alors il faut négocier.
Une fois que tout le monde est tombé d’accord, il faut décider où on met l’argent précisément. Sur ces 51 milliards d’euros, l’Education a par exemple droit à plus de 2 milliards d’euros pour financer l’accompagnement des élèves handicapés.
Enfin, à partir d'octobre, ce budget est discuté au Parlement où travaillent les députés et les sénateurs. Une fois le budget voté, ils vérifient que le gouvernement le respecte bien.


Pourquoi les impôts sont-ils critiqués ?

En France, certains services sont payés par tout le monde, qu’on en ait besoin ou pas. Par exemple, même si on n’a pas d’enfants, on paie des impôts qui servent à financer les écoles. On paie aussi pour que les hôpitaux fonctionnent même si on n’est pas malade. Comme ça quand on a un accident, on peut être soigné.
Le problème, c’est que des personnes ont besoin d’être soignées ou d’étudier mais qu’il n’y a pas d’hôpital ou d’école à côté de chez elles parce qu’elles habitent des communes où il n’y a pas beaucoup d’habitants. C’est notamment pour cette raison que des «gilets jaunes», les personnes qui manifestent et bloquent des routes dans le pays depuis plusieurs semaines, sont en colère.
«Avec les gilets jaunes, une question importante revient sur la table, remarque Agnès Michel, spécialiste de la fiscalité et membre de l’association Terra Nova : Est-ce que le pot commun dans lequel tout le monde met de l’argent est bien utilisé ensuite ? Si je paye des impôts pour la santé mais que je n’ai pas un médecin ou un hôpital à côté de chez moi, c’est embêtant. Ça veut dire que je paye mais que je n’ai pas le service qui va avec.»
De nombreux gilets jaunes accusent aussi le président, Emmanuel Macron, de favoriser les plus riches parce qu’il a supprimé l’an dernier l’impôt de solidarité sur la fortune (on dit aussi ISF). Il a été remplacé par un autre impôt qui concerne moins de personnes : les investisseurs ne le paient plus. Pour les gilets jaunes, c’est un cadeau fait aux plus riches. Emmanuel Macron leur répond que c’est de l’argent pour aider les entreprises qui en ont besoin.
L’impôt qui rapporte le plus d’argent à l’Etat est celui qui vient de la TVA, que tout le monde paie en faisant ses courses. Qu’on gagne 10 000 euros par mois ou 1 500 euros, on paie la même la somme sur le dentifrice, le pain ou les vêtements. Certaines personnes trouvent que ce n’est pas juste.
Ensemble des personnes et des organisations qui dirigent un pays.


