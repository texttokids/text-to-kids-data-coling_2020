


Demandez-lui si vous avez besoin d'une consultation avec un chirurgien. Il pourrait être nécessaire de consulter un chirurgien si votre facture est grave, par exemple lors d'une fracture composée. Certaines fractures sont instables et demandent une intervention chirurgicale pour remettre les morceaux d'os en place (par exemple avec des câbles et des vis) pour que l'os puisse guérir correctement 
Une fracture qui empêche sérieusement votre mobilité et qui provoque un mauvais alignement de vos phalanges va surement devoir être opérée pour que le doigt puisse retrouver sa liberté de mouvement.
Vous pourriez être surpris de vous rendre compte combien il est difficile de faire des tâches de la vie quotidienne sans posséder la mobilité complète de ses doigts. Certains professionnels comme les chiropraticiens, les chirurgiens, les artistes et les mécaniciens ont besoin du maximum de mobilité de leurs doigts pour exécuter avec précision leur travail. C'est pourquoi il est essentiel de prendre soin de ses fractures aux doigts.







Mettez de la glace et de la pression sur votre doigt relevé. Gérez le gonflement et la douleur en mettant de la glace et de la pression sur votre doigt que vous avez relevé. En vous occupant rapidement de votre blessure, vous arriverez à guérir plus rapidement. Assurez-vous aussi de donner du repos à votre doigt 
Mettez de la glace sur le doigt. Enroulez une poche de légumes congelés ou de glace dans une serviette fine et appliquez-la doucement sur le doigt pour faire réduire le gonflement et la douleur. Appliquez la glace immédiatement après la blessure et retirez-la toutes les 20 minutes.
Appliquez de la pression sur la blessure. Enroulez doucement, mais fermement le doigt dans un bandage élastique pour gérer le gonflement et immobiliser le doigt. Lors du premier rendez-vous avec le médecin, demandez-lui si vous pouvez immobiliser votre doigt pour éviter qu'il ne continue de gonfler ou pour l'empêcher de gêner les mouvements des autres doigts.
Relevez votre main. Lorsque cela est possible, gardez vos doigts au-dessus du niveau de votre cœur. Il pourrait être plus confortable de vous assoir sur un canapé en posant vos jambes sur des coussins et votre main sur le dossier du canapé.
Vous ne devriez pas non plus utiliser votre doigt blessé pour les activités de la vie quotidienne jusqu'à ce que votre médecin vous dise le contraire.



Demandez à votre médecin si vous avez besoin d'une attelle. Les attelles sont utilisées pour immobiliser les doigts cassés pour éviter de les abimer encore plus. Vous pouvez fabriquer une attelle d'urgence avec un bâtonnet en bois et un bandage souple jusqu'à ce que vous arriviez chez le médecin pour vous faire poser un bandage plus approprié 
Le genre d'attelle dont vous avez besoin peut varier selon le doigt fracturé. Les fractures mineures pourraient être maintenues en place en maintenant le doigt fracturé contre le doigt qui se trouve à côté.
Vous pourriez aussi avoir besoin d'une attelle qui empêche votre doigt de se plier en arrière. Une attelle souple est installée pour que le doigt blessé reste légèrement recourbé vers la paume de la main et maintenu en place par des attaches souples.
Une attelle en aluminium en forme d'U peut empêcher votre doigt de s'étirer. Elle est installée sur le dos du doigt pour l'empêcher de bouger 
Dans les cas les plus graves, votre médecin pourrait installer une attelle flexible en fibre de verre qui part de votre doigt jusqu'au poignet. C'est en fait une sorte de miniplâtre pour votre doigt.



Demandez à votre médecin si vous avez besoin d'une intervention chirurgicale. La chirurgie pourrait être nécessaire pour traiter et guérir correctement la fracture lorsque l'immobilisation et la patience ne suffisent pas. En général, les fractures qui demandent une intervention chirurgicale sont plus compliquées que celles qui demandent une simple immobilisation 
Une fracture composée, une fracture instable, la perte de fragments osseux ou une fracture qui met en danger une articulation demandent une opération chirurgicale, car les fragments d'os doivent être remis en place pour que l'os puisse guérir dans la bonne configuration.



Prenez des analgésiques. Votre médecin pourrait vous recommander de prendre des médicaments antiinflammatoires non stéroïdiens (AINS) pour vous aider à gérer la douleur provoquée par le doigt cassé. Les AINS permettent de réduire les effets négatifs d'une inflammation qui dure et de soulager la douleur et la pression sur les nerfs et les tissus tout autour. Les AINS n'empêchent pas la guérison 
On utilise généralement de l'ibuprofène (Nurofen) ou du naproxène (Aleve) pour soulager la douleur provoquée par les fractures. Vous pouvez aussi prendre du paracétamol (Dafalgan), mais ce n'est pas un AINS et il ne permet pas de réduire l'inflammation 
Votre médecin pourrait aussi vous prescrire un médicament qui contient de la codéine pour gérer la douleur sur le court terme si celle-ci devient trop intense. La douleur va probablement être beaucoup plus forte au début du processus de guérison et votre médecin va réduire la dose du médicament au fur et à mesure que l'os guérit.
