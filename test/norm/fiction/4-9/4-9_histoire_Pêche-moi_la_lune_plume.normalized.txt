plume est un petit crapaud fils de Crapautine et de Crapoto .
Crapoto est un humble chasseur de mouches et il aimerait bien que son fils prenne la relève lorsqu' il ne sera plus en mesure de chasser .
mais , depuis son plus jeune âge , Plume ne rêve que d' une chose : épouser une belle princesse .
un jour qu' il pêche au bord de son étang préféré , le sol se met à trembler .
cet énorme bruit saccadé n' est autre que le galop des chevaux d' un carrosse royal .
tous ses sens en émoi , il se précipite au bord du chemin et , avant que les cent chevaux de la délégation ne s' éloignent dans un nuage de poussière , il l' aperçoit .
elle .
la Princesse Lila .
elle est d' une beauté renversante et quand Plume la voit il en tombe éperdument amoureux .
il retourne chez lui , en toute hâte .
prépare son petit baluchon avec sa guitare et quelques sandwiches à la mouche et au hanneton ( pour ne pas mourir de faim sur le chemin ...
) , embrasse sa maman Crapautine , qui ne peut retenir une larme , son papa Crapoto et , le coeur gros mais bien décidé à aller jusqu' au bout du monde pour retrouver l' amour de sa vie , il prend la route .
durant des jours et des nuits , il marche sans relâche , en suivant la piste des chevaux royaux .
parfois il perd leurs traces et , désespéré , songe à abandonner .
mais , notre ami est persévérant et , bien vite , il retrouve les empreintes et reprend son périple .
c' est au terme de cent jours et cent nuits de voyage , parfois sous le vent et la pluie , que notre petit crapaud réussit à atteindre le palais de Lila .
il lui faut encore trente jours de plus pour traverser les douves et escalader les hautes murailles du château .
et , un soir d' été , tous ses efforts sont récompensés : l' ultime ascension de la tour ouest lui permet d' atteindre le balcon de la chambre de Lila .
elle est aussi belle que le jour où il l' avait aperçue , au bord du chemin de l' étang .
elle brosse ses longues boucles dorées et se parle en se regardant dans un miroir : " je suis vraiment la plus belle du royaume ... non du monde entier . que dis je , je suis la plus belle de l' univers ! "
de là où il est , Plume ne peut pas l' entendre .
il ignore que Lila est vaniteuse , égoïste et capricieuse .
elle n' aime que les belles choses et déteste ne pas obtenir ce qu' elle désire .
notre petit crapaud va l' apprendre à ses dépens .
empli d' amour , il s' apprête à lui chanter une chanson .
dans son baluchon , il se saisit de sa guitare et commence son ode à l' amour .
Lila se retourne et ne peut retenir une grimace de dégout : " pouah ! ! ! ! qui es tu vilaine créature ? et quelle est cette horrible chanson ! arrête donc ! tu me casses les oreilles ! ! ! ! " un peu surpris mais pas découragé , le petit animal se présente : " bonjour , je suis Plume , le petit crapaud . depuis que je t' ai aperçue près de mon étang où je pêchais , je ne souhaite qu' une seule chose , t' épouser . veux tu devenir ma femme ? "
Lila se met à rire et , après un instant de réflexion , elle demande à Plume : " que serais tu capable de faire pour moi ? " plume , toujours aveuglé par ses sentiments , ne voit pas que Lila se joue de lui .
il lui répond : " tout . je suis prêt à tout pour toi ! " Lila , réfléchit un instant .
une robe ?
des bijoux ?
un parfum ?
non , elle en possède déjà tellement .
pour " la princesse la plus belle de l' univers " , il faut une preuve d' amour vraiment extraordinaire .
ça y est !
Lila sait .
" puisque tu sais pêcher , je ne veux qu' une seule chose : plume , pêche moi la lune et je T épouserai ! mais si tu n' es pas revenu d' ici un mois , notre marché ne tient plus . "
pendant des nuits , le petit crapaud essaye de pêcher la lune dans l' eau saumâtre des douves .
il refuse de se décourager mais , quand le dernier quartier de lune disparait dans la nuit noire , il doit se résoudre à abandonner .
le coeur brisé , il s' assoit dans un coin , prend sa guitare et se met à chanter son désespoir .
ce que Plume ne sait pas c' est qu' il n' est pas seul ce soir .
à quelques pas de lui , cachée entre les roseaux , Iris écoute sa douce mélancolie et , comme envoutée , elle se décide à se rapprocher du malheureux chanteur .
quand Plume voit apparaître cette jolie grenouille , son coeur se remet à battre la chamade .
c' est une évidence pour lui , il a enfin trouvé sa princesse .
le temps de prendre leur baluchon et voilà nos deux tourtereaux en route pour l' étang natal de Plume .
leur coeur est si léger que le retour leur semble rapide comme un battement de cil .
c' est avec une joie infinie , que Crapoto et Crapautine voient revenir Plume .
et quel bonheur de voir Iris à ses côtés !
quelques jours plus tard , ils célèbrent un fabuleux mariage et , dès lors , vécurent heureux jusqu'à la fin des temps .
quant à la princesse Lila ?
elle ne trouva jamais un prince à la mesure de ses exigences et finit seule , vieille et triste .
regrettant , parfois , l' absence de la vilaine petite bête qui , par amour , était prête à lui pêcher la lune .