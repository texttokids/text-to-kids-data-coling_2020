prologue
cette histoire , que je vais vous conter , commence par l' arrivée au monde d' un bébé un jour de juillet mille neuf cent quatre vingt quinze.
cependant Anna , cette nouveau née , ne connaitra jamais sa maman .
sa " mamica " est partie , la laissant seule dans ce nouvel espace que les nourrissons , à peine quittée la chaleur protectrice du ventre maternel , découvrent progressivement .
qu' est ce que cette enfant faisait là , éloignée à jamais du lien maternel ?
pourquoi la vie l' avait elle projetée dans cette sphère où sa mère n' avait désormais plus de place ?
à dix huit ans à présent , brune , élancée , Anna aime pourtant à parler de son enfance heureuse , elle qui aurait dû vivre une autre vie , ailleurs , très loin du Roussillon .
et surtout près de parents qui l' avaient tant imaginée et s' apprêtaient à l' accueillir avec joie .
bonjour la vie
le sept juillet dix neuf cent quatre vingt quinze, à sept heures douze exactement , dans la maternité d' un petit hôpital d' une ville lointaine , un cri retentit .
une petite fille vient de voir le jour .
Hélène et Patrice , les deux médecins français qui viennent de l' aider à arriver au monde , vont tenir un rôle capital pour son avenir .
peu après la naissance de cette petite orpheline de mère , ils effectuent de nombreuses demandes et recherches au sujet de sa famille .
ils finissent par être informés que le père de la fillette a malheureusement disparu peu avant qu' elle ne voie le jour .
l' enfant n' a , par ailleurs , plus aucune famille directe pour la prendre en charge et s' occuper d' elle .
mariés l' un à autre , les médecins espèrent de longue date obtenir le droit , de la part des autorités , d' accueillir un enfant sans famille .
ils sont aussi très admiratifs devant ce si joli petit bout qui a eu rapidement en elle la force de vivre sans sa maman .
dès lors , ils décident de tout mettre en oeuvre pour être autorisés à l' élever et la voir grandir .
l' enfant restée sans aucun lien familial , ils réussiront quelques mois plus tard , visa accordé et toutes formalités accomplies , à l' emmener jusqu'à leur petite ville roussillonnaise .
le bébé est d' abord prénommée Ana par le personnel de la pouponnière roumaine où elle passe ses premiers mois .
ce sont ses parents de coeur , qui choisissent par la suite de rajouter un " N " à son prénom .
pourquoi ce deuxième " N " ?
est ce pour " nouveau Nid " , puisque leur famille formera désormais son nouveau nid , ou bien pour " nuit de Noël " car ils prévoient de l' amener chez eux la veille de Noël ?
non .
tout simplement parce que celle qui sera à présent sa grand mère du Roussillon , se nomme Anna .
est ce le hasard ou le destin ?
une chose est sure , la petite fille est venue au monde pour être aimée et jamais délaissée .
les PREMIÈRES ANNÉES
les souvenirs qu' Anna garde de son enfance sont gravés dans son coeur .
dès son arrivée dans le Roussillon à l' âge de cinq mois et demi seulement , elle s' était très vite familiarisée avec ce nouvel environnement de vie et ne pleurait que rarement .
étape par étape , elle semblait prendre conscience de ce bonheur auprès de ses parents adoptifs et s' éveillait harmonieusement à la vie .
entrée à l' école maternelle à trois ans , elle aimait jouer avec les autres enfants .
c' était une petite fille adorable , rieuse , appréciée de tous .
pendant les premières années de sa scolarité , passée du statut de bébé à celui d' élève et du cocon familial à la collectivité , elle montrait une incroyable capacité à s' adapter très facilement aux changements .
vint l' entrée au cours préparatoire .
vive et fantaisiste par moments , elle se devait dès lors de rester sérieuse .
après l' été , ses copains de jeu de la maternelle s' étaient transformés malgré elle en simples camarades de classe pendant les cours .
la distraction était désormais réservée à l' heure des récréations .
elle était de loin la plus grande de sa classe , ce qui lui valait de ne jamais passer inaperçue auprès de la maîtresse , que ce soit dans le rang en entrant ou en sortant , ou simplement assise à son pupitre d' écolière .
elle se laissait donc parfois plus facilement surprendre à parler ou à rêver .
néanmoins , après l' école , le soir venu , elle adorait retrouver son univers et ses drôles de copains de toujours , du moins d' aussi loin qu' elle s' en souvienne .
tous l' attendaient dans sa chambre : Teddy l' ours , Marge la vache , Bobby le chien , Froggy la grenouille et tant d' autres .
je ne vais pas vous les énumérer tous car ils sont très nombreux , ces fidèles copains .
aussi , Anna n' a jamais pu se résoudre à s' en séparer .
petite , les avoir auprès d' elle la rassurait .
aujourd'hui encore , elle ressent de temps à autre ce besoin de douceur .
elle aime le contact de leur peluche soyeuse , parfois abimée au fil des ans par de fréquentes manipulations .
ces amis là , les plus usés , sont toujours pour elle les plus attendrissants , car ils la ramènent délicatement vers sa plus petite enfance .
une pointe de fantaisie
en grandissant , la fillette aimait de plus en plus se retrouver en présence d' autres enfants .
parfois , elle excellait dans l' art de leur faire de petites farces ou se plaisait à leur raconter des histoires à faire peur .
comme le jour où elle raconta en classe qu' en allant chercher des champignons dans la nature avec ses parents , ils étaient tombés nez à nez sur un loup .
Fanny , une élève , lui demanda :
" il était gros comment ?
pas très gros , répondit elle d' un air sérieux , mais assez grand !
et de quelle couleur était il ?
questionna Romain .
ben , gris , pardi !
répondit au hasard Anna , toujours aussi sérieuse .
qu' est ce qu' il a fait ?
demandèrent plusieurs enfants .
nous avons tapé dans les mains et il a poursuivi son chemin , dit elle .
il s' agissait certainement d' un chien errant , lança la maîtresse .
oh non , Madame !
rétorqua Anna , comme sure d' elle , c' était bien un loup !

l' institutrice mit fin à cet échange et ordonna aux enfants de s' intéresser au travail du jour .
mais pendant la récréation qui suivit , un attroupement vint se former autour d' Anna , très fière , qui en rajouta .
elle se rendait bien compte qu' elle avait exagéré .
elle se plaisait néanmoins à faire croire aux autres les histoires sorties tout droit de son imagination .
jusqu'à se persuader parfois inconsciemment de les avoir vraiment vécues !
le plus surprenant dans cette histoire là , c' est que le pays où elle a vu le jour compte effectivement de nombreux loups .
au centre de loisirs aussi , les mercredis étaient très animés en sa présence .
avec les autres enfants , elle profitait quelquefois de ces journées de détente pour revêtir des déguisements .
ces travestissements , souvent rudimentaires , lui facilitaient l' invention d' histoires qui parvenaient finalement à faire rire également les animateurs .
évidemment , Anna menait presque toujours la danse .
où trouvait elle autant d' idées ?
que d' histoires n' a t elle racontées !
et le plus souvent des histoires à faire peur , ses préférées .
il était même amusant de voir à quel point les enfants finissaient par se mêler à toutes ces aventures extravagantes sorties de son imagination débordante .
elle captivait autant l' attention de ses camarades du centre de loisirs , témoins privilégiés d' heures d' amusement , que de tous ceux de sa classe .
lors de rencontres , les éducateurs respectifs , ignorant qu' Anna était une enfant adoptive , faisaient parfois part à ses parents des impressions liées à son comportement .
ils trouvaient son intarissable esprit d' invention surprenant .
certains allaient même jusqu'à leur évoquer la curieuse sensation que plusieurs vies transparaissaient dans son attitude .
il en était de même pour les objets qu' elle touchait ou déplaçait .
ils changeaient d' usage selon ce qu' elle avait décidé de raconter .
elle formait , à elle seule , une " entreprise de recyclage d' objets " le temps d' une narration .
une fois , alors qu' elle venait de rentrer au cours moyen , elle se mit en quête , lors de la récréation , de relater à ses camarades l' extrait d' un film dont elle avait entendu parler par des plus grands , chez les amis de ses parents .
il s' agissait d' une simple comédie satirique .
mais vous n' aurez aucun mal à imaginer qu' elle se mit à broder , allant même jusqu'à transformer certains détails pour les rendre effrayants .
le film contant les péripéties d' invités au château du comte Dracula dans les Carpates en Roumanie , elle s' exclama subitement au milieu de son récit d' un ton monocorde jusqu' alors :
" fermez toutes les portes ! empêchez les de sortir ! je veux leur sang ! "
les enfants réunis autour d' elle , surpris , sursautèrent simultanément , certains émettant un cri d' horreur .
son habileté pour joindre le geste à la parole donnait encore plus de piquant à l' histoire .
" et alors ?
demanda Lara , impatiente .
alors , il les poursuivit dans tout le château , continua Anna .
et puis ?
vite , raconte nous !
la cloche va bientôt sonner pour entrer en classe !
poursuivit Gaël .
ils réussirent à passer par une trappe , et ...

la cloche retentit .
les enfants , restés bouche bée et sur leur faim , durent s' éparpiller , car des élèves d' autres classes s' étaient joints au groupe .
à leur plus grande déception , ils se mirent tous en rang .
comme à l' accoutumée , Anna avait su subjuguer ses camarades .
du haut de son mètre quarante cinq , il émanait d' elle un charisme rare pour son âge .
elle attirait la foule et elle fascinait .
mais la fillette , sure de l' affection extrême que lui portaient ses parents dont elle était l' enfant unique , se pensait parfois tellement protégée qu' elle se croyait invulnérable .
elle en profitait pour n' en faire parfois qu' à sa tête .
elle se faisait alors punir , accusée quelquefois par les enseignants de divertir les autres élèves pendant la classe , les écartant ainsi de leur travail .
il fallait en arriver de temps à autre à ces punitions pour qu' elle réalise que la discipline était applicable à tous et que seul le temps de la récréation était dévolu à la distraction .
une nouvelle inattendue
au tout début de l' année où elle allait fêter ses dix ans , les parents d' Anna vinrent la voir dans sa chambre pendant qu' elle jouait et lui demandèrent , l' air cérémonieux , de les écouter attentivement .
" Anna , ma chérie , nous avons quelque chose d' important à te dire , annonça la maman .
le papa se mit à poursuivre , jetant un coup d' oeil furtif à la maman :
oui , Anna , tu es grande maintenant .
c' est pourquoi nous avons pensé , maman et moi , qu' il serait intéressant que tu aies de la compagnie pendant les vacances .
comment cela ?
questionna l' enfant .
ce que veut dire papa , reprit à son tour la maman , c' est que dans le cadre de notre travail , nous avons la possibilité , grâce à une association , de permettre à des enfants que nous soignons , ou avons déjà soignés par le passé , de connaître de meilleurs moments de vie , le temps des vacances .
ce sont des enfants dont les parents ne peuvent plus s' occuper et qui peuvent alors être accueillis temporairement dans d' autres familles .
voilà , donc , ce qu' essaie de te faire comprendre ta maman , continua le père , c' est que nous envisageons d' accueillir un enfant pour les vacances de février , nous verrons comment cela va se passer et si l' essai est concluant , il pourra probablement revenir aux vacances de Pâques , et si tout continue à très bien se passer entre tous , il pourra vraisemblablement rester avec toi chez Nanette et Papy Germain qui prendront le relais l' été prochain .
la fillette écoutait sans broncher .
surprise mais intriguée .
il y eut un petit silence , puis la mère enchaina à nouveau :
Anna , tu te souviens qu' on a déjà parlé ensemble que nous devrions bientôt nous absenter , ton père et moi , pendant une période de deux mois en mission médicale à l' étranger ?
alors , c' est confirmé , c' est pour l' été qui vient .
ah !
fit simplement Anna .
bon !
s' exclama le papa .
nous n' allons pas y aller par quatre chemins .
tout cela pour te dire que nous avons contacté l' association en relation avec notre équipe médicale déjà sur place et un jeune garçon pourrait venir en février prochain afin de se familiariser avec nous tous .
qu' en penses tu ?
j' aurais préféré que vous restiez cet été !
mais Anna , reprit le père dans un léger soupir , tu sais très bien que quoiqu' il en soit , tu vas passer tout l' été avec Nanette et Papy .
il était prévu que nous ne pouvions pas prendre de repos à cette période .
oui , je sais , répondit elle , dans un murmure .
alors , dit il , nous aimerions bien que tu comprennes que c' est important aussi bien pour toi que pour lui .
pourquoi ?
questionna t elle , étonnée .
eh bien , pour ne pas vous sentir seuls l' un et l' autre pour les grandes vacances !
de plus , tu passes toujours de très bons moments à la campagne , mais lui , ne sait pas ce que signifie passer de belles vacances .
aussi , après un long séjour à l' hôpital , ce ne peut être que bénéfique pour lui .
O .
K .
, marmonna t elle , pas vraiment convaincue qu' un étranger vienne fouler ses plates bandes .
le statut d' enfant unique lui convenait très bien et elle se demanda vraiment ce qui avait pu faire penser le contraire à ses parents .
elle ajouta quand même :
j' aurais préféré que ce soit une fille qui vienne , alors !
bien sûr , trésor , renchérit le papa , nous comprenons , mais pour la période où nous serons absents , seuls des garçons de ton âge , ou à peu près , ont la possibilité d' être accueillis .
les filles qui restent sur place sont beaucoup trop petites pour partir si loin .
tu as l' habitude de tes copains du centre de loisirs pourtant , tu joues beaucoup avec eux .
c' est vrai , dit la fillette , mais il dormira où ?
ne te tracasse pas pour cela , la rassura sa maman , ce n' est pas très important .
nous avons suffisamment de place ici et il en est de même à la campagne chez tes grands parents .
vous aurez chacun votre espace .
bon , alors d' accord !
finit par dire Anna .
à la bonne heure !
conclut le père .
Anna repartit dans sa chambre et avec l' imagination débordante que chacun lui connait , mit en scène une histoire qu' elle conta à ses amies les peluches alignées sur des étagères .
elle leur mima même des détails comme si elles étaient en mesure de la comprendre .
il est vrai que cette rencontre à venir était un bon sujet pour élaborer un nouveau scénario .
et puis , finalement , elle était déjà heureuse de partir l' été prochain chez ses grands parents Germain et Anna , dits Papy et Nanette .
elle les adorait et en plus , ils habitaient un endroit superbe dans la jolie vallée de la Têt , dans le Conflent !
que d' aventures en perspective !