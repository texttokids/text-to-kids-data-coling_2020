et le colosse se mit à pleurer comme une femme .
eh bien , que vous disais je ?
demanda Salvator à Jean Robert , qui regardait avec curiosité cet étrange spectacle .
c' est vrai , dit le poète .
allons , dit Salvator , on te la rendra , ta fille .
vous ferez cela , monsieur Salvator ?
puisque je te le promets .
oui , vous avez raison , c' est moi qui ai tort : du moment que vous promettez , c' est clair que vous tiendrez ...
ah !
faites cela , monsieur Salvator , faites cela , et , s' il le faut , eh bien , voyez vous , je ne vous donnerai plus la peine de me jeter du haut en bas des escaliers .
vous me direz : " jean Taureau , jette toi ! " et je m' y jetterai de moi même .
monsieur Salvator , dit en rentrant l' homme qui s' était chargé d' aller frapper à l' hôpital , c' est ouvert là , en face .
pas pour moi , j' espère ?
dit Barthélémy .
et pour qui donc ?
demanda Salvator .
oh !
je n' y vas pas .
comment ?
tu n' y vas pas ?
je n' aime pas l' hôpital : l' hôpital , c' est bon pour les gueux , et l' on est encore assez riche , Dieu merci , pour se faire soigner chez soi .
oui , seulement , chez soi , on est mal soigné , chez soi , on mange avant le temps , on boit avant l' heure , et , quand on s' est soigné deux ou trois fois chez soi comme tu te soignes , on entre un beau matin à l' hôpital pour n' en plus sortir qu' une nuit ...
allons , Barthélémy !
allons !
je n' en veux pas , de l' hôpital , je vous dis !
eh bien soit !
retourne chez toi , et cherche ta fille toi même , tu commences à m' ennuyer , à la fin .
monsieur Salvator , j' irai où vous voudrez ...
monsieur Salvator , où est l' hôpital ?
mais je le vénère , l' hôpital !
me voilà .
à la bonne heure .
mais vous lui reprendrez ma petite Fifine , n' est ce pas ?
je te promets qu' avant trois jours , tu auras de ses nouvelles .
qu' est ce que je ferai donc pendant ces trois jours ?
tu te tiendras tranquille .
plus tôt , si c' est possible , n' est ce pas , monsieur Salvator ?
on fera ce que l' on pourra .
va t' en !
oui , oui , je m' en vas , monsieur Salvator .
tiens , c' est drôle !
où sont donc mes jambes ?
je ne peux plus marcher !
Salvator fit un signe : deux hommes s' approchèrent de Barthélémy , qui s' appuya sur eux , et qui sortit en disant :
vous m' avez promis , dans trois jours au plus tard , de me donner des nouvelles de ma fille , monsieur Salvator , ne l' oubliez pas !
et , de l' autre côté de la rue , à la porte de l' hôpital , qui allait se refermer sur lui , le charpentier criait encore :
n' oubliez pas ma pauvre petite Fifine , monsieur Salvator !
vous aviez raison , dit Jean Robert , ce n' est pas au cabaret qu' il faut voir les hommes .
ce qu' on entendait au faubourg Saint Jacques , pendant la nuit du mardi gras au mercredi des cendres , dans la cour d' un pharmacien droguiste .
l' opération était finie , le malade à l' hôpital , il ne restait plus aux jeunes gens qu' à se remettre en chemin avec cette consolante idée que , si la fantaisie ne leur fût pas venue de courir les rues de Paris , la nuit , à trois heures du matin , un homme serait mort qui avait peut être encore trente ou quarante ans à vivre .
mais , avant de se mettre en chemin , Salvator demanda à son hôte de l' eau et une cuvette pour laver ses mains tachées de sang .
l' eau était commune , mais les cuvettes étaient rares chez le digne pharmacien , la seule qu' il possédât contenait le sang tiré par Salvator de la veine du charpentier , et Salvator avait bien recommandé que l' on conservât soigneusement ce sang pour le montrer au docteur qui ferait , le matin , la visite à l' hôpital Cochin .
la demande du jeune homme eut donc d' abord l' air d' être une indiscrétion .
le pharmacien regarda tout autour de lui , et finit par dire à Salvator :
dame !
si vous voulez vous laver les mains à grande eau , passez donc dans la cour , et lavez vous à la pompe .
Salvator accepta , quelques gouttes de sang avaient aussi jailli sur les mains de Jean Robert : celui ci suivit son ami .
mais une impression des plus douces les arrêta sur le seuil de la porte de cette cour .
tous deux se regardèrent .
en effet , leur étonnement était grand : ils entendaient tout à coup , du moment que la porte de la cuisine du pharmacien s' était ouverte , au milieu du silence et du calme de cette nuit sereine , vibrer , comme par enchantement , les accords les plus mélodieux .
d' où venaient ces sons suaves ?
de quel endroit ?
de quel instrument céleste ?
il y avait là , tout près , la haute muraille d' un couvent .
le vent d' est enlevait il à l' orgue de l' église ces ravissants accords , pour les apporter aux rares passants de la rue Saint Jacques ?
sainte Cécile elle même était elle descendue du ciel dans cette pieuse maison pour célébrer le mercredi des cendres ?
l' âme de quelque soeur novice , morte à l' âge des anges , s' élevait elle aux cieux aux sons des harpes divines ?
en effet , l' air entendu n' était , certainement , ni un chant d' opéra , ni le solo joyeux d' un musicien , au retour du bal masqué .
c' était peut être un psaume , un cantique , une page déchirée de quelque vieille musique biblique .
celle de Rachel pleurant ses fils dans Rama , et ne voulant pas être consolée , parce qu' ils n' étaient plus !