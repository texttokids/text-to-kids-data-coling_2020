" qu' est ce donc ?
demandai je à Orsola .
" je ne sais ...
allez voir qui se plaint .
" j' essayai de me lever de ma chaise , mais je n' avais pas fait trois pas , que je retombai sur un fauteuil .
" tenez , dit elle , buvez ce dernier verre de vin pendant que j' y vais aller à votre place .
" il arrivait un moment où je ne savais plus faire que ce que me disait Orsola .
je vidai le verre jusqu'à la dernière goutte .
alors ce fut elle qui se leva et sortit .
" je ne sais combien de temps elle resta hors de la chambre : j' étais tombé dans cette somnolence de l' ivresse qui vous isole entièrement de ce qui vous entoure .
j' en fus tiré par le contact d' un verre que l' on approchait de mes lèvres , j' ouvris les yeux et je reconnus Orsola .
" eh bien !
lui demandai je , conservant un vague souvenir des plaintes que j' avais entendues .
" oh !
dit elle , c' est Gertrude qui est bien malade !
" Gertrude ...
malade ?
balbutiai je .
" oui , dit Orsola , elle se plaint de crampes d' estomac et ne veut rien prendre de ma main .
vous devriez descendre et la faire boire vous même , ne prît elle qu' un verre d' eau sucrée .
" conduis moi , dis je à Orsola .
" alors je me souviens que je descendis l' escalier , qu' Orsola me conduisit dans une antichambre , qu' elle me fit sucrer un verre d' eau avec du sucre en poudre , et que , me poussant dans la chambre de la malade :
" allons , portez lui cela , dit elle , et tâchez de ne pas lui laisser voir que vous êtes ivre .
" en effet , honteux moi même de l' état dans lequel je me trouvais , je rappelai toute ma raison , et , marchant vers le lit de Gertrude d' un pas assez ferme :
" tenez , ma bonne Gertrude , lui dis je , buvez ce verre d' eau : cela vous fera du bien !
" Gertrude fit un effort , allongea le bras , et vida le verre .
" oh !
dit elle , monsieur , toujours le même gout !

monsieur , monsieur , un médecin !

monsieur , bien sûr , je suis empoisonnée !
" empoissonnée ?
répétai je en regardant avec terreur autour de moi .
" oh !
monsieur , au nom du ciel !
monsieur , au nom de votre pauvre frère , un médecin !
un médecin !
" je sortis effrayé .
" tu entends ?
dis je à Orsola , elle croit qu' elle est empoisonnée , et elle demande un médecin .
" eh bien , dit Orsola , courez jusqu'à Morsang et ramenez Monsieur Ronsin .
" c' était , en effet , un vieux médecin qui venait quelquefois diner avec nous , lorsque ses courses le conduisaient du côté du château .
" je pris mon chapeau et ma canne .
" voyons , dit Orsola , un dernier verre de vin : il fait froid et vous avez deux lieues à faire .
" et elle me présenta un breuvage qui , quelque habitué que je fusse aux liqueurs les plus fortes , me brula l' estomac comme si j' avais avalé du vitriol !
" je sortis , je traversai le jardin , je gagnai , tout en trébuchant , la porte de la campagne , mais , à peine eus je fait deux cent pas sur la route de Morsang , que je vis les arbres tourner , que le ciel me parut couleur de feu , et que , la terre se dérobant sous mes pieds , je tombai sur le revers du chemin .
" le lendemain , je me retrouvai dans mon lit , il me semblait que je sortais d' un cauchemar horrible !
" je sonnai : Orsola accourut .
" est il vrai que Gertrude soit morte , ou bien l' ai je rêvé ?
" c' est vrai , dit elle .
" mais , ajoutai je hésitant , morte ...
empoisonnée !

" cela , c' est possible .
" comment , c' est possible ?
m' écriai je .
" oui , dit Orsola , seulement , gardez vous d' en parler , attendu que , comme elle n' a rien pris que de ma main ou de la vôtre , on pourrait dire que c' est nous qui l' avons empoisonnée !
" et pourquoi dirait on cela ?
" dame , répondit tranquillement Orsola , le monde est si méchant !
" mais , enfin , il faudrait donner une raison à ce crime , dis je tout épouvanté .
" on en trouverait une .
" laquelle ?
" on dirait que vous vous êtes d' abord débarrassé de la gouvernante pour vous débarrasser ensuite plus facilement des enfants , dont vous devez hériter .
" je jetai un cri et cachai ma tête sous mes draps ... "
oh !
la malheureuse !
murmura le moine .
attendez !
attendez !
dit le mourant , vous n' êtes point au bout ...
seulement , ne m' interrompez pas : je me sens bien faible !
frère Dominique écouta , la poitrine haletante , le coeur serré .
où l' araignée tend sa toile .
Monsieur Gérard poursuivit :
la mort de Gertrude n' éveilla aucun soupçon , elle causa seulement une grande douleur .
les enfants surtout étaient inconsolables .
Orsola voulut remplacer Gertrude près d' eux , mais ils l' avaient en horreur , la petite Léonie surtout ne pouvait pas la voir .
" j' étais tombé dans une mélancolie profonde , pendant quatre ou cinq jours , ce fut moi qui me tins enfermé dans ma chambre .
"était revenu , il essaya de me consoler de cet évènement .
il comprenait que je regrettasse une bonne et fidèle domestique , mais il ne comprenait rien à un chagrin qui ressemblait presque à du remords .
il me proposa de prendre une autre femme pour soigner les enfants , mais les enfant ne s' en souciaient point , et , craignant l' opposition d' Orsola , j' arguai de leur répugnance pour ne pas remplacer la pauvre Gertrude .