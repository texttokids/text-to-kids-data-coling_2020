pourquoi tu n' as plus de cheveux sur la tête Papy ?
tu as rasé ta tête ?
un voleur t' a volé tes cheveux pendant que tu dormais ?
tu as une maladie ?
tu as des poux qui ont mangé tes cheveux ?
mes cheveux sont partis car ils étaient là depuis trop longtemps .
quand on est très vieux , parfois , les cheveux tombent et ne repoussent pas .
et tu n' as pas froid à la tête ?
parfois j' ai un peu froid alors je mets une casquette , mais ça ne me fait pas mal .
oh moi je t' aime Papy comme ça , tu as la peau de la tête toute douce !