l' autocensure pratiquée au Royaume Uni , qui supprimait toute
critique de l' Union soviétique , son alliée pendant la Seconde
c' est qu' elle est pour une bonne part volontaire .
quiconque a
vécu quelque temps dans un pays étranger a pu constater que
certaines informations , qui auraient normalement dû faire les gros
titres , étaient passées sous silence par la presse anglaise , non en
vertu d' une intervention du gouvernement , mais parce qu' il y a eu un
accord tacite pour considérer qu' il " ne fallait pas " publier de
bien que la première édition ait prévu cette préface , elle ne fut pas
en dix neuf cent soixante douze , le bibliothécaire britannique Ian Angus deux noeuds ,
spécialiste des oeuvres de George Orwell a retrouvé le manuscrit de
cette préface , intitulée " The Freedom of the Press " et Bernard
crick l' a publiée dans le The Times Literary Supplement du quinze
septembre dix neuf cent soixante douze, avec une introduction intitulée " How the essay came to
be written " .
Bernard Crick l' a publiée dans l' édition
italienne de La fattoria degli animali précédée d' une " Introduzione
all' introduzione que Orwell soppresse alla Animal Farm " , Arnoldo
Mondadori Editore , collection " oscar " cent deux , dix neuf cent soixante treize .
Orwell écrivit une autre préface pour l' édition en ukrainien ,
préparée par Ihor ev enko deux noeuds ( Kolhosp Tvaryn , Prometej ,
dix neuf cent quarante sept ) .
cette édition était destinée à des réfugiés ukrainiens vivant
dans des camps pour personnes déplacées situés en Allemagne dans les
zones d' occupation anglaise et américaine .
le manuscrit en est perdu ,
elle n' est disponible qu' en retraduction vers l' anglais du texte en
dessin préliminaire pour la conception du dessin animé éponyme .
comme l' indique cet extrait situé en tout début du premier
chapitre , ce roman commence par un rêve dont le contenu évoque
la prise en charge de leur destin par les animaux eux mêmes animés
par les idéaux d' un vieux cochon dénommé Sage l' Ancien , ils décident de
se révolter contre leur maître , Monsieur Jones , dans l' espoir de mener une
vie autonome dans l' égalité , l' entraide et la paix pour tous .
la ferme est passée sous le contrôle des animaux .
elle est dès lors
gérée dans le respect des sept commandements qui prônent le pacifisme
tout en définissant les spécificités des animaux , présentées comme une
richesse .
l' Ennemi est clairement désigné : l' homme doit disparaitre du
lieu , une cohésion doit se créer entre les bêtes et se renforcer autour
très rapidement , les cochons forment une élite et sont amenés à prendre
le pouvoir , asservissant les autres animaux .
ils utilisent leur
intelligence supérieure pour manipuler les craintes et modifier le
passé à leur avantage .
les idéaux sont très vite dénaturés , les
principes généreux insensiblement dévoyés .
un dictateur émerge , chasse
son principal rival , puis exécute les " traitres " pour asseoir son
pouvoir de plus en plus hégémonique .
il instaure un culte de la
personnalité , maintient ses congénères en état de soumission et les
ce maître , devenu tout puissant avec l' aide des chiens et des autres
cochons , continue à leur faire miroiter le même espoir , mais leur fixe
un objectif inaccessible tout en leur promettant sans cesse une vie
meilleure afin de les maintenir dans cette utopie .
les années passent
et l' ouvrage s' achève sur un constat amer pour les autres animaux
asservis plus rien ne semble distinguer les cochons de leurs anciens
un jour , les animaux de la ferme du Manoir , profitant de la négligence
du propriétaire , sont convoqués dans la grange par Sage l' Ancien , le
plus vieux cochon de la ferme .
parmi eux , Malabar , Douce et Lubie , des
chevaux de traits , Benjamin , un âne cynique peu disposé à l' ouverture
d' esprit , Edmée , une chèvre blanche , Filou , Fleur et Constance trois
chiens , une chatte , des cochons , des moutons , des vaches , des poules ,
des canards , des oies et des pigeons .
avant de leur faire part de son
rêve de la veille , Sage l' Ancien souhaite leur transmettre la sagesse
qu' il a acquise avec les années .
selon lui , aucun animal d' Angleterre
n' est heureux .
et tout cela , par la faute des humains qui accaparent la
totalité du fruit du travail animal : le lait et les oeufs sont captés
au seul bénéfice des hommes .
l' homme est pourtant l' espèce la plus
faible de la nature terrestre , mais il a réussi à profiter des forces
des autres sans rien fournir en échange .
sage l' Ancien décrit alors
leur sort : les animaux mènent une vie laborieuse , courte et tragique ,
sans qu' il s' agisse d' une loi de la nature .
ils peuvent mettre fin à
cette injustice : sage leur décrit un monde débarrassé d' humains et leur
laisse entrevoir les nombreux avantages dont les animaux pourraient
alors profiter par exemple , travailler dignement et non plus comme
des esclaves , avoir des loisirs , vivre plus longtemps , profiter d' une
retraite , pouvoir se nourrir à sa faim et nombre d' autres avantages .
dès lors , le vieux Sage exhorte tous les animaux à se soulever contre
le fermier , Monsieur Jones , l' unique source de tous leurs problèmes .
il se produit alors un incident dans l' assemblée : des rats sortent de
leur trou pour écouter Sage l' Ancien .
les chiens et le chat se jettent
sur eux et Sage l' Ancien fait alors voter l' assemblée pour décider si ,
oui ou non , les rats , ainsi que les autres animaux sauvages , sont des
ennemis .
les rats sont acceptés comme " camarades " à une écrasante
majorité .
en fait , seuls les chiens et la chatte ont voté contre .
sage l' Ancien raconte enfin son rêve , tout en entonnant un chant
révolutionnaire qui s' en inspire .
celui ci est intitulé Bêtes
d' Angleterre ( établi sur l' air de La Cucaracha ) .
les
animaux reprennent le chant dans l' enthousiasme .
le brouhaha provoque
le réveil du propriétaire de la ferme , Monsieur Jones : croyant à la présence
d' un renard en train de rôder , il se met à tirer à l' aveuglette .
version du drapeau de l' Animalisme , comme l' a indiqué George Orwell .
selon le livre , le vert représente les champs de l' Angleterre , tandis
que le sabot et la corne représentent la République des Animaux
trois jours plus tard , Sage l' Ancien meurt paisiblement dans son
sommeil .
aussitôt , une intense activité conspiratrice se développe
parmi les animaux .
seul Moïse , le corbeau apprivoisé , reste fidèle à Monsieur
par chance , la révolution a lieu plus tôt et plus facilement qu' espéré :
un soir , Mister Jones rentre saoul du bar de la ville et va se coucher
directement sans nourrir les bêtes .
le manque de nourriture provoque la
colère des animaux : dans un moment de fureur , ils attaquent Jones et
ses ouvriers agricoles , et parviennent à chasser tous les humains de la
ferme .
le corbeau Moïse suit M me Jones dans sa fuite .
arraché aux mains de ses propriétaires , le domaine est renommé la
regard de leur intelligence supérieure : les cochons Napoléon et Boule
de neige , tous deux secondés par Brille Babil , un goret excellent dans
l' art du discours et que les animaux écoutent volontiers .
tous trois
de la ferme , l' " animalisme " , néologisme à usage historique .
peu
après , ils réunissent les animaux dans la grange et inscrivent sur le
mur les sept grands commandements de ce nouveau système :
le drapeau de l' animalisme représentant un sabot et une corne , symbole
de la lutte des animaux , est fièrement hissé sur un mât au milieu de
l' ancien domaine des Jones .
cependant , les vaches donnent à ce moment
du lait et personne ne sait quoi en faire : lorsque les animaux
reviennent du travail aux dépendances , le lait a disparu .
les animaux effectuent la fenaison sous la direction des cochons , et
leur récolte s' avère plus productive que celle des humains .
ils sont
heureux , mangent à leur faim et parviennent à résoudre les problèmes
qui se posent à eux , même si quelques animaux semblent être moins
disposés à s' investir que d' autres : lubie , la jument blanche , qui était
plutôt contente de sa vie d' avant , et la chatte , sans oublier les
cochons Boule de Neige et Napoléon qui perdent leur temps en longues
discussions et sont souvent en situation de désaccord .
benjamin , lui ,
reste le même , et bien qu' on lui demande si la vie n' est pas mieux
depuis l' expulsion de Jones , Benjamin prétexte que leur vie restera
toujours la même et reste neutre quant au conflit entre Boule de Neige
des commissions créées par Boule de Neige permettent d' éduquer les
animaux , bien que cela se révèle difficile .
les cochons ont en effet
appris à écrire à l' aide d' un vieil abécédaire appartenant aux
enfants des Jones mais pour les autres animaux , ils ne retiennent que
quelques mots ou seulement quelques lettres , selon leurs capacités .
cependant , les sept commandements sont résumés par une maxime dans un
souci de simplification : " quatre pattes , oui ! deux pattes , non ! " .
les oiseaux sont cependant en colère , car ils sont eux même des
bipèdes , mais Boule de Neige leur explique que , dans leur cas , les
napoléon décide de s' occuper de la formation des jeunes , mais en fait ,
il ne prend en charge que les neufs chiots de la dernière portée du
couple de chiens de la ferme .
certaines portions de nourriture ,
notamment des pommes , sont détournées au profit des cochons qui
commencent à bénéficier d' un traitement différent de celui des autres
animaux , jouant sur leur régime alimentaire ( au contraire des autres
animaux , les suidés ont un régime omnivore ) .
on découvre que le lait
disparu a été bu par les cochons .
brille babil explique que cela est
nécessaire pour garder leur aptitudes intellectuelles , et ne pas
faillir à leurs devoirs sous peine , selon lui , du retour de Jones .
l' hymne révolutionnaire de la ferme des animaux finit par se répandre
dans les fermes des environs par le biais des oiseaux .
les humains
s' inquiètent de cette épidémie contestataire .
pour éviter que cela se
reproduise chez eux , ils mentent à leur animaux en prétendant que ceux
de la ferme se gèrent mal et ont même recours au cannibalisme .
cependant , les animaux de la ferme ne tardent pas à montrer qu' ils se
le fermier Jones , accompagné de ses voisins Pilkington et Frederick ,
tous armés , décident donc de récupérer coute que coute la ferme dirigée
par les animaux .
mais ces derniers commandés par Boule de neige et avec
l' aide du cheval Malabar se sont préparés à cette attaque et
réussissent à chasser les agresseurs .
cependant , un mouton est tué et
le cochon Boule de Neige est blessé .
un homme qui assistait le fermier
Jones est également blessé , mais il parvient à s' échapper de la ferme .
boule de Neige et Malabar sont décorés pour leur bravoure et le mouton
les animaux , victorieux , décident d' appeler cette bataille mémorable du
moulin à vent et cheval , tableau du peintre anglais William
la jument Lubie disparait un jour d' hiver .
peu avant , elle avait été
aperçue en compagnie d' un humain : lubie a donc préféré abandonner la
ferme et renouer avec la communauté humaine .
un peu plus tard , son amie
l' autre jument , Douce , trouve du sucre et des rubans cachés sous la
paille de son box .
durant la même période , le nouvel hiver de liberté
est long et rigoureux et les animaux restés dans la ferme souffrent du
boule de neige a l' idée de créer un moulin à vent sur la colline
pour générer de l' électricité et alléger le travail des animaux .
napoléon , catégoriquement opposé à ce projet qu' il juge inutile , tente
de rallier les animaux à sa cause face à son adversaire Boule de Neige ,
en scandant le slogan : " votez pour Napoléon et la mangeoire sera
pleine !
" mais le charisme de Boule de neige a raison du caractère
rude de son adversaire .
jaloux , Napoléon décide alors de lancer aux
trousses de Boule de neige les chiens qu' il a élevés en cachette , et
qui sont devenus de féroces molosses .
boule de Neige parvient à
s' enfuir de la ferme et à semer la meute .
tous les animaux comprennent
alors qu' il s' agit des neufs chiots de Napoléon , devenus grands et
qu' il a dressés à son service .
se retrouvant sans adversaire , Napoléon
annonce que , dorénavant , les débats publics sont abolis et qu' il
devient le chef .
les animaux , même Malabar , sont désemparés .
quatre
jeunes gorets commencent à protester mais se taisent quand les chiens
se mettent à grogner .
enfin les moutons ruinent toute discussion en
bêlant " Quatrespattes oui ! Deuxpattes , non ! " .
brille babil finit par
convaincre les animaux avec un argument de poids : tout vaut mieux que
le retour du fermier Jones .
boule de Neige , présenté comme criminel par
brille babil , est encore perçu par des animaux comme un brave de la
ainsi sa confiance inaliénable dans le nouveau chef .
les réunions du dimanche sont désormais ritualisées : les animaux
doivent saluer le drapeau , puis défiler devant le crâne exposé de Sage
l' Ancien .
ils ne s' assoient plus et Napoléon se place sur une estrade ,
face à eux .
les animaux sont surpris quand Napoléon annonce que le
moulin contre lequel il s' est montré si hostile sera finalement
construit .
à l' aide de trois molosses , Brille Babil persuade les
animaux que l' idée et les plans du moulin venaient en fait de Napoléon
et qu' ils avaient été volés par Boule de Neige sans en apporter la
preuve .
il en profite pour encenser Napoléon qui a fait preuve de
tactique ( comme son homonyme humain ) en paraissant hostile au moulin
pour se débarrasser de Boule de Neige .
la construction du moulin permet
de justifier l' augmentation du travail et la baisse des rations
alimentaires pour les deux ans à venir sauf celles des cochons .
l' annulation de toutes les autres réunions et autres débats fait
comprendre que , désormais , toute question sera débattue uniquement par
un comité de cochons , la construction du moulin était la première
une très forte tempête s' abat sur la ferme .
les animaux découvrent que
leur moulin , construit sous la " direction éclairée " de Napoléon , a
bien qu' absent , Boule de neige est cependant tenu responsable des
malheurs des animaux en tant que " bouc émissaire " .
le porc
brille babil affirme , sans le prouver , être en possession de documents
secrets , selon lesquels Boule de Neige serait l' agent du fermier Jones
depuis le début , voire bien avant la révolte mais il se heurte au
scepticisme affirmé de Malabar , qui se range cependant sous l' aile de
napoléon , le soutenant .
les autres animaux acceptent les explications
sans rien demander .
une récompense est même promise à qui ramènera
pendant ce temps , la vie des autres animaux ne s' améliore pas , tandis
que les cochons et les chiens jouissent de nombreux privilèges : ils
reçoivent de plus grosses rations de nourriture , ils peuvent se lever
plus tard et ne participent pas aux corvées et aux tâches les plus
dures , les missions de ces deux espèces se limitant à la supervision de
la ferme pour les cochons et à la surveillance des autres animaux pour
le quatrième commandement a été modifié : les animaux ne doivent pas dormir
dans des lits avec des draps .
ainsi les cochons peuvent dormir dans les
lits de l' ancienne maison des Jones .
les animaux , ne se souvenant plus
de la nuance , émettent des doutes , balayés comme d' habitude par la
persuasion de Brille Babil appuyée par la menace de trois molosses .
l' hiver suivant est aussi éprouvant que l' hiver précédent , et les
animaux peinent à reconstruire le moulin .
le rationnement imposé
augmente leurs difficultés , mais ils s' efforcent de faire bonne figure
devant les humains qui , de loin , continuent à les observer .
napoléon décide tout de même de livrer des oeufs aux humains en échange
de provisions .
si dans un premier temps s' y opposent les poules qui
croyaient être propriétaires de leurs oeufs , elles sont obligées
d' obéir : napoléon supprime leurs rations et punit de mort tout animal
leur donnant le moindre grain .
cet épisode fait neuf mortes chez les
poules , et une autre cause de leur décès est invoquée .
de toute façon , pour Napoléon , le fuyard Boule de Neige devient le
responsable de tous les maux de la ferme .
celui ci , pourtant à
l' origine de la révolte , est même accusé d' avoir été un espion du
fermier Jones .
au début , les animaux ont du mal à y croire mais ils se
laissent persuader par Brille Babil qui a enfin trouvé comment
dénaturer la bravoure de Boule de Neige lors de la Bataille de
l' étable .
enfin , afin d' éliminer toute contestation , Napoléon déclare
que des traitres se cachent parmi eux : il les dénonce , prépare un
simulacre de procès et fait exécuter par les molosses les cochons qui
avaient élevé la voix à sa prise de pouvoir , les poulets de la révolte
et trois moutons .
malabar est le premier attaqué par les molosses mais
il en fait fuir deux et blesse grièvement le troisième , qu' il épargne à
après cet épisode , les animaux sont déçus mais soumis .
pourtant la
jument Douce , nostalgique , tente de chanter leur hymne révolutionnaire
plus d' actualité , la révolution étant terminée le chant est
napoléon , qui joue de plus en plus de son image de stratège et d' animal
vénérable et vénéré , du moins par ses amis porcs et sa garde canine ,
entre en relation commerciale avec les fermes voisines pourtant tenues
par des humains , en établissant des contacts basés sur les échanges
le moulin est enfin terminé .
napoléon réunit tous les animaux pour
annoncer qu' il a vendu du bois au fermier Frederick , mais se rend vite
compte que ce dernier l' a floué avec de faux billets , à la place du
chèque proposé mais refusé par peur qu' il soit faux .
napoléon prononce
alors des menaces de mort à l' encontre de Frederick .
dès le lendemain ,
la ferme est attaquée par les hommes du fermier escroc , désireux de
s' emparer du domaine , et le moulin est à nouveau détruit .
la bataille
est rude , de nombreux animaux sont tués ou blessés mais les cochons ,
napoléon en tête , fêtent la victoire et décident de boire de l' alcool
pourtant interdit par les sept commandements .
ceux ci sont dès lors
modifiés en ce sens : la consommation d' alcool n' est plus interdite ,
elle doit juste être effectuée sans excès .
de l' orge est d' ailleurs
cultivée pour la consommation d' alcool des cochons .
la modification des commandements est révélée par un évènement que les
animaux ne comprennent pas , hormis Benjamin qui garde le silence : après
un énorme fracas à minuit au clair de lune , Brille Babil est retrouvé
inconscient devant les commandements , avec à ses côtés une échelle
brisée en deux et un pot de peinture blanche renversée .
cheval mort sur le tombereau d' un équarrisseur .
tableau du XIX E siècle
le cheval Malabar , blessé lors de la dernière bataille contre les
humains , souhaite pourtant participer à la reconstruction du moulin .
l' hiver est de nouveau très rigoureux et seuls les cochons et les
chiens mangent correctement : l' écart se creuse de plus en plus entre
autoproclamé président , Napoléon supervise le travail des animaux et la
reconstruction du moulin .
malabar n' en peut plus : éreinté par les
efforts fournis , il tombe malade d' épuisement .
napoléon prend les
dispositions pour le faire soigner à l' hôpital voisin .
malabar est
emmené par un fourgon , mais l' âne Benjamin s' aperçoit qu' on emporte le
cheval Malabar chez l' équarrisseur .
c' est la seule fois où
benjamin prend position : il ameute tous les animaux pour sauver Malabar
en tentant d' arrêter le fourgon trop tard , Malabar n' a plus la force
de s' échapper et les chevaux du fourgon ne réagissent pas à l' appel
cependant , trois jours plus tard , Brille Babil , porte parole de
napoléon , annonce aux animaux que Malabar est bel et bien mort à
l' hôpital , il décrit ses derniers instants avec force détails , tout en
dénonçant la rumeur de l' abattoir comme fausse nouvelle .
les
autres animaux croient en cette déclaration " officielle " .
le
soir même , les cochons organisent un banquet avec une pleine caisse de
whisky , achetée avec l' argent gagné grâce à la vente du cheval Malabar
les années passent , si la ferme est devenue riche et prospère grâce aux
des porcs bien gras en profitent réellement : les autres survivent et
continuent une vie de labeur , sans hommes , certes , mais toujours aussi
dure et éprouvante .
beaucoup d' animaux sont des bêtes achetées et plus
personne ne se souvient des évènements succédant à la révolte .
en
outre , le corbeau Moïse revient après des années d' absence .
napoléon et ses amis évoluent physiquement .
un beau jour , ils finissent
par se déplacer juchés sur leurs deux pattes arrières à l' instar des
humains .
les moutons de la ferme , manipulés par Brille Babil qui les
avait isolés pendant une semaine , reprennent un des commandements ,
transformés au seul profit des porcs " Quatrepattes bon !
, Deuxpattes
mieux !

napoléon et ses cochons révisent secrètement quelques
commandements pour se débarrasser des accusations de violation de la
loi , sans se soucier de la réaction des animaux , qui avec leur piètre
mémoire , finissent par être convaincus par les cochons que les choses
ont toujours été ainsi .
les commandements modifiés sont les suivants
quatre .
aucun animal ne doit dormir dans un lit ...
avec des draps ,
six .
aucun animal ne tuera un autre animal ...
sans raison valable .
puis , les porcs finissent par rendre au domaine agricole son nom
d' origine de " ferme du Manoir " .
un soir , les maîtres porcins de la
ferme invitent les fermiers humains des alentours afin de se
réconcilier avec eux en leur promettant à l' avenir des relations
amicales et coopératives .
les humains félicitent les cochons pour leur
réussite : les bêtes de la " ferme du Manoir " produisent plus que les
leurs , sans rechigner , en dépit de rations alimentaires réduites .
douce
demande alors à Benjamin , plus taciturne que jamais et ayant comme
opinion que les déboires et autres sont les lois inviolables et
inaltérables de la vie , de lui lire les commandements inscrits sur le
mur , mais il lui répond qu' il n' en reste plus qu' un seul :
finalement , alors que l' auguste assemblée humaine et porcine réunie
dans l' ancienne maison des Jones se jurait coopération et amitié , la
minute suivante une querelle éclate entre les hommes et les cochons de
napoléon devenus de plus en plus gras au motif d' une tricherie aux
cartes .
observant cette dispute à travers la fenêtre de la maison , les
autres animaux de la ferme s' aperçoivent qu' ils sont eux mêmes devenus
l' homme au cochon , et de nouveau du cochon à l' homme , mais déjà il
anglais ) Un verrat âgé ( de race Middle White ( en ) )
qui suscite la rébellion .
il est une combinaison de Karl Marx
et de Lénine , en ce sens que c' est ce personnage qui imagine
les principes de la révolution et que sa dépouille sera offerte à
en anglais ) Un gros verrat , à l' air féroce , de race
napoléon est le " méchant " du livre , il ne considère pas les
autres animaux comme ses égaux .
le choix du nom Napoléon est une
référence probable à l' empereur qui confisqua la Révolution
Française , et montre que la portée de l' ouvrage ne se
limite pas à la révolution russe , mais peut s' étendre à
de la ferme après la " destitution " du fermier Jones , il est le
seul cochon qui considère les autres animaux véritablement comme
ses égaux , car il ne cesse d' inventer des projets pour
l' amélioration de la ferme .
il sera contraint de s' enfuir afin
d' éviter d' être tué par les chiens lancés à ses trousses par
napoléon .
il sert ensuite de bouc émissaire , Napoléon accusant
son adversaire exilé d' être à l' origine de diverses complications
rencontrées par la ferme , notamment la chute du moulin pourtant due
ministre de la propagande , il est très convaincant et malin , ce qui
généralement des propagandistes du régime communiste .
Comrade Napoléon ( Camarade Napoléon ) , qui remplace Bêtes
d' Angleterre , en référence au remplacement de L' Internationale
laquelle on enseigne la théorie de l' inégalité entre les animaux .
de Napoléon sur la ferme .
ils seront exécutés .
référence aux
exécutions pendant la période des Grandes Purges de
Grigori Zinoviev , Lev Kamenev , Nikolaï Boukharine et
respectable , mais naïf et crédule .
il prend en charge une bonne
partie du labeur nécessaire à l' entretien de la ferme .
sa force
physique engendre la crainte parmi les cochons : elle pourrait se
retourner contre eux .
il évoque les stakhanovistes
car il croit que tous les problèmes peuvent être résolus en
travaillant plus .
cependant , quand il est blessé ,
napoléon le vend à un équarrisseur , ce qui lui permet de se
procurer du whisky , alors que Malabar l' a toujours soutenu .
promptement la ferme après la révolution , allégorie des émigrants
qui quittèrent la Russie dès les premiers temps des évènements de
lucide .
il est l' un des rares animaux ayant pu apprendre à lire ,
sans avoir besoin de prouver son don et le plus vieil animal de la
ferme .
benjamin est le meilleur ami de Malabar .
il finit par
comprendre ce qui se passe réellement à la ferme , mais il ne fait
pas part de ses inquiétudes aux autres animaux .
il veut , peut être ,
ne pas leur donner de faux espoirs .
c' est le seul animal à avoir un
certain sens de la réalité .
Morris Dickstein deux noeuds
suggère : " There is perhaps a touch of Orwell himself in this
creature' S timeless skepticism ( il y a probablement une touche
d' Orwell lui même dans le scepticisme intemporel de cette créature )
George " , " after his grumbling donkey Benjamin , in Animal Farm
essentially selfish , representing a view of human nature that is
apolitical and thus can hardly be the voice of Orwell within the
book , as some readers hold .
( benjamin est essentiellement égoïste ,
représentant une vision de la nature humaine qui est apolitique et
qui peut donc difficilement être la voix d' Orwell dans le livre ,
comme certains lecteurs le prétendent .

cependant , il
par la ferme à la fin du roman et bien qu' ils soient de bons
compagnons , ils sont bornés et ne savent pas lire .
invétéré .
les animaux se révoltent après que , complètement ivre , il
les néglige et oublie de s' occuper d' eux et plus particulièrement
de leur donner à manger .
c' est une allégorie du tsar Nicolas
février mille neuf cent dix sept. il mourra ivre dans une pension au cours du roman .
Pinchfield , une ferme voisine , petite mais bien entretenue , qui
conclut une brève alliance avec Napoléon avant de le trahir , en
l' allégorie , et à l' Allemagne Nazie qui conclut le Pacte
germano soviétique dans le but de se préparer à envahir
propriétaire de Foxwood , une grande ferme du voisinage .
il est
l' antithèse de Frederick , plus riche car il possède plus de terres ,
mais son domaine nécessite plus d' entretien que celui de Frederick ,
administré plus efficacement .
en mauvais termes avec Frederick .
il
craint cependant comme lui la contagion de la révolution des
liaison entre la Ferme des animaux et les autres propriétés .
d' abord chargé d' approvisionner la ferme en produits de première
nécessité , il est amené à y introduire des produits de luxe et de
dans le roman , le corbeau Moïse évoque l' homme de religion
en exil , puis réapparait plusieurs années après , pour reprendre son
rôle habituel : discourir et ne pas travailler .
il charme les
habitants de la ferme avec des récits évoquant un endroit
merveilleux au delà des nuages , appelé Montagne de Sucrecandi , où
les animaux pourront se reposer éternellement , bien loin de leur
labeur terrestre .
Orwell évoque la religion comme étant " the black
raven of priestcraft , embodied in figures doing no useful work ,
promising pie in the sky when you die , and faithfully serving
whoever happens to be in power .
( le corbeau noir du clergé , incarné
par des personnages ne faisant aucun travail utile , promettant la
récompense du ciel après la mort , et servant fidèlement celui qui
la ferme , Fleur et Constance ( Jessie et Bluebell ) , récupérés par
napoléon et destinés à lui servir de garde rapprochée .
ce
sont eux qui chassent Boule de Neige de la ferme .
ils représentent
aussi le NKVD , la police soviétique , chargée par Staline
ferme .
comme l' âne Benjamin et le cochon Boule de Neige , elle sait
lire , mais elle reste un personnage secondaire .
elle
des évènements , ils soutiennent aveuglément Napoléon .
ils
représentent le public , le " peuple " ou ce qu' on peut dénommer la
majorité silencieuse , terme déjà connu à l' époque de la
révolution , qu' elles pourraient désormais garder leurs oeufs , cette
promesse n' est pas tenue , elles sont parmi les premiers à se
rebeller contre Napoléon .
leur révolte échouera
avoir reçu la promesse qu' elles pourraient garder leur lait pour
nourrir les veaux .
mais le lait sera volé par les
cochons .
contrairement aux poules , elles ne protestent
chiens de base et parents des molosses , ils meurent de vieillesse
au cours du roman .
on ignore s' ils rejoignent leurs chiots dans la
autres animaux , ils ont , eux aussi , un rôle mineur .
ferme , probablement des pigeons domestiqués encore un rôle
britannique .
c' est aussi un homme engagé , gagné à la cause du
prolétariat , qui deviendra un combattant engagé au côté des
républicains .
mal reçu par les communistes , il rejoindra les
durant la période où il rédige la Ferme des animaux , publié en mille neuf cent quarante cinq ,
George Orwell envoie entre mille neuf cent quarante et un et mille neuf cent quarante six seize articles ( " les Lettres
de Londres " ) à la revue américaine d' inspiration trotskiste
partisan Review .
l' homme est donc extrêmement méfiant
vis à vis des thèses staliniennes et du Mouvement communiste
international qui est inféodée à l' URSS , à cette époque .
il essuie quatre refus d' éditeurs qui évoquent le caractère injurieux
du texte , assimilant les dirigeants communistes à des cochons .
l' éditeur Fredric Warburg , en dépit de la pression exercée par son
entourage et des agents du ministère de l' Information , le publie en
la révolution russe .
mais , dans mon esprit , il y avait une
application plus large dans la mesure où je voulais montrer que
cette sorte de révolution ( une révolution violente menée comme une
conspiration par des gens qui n' ont pas conscience d' être affamés de
pouvoir ) ne peut conduire qu' à un changement de maîtres .
la morale ,
selon moi , est que les révolutions n' engendrent une amélioration
radicale que si les masses sont vigilantes et savent comment virer
leurs chefs dès que ceux ci ont fait leur boulot .
le tournant du
récit , c' est le moment où les cochons gardent pour eux le lait et
les pommes ( Kronstadt ) .
si les autres animaux avaient eu alors
la bonne idée d' y mettre le holà , tout se serait bien passé .
si les
gens croient que je défends le statu quo , c' est , je pense , parce
qu' ils sont devenus pessimistes et qu' ils admettent à l' avance que
la seule alternative est entre la dictature et le capitalisme
laisser faire .
dans le cas des trotskistes s' ajoute une complication
particulière : ils se sentent coupables de ce qui s' est passé en
urss depuis dix neuf cent vingt six environ , et ils doivent faire l' hypothèse qu' une
dégénérescence soudaine a eu lieu à partir de cette date .
je pense
au contraire que le processus tout entier pouvait être prédit et
il a été prédit par un petit nombre de gens , Bertrand Russel
par exemple à partir de la nature même du parti bolchevique .
j' ai
simplement essayé de dire : " vous ne pouvez pas avoir une révolution
si vous ne la faites pas pour votre propre compte , une dictature
le roman est une référence directe à la Révolution russe ( photo de mars
le texte du roman établit du début jusqu'à la fin , de par la volonté
même de son auteur , un parallèle entre la révolution des animaux et la
révolution russe et l' évolution de l' Union soviétique avec la
montée en puissance de Staline , devenu chef suprême de la nation après
le roman décrit des faits qui peuvent très facilement se comparer à
l' histoire de l' Union soviétique depuis mille neuf cent dix sept , jusqu'à la date
famille et donne le pouvoir aux bêtes rappelle la révolution russe
de mille neuf cent dix sept qui chassa le tsar Nicolas le deuxième et sa famille .
rappelle la faucille et le marteau choisi par les révolutionnaires
neige rappelle celle entre Staline et Trotski après la mort de
avouent des faits probablement inventés correspondent aux grands
parallèle avec les rations importantes dont les cochons bénéficient
correspond au rationnement et aux famines soviétiques en
George Henry Soule Jr. , journaliste à The New Republic , un
magazine américain d' opinion émet , peu de temps après la
sortie du livre un une critique assez acerbe à l' encontre du roman
et considère que le livre manque de clarté .
pour ce journaliste
américain , même si les allusions à l' histoire de l' Union soviétique
sont évidentes , George Orwell ne ferait que présenter des idées
stéréotypées sur un pays qu' il ne connait probablement pas
la maison " Secker et Warburg " est la première maison d' édition à
Fairy Story , London , Penguin , coll .
" fiction " , mille neuf cent quatre vingt neuf ( un re édition
Gallimard , coll .
" Folioplus classiques vingt E siècle " , deux mille sept
Mangin ) , Belin Gallimard , coll .
" Classicolycée " , deux mille treize
virginie Manouguian ) , Belin Gallimard , coll .
" Classicocollège " ,
l' ouvrage reçoit également le prix Hugo du meilleur roman court ,
article connexe : prix Hugo du meilleur roman court .
son épouse Joy Batchelor , qui a bénéficié de financements de
de l' opération Mockingbird , diffusé en dix neuf cent cinquante quatre , reste assez
fidèle au roman , à l' exception de la disparition de Boule de neige ,
et la fin qui se présente plus dans le sens d' une " happy end " ,
avec le renversement du régime des cochons par opposition aux
animaux épuisés et désespérés de voir les cochons se conduire comme
des humains .
ce changement de fin était une volonté de la CIA , car
elle ne désirait pas qu' on assimile le communisme avec le
capitalisme , ce que le livre semblait dénoncer .
le film a
bénéficié d' une rediffusion en salle et de la sortie d' un DVD en
Netflix en vingt cent dix huit .
ce film , prévu pour être adapté par le
Serkis , devrait se dérouler durant l' époque actuelle .
est une adaptation également fidèle au roman mais qui présente une
fin différente , dans le même genre que celle du film de dix neuf cent cinquante quatre .
le
film mêle prise de vues réelles et images de synthèse et
animatroniques , en utilisant de véritables animaux .
la Grange de Dorigny est le théâtre du campus de l' Université de
par le metteur en scène suisse , Christian Denisart .
la pièce a
notamment été jouée à la " grange de Dorigny " , bâtiment qui abrite
le théâtre du campus de l' Université de Lausanne .
une tournée
est prévue au cours de l' année vingt cent dix huit dans les théâtres de nombreuses
communes de suisse romande , telles que Gland , Vevey ,
en créole mauricien Repiblik Zanimo et paru entre novembre
mille neuf cent soixante quatorze et le un er avril mille neuf cent soixante quinzea été la première bande dessinée en
créole des Mascareignes .
elle y est sorti dans un contexte
d' élection de l' île Maurice .
elle est en fait une oeuvre de
propagande , éditée dans le journal Libération du parti de
l' Union démocratique mauricienne ( UDM ) , c' est une critique
envers son opposant , le Mouvement militant mauricien ( trois mille ) de
gauche .
des documents des services secrets britanniques ,
déclassifiés en mille neuf cent quatre vingt seize , expliquent qu' il s' agit en fait d' une oeuvre
de propagande volontairement libre de droits et destiné à être
traduite dans différentes langues à destination des pays où le
communisme était en émergence .
d' après Andrew Defty , dès mille neuf cent cinquante et un ,
la bande dessinée fut publiée par épisodes dans des journaux , en
Érythrée .
d' autres diffusions étaient planifiées .
Novedi , avec un scénario de Jean Giraud et des dessins de
marc Bati , est l' adaptation en bande dessinée du premier film .
du roman , même libre , en est très inspirée et reprend beaucoup
d' éléments du roman ( comme une milice de chiens , et les cochons se
tenant debout ) .
l' intrigue suit ainsi une chatte nommée Miss
Bengalore , qui , avec l' aide d' un lapin nommé César et un vieux rat
du nom d' Azélar , tente de mener une rébéllion contre le président
en deux mille dix sept , une information , publiée par plusieurs sites spécialisés sur
les jeux vidéo , annonce qu' une équipe de développeurs travaille sur
l' adaptation en jeu vidéo du roman .
le gameplay ( ou
jouabilité ) est basé sur la gestion et l' évolution de la ferme en tant
qu' un état indépendant .
les joueurs pourront ainsi choisir entre
absolu et du totalitarisme " .
la distribution commerciale de jeu ,
simplement dénommé " animal Farm " est prévue avant la fin de l' été
cet album puise son inspiration dans le roman d' Orwell .
les
titres des chansons font référence aux cochons et aux moutons .
la couverture de la pochette du trente trois tours représente un
ensemble industriel au milieu de Londres ( la Battersea
Power Station , au dessus duquel flotte un dirigeable en forme de
cochon fabriqué spécialement pour l' occasion par les
industries Zeppelin .
la symbolique veut que du haut du
ciel , le cochon observe les " errances et la décadence de la
chanson , parue dans leur album Let' s Get Free et dénommée
ce groupe de metalcore américain a créé la chanson " The Nature
of The Beast " , en s' inspirant du roman .
celle ci s' inspire
l' espace , épisode dénommé Le Porc de l' angoisse .
américaine , dénommé La Ferme des animaux ( Animal Farm ) , peut être
considéré comme un clin d' oeil en raison du titre original , mais n' a
britannique de science fiction Doctor Who dénommé ,
The Daleks' Master Plan , un personnage du conseil des daleks
déclare une phrase se traduisant par " nous sommes tous égaux , mais
certains sont égaux plus que d' autres .
" , en référence au livre .
Charlton Herston ) , lors de son jugement , prononce " il me
semble que certains simiens sont plus égaux que d' autres " , ce qui
animaux , reprend le thème et le concept du roman avec la rébellion
des animaux magiques qui en ont assez de vivre à la ferme .
se rebelle , s' autorise une analogie au titre du roman de George
Orwell , même si le contenu de l' histoire est fondamentalement
différent .
certains personnages du dessin animé semblent évoquer
ceux du roman , avec des animaux au comportement humain .