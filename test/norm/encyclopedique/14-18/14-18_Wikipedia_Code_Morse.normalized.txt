message transmis en Scott ( Morse lumineux ) , par un timonier , au
le code peut être transporté via un signal radio permanent que
l' on allume et éteint ( onde continue , généralement abrégé en CW , pour
continuous wave en anglais ) , ou une impulsion électrique à travers un
câble télégraphique ( de nos jours remplacé par d' autres moyens de
communication numérique ) , ou encore un signal visuel ( flash lumineux ) .
l' idée qui préside à l' élaboration du code morse est de coder les
caractères fréquents avec peu de signaux , et de coder en revanche
sur des séquences plus longues les caractères qui reviennent plus
rarement .
par exemple , le " E " , lettre très fréquente , est codée
par un simple point , le plus bref de tous les signes .
les vingt cinq autres
lettres sont toutes codées sur quatre signaux au maximum , les chiffres
sur cinq signaux .
les séquences plus longues correspondent à des
symboles les plus rares : signes de ponctuation , symboles et caractères
parallèlement au code morse , des abréviations commerciales plus
en utilisant des mots tels que BYOXO ( Are you trying to crawl out of
it ?
) , liouy ( Why do you not answer my question ?
) et AYYLU ( Not clearly
coded , repeat more clearly .

l' intention de ces codes était
d' optimiser le cout des transmissions sur les câbles .
les
radioamateurs utilisent toujours certains codes appelés Code Q
et Code Z Ils sont utilisés par les opérateurs afin de s' échanger
des informations récurrentes , portant par exemple sur la qualité de la
liaison , les changements de fréquences et les télégrammes .
antenne radiotélégraphique de canot de sauvetage en dix neuf cent quatorze .
articles détaillés : officier radiotélégraphiste de la marine
marchande et Radiotélégraphiste de station côtière .
les premières liaisons radiotélégraphiques sans fil utilisant le
code morse datent du début du XX E siècle .
en dix neuf cent trois , la
conférence de Berlin attribue la longueur d' onde de six cents mètres
officialise en dix neuf cent six le signal SOS comme appel de détresse .
jusqu' en dix neuf cent quatre vingt sept , plusieurs conférences mondiales des
radiocommunications définissent les bandes à utiliser pour les
communications en télégraphie morse .
depuis le un er février
maritimes côtiers et mobiles de France et de nombreux autres
pays ont abandonné la veille radiotélégraphique obligatoire et
cessé les émissions en morse , notamment sur la fréquence de
cinq cents kilo hertz ( maritime et aéronautique ) et sur la fréquence de
huit trois cent soixante quatre Hz , affectées au trafic de détresse ou d' appel en
radiotélégraphie , depuis les années mille neuf cent soixante dix , un système de
satellites de télécommunications ayant pris le relais .
à partir de
ce moment , le trafic maritime radiotélégraphique et
radiotéléphonique utilisant les ondes hertziennes commence à
décliner lentement .
cependant , il existe encore à ce jour un
des fréquences internationales affectées par l' UIT à la diffusion
de l' heure , de la météo marine ou aux communications
maritimes en radiotélégraphie ( parmi d' autres , quatre cent quatre vingt deux kilo hertz
la bande des six cents mètres notamment reste utilisée par une
vingtaine de pays dans le monde , parmi lesquels : l' Arabie
saoudite , l' Argentine , l' Azerbaïdjan , le Cameroun , la
l' Érythrée , les États Unis , l' Indonésie , l' Italie ,
l' Irlande , Oman , la Roumanie , la Fédération de
Russie , les Samoa américaines et les Seychelles .

quelques exceptions près , la plupart des stations maritimes encore en
activité n' émettent plus en morse que leur indicatif d' appel et
certaines fréquences destinées au trafic en CW de la
marine marchande ont encore une affectation , même si elles ne sont
plus utilisées que par quelques pays et très rarement .
depuis le début du XX E siècle et l' invention de la lampe
Aldis , les bateaux peuvent également communiquer en morse lumineux .
alors que la capacité à émettre de tels signaux reste exigée pour
devenir officier de la marine marchande dans de nombreux pays ,
dont la France , cette pratique a tendance à devenir rare
et ne se retrouve plus que dans la marine de guerre et chez
les premières liaisons radiotélégraphiques aéronautiques remontent
au début du XX E siècle et ont cessé avant les années dix neuf cent soixante dix , à une
communiquaient en radiotélégraphie dans la bande aéronautique des
neuf cents mètres ( trois cent trente trois , trente trois kilo hertz ) , en vol au dessus des mers et des
océans dans la bande marine des six cents mètres ( cinq cents kilo hertz ) ,
sur la longueur d' onde de radiogoniométrie de quatre cent cinquante mètres
transcontinental radiotélégraphique au dessus des océans dans la
en vol une antenne pendante longue de cent vingt mètres à quatre cent cinquante mètres avait
pour but d' établir les communications radiotélégraphiques sur ces
longueurs d' onde .
à l' extrémité de l' antenne pendait un plomb
une autre antenne tendue le long de la coque de l' aéronef établissait
sol sur la longueur d' onde de neuf cents mètres ( trois cent trente trois , trente trois kilo hertz ) et dès
les fréquences utilisées autrefois par l' aviation pour les
communications ( notamment celles voisines de trois cents kilo hertz ) sont aujourd'hui
attribuées aux radiobalises de type NDB qui émettent des
signaux radiotélégraphiques automatisés ( indicatif composé de deux à
trois lettres , transmis en morse à intervalles réguliers ) .
l' aviation
utilise également la sous bande VHF pour d' autres types de
radiobalises ( systèmes VOR et ILS ) qui transmettent
des communications radiotéléphoniques , elles s' effectuent de nos
jours sur les bandes VHF pour le trafic local , et HF pour le
dans certaines circonstances , la radiotélégraphie présente des
avantages par rapport à la radiotéléphonie : par exemple , en cas
de fort parasitage , il est plus aisé de reconnaitre les signaux codés
en morse que ceux , beaucoup plus complexes , transmis par la voix .
plus discret que la radiotéléphonie qui demande de prononcer les mots
hautement et clairement .
pour ces raisons , la plupart des armées dans
le monde forment des officiers radio maîtrisant la télégraphie et
il arrive également que les navires de guerre , s' ils sont
suffisamment proches , utilisent le morse lumineux appelé le Scott
pour communiquer à l' aide d' un projecteur , d' un feu de mâture visible
sur tout l' horizon ( FVTH ) ou d' une Lampe Aldis .
c' est par exemple
le cas lorsqu' ils sont contraints d' observer une période de
les radioamateurs utilisent assez fréquemment le code morse pour
les communications de loisir en radiotélégraphie et jouissent à cet
jusque dans les années dix neuf cent quatre vingt dix , pour obtenir la licence de
radio amateur aux États Unis ( de la FCC ) , il fallait être
capable d' envoyer cinq mots encodés en morse par minute .
la licence avec
le plus de droits exigeait vingt mots par minute .
l' épreuve actuelle de
lecture au son à l' examen ( jusque dans les années vingt cent onze en
France , uniquement pour la un re classe de radioamateurisme )
requiert une vitesse minimum de douze mots par minute .
les opérateurs
radio militaires et radioamateurs entrainés peuvent comprendre et
le Règlement des radiocommunications deux noeuds se compose de règles liées au
service de radio amateur .
il est révisé tous les trois ans à la
conférence mondiale des radiocommunications ( CMR ) .
la révision de
l' article vingt cinq du Règlement des radiocommunications à la Conférence de
vingt cent trois , en particulier , a supprimé l' exigence de connaissance du code
morse à l' utilisation des fréquences inférieures à vingt neuf , sept mégahertz cela
affecte la plupart des pays , mais certains ( dont la Russie )
du code morse .
on soupçonne d' ailleurs ces derniers d' effectuer
régulièrement des communications chiffrées utilisant le morse .
d' un signal lumineux .
il est à ce titre un passe temps présent
notamment chez les scouts et éclaireurs .
pour les mêmes
raisons , le code a été adopté par certains sportifs que les
activités amènent à être isolés : alpinistes ou plongeurs par
exemple .
le morse peut entre autres servir à signaler une
un prisonnier de guerre , Jeremiah Denton , lors d' une interview
télévisée de propagande réalisée par ses gardiens nord vietnamiens
en mille neuf cent soixante six .
tout en parlant , il énonça le mot " torture " par une
deux types de code morse ont été utilisés , chacun avec ses
particularités quant à la représentation des symboles de l' anglais
télégraphique à l' origine de la première télécommunication à longue
distance .
le code morse international est le code le plus communément
c' est en mille huit cent trente huit que Friedrich Clemens Gerke crée un alphabet
s' agit d' une modification du code morse originel , plus tard appelé code
morse américain .
auparavant , certains espaces étaient plus longs que le
point à l' intérieur même d' un caractère , ou le trait pouvait être plus
long , comme pour la lettre L Gerke simplifie le code en n' utilisant
plus que deux longueurs standards , le point et le trait .
deux types d' impulsions sont utilisés .
les impulsions courtes ( notées
temps et les longues ( notées " " , trait ) à une impulsion de trois de
temps , les impulsions étant elles mêmes séparées par un de temps
alors que se développent de plus en plus de variantes du code Morse
dans le monde , l' ITU adopte en dix huit cent soixante cinq , comme code morse international ,
l' alphabet morse de Gerke avec quelques modifications .
il sera
rapidement utilisé en Europe .
les compagnies de ( radio ) télégraphie
américaines continueront à utiliser le code originel , qui sera alors
le code morse international est toujours utilisé aujourd'hui ( certaines
parties du spectre radio sont toujours réservées aux seules
transmissions en morse ) .
utilisant un simple signal radio non modulé ,
il demande moins d' équipement pour envoyer et recevoir que d' autres
formes de communications radio .
il peut être utilisé avec un bruit de
fond important , un signal faible et demande très peu de bande
on utilise deux symboles " positifs " , appelés point et trait ( ou
totale d' émission d' un trait ( y compris la coupure élémentaire entre
signaux ) détermine la vitesse à laquelle le message est envoyé , elle
est utilisée en tant que cadence de référence .
un message simple serait
cent O D E M O R
voici la cadence du même message ( " égale " signifie " signal actif " ,
longueur d' un " ti " .
il se note par le passage d' un symbole à
les personnes familières du morse écriraient donc " code morse "
ainsi : .

et le prononceraient
il existe d' autres formes de représentation , la représentation
compressée , par exemple , qui associe au " ti " un point en bas , et au
les opérateurs composent des messages en morse à l' aide d' un dispositif
touche : un signal est envoyé lorsque cette dernière est enfoncée .
l' opérateur doit donc calibrer lui même la durée des points et des
traits , ce qui donne à chaque émission un caractère personnel , mais
demande trois ou quatre mouvements de doigt par signe .
palets , dont l' un génère les traits , et l' autre génère les points ,
l' appui simultané déclenchant l' alternance point trait .
avec un tel
manipulateur , un seul " mouvement " de deux doigts tenant la tige
levier du manipulateur par caractère , en poussant à gauche le dit
levier pour ( entre pouce et index ) produire les points et à droite
pour produire les traits , c' est un circuit logique , en général
incorporé à l' émetteur , qui génère intervalles , traits et points de
durées appropriées , et qui sont de ce fait réglables .
ce type de
manipulateur permet une moindre fatigue , et par là même une
possibilité de manipulation plus rapide , avec pour résultat final
une meilleure transmission du message en morse , mais la maîtrise en
est nettement plus ardue que pour l' usage du manipulateur classique
la vitesse de manipulation s' exprime en mots par minute , et varie d' une
dizaine de mots par minute pour un débutant ou une identification
d' émetteur compréhensible par tous , à soixante mots par minute ou plus pour
un manipulateur expert .
le record est détenu par Ted McElroy qui
atteint le score de soixante quinze , deux mots par minute au championnat mondial de
il existe également des générateurs informatiques automatiques , qui
sont généralement couplés avec des décodeurs automatiques .
voici quelques tables récapitulant l' alphabet morse et quelques signes
lettres Mnémoniques Code international Lettre Mnémoniques Code
B deux noeuds B , DE , NI , TS ...
son ?
écouter
cent deux noeuds C , KE , NN , TR .
son ?
écouter
cinq cents deux noeuds D , NE , TI .
son ?
écouter
F deux noeuds F , UE , IN , ER .
son ?
écouter
G deux noeuds G , ME , TN .
son ?
écouter
H deux noeuds H , SE , II , ES ...

son ?
écouter
J deux noeuds J , WT , AM , EO .
son ?
écouter
K deux noeuds K , NT , TA .
son ?
écouter
cinquante deux noeuds L , RE , AI , ED .

son ?
écouter
mille deux noeuds M , TT Son ?
écouter
Z deux noeuds Z , GE , MI , TD .
son ?
écouter
chiffre Code international Chiffre Code international
zéro zéro Son ?
écouter
un un .
son ?
écouter
deux deux .
son ?
écouter
trois trois ...
son ?
écouter
quatre quatre ...

son ?
écouter
note : le symbole " " a été ajouté en deux mille quatre .
il combine le À et
lettres Mnémoniques Code international
deux noeuds TO
abréviations et signaux divers à employer dans les radiocommunications
abréviation Mnémoniques Code international Signification
sos ...

appel de détresse
K deux noeuds .
contact , invitation à transmettre ...
aa .
début de message .

erreur de la part de l' émetteur du
mauvaise
signalisation , réglez vos feux !
( si transmission lumineuse )
lumière

plus
vous .
" : j' attends une réponse de votre part )
ve ...

tout compris !
as .

attendez , patientez ...
va , SK ...

fin de vacation contact .
une erreur fréquente est de considérer le code de détresse
international comme la succession des lettres " S O S " et de l' envoyer
en tant que tel ( égale égale égale égale égale égale égale égale égale égale égale égale égale égale égale ) .
la bonne façon de
l' envoyer est en enchainant les neuf éléments comme s' ils formaient une
lorsque étendre l' alphabet morse à d' autres lettres ne suffit pas , on
ainsi , le code wabun est utilisé pour transmettre du texte en
japonais .
les symboles représentent des kana syllabiques .
en Chine , un autre système était utilisé , le code
cette méthode a été inventée par un psychologue allemand , Ludwig Koch ,
dans les années dix neuf cent trente .
c' est une des méthodes permettant un
qu' il est invariablement divisé en ces constituants : le ti et le
fréquence légèrement différente pour le ta ( en réduisant cette
la méthode Koch nécessite un ordinateur ( équipé d' un logiciel
spécifique ) ou un professeur pour pouvoir écouter du code .
en
commençant tout de suite avec une vitesse supérieure à douze mots minute ,
elle permet d' apprendre à écouter du code morse correct , et non déformé
par une vitesse faible .
elle permet aussi la reconnaissance des
caractères par réflexe et sans phase de réflexion ( ce qui est de toute
façon impossible à une telle vitesse , et aux vitesses supérieures ) .
dans les méthodes " traditionnelles " , on apprend l' ensemble de
l' alphabet et on pratique à une vitesse faible , par exemple , cinq
mots min .
avec la méthode Koch , on commence par reconnaitre seulement
deux caractères , puis trois , puis quatre ...
mais une vitesse d' au moins
douze mots minute .
cela évite les frustrations du " plateau des
dix mots minute " des méthodes " traditionnelles " .
on utilise traditionnellement cet ordre pour les caractères : K , M , R ,
S , U , À , P , T , L , O , W , I , " . " , N , J , E , F , zéro , Y , " , " , V , G , cinq ,
Donald R " Russ " Farnsworth propose dans sa méthode d' utiliser la
vitesse cible pour l' apprentissage ( commencer tout de suite à
vingt mots minute , par exemple ) mais avec des espaces inter mots et
inter lettres plus élevés que requis par la vitesse cible .
elle donne
ainsi plus de temps à la compréhension de chaque signe , tout en
utilisant une vitesse élevée dès le départ pour la reconnaissance des
on peut d' ailleurs combiner la méthode Farnsworth avec la méthode
koch : en commençant à vingt mots minute , avec deux caractères , avec des
espaces triples par rapport à la normale , par exemple .
il existe différents moyens mnémotechniques assez simples pour
apprendre les vingt six lettres de l' alphabet en morse mais vu qu' ils
induisent des ralentissements dans la compréhension des messages , il
n' est pas recommandé de les utiliser pour apprendre le morse à
dans le tableau ci dessous , un mot est affecté à chaque lettre de
l' alphabet .
ces mots se trouvent dans les trois E et quatre E colonnes du
tableau .
au cas où plusieurs mots possibles sont affectés à une lettre ,
il suffit d' en choisir un .
le procédé mnémotechnique consiste
simplement à apprendre une liste de vingt six mots correspondant aux
chaque mot traduit le codage morse de la lettre qui lui est associée .
pour chaque syllabe du mot on a un ou un .
le sera représenté
pour une syllabe à consonance " O " ou " on " et le pour toutes les
par exemple , pour la lettre P , le mot " psychologie " ( psy cho lo gué )
a ses deux syllabes centrales en " O " ( cho lo ) , les autres n' ont pas de
consonance en " O " ou en " on " .
le code de la lettre P est donc .
avec deux signaux longs pour les deux syllabes centrales et deux courts aux
un autre moyen est d' utiliser les mots de la dernière colonne du
tableau .
pour chaque lettre des mots on a un ti ou un ta .
une consonne
il existe une règle différente pour les lettres composées uniquement de
points ou de traits .
il faut retenir les mots mnémotechniques :
la position de la lettre dans ces mots renvoie au nombre de traits ou
par exemple , le S est codé par trois points car la lettre est en
le code morse est facilement mémorisable à l' aide des codes courts et
longs remplacés par des syllabes .
le code long deux noeuds remplacé par une
syllabe en " O " .
le code court deux noeuds remplacé par une des autres
voyelles .
par exemple , À égale .
égale al point O ( une syllabe en " a " pour le .
pour l' utilisation de la méthode consonne voyelle , toute consonne
remplace un trait deux noeuds alors que toute voyelle signifie un point deux noeuds .
l' idéal étant de trouver un mot correspondant qui comprend la lettre ou
méthode des consonances en " O " ou " on " méthode des
" choco bonbons " , " Chocobons bons " ( kinder ) " T M O "
cinq cents .
docile , " do ré mi " , Donald Duck , document , dominé , Donrémy ,
H ...

hilarité , hystérique , habituer , hurluberlu , hululement ,
J Jablonovo , Jiromoto , " j' ai mon lolo " , " j' ai gros bobo " ,
K .
Kohinor , Korridor , Kolimor , Kominform , Koalo Kim
O ostrogoth , oh bobo , oromo , " Ododo " , " Oporto " " T M O "
Q .
" quoquorico " , " coq au rico " " Qcul " , " phoq deux noeuds " , " fliq "
R ramoneur , rigoler , radoter , revolver , Rivoli , Ricoré ère
S ...
sardine , salade , simili , similaire , samedi , sirène , Sahara ,
U union , ultrason , ultimor , urinons , urinoir deux noeuds uit , ouf
cinq ...
Valparaíso , valentino , végétation deux noeuds oies , oeuv ( re , oeuf ) ,
W wagon post deux noeuds , wagon long , les Wallons ne sont point barbares
dix .
Xochimilco , oxydation , " x' Trocadéro " , xodérido , " monsieur X
porte des lunettes : oo " Xéon , Xiin , noix , deux
Y .
Yoshimoto , Yokimono , Yolimoto , Yolandolo , " yoga yougo " ,
Z .
Zoroastre , " Zorro le grand " , " zoulou méchant " , " Zorro est
il est aussi simple de mémoriser le S et le O grâce au fameux signal
sos : trois brèves , trois longues , trois brèves ( ...

pour les personnes qui ont plutôt une mémoire visuelle , il est
les lettres sont regroupées par deux , celle de gauche représentant un deux noeuds
et celle de droite un deux noeuds .
un symbole deux noeuds est mis quand il n' existe pas
de lettre correspondant au code de l' emplacement .
dans cet arbre , le
lisibilité de l' arbre et ayant peu d' intérêt ) , mais il ne tient qu' au
lecteur de les ajouter pour obtenir un arbre complet .
cela ajouterait
une ligne et remplacerait le symbole deux noeuds correspondant à ( ) .
pour retenir cet arbre , on peut se servir des groupes de lettres et les
retenir dans l' ordre des lignes : et IA NM SU RW ...
avec pour chaque
groupe un moyen .
on peut trouver ses propres moyens à partir de choses
côtoyées tous les jours et abrégées , pour plus de facilité à le
certaines personnes retiennent ces groupes de lettres en apprenant une
phrase .
par exemple : " encore très irritée après nos manigances
sexuelles , Ursuline réimplora Wendy de kidnapper Gérard ou Hervé ,
violeurs fanatiques et libérés , en promettant jurant buter X , ce yankee
zélé quadragénaire .
" ici , chaque première lettre de chaque mot doit
combler les " trous " après les lettres " F " et " L " .
une fois l' arbre mémorisé , il suffit alors de le parcourir et à chaque
intersection de regarder si on passe par la lettre de gauche ( un point )
l' avantage de cet arbre est de fonctionner dans les deux sens de
transcription de morse vers lettre ( partir d' en haut en suivant un
trajet et aboutir à la lettre ) et de lettre vers morse ( trouver la
lettre dans l' arbre et en déduire le trajet , donc le code , en partant
d' autres moyens existent , qui font appel à des phrases ou à des
expressions permettant d' ordonner les signes en fonction de leurs
mot Lettre Code international Mot Lettre Code international
verre V ...
boire B ...
caption : " eish " et " T MOCH ( t' es moche ) "
lettre Code international Lettre Code international
premier mouvement Allegro con brio de la Symphonie N O cinq en ut
mineur de Beethoven .
en voici la représentation notée
elle laisse entendre " ti ti ti ta " ( ...

c' est ce
premier mouvement de la symphonie qui servait usuellement
d' indicatif aux émissions de la BBC adressées aux pays occupés par
l' Allemagne , V signifiant victoire .
en outre , pour cette raison , la
mille neuf cent quarante quatre pour annoncer aux réseaux de Résistance le débarquement
jazz Slim Gaillard est construit selon la répétition du
préfixe général demandant l' attention , " " ( celui ci
précédait le " D " pour composer le signal radio de détresse
définitive du code " sos " à la conférence internationale de
codes dans ses compositions .
ainsi , considérant un manque de
soutien de la part de Virgin pour sa création musicale , il
insère un message codé en Morse à destination de son PDG ,
richard Branson dans son album Amarok paru en mille neuf cent quatre vingt dix .
le
message apparait vers la quarante huit E minute et est le suivant :
en ostinato .
celle ci est scandée par un choeur de femmes
l' oralisant selon la prononciation anglaise des deux symboles : dot
appelée " special " est le mot " S M S " en morse
est la phrase " Connecting people " , le slogan de Nokia .
l' astromobile Curiosity contiennent le code Morse J , P et
Laboratory ) .
le but des chercheurs de la NASA est d' utiliser les
marques laissées par les roues sur la surface de Mars pour en
observer ( par comparaison entre les distances calculées et celles
réellement parcourues ) d' éventuels dysfonctionnements .
rush correspond au rythme obtenu quand on joue YYZ en morse
c' huit fis seize c' huit c' huit c' huit fis seize c' huit c' huit c' huit c' huit fis seize fis seize
de fait , YYZ est le code aéronautique ( OACI ) de l' aéroport de
Toronto , la ville natale du groupe , et il s' agit donc du patron de
et trois notes courtes à nouveau , autrement dit " sos " en morse .
Disneyland Paris grésille .
il s' agit en fait d' un message en
certaines occasions traduire des messages en morse ( notamment dans
les quêtes d' investigation ) .
c' est le cas dans la quête " c' est
mort " , ou encore " pas moyen de reposer en paix " .