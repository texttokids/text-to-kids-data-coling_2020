Lannion est une commune française située dans le département des Côtes d' Armor en région Bretagne .
ses habitants sont appelés les Lannionnais .
la commune actuelle est formée de la fusion des communes de Lannion , Brélévenez , Buhulien , Loguivy lès Lannion et Servel en dix neuf cent soixante et un .
Lannion est une ville pont sur le Léguer .
la proximité de l' embouchure du Léguer fait que le niveau de la rivière au centre ville varie de plusieurs mètres en fonction des marées .
cette caractéristique a permis l' établissement d' un gué dans les temps les plus reculés au niveau du pont de Kermaria , point où s' arrête l' influence des marées ( preuve existante à proximité de la rue Saint Christophe le Passeur ) .
cette particularité sera mise à profit pour la construction en dix neuf cent quatre vingt douze d' un stade d' eau vive marémoteur .
un barrage permet de retenir l' eau amenée par la marée montante et de la relâcher dans un parcours artificiel à marée descendante .
Lannion est localisée à l' extrémité occidentale du domaine nord armoricain , dans le Massif armoricain qui est le résultat de trois chaines de montagne successives .
le site géologique de Lannion se situe plus précisément dans un bassin sédimentaire essentiellement briovérien limité au nord par un important massif granitique cadomien , le batholite nord trégorrois .
ce pluton fait partie d' un ensemble plus vaste , le batholite mancellien .
l' histoire géologique de la région est marquée par la chaîne cadomienne .
à la fin du Précambrien supérieur , les sédiments briovériens environnants ( formations volcano sédimentaires ) sont fortement déformés , plissés et métamorphisés par le cycle cadomien , formant des schistes argileux , les schistes tuffacés de Locquirec , les brèches et tufs carbonifères du Dourduff .
cette chaîne montagneuse , qui devait culminer à environ quatre mètre , donne naissance à des massifs granitiques ( dont le batholite côtier nord trégorrois associé à un volcanisme d' arc insulaire et daté à six cent quinze Ma ) .
dans le domaine continental , l' épaississement consécutif à l' orogenèse cadomienne , provoque la fusion crustale à l' origine de la mise en place des dômes anatectiques ( migmatites de Guingamp et Saint Malo au sud est de Lannion ) qui est datée entre cinq cent soixante et cinq cent quarante Ma .
l' orogenèse hercynienne qui a donné une chaîne montagneuse atteignant six mètre , s' accompagne d' un métamorphisme et d' un magmatisme qui se manifeste par un important plutonisme : le chapelet nord de granites rouges tardifs ( ceinture batholitique de granites individualisée pour la première fois par le géologue Charles Barrois en mille neuf cent neuf ) , formant de Flamanville à Ouessant un alignement de direction cadomienne , contrôlé par les grands accidents directionnels WSW ENE, datés de trois cents Ma , correspond à un magmatisme permien .
le massif granitique de Plouaret au sud de Lannion , lié au fonctionnement du cisaillement nord armoricain , fait partie de ce chapelet .
la commune se situe ainsi dans un pays de basses collines aux sommets aplanis , appartenant à un couloir topographique et tectonique qui va de la baie de Lannion à la baie de Saint Brieuc .
creusée par l' érosion dans des formations schisteuses volcano sédimentaires et métamorphiques , cette dépression est limitée au Nord , à l' Ouest et au Sud par trois massifs granitiques et des bordures escarpées
commandées par des failles .
une promenade géologique dans la commune permet de découvrir l' utilisation de pierres proximales ( privilégiées pour des raisons de cout ) , témoignant de la diversité de la palette lithologique que l' évolution géologique a conféré au territoire lanionnais .
depuis l' époque romane , ces constructions sont un marqueur de la richesse du substrat géologique local et déterminent pour partie le rang social des propriétaires .
ces pierres proximales sont les schistes argileux qui fournissent de médiocres moellons à la teinte brunâtre , des schistes tuffacés qui livrent des moellons et des ardoises très épaisses , sortes de lauzes , mais aussi de grandes dalles , recherchées naguère pour les recouvrements des sols et les plaques tumulaires , et des granitoïdes dont le minéralogiste Beudant disait qu' on les utilise dans les oeuvres dont " on veut éterniser la durée " ( granite du Yaudet , diorites et granodiorites issus du massif de Plouaret , complexe granitique de Ploumanac' h sur la côte de granit rose dont le district de l' Île Grande qui offre des granites de faciès variés , facilement transportables grâce au Léguer ) .
exemple de polylithisme , le porche sud de l' église de la Trinité de Brélévenez .
Polylithisme accusé dans la façade occidentale de l' église Saint Jean du Baly .
couvent des Ursulines en moellons de schistes tuffacés et granite blanc de l' Île Grande .
calvaire dû à Yves Hernot un , en diorite de Lanvellec et kersanton .
manoir de Crec' h Ugien : murs et arcs de décharge en schistes tuffacés , encadrement et chainage d' angle en granites de l' Île Grande .
la ville de Lannion est desservie par une quatre voies la reliant à Guingamp sur la RN douze ( Paris Brest ) .
elle est en outre dotée :
d' un aéroport , Lannion Côte de Granit , qui affichait un trafic annuel moyen de trente passagers par an avant la fermeture de la liaison avec Paris en vingt cent dix huit ,
d' une gare TGV depuis juillet vingt cent zéro, à la suite de l' électrification de la voie entre Plouaret et Lannion .
en basse saison , le TGV assure une liaison le vendredi soir depuis Paris avec un retour vers la capitale le dimanche soir .
en haute saison , une rotation par jour est assurée en plus de la desserte des weekends .
la gare est desservie quotidiennement par des TER Bretagne .
les transports sur la ville de Lannion et sa communauté d' agglomération sont quant à eux assurés par les TILT ( Transports intercommunaux Lannion Trégor ) , qui comportent six lignes :
ligne À : hôpital Aéroport via Quai d' Aiguillon ( principal pôle de correspondance du réseau ) ,
ligne B : Kerbabu Coppens via Quai d' Aiguillon ,
ligne C : Alcatel Kérilis ,
ligne Navéo : navette centre ville ,
ligne F : lignes du marché ( jeudi matin ) .
Lannion est située à :
trente cinq kilomètres de Guingamp ,
trente huit kilomètres de Morlaix ,
soixante six kilomètres de landerneau
soixante neuf kilomètres de Saint Brieuc ,
quatre vingt quatre kilomètres de Brest ,
cent vingt kilomètres de Quimper ,
cent soixante six kilomètres de Rennes ,
deux cent douze kilomètres de Fougères ,
cinq cent quinze kilomètres de Paris .
l' origine du nom Lannion vient de " lann " , qui désigne un établissement religieux créé par les Bretons du haut Moyen Âge et , selon une hypothèse , un anthroponyme Yuzon .
ce nom de Lannyuzon a évolué en Lannuon , forme moderne en Langue bretonne et en " Lannion " , variante évoluée puis administrativement cristallisée de la forme ancienne bretonne , devenue l' appellation officielle et administrative du duché de Bretagne , avant et après quinze cent trente deux , probablement aux alentours des XIIIe XIVe siècles : carte de l' Atelier cartographique Troadec du Conquet Konk Leon pour l' Histoire de la Bretagne de Bertrand d' Argentré de quinze cent quatre vingt deux et mille cinq cent quatre vingt huit : Lanion , Carte de Nolin , seize cent quatre vingt quinze : Lanien ou Lanion , puis de l' Administration française royale et républicaine et de la société francophone nobiliaire et marchande , du XVIIe siècle jusqu'à nos jours , en résumé la forme " Lannyon " et ses variantes sont attestées de onze cent quatre vingt dix neuf à quinze cent seize , où la forme " Lannion " apparait ( Erwann Vallerie , Traité de toponymie historique de la Bretagne , dix neuf cent quatre vingt quinze ) , l' appellation de Lannuon étant usitée majoritairement par le peuple bretonnant .
les mégalithes nombreux dans la région , témoignent de son passé ancien , sans compter les silex , haches et pierres polies appartenant aux âges préhistoriques .
les rivières nombreuses et l' or alluvionnaire dans le Massif armoricain , expliquent la présence et la richesse du développement des populations préhistoriques .
près de Lannion , le tumulus de la Motta comporte des bijoux de métal précieux .
en avril deux mille quatorze, des archéologues de l' Inrap ont effectué des fouilles sur le site de Kervouric , préalablement à la construction de logements .
celui ci surplombe une vallée encaissée du Léguer .
on a ainsi retrouvé la trace de trois grandes maisons de bois et de torchis ainsi que de nombreux vestiges ( céramiques , bracelets , silex ) du Néolithique , vieux de sept mille ans .
des monnaies et des débris de poteries attestent la présence gauloise plusieurs siècles avant le début de l' ère chrétienne .
quelques chemins et des fortifications façonnent leur territoire .
le promontoire du Yaudet , estuaire du Léguer , sert à la fois de défense militaire et d' étape pour les navires commerçants , d' après des pièces de monnaies phéniciennes retrouvées sur les lieux .
le " menhir " du Crec' h à Servel et celui de Saint Patrice , appelé aussi " menhir de justice " ( rue des Frères Lagadec , inclus dans un mur ) , sont en réalité deux stèles gauloises .
la seconde a été christianisée .
Lannion est un passage obligatoire pour franchir le Léguer au plus près de la côte , surtout à marée haute .
du temps des Romains , pour aller du Yaudet par la terre vers l' est , les routes passaient inévitablement par Lannion .
quelques auteurs attribuent l' origine de Lannion à la destruction de Lexobie ( l' actuel Yaudet ?
) par les danois en huit cent trente six .
le Léguer , comme les autres cours d' eau , était une voie de pénétration facile pour les envahisseurs , aussi Lannion est elle dotée d' un château attesté dès le Moyen Âge .
le premier barrage servant de piège à poisson construit à Servel , long de cent quatre vingt dix mètres , formé de pieux reliés par des claies en bois , avec des plates formes triangulaires ancrant la structure dans le sable et servant aussi de brise lames , a été construit entre six cent treize et six cent quinze ( ces dates ont été retrouvées grâce à la datation du bois des chênes ayant servi à sa construction ) dans l' estuaire du Léguer .
à marée descendante , les poissons étaient piégés dans le bassin de retenue .
Lannion est impliqué , pendant la guerre de Cent Ans à la guerre de Succession de Bretagne ( rue Geoffroy de Pontblanc , tué en treize cent quarante six ) .
en quinze cent quatre vingt sept , le vingt deux mars , commencent les massacres perpétrés pendant les guerres de religion en France , avec l' attaque de Perros Ploumanac' h , ayant fait allégeance à la Ligue , par les Royaux de La Rochelle .
les paroisses alentour se partagent entre ligueuses ( Plestin les Grèves ) , ou royalistes comme Lannion .
" le troisième et septième jours de juillet quinze cent quatre vingt dix fut brulée et ravagée la paroisse de Plestin par ceux du parti du roi . et au réciproque le vingt et un du même mois de juillet quinze cent quatre vingt dix fut pareillement brulée et ravagée la paroisse de Plouaret , Ploubezre et la ville de Lannion par ceux qui tenaient le parti du duc de Mercoeur " a écrit le curé de Lanvellec .
malgré la conversion de Henri le quatrième au catholicisme , Ligueurs et Royaux continuent leurs attaques et les destructions des villes du camp opposé .
mais le Trégor est bien affaibli et le roi ramène la paix , confirmée par l' Édit de Nantes de quinze cent quatre vingt dix huit .
Lannion fait partie de l' évêché de Tréguier avant la Révolution et le roi y contrebalance la puissance de l' évêque en faisant de Lannion le siège de sa juridiction .
révolte des Bonnets Rouges en mille six cent soixante quinze .
un de ses habitants fut exclu de l' amnistie royale de février mille six cent soixante seize .
lors de l' ouverture de l' école publique de Lannion le premier janvier dix huit cent quatre vingt deux , il n' y a " ni tableaux noirs , ni encriers , ni livres , ni cahiers " , tout le matériel scolaire se compose de trois petites tables prêtées par le collège .
le port de Lannion contribue fortement à son essor avant la guerre dix neuf cent trente neuf dix neuf cent quarante cinq .
mais au début de la deuxième moitié du XXe siècle , Lannion n' est qu' un " gros bourg " .
à l' aube des années dix neuf cent soixante , elle est choisie pour accueillir le Centre national d' études des télécommunications ( CNET ) .
la petite ville a alors besoin d' importantes réserves foncières pour l' installer , loger les familles des techniciens , d' autant que de nombreuses entreprises vont suivre cette arrivée .
le vingt cinq avril dix neuf cent soixante et un, un arrêté préfectoral scelle la fusion de Lannion avec les communes de Buhulien , Loguivy , Servel et Brélévenez .
au cours des deux premiers mandats suivant cette fusion , chaque ancienne commune continue d' élire ses représentants au conseil municipal du " grand Lannion " .
malgré les crises des années dix neuf cent quatre vingts et dix neuf cent quatre vingt dix , la ville de Lannion a conforté " ses atouts dans les télécoms et la fibre optique " et , avec des sites importants d' Orange ou Nokia , elle est aujourd'hui qualifiée de " mini silicon Valley " .
Lannion est le chef lieu de l' arrondissement de Lannion et fait partie de la cinquième circonscription des Côtes d' Armor .
elle est le chef lieu du canton de Lannion depuis la Révolution .
dans le cadre du redécoupage cantonal de vingt cent quatorze en France , ce canton voit sa composition modifiée , et Lannion en est le bureau centralisateur .
Lannion est à l' origine de la communauté d' Agglomération de Lannion Trégor , créée en vingt cent trois et renommée en vingt cent huit Lannion Trégor Communauté .
la ville a engagé une politique de développement durable en lançant une démarche d' Agenda vingt et un .
cette section est vide , insuffisamment détaillée ou incomplète .
votre aide est la bienvenue !
comment faire ?
Lannion qui participe depuis plusieurs années au concours des villes fleuries , possède quatre fleurs .
en septembre vingt cent seize, un projet d' extraction de sable coquillier dans la baie de Lannion a été stoppé par la Compagnie armoricaine de navigation ( CAN ) , une filiale du groupe Roullier , en raison d' une forte opposition de la population locale .
le onze septembre , une manifestation avait ainsi réuni entre quatre et cinq personnes qui rejetaient le projet .
cette sous section présente la situation des finances communales de Lannion .
pour l' exercice deux mille treize , le compte administratif du budget municipal de Lannion s' établit à trente trois deux cent trente neuf euro en dépenses et trente cinq cent cinquante euro en recettes :
en deux mille treize , la section de fonctionnement se répartit en vingt trois cinq cent vingt neuf euro de charges ( un cent vingt et un euros par habitant ) pour vingt cinq huit cent trente quatre euro de produits ( un deux cent trente et un euros par habitant ) , soit un solde de deux trois cent cinq euro ( cent dix euros par habitant ) , :
le principal pôle de dépenses de fonctionnement est celui des charges de personnels pour une valeur de treize deux cent quatre euro ( cinquante six pour cents ) , soit six cent vingt neuf euros par habitant , ratio inférieur de quatorze pour cents à la valeur moyenne pour les communes de la même strate ( sept cent trente trois euros par habitant ) .
sur les cinq dernières années , ce ratio augmente de façon continue de cinq cent soixante cinq euros à six cent vingt neuf euros par habitant ,
la plus grande part des recettes est constituée des impôts locaux pour une valeur de neuf cent quatre vingt seize euro ( trente six pour cents ) , soit quatre cent trente huit euros par habitant , ratio inférieur de vingt et un pour cents à la valeur moyenne pour les communes de la même strate ( cinq cent cinquante six euros par habitant ) .
sur la période vingt cent neuf vingt cent treize , ce ratio augmente de façon continue de trois cent quatre vingt dix neuf euros à quatre cent trente huit euros par habitant .
les taux des taxes ci dessous sont votés par la municipalité de Lannion .
ils ont varié de la façon suivante par rapport à vingt cent douze :
la taxe d' habitation constante dix huit , cinquante pour cents ,
la taxe foncière sur le bâti sans variation dix huit , soixante sept pour cents ,
celle sur le non bâti constante cinquante neuf , neuf pour cents .
la section investissement se répartit en emplois et ressources .
pour deux mille treize , les emplois comprennent par ordre d' importance :
des dépenses d' équipement pour une valeur de huit cinq cent dix euro ( quatre vingt huit pour cents ) , soit quatre cent six euros par habitant , ratio voisin de la valeur moyenne de la strate .
sur les cinq dernières années , ce ratio fluctue et présente un minimum de deux cent quatre vingt quatorze euros par habitant en deux mille onze et un maximum de quatre cent six euros par habitant en deux mille treize ,
des remboursements d' emprunts pour une somme de un cent soixante dix huit euro ( douze pour cents ) , soit cinquante six euros par habitant , ratio inférieur de quarante deux pour cents à la valeur moyenne pour les communes de la même strate ( quatre vingt seize euros par habitant ) .
les ressources en investissement de Lannion se répartissent principalement en :
nouvelles dettes pour une valeur de deux deux cents euro ( vingt quatre pour cents ) , soit cent cinq euros par habitant , ratio voisin de la valeur moyenne de la strate .
sur les cinq dernières années , ce ratio augmente de façon continue de zéro euro à cent cinq euros par habitant ,
subventions reçues pour une valeur de un quatre vingt seize euro ( douze pour cents ) , soit cinquante deux euros par habitant , ratio inférieur de vingt neuf pour cents à la valeur moyenne pour les communes de la même strate ( soixante treize euros par habitant ) .
l' endettement de Lannion au trente et un décembre deux mille treizepeut s' évaluer à partir de trois critères : l' encours de la dette , l' annuité de la dette et sa capacité de désendettement :
l' encours de la dette pour neuf six cent trente quatre euro , soit quatre cent cinquante neuf euros par habitant , ratio inférieur de cinquante huit pour cents à la valeur moyenne pour les communes de la même strate ( un quatre vingt douze euros par habitant ) .
depuis cinq ans , ce ratio fluctue et présente un minimum de trois cent quatre vingt onze euros par habitant en deux mille dix et un maximum de quatre cent cinquante neuf euros par habitant en deux mille treize ,
l' annuité de la dette pour une valeur totale de un quatre cent cinquante deux euro , soit soixante neuf euros par habitant , ratio inférieur de quarante huit pour cents à la valeur moyenne pour les communes de la même strate ( cent trente trois euros par habitant ) .
pour la période allant de deux mille neuf à deux mille treize , ce ratio fluctue et présente un minimum de soixante euros par habitant en deux mille douze et un maximum de quatre vingts euros par habitant en deux mille neuf ,
la capacité d' autofinancement ( CAF ) pour une valeur totale de trois cinq cent dix huit euro , soit cent soixante huit euros par habitant , ratio voisin de la valeur moyenne de la strate .
sur la période vingt cent neuf vingt cent treize , ce ratio diminue de façon continue de deux cent trente et un euros à cent soixante huit euros par habitant .
la capacité de désendettement est d' environ deux années en vingt cent treize .
sur une période de quatorze années , ce ratio est constant et faible ( inférieur à quatre ans )
l' évolution du nombre d' habitants est connue à travers les recensements de la population effectués dans la commune depuis dix sept cent quatre vingt treize .
à partir de vingt cent six , les populations légales des communes sont publiées annuellement par l' Insee .
le recensement repose désormais sur une collecte d' information annuelle , concernant successivement tous les territoires communaux au cours d' une période de cinq ans .
pour les communes de plus de dix habitants les recensements ont lieu chaque année à la suite d' une enquête par sondage auprès d' un échantillon d' adresses représentant huit pour cents de leurs logements , contrairement aux autres communes qui ont un recensement réel tous les cinq ans , .
en vingt cent dix sept , la commune comptait dix neuf huit cent quatre vingts habitants , en augmentation de deux , cinquante huit pour cents par rapport à vingt cent douze ( Côtes d' Armor : plus zéro , cinquante cinq pour cents , France hors Mayotte : plus deux , trente six pour cents ) .
la charte de la langue bretonne Ya d' ar brezhoneg a été votée par le conseil municipal le vingt trois octobre vingt cent six. le label de niveau un de la charte a été accordé à Lannion le seize novembre vingt cent six.
à la rentrée vingt cent dix huit , cinq cent quarante élèves étaient scolarisés dans des écoles Diwan et dans les classes bilingues .
la ville possède huit écoles maternelles publiques , neuf écoles primaires publiques , deux collèges publics et un lycée public .
Lannion possède également des établissements d' enseignement supérieur , tous associés à l' université de Rennes un : un IFSI IFAS( institut de formation en soins infirmiers et aides soignants ) , un IUT ( Institut Universitaire de Technologie ) et une école d' ingénieurs ( École nationale supérieure des sciences appliquées et de technologie , ENSSAT ) .
à Lannion , il y a deux écoles maternelles privées , deux écoles primaires privées , un collège privé et un lycée privé .
l' école Diwan Lannuon ( maternelle et primaire ) a été créée en mille neuf cent soixante dix huit et est située à Loguivy .
à la rentrée deux mille dix neuf , cent six élèves y sont scolarisés dans cinq classes .
la ville de Lannion a de nombreux équipements sportifs dont un stade d' eau vive pour la pratique du canoë kayak .
le club de canoë kayak est un des meilleurs nationaux en slalom .
il est régulièrement dans les trois meilleurs clubs lors des Championnats de France des clubs de Slalom .
plusieurs athlètes se sont illustrés sur la scène internationale : Sébastien Combot ( médaille d' or aux championnats du monde deux mille sept ) , Philippe Quémerais Yann Le Pennec ( cinquième aux Jeux olympiques deux mille quatre ) .
le trois mars vingt cent huit, la ville de Lannion dispose d' un espace aquatique ludique baptisé " ti Dour " , remplaçant l' ancienne piscine municipale ( qui a définitivement fermé ses portes le quinze février vingt cent huit) à la place de l' ancien stade .
elle se trouve à quelques centaines de mètres de l' ancienne piscine .
elle dispose d' un toboggan , d' un espace sportif et d' un espace ludique .
la ville dispose aussi d' un club d' handibasket , le CTH Lannion , évoluant en première division du championnat de France et participant à la Coupe d' Europe .
l' Équipe première compte dans ces rangs plusieurs internationaux français , dont Franck Etavard ( en sélection masculine ) et Agniès Etavard ( en sélection féminine ) .
la ville a été sacrée en juin deux mille quatorze" ville la plus sportive de France " dans sa catégorie des villes de moins de vingt habitants .
le challenge est organisé chaque année par le journal L'Équipe .
en mille neuf cent soixante , l' implantation du Centre national d' étude des télécommunications ( CNET , devenu France Télécom R&D , maintenant Orange Labs ) , imprime une forte orientation à l' industrie de la ville .
d' autres entreprises de hautes technologies emboitent le pas : Alcatel , Sagem ...
cette affluence d' entreprises a aussi été propice à l' installation d' un IUT , l' IUT de Lannion , et d' une école d' ingénieurs en mille neuf cent quatre vingt six : l' ENSSAT ( École nationale supérieure des sciences appliquées et de technologie ) .
Lannion regroupe désormais une partie importante des activités de recherche en télécommunication en France , au sein de la technopole Anticipa , qui comporte plus d' une centaine de PME et PMI .
le siège du pôle de compétitivité à vocation mondiale , " images et Réseaux " se trouve à Lannion , de même que la grappe d' entreprises Photonics Bretagne .
la ville possède une antenne de la Chambre de commerce et d' industrie des Côtes d' Armor .
la synergie de ces organismes et associations permet au bassin de Lannion et du Trégor Goëlo d' être une référence en matière d' innovation dans différents secteurs :
tic ( quatre vingt quinze entreprises ) ,
optique photonique ( vingt entreprises ) ,
industrie agro alimentaire ,
industrie marine ( trente et un entreprises ) .
Lannion est également la commune d' où le câble sous marin " apollo undersea south cable " américain aboutit en France .
le site est considéré comme d' importance stratégique et vital pour les États Unis selon un document secret émanant des révélations de télégrammes de la diplomatie américaine par WikiLeaks .
le câble sous marin WASACE Nord deux noeuds reliant la France aux États Unis , mis en service en vingt cent quatorze , part également de Lannion .
Lannion se trouve dans une zone géographique stratégique pour son économie .
à mi chemin entre Brest et Rennes , ces deux pôles économiques sont très fortement liés à Lannion pour le développement des technologies .
d' azur à l' agneau pascal couché d' argent portant une croix haute d' or au guidon de gueules chargé de l' inscription LAUS DEO en lettres capitales aussi d' or .
les armes de la ville reprennent la devise Laüs deo soit " louange à Dieu " .