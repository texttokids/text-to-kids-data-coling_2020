le ministre délégué à la Santé , Bernard Kouchner , soutient la démarche de contestation du brevet , estimant " indispensable que les équipes françaises spécialisées dans le dépistage du cancer du sein puissent continuer ( ... ) , le dépistage et la prise en charge clinique étant intimement liés " .
les laboratoires français ne sont pas seuls à s' alarmer de ce monopole .
la section allemande de l' association Greenpeace , soutenue par le corps médical allemand , prépare une plainte contre les brevets des gènes BRCA .
le laboratoire allemand d' analyses médicales Bioscientia a obtenu de la part de Myriad Genetics une forme de coopération : les échantillons à analyser seraient envoyés dans un premier temps à SALT Lake City .
en cas de résultat positif , une deuxième analyse serait effectuée par Bioscientia .
le test reviendrait aux femmes allemandes à environ dix euros contre deux sept cent cinquante euros aux États Unis .
le test , en France , revient aujourd'hui à environ sept cent cinquante euros , pris en charge intégralement par la Sécurité sociale .