pauvre micro !
que vous avait il fait , le micro ?
pourtant , il en a vu d' autres , il en a recueilli d' autres , des volées de revers , des réparties cinglantes , des savants argumentaires techniques .
mais jusqu'à présent le micro était respecté , du respect que mérite un honnête micro de l' Assemblée nationale affecté au banc du gouvernement , un micro impartial , au dessus des partis , insensible aux alternances , fièrement réservé aux réponses ministérielles .
donc le premier ministre , attaqué par la droite à propos de son passé , bouscula cavalièrement le micro de l' Assemblée .
O réflexe funeste !
O attentat subliminal de la main de l' exécutif contre un modeste auxiliaire du législatif !
si TF1 accorda évidemment à cette image la place reine dans le sommaire du " vingt heures " , c' est parce qu' elle parle toute seule , sans qu' il soit besoin d' y adjoindre un commentaire .
elle parle son propre langage , comme un lapsus .
comme le lapsus du même premier ministre quelques jours plus tôt , commençant à prononcer trotsk ...
au lieu de travaillistes et en souriant aussitôt , sans nervosité alors , mais c' était devant un parterre de camarades .
elle est un lapsus visuel , comme le geste de dédain , naguère , de Fabius face à Chirac .
elle annule la phrase , pourtant bien trouvée , bien calibrée " c' est quand même moins grave que de tarder à s' expliquer devant des juges " qu' elle était censée accompagner .
une réplique qui eût fait mouche prononcée avec sérénité , par une âme en paix avec elle même .
car ce n' est plus un combat d' homme à homme , c' est un combat d' âme à âme qui se déroule devant nous .
de fêlure à fêlure , un combat entre tireurs d' élite , chacun devant atteindre au millimètre près les fêlures de l' autre .
touché !
de la belle ouvrage : l' âme de Jospin est à nu , sans défense , livrée , vulnérable .
ils ne devaient pas rêver d' un si beau carton , à l' Elysée , en retournant le tracasser .
l' image parle sa propre langue , c' est sûr , mais peut on tenter de la déchiffrer ?
à ce moment là , dans ce lieu là , des millions de citoyens en retiendront davantage qu' un homme énervé bousculant un micro .
quoi d' autre ?
au choix , selon les sympathies , plusieurs allégories opposées y sont discernables .
le " premier ministre de la France " bousculant hautainement un humble parlementaire , le candidat talochant à distance le candidat adverse , le militant non repenti du Grand Soir rudoyant une institution bourgeoise , l' ancien rebelle se rebellant une fois encore contre une assemblée de conformistes , et leur signifiant qu' il ne sera jamais des leurs , ou bien l' homme mûr d' aujourd'hui repoussant un fantôme qui l' assiège , un fantôme à coiffure afro qui se trouve forcément face à lui .
ce fantôme , lui seul en connait les traits , les contours exacts .
les autres , tous les autres , sont donc libres de rêver .
et les imaginations de se déployer .
clandestinité , complot , duplicité , Grand Soir , société secrète , jour et nuit , main gauche ignorant la main droite : le feuilleton Jospin charrie tous les éléments de la vieille mythologie française de la conspiration , que l' on peut suivre à la trace de Michelet à Eugène Sue , du complot jésuitique au complot maçonnique .
lisons dans la presse les relations du passé de " Michel " , pseudonyme de Lionel .
tous les ingrédients y figurent : l' Organisation qui tisse sa toile dans l' ombre , patiente et méthodique , la compartimentation chaque membre ne traite qu' avec un tout petit nombre de référents , le noyautage et son avatar trotskiste : l' entrisme .
dès lors , le soupçon peut galoper .
dès lors on regarde différemment toutes les photos des années dix neuf cent soixante dix que remontre aujourd'hui la télévision , où un Jospin à tignasse , à la tribune des congrès , se penche sur les mêmes papiers que Mitterrand .
il semblait simplement lire : on dirait à présent qu' il espionne et photographie mentalement , se préparant à rendre compte .
et puis s' il fut duplice hier , pourquoi ne serait il pas duplice aujourd'hui ?
pourquoi sa main qui bouscule le micro ne feindrait elle pas d' ignorer ce que fait l' autre main ?
pourquoi , semblant décourager Montebourg , ne l' encouragerait il pas en sous main ?
tout concourt à nourrir l' inépuisable soupçon .
dans le cortège de supputations autour de la " taupe " Jospin , se mêlent fantasmes et réalités .
pour démêler les uns des autres , qui sinon lui même ?