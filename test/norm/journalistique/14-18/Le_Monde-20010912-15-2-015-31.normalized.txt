" cette fois , les épargnants commencent à avoir vraiment peur " , avoue Todd Clark , de la société de Bourse WR Hambrecht .
" tout le monde à Wall Street s' attend maintenant un jour ou l' autre à la grande dégringolade , la panique " , ajoute t il .
le krach n' était pas pour lundi dix septembre et en règle générale il ne se produit jamais quand on l' attend .
Wall Street a même limité les dégâts lundi .
l' indice Dow Jones est resté stable et le NASDAQ a regagné zéro , quarante six pour cents , mais le sentiment général reste noir ( lire aussi pages deux et huit ) .
l' Espoir d' un redémarrage en fin d' année de l' économie , qui avait permis à la Bourse de New York de retrouver un peu de vigueur au printemps , a totalement disparu .
les investisseurs se désespèrent de ne pas voir les effets positifs sur la croissance des remboursements d' impôts et surtout des sept baisses consécutives du loyer de l' argent décidées par la Réserve fédérale depuis le début de l' année .
l' atonie de la croissance américaine inquiète aussi son " arrière cour " latino américaine .
lundi , la Bourse de Buenos Aires a dégringolé de quatre , vingt neuf pour cents , et celle de São Paulo de deux , soixante et onze pour cents .
surtout , elle rejaillit en Europe .
les yeux rivés sur Wall Street , les marchés européens ont aussi enregistré , lundi , de nouveaux records annuels de baisse à Paris , Francfort , Londres , Madrid , Milan , Amsterdam , Stockholm et Zurich .
ces places sont ainsi toutes revenues plus de deux ans en arrière .
l' indice CAC40 a cédé zéro , soixante sept pour cents , à quatre trois cent quatre vingt trois , soixante quatorze points .
le Footsie a perdu zéro , quatre vingt un pour cents , à cinq vingt neuf , trente points , après être descendu en séance sous les cinq points .
l' indice Dax s' est replié de son côté de un , vingt huit pour cents , à quatre six cent soixante dix , treize points .
mais les Bourses européennes ont tout de même réduit leurs pertes en fin de journée , les marchés américains virant au vert après une ouverture morose , et l' action Nokia entrainant à la hausse l' ensemble des valeurs technologiques .
le titre Nokia a en effet progressé de six , vingt sept pour cents , à la suite des prévisions optimistes émises par le fabricant américain de composants pour téléphones portables RF Micro Devices .
la consommation RÉSISTE Mais , en Europe comme à Wall Street , les valeurs technologiques ne sont plus les seules à être abandonnées .
l' éclatement de la bulle spéculative autour d' Internet a fait plonger le NASDAQ de près de soixante dix pour cents en dix huit mois .
dans le même temps , le Dow Jones avait limité les dégâts en perdant moins de vingt pour cents .
mais la " vieille " économie n' inspire plus confiance .
les valeurs cycliques , les plus sensibles à la conjoncture , qui avaient rebondi en avril et en mai , sont aujourd'hui les plus touchées .
des titres de " père de famille " comme Boeing , Caterpillar , Walt Disney , American Express , Merrill Lynch , Ford ou Général Motors ont été vendus massivement lors des derniers jours .
les observateurs ont sous estimé l' ampleur du choc subi par la première économie mondiale .
plus de quatre milliards de dollars sont partis en fumée en quelques mois avec l' éclatement de la bulle autour de la nouvelle économie .
il s' agissait peut être de richesse virtuelle , mais elle se traduisait pour partie en investissement , en consommation et en confiance .
dans la foulée , le taux de croissance américain s' est effondré .
il est tombé de cinq , sept pour cents en rythme annuel au printemps deux mille à zéro , deux pour cents pour la même période cette année .
un retournement de conjoncture d' une telle brutalité correspond généralement au passage de la croissance à la récession .
le chiffre un peu inespéré de zéro , deux pour cents de croissance au deuxième trimestre aux États Unis tient à la résistance surprenante de la consommation .
les américains n' ont pas perdu leurs habitudes d' une décennie de prospérité et ont continué à dépenser au deuxième trimestre .
mais pour combien de temps ?
car le chômage , signe le plus tangible de l' affaiblissement de l' économie , augmente rapidement et risque de peser sur le moral des ménages .
le taux de chômage est passé en dix mois , entre octobre deux milleet août deux mille un, de trois , neuf pour cents de la population active ( au plus bas depuis trente ans ) , à quatre , neuf pour cents .
au regard des niveaux européens et français , le chômage reste faible , mais néanmoins sept cents salariés américains ont perdu leur emploi en moins d' un an .
le chômage AUGMENTE La consommation qui représente soixante huit pour cents de l' économie américaine a tenu l' activité à bout de bras depuis un an .
si elle commence à donner des signes de lassitude , la fin d' année s' annonce difficile .
" l' économie est maintenant très proche de la récession " , souligne Peter Hooper , économiste en chef de Deutsche Banc Alex Brown .
" si les actions continuent de baisser et le chômage d' augmenter , les consommateurs pourraient réduire leurs dépenses et entrainer l' économie encore plus bas " , ajoute t il .
le moral des ménages américains , dont la moitié détient un portefeuille d' actions , dépend essentiellement de trois facteurs : l' emploi , le niveau des revenus et la tendance à Wall Street .
le chômage augmente , les entreprises dont les profits baissent ne vont certainement pas augmenter les salaires .
enfin , les actions risquent de continuer à souffrir .
les baisses ont été plus que compensées par la diminution des résultats : le rapport cours bénéfices des valeurs entrant dans la composition de l' indice S et P cinq cents reste proche de vingt quatre , un niveau historiquement élevé .
" personne ne peut considérer que le marché boursier est bon marché " , affirme Jim Starck , président de InvesTech Research .
" je ne vois pas ce qui peut arrêter aujourd'hui le repli de Wall Street " , résume Jude Wanniski , analyste du cabinet Polyconomics .
en écho , comme pour mieux souligner sa crainte de voir la Bourse baisser encore , le président de la Réserve fédérale de Philadelphie , Anthony Santomero , a déclaré lundi à New York que le comité de politique monétaire de la banque centrale américaine doit être capable de baisser ses taux entre deux réunions formelles .
son collègue de la Réserve fédérale de Saint Louis , William Poole , a , lui aussi , souligné que dans certaines circonstances les arguments plaidaient en faveur d' une baisse entre deux réunions .
un message qui a permis à la Bourse américaine de tenir lundi .
pour combien de temps ?