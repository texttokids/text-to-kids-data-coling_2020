formée à l' école de Mudra à Bruxelles , Maguy Marin danse " tout son saoul " pendant trois ans chez Béjart , quand elle décide de devenir chorégraphe .
après avoir créé avec Daniel Ambash le Ballet Théâtre de l' Arche , c' est seule qu' elle crée , en dix neuf cent quatre vingt un , May B et triomphe dans le monde entier .
jean Morloch , à la Maison des arts de Créteil , l' accueille en résidence .
en dix neuf cent quatre vingt dix , elle y installe son centre chorégraphique national avec un palmarès de succès .
parmi eux : Babel , Babel un , Éden un , Les sept Péchés capitaux un , Coups d' Etats un , Cortex un , Waterzooi un , Ram Dam un .
en dix neuf cent quatre vingt cinq , une première collaboration avec l' Opéra de Lyon frappe tel un coup de baguette magique : il s' agit de Cendrillon .
Maguy Marin a également chorégraphié pour le Ballet de Paris dès dix neuf cent quatre vingt sept avec Leçons de ténèbres .
aujourd'hui , avide de retrouver plus de liberté , et une vie d' artiste fondée sur le partage , elle s' est installée à Rillieux la Pape dans le cadre du programme " partenaire pour la ville " .