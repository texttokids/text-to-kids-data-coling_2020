dix jeudi douze février vingt cent treize
objectif sciences
la grotte submergée la plus grande du monde s' étend sur trois cent quarante sept kilomètres .
elle a été découverte au Mexique Amérique centrale .
tut , tut !
direction la salle d' opération
qu' on soit petit ou grand devoir subir une opération , c' est effrayant .
à l' hôpital de Valenciennes dans le Nord , les plus jeunes patients peuvent de sormais se rendre au bloc opératoire ( la salle on
ont lieu les opérations dans une jolie voiture électrique .
plusieurs ont été offertes au service , dans le but de diminuer le stress des jeunes patients .
et vu leur sourire , ça marche !

on trouve ces grottes inondées en grand nombre au Mexique , dans la région du Yuafán .
des chercheurs les étudient pour comprendre comment elles se sont formées , mais aussi pour trouver les trésors qu' elles renferment .
c' est en menant leurs recherches qu' ils ont trouvé un passage reliant deux grandes grottes : sac Actun , qui mesure soixante trois kilomètres et celle de Dos Ojos , long de quatre vingt quatre kilomètres .
pour cela , ils ont désormais baptisée Sac Actun .

les Mayas sont un peuple qui vivait dans la péninsule du Yucatán il y a environ quatre mille cinq cent ans .
en les étudiant , les chercheurs espèrent en apprendre plus sur les Mayas .
le dix huit janvier dernier , un drone de ce type a porté assistance à deux baigneurs qui étaient en train de se noyer .
il leur a lancé une bouée et les sauveteurs sont allés les chercher .
selon les autorités australiennes , c' est la première fois qu' un drone sauve des nageurs .
naissance d' un éléphant de Sumatra
insolite Un voyage à cent euros vers le Titanic
dans la nuit du quatorze au quinze avril dix neuf cent douze, le paquebot Titanic heurte un iceberg au milieu de l' Atlantique .
le bateau se brise en deux et coule à près de quatre mille mètres de profondeur .
il reste environ vingt cent zéro éléphants de Sumatra nature , répartis sur les îles de Bornéo et cinquante quatre en Asie du Sud Est .
c' est une espèce en grand danger de disparition car les forêts des îles où ils vivent sont de pour faire de la place à l' agriculture .
depuis , la légende du bateau qui se voulait insubmersible ( impossible à couler ) continue de fasciner .
un film , sorti en dix neuf cent quatre vingt dix sept , a relancé l' intérêt pour cette histoire terrible , qui vit la mort de près de quinze cent zéro passagers .
l' épave du Titanic est localisée le premier septembre dix neuf cent quatre vingt cinq. des robots descendent à plusieurs reprises prendre des photos et étudier
une semaine d' expédition .
mais celle ci se détériore ( s' abîme ) et les prévisions les plus pessimistes prédisent qu' elle aura disparu d' ici vingt ans .
l' entreprise américaine OceanGate propose de descendre observer l' épave à bord d' un petit Sous marin conçu spécialement .
pour une semaine d' expédition , il en coutera un peu plus de cent euros .
il faut aussi être en bonne condition physique .
premier départ , du vingt sept juin au huit juillet prochain .
depuis quelques mois , la Nouvelle Zélande , pays situé dans l' océan Pacifique , est dirigée par Jacinda Ardern .
elle est devenue Premier ministre et a fait une grande annonce il y a quelques jours .
elle attend un bébé .
cela a un peu surpris , puis ravi le pays .
certains habitants ont alors eu l' idée de lui envoyer des cadeaux pour le bébé .
mais ils étaient certains qu' il serait très gâté .
des programmes de conservation de l' espece ont tenté d' empêcher sa disparition .
ils surveillent la maman de plusieurs semaines .
le petit est en bonne santé veillé par sa maman et deux autres éléphantes .
l' argent versé par les passagers pour aller voir l' épave du Titanic ira à la recherche , l' exploration et l' évaluation de ce qu' il reste du célèbre paquebot .