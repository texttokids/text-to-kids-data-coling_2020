numéro quarante six vingt trois février au premier mars deux mille dix huit
le Salon de l' agriculture et les éleveurs
des vaches , des cochons ou des poules en plein Paris : comme chaque année , des agriculteurs de toute la France se retrouvent dans la capitale pendant plusieurs jours pour présenter leurs animaux et leurs produits au Salon de l' agriculture .
ce grand évènement se déroule cette fois du vingt quatre février au quatre mars .
parmi les agriculteurs présents , il y a des éleveurs .
comment travaillent ceux qui s' occupent des animaux ?
que devient le lait de la vache et d' où vient la viande que l' on mange ?
dans ce nouveau numéro , je t' emmène découvrir les fermes .
Corinne élève des poulets et des dindes
" j' aime passer toute la journée dehors , dans la nature " , explique Corinne .
cette agricultrice vit dans la Drôme , dans le sud est de la France .
dans sa ferme , elle élève des volailles ( des poulets et des dindes ) .
elle cultive aussi des arbres fruitiers ( abricotiers , poiriers ) et des champs de céréales .
Corinne travaille en famille , avec son mari , leur fils et sa femme et le frère de Corinne .
ils possèdent quarante volailles et ça demande beaucoup de travail : " je me lève à six heures et je vais voir les volailles dans leur bâtiment , pour m' assurer qu' il est à la bonne température et qu' elles ont assez d' eau , de nourriture et qu' elles sont en bonne santé . " ensuite , elle remet de la paille dans les bâtiments où vivent les volailles .
" c' est une tâche assez longue car ça se fait à la main " , explique t elle .
l' après midi , elle doit souvent remplir des papiers ou payer des factures par exemple .
parfois , elle aide aussi son mari à s' occuper des arbres fruitiers : soigner les arbres , ramasser les fruits lorsque c' est la saison ...
le soir , elle fait un dernier tour des volailles vers dix neuf heures avant de rentrer dans sa maison , située tout près .
" lorsqu' on a des animaux , il faut habiter dans la ferme . c' est plus pratique , et ça nous rassure . mais du coup , on ne peut pas prendre beaucoup de vacances . "
quand les volailles ont un peu plus d' un mois , elles sont emmenées à l' abattoir .
la viande est ensuite mise en barquettes par d' autres personnes , pour être vendue dans les supermarchés de la région .
Corinne et son mari travaillent dans leur ferme depuis vingt sept ans .
mais ils ont commencé à élever des volailles il y a cinq ans .
" le métier d' agriculteur , c' est savoir s' adapter . il faut s' adapter aux nouvelles lois , à ce que les gens ont envie de manger . "
saint barthélemy de vals , le village où vit et travaille Corinne
qu' est ce que le Salon de l' agriculture ?
le Salon de l' agriculture existe depuis cinquante quatre ans et se tient tous les ans à Paris .
cette année , il a lieu du vingt quatre février au quatre mars .
on dit que c' est un salon parce qu' il réunit des agriculteurs de toute la France qui viennent avec leurs bêtes et les produits qu' ils fabriquent .
ils vendent ainsi sur place aux visiteurs du fromage , du miel , du vin ou de la viande ...
des entreprises qui travaillent à partir des produits de l' agriculture sont aussi présentes .
" le salon permet aux agriculteurs de montrer le résultat de leur travail . pour les visiteurs , c' est l' occasion de leur demander comment ils travaillent . certains viennent voir les animaux , demander aux éleveurs combien de fois par jour ils traient les vaches . d' autres encore veulent gouter les produits des régions qu' ils ne connaissent pas " , explique Valérie Le Roy , la directrice du Salon de l' agriculture .
" en France , l' agriculture est très importante : beaucoup de gens ont eu des grands parents ou un membre de leur famille qui était agriculteur " , indique Valérie Le Roy .
cet évènement intéresse donc de nombreuses personnes .
la visite du Salon est aussi une tradition pour les femmes et les hommes politiques .
ils y vont pour se montrer et pour voir les agriculteurs , qui leur parlent chaque année des problèmes qu' ils rencontrent .
comme tous les présidents avant lui , Emmanuel Macron s' y rendra aussi .
qu' est ce qu' un éleveur ?
en France , sept cent dix personnes travaillent dans l' agriculture , c' est à dire environ une personne sur cent .
il y a quatre cent cinquante fermes , dont les tailles sont très différentes .
certains agriculteurs cultivent la terre pour faire pousser des fruits , des légumes et des céréales .
d' autres élèvent des animaux : vaches , cochons , poules , chevaux ...
ce sont des éleveurs .
certains pratiquent l' élevage " intensif " , c' est à dire qu' ils élèvent un maximum d' animaux pour produire le plus possible de viande , de lait ou d' oeufs .
on parle par exemple d' élevage intensif quand une ferme a plus de quarante volailles ( comme des poules ) .
les animaux grandissent souvent serrés les uns contre les autres dans de grands hangars avec de la lumière artificielle , sans lumière du jour donc .
les volailles sont tuées lorsqu' elles ont entre un mois et trois mois .
d' autres éleveurs ont de plus petites exploitations .
c' est par exemple le cas d' Anthony Clouté , quarante deux ans , qui possède un poulets à Pontiacq Viellepinte , dans les Pyrénées Atlantiques .
cet agriculteur achète des poussins lorsqu' ils ont un jour .
il les élève pendant un mois dans des espaces chauffés , puis pendant trois mois dans des enclos à l' air libre .
" les poulets ont des petites cabanes pour la nuit ou pour se protéger du froid . il y a aussi des arbustes pour leur faire de l' ombre l' été " , explique t il .
Anthony élève donc ces poulets pendant quatre mois , soit un mois de plus que dans l' élevage intensif .
il les nourrit avec les céréales qu' il cultive sans pesticide et avec le moins d' engrais possible .
" j' ai choisi d' élever peu de volailles pour leur bien être mais aussi parce qu' on ne peut pas faire à la fois de la quantité et de la qualité " , remarque t il .
l' éleveur vend ensuite ses poulets sur les marchés ou avec l' aide d' associations qui mettent en lien les petits producteurs et les consommateurs .
" ça me permet d' être payé en fonction du travail que je fais mais aussi de voir les personnes qui mangent ma viande . je fais du poulet pour nourrir les gens . c' est important d' avoir leurs retours " , ajoute cet agriculteur .
que devient le lait de la vache ?
en France , soixante dix agriculteurs élèvent des vaches laitières .
ces fermiers produisent vingt trois , cinq milliards de litres de lait par an .
mais tout ce lait n' est pas seulement bu en France .
d' ailleurs , une grande partie du lait n' est pas bu du tout mais transformé en d' autres produits .
dès qu' il sort du pis de la vache , le lait est gardé au froid .
l' agriculteur le garde dans sa ferme entre deux et trois jours .
ensuite , il est transporté dans une laiterie .
là , le lait est transformé en produits laitiers comme du fromage , du beurre ou des yaourts .
les produits qui sortent des laiteries sont principalement vendus dans les supermarchés .
beaucoup sont aussi vendus dans les pays étrangers .
quand on achète une brique de lait , une partie de l' argent revient à l' agriculteur .
une partie va à la laiterie et une autre au magasin qui a vendu la brique .
chacun veut gagner une " marge " , c' est à dire vendre le lait ou le produit un peu plus cher que ce que ça lui a couté à fabriquer .
si on achète un produit un euro et qu' on le vend un euro , on n' a rien gagné .
en revanche , si on vend ce produit un , vingt euro , on a gagné vingt centimes : c' est ça , la marge .
parfois , des agriculteurs manifestent à cause des marges .
ils ne sont pas d' accord avec ceux qui dirigent les laiteries ou les supermarchés .
ils n' arrivent pas à s' entendre sur le prix du lait .
les agriculteurs disent que les laiteries n' achètent pas leur lait assez cher , par rapport à ce que leur coute la fabrication de ce lait ( machines pour traire les vaches , temps passé à les soigner ...

" souvent , les laiteries et les grandes surfaces font partie de gros groupes , d' entreprises puissantes . ce sont elles qui fixent les prix et les agriculteurs n' ont pas assez de pouvoir pour s' y opposer " , explique Vincent Chatellier , scientifique spécialiste de l' agriculture .
cette " guerre des prix " existe aussi pour d' autres produits issus de l' agriculture , comme la viande .
comment savoir d' où viennent la viande ou le lait ?
quand on achète de la viande au supermarché , par exemple du steak haché , on peut trouver des indices sur l' étiquette pour savoir d' où elle vient .
il y a :
toutes ces informations sont utiles pour savoir d' où vient cette viande .
elles permettent de retracer le parcours de la viande : d' où venait la vache , où elle a été abattue , dans quelle usine elle a été hachée et mise en barquette .
ces informations peuvent être utiles en cas de problème .
si plusieurs personnes tombent malades après avoir mangé de la viande , le supermarché regarde le numéro du lot pour voir quelles autres barquettes de viande ont été fabriquées au même moment que celle qui a rendu les gens malades .
pour éviter que d' autres personnes tombent malades , le magasin les retire alors de ses rayons et prévient les clients qui en ont acheté de ne pas manger la viande .
ça peut arriver avec de la viande mais aussi avec du lait , comme il y a quelques semaines avec le lait en poudre de l' entreprise Lactalis .
des bébés ont été malades après en avoir bu .
le lait en poudre avait en effet été contaminé par une bactérie .