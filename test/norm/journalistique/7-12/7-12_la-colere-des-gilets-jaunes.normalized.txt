numéro quatre vingt trois sept au treize décembre deux mille dix huit
la colère des gilets jaunes
depuis plusieurs semaines , on parle beaucoup des " gilets jaunes " dans l' actualité .
ce sont des personnes qui manifestent et bloquent des routes dans tout le pays parce qu' elles trouvent que le Président prend des décisions injustes .
il n' y avait jamais eu de mouvement comme celui ci en France .
au début du mois , des violences ont éclaté à Paris , mais la plupart des manifestants refusent ces débordements .
cette semaine , je t' emmène sur les routes de France pour mieux comprendre ce qu' il se passe .
Lola , sept ans , a participé à des rassemblements
deux samedis de suite , Lola a suivi sa maman dans des rassemblements de " gilets jaunes " sur des ronds points près de chez elle , en Normandie .
" les gens disaient que Macron doit démissionner , raconte la petite blonde de sept ans . c' était pour que Macron baisse les prix des trucs qui coutent cher . "
Lola était entourée de dizaines de personnes qui demandaient au président de la République de changer des choses dans le pays .
elles portaient toutes des gilets jaunes , le symbole de ces rassemblements .
il s' agit de vestes fluorescentes que les conducteurs doivent porter s' ils descendent de leur véhicule au bord de la route , afin d' être bien vus par les autres automobilistes .
Lola , elle , n' a pas mis de gilet , c' était beaucoup trop grand pour elle !
du haut de ses sept ans , elle a pris une sorte de trompette pour faire du bruit et mettre un peu d' ambiance .
elle a cru comprendre que " chaque fois qu' on paye les trucs , c' est Macron qui a les sous " .
sa maman lui souffle que ce sont les impôts , c' est à dire l' argent que les citoyens donnent à l' Etat pour qu' il paye les enseignants et les policiers ou répare des routes , par exemple .
le Président n' utilise pas cet argent pour lui même , c' est interdit .
mais c' est lui qui décide comment il est utilisé à travers le pays , et les manifestants pensent qu' il ne prend pas des décisions justes .
lors du premier rassemblement , tout s' est bien passé , il y avait une bonne ambiance , de la musique .
mais le samedi suivant , c' était différent .
des policiers et des gilets jaunes se sont opposés , et les policiers ont utilisé du gaz lacrymogène , qui pique les yeux .
" j' ai couru avec ma maman " , dit Lola .
est ce qu' elle a eu peur ?
" non , moi j' ai rigolé ! s' amuse t elle . mon père il a pris le gaz , il a eu mal aux yeux . " sa mère ne veut plus l' emmener aux rassemblements , c' est trop dangereux .
alors seul le papa de Lola continue de manifester .
Déville lès Rouen , la ville où habite Lola
que se passe t il en France ?
de nombreuses personnes sont de plus en plus en colère un peu partout en France .
une grande journée de manifestations est prévue ce week end dans tout le pays et beaucoup de personnes craignent qu' il y ait des violences .
tout a commencé il y a trois semaines .
des personnes se sont rassemblées dans plusieurs villes contre la décision du gouvernement d' augmenter les taxes sur le prix du carburant .
leur signe distinctif : ces manifestants portent un gilet fluorescent jaune .
c' est pour ça qu' on les appelle les " gilets jaunes " .
peu à peu , de plus en plus de personnes se sont rassemblées pour bloquer des routes , des péages , occuper des ronds points ...
des milliers de personnes portent désormais un gilet jaune .
elles ne sont plus seulement en colère contre les prix du carburant .
elles disent au gouvernement qu' elles n' ont plus assez d' argent pour vivre et que ça suffit .
la grande journée de manifestations prévue dans toute la France ce week end inquiète parce que ça s' est mal passé à Paris la semaine d' avant , le samedi premier décembre .
des groupes de personnes ont fait preuve d' une rare violence .
il y a eu moins cent trente blessés .
des monuments et lieux importants comme l' Arc de Triomphe ou les Champs Élysées , ont été très endommagés .
des voitures ont été brulées , des vitrines de magasins ont été détruites et des boutiques dévalisées .
résultat : des commerçants sont inquiets parce qu' ils ne peuvent pas bien vendre alors que c' est bientôt Noël .
" ce qui s' est passé à Paris n' est pas représentatif de ce qu' il se passe sur le territoire " , remarque le sociologue Alexis Spire .
les gilets jaunes sont en effet avant tout " des rassemblements de petites dizaines de personnes qui se retrouvent tous les jours , pendant des heures , partout en France , dans une ambiance bon enfant , explique le sociologue Benoît Coquard . ils veulent être bien vus , ne veulent pas bloquer les pompiers , les infirmiers ou les gens du coin , refusent la violence et rangent quand il y a de la casse , quand c' est sale " .
accusé de ne pas réagir , le gouvernement a finalement décidé mercredi cinq décembre de ne pas augmenter les taxes sur le prix du carburant , au moins pour l' année deux mille dix neuf .
mais ça ne suffit pas du tout pour les manifestants .
pourquoi les gilets jaunes sont ils mécontents ?
à l' origine , les " gilets jaunes " ont décidé de manifester à cause de l' augmentation des taxes sur le prix du carburant .
ce produit permet aux voitures de fonctionner mais il est très mauvais pour la planète .
le problème , c' est que de nombreuses personnes ont besoin de leur voiture .
la plupart des gilets jaunes habitent loin des grandes villes , là où il y a peu de transports en commun .
ils doivent donc davantage prendre leur voiture pour se déplacer .
en plus de ça , des lieux indispensables comme la poste , l' hôpital ou des entreprises ferment depuis plusieurs années dans les territoires moins peuplés .
donc les gens doivent de plus en plus prendre leur voiture pour aller au travail , chez le médecin , faire les courses ...
augmenter le prix du carburant , ça fait augmenter les dépenses de ces familles , qui voient ça comme une injustice parmi de nombreuses autres .
" ce sont des gens qui ont un emploi mais qui ne vivent pas dans le luxe . ils ont l' impression que l' Etat leur prend leur argent alors que ce n' est pas eux qui en ont le plus " , constate le sociologue Benoît Coquard .
les gilets jaunes partagent tous un point commun : leur mécontentement contre les dirigeants politiques , en particulier contre Emmanuel Macron .
ils accusent le président et le gouvernement d' être responsables des difficultés de leur vie quotidienne .
c' est pour ça que les gilets jaunes dénoncent les différentes inégalités qui existent en France .
ils souhaitent que les gens très riches payent , comme avant , l' impôt sur la fortune .
c' est de l' argent que les plus fortunés devaient verser à l' Etat en fonction des biens ( maison , voitures , bijoux , et caetera ) qu' ils possèdent .
ça aidait à construire des hôpitaux ou payer le salaire des enseignants , par exemple .
Emmanuel Macron a décidé de le supprimer .
les gilets jaunes demandent aussi que l' écart soit moins grand entre le salaire minimum et celui de certains patrons de très grosses entreprises , qui peuvent gagner des centaines de milliers d' euros .
qui a le dernier mot : le peuple ou le président ?
beaucoup de " gilets jaunes " disent qu' ils se méfient des responsables politiques , qu' ils ne leur font pas confiance pour améliorer leur vie .
d' ailleurs , un certain nombre de manifestants ne votent pas , ils pensent que ça ne sert à rien .
c' est notamment pour ça qu' ils préfèrent s' organiser entre eux .
la France est une démocratie : ça veut dire que le pouvoir appartient à tous les citoyens .
mais si tout le monde faisait comme il voulait , ce serait le bazar .
alors les habitants élisent des représentants ( maires , députés , président ...
) , à qui ils donnent le droit de prendre des décisions pour tout le monde .
le président de la République est élu tous les cinq ans , mais " c' est impossible de gouverner sur une longue période sans l' accord de la majorité des gens sur les grandes décisions , note le politologue Bruno Cautrès .
en France , comme dans les autres démocraties , on ne peut pas être le grand chef et tout décider tout seul , sinon les citoyens se mettent en colère .
parfois , les manifestants obtiennent ce qu' ils demandent .
c' est grâce à des grandes manifestations que les travailleurs français ont obtenu il y a longtemps le droit de partir en vacances tout en étant payés ou que le salaire minimum a été augmenté .
ce sont des avancées obtenues grâce à la pression de gens qui ont défilé dans la rue !
mais parfois , les dirigeants refusent de changer d' avis , comme ça a été le cas l' année dernière avec une loi sur le travail qui a quand même été votée alors que beaucoup de gens avaient manifesté contre .
en vingt cent dix sept , des milliers de manifestants se sont opposés pendant plusieurs semaines à une loi très importante sur le travail ( comme ici à Paris avec des étudiants inquiets ) qui a quand même été adoptée à la fin .
photo Martin Colombet .
Hans Lucas
pendant trois semaines , le président Emmanuel Macron montrait que les manifestations des gilets jaunes ne le feraient pas changer d' avis : " depuis son élection , il veut montrer aux français qu' il n' est pas comme les autres présidents avant lui , qu' il a un projet , qu' il s' y tient " , analyse le politologue Bruno Cautrès .
sauf que les gilets jaunes veulent être considérés comme des citoyens à part entière .
ils n' apprécient pas du tout l' attitude des dirigeants , notamment celle du président , " qu' ils jugent méprisant , déconnecté de la réalité , du quotidien " , note le sociologue Alexis Spire .
le mouvement des gilets jaunes est en plus très soutenu par la population française .
or " lorsque tout le monde se sent concerné par les revendications et quand les manifestants ne donnent pas le sentiment de défendre une catégorie de français contre les autres , très souvent ils finissent par obtenir gain de cause , en tout cas en partie " , remarque Bruno Cautrès .
c' est notamment pour ça que le gouvernement a décidé de ne finalement pas augmenter la taxe sur le carburant .
" il faut qu' Emmanuel Macron montre qu' il a tiré des leçons " de tout ça , recommande Bruno Cautrès .
" ça veut dire davantage dialoguer avant de prendre ses mesures " .
parce qu' un pays est dirigé par des élus , mais en lien avec le peuple .