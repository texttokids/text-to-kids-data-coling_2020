numéro vingt cinq vingt neuf septembre au cinq octobre deux mille dix sept
ouragans : une année record
Harvey , Irma , José , Maria ...
depuis la fin du mois d' août , des ouragans très puissants se sont formés dans le nord de l' océan Atlantique , faisant des dizaines de victimes et des dégâts très importants .
pourquoi y a t il des ouragans si intenses cette année ?
comment se forment ils ?
peuvent ils toucher la France métropolitaine un jour ?
je t' emmène à la découverte de ce phénomène climatique impressionnant .
Tashania , onze ans , habite Saint Martin , dévasté par Irma
Tashania , onze ans , vit avec ses parents et son petit frère de sept ans , au bord de l' étang aux Poissons ( que tout le monde appelle " le lagon " ) à Quartier d' Orléans .
c' est le secteur le plus pauvre de Saint Martin , une île située dans les Antilles et partagée entre la France et les Pays Bas .
le six septembre , elle était chez elle quand l' ouragan Irma a frappé son île : " au début , j' étais dans ma chambre toute seule , et puis j' ai eu peur à cause du bruit . ça faisait " boum " . je suis descendue et on est restés longtemps dans le salon . à un moment , il y a eu un bruit très fort et un bout du toit s' est envolé . j' ai vu un trou . il pleuvait dans la maison " , raconte t elle .
elle s' est alors cachée dans les toilettes avec son petit frère et sa mère et a " un peu pleuré " .
elle a eu très peur " quand l' électricité et la radio se sont éteintes . on était dans le noir . à la fin , on est partis dans la maison des voisins et on était beaucoup alors j' étais contente " .
toute sa famille est heureusement en vie , même les sept chiots nés la veille de l' ouragan .
de nombreux meubles et objets ont été cassés chez elle et , même s' il n' y a pas trop de dégâts dans sa chambre , elle préfère dormir avec toute sa famille dans le salon parce qu' elle a peur qu' un autre cyclone revienne : " on rigole parce que papa prend beaucoup de place et ça nous fait tomber du matelas . "
Tashania aimerait avoir " une maison plus solide " pour être protégée des futurs ouragans mais ses parents " n' ont pas trop d' argent " .
depuis Irma , elle ne peut plus aller à l' école et ça la rend " triste " .
heureusement , ses copines habitent à côté de chez elle .
lors de l' ouragan , l' une d' elles " a dû nager pour sortir de sa maison parce que l' eau est montée très haut dedans . " pour occuper ses journées , elle joue à cache cache : " depuis le cyclone , c' est plus drôle parce qu' il y a plein de nouvelles cachettes . mais mon petit frère me trouve tout le temps . "
l' Île de Saint Martin et le village Quartier d' Orléans où habite Tashania
qui sont Harvey , Irma et Maria ?
depuis la fin du mois d' août et en l' espace de trois semaines , le nord de l' océan Atlantique a été frappé par quatre ouragans très puissants .
comme c' est le cas pour tous les ouragans , on leur a donné des prénoms : les deux plus intenses s' appelaient Irma et Maria .
les Antilles ont été particulièrement touchées .
ce sont des îles situées dans l' océan Atlantique , entre l' Amérique du Nord et l' Amérique du Sud .
elles sont habituées aux tempêtes tropicales et aux ouragans , qui se produisent régulièrement entre juin et novembre .
mais , cette fois , ils ont été particulièrement puissants .
les ouragans sont classés en catégories selon la vitesse de leurs vents .
et les quatre ouragans récents étaient de catégorie quatre ou cinq !
les ouragans sont classés en cinq catégories selon la puissance de leurs vents
le premier de ces puissants ouragans s' appelait Harvey ( classé en catégorie quatre ) : il a touché le sud des États Unis fin août et à fait de nombreux dégâts , surtout à cause de pluies torrentielles et d' inondations .
puis ce fut au tour d' Irma , l' ouragan le plus puissant jamais enregistré dans l' océan Atlantique ( catégorie cinq ) , de frapper les Caraïbes , en particulier les îles françaises de Saint Barthélemy et de Saint Martin .
quinze jours plus tard , Maria , un nouvel ouragan , sévit dans la même zone et frappe de plein fouet l' île de la Dominique en atteignant la catégorie cinq .
il y a eu des dizaines de morts à cause de ces phénomènes exceptionnels .
dans les îles les plus touchées , de très nombreux habitants se sont retrouvés sans maison , sans électricité et sans eau potable .
l' Île de Saint Martin , dévastée après l' ouragan Irma
les îles françaises de la Martinique et de la Guadeloupe ont été très touchées aussi .
sur ces deux dernières îles , quasiment toutes les bananeraies ont été dévastées ou abimées .
au début , certaines îles comme Saint Martin étaient coupées du monde : les avions ne pouvaient plus atterrir parce que les aéroports étaient abimés , le réseau de téléphone ne fonctionnait plus , et il y avait des coupures d' électricité .
et puis , petit à petit , les secours ont pu arriver sur place pour aider la population .
qu' est ce qu' un ouragan ?
un ouragan est une large zone de nuages orageux associée à un tourbillon de vents très violents , qui tourne sur lui même comme une toupie .
on l' appelle aussi cyclone tropical .
chaque année , entre juin et novembre , un vent orageux apporte la pluie en Afrique de l' Ouest .
ce vent fait ensuite un long voyage au dessus de l' océan Atlantique .
si la température de l' eau qu' il survole est supérieure à vingt six degrés ( au moins les cinquante premiers mètres de profondeur ) et que l' air est humide , ce vent a des chances de se transformer en ouragan .
après un long voyage , les ouragans se forment dans le nord de l' océan Atlantique
comment se forme un ouragan ?
certains ouragans vivent une semaine , d' autres jusqu'à quinze jours .
s' ils traversent une zone d' eau froide ou s' ils passent sur la terre ferme , ils perdent leur énergie et ils finissent par disparaitre .
tempêtes , cyclones , tornades : quelles différences ?
tempête tropicale : les vents soufflent entre soixante trois et cent dix sept kilomètres par heure .
les pluies peuvent être fortes , les vents commencent à faire des dégâts et , à cause d' eux , la mer devient dangereuse .
cyclone tropical : les vents dépassent les cent dix huit kilomètres par heure .
c' est presque aussi rapide qu' une voiture qui roule sur l' autoroute ( soit cent trente kilomètres par heure en France ) .
le cyclone a deux surnoms selon la région où il se forme : typhon quand il apparait dans l' océan Pacifique , et ouragan quand il nait dans l' océan Atlantique .
tornade : c' est un tourbillon de vents très violents ( jusqu'à cinq cents kilomètres par heure ) qui nait sous un nuage d' orages .
la tornade peut être très petite , très brève ( quelques minutes ) et très localisée : elle peut détruire une maison sans toucher celle d' à côté .
parfois , les ouragans donnent lieu à des tornades , comme ça a été le cas avec Irma .
pourquoi n' y a t il pas d' ouragans en métropole ?
" des ouragans comme Irma ou Maria ne peuvent pas se former en France métropolitaine " , remarque Robert Vautard , climatologue , chercheur au Laboratoire des Sciences du climat et de l' environnement ( LSCE ) .
d' abord , l' eau n' est pas assez chaude .
la Méditerranée peut atteindre les vingt six degrés mais seulement en surface , donc aucun cyclone ne peut s' y former .
ensuite , les vents que l' on a en Europe ne permettent pas aux cyclones de se développer avec la même puissance que dans les tropiques .
la différence de vent entre le haut et le bas de l' atmosphère est aussi parfois trop forte en métropole , ce qui empêche les tourbillons de bien se former parce que les nuages partent dans tous les sens .
" les cyclones tropicaux peuvent quand même finir leur course en Europe , mais sous la forme d' une dépression avec des vents beaucoup plus faibles " , conclut Robert Vautard .
peut on craindre des ouragans en Europe dans les années à venir avec le réchauffement climatique ?
" non , répond Robert Vautard . même si la mer se réchauffe de quelques degrés , nous n' aurons jamais de cyclones tropicaux avec des vents de plus de trois cents kilomètres par heure . "
en Europe , nous avons en revanche des tempêtes .
elles ont plutôt lieu en hiver et ne sont pas liées à la température de l' océan mais aux courants de vents violents d' altitude .
à partir de cent cinquante kilomètres par heure de vent , c' est à dire plus de deux fois moins que la vitesse atteinte par Irma , on parle en France métropolitaine d' événements exceptionnels .
parmi les tempêtes les plus violentes , il y a eu Lothar et Martin en dix neuf cent quatre vingt dix neuf qui ont fait quatre vingt douze morts et Xynthia en vingt cent dix qui a frappé principalement l' ouest de la France et fait cinquante trois morts .
pourquoi cette année bat elle des records ?
Harvey , Irma , José , Maria ...
depuis la fin du mois d' août , des ouragans de catégories quatre ou cinq ont touché les habitants de l' Atlantique nord et des Caraïbes .
ces ouragans très puissants ont même battu des records : Irma est resté soixante douze heures en catégorie cinq ( le temps le plus long jamais enregistré ) et Maria est passé de catégorie trois à cinq en une quinzaine d' heures alors que d' habitude , il faut au moins un jour .
en ce moment , dans les Caraïbes , la mer est à vingt huit degrés sur plusieurs dizaines de mètres de profondeur .
elle est de un , cinq degré plus chaude que la moyenne des trente années précédentes .
or , plus la mer est chaude , plus elle aide un ouragan à se former et lui donne de la force .
est ce à cause du réchauffement climatique ?
en cent cinquante ans , la température a augmenté environ d' un degré sur la Terre et les eaux tropicales se sont réchauffées , comme tous les océans .
" c' est un facteur possible mais on est loin de démontrer que ce petit degré a permis une augmentation de ce genre d' événement . le hasard a peut être une grande part de responsabilité . ça peut être bien plus calme l' année prochaine " , indique le climatologue Robert Vautard .
dans les mois à venir , un groupe de chercheurs va donc se réunir pour comprendre pourquoi ces gros ouragans se sont formés cette année .
des chercheurs avaient par exemple montré que l' ouragan Sandy , qui avait touché les États Unis en vingt cent douze , aurait été moins fort s' il était né trente ans plus tôt , parce que la mer était moins chaude à ce moment là .
depuis quarante ans environ , on a des données plus précises sur les ouragans : " leur intensité augmente légèrement dans l' Atlantique et les Caraïbes " , explique la climatologue Valérie Masson Delmotte , coprésidente du Giec , un groupe d' experts sur l' évolution du climat .
avec le réchauffement climatique , " on ne s' attend pas à avoir plus d' ouragans mais à ce que l' intensité des plus puissants augmente . il reste encore beaucoup d' interrogations , notamment sur la manière dont les trajectoires et la vitesse de déplacement des ouragans pourraient changer . la montée du niveau de la mer renforce aussi le risque de submersion sur les côtes " , ajoute t elle .