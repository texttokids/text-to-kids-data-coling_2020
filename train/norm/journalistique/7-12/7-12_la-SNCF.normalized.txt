numéro quarante sept deux au huit mars vingt cent dix huit
la SNCF
la SNCF , l' entreprise qui fait rouler les trains en France , est en ce moment au coeur de l' actualité .
le gouvernement souhaite changer des choses dans la façon dont elle fonctionne , et des gens qui y travaillent ne sont pas contents .
qui sont ces " cheminots " dont on parle tant ?
comment fonctionne la SNCF et quelle est son histoire ?
pour tout comprendre , monte à bord du P' tit Libé Express !
jean philippe est conducteur de train
un peu avant dix huit heures , Jean Philippe commence sa journée de travail .
depuis dix ans , il conduit des trains .
avant de faire démarrer son véhicule , cet homme de trente huit ans se rend dans les bureaux de la gare de l' Est , à Paris , pour récupérer son emploi du temps .
son train ( appelé Transilien ) doit quitter la gare à dix huit heures vingt et un et rouler jusqu'à Château Thierry , une ville située à une centaine de kilomètres de Paris .
il a treize minutes pour préparer sa journée avant de rejoindre son engin .
" tout petit , je voulais conduire des trains . c' est un rêve de beaucoup d' enfants " , se souvient Jean Philippe , qui jouait avec un train électrique quand il était plus jeune .
il faut dire qu' il connaissait déjà bien cet univers : son père , son grand père et son arrière grand père , notamment , étaient cheminots .
" la ligne , on doit la connaître par coeur . on doit connaître les gares , les vitesses , les trains " , explique Jean Philippe .
son Transilien roule la plupart du temps à cent quarante kilomètres par heure , mais parfois la vitesse est moins élevée , par exemple quand il y a des travaux sur la ligne .
à cette vitesse et avec un véhicule aussi grand , il faut beaucoup de temps pour s' arrêter .
jean philippe doit commencer à freiner un kilomètre ( la longueur de dix terrains de foot ) avant chaque gare pour réussir à s' arrêter devant les voyageurs qui attendent .
pour savoir quand commencer à freiner , il a ses repères , comme une maison originale sur le bord de la route ou un tunnel .
si quelqu' un est trop proche du bord du quai , il déclenche un gros klaxon .
qu' est ce qui lui plait dans ce métier ?
" être autonome , répond Jean Philippe . et je ne fais jamais deux fois la même chose . " à chaque trajet , il transporte au moins un personnes , parfois deux .
ce jour là , il commence le travail à dix huit heures mais parfois c' est à trois heures , à midi ou encore à vingt heures .
ça change tout le temps .
huit à dix fois par mois , il ne dort pas chez lui mais dans la ville où il termine son trajet .
des logements sont prévus pour les conducteurs .
comment fait il pour avoir une vie de famille , avec sa femme et sa fille de vingt mois ?
" c' est compliqué , sourit il . mais je travaille souvent en soirée donc je passe toute la journée avec ma fille . " même si , dans sa famille , on est cheminot depuis plusieurs générations , Jean Philippe l' assure : ça ne le dérangera pas si sa fille ne suit pas ses pas .
pourquoi parle t on de la SNCF ?
aujourd'hui en France , une entreprise s' occupe des trains : la SNCF ( Société nationale des chemins de fer français ) .
mais dans quelques mois , d' autres auront le droit de faire rouler des trains dans le pays .
la SNCF est habituée à travailler toute seule et va donc devoir modifier sa façon de fonctionner .
le gouvernement voudrait que la SNCF lui coute moins cher .
chaque année , il lui donne dix milliards d' euros pour acheter ses trains , les faire rouler et réparer les rails .
le vingt six février , le Premier ministre , Édouard Philippe , a expliqué de quelle façon l' entreprise allait devoir changer .
il y a une proposition dont on parle beaucoup : la fin du statut de cheminot .
voyons ce que ça veut dire .
la grande majorité des personnes qui travaillent à la SNCF sont appelées des cheminots .
les cheminots ont droit à certaines choses que d' autres travailleurs n' ont pas .
par exemple , ils peuvent prendre le train gratuitement et leur famille aussi .
ils partent à la retraite plus tôt que la plupart des personnes qui travaillent dans d' autres entreprises .
et puis , si la SNCF perd beaucoup d' argent , les cheminots ne peuvent pas être renvoyés , alors que c' est possible dans d' autres entreprises .
les cheminots ont donc des avantages .
ça existe aussi dans d' autres professions .
par exemple , les journalistes ont le droit de payer moins d' impôts que la plupart des autres travailleurs et ils peuvent aller gratuitement dans les musées .
le gouvernement trouve que les cheminots ont trop d' avantages et que ça coute trop cher .
il veut donc qu' on arrête de les donner aux nouvelles personnes qui travaillent à la SNCF .
des cheminots ne sont pas contents .
certains veulent faire grève , c' est à dire arrêter de travailler , pour que le gouvernement change d' avis .
la SNCF et les autres ?
la SNCF s' occupe de faire rouler les trains en France .
mais il existe quelques exceptions .
par exemple , ce n' est généralement pas elle qui s' occupe des tramways et des métros , qui se déplacent à l' intérieur d' une même ville et dans quelques communes à côté .
ce n' est pas non plus elle qui fait rouler les trains en Corse mais une entreprise appelée les Chemins de fer de la Corse .
qu' est ce que la SNCF ?
il existe des trains en France depuis dix huit cent vingt sept , c' est à dire depuis près de deux cents ans .
à une époque , il y avait plusieurs compagnies de train dans le pays et chacune s' occupait d' une zone en particulier ( par exemple , l' est de la France ) .
en dix neuf cent trente huit , le gouvernement les a réunies dans une même entreprise , appelée la Société nationale des chemins de fer français .
la SNCF a donc quatre vingts ans aujourd'hui .
les compagnies de chemin de fer avant la création de la SNCF
peu à peu , la SNCF a fait en sorte qu' il y ait des trains dans toute la France .
à l' époque , on comptait treize gares alors qu' il y en a trois aujourd'hui .
les gens étaient cheminots de père en fils .
beaucoup vivaient avec leur famille dans des cités cheminotes .
quand on voyageait dans un train , il y avait des voitures restaurants avec des cuisiniers qui faisaient les repas sur place , alors qu' aujourd'hui on achète des plats tout faits .
comme les trajets duraient plus longtemps qu' aujourd'hui , il y avait beaucoup de trains avec des couchettes ( des sortes de lits ) sur lesquelles les voyageurs dormaient .
dans les gares , il y avait des bagagistes , qui portaient les valises .
à la création de la SNCF , il existait des locomotives électriques , mais les trains étaient surtout tirés par des locomotives à vapeur .
ces machines consommaient énormément de charbon et ça coutait cher .
en plus , il fallait huit heures pour les mettre en marche !
pour un trajet entre Paris et Marseille avec une locomotive à vapeur , le trajet durait vingt heures contre trois heures aujourd'hui .
aujourd'hui , la grande majorité des trains fonctionnent grâce à l' électricité .
à quoi ressemble le réseau de chemin de fer ?
la France est l' un des pays où il y a le plus de lignes de chemin de fer au monde .
on en compte trente kilomètres .
il existe différents types de trains , qui ne servent pas tous à la même chose .
Frank Bernard , qui travaille à la SNCF , a aidé le P' tit Libé à y voir plus clair .
les lignes de chemin de fer de la SNCF
si on veut voyager à l' intérieur d' une région , on prend généralement un TER ( train express régional ) .
ces trains font des distances plutôt courtes .
chaque région s' occupe de ses TER , décide où elle installe des lignes , et caetera En Île de France , ces trains ont un autre nom : Transilien .
certaines lignes de train traversent plusieurs régions .
on les appelle des Intercités .
là , c' est l' Etat qui décide où on les installe et comment elles fonctionnent .
et puis il y a les TGV , les trains à grande vitesse .
comme leur nom l' indique , ils roulent plus vite que les autres .
ils servent à faire de longues distances .
le chemin de fer , c' est un peu comme la route : il y a des endroits où on peut rouler vite et d' autres pas .
pour pouvoir rouler vite , il faut des lignes droites .
quand il y a des virages ou des pentes , les trains doivent ralentir , sinon les roues peuvent sortir des rails .
on dit alors que les trains déraillent , et ça peut être très dangereux .
en montagne , par exemple , il y a beaucoup de virages et de pentes .
si on veut que le train aille vite , il faut lui permettre d' aller tout droit et à plat .
alors on creuse des tunnels ou on construit des ponts .
les trains ne font pas seulement voyager des humains , ils transportent aussi des marchandises , comme des voitures , des céréales , du sable , des machines à laver ...
c' est ce qu' on appelle le fret .
quel est l' avenir du train ?
" tant qu' il y aura des villes , il y aura des chemins de fer , affirme l' historien Clive Lamming . le train a un avenir , c' est sûr et certain parce qu' il peut transporter beaucoup de monde . " quinze trains circulent chaque jour en France et transportent plus de quatre millions de voyageurs .
" sur une journée , le train transporte cinquante fois plus de personnes qu' un avion , remarque Clive Lamming . l' aéroport de Roissy ( en région parisienne ) , c' est deux cent cinquante voyageurs par jour . une gare parisienne , c' est entre quatre cents et sept cents voyageurs " .
le train est aussi meilleur pour l' environnement par rapport à d' autres moyens de transport .
quand un train de banlieue transporte un personnes , ça fait presque autant de voitures en moins sur les routes .
dans le monde , les habitants qui prennent le plus le train sont les suisses puis les japonais .
aux États Unis , les habitants ne voyagent pas en train , ils préfèrent l' avion et le bus .
en revanche , là bas , huit marchandises sur dix sont transportées par chemin de fer alors qu' en France , c' est environ un sur dix .
ici , on utilise surtout les camions .
à quoi pourrait ressembler le train dans le futur ?
certains projets sont impressionnants , comme l' Hyperloop , un train envoyé dans un tube qui ne toucherait plus le sol et qui irait à plus de un kilomètres par heure , plus vite que certains avions .
" ça reste des rêves d' ingénieur , des essais de laboratoire sans lendemain , estime Clive Lamming . peu de passagers pourront y rentrer alors que le train est un transport de masse . "
si ce projet un peu fou parait encore loin , il y aura en revanche bientôt des trains sans conducteurs , pilotés à distance .
en deux mille vingt deux , le RER E , utilisé par des voyageurs de la région parisienne , sera à moitié automatique .
si ça marche , il deviendra ensuite totalement automatique .