numéro cinquante et un trente mars au cinq avril deux mille dix huit
météo : un hiver perturbé
de la douceur en janvier , des températures glaciales avant le printemps , pas de soleil , beaucoup de pluie et de la neige : le temps est il détraqué en France ?
quel est le bilan de cet hiver qui nous a semblé interminable ?
pourquoi a t il fait si froid fin février ?
et comment travaille Météo France ?
dans ce nouveau numéro , je t' explique tout sur la météo et le climat , et tu verras que c' est très différent .
Morjane , dix ans , a bien profité de la neige
Morjane , dix ans , habite à Gigean , une petite ville située près de Montpellier .
pendant les vacances de février , elle n' a pas pu bouger de chez elle pendant près de trois jours parce qu' il y avait trente centimètres de neige , ce qui est très rare dans le sud de la France .
ses parents ne pouvaient pas prendre leur voiture parce que les routes n' avaient pas été déneigées .
ils n' ont pas pu aller au travail et son père a dû laisser sa voiture à l' entrée de la ville parce qu' il ne pouvait plus avancer .
pour les parents de Morjane , ce n' est donc pas un merveilleux souvenir , contrairement à leur fille : " c' était énorme , il y avait de la neige partout , raconte t elle . je n' avais jamais vu de neige chez moi . pour une fois , on n' a pas eu besoin de partir loin dans les montagnes . quand on y va , je suis malade en voiture parce que ça tourne . "
Morjane a beaucoup joué dehors avec son petit frère , Camil , sept ans , et leurs voisins .
ils ont évidemment fait des batailles de boules de neige et des bonshommes de neige .
" j' avais beaucoup de devoirs pendant les vacances mais quand il y a eu de la neige , je n' ai pas du tout travaillé " , se souvient elle .
le soir , toute la famille se retrouvait au chaud pour une soirée télé avec du chocolat chaud et de la chantilly .
" c' est l' un des plus beaux moments de ma vie ! " s' exclame Morjane .
Gigean , la commune où habite Morjane
quel est le bilan de cet hiver ?
pas de soleil , beaucoup de pluie , des tempêtes , de la neige mais aussi de la chaleur ...
l' hiver a été agité en France .
le printemps a démarré le vingt mars , et pourtant , on a encore grelotté .
les mois de décembre et janvier ont d' abord été très arrosés : il n' avait jamais autant plu en France depuis plus de cinquante ans .
de nombreux cours d' eau ont débordé , comme la Seine , la Marne et l' Yonne , ce qui a provoqué des inondations dans plusieurs régions françaises , en particulier en Île de France .
deux cent soixante quinze communes ont été classées en état de catastrophe naturelle .
cela veut dire que les gens dont la maison a été inondée , par exemple , peuvent recevoir de l' argent pour réparer les dégâts .
la France a aussi été touchée par des vents violents .
au début du mois de janvier , la tempête Eleanor a fait sept morts et de gros dégâts .
le soleil a été quasi absent en janvier .
à Rouen ( nord ouest de la France ) par exemple , le soleil n' a brillé que dix huit heures en un mois !
il n' y a pas eu beaucoup de soleil mais il a fait très doux .
c' est même le mois de janvier le plus chaud qu' on n' ait jamais connu depuis qu' on fait des relevés météo , donc depuis plus d' un siècle .
puis le froid est arrivé en février .
la neige est tombée plusieurs fois en montagne mais aussi en plaine au début et à la fin du mois .
il y a eu tellement de neige à Paris que certains ont même fait du ski ou du snowboard dans la capitale .
il y a aussi eu jusqu'à trente centimètres de neige en Corse , où les habitants n' avaient pas vu ça depuis plus de trente ans .
toute cette neige a provoqué plusieurs fois la pagaille sur les routes en région parisienne et dans le sud de la France , où des milliers d' automobilistes ont été bloqués .
des vols d' avion ont aussi été annulés et des trains ont été très retardés .
une vague de froid a de nouveau touché le pays à quelques jours du printemps et il a neigé dans le Sud Est , notamment à Marseille le vingt et un mars .
pourquoi a t il fait si froid ?
de la neige et des températures négatives au printemps , " ce n' est pas exceptionnel , remarque Frédéric Nathan , prévisionniste à Météo France . ça arrive juste moins souvent à cause du réchauffement climatique " .
nous avons donc l' impression que c' est exceptionnel parce que nous ne sommes plus habitués .
la dernière fois qu' il y a eu de grosses chutes de neige au printemps , c' était en mars deux mille treize, il y a cinq ans .
" notre mémoire se rappelle de saisons parfaites avec des mois d' été beaux et chauds et des hivers froids . mais ça n' existe pas ! rappelle Frédéric Nathan . il y a des variations de chaud et de froid à chaque saison . "
la vague de froid qui s' est abattue sur la France à la fin du mois de février , avec des températures allant jusqu'à dix degrés , s' appelle le Moscou Paris .
c' est une masse d' air très froide qui vient de Sibérie , l' une des régions les plus glaciales au monde , située dans le nord de la Russie , près du pôle Nord .
là bas , il fait souvent cinquante degrés en janvier !
en général , cet air froid n' arrive pas jusque chez nous .
mais là , il a réussi grâce à un anticyclone qui s' est formé dans l' atmosphère dans le nord de l' Europe .
un anticyclone se forme lorsque des mouvements dans l' atmosphère plaquent l' air au sol .
il crée un temps calme , qui peut se transformer en grisaille l' hiver .
comme un anticyclone tourne dans le sens des aiguilles d' une montre , il nous a donc ramené l' air froid de Sibérie .
l' inverse d' un anticyclone s' appelle une dépression : elle se forme lorsque des mouvements dans l' atmosphère aspirent l' air vers le haut .
elle crée un temps agité avec du vent , des nuages et donc de la pluie .
une dépression tourne dans le sens inverse des aiguilles d' une montre .
en France , le climat est tempéré , avec quatre saisons : l' automne , l' hiver , le printemps et l' été , en fonction de la position de la Terre par rapport au Soleil .
mais le temps varie aussi en fonction de la position des dépressions et des anticyclones .
comment peut on prévoir le temps ?
sur la Terre , il existe des milliers de stations météorologiques qui enregistrent tout ce qui se passe dans l' atmosphère .
en France , il y en a cinq cent cinquante quatre .
dans chaque station , des appareils enregistrent les variations de l' air comme l' hygromètre qui mesure l' humidité de l' air ou la girouette , qui indique la direction du vent .
au sol , il existe aussi des radars .
on en compte une vingtaine en France .
chaque radar couvre un rayon d' une centaine de kilomètres et permet de voir où il pleut , où il neige et à quelle intensité .
dans l' espace , des satellites prennent des photos du ciel pour voir le mouvement des nuages et calculer la direction et la vitesse des vents .
certains surveillent toujours la même zone sur des millions de kilomètres .
dans l' air , on envoie aussi des ballons avec des capteurs à l' intérieur pour mesurer la pression , l' humidité ou la force des vents .
enfin en mer , certains navires étudient les courants et le temps qu' il fait .
toutes ces informations qui viennent de la Terre , de l' espace et de la mer sont envoyées en temps réel à un énorme ordinateur de Météo France situé dans son gros centre à Toulouse .
avec toutes ces données , ce super ordinateur donne chaque jour un scénario du temps qu' il fera aux prévisionnistes de Météo France .
les prévisionnistes de Météo France travaillent dans huit centres répartis dans le pays .
les premiers arrivent à sept heures et regardent d' abord la météo du jour car " pour prévoir le temps qu' il fera demain , il faut déjà connaître le temps qu' il fait aujourd'hui " , explique Frédéric Nathan , prévisionniste à Météo France .
ensuite , ils étudient le scénario du gros ordinateur de Toulouse et le comparent à d' autres ordinateurs européens ou américains .
tout le monde se met ensuite d' accord lors d' une grande réunion qui se tient à neuf heures .
enfin , les prévisionnistes qui travaillent avec les médias leur font un résumé et leur envoient des cartes .
malgré tout , les prévisions ne sont pas toujours bonnes .
certains phénomènes , comme le risque d' orages , ou d' averses , ne sont pas toujours faciles à voir .
" on sait qu' il va y avoir des averses dans une zone mais on ne sait pas où exactement " , explique Frédéric Nathan , de Météo France .
c' est comme quand on fait bouillir de l' eau .
on sait que des bulles vont apparaître mais on ne sait pas à quel endroit .
météo et climat : quelle est la différence ?
quand il fait très froid quelque part , certains disent que c' est bien la preuve qu' il n' y a pas de réchauffement climatique .
d' autres expliquent aussi que si les scientifiques se trompent parfois sur le temps qu' il fera le lendemain , ils ne peuvent donc pas prévoir le temps qu' il fera dans cent ans .
sauf que la météo et le climat , ce n' est pas la même chose !
la météo surveille le temps qu' il fait au jour le jour et peut prévoir le temps sur dix jours maximum , donc sur une période courte .
à l' inverse , le climat se mesure sur des périodes très longues .
la météo , c' est comme une note à un contrôle tandis que le climat , c' est la moyenne d' une classe entière sur toute la scolarité , du primaire à l' université .
quand on dit que les températures ont été supérieures ou inférieures de cinq degrés par rapport aux normales de saison , ça veut dire par rapport à la moyenne des températures enregistrées au même endroit et à la même période au cours des trente dernières années .
c' est comme ça qu' on sait que le mois de janvier a été exceptionnellement doux avec trois degrés de plus que les températures normales à cette saison .
en février , il a fait très froid en France , mais " sur l' ensemble du globe , le mois de février est l' un des plus chauds jamais relevés " , explique Frédéric Nathan , prévisionniste à Météo France .
le pôle Nord a pendant ce temps connu un pic de chaleur avec des températures de trente degrés au dessus des normales de saison et la banquise n' a jamais eu un niveau aussi bas au mois de mars , soit depuis le début des mesures il y a plus de cinquante ans .
" il y aura toujours des périodes de froid intense mais , à cause du réchauffement climatique , il y a de moins en moins de records de froid et de plus en plus de records de chaud " , remarque Frédéric Nathan .
à cause du changement climatique , selon les scientifiques , la température moyenne sur Terre a grimpé de plus d' un degré en un siècle .
ça ne parait pas beaucoup comme ça , mais pour la planète c' est énorme .
évolution de la température moyenne sur Terre de dix neuf cent cinq à vingt cent quinze