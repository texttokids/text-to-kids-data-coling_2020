numéro cent seize vingt trois au vingt neuf août deux mille dix neuf
G sept : la diplomatie mondiale
du vingt quatre au vingt six août , il va y avoir beaucoup d' agitation dans la ville de Biarritz , dans le sud ouest de la France .
ce ne sera pas à cause des touristes mais d' une grande réunion qui regroupe les chefs de certains des pays les plus puissants de la planète .
ça s' appelle le G sept .
que va t il se passer ?
dans quelles autres circonstances les dirigeants mondiaux se rencontrent ils ?
c' est quoi la diplomatie ?
cette semaine , je t' emmène découvrir les coulisses du pouvoir !
Charles , dix ans , vit à Biarritz
Charles , dix ans , va assister à un drôle de spectacle depuis sa maison .
celle ci se trouve dans le centre ville de Biarritz , dans le sud ouest de la France , où vont se rassembler les chefs de sept grandes puissances mondiales du vingt quatre au vingt six août .
ça s' appelle le G sept .
un évènement auquel vont assister des milliers de personnes ( dirigeants , délégués , journalistes ...

pour assurer la sécurité de tout le monde , des mesures très strictes sont mises en place .
la ville va être divisée en deux zones où personne n' aura le droit de rentrer , sauf les habitants qui y vivent ou y travaillent .
Charles vit entre ces deux zones .
" mon père a récupéré à la mairie des badges qui montrent qu' on a le droit d' y rentrer , explique t il . moi , je n' en ai pas , je dois juste avoir ma carte d' identité sur moi et être avec mes parents . " dans les rues , il y aura aussi beaucoup de policiers et de gendarmes .
Charles a aperçu deux hélicoptères de la police et de l' armée dans le ciel .
" c' est fou , on ne voit pas ça tous les jours " , lance t il .
à quelques mètres de chez lui , il y a la plage principale de la ville , qui sera interdite d' accès pendant le G sept , tout comme la gare ou l' aéroport .
certains habitants ne sont pas contents de toutes ces complications qui vont perturber leur quotidien .
une partie des commerçants sont inquiets : ils craignent de perdre de l' argent parce que les touristes , très nombreux à Biarritz à cette période , ne pourront pas se balader dans certains endroits et aller dans leurs magasins .
Charles et sa famille sont plus confiants : " la vie continue " , assure Nicolas , son père .
le jeune garçon espère voir les voitures qui transportent les dirigeants passer dans sa rue .
" on n' a pas le droit de regarder avec des jumelles ou de prendre des photos , mais je garderai le souvenir dans ma tête " , confie t il .
Biarritz , la ville où habite Charles .
que se passe t il à Biarritz ?
des quartiers entiers fermés , des voitures interdites de circuler , treize deux cents policiers et gendarmes mobilisés : du vingt quatre au vingt six août , la ville de Biarritz , dans le sud ouest de la France , va être sous très haute sécurité .
la raison ?
elle va accueillir les présidents et les premiers ministres de sept grandes puissances mondiales .
ça s' appelle le G sept , pour " le groupe des sept secondes .
ces pays font partie d' une sorte de club parce qu' ils ont tous beaucoup de pouvoir et partagent des points communs .
" ils ont un niveau de développement semblable , des prises de position proches et ce sont des démocraties " , liste Delphine Allès , professeure en relations internationales à l' Institut national des langues et civilisations orientales ( Inalco ) .
ce sont des pays très riches , mais plus autant qu' à l' époque où le G sept a été créé , il y a plus de quarante ans .
aujourd'hui , il y en a d' autres qui sont aussi très puissants .
pour en tenir compte , depuis vingt cent huit , les dirigeants de vingt pays ( ceux du G sept mais aussi la Chine , l' Inde , le Brésil ou encore l' Afrique du Sud ...
) se rencontrent chaque année dans le cadre du G vingt .
mais les chefs des pays du G sept continuent aussi à se réunir pour discuter en plus petit comité de sujets très importants , comme le commerce ou le changement climatique .
cette fois ci , la France préside la rencontre et a choisi la ville de Biarritz .
sur place , des milliers de personnes sont attendues , comme des dirigeants d' autres pays ou de très nombreux journalistes .
le G sept est aussi beaucoup critiqué : il est perçu comme un petit club de pays riches qui imposent leur vision de la politique et de l' économie au reste du monde .
à chaque sommet , de nombreuses personnes manifestent parce qu' elles ne sont pas d' accord avec les politiques menées par ces pays .
" leur objectif est d' avoir toujours plus de richesses , ce qui provoque toujours plus d' inégalités entre les plus riches et les plus pauvres , et pollue la planète " , explique Jakes Bortayrou , membre du " G sept EZ ! " , un groupe opposé au G sept .
les opposants organisent des débats et des manifestations dans deux villes près de Biarritz , dont une grande manifestation le samedi vingt quatre août .
ils attendent environ douze participants .
où les chefs de chaque pays se rencontrent ils ?
chaque dirigeant gère son propre pays .
mais il n' est pas seul au monde .
il doit aussi rencontrer les chefs d' autres pays pour prendre des décisions sur plein de sujets , comme la résolution d' un conflit par exemple .
les visites
un chef d' Etat peut rencontrer un autre chef dans son pays .
il existe plusieurs types de visites .
la plus importante est la visite d' Etat .
" ça sert à montrer à un chef d' Etat qu' il est important , ou à se réconcilier avec lui si la relation était tendue " , indique Delphine Allès , professeure en relations internationales à l' Inalco , un institut qui enseigne les langues et les cultures des sociétés .
quand ça se passe en France , un service spécial , appelé le protocole , règle tout ce qu' il va se passer à la minute près , des mois à l' avance : comment le chef d' Etat va être reçu , qui il va rencontrer ...
des drapeaux de son pays sont accrochés sur les Champs Élysées , à Paris .
le soir , un grand diner a lieu à l' Elysée avec cent à deux cents invités .
chacun est placé selon un ordre très précis : plus on occupe un poste important , plus on est installé près du chef d' Etat .
" on accepte parfois les tenues traditionnelles ou des spectacles pendant le diner " , explique le service du protocole de l' Elysée .
des interprètes sont aussi présents pour traduire les discours et les conversations .
lors de leurs visites , les chefs d' Etat s' échangent aussi des cadeaux de toutes sortes : oeuvres d' art , meubles ou même animaux .
" pour choisir un bon cadeau , on demande quels sont les gouts de la personne qu' on reçoit pour ne pas tomber à côté " , assurent les membres du service du protocole .
les organisations internationales
des dirigeants se regroupent pour former des organisations internationales .
ce sont des sortes de clubs dont les pays membres travaillent ensemble pour développer l' économie ou assurer la sécurité .
par exemple , l' organisation des Nations unies ( ONU ) rassemble cent quatre vingt treize pays , soit presque tous les pays du monde .
elle a été créée en mille neuf cent quarante cinq , pour maintenir la paix .
les sommets
les chefs des pays peuvent se retrouver à plusieurs pendant de grandes réunions qu' on appelle des sommets .
c' est le cas par exemple du G sept .
ils peuvent y discuter de sujets divers ou d' un thème précis , comme l' environnement .
comment les chefs d' Etat communiquent ils ?
quand les chefs des différents pays se rencontrent , ils font des discours devant les caméras .
mais ils discutent aussi entre eux plus discrètement .
ils peuvent aussi s' appeler ou s' envoyer des messages .
ce qu' ils se disent doit rester confidentiel , pour éviter que des espions d' un autre pays écoutent leurs conversations .
celles ci sont donc cryptées : seul celui qui envoie le message et celui qui le reçoit peuvent le lire .
quand deux chefs d' Etat se téléphonent , ce n' est pas vraiment comme quand deux amis s' appellent .
" leurs conseillers sont présents et ils préparent les réponses à donner " , précise Delphine Allès , professeure en relations internationales à l' Inalco .
mais les dirigeants des pays ont des relations plus détendues qu' avant : " ils se connaissent bien , ils se tutoient , raconte Thomas Gomart , directeur de l' Institut français des relations internationales ( Ifri ) . ils peuvent aussi s' échanger leur numéro de téléphone personnel . "
aujourd'hui , les chefs d' Etat utilisent de plus en plus les réseaux sociaux , et plus particulièrement Twitter .
le président des États Unis , Donald Trump , a bouleversé la manière dont un chef d' Etat utilise Twitter parce qu' il poste tous les jours des messages dans lesquels il réagit très vite à ce qu' il entend et écrit tout ce qu' il pense .
parfois , il n' hésite pas à insulter des gens , comme le dictateur nord coréen Kim Jong un qu' il a traité de " fou " et de " petit gros " en vingt cent dix sept .
les réseaux sociaux peuvent également permettre aux citoyens ou aux dirigeants de pays moins connus dans le monde de s' adresser directement aux chefs plus puissants .
par exemple , en vingt cent dix sept , la présidente de la République des îles Marshall ( un pays dans l' océan Pacifique ) a demandé sur Twitter à Donald Trump de ne pas quitter un accord important pour lutter contre le réchauffement climatique .
le " téléphone rouge " cette expression ne désigne pas en réalité un vrai téléphone rouge .
c' est le nom donné à un système qui permet à deux pays en conflit de communiquer directement et rapidement .
la couleur rouge symbolise l' urgence des messages .
le premier " téléphone rouge " a été installé en dix neuf cent soixante trois entre les États Unis et l' URSS ( formée en partie par l' actuelle Russie ) , qui étaient alors ennemis .
c' était une grosse machine pour envoyer des messages écrits .
c' est quoi la diplomatie ?
on dit souvent aux enfants que si on a un problème avec quelqu' un , il vaut mieux en parler avec lui que se battre .
pour les dirigeants des pays , c' est la même chose .
plus ils discutent et ont de bonnes relations , moins il y a de guerres dans le monde .
c' est ce qu' on appelle la diplomatie .
ce n' est pas facile et ça ne fonctionne pas toujours parce que les dirigeants ne sont pas toujours d' accord entre eux .
ils doivent donc négocier : chacun explique et défend son point de vue tout en essayant de trouver ensemble un accord .
pour ça , plein de personnes , appelées des diplomates , travaillent pour eux .
voici deux exemples .
les sherpas
au départ , les Sherpas sont des guides qui aident les alpinistes à gravir les montagnes de l' Himalaya .
c' est aussi le surnom donné aux conseillers des dirigeants qui préparent les sommets du G sept .
" on retrouve l' idée du sommet : pas celui d' une montagne , mais une grande réunion , explique Philippe Étienne , sherpa français pour le G sept de Biarritz . le sherpa aide le chef de son pays et fait avancer les dossiers . "
" il faut avoir de la persévérance et savoir créer une bonne entente entre les conseillers , assure Philippe Étienne . on doit être très proche du président qu' on accompagne . on porte des idées qui ne sont pas les nôtres mais celles de notre président , pour notre pays . "
l' ambassadeur ou l' ambassadrice représente son pays et le défend dans un pays étranger , où il vit et travaille .
il doit négocier avec les dirigeants de ce pays et très bien savoir ce qu' il s' y passe : comment le territoire est gouverné , qui sont les opposants , comment vivent les citoyens ...
il doit donc tisser des liens avec plein de gens .
" ça se fait petit à petit , en organisant des diners ou en visitant le pays " , explique Patrick Nicoloso , ancien ambassadeur de la France dans plusieurs pays , notamment d' Afrique .
pour négocier , " ça se fait souvent dans les couloirs , en dehors des réunions , assure Patrick Nicoloso . il faut avoir beaucoup de patience parce que c' est un travail lent et compliqué qui prend parfois des dizaines d' années . " Laurent Stéfanini , ambassadeur de la France à l' Unesco ( une agence rattachée à l' ONU ) , ajoute : " pour défendre ses idées , il faut être fort et déterminé , tout en faisant preuve d' une grande politesse . "