numéro vingt neuf vingt sept octobre au deux novembre deux mille dix sept
les dangers du glyphosate
en ce moment , on parle beaucoup du glyphosate .
ce mot un peu compliqué est le nom d' un produit qui permet d' enlever rapidement et facilement les " mauvaises herbes " .
on appelle aussi ça du désherbant .
beaucoup de gens l' utilisent pour jardiner et des agriculteurs s' en servent pour s' occuper de leurs champs .
jean marc , agriculteur , a utilisé du glyphosate
jean marc , cinquante cinq ans , est agriculteur en Bretagne .
il élève des vaches et vend leur lait .
pendant sept ans , il a utilisé du glyphosate pour s' occuper de ses champs .
il faisait pousser de l' herbe , des céréales ou encore du colza pour que ses bêtes puissent manger .
mais il y avait aussi beaucoup de mauvaises herbes , et Jean Marc voulait s' en débarrasser .
donc il les tuait avec des produits contenant du glyphosate .
" je savais que je prenais des risques . il y avait des têtes de mort sur tous les bidons " , raconte Jean Marc .
ce symbole permet en effet d' indiquer qu' un produit est dangereux .
" c' était un véritable stress d' utiliser des produits chimiques . il fallait mettre un masque , des gants ... " précise t il .
" j' avais l' impression que c' était utile pour nourrir mes vaches et que je ne pouvais pas faire autrement " , se souvient l' éleveur .
il se disait qu' en ayant de quoi nourrir beaucoup de bêtes , il produirait davantage de lait et qu' il pourrait ainsi gagner plus d' argent pour vivre .
" l' agriculture industrielle dit que pour nourrir le monde , il faut produire beaucoup , et donc lever tous les obstacles , comme les mauvaises herbes , pour avoir plus de lait " , note t il .
il n' est pas du tout d' accord avec cette façon de voir les choses .
en plus , " le lait que je vendais était certainement contaminé ( quand j' utilisais du glyphosate ) " , reconnait l' éleveur .
il y a dix ans , il a décidé d' arrêter d' utiliser ce produit .
" j' ai eu envie d' offrir à mes enfants un monde moins dangereux . " désormais , il désherbe ses champs avec des machines , dont les grosses roues accrochent les mauvaises herbes et les arrachent .
mais d' autres paysans continuent à utiliser des produits chimiques .
" on est en train de détruire l' environnement dans lequel on est " , regrette Jean Marc .
Rostrenen , la ville où Jean Marc habite et travaille
pourquoi parle t on du glyphosate ?
mise à jour Le vingt sept novembre , l' Union européenne a décidé d' autoriser le glyphosate pour cinq années supplémentaires .
le président Emmanuel Macron a annoncé qu' il voulait que ce désherbant soit interdit en France dans les trois ans qui viennent .
des représentants des pays de l' Union européenne doivent se réunir dans les semaines qui viennent pour parler du glyphosate .
cet herbicide est autorisé en Europe , mais seulement jusqu'à la fin de l' année .
les pays européens doivent donc dire si on peut continuer à l' autoriser ou s' il faut l' interdire .
le gouvernement français souhaite qu' on empêche son utilisation , mais pas tout de suite , afin de laisser le temps aux agriculteurs de trouver une autre façon de travailler .
la question se pose parce que le glyphosate est soupçonné d' être dangereux pour la santé et pour l' environnement .
d' ailleurs , cinquante quatre députés français ont récemment demandé qu' il soit interdit et une pétition a été signée par plus de un , trois million d' Européens contre ce produit .
une famille française a par ailleurs décidé de porter plainte contre des entreprises qui en fabriquent .
leur fils , Theo , a dix ans .
certaines parties de son corps ne se sont pas formées correctement quand il était dans le ventre de sa mère .
aujourd'hui , il a des difficultés pour respirer et parler .
il a dû être opéré cinquante deux fois .
sa maman pense que c' est parce qu' elle a désherbé avec du glyphosate quand elle était enceinte et que le produit qu' elle a respiré a fait du mal à Theo .
plusieurs études ont montré qu' on avait tous du glyphosate dans notre corps , car ce produit est partout : dans les aliments qu' on mange , dans les sols , dans l' air , dans l' eau ...
comment fonctionne le glyphosate ?
le glyphosate est un produit chimique qui tue les plantes .
" normalement , les agriculteurs utilisent différents types de désherbants pour tuer les différents types de mauvaises herbes . mais le glyphosate est capable de tuer toutes sortes de mauvaises herbes " , explique Laure Mamy , chercheuse à l' Institut national de la recherche agronomique ( Inra ) .
il est aussi et surtout utilisé par des gens dans leur jardin .
les agriculteurs et les personnes qui ont un potager veulent se débarrasser des mauvaises herbes car , pour pouvoir pousser , elles prennent les aliments du sol et ça en fait moins pour les cultures .
dans les jardins , certains les enlèvent parce que ça ne fait pas très joli au milieu des fleurs .
le glyphosate est le désherbant le plus acheté dans le monde , parce qu' il est facile à utiliser , très efficace et ne coute pas très cher .
on le trouve dans de nombreuses marques d' herbicides .
le plus connu s' appelle le Roundup .
il est vendu dans des magasins de jardinage .
ce produit est tellement efficace qu' il tue toutes les plantes sur son passage , et pas seulement les mauvaises herbes .
" si on le met dans un champ de blé , il tue le blé " , illustre Laure Mamy .
pour éviter ça , les agriculteurs français ne l' utilisent pas n' importe quand : ils en mettent sur leur champ après la récolte , avant de semer de nouvelles graines .
c' est durant cette période que les mauvaises herbes s' installent .
en Argentine et aux États Unis , les paysans peuvent s' en servir quand ils veulent car ils ont le droit d' utiliser des organismes génétiquement modifiés ( OGM ) .
il s' agit de plantes qu' on a modifiées dans des laboratoires pour leur permettre de résister au glyphosate .
comme ça , le produit ne tue que les mauvaises herbes , pas les bonnes !
mais à force , des mauvaises herbes sont devenues résistantes elles aussi ...
en France , les OGM sont interdits car on se demande si ce n' est pas mauvais pour la santé et pour l' environnement .
est ce vraiment dangereux ?
cette question semble simple , et pourtant c' est compliqué d' y voir clair .
parce que certaines personnes ont intérêt à cacher la vérité .
en deux mille quinze , il y a deux ans , des experts très sérieux du Centre international de recherche sur le cancer ont rendu publics les résultats de leur étude sur ce désherbant .
ils ont montré que le glyphosate pouvait être la cause de cancers .
d' ailleurs , aux États Unis , trois cinq cents personnes ont porté plainte contre Monsanto car elles pensent que leur cancer ou celui de leurs proches est dû à ce produit .
Monsanto est le nom de l' entreprise américaine qui fabrique le Roundup , le plus connu des désherbants contenant du glyphosate .
si les gens découvrent que le glyphosate est dangereux , ils ne voudront plus en acheter et les gouvernements pourraient même décider de l' interdire dans leur pays .
Monsanto vivrait ça comme une catastrophe car elle perdrait des millions d' euros !
alors l' entreprise a tout fait pour dire au monde que l' étude était fausse , qu' elle avait été mal faite .
et ce n' était pas la première fois qu' elle faisait quelque chose comme ça .
depuis des années , des études montrent que le glyphosate provoque des problèmes de santé .
et depuis des années , Monsanto essaie de convaincre les gens que son désherbant n' est pas dangereux .
au point de faire des choses pas très honnêtes ...
l' entreprise a par exemple écrit elle même des études scientifiques montrant que le glyphosate n' était pas dangereux .
puis elle a payé des scientifiques extérieurs pour qu' ils fassent croire que c' était leur propre travail .
" cacher sous le tapis " veut dire " faire en sorte que personne ne voie " .
à chaque fois qu' une étude sort et dit que cet herbicide est mauvais pour l' environnement ou pour la santé , Monsanto fait tout pour intimider les chercheurs , les forcer à retirer leur étude , convaincre les responsables politiques que c' est faux ...
l' objectif est que les gens oublient vite cette mauvaise publicité et continuent à acheter du désherbant .
comment jardiner sans produits chimiques ?
les produits chimiques fabriqués par les êtres humains sont bien pratiques pour désherber , tuer les bêtes qui mangent les plantes ou aider les plantes à pousser .
mais ce n' est bon ni pour la santé ni pour l' environnement , donc autant faire autrement .
Ghislaine Deniau est guide animatrice à Terre vivante , une entreprise qui défend la nature .
elle a écrit le livre les Enfants !
vous venez jardiner ?
et donne ses conseils pour s' occuper des plantes de façon naturelle .
l' idée est de reproduire ce qui se passe dans la nature en protégeant la vie dans le sol et en s' aidant des petites bêtes .
la nature va faire le travail du jardinier .
on couvre le sol un maximum de temps dans l' année , parce que ça empêche les herbes de pousser .
on peut utiliser des feuilles mortes , qu' on met tout autour des cultures .
en plus , quand elles se décomposent , elles nourrissent les vers de terre , les cloportes , les mille pattes ...
ensuite , ils font des excréments , qui permettent aux plantes de bien pousser .
en général , on utilise le compost .
c' est un engrais fabriqué avec des déchets naturels ( épluchures de banane , trognons de pomme , feuilles de salades abimées , feuilles mortes , branches ...
) qui sont décomposés grâce à des petites bêtes ( vers , champignons , bactéries , mille pattes ) .
il est très bon pour les plantes , pas cher , et il permet aussi de moins polluer en diminuant la quantité de déchets mis à la poubelle .
il y a des petites bêtes qui peuvent manger les bestioles qui nous gênent .
si on arrive à attirer un hérisson dans son jardin , il peut manger les limaces ou les escargots .
on peut installer des nichoirs pour les mésanges , qui vont manger les chenilles .
si on a la place , on peut installer une petite mare pour les libellules , qui vont manger les mouches s' attaquant aux carottes .