numéro cinquante vingt trois au vingt neuf mars deux mille dix huit
les armes à feu aux États Unis
samedi vingt quatre mars a lieu une grande marche aux États Unis pour demander le contrôle des armes à feu .
là bas , la loi permet d' avoir une arme beaucoup plus facilement qu' en France ou dans d' autres pays .
le problème , c' est que les gens , parfois très jeunes , qui veulent en tuer d' autres arrivent aussi à trouver des armes .
au mois de février , un adolescent est entré dans son ancien lycée et a tué dix sept personnes .
cet évènement a choqué beaucoup d' Américains .
Izaiah , dix sept ans , vit aux États Unis
Izaiah habite à Durham , en Caroline du Nord , un État des États Unis .
il est en première année de lycée .
dans son pays , on a le droit d' avoir une arme pour se défendre , contrairement à la France .
Izaiah connait des gens qui en ont chez eux .
dans certains endroits comme les supermarchés , il y a des détecteurs pour vérifier qu' on n' entre pas avec une arme .
dans plusieurs écoles du pays , il y a déjà eu des tueries .
c' est à dire que quelqu' un est entré avec une arme à feu et a blessé ou tué au moins quatre personnes .
il y en a eu une en Floride au mois de février , dans un lycée de la ville de Parkland .
" c' est fou de se dire que l' école peut être dangereuse ! " S exclame Izaiah .
pour le garçon , ce sujet est très important .
il en parle souvent avec ses amis , sa famille et ses professeurs .
quand il avait treize ans , Izaiah avait très peur que quelqu' un vienne avec une arme dans son école .
" j' en ai parlé à ma famille et ils m' ont rassuré , se souvient il . ça m' a vraiment aidé . "
les américains ne sont pas tous d' accord avec les règles qui autorisent les armes .
" les élèves de mon lycée mettent en place des groupes pour protester . ils organisent des manifestations et parlent beaucoup sur les réseaux sociaux , comme Facebook , Instagram et Twitter " , raconte le garçon .
Izaiah a l' impression que les jeunes souhaitent plus de règles sur les armes que leurs parents .
" on en a marre d' avoir peur . on nous dit toujours que la catastrophe n' aurait pas pu être évitée , mais ça n' a pas l' air vrai , ajoute t il . je serais plus heureux s' il y avait plus de lois de contrôle . "
Durham , la ville où habite Izaiah
pourquoi une marche est elle organisée ?
samedi vingt quatre mars , une grande manifestation a lieu aux États Unis , en particulier à Washington , la capitale .
des gens ont prévu de se rassembler et de marcher ensemble pour demander un plus grand contrôle des armes à feu ( fusils , pistolets , mitraillettes ...

en France , seules les personnes qui en ont besoin pour leur travail et les chasseurs peuvent avoir une arme .
mais aux États Unis , beaucoup plus de gens sont autorisés à en avoir , pour se défendre .
cette marche est organisée par des lycéens de la ville de Parkland , en Floride .
le quatorze février dernier , le jour de la Saint Valentin , un garçon de dix neuf ans est entré dans leur lycée avec une arme à feu .
cet ancien élève de l' école a tiré sur des lycéens et des professeurs .
dix sept personnes sont mortes .
depuis la tuerie , les survivants prennent souvent la parole en public ou dans les médias .
ces lycéens veulent que les responsables politiques modifient les lois pour qu' il soit plus difficile d' avoir une arme .
la manifestation organisée par les lycéens s' appelle la " marche pour nos vies " .
des célébrités ont dit qu' elles y participeraient , pour montrer qu' elles sont d' accord avec les idées défendues par les adolescents .
l' acteur George Clooney , l' animatrice de télévision Oprah Winfrey ou encore le réalisateur de films Steven Spielberg ont tous donné plus de quatre cents euros pour soutenir les lycéens .
des manifestations sont aussi organisées dans d' autres pays , par exemple en France , pour demander à changer les lois américaines .
aux États Unis , environ cent dix sept personnes sont touchées par des balles chaque année .
parmi elles , quinze sont tuées .
c' est beaucoup plus que dans les autres pays .
mercredi quatorze mars , un mois exactement après la tuerie , des milliers de lycéens se sont rassemblés dans tout le pays dans le cadre de la " marche nationale des écoles " , pour rendre hommage aux victimes et demander au gouvernement de mieux contrôler les armes les plus dangereuses .
sept mille paires de chaussures ont été posées à Washington , près du Capitole , où travaillent les députés américains , et près de la Maison Blanche , où habite le président .
elles symbolisent les sept enfants tués par des armes à feu depuis cinq ans .
que dit la loi ?
le tireur de Floride avait dix neuf ans .
à cet âge , il n' avait pas le droit de boire de l' alcool dans un bar , ni d' acheter une arme .
mais il avait le droit d' en avoir une ( par exemple si c' était un cadeau ) .
aux États Unis , les lois sont différentes dans chaque État .
en général , pour avoir le droit de garder une arme sur soi , il faut un permis de port d' arme .
comme un permis de conduire , ce papier prouve qu' on sait se servir de cette arme et qu' on connait la loi .
mais tous les États ne donnent pas ce droit à tout le monde .
par exemple , dans la ville de New York , située dans l' Etat qui s' appelle aussi New York , il est très difficile d' avoir ce permis .
à Parkland , en Floride , c' est plus facile .
l' âge minimum pour avoir une arme aux États Unis n' est pas le même d' un État à l' autre
ce n' est pas parce qu' on a un permis qu' on peut utiliser son arme .
la loi précise qu' on a le droit de tirer uniquement pour se défendre .
en Floride , si on montre son arme en public sans raison , on risque jusqu'à trois ans de prison .
si on tire , même en l' air ou au sol , la peine peut aller jusqu'à vingt ans .
en Floride , pour acheter une arme , il faut :
depuis la tuerie en Floride le quatorze février , la loi a été un peu modifiée .
dans cet État , on n' a plus le droit d' acheter aucune arme avant vingt et un ans ( avant , c' était dix huit ans pour certaines ) , et il faut désormais attendre pendant quelques jours avant de la recevoir , pour réfléchir à ce que l' on fait .
désormais , les adultes ( professeurs , directeurs , surveillants ) ont le droit de porter des armes dans les écoles .
des programmes seront mis en place dans les écoles pour surveiller les élèves qui souffrent de maladies mentales ( qui empêchent de penser clairement ) .
pourquoi les armes sont elles aussi importantes aux États Unis ?
les États Unis sont un pays où beaucoup d' armes circulent .
on compte environ quatre vingt cinq armes pour cent habitants .
en France , c' est vingt huit armes pour cent habitants .
c' est aussi le pays occidental où le nombre de personnes tuées par balles est le plus élevé .
mais ceux qui défendent les armes disent : " ce ne sont pas les armes qui tuent , ce sont les gens . "
dans ce pays d' Amérique , porter une arme fait partie de la vie de tous les jours .
" les lycéens de Floride grandissent dans un État où ils voient des armes depuis toujours , beaucoup en ont à la maison ou dans leur famille . dans deux ou trois ans , ils seront peut être de grands défenseurs des armes " , estime Jean Éric Branaa , professeur à l' université Paris deux un .
aux États Unis , le droit d' avoir une arme est écrit dans la loi depuis plus de deux cents ans .
c' est même la deuxième des dix règles les plus importantes du pays , on l' appelle le deuxième amendement .
cette loi de mille sept cent quatre vingt onze dit qu' un groupe armé bien organisé est obligatoire pour que le pays soit libre et sécurisé .
elle précise que le droit d' avoir des armes sera toujours respecté .
cette loi a été écrite juste après la Guerre d' indépendance .
avant cette guerre , les États Unis appartenaient à l' Angleterre .
les américains ont pris les armes pour se battre contre le gouvernement anglais et devenir un pays libre .
après leur victoire , ils ont fait des lois .
" les américains qui ont écrit ces lois estimaient que le droit de posséder une arme assurait leur liberté , car il ne fallait pas qu' un gouvernement puisse tout contrôler et empêcher les habitants de penser par eux mêmes " , précise le chercheur Jean Éric Branaa .
et puis quand les cow boys sont arrivés , au XIXe ( dix neuvième ) siècle , ils défendaient leurs fermes et leurs familles contre les animaux sauvages , les bandits , les indiens et les autres cow boys .
ils faisaient tout ça avec des armes à feu .
certains américains pensent aussi qu' ils doivent faire le travail à la place de la police , surtout quand elle ne le fait pas bien .
par exemple si certains policiers sont racistes .
encore aujourd'hui , l' idée est de pouvoir se défendre soi même dans la nature sauvage ou un quartier dangereux .
pourquoi ça ne change pas ?
entre vingt cent onze et vingt cent seize , cent lois pour mieux contrôler les armes ont été proposées aux États Unis , mais aucune n' a été acceptée par les élus qui votent les lois .
l' ancien président Barack Obama a essayé de changer les choses , mais il n' a pas réussi .
le président actuel , Donald Trump , lui , aime bien les armes .
il pense par exemple qu' il faut en donner aux professeurs dans tout le pays , pour qu' ils puissent se défendre si quelqu' un veut attaquer une école .
le douze mars , il a écrit sur Twitter : " si les écoles sont des zones sans armes , la violence et le danger sont invités à y entrer . "
les partis politiques s' opposent beaucoup sur la question des armes .
les Républicains , le parti de Donald Trump , sont nombreux à être pour les armes .
les Démocrates , le parti de Barack Obama et d' Hillary Clinton , sont souvent opposés aux armes .
mais " personne ne cherche à interdire les armes aux États Unis . ils veulent juste enlever les plus dangereuses " , assure Jean Éric Branaa , professeur à Paris deux un .
les armes à feu font partie de la vie des américains et de l' identité du pays .
s' il est aussi difficile de changer les lois sur les armes , c' est en grande partie à cause d' un groupe appelé la NRA ( National Rifle Association en anglais , ce qui veut dire " association nationale des armes à feu " ) .
ce groupe défend les armes .
il existe depuis près de cent cinquante ans et a un pouvoir énorme dans le pays .
plus il y a d' armes , plus la NRA gagne de dollars , car ceux qui ont des armes la soutiennent .
l' association compte cinq millions d' adhérents : c' est plus que les habitants de l' Irlande !
la NRA donne de grosses sommes d' argent à des personnalités politiques et des entreprises .
en échange , ces personnes doivent parler des armes de façon positive .
" la NRA est très puissante , très active et très organisée , explique Jean Éric Branaa . aujourd'hui , pour un élu politique , résister à la NRA , c' est pratiquement perdre son élection . " par exemple , la NRA a donné vingt quatre millions d' euros à Donald Trump pendant la campagne présidentielle .
l' association a aussi plusieurs magazines et une chaîne de télévision .
elle donne une note à chaque personne élue dans le pays pour dire si elle défend bien les armes ou pas , pour que les américains qui aiment les armes sachent pour qui voter .