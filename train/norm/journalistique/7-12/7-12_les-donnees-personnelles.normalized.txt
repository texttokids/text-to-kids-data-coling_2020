numéro cinquante deux six au douze avril deux mille dix huit
les données personnelles
le réseau social Facebook est très critiqué en ce moment .
on l' accuse de ne pas protéger assez les données personnelles de ses utilisateurs .
les données personnelles , ce sont des informations qui permettent de savoir qui on est : notre nom , notre prénom , notre date de naissance , notre numéro de téléphone , des photos de nous ...
en réalité , sur Internet , beaucoup de sites et d' applications gardent nos données personnelles .
parfois , on se rend compte qu' on leur donne des informations sur nous , parfois pas vraiment .
des élèves de sixième parlent de données personnelles
le P' tit Libé a discuté avec des élèves de la six point D du collège Pierre Perret de Bernes sur Oise ( en région parisienne ) des données personnelles qu' ils laissent sur les sites Internet et sur les réseaux sociaux .
Azizet : c' est notre nom , notre prénom , notre numéro de téléphone , notre adresse , nos contacts , tout ça .
Azizet : moi j' ai Facebook , Snapchat et Instagram .
ça me sert à communiquer quand j' ai pas de crédit .
Naïla : c' est mieux d' envoyer des messages normaux , parce que sur Snapchat , Instagram , des fois , il y a des pirateurs ( remarque du P' tit Libé : on dit " pirates " ) .
ça peut être dangereux .
Azizet : j' ai vu une méthode sur Internet : il faut changer son mot de passe toutes les deux semaines , comme ça si quelqu' un a votre mot de passe , le nouveau il l' aura plus .
Naïla : sur Instagram , il faut pas se mettre en compte public , parce qu' il y a des personnes qui peuvent voir vos photos et qui peuvent pirater .
Shana : sur les réseaux sociaux , il faut faire attention .
il faut être prudent , ne pas publier n' importe quoi , ne pas mettre des trucs trop privés .
sur Snapchat , j' ai que des gens que je connais .
quand des gens que je connais pas m' ajoutent , je refuse .
il faut éviter de mettre le moyen de localisation sinon tout le monde peut trouver où on est
Azizet : quand vous installez des jeux sur le téléphone , ça met " autorisez vous l' accès à vos contacts , votre appareil photo " et tout .
quand c' est une application que j' adore trop et dont j' entends beaucoup parler , je mets " accepter " .
jolly gaby : je ne m' en fiche pas que les sites aient des vraies données de nous !
après ils peuvent faire n' importe quoi avec .
c' est pour mieux nous connaître et pour mettre des pubs qui sont pour nous .
bernes sur oise , la ville où ces élèves vont au collège
de quoi Facebook est il accusé ?
fin mars , des journalistes ont révélé une information qui a choqué beaucoup de gens : les données personnelles de nombreux utilisateurs de Facebook auraient été utilisées pour aider Donald Trump à devenir président des États Unis .
sur ce réseau social , on peut écrire des messages , partager des photos , des vidéos ou des liens vers des sites Internet .
on peut aussi faire des jeux ou des quiz , par exemple .
mais ces jeux ou quiz ne sont pas créés par Facebook , ils viennent d' autres entreprises .
pendant plusieurs mois , de très nombreuses personnes ont répondu à un test de personnalité sur Facebook .
il fallait dire si on était bavard ou intéressé par l' art , par exemple .
une entreprise a récupéré toutes les réponses , mais aussi les données personnelles des gens qui ont répondu et celles de leurs " amis " sur Facebook , comme leur nom ou les sujets qu' ils aiment bien ...
cette entreprise s' appelle Cambridge Analytica .
grâce à toutes ces informations , elle a pu avoir une idée des opinions et des gouts des gens .
on l' accuse d' avoir utilisé ces informations pour créer des publicités différentes selon les gouts des utilisateurs de Facebook .
c' est comme si P' tit Libé aimait les chiens et voyait tout à coup des pubs pour des croquettes sur Facebook .
les entreprises font souvent des pubs sur les réseaux sociaux en fonction des gouts des gens .
le problème , c' est que Cambridge Analytica est accusée d' avoir créé et envoyé des pubs pour aider Donald Trump à gagner l' élection présidentielle américaine .
certaines personnes se demandent aujourd'hui si ces pubs n' ont pas donné envie à des internautes de voter pour Donald Trump alors qu' ils n' avaient pas prévu de le faire au départ .
même si Facebook n' a pas créé le jeu , on lui reproche d' avoir autorisé le créateur du test de personnalité à récupérer les données des internautes et celles de leurs " amis " .
ça fait longtemps que Facebook est accusé de ne pas respecter les données personnelles des internautes .
où se trouvent nos données personnelles ?
quand on parle de données personnelles , on parle des informations qui permettent de savoir qui on est : notre prénom , notre date de naissance , notre numéro de téléphone , des photos de nous ...
on ne se rend pas toujours compte de toutes les informations qu' on partage sur Internet .
imaginons la vie d' une petite fille de onze ans .
appelons la Inès .
depuis sa naissance , ses parents publient souvent des photos et des vidéos d' elle sur leur compte Facebook .
depuis onze ans donc , de nombreuses images d' Inès existent sur Internet , sans qu' elle ait donné son avis .
maintenant qu' elle est grande , elle va toute seule sur Internet .
elle n' a pas encore l' âge ( il faut avoir au moins treize ans ) , mais elle s' est créé une adresse mail sur Gmail .
elle a alors dû donner son prénom et son nom , puis on lui a demandé son numéro de téléphone et sa date de naissance .
elle a donné toutes ces informations à Google , l' entreprise qui s' occupe de Gmail .
Inès a un smartphone .
elle télécharge parfois des applications pour jouer à des jeux gratuits .
quand elle a installé Minion Rush , elle a autorisé l' application à accéder à ses photos , à ses vidéos et aux fichiers qui sont sur son téléphone .
quand elle va sur des sites , Inès voit des messages disant " en poursuivant votre navigation , vous acceptez l' utilisation des cookies " .
elle accepte toujours , sans savoir que les cookies sont des petits fichiers que les sites Internet laissent dans l' ordinateur et qui retiennent ce qu' on est allé voir en ligne .
elle n' a pas l' âge non plus ( il faut aussi avoir au moins treize ans ) , mais Inès adore utiliser Snapchat .
sur le réseau social , elle envoie plein de photos et de vidéos d' elle à ses copines .
en attendant que ses copines ouvrent ses messages , Snapchat conserve les photos d' Inès .
un jour , sa copine Manon lui a montré un truc marrant : il est possible de voir sur une carte tous les endroits où Inès est allée .
c' est normal , elle n' avait pas désactivé la fonction de Google qui enregistre sa position : dès qu' elle se déplace avec son téléphone , tous ses trajets sont donc enregistrés .
Inès se rend compte qu' il y a beaucoup d' informations sur elle en ligne .
elle se demande si ça lui plairait , quand elle aura dix sept ans , de revoir des vidéos d' elle à onze ans en train de danser sur le réseau social Musical .
ly .
ou pire , que quelqu' un d' autre tombe dessus ...
la honte !
à quoi servent nos données personnelles ?
les sites Internet et applications pour téléphones et tablettes sont obligés d' expliquer quelles données personnelles ils récupèrent et à quoi elles leur servent .
google précise , par exemple , que le fait d' avoir accès à des informations sur nous permet de nous rendre mieux service .
quand on lui donne accès à l' endroit où on se trouve , ça lui permet d' améliorer les résultats de recherche .
si je suis à Paris et que je tape " restaurant chinois " sur Google , c' est quand même plus pratique d' avoir des propositions de restaurants chinois à Paris , et pas à Bordeaux ou à Lyon .
mais les données personnelles , ça ne sert pas qu' à ça .
une phrase célèbre résume bien les choses : " si c' est gratuit , c' est toi le produit . "
pour comprendre , prenons l' exemple de Facebook , qui possède aussi le réseau social Instagram et la messagerie WhatsApp .
l' an dernier , l' entreprise a gagné plusieurs milliards d' euros .
pourtant , on utilise Facebook , Instagram et WhatsApp sans dépenser un seul centime .
alors comment l' entreprise gagne t elle de l' argent ?
grâce aux données personnelles de ses utilisateurs !
avec les informations qu' on donne à Instagram , par exemple , le réseau social nous connait un peu .
il a les informations qu' on communique directement ( nom et sexe ) , mais il regarde aussi quels types de photos on publie , quels types de photos on aime , on commente .
mais aussi si on passe plus de temps sur telle image ou sur telle autre ...
grâce à toutes ces informations , Instagram vend des espaces de publicité .
par exemple , le P' tit Libé lui donne de l' argent pour que des pubs pour le P' tit Libé s' affichent chez les gens qui ont des enfants entre sept et douze ans .
c' est beaucoup plus intéressant que de payer pour que les pubs s' affichent chez n' importe qui , même des gens qui n' ont pas d' enfants ...
là , au moins , le P' tit Libé espère que des utilisateurs d' Instagram abonneront leurs enfants .
comment protéger nos données personnelles ?
on peut se dire qu' on s' en fiche qu' Instagram sache qu' on aime les gâteaux au chocolat et permette à des entreprises de nous proposer des pubs pour des gâteaux au chocolat .
mais on ne sait pas toujours ce que deviennent nos données personnelles , qui les utilise et pour faire quoi .
voici quelques conseils pour protéger ses données personnelles ( attention , quand on est enfant , il faut l' autorisation de ses parents pour donner des informations sur soi à des sites ) .
on a beau être dans une conversation privée avec sa copine , on ne peut jamais être totalement sûr que les échanges ne seront montrés à personne .
pour ne pas avoir de mauvaises surprises , il ne faut pas envoyer de photos de soi dans une situation qui pourrait nous gêner plus tard .
normalement , les sites Internet et applications sur le téléphone ou la tablette n' ont le droit de récupérer que les données personnelles dont ils ont vraiment besoin .
si on commande une trottinette , c' est normal que le site demande notre adresse puisqu' il doit nous l' envoyer .
mais le jeu Minion Rush n' a pas besoin d' avoir accès aux photos qui sont dans notre téléphone pour fonctionner .
il suffit donc de lui refuser cet accès .
parfois , il est écrit " facultatif " à côté des informations demandées .
ça veut dire qu' on n' est pas obligé de les donner .
alors autant ne pas les donner .
sur les réseaux sociaux , on a parfois envie d' avoir beaucoup d' " amis " , car ça donne l' impression que plein de gens nous aiment .
mais il vaut mieux n' accepter que les personnes qu' on connait vraiment , et faire attention à mettre son compte en privé pour éviter que n' importe qui voie ce qu' on publie .
pour éviter que quelqu' un ait accès à nos comptes , sur notre boite mail , sur Snapchat ou ailleurs , il faut créer des mots de passe difficiles .
et en choisir un différent pour chaque site .
sur Instagram ou Gmail , par exemple , on peut aussi activer la validation en deux étapes .
ça veut dire que quand on veut se connecter à notre compte , on reçoit un code par texto , à écrire sur le site ou l' application .
ça permet de s' assurer que personne d' autre n' a accès à notre compte .
pour éviter que Google ne sache trop de choses , on peut utiliser la navigation privée .
ça permet de laisser un peu moins de traces sur Internet .
pour être en mode privé , dans le navigateur Internet ( Chrome , Firefox , Safari ...
) , il faut cliquer sur " fichier " ( ou sur les trois points ou traits situés en haut à droite de la fenêtre ) , puis " nouvelle fenêtre de navigation privée " ou " nouvelle fenêtre privée " .
ainsi , Google n' enregistre pas les données .
il existe aussi un moteur de recherche qui n' enregistre pas les données personnelles , contrairement à Google : c' est Qwant , et sa version pour enfants Qwant Junior .
afin de ne pas être embêté par des sites qui revendent nos données personnelles , on peut créer une adresse mail qui sert uniquement aux réseaux sociaux et aux sites de jeux .
et on garde une autre adresse pour communiquer avec sa famille , ses amis ou ses profs .