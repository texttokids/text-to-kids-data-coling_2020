numéro un octobre deux mille quinze
les migrants
pour ce premier numéro , " le P' tit Libé " a décidé de raconter ce qu' on appelle la " crise des migrants " .
parce qu' on voit beaucoup d' images de gens qui fuient leur pays sur des bateaux , qu' on entend parler de " réfugiés " et de pays qui se disputent sur la façon de les accueillir , et que tout ça n' est pas forcément facile à comprendre .
l' histoire de Suyrana , dix ans
Suyrana a dix ans .
elle vivait avec ses deux petites soeurs et ses parents dans une grande maison dans le nord de la Syrie .
son papa Mohammad était directeur du service touristique des douanes .
mais un jour , la guerre a éclaté et la vie est devenue dangereuse .
Suyrana ne pouvait plus aller à l' école .
il y avait des bombardements plusieurs fois par semaine .
il y a trois ans , son papa a donc décidé de protéger sa famille et de quitter le pays tous ensemble pour aller de l' autre côté de la frontière , en Turquie .
la famille a vécu dans un petit appartement pendant un an , mais c' était aussi un peu dangereux d' habiter là parce que la guerre était juste à côté .
le papa de Suyrana connaissait un Français depuis très longtemps , qui a aidé la famille à venir se réfugier en France .
grâce à lui , Suyrana est arrivée en Bretagne le vingt quatre septembre vingt cent treize.
elle habite maintenant avec sa famille dans un petit appartement à Carhaix .
son papa cherche du travail .
mais ce n' est pas facile de trouver un travail dans un nouveau pays , surtout quand on ne parle pas la même langue .
Suyrana est aujourd'hui en CE deux , elle s' est fait des amis et a appris le français .
elle le parle même mieux que ses parents !
aujourd'hui elle n' a plus peur , mais elle rêve de retourner un jour en Syrie , où vit encore le reste de sa famille , qui n' a pas pu partir .
le parcours de suyrana
qu' est ce qu' un migrant ?
un migrant , c' est quelqu' un qui quitte son pays .
il peut aller dans le pays d' à côté ou très loin .
certaines personnes décident de partir parce qu' elles ont envie de découvrir une autre culture , ou parce que leur patron leur a proposé de travailler ailleurs .
d' autres migrants ne choisissent pas vraiment de quitter leur pays .
ils auraient préféré rester chez eux , mais la vie y est trop difficile .
c' est d' eux qu' on parle beaucoup aux informations en ce moment .
et c' est d' eux que nous allons parler .
parce qu' ils sont en danger
certaines personnes sont menacées de mort dans leur pays .
soit parce qu' elles sont différentes des autres , soit parce qu' elles pensent différemment , soit parce que c' est la guerre .
ces migrants quittent leur pays pour sauver leur vie .
ils souhaitent alors se rendre dans un autre État pour qu' il les protège .
pour ça , ils demandent l' asile .
si le pays qui les accueille accepte de les protéger , donc de leur accorder l' asile , ils deviennent des réfugiés .
à cause de la pauvreté
dans certains pays , il est difficile de trouver du travail , donc de gagner de l' argent pour pouvoir avoir une maison , s' acheter à manger ou aller à l' école .
des habitants quittent alors leur pays pour un autre , où ils pensent trouver du travail plus facilement .
à cause du climat
avec le réchauffement climatique , il y a de plus en plus de catastrophes naturelles ( cyclones , inondations ...
) , qui poussent les gens à s' enfuir de leur pays .
d' où viennent les migrants ?
les migrants qui arrivent en Europe viennent surtout d' Afrique et d' Asie .
en ce moment , ils viennent particulièrement de trois pays loin de chez nous : la Syrie , l' Erythrée et l' Afghanistan .
l' an dernier , un migrant sur deux venait de Syrie et d' Erythrée .
les habitants de ce pays se font la guerre depuis plus de quatre ans .
au départ , le Président a été violent envers la population , et maintenant différents groupes de personnes se battent .
les combats sont terribles .
la moitié de la population a dû quitter sa maison .
quatre millions de syriens ont même fui leur pays .
le conflit a fait beaucoup de morts et de nombreux enfants ne peuvent plus aller à l' école .
dans l' actualité , on parle peu de ce pays très pauvre .
il est dirigé depuis vingt deux ans par un dictateur , un homme qui exerce le pouvoir tout seul .
les habitants n' ont aucune liberté et vivent la peur au ventre .
beaucoup sont envoyés en prison du jour au lendemain .
plus de trois cent soixante Érythréens ont réussi à fuir leur pays mais c' est très dangereux : la police a pour mission de tuer ceux qui partent et de punir leurs familles .
l' Afghanistan est un pays qui a déjà connu plusieurs guerres .
aujourd'hui , il est attaqué par les talibans , des religieux dangereux qui vivent dans le pays et veulent prendre le pouvoir .
beaucoup d' habitants se font blesser ou tuer .
ils sont deux , six millions à avoir quitté leur pays .
où vont les migrants ?
la plupart des migrants se réfugient dans les pays à côté du leur : la Turquie , l' Irak , la Jordanie , l' Egypte et le Liban .
au Liban , un habitant sur quatre est aujourd'hui un réfugié syrien , c' est beaucoup .
les migrants viennent aussi en Europe , comme c' est le cas en ce moment pour de nombreuses familles de syriens .
mais le chemin le plus court pour venir en Europe est très dangereux .
pour cela , les migrants payent des passeurs .
ce sont des hommes qui profitent de leur malheur : ils leur font payer très cher une traversée en mer Méditerranée sur des petits bateaux dans lesquels ils sont trop nombreux .
beaucoup de migrants meurent pendant ce voyage parce que leur bateau se renverse dans l' eau .
ceux qui arrivent en Europe veulent surtout vivre en Allemagne ( parce qu' il y a peu de chômage notamment ) .
d' autres choisissent la Suède et l' Italie .
la France est leur quatrième destination .
mais la France accepte d' accueillir moins de migrants que d' autres pays .
dans l' Union européenne , presque la moitié des demandeurs d' asile obtiennent un statut de réfugié .
mais c' est deux fois moins pour ceux qui font la demande en France !
pourquoi ne peuvent ils pas aller où ils veulent ?
si on parle autant des migrants , c' est parce qu' il y en a plus qu' avant .
ces derniers mois , les conditions de vie sont devenues de plus en plus difficiles dans les pays comme la Syrie ou l' Erythrée , à cause de la guerre , de la pauvreté ou de l' injustice .
alors , beaucoup de gens décident de partir et de venir en Europe , où il n' y a pas la guerre .
pour pouvoir vivre dans un nouveau pays , un migrant doit demander l' autorisation .
mais certains vivent en Europe sans le dire aux autorités et n' ont pas le droit de rester .
on dit qu' ils sont clandestins , ou sans papiers .
s' ils se font arrêter par la police , ils peuvent être expulsés , c' est à dire renvoyés dans leur pays .
les pays européens , comme la France , l' Allemagne ou l' Italie , essaient de s' organiser pour les accueillir , les répartir dans les pays et s' occuper d' eux .
mais en ce moment , c' est plus compliqué parce qu' ils ne sont pas d' accord entre eux .
beaucoup de gens ont envie d' aider et d' accueillir ceux qui fuient la guerre et la misère .
en France et en Europe , il y a la paix et la liberté ( et des écoles pour tout le monde !

c' est une chance .
mais tout n' est pas parfait : il y a des gens qui ne trouvent pas de travail , qui n' ont pas assez à manger ou qui n' ont pas assez d' argent pour avoir un toit .
alors , quand les familles qui fuient la guerre arrivent dans un autre pays , certains habitants de ce pays ont peur de devoir partager , et de devenir plus pauvres .
ils veulent empêcher les migrants de venir .
mais ceux qui fuient sont souvent tellement désespérés , et ont tellement envie de partir et de rejoindre notre continent , qu' ils sont prêts à passer par tous les moyens , même si la police essaie de les en empêcher .
et pour que la police ne les trouve pas , ils se cachent parfois dans des camions , ou prennent le bateau même quand c' est très dangereux .
qu' est ce qu' il se passe à Calais ?
il y a des migrants dans toute la France .
mais aux informations , on parle surtout de Calais , parce qu' ils sont nombreux dans cette ville .
calais se trouve juste en face de l' Angleterre , de l' autre côté de la mer qui s' appelle la Manche .
les migrants ont très envie d' aller en Angleterre .
déjà parce qu' ils parlent anglais .
et puis ils pensent qu' il sera plus facile de trouver du travail là bas , car le taux de chômage est plus faible .
enfin , les demandes d' asile sont plus souvent acceptées , et , si ça ne marche pas , les migrants savent aussi qu' il y a moins de contrôles , donc moins de risques d' être renvoyés dans leur pays .
mais le problème , c' est que les migrants n' ont pas le droit d' aller en Angleterre , alors ils essayent de le faire en secret .
ils tentent de monter dans les camions qui vont en Angleterre sans qu' on les voie , mais se font souvent attraper .
en attendant de réussir , ils vivent dans des tentes , dans des conditions difficiles , et des gens viennent les aider .