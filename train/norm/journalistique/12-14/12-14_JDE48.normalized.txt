jeudi vingt huit septembre vingt cent dix sept
mis en place dans le championnat de rugby , le carton bleu doit permettre à l' arbitre de faire sortir
un joueur qui aurait subi un choc trop violent à la tête .
à quoi sert le carton bleu ?
une carrière qui brise ?

plus grands , plus rapides , plus forts .
les joueurs de rugby ne sont plus les mêmes qu' il y a vingt ans .
leur physique a évolué grâce à de nouvelles méthodes d' entraînement , un changement d' alimentation et la prise de produits permettant d' avoir plus de muscles .
le style de jeu est devenu plus rude aussi .

plus fréquents , plus violents Résultat , sur le terrain , les chocs entre joueurs sont plus fréquents et surtout plus violents .
les sorties de joueur pour cause de blessure ont augmenté ces dernières années .
les joueurs , eux , ont tendance à s' habituer à la douleur , pour garder leur place dans leur équipe .
au point que cela inquiète médecins et professionnels du rugby .

ce sont notamment les commotions cérébrales qui sont pointées du doigt .
ce sont des chocs à la tête qui peuvent endommager le cerveau .
les images de joueurs de rugby qui quittent le terrain l' air
hagard ( perdu ) et les yeux dans le vague ou carrément KO témoignent des dégâts que peuvent provoquer ces collisions .
et cela ne concerne pas que les joueurs professionnels .
les amateurs
( qui ne sont pas payés pour jouer ) subissent aussi une augmentation du nombre de ces blessures .

alors la Fédération française de rugby a décidé de mettre en place un nouvel outil .
quand un médecin ou un arbitre estimera qu' un joueur souffre d' une commotion cérébrale ( choc à la tête ) il pourra brandir un carton bleu .
le joueur devra quitter le terrain .
sa licence sera bloquée pour dix jours .
il ne pourra revenir sur le terrain qu' avec une autorisation du médecin .
si c' est la deux degrés fois que cela lui arrive le joueur sera arrêté durant trois semaines .
et s' il subit trois commotions en un an il devra se reposer trois mois .
le carton bleu est la partie visible des efforts menés actuellement .

en coulisses , les joueurs dirigeants , entraineurs et médecins savent qu' il faudra prendre rapidement des mesures fortes .
il n' est plus possible de jouer la santé des rugbymen .

en Afrique du Sud , un club de boxe réserve des entrainements aux mamies .
elles ont plus de soixante dix ans et de l' énergie à revendre !