en ce moment , la France subit de nombreuses intempéries .
il pleut , il neige , il y a beaucoup de vent .
il y a même des tempêtes et certains départements sont en alerte .
à cause de ces intempéries , il arrive qu' il y ait des coupures de courant .
en effet , les réseaux , fils ou sites de production peuvent être endommagés .
dans ce cas , le courant n' arrive plus jusqu'à certains pâtés de maison voire des communes entières !
il est urgent d' intervenir : cela peut être dangereux car on peut se blesser en y voyant mal , ou alors avoir très froid car le chauffage ne fonctionne plus .
lorsque cela arrive , les entreprises responsables de la distribution d' électricité sont très vites informées du problème et doivent le résoudre dans les plus brefs délais .
les bureaux régionaux organisent les réparations en urgence pour que les personnes touchées puissent récupérer l' électricité dans leur maison au plus vite .
il faut rapidement identifier d' où vient la coupure pour pouvoir agir et envoyer les hommes en bleus réparer le réseau .
les équipes des compagnies d' électricité se réunissent et décident où les envoyer en urgence .
lors d' intempéries , le réseau électrique est souvent endommagé à plusieurs endroits , ce qui va nécessiter plusieurs interventions .
vite , vite !
les hommes en bleu , malgré le mauvais temps et le danger , vont intervenir sur les lignes pour rétablir le courant dans toutes les maisons affectées par la coupure .
c' est loin d' être un travail de tout repos , mais grâce à eux , on ne reste pas longtemps dans le noir et on évite de nombreux accidents !
ce sont un peu des super héros dans leur genre .
ça y est , le courant est revenu !
mais tant que les intempéries ne sont pas finies , les hommes en bleus restent prêts à intervenir de nouveau .