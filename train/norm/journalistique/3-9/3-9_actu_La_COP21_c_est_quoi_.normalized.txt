la COP vingt et un , c' est quoi ?
la COP vingt et un est une grande réunion durant laquelle des adultes parlent des problèmes de notre planète , la Terre , comme le réchauffement climatique , par exemple .
chaque année , sur notre planète , il fait de plus en plus chaud .
ceci a des conséquences sur notre environnement et donc sur les animaux et la végétation .
si nous n' étions pas alertés par des changements dans la nature et la météo , ce phénomène passerait presque inaperçu ...
pour observer et comparer , au fil du temps , le réchauffement de la Terre , il te faudrait trente ans , car la température monte tout doucement .
tout cela est bel et bien en train de se passer et nous en sommes principalement la cause !
l' une des raisons est la déforestation .
les hommes coupent trop d' arbres et abiment les forêts .
pourtant , les forêts sont vitales pour nous car elles nettoient l' air que nous respirons .
une autre raison du réchauffement climatique est notre production de déchets : nous jetons trop de choses à la poubelle .
certaines sont brulées , d' autres sont réutilisées pour fabriquer d' autres objets , on dit qu' elles sont recyclées .
mais beaucoup de déchets se retrouvent aussi entassés les uns sur les autres et mettent des dizaines d' années à disparaitre dans la nature ...
les hommes ont trouvé des solutions .
par exemple , les éoliennes .
elles produisent de l' électricité pour éclairer nos maisons , grâce à la force du vent qui fait tourner leurs hélices .
contrairement au pétrole , cette énergie , ne pollue pas l' atmosphère .
il existe encore pleins d' autres solutions , mais quelles sont les meilleures ?
c' est pour cela que des milliers de personnes vont se réunir à la COP vingt et un pour trouver des moyens de protéger notre environnement du réchauffement climatique .
ainsi nous pourrons garder une planète Terre en bonne santé et plus belle encore qu' elle ne l' est aujourd'hui !