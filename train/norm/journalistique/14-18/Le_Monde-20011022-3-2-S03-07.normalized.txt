son nouveau rôle devrait lui aller à ravir .
dans " star Academy " , sorte de remake musical de " loft Story " qui démarre samedi vingt octobre à vingt et un heures moins dix sur TF1 , Alexia Laroche Joubert joue les directrices d' école .
c' est elle qui veille sur les seize " pensionnaires " huit garçons et huit filles sélectionnés par concours parmi cent mille candidats , enfermés pendant trois mois dans le château des Vives Eaux de Dammarie lès Lys ( Seine et Marne ) , transformé en centre de formation artistique pour les besoins de l' émission .
après avoir mené à la baguette mais en coulisses les lofteurs de M6 ( elle était productrice et chef de projet de " loft story " ) , cette jeune femme de trente et un ans exercera en pleine lumière son autorité sur les apprentis stars de TF1 .
et si l' on en croit le règlement et les premières déclarations de madame la directrice , on ne devrait pas rigoler tous les jours dans le château des Vives Eaux .
" je serai rigide et disciplinaire " , insiste la mère sévère , au cas où on se serait laissé abuser par son sourire angélique et son allure d' éternelle étudiante .
" elle a un tempérament de cheftaine " , confirme Marc Olivier Fogiel , auprès de qui elle a fait ses premiers pas dans la production .
séduit par l' " énergie " et la " détermination " de celle qui , à l' époque , n' était encore qu' assistante sur Canal Jimmy ( étudiante en DESS de droit , elle y est entrée comme stagiaire grâce à son " ex beau père " , Michel Thoulouze , alors patron de la chaîne ) , Fogiel l' a recrutée sur Canal plus au début des années dix neuf cent quatre vingt dix , pour le seconder à " télé Dimanche " .
en dix neuf cent quatre vingt dix sept , c' est lui qui , involontairement , lui ouvre la voie vers " loft Story " en la présentant à Stéphane Courbit , associé d' Arthur dans Case production , en quête d' une rédactrice en chef pour " exclusif " , un nouveau magazine consacré aux potins du show biz .
" j' aime le star système , c' est mon côté frime " , minaude Alexia Laroche Joubert .
elle dirigera l' émission pendant trois ans avant de se laisser envouter par les sirènes de la télé réalité .
rachetée en dix neuf cent quatre vingt dix neuf par Endemol ( " Big Brother " ) , case s' empresse d' introduire à la télévision française ces nouveaux programmes où des individus ordinaires sont observés jour et nuit par des caméras cachées .
" fascinée " par le concept , Alexia Laroche Joubert se lance " sans vraiment réfléchir " et propose à Courbit de produire l' adaptation française de " Big Brother " .
ce sera " loft Story " .
" l' émission réunissait tout ce dont je rêve en télévision : de l' humain , du direct , et de l' imprévu " , commente t elle , décidée à assumer jusqu' au bout ce programme , qui suscita une polémique d' ampleur nationale .
des regrets ?
" j' ai eu une chance incroyable de participer à cette aventure " , assène t elle , ajoutant cultiver un véritable gout pour " la trash TV , les shows à l' américaine , les émissions du type " c' est mon choix " ou " perdu de vue " " .
le torrent de critiques soulevé par " loft Story " ne l' aurait donc pas atteinte ?
" je les ai trouvées souvent injustifiées . de toute façon , je ne suis pas du genre à me laisser freiner par le doute . et puis ma famille m' a beaucoup soutenue . mes grands parents ont trouvé ça " top " . " sa mère , Martine Laroche Joubert , grand reporter à France deux , en tournage dans l' Himalaya au moment de la tornade " loft Story " , garde , elle , un souvenir amer de cette période : " je me suis fait agresser dans les diners en ville par des gens des médias ou du show biz qui n' acceptaient pas qu' on donne la vedette à des filles et des garçons ordinaires issus d' un autre milieu qu' eux . " elle ne partage pas l' engouement de sa fille , mais elle la soutient et la défend comme une maman : " Alexia fait son métier avec humanité , elle n' a rien à voir avec la caricature de la productrice qu' on imagine . " avec humanité peut être , mais sans états d' âme .
passée pendant l' été de M6 à TF1 , Alexia Laroche Joubert côtoie désormais quotidiennement ceux qui ne s' étaient pas privés de cracher publiquement sur son " loft " , qualifié d' " émission poubelle " .
comment vit elle la situation ?
" je prends rarement du recul sur les choses . je fonce " , répond elle , avec son sourire désarmant .
à quoi bon se poser des questions ?
au printemps vingt cent deux , elle sera d' ailleurs de retour sur M6 pour " loft Story deux secondes .