quasi clone de parvest Europe Mid Cap , un compartiment de la sicav luxembourgeoise de BNP Paribas , la sicav de droit français Antin Europe Mid Cap se place parmi les dix premiers fonds de la catégorie actions européennes petites et moyennes capitalisations établie par la méthode APT .
comme sa cousine luxembourgeoise , ce produit a pour vocation d' investir sur des " grosses valeurs moyennes " .
pour être éligible à Antin Europe Mid Cap , une société doit disposer d' un flottant ( la part des actions disponibles en Bourse ) compris entre un et cinq milliards d' euros .
" nous choisissons des sociétés " investissables " que nous pouvons acheter ou vendre rapidement " , explique Catherine Guinefort , de chez BNP Paribas .
la gestion privilégie des entreprises qui connaissent une croissance soutenue , régulière et prévisible sur trois à cinq ans .
" cela nous conduit à avoir un biais sectoriel fort , remarque Madame Guinefort . traditionnellement , nous choisissons des sociétés qui appartiennent aux secteurs de la santé , de la gestion de l' épargne , de l' agroalimentaire ou des services aux entreprises " , observe t elle .
à la mi novembre , le secteur industriel représentait près de vingt pour cents du portefeuille , la santé et la finance pesaient chacune plus de dix sept pour cents de l' actif : une structure d' investissement qui est reflétée dans une grande mesure par l' analyse du risque telle qu' elle est réalisée par la méthode APT .
parmi les principales lignes du portefeuille , on compte Essilor .
" cette société est numéro un mondial dans son secteur et enregistre une croissance régulière de ses résultats " , note Madame Guinefort .
le poids des investissements sur la foncière Sophia est aussi significatif .
" nous sommes confiants sur le cycle immobilier d' ici à deux ans , car il n' y a pas d' excès dans l' offre et la construction " , note t elle .
hors de France , Antin Europe Mid Cap a misé sur Altana .
cette entreprise pharmaceutique allemande connait une " superbe " croissance de ses résultats .
la sicav a aussi parié sur Kerry Group , une société irlandaise spécialisée sur les ingrédients alimentaires , ou Givaudan , une entreprise suisse d' arômes .
autre investissement important , Cattles , un établissement financier anglais qui distribue des crédits auprès d' une clientèle modeste .
d' une manière générale , chaque ligne ne pèse pas plus de trois pour cents du portefeuille , composé d' environ soixante dix titres .
les investissements sont réalisés pour une durée de trois ans environ .
" trois facteurs nous conduisent à vendre une action , outre des performances très décevantes , explique Madame Guinefort . nous nous séparons d' un titre lorsque nous constatons une baisse durable du taux de croissance de la société , quand son poids dans le portefeuille dépasse trois , cinq pour cents ou si la société grossit de taille " , précise t elle .