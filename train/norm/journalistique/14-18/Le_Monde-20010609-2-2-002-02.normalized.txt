le leader du Parti conservateur , William Hague , a immédiatement tiré , vendredi huit juin , les conséquences de sa large défaite .
il démissionnera dès qu' un successeur sera désigné " pour poursuivre le travail et prendre de nouvelles initiatives " .
désigné en mille neuf cent quatre vingt dix sept comme successeur de John Major après le raz de marée travailliste , le député de Richmond ( Yorkshire ) paie aujourd'hui le prix de ses graves erreurs de stratégie électorale .
en privé , de nombreux édiles conservateurs avaient critiqué l' accent mis pendant la campagne sur le maintien de la livre sterling et sur la lutte contre l' immigration plutôt que sur le délabrement des services publics .
âgé de quarante ans , l' ancien poulain de Margaret Thatcher formé à Oxford , chantre du conservatisme traditionnel et des valeurs morales , a toujours ramé à contre courant .
parmi les possibles candidats à son remplacement figurent deux personnalités du cabinet fantôme : Michael Portillo ( finances ) et Ian Duncan Smith ( défense ) .
( Corresp .
