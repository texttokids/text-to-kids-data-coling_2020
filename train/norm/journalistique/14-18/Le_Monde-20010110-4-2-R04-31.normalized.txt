redwood ( californie ) de notre envoyé spécial La grosse BMW blanche se gare avec quelques minutes de retard sur un parking de Redwood , une de ces villes sans charme de la Silicon Valley .
Gene Kan s' excuse .
un rendez vous plus long que prévu .
entre le logiciel de partage de fichiers Gnutella ( " le Monde interactif " du vingt cinq octobre vingt cent zéro) , dont il est le porte parole mondial très médiatique , et sa toute jeune société , Infrasearch , l' agenda de cet Américain d' origine chinoise de vingt quatre ans est plus que surchargé .
sur la route qui nous conduit vers un hypothétique café ouvert en ce milieu d' après midi pluvieux , Gene Kan est peu loquace .
ce qu' il a à dire , il le dit avec son clavier et ses ordinateurs .
" programmer un PC , c' est comme de l' art . c' est même mieux , car c' est moins cher . tu achètes un ordinateur et tu peux produire une multitude de choses . la seule contrainte , c' est le temps . " sur son CV , disponible en ligne , la liste de ses connaissances en programmation donne le vertige .
certains journalistes n' ont d' ailleurs pas hésité à qualifier Gene Kan de " petit génie de l' informatique " .
" je programme depuis l' âge de treize ans , précise t il . quand je suis arrivé à l' université de Berkeley pour mes études en informatique , j' en connaissais plus en programmation que ceux qui en sortaient . " courtisé par les grands Devant son jus de fruits , qu' il boit à la bouteille , Gene Kan prend confiance .
il revient sur l' année de folie qu' il vient de vivre .
parfait inconnu il y a encore peu , il est devenu en quelques mois une figure du Web .
Gene Kan est en effet l' un des principaux développeurs et surtout le porte parole de Gnutella , ce logiciel d' échange de fichiers numériques qui , avec Napster , a secoué l' industrie du disque en l' an vingt cent zéro .
" mon rôle a été d' unifier le développement de Gnutella . Justin Frankel , l' inventeur , est une personne plus recluse , plus timide , alors c' est moi qui suis monté au créneau , raconte t il . mais cela n' a pas changé ma vie . j' y ai laissé du temps et de l' argent . maintenant que Gnutella devient une affaire commerciale , j' y ai moins ma place . " tout cela dit avec cette voix monocorde dont il se départ pas .
c' est sans doute l' expression d' un profond cynisme , que Gene Kan revendique haut et fort .
il faut l' entendre médire de la Silicon Valley , sur " ces salaires ridiculement hauts " , pour comprendre que , malgré la notoriété , Gene Kan n' a pas " la grosse tête " .
juste perceptible , derrière ce masque peu enclin à la rigolade , la fierté d' avoir participé à une page importante de la Toile .
" ce qui est dingue avec Napster et Gnutella , c' est que ce sont des gamins qui ont réussi à bouleverser les choses et à perturber la vie de tant de gens ! " conscient que Gnutella ne lui rapporterait pas un centime , " c' est juste un outil , et on ne construit pas une maison avec un marteau " , Gene Kan a donc décidé de voler de ses propres ailes .
le voilà désormais loin de l' univers des gamins , à la tête d' Infrasearch , une société très prometteuse qui suscite un bel engouement outre Atlantique .
fondé sur les échanges d' égal à égal ( peer to peer ) , le moteur de recherche développé par la start up devrait permettre d' indexer la Toile de manière beaucoup plus intelligente et plus performante que les moteurs actuels .
" nous voulons utiliser l' intelligence de chaque site , résume Gene Kan . qui mieux qu' un administrateur sait ce qu' il y a sur ses pages ? " preuve de la pertinence de cette technologie , les fondateurs de Netscape et de Google ont été les premiers séduits par Infrasearch , qui a déjà levé quarante millions de francs sur une simple démonstration .
ses habits de PDG , Gene Kan n' a qu' une hâte de les enlever .
un peu trop grands pour lui , à son gout .
" j' aime ça , mais à vingt quatre ans , c' est un peu difficile . je suis obligé de faire des choses dont je n' imaginais même pas l' existence . et ce n' est pas parce que je suis un peu connu qu' il est plus facile pour moi de lever de l' argent . je suis encore trop jeune pour les investisseurs . " lui qui n' avait eu aucune difficulté à trouver du travail à sa sortie de Berkeley se retrouve aujourd'hui confronté au miroir aux alouettes de la Silicon Valley .
" c' est peut être la seule région du monde où , de chaque côté d' une rue , vous pouvez avoir la maison d' un milliardaire comme Larry Elisson ( PDG d' Oracle ) et en face les cabanes misérables des Mexicains . " passionné de mécanique Gene Kan se considère comme " un bourgeois de la classe moyenne qui gagne dix fois moins que quand il était employé " .
" ici ce qui est fou , dit il , c' est que vous pouvez gagner un million de dollars par an et être pauvre ! quelle région de merde quand même ! " il n' est , malgré son discours , pas du genre à cracher dans la soupe .
l' argent ne lui a pas fait perdre sa lucidité , voilà tout .
" mon père , qui était informaticien , confie t il , s' est fait virer après vingt ans passés dans la même entreprise quand il y a eu la crise dans cette profession . il m' a appris à ne pas miser sur une seule activité . " lui se verrait bien camionneur .
sans rire .
ils possède d' ailleurs deux engins , dont un remorqueur de tank .
passionné de courses automobiles et de mécanique , il n' éprouverait aucune honte à passer du statut de PDG courtisé par les grands de la high tech à celui de routier sillonnant les États Unis .
dans sa maison de Belmont , non loin de l' aéroport de San Francisco , sa petite amie française vient de le rejoindre .
quand il a un peu de temps , pour oublier ses tracas de jeune PDG , il retape ses bolides et s' attelle à la rédaction d' un livre consacré aux supermarchés de la Vallée .
" les supermarchés illustrent très bien la stratification des gens ici . il y a des grandes surfaces chics et d' autres beaucoup plus modestes . " la nuit tombe sur Redwood .
Gene Kan remonte dans sa belle voiture .
les camions attendront encore un peu .
l' aventure Infrasearch ne fait que débuter .