José Moure
les lettres françaises
du labyrinthe comme figure du sublime
dans son Guide de l' amateur du musée du Louvre , Théophile Gautier écrit : " entrons donc sans plus tarder , car nous avons à parcourir un labyrinthe de chefs d' oeuvre , dont notre description sera le fil . " le fil est une ligne .
avec la ligne , on se retrouve aux abords immédiats de la mort .
on en réchappe .
ligne dérive du latin linum , fil de lin .
cette ligne , ce fil permit , une fois le Minotaure tué , de sortir du labyrinthe lui même , après tout , d' abord réseau de lignes .
on rentre dans le labyrinthe avec une ligne .
on la laisse s' écouler .
on sort en suivant la ligne , en la reprenant , en en faisant une nouvelle pelote , un point .
la pelote est abandonnée , comme Ariane .
Ovide décrit le Minotaure comme " un monstre qui unit en lui les deux formes d' un taureau et d' un jeune homme ... " le Minotaure est le fruit de l' accouplement d' une femme et d' un taureau : double monstruosité , l' engendrement et son résultat .
un glaive trancha le fil , celui de la vie .
il le fallait .
Ariane sauve Thésée qui sauve Ariane fille de Minos , un des trois juges des enfers après sa mort , demi soeur de Phèdre , " fille de Minos et de Pasiphaé " et donc , aussi , demi soeur du monstre sans cesse affamé .
trancher le fil , par le fil de l' épée , et sortir du dédale en suivant le fil , donné par et pour l' amour .
le fil permet à l' amour de se faire .
le Minotaure est la terreur .
on l' entend .
les mugissements glaçants se répercutent sur les murs du labyrinthe pour couvrir la vallée comme une ombre .
on ne voit pas le fils illégitime de Pasiphaé et de la bête .
seuls des vierges l' ont vu , lorsque les crocs se sont refermés .
il pourrait être aveugle , ce petit fils du Soleil enfermé dans son antre , guidé par la faim , la soif , les odeurs alléchantes , la fureur .
ses yeux , imaginons des trous noirs où l' effroi se lit .
on ne voit pas le Minotaure : il est partout dans le labyrinthe .
la main ne doit pas lâcher le fil qui glisse doucement comme s' il caressait la peau .
suivre la ligne , la faire et la défaire , d' un point l' autre , sauve des crocs qui percent la peau , déchirent les chairs , broient les os , éclatent le crâne en faisant gicler le cerveau , sauve des lèvres et de la langue qui lèchent , sucent , aspirent le sang répandu , avalent les entrailles , pompent les viscères , font rouler les globes oculaires avant de les recracher comme de vulgaires pépins .
on les voit ces yeux jonchant un sol dont la terre est devenue boue d' être par trop gorgée de sang .
non , la porte du labyrinthe est franchie dans l' autre sens afin de donner au corps sa portion de lumière .
la peau reste douce et chaude à la caresse , parfumée et savoureuse au baiser .
on se détourne , on se retourne pour ne laisser , dit Racine , qu' une terre " fumant du sang du Minotaure " .
Thésée , entré de plein gré dans le labyrinthe , parcourant les couloirs où se répercutent les cris et les pleurs mêlés aux mugissements , Thésée , trouvant la bête illégitime , ou découvert par elle , et la perçant de son glaive , la laissant inerte sur le sol où elle pourrira , Thésée , son exploit accompli , sorti du labyrinthe en suivant le fil qui le sauvait d' une mort obscure et sans gloire , Thésée sauvé par ce fil retrouve la petite fille du soleil , Ariane dont il tenait la ligne : il la quitta , l' abandonnant en pleurs sur l' île déserte de Naxos alors qu' il faisait voile vers Athènes .
Dionysos , alors , la recueillit et l' aima .
dans les Dithyrambes de Dionysos , de Nietzsche , le dieu déclare à Ariane : " je suis ton labyrinthe ... " deux dangers menaçaient Thésée : le Minotaure et le labyrinthe , fil du glaive pour l' un , fil de lin pour l' autre lignes droites et tendues .
la ligne brisée chute sur la mort comme si le fil d' Ariane , trop tendu , se sectionnait à l' angle d' une pierre le jetant dans le ravage des dents aiguisées , entrainant le corps dans l' estomac du monstre , le plongeant dans les sucs gastriques qui rongent , transforment en bouillie pour , à la fin , être chié .
on se tient droit sur le fil d' Ariane , une perche à la main , on marche sur le fil d' Ariane comme un funambule .
regardons les cordes de la lyre comme lignes .
cet instrument de musique fut inventé par Hermès , " subtil et éloquent , voleur , ravisseur de boeufs , conducteur de songes , éclaireur de nuit , gardien de portes " , comme le dit l' Hymne homérique à Hermès mais aussi , selon l' Hymne orphique " maître des danses et des transes " .
la lyre plut tant à Apollon qu' il la lui échangea contre le troupeau de boeufs qu' il lui avait volé .
" et , quand l' archer eut appris à jouer de l' aimable cithare , elle résonna toujours sur son bras " , dit encore l' Hymne homérique à Hermès .
la lyre comporte un nombre de cordes identique au nombre de jeunes filles et au nombre de jeunes garçons sacrifiés au Minotaure : septembre C' est grâce à Dédale ( si j' ose dire ...
) que le Minotaure a pu exister car il construisit la vache de bois qui permit son engendrement , et c' est Dédale encore qui fabriqua Talos , le géant de bronze animé qui garde l' entrée du labyrinthe pour empêcher les sujets du sacrifice de s' en échapper , et c' est Dédale , toujours , qui inventa le labyrinthe et dont l' image , nous raconte Virgile , figure sur les murs qui enferment la sibylle .
dédale est une créature apollinienne .
or le chiffre sept est associé au culte d' Apollon , dont la lyre était l' un des attributs , on célébrait son culte le septième jour du mois .
une phrase attribuée à Hippocrate nous dit : " le nombre sept , par ses vertus cachées , maintient dans l' être toutes choses , il dispense vie et mouvement , il influence jusqu' aux êtres célestes . "