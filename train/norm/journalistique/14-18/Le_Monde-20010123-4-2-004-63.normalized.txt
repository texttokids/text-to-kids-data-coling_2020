la justice a engagé , lundi vingt deux janvier , une enquête sur Joseph Estrada , des membres de sa famille ainsi que des associés , aux motifs de pillage économique ( passible de la peine de mort ) , de parjure et de corruption .
en évacuant , samedi vingt janvier , le palais présidentiel de Malacanang , l' ancien président n' avait pas évoqué l' éventualité d' un exil et , au contraire , affirmé qu' il ferait face à " tout futur défi qui puisse se présenter " .
il avait également dit ne pas souhaiter " être un facteur qui empêcherait la restauration de l' unité et de l' ordre dans notre société civile " tout en exprimant " de sérieux doutes à propos de la légalité et de la constitutionnalité de la proclamation ( de Gloria Macapagal Arroyo ) comme présidente " .
enfin , même s' il n' a pas formellement démissionné , la justice estime qu' il a perdu son immunité présidentielle .
( Corresp .
