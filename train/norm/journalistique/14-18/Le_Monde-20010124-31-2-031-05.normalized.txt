la réalisatrice revient sur le chemin qui lui fait remonter un quart de siècle et pénétrer dans les casernes .
mon vingt cinq avril .
" le jour même du vingt cinq avril , je n' étais pas au Portugal . j' étais en Autriche , où je vivais avec mes parents . ils ont été pris d' une euphorie hors du commun . mes parents n' étaient pas exilés politiques mais ils préféraient être ailleurs . mon père est musicien , c' est le compositeur de la musique du film , et il avait fait ses études à l' Académie de Vienne . il y est resté . nous sommes rentrés au Portugal juste après la révolution . en Autriche , j' étais au lycée français , j' avais été élevée comme une enfant du Nord , j' aimais la propreté , l' ordre , le calme . " je suis arrivée dans un pays en plein chaos , en pleine euphorie révolutionnaire , où tout était sale , tout était confus .
j' étais traînée de manif en manif où il fallait embrasser quarante personnes .
ma première réaction a été de penser : " ceci n' a rien à voir avec moi , ceci n' est pas mon pays . " ce n' est que deux ans plus tard que j' ai réalisé que ç' avait été formidable de vivre ces moments .
comme ma mère était journaliste politique , j' ai vu de très près des choses importantes .
j' ai croisé beaucoup de protagonistes , en particulier Otelo de Carvalho , mais aussi d' autres capitaines .
on habitait à l' hôtel comme beaucoup de gens qui étaient rentrés de l' étranger .
dans cet hôtel , je me souviens avoir vu les nouveaux députés et les membres du gouvernement discuter de la Constitution autour d' une table du bar .
" quelques raisons de faire ce film . " il n' y a aucune fiction sur la " révolution des oeillets " .
il y a de nombreux et très beaux documentaires , dont un portugais , Le Bon Peuple portugais , qui a été fait en mille neuf cent soixante seize , en noir et blanc .
il a été l' une de mes bases de travail .
il y a beaucoup de documentaires réalisés par des étrangers , dont Glauber Rocha qui a fait des images vraiment amusantes .
dans mon film , il y a des interviews de rue , qu' on pourrait croire spontanées .
les gens qui répondent sont des figurants , pas des acteurs , mais ils ont appris par coeur des réponses tirées des documentaires de Glauber Rocha .
" je suis tombée sur des textes écrits par les capitaines d' avril , notamment Maia . c' est un journal qui décrivait à la fois la guerre coloniale , une expérience traumatisante , et cette journée particulière . un texte très cinématographique avec des références à Charlie Chaplin pour certains moments burlesques , mais aussi à des films de guerre , le tout marqué par une autodérision très saine . je me suis mise à lire tout ce qu' avaient écrit les capitaines . à chaque fois j' ai retrouvé une description d' eux mêmes comme s' ils s' étaient vus dans un film . et c' est çe qui m' a orientée vers la fiction . " dans le film , il n' y a que trois personnages qui ont gardé leurs noms , Maia , le général Spinola et le président Caetano .
les autres sont inspirés de l' histoire , de dialogues qui ont été rapportés , mais en brouillant un peu les pistes , il me fallait pouvoir prendre des libertés de fiction .
pour le dialogue entre Maia et Caetano , ou la confrontation entre les chars des capitaines et ceux restés fidèles au gouvernement , je suis restée fidèle à la réalité historique .
" un regard sur les capitaines . " mon film est un premier film classique et un premier film de femme classique , même s' il inclut quelques blindés et des scènes de foule .
parce que , dans un premier film , on met beaucoup de soi même .
l' histoire du cinéma est pleine d' hommes qui regardent les femmes , il est naturel qu' une femme regarde les hommes .
le monde qui nous est opposé , c' est celui des militaires .
quand j' entrais dans les casernes pour préparer le film , c' était comme se poser sur une autre planète .
quand même , les militaires , ce sont de petits bonshommes verts .
le regard qu' une femme porte sur eux est nécessairement différent parce que nous n' avons pas le traumatisme du service militaire .
" j' ai été séduite par l' incroyable vulnérabilité dans laquelle ils ont accepté de se placer . ce sont des hommes qui ont été entrainés toute leur vie à obéir et à tirer . ils ont décidé de désobéir et de ne pas tirer . la " révolution des oeillets " a eu lieu en pleine guerre froide . néanmoins , derrière ces capitaines , pendant la préparation , le jour de la révolution , il n' y avait pas de bloc , ni le parti , ni la CIA . cette pureté dans laquelle ils ont agi , en liberté , par conviction personnelle , c' est très moderne . je voulais voir ces vingt quatre heures de pureté . tout de suite après , même si l' euphorie a persisté , la guerre froide a repris ses droits , et on sait très bien derrière quels partis se trouvaient les uns et les autres . " Otelo de Carvalho a aidé le film , c' est sa voix qu' on entend lorsque le poste de commandement donne des instructions .
il était un peu triste que le film ne soit pas sur lui , mais sur Maia .
mais pour un film sur le poste de commandement , il faudrait un bien meilleur écrivain que moi .
Maia a été durement puni .
en mille neuf cent quatre vingt huit , il a été atteint d' un cancer , et l' Etat lui a refusé une pension alors que , la même année , deux fonctionnaires de la PIDE en ont reçue .
il a été maltraité parce qu' il avait été incorruptible .
après la révolution , une pléthore de partis sont apparus , et chacun cherchait à avoir son militaire pour le cautionner .
Maia ne s' est pas prêté à ça , il l' a payé durement .
" les Portugais et Capitaines d' avril . " c' est le film portugais qui a attiré le plus de public au Portugal en deux mille .
dans les salles , les gens ont chanté .
dans les journaux , il y a eu une grosse polémique .
j' ai été frappée par la vivacité et la passion de certaines réactions .
j' ai surtout été attaquée sur mon droit , appartenant à la seconde génération , à m' approprier une histoire qui appartient à la génération de mes parents .
pendant les années quatre vingts , la révolution était ringarde , c' était la période des yuppies .
je me souviens de m' être fait quasiment jeter de certaines librairies quand je demandais de la documentation sur la révolution , comme si je demandais des bouquins pornos .
