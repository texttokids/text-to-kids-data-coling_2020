" combien y a t il d' ermites en France ? ils seraient entre deux cents et trois cents , peut être plus de cinq cents si l' on compte les ermites " sauvages " , hors institution . il n' existe pas de recensement des ermites , dont les engagements et les conditions de vie sont très divers . l' érémitisme urbain , dans une maison ou dans une HLM , constitue déjà un monde en soi . on peut cependant distinguer trois types d' ermites , à partir de leur statut . les uns sont rattachés à un évêque , dont ils dépendent . d' autres appartiennent à une congrégation religieuse ou à un monastère . reste la masse des " électrons libres " , ceux qui ne sont liés à aucune institution religieuse , mais sont reconnus comme ermites par leurs pairs . on trouve aussi des ermites errants , des gyrovagues dans le langage religieux . j' en ai raté deux de justesse au cours de mon enquête ! on m' a parlé d' une femme qui accompagne des gens du voyage et se fait passer pour folle , par humilité ou pour échapper aux contacts . mais ces cas extrêmes ne doivent pas faire confondre les ermites avec des babas cool ou des adeptes du New Âge ! comment définiriez vous le " vrai " ermite ? je retiendrais quatre critères : la solitude , le silence , l' ascèse et la prière . ce sont les quatre piliers de la vie érémitique . mais la solitude est parfois difficile à protéger . comme les moines , les ermites respectent une tradition d' accueil . ils ne peuvent pas fermer leur porte à un visiteur . certains d' entre eux , qui ont un fort rayonnement spirituel , reçoivent même beaucoup de visites . de là , un équilibre difficile à trouver entre isolement et hospitalité . plusieurs ermites m' ont supplié de ne pas donner leur adresse . " l' ermite type est âgé de quarante à quatre vingts ans .
on choisit rarement cette voie plus jeune : il faut un temps de réflexion et de discernement .
les deux tiers des ermites sont des femmes .
je ne sais pas comment expliquer ce phénomène .
peut être les femmes ont elles plus de force de caractère pour supporter la solitude .
comment devient on ermite ?
il n' existe pas de " filière " .
les laures , c' est à dire les regroupements d' ermites , dont le plus connu est celui de Montmorin ( Hautes Alpes ) , peuvent servir de " sas " , d' étape d' apprentissage .
en tout cas , il est recommandé d' avoir déjà une expérience de vie religieuse comme cénobite , comme moine vivant en communauté .
le plus difficile est de trouver un ermitage qui réponde à la vie qu' on souhaite mener .
veut on être complètement isolé , ou moyennement isolé ?
quel rapport a t on avec la sécurité ?
cette question est très importante pour les femmes .
car l' ermite est à la merci des agressions , surtout s' il reçoit des marginaux .
les départements où l' on trouve le plus d' ermites sont l' Ardèche , la Drôme , le Vaucluse , les Alpes de Haute Provence , le Var , le Tarn et le Tarn et Garonne .
pour des raisons climatiques évidentes : il est plus facile de vivre sans électricité ni chauffage dans le Midi .
mais je crois qu' il faut tenir compte aussi d' un rapport privilégié avec la nature .
les ermitages sont souvent situés dans des sites sauvages et magnifiques .
il y a un certain écologisme dans l' érémitisme .
de quand date le renouveau de l' érémitisme ?
ce choix de vie a connu une éclipse pendant soixante quinze ans .
à la fin du XIXe siècle , il a cessé d' exister comme mouvement dans l' Eglise catholique .
cette disparition est due à une réaction des milieux ecclésiaux , qui ont toujours entretenu une méfiance à l' égard de la voie érémitique , jugée dangereuse et incontrôlable .
elle a refait son apparition grâce au concile Vatican deux ( dix neuf cent soixante deux dix neuf cent soixante cinq ) , qui la mentionne comme digne d' intérêt .
en dix neuf cent quatre vingt trois , le droit canon a inséré l' érémitisme dans ses règlements et l' a ainsi légitimé .
" aujourd'hui , les valeurs de l' érémitisme , comme le silence ou l' ascèse , apparaissent en contradiction avec ce que vit l' homme moderne . par là même , elles acquièrent une dimension supérieure et deviennent attirantes . dans une société qui valorise les choix de vie extrême , la radicalité de cet engagement peut être séduisante . les ermites sont un peu les " lofteurs de Dieu " . sauf qu' ils ne vivent pas sous l' oeil des caméras . pour eux , c' est sous le regard de Dieu qu' ils accomplissent le moindre geste . les ermites sont ils des êtres exceptionnels ? ce ne sont pas forcément des saints , mais ce sont toujours des personnalités fortes . on trouve fréquemment chez eux une faille , un défaut sans lequel ils ne seraient pas ce qu' ils sont . certains sont de véritables ours . l' un de ceux que j' ai rencontrés m' a écrit récemment : " les ermites sont pour la plupart des braves gens , mais il faut attendre avant de leur accorder une auréole .
" je crois que le mot " brave " est à prendre ici au sens fort de courageux . il faut du courage , en effet , pour affronter la solitude . "