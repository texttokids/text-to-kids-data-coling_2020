une impression , seulement une impression .
le tribunal de Paris serait il devenu le dernier salon où l' on juge ?
en regardant , en écoutant , en lisant ceux qui vécurent , dans la bousculade , les cris " Christine , Christine " , et la moiteur étouffante d' une salle trop petite pour un procès trop vaste , on se prit à sourire .
c' est qu' on entendait de drôles de choses à la radio .
notamment des considérations à faire pleurer les cailloux " sur les amants déchirés qui ne se parlent même plus " .
et ne se regardent pas , autrement que d' un coup d' oeil furtif et indifférent .
forcément indifférent .
c' est qu' on vit de drôles de choses à la télévision .
et notamment Roland Dumas " portant beau " .
nécessairement beau .
car cet homme là est l' élégance faite homme .
un pléonasme d' élégance même , dans la nécessité du romanesque indispensable aux romans de justice .
il faut tenir pour établi désormais , dans la caractérisation des personnages , et n' y revenons plus , que Roland Dumas se reconnait , dans la foule platement ordinaire et anonyme des justiciables , à trois , quatre caractéristiques : son élégance innée .
sa splendeur grisonnante .
cette canne qui pour d' autres serait tutrice et pour lui semble raffinement .
et enfin et surtout , ce regard " vif , aigu , moqueur , malicieux , aiguisé " noté par tous , en des termes si proches qu' il doit bien y avoir là une esquisse de vérité .
le regard , tout est dans le regard .
ainsi cette considération lue , ici ou peut être bien là , de nature à nous faire méditer : " Roland Dumas , l' oeil pétillant , jauge ses juges . " admirable !
presque autant que l' entrée majestueuse et calculée , en frégate intrépide fendant la marée journalistique , du citoyen avocat justiciable Dumas .
l' anecdote retiendra que tous les prévenus , même Christine , prirent le périf soigneusement balisé par la garde du palais .
tous sauf lui .
un Roland Dumas ne rentre pas au palais de justice de Paris par l' entrée des fournisseurs et des suppliciés !
il fonce droit devant .
droit dedans !
à nous deux l' arène , supplice médiatique librement consenti et soigneusement médité .
ni journalistes ni juges , ni Dieu , ni maître , ralliez vous à mon panache gris !
admirable en effet .
maître Roland Dumas , maître de lui , maître chez lui !
on ne fit guère mieux pour poser le décor et l' intrigue , étant entendu que pour illustrer à gros traits le chant des justiciables désunis , le splendide Roland et la belle Christine s' assirent aussi loin l' un de l' autre que l' autorisaient les bois et bancs de justice .
belle symbolique d' une rupture consommée !
nous nous sommes tant jugés !
retenons pour finir , et par ce qu' on a lu , que le tribunal féminin eut l' excellent réflexe , honneur aux dames , de consacrer l' essentiel de ce lever de rideau à essayer de comprendre pourquoi Elf , en son temps , jugea que les services de la belle Christine n' avaient pas de prix .
et encore moins de limites , puisqu' elle se dévoua dit elle , vingt quatre heures sur vingt quatre , à la cause pétrolifère .
tout cela est raconté par ailleurs et par des témoins fiables , eux .
pour nous , contentons nous de cette phrase dérobée , peut être un peu préparée lors des répétitions , mais néanmoins ronflante : " je suis fière d' avoir fait tout ce travail . c' était bon pour Elf et bon pour la France . " trompettes !