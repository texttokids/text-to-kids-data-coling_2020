
bébé va au marché avec Maman .

le marché est bondé .
bébé est très curieux .
bébé est si curieux que madame Ade , la marchande de bananes , donne à Bébé six
bananes .

bébé est très étonné .
bébé mange une banane ...

et met cinq bananes dans le panier .

maman ne remarque rien .
elle est occupée à acheter du riz .

le marché est bondé .
bébé a très chaud .
bébé a si chaud que monsieur Femi , le marchand d' oranges , donne à Bébé cinq
oranges juteuses .

bébé sourit .
bébé suce une orange ...

et met quatre oranges dans le panier .

maman ne remarque rien .
elle est occupée à acheter de l' huile de palme faite maison .

le marché est bondé .
bébé est très joyeux .
bébé est si joyeux que monsieur Momo , le marchand de biscuits , donne à Bébé
quatre chin chin sucrés .

bébé applaudit .
bébé mange un chin chin ...

et met trois chin chin dans le panier .

maman ne remarque rien .
elle est occupée à acheter des piments .

le marché est bondé .
bébé est très drôle .

bébé est si drôle que madame Kunle , la marchande de maïs sucré , donne à Bébé
trois épis de maïs grillés .

bébé rayonne .
bébé mange un épi de maïs ...

et met deux épis de maïs dans le panier .
maman ne remarque rien .
elle est occupée à acheter des chaussures .

le marché est bondé .
bébé est très coquin .
bébé est si coquin qu' il fait tomber tous les vêtements .

mais Bébé est si embêté que madame Dele donne à Bébé deux morceaux de noix
de coco .

bébé salive .
bébé mange un morceau de noix de coco ...

et met l' autre morceau dans le panier .

maman ne remarque rien .
son panier est très lourd .
très , très lourd .
et Maman pense soudain que son joli Bébé doit avoir bien faim maintenant .
" taxi ! crie Maman . nous devons vite rentrer à la maison ! "

maman dépose son panier .
" mais qu' est ce que c' est que ça ?
s' écrie Maman .
cinq bananes !
quatre oranges !
trois biscuits chin chin !
deux épis de maïs grillés !
un morceau de noix de coco !
je n' ai jamais acheté tout ça !

" en effet ! "
rit madame Ade , la marchande de bananes ,
et monsieur Femi , le marchand d' oranges ,
et monsieur Momo , le marchand de chin chin ,
et madame Kunle , la marchande de maïs ,
et madame Dele , la marchande de noix de coco .
" c' est nous qui les avons offerts à Bébé ! "

maman regarde son Bébé .
bébé rit .
maman rit aussi .
" quel gentil Bébé ! dit elle . tu as tout rangé directement dans le panier ! "

maman grimpe sur le mototaxi
bébé s' endort .
" pauvre Bébé ! dit Maman . et en plus il n' a rien eu à manger ! "