Emma et le Père Noël
la grand mère d' Emma lui montre les belles maisons le long de la rue , les jardins , les balcons , les frises .
c' est beau , n' est ce pas ?
dit grand mère .
oui , oui , dit Emma , énervée et fatiguée de lever la tête chaque fois que Grand mère lui montre encore une porte et un portail .
elle aimerait arriver et cesser ce défilé de murs .
elle n' en peut plus .
et regarde cette belle maison là haut !
mais !
ça ne m' intéresse pas les maisons !
qu' est ce qui t' intéresse alors , Emma ?
les jouets , les cadeaux et les surprises !
et quoi encore ?
le Père Noël .
dans sa chambre , Emma prend un des catalogues de jouets .
c' est son livre préféré !
avec un feutre , elle entoure chaque image de jeux et de jouets qu' elle veut .
après elle enverra toutes ces pages au Père Noël dans une grande enveloppe .
elle l' a déjà préparée en dessinant des coeurs et des fleurs dessus .
elle met en premier l' image la plus importante , celle de la balançoire .
c' est ce qu' Emma veut le plus au monde .
se balancer , décoller , voler , elle adore !
maman , tu crois que le père Noël va m' apporter la balançoire ?
je pense que c' est trop gros pour mettre dans sa hotte .
mais il l' a quand même apporté à Anna Noël dernier .
ça vient dans une boite et puis son papa l' a monté .
oui mais Emma , il n' y a pas assez de place dans notre tout petit jardin .
on la met dans le salon sinon ...

et où va t on s' asseoir ?
alors dans ma chambre !
et où vas tu te coucher ?
Emma fait le tour de la maison .
j' ai une idée maman : on met la balançoire dans ta chambre et toi et papa vous pouvez dormir sur le canapé lit au salon .
je ne pense pas que le père Noël va être d' accord .
mais , il est gentil le père Noël !
c' est qui le père Noël d' après toi ?
c' est le gros bonhomme en rouge , toujours souriant ...
comme moi puisque tu m' as dit qu' il faut être gracieuse .
et s' il ne souriait pas , tu l' aimerais quand même ?
oui , il est quand même gentil .
et s' il était un voyou , tu l' aimerais quand même ?
oui , il y a des voyous gentils aussi .
tu n' as qu' à voir Gaëtan en classe !
et s' il était un voleur , tu l' aimerais quand même ?
mais !
ce n' est pas un voleur le père Noël , qu' est ce que tu racontes ?
tu as raison Emma .
mais je pense que le père Noël est gentil aussi pour les parents et il ne voudrait pas nous mettre dehors de notre chambre .
Emma est troublée .
elle regarde le grand cerisier qui prend toute la place dans le jardin .
est ce qu' on a vraiment besoin d' un cerisier ?
se demande t elle .
une balançoire est beaucoup plus importante !
Emma regarde le catalogue encore et encore , mais elle n' arrive pas trouver une seule chose qu' elle voudrait autant qu' une balançoire .
la nuit au lit elle compose une lettre dans sa tête au père Noël .
" cher père Noël ,
comme tu sais puisque tu sais tout , je suis très sage ( et je fais bien mes pages dans le cahier des moyens à l' école ) .
je sais que le cadeau que je veux est très difficile et lourd .
c' est un problème .
mon papa dit que quand on a un problème , on réfléchit .
et comme tu es très , très , très intelligent , je sais que tu vas trouver un moyen de me la donner .
merci , à bientôt , je t' attends .
et je sais aussi que je ne peux pas encore écrire et envoyer cette lettre , mais tu es tellement fort que tu lis mes pensées .

contente et en paix , Emma s' endort .
elle est encore soucieuse le matin .
qu' est ce qu' il y a Emma ?
demande son père .
je m' inquiète pour le père Noël .
il a la barbe blanche , il est vieux , est ce qu' il va pouvoir m' apporter ma balançoire ?
son papa est tout aussi troublé .
peut être pas cette année Emma , mais un jour !
quand alors ?
on verra ...

Emma déteste quand ses parents disent " on verra " .
ça ne veut rien dire , ou ça veut dire " va te faire cuire un oeuf ! "
Emma s' occupe à faire des cadeaux pour ses grand mères et ses parents .
elle dessine des balançoires sur chaque feuille et les signe " Emma " .
la veille de Noël , il n' y a rien d' autre dans sa tête qu' une balançoire qui lui donne un peu le mal de mer dans son lit .
le matin de Noël , il n' y a que de paquets petits sous l' arbre .
Emma regard par la fenêtre .
le vieux cerisier est toujours là .
elle jette un coup d' oeil dans la chambre des parents .
leur lit est toujours là .
elle n' a pas d' appétit , même pour les gâteaux de Noël .
viens vite Emma .
habille toi .
on va chez grand mère .
Emma n' a pas envie d' être sage .
elle est trop déçue .
mais elle suit ses parents .
et là , arrivée chez grand mère , elle la voit : la balançoire est installée dans le grand jardin de grand mère .
comme il est intelligent le père Noël .
il a trouvé comment faire !
Emma pense : ce n' est pas la peine de vouloir ce qu' on ne peut pas avoir .
mais dès fois ça marche !