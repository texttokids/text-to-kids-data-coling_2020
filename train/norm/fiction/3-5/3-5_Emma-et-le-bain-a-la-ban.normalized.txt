Emma et le bain à la banane
quelque fois le dimanche matin , Emma fait une matinée de beauté .
elle déteste se laver la tête , mais elle adore se mettre des crèmes en pinçant des tubes ou en versant des pommades parfumées sur tout son corps .
elle collectionne des petites bouteilles de shampoing et des savonnettes que mamie lui apporte des hôtels quand elle voyage .
Emma accepte donc le shampoing sans crier , sans pleurer .
elle chante même dans son bain :
" bulles de savon , li la la
le bain est bon , lu la la
dans le fleuve , lé la la
pour une peau neuve , lo la la .

elle n' est pas seule dans le bain car il y a la famille de canards , des gobelets gigognes , une grenouille et une baleine , mais pas de requin .
et dans sa tête il y a Antonin , Guillaume , Anna et Mamie .
mais quand le téléphone sonne , toute la bonne humeur d' Emma s' évapore .
sa mère parle et parle et parle et du coup , même avec les canards et la baleine , Emma se sent seule .
" maman ! " crie t elle .
" attends Emma ... "
" maaaamaaan ! " hurle t elle .
" je suis au téléphone ! "
" maaaaaaaaaaaamaaaaaaaaaan ! ! ! ! ! ! ! "
" deux secondes , Emma ! "
" maman , j' ai faim . "
maman raccroche et vient dans la salle de bains .
" maman , je veux une banane . "
tu mangeras quand tu seras sortie du bain et habillée .

" maman ! une banane ! dans le bain ! "
" toute à l' heure , Emma . "
" tout de suite ! " pleurniche t elle .
" Emma ! " sa mère essaie de la raisonner .
" tu ne peux pas manger une banane dans le bain . elle peut tomber dans l' eau sale pleine de savon . "
Emma n' est pas d' accord .
pourquoi la banane tomberait elle ?
elle mange bien des bananes dans la rue et elles ne tombent pas par terre .
la seule réponse d' Emma est de faire une crise .
elle remplit le bain de ses larmes .
sa mère en a marre .
" calme toi ! "
" une banane ! " pense Emma .
" ce n' est pas la mer à boire . "
tout d' un coup sa mère revient avec ...
une banane .
" on fera l' expérience ! " elle se met à peler la banane .
" non , c' est moi qui l' épluche ! "
" bon Emma ! aujourd'hui c' est toi qui commandes . que veux tu d' autre dans le bain ... un ananas , une noix de coco , une mangue , une pastèque ? "
" non , juste une banane ! "
Emma l' épluche , elle tient la peau de banane dans une main et la banane dans l' autre .
elle croque et sa bouche se remplit du gout de la banane .
c' est bien la chose la plus délicieuse qu' elle ait jamais mangée .
et dans le bain !
elle mord encore .
mais cette fois , la banane lui glisse de la main et tombe à l' eau .
elle flotte un moment et puis se noie .
maman ne dit rien , mais elle ne dit rien si fort qu' Emma entend .
sa mère est contrariée .
Emma sort du bain sans se faire prier .
" regarde ma peau Maman comme elle est belle .
un bain à la banane , ça rend belle .
une banane , ça bouche aussi la baignoire , pense maman .
" c' était la dernière banane dans la maison . "
" mais il y en a d' autres au marché Maman ! "