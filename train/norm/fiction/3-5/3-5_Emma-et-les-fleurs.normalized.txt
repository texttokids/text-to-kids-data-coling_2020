Emma et les fleurs
fleur
ce n' est pas parce qu' elle s' appelle Fleur qu' elle aime les fleurs .
ou peut être que si .
il n' y a pas plus belle qu' une fleur , ou encore mieux , des fleurs , un jardin fleuri , une place fleurie et même un rond point fleuri .
son magasin préféré n' est pas une pâtisserie , ni une boutique de bonbons , ni un magasin de jouets .
c' est chez le fleuriste et elle peut rester un long moment à regarder les plantes et les bouquets .
" comme les fleurs sont belles ! " dit elle à sa mère .
c' est vrai que les bouquets sont magnifiques , mais ça lui fait quand même de la peine qu' on les coupe et puis ils ne vivent qu' un bref moment et puis les pétales tombent et pourrissent et puis elles meurent .
ça lui fait tellement de peine qu' elle préfère ne pas les avoir chez elle .
sa mère lui dit : " rien ne vit pour toujours , même pas nous . "
" oui , mais on vit quand même plus longtemps que des fleurs coupées , " dit Fleur .
les fleurs savent qu' elles n' ont pas une vie longue et il faut en profiter chaque seconde .
fleur pense que c' est une bonne idée .
elle se met devant son bouquet et décide qu' elle ne va même pas dormir de peur de perdre une précieuse minute .
sauf que ses yeux se ferment tous seuls et elle se retrouve dans son lit le lendemain matin .
c' est peut être les fleurs qui l' ont portée jusque là .
" les fleurs ne sont pas toutes belles . les glaïeuls , par exemple , sont moches . et tu sais , ma fleurette , chaque fleur a un nom . "
" c' est comme nous , on ne nous appelle pas tous " femme " ou " fille " ou " garçon " .
" alors pourquoi tu m' as nommé Fleur et pas Giroflée ou Myosotis ou Muguet ou Jonquille ou Hibiscus ou Anémone ? "
" parce que pour moi , tu es toutes les fleurs en une seule fille . "
fleur est contente d' être tout un bouquet à elle toute seule .