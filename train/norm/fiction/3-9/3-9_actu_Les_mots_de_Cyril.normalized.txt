Cyril est un beau garçon qui adore aller à la plage .
en toutes circonstances , même avec ses amis , il est élégant et bien coiffé .
Cyril a toujours le plus joli maillot de bain .
mais il est aussi le premier à aimer faire des grimaces .
soudain sa copine Simone , lui dit : " jouons à un jeu . on écrit des lettres dans le sable et on essaye de les deviner " .
" ouais c' est génial ! mais ce serait plus drôle si on avait d' autres enfants qui jouaient . " , dit Cyril .
" attends je vais prendre un plot pour qu' on m' entende mieux . on recherche des copains pour jouer avec des lettres sur la plage ! "
quelques minutes plus tard , Cyril revient avec trois copains .
le jeu commence et Simone écrit la lettre " T " dans le sable .
puis elle murmure le mot à découvrir à l' oreille de Cyril .
un premier enfant dit : " un T' irate " .
" non un Pirate prend un P " s' exclame Cyril en riant .
" un trampoline ! " , crie un deuxième en sautant .
" non plus . " répond Cyril .
de plus en plus de monde se retrouve autour de Cyril .
Simone recoiffe Cyril pour qu' il soit toujours aussi beau .
elle en profite pour lui faire un bisou .
l' un des enfants trouve enfin le mot juste : c' est une tour !
le public applaudit très fort .
Cyril propose alors à tous ses nouveaux copains d' aller manger une glace .
gourmand , il prendra le plus grand des cornets .
" j' en ai même pas mis partout ! " dit il fièrement à Simone avec les doigts plein de fraise .