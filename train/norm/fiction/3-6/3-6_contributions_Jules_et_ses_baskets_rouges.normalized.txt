jules est passionné par le football .
il regarde tous les matchs de Messi , son joueur préféré , à la télévision .
et ce samedi a lieu un match très important pour lui et son équipe .
il vont rencontrer l' école voisine qui est leur principale rivale .
mais aujourd'hui , à l' entraînement , Jules est triste .
ses baskets sont abimées et usées .
il y a des trous et elles lui font mal aux pieds .
alors Jules reste assis sur le banc et ne veut plus jouer au football .
" que t' arrive t il ? " , lui demande son entraineur .
" mes baskets sont usées et j' ai peur de rater le match de samedi . " , répond Jules , tête baissée .
le jour du grand match est arrivé , mais au réveil , Jules n' a toujours pas retrouvé le sourire .
il se lève de son lit et trouve une boite posée à ses pieds .
qu' est ce que cela peut bien être ?
jules ouvre la boite et découvre une magnifique paire de baskets rouges !
" quelles sont belles ! " , s' écrie Jules .
il les enfile immédiatement et court voir sa maman pour la remercier de ce joli cadeau .
c' est maintenant l' heure du grand match .
jules s' échauffe et se prépare avec son équipe pour la rencontre .
ils sont très concentrés .
toutes leurs familles sont là pour les encourager .
le match commence .
jules s' élance après le ballon et court , court très vite pour le rattraper .
mais à ce moment là , un joueur de l' équipe adverse se jette vers lui pour lui faire un tacle .
jules trébuche et tombe !
aïe aïe aïe , il a mal au genou !
jules est blessé , au sol .
mais il décide de ne pas se décourager .
il regarde ses belles baskets rouges qu' il aime tant et se dit qu' il ne doit rien lâcher pour sa maman .
il se relève , frotte ses genoux et repart !
il court encore plus vite après le ballon .
tellement vite que les autres joueurs n' arrivent plus à le rattraper .
il tire et marque un but !
tout le monde l' applaudit , surtout sa maman , dans les gradins , qui est fière de son petit garçon courageux .
jules est heureux , son équipe a gagné le match grâce à ses baskets magiques et sa maman .