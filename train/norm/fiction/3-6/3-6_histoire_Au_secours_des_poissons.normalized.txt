ronron , le petit cochon se balade dans la ferme .
il adore flâner et siffler pour le plus grand plaisir des autres animaux .
quand , tout à coup , des petits cris se font entendre .
" on dirait que ça vient de la marre aux poissons " se dit Ronron .
il décide d' aller voir à toute vitesse .
mais qui est là en train de plonger son bec dans la marre pour attraper les poissons ?
" que fais tu là ? dit il à Jean le héron "
ronron voit des bulles se former dans l' eau .
" au secours , venez nous aider " s' écrient les petits poissons .
" arrête Jean , laisse les poissons tranquilles " .
dit Ronron " nous avons décidé de ne toucher à aucun animal dans la ferme . "
" ne t' occupe pas de ça Ronron , dit Jean en ricanant , voici mon repas de midi . " " peut être mais il s' agit des poissons de la ferme , et on ne touche pas aux poissons de la ferme ! " s' écrie Ronron .
ronron ne se laisse pas faire .
il prend son élan et bouscule le héron qui tombe dans la marre aux poissons .
" vilain petit cochon , mon plumage est tout mouillé maintenant " .
tout à coup , tous les poissons se regroupent autour du héron et viennent tirer ses plumes .
" aie , aie , aie , vous me faites mal , arrêtez , arrêtez ! "
le héron arrive à se dégager et prend son envol en râlant .
les poissons s' amusent avec le petit cochon .
ils sont tranquilles pour un bon moment .
" merci Ronron , heureusement que tu étais là ! " .