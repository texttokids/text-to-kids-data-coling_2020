il était une fois une grande famille .
il y avait les grands parents , les parents , leurs deux enfants , une fille et un garçon , leurs cousins et leur chien Max .
les enfants !
annonça la mère .
savez vous quel jour nous sommes aujourd'hui ?
c' est le fameux Lundi de Pâques !
à cette occasion , un mystérieux lapin est venu cette nuit déposer des oeufs magiques partout dans le jardin ...
mais attention !
tous les oeufs sont cachés !
nous allons organiser une chasse au trésor .
le premier qui récoltera le plus d' oeufs colorés aura gagné !
vite tous au jardin !
s' écrièrent tous les enfants .
la petite Pauline court au pied de l' arbre et découvre trois magnifiques oeufs rouges .
le petit Valentin court vers les pots de fleurs et réussit à trouver deux gros oeufs bleus .
le petit Pascal s' empare du nain de jardin et le secoue pour voir si un oeuf sort de son chapeau .
mais le nain de jardin est vide !
alors les enfants , avez vous trouvé des oeufs magiques ?
demande le père .
j' ai trouvé deux oeufs bleus !
s' écrie Valentin .
j' ai trouvé trois oeufs rouges !
s' écrie Pauline .
bravo Pauline tu as gagné !
mais Pascal tu n' as rien trouvé ?
demande la mère .
moi je n' ai rien trouvé ...
le nain de jardin était vide !
mais le nain de Jardin cachait un oeuf jaune !
qui a bien pu le manger ?

c' est Max !
tout le monde se tourne vers le chien Max qui a le museau recouvert d' un papier jaune .
l' oeuf en chocolat est caché sous ses pattes .
mais maman pourquoi ce sont des oeufs magiques ?
ouvre ton oeuf et tu verras ...
et oui le lundi de Pâques , il y a des oeufs en chocolat !
tous les enfants crient de joie et se mettent à dévorer les chocolats .