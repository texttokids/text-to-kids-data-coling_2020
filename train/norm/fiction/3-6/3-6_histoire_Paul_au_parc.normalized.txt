le mercredi , Paul se rend chez sa grand mère .
paul adore ce jour car il sait qu' il va lire un livre dans le fauteuil moelleux de son grand père .
mais il sait surtout qu' il va pouvoir se régaler en allant faire quelques courses avec sa grand mère .
sur le chemin , il y a un camion à crêpes , et Paul déguste toujours une crêpe au sucre .
hum ...
un délice !
mais le moment qu' il attend avec impatience arrive très vite .
aller au parc et retrouver les copains du mercredi !
alors en arrivant devant l' entrée , dès qu' il voit le toboggan , Paul observe ceux qui sont présents .
il y a Lily , Claire , Léon , Tom et plein d' autres copains et copines .
ensemble ils vont jouer dans le bac à sable , grimper à l' échelle , essayer de s' attraper .
paul va même jouer au pirate en imaginant qu' il est comme " Peter Le Pirate " .
et puis vient le moment de rentrer , Paul n' est pas toujours content mais il sait que sa grand mère lui a préparé son plat préféré .
en arrivant à la maison , son grand père lui sert des pâtes à la sauce tomate .
hum un régal .
" c' est cool d' être chez ses grands parents ! " dit il .