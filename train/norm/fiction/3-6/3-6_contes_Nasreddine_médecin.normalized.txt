Nasreddine Hodja avait envie d' apprendre la médecine .
il alla voir le médecin le plus célèbre de sa ville et lui fit part de son désir : " tu tombes bien , lui dit le médecin , je vais visiter quelques malades , viens avec moi , tu pourras ainsi apprendre le métier sur le terrain . "
Nasreddine accompagna le médecin chez le premier malade .
le médecin regarda à peine le patient et lui dit : " ton cas est très simple : ne mange plus autant de cerises , bois une tisane avant de dormir et demain tu seras guéri . "
Nasreddine Hodja était plein d' admiration .
dans la rue , il ne tarit pas d' éloges : " ô ! maître , vous êtes vraiment un grand médecin ! comment , sans toucher le malade , avez vous pu deviner de quoi il souffrait ? "
" c' est très simple , lui répondit il , j' ai regardé sous le lit et j' ai vu qu' il y avait un gros tas de noyaux de cerises . j' en ai déduit qu' il en avait trop mange . "
le Hodja se dit que la médecine était plutôt simple et qu' il pouvait l' exercer à son tour .
il se déclara médecin et , dès le lendemain , alla visiter son premier patient .
il entra , regarda sous le lit et ne vit que les vieilles babouches du malade .
" ton cas est simple , lui dit il , ne mange plus autant de babouches , bois une tisane avant de dormir et demain tu seras tout à fait guéri . "