la princesse Violette est impatiente , car ce soir a lieu le bal masqué et elle a enfin l' âge de s' y rendre .
son père le roi Hubert et sa mère la reine Velda organisent chaque année un bal masqué en l' honneur de tous les amis du royaume .
violette a toujours rêvé d' y aller et de pouvoir s' amuser avec ses amis .
elle enfile une magnifique robe rouge à volants , se pose sur le visage un masque de la même couleur , puis elle descend au salon où se passent les festivités .
elle y retrouve Ronan , Zena et Chloé , ses amis de toujours .
" comme tu es belle Violette dans cette robe ! " complimente Ronan .
" oh , oui et ton masque comme il brille ! " lance Zena .
les quatre amis dansent ensemble , ils s' amusent beaucoup .
ils essayent de deviner qui se cache derrière les masques tout en dégustant des hors d' oeuvres .
violette aperçoit Gaspard qui vient de passer la porte du salon , elle l' a invité à participer à cette soirée .
le garçon porte un élégant costume qui a été confectionné par la couturière du château .
c' est la maman de Violette qui le lui a offert pour son anniversaire .
" bonsoir Gaspard , tiens , je t' ai gardé un masque " " merci Princesse , tu es vraiment très jolie , ta robe te va très bien ! " dit il en enfilant son masque .
violette rougit et attrape Gaspard par le bras pour aller danser .
la reine Velda , arrive à la hauteur du groupe d' amis et déclare d' une voix douce qu' il est l' heure d' aller au lit .
violette salue sa petite troupe .
elle monte les escaliers du château en direction de sa chambre avec pleins de merveilleux souvenirs en tête .