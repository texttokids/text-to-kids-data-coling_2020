pourquoi je dois faire attention dans la cuisine ?
parce qu' il y a un monstre caché dans le frigo ?
parce que ça sent mauvais ?
parce que je peux me bruler la main sur la casserole ?
parce que je peux me coincer les doigts dans le placard ?
parce que je peux me cogner fort sur le coin de la table ?
parce que je risque de renverser la crème par terre ?
parce que la fumée me fait tousser ?
la cuisine est pleine de dangers .
tu dois rester assis à table , et regarder sans toucher pour ne pas te blesser .
si je ne peux pas toucher , je peux au moins gouter ?