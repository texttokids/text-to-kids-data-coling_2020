aujourd'hui , c' est l' anniversaire de Nala , une grande copine de Ron Ron !
et Ronron a envie de faire une belle surprise à la chèvre .
il réunit tous les animaux pour organiser une fête .
" si je vous ai réunis , c' est pour organiser une surprise à notre amie Nala . et j' aimerais que vous m' aidiez . " explique Ronron .
" vous , les poissons et les grenouilles , vous allez inventer une jolie chanson pour notre amie Nala . " pascal " le cheval " et Patí " la vache " iront cueillir des fleurs et de l' herbe bien fraiche pendant que j' emmènerai Nala se promener pour l' éloigner de la ferme " .
tout le monde est ravi de participer .
pendant que Ronron se promène avec Nala , tous les animaux se mettent au travail .
au retour des deux amis , tout est prêt pour la surprise .
les grenouilles et les poissons chantent en choeur " joyeux anniversaire Nala ! " .
et les amis se réunissent pour déguster ensemble la motte d' herbe et de fleurs offerte à la petite chèvre .
Nala est aux anges , et Ronron , lui , est très fier de sa surprise .