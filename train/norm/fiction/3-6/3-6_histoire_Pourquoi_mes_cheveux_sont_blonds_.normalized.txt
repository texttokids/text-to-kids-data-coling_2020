pourquoi mes cheveux sont blonds ?
parce que j' ai mangé trop de tarte au citron ?
et pourquoi ma copine Clémentine a les cheveux oranges ?
parce qu' elle a trop mangé de carottes ?
et pourquoi Tom a les cheveux tout noirs lui ?
parce que ses cheveux sont très sales ?
quand ton corps s' est formé dans le ventre de Maman , il s' est rempli de mélanine , comme de la peinture .
plus tu as de mélanine , plus tes cheveux seront sombres .
et moins tu as de mélanine , plus tes cheveux seront blonds ou clairs .
et pourquoi Tom il a beaucoup de mélanine et pas moi ?
les parents de Tom ont surement les cheveux noirs , du coup il avait beaucoup de chances d' avoir aussi les cheveux foncés .
et pour toi , regarde Maman , elle a les cheveux blonds comme toi et c' est très joli !
tu penseras à donner un peu de mélanine à Maman pour qu' elle en mette dans ma petite soeur qui est dans son ventre , car elle m' a tout donné déjà !