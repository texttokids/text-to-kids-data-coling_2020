pourquoi il pleut ?
parce que le soleil pleure sur nous ?
parce qu' un géant nous fait pipi dessus ?
ou bien c' est la lune qui arrose les arbres de la terre avec un arrosoir ?
la pluie , ce sont des milliers de petites gouttes d' eau qui font la taille d' un grain de riz qui tombent .
elles tombent des nuages gris que tu vois dans le ciel .
l' eau de la mer monte dans les nuages blancs , et ils l' absorbent comme une éponge .
quand ils sont trop lourds , les gouttent se jettent dans le vide et atterrissent sur nos têtes .
puis les gouttes retournent dans le ciel , retombent et ainsi de suite .
ça a l' air bien d' être une goutte d' eau !