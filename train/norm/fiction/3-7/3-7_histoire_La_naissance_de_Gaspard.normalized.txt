bonjour , moi c' est Gaspard .
et voici Jean Étienne .
c' est mon papa .
papa est ce qu' on appelle un garçon de bonne composition .
toujours de bonne humeur et très créatif , il adore les nouvelles aventures .
et bien il va être servi !

il y a quelques mois , quand ma maman , Émilie , lui a annoncé au petit déjeuner , qu' il serait bientôt papa , il a failli s' étrangler avec sa tartine .
s' étrangler , oui , mais de bonheur , je vous rassure .
et voilà le début de notre belle aventure !
c' est que c' est un sacré projet l' arrivée d' un bébé !
il faut penser à des tas de choses : la maison et la voiture sont elles assez grandes ?
comment va t on décorer ma chambre ?
quel endroit choisir pour ma naissance ?
etc ...
sans parler de l' achat des vêtements , des biberons et des couches !

maman et papa ont été drôlement occupés avant mon arrivée !
pendant ce temps là , bien au chaud dans le ventre de maman , je grandissais doucement .
à la fois bercé par la voix de ma maman et les caresses de mon papa .
c' était drôle de jouer à " toc , Toc , Toc " .
comment ça !
vous ne connaissez pas ce jeu ?
mais si !
vous ne vous en souvenez pas , c' est tout ...
je vais vous réexpliquer les règles .
vous allez voir , c' est très simple .
tout d' abord , mon papa s' approche du ventre de ma maman .
il murmure mon prénom : " Gaspard ? eh , Gaspard ! tu veux jouer à " toc , Toc , Toc " avec moi ? " comme j' adore jouer avec mon papa , je donne un grand coup dans le ventre de ma maman et le jeu peut commencer !
papa tapote , doucement , le ventre de maman et moi je dois donner un coup de pied , de tête ou de poing à l' endroit où papa a mis sa main .
il m' est même arrivé de donner un coup de ...
fesses !
rassurez vous , ça ne fait pas mal à ma maman car , quand on est un tout petit bébé , on a aussi de toutes petites mains , de tous petits pieds et , même , de toutes petites fesses !
le jeu s' arrête quand l' un de nous en a assez .
et puis , les mois ont passé et me voilà enfin !
Gaspard , le plus beau bébé du monde !
je n' invente rien , c' est maman qui l' a dit .
de retour à la maison , plein de gens viennent me voir les bras chargés de cadeaux .
il y a ceux qui disent à papa : " oh , Jean Étienne , c' est ton portrait craché ! " et ceux qui disent à maman : " Émilie , comme Gaspard te ressemble ! "
moi , ça m' est égal !
la seule chose qui compte , c' est tout l' amour et la tendresse que papa et maman me donnent chaque jour quand je suis tout contre eux .