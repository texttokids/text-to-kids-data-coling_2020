il est midi , l' heure du déjeuner .
et Margot a très faim .
" maman , on mange quoi à midi ? " demande la petite fille .
" je prépare des cordons bleus avec des petits pois Margot " répond sa maman .
" oh non ! " , dit Margot en grimaçant , " j' aime pas les petits pois " .
" tu en mangeras un peu , c' est important de manger des légumes . en attendant que le repas soit prêt , va mettre la table s' il te plait " .
margot fait la tête , elle n' a pas envie de manger des légumes à midi .
la petite fille met la table , tandis que sa maman appelle tout le monde pour le déjeuner .
elle sert l' enfant qui fait toujours la tête .
" mange au moins une cuillère Margot " dit le papa .
" mais je n' aime pas ça ! pourquoi faut il manger des légumes ? "
" tu sais Margot , quand j' étais petit , moi non plus je n' aimais pas les légumes " , raconte son Papa .
" et puis , dans une cuillère de petits pois , tu as pleins de bonnes choses qui t' aident à avoir de l' énergie . "
" ton corps a besoin de légumes pour grandir et bien se porter " dit la maman .
" tu ne veux pas être grande et forte ? " margot écoute , baisse la tête et répond tout doucement : " si ... " .
papa prend l' assiette de Margot et place les petits pois de manière à dessiner un bonhomme qui sourit .
margot rigole en voyant son assiette et dit " je vais lui manger le nez ! " et Margot prend une cuillère de petits pois .
" maintenant je vais lui manger les yeux ! " petit à petit , les petits pois disparaissent de l' assiette .
margot a fini tous ces légumes verts , sans grimacer , et en s' étant bien amusée !