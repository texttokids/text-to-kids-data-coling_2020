la nuit s' étend à l' infini et le silence se fait , c' est un instant féerique .
chaque petite maison du village s' illumine et la magie de Noël va commencer .
derrière chaque fenêtre allumée , règne une atmosphère unique , propre à Noël : un délicieux mélange d' effervescence , d' excitation mais aussi de douceur , de paix et de communion .
chaque famille prépare sa veillée de Noël .
dans la campagne et sur les toits , tout est blanc , la neige a fait son apparition pour le bonheur des petits et des grands et dans ce bel univers ouaté , chaque enfant attend le père Noël .
dans un petit chemin de traverse , bordé d' arbres , posés là comme dans un livre d' image , deux petits chaussons roses attendent tristement que quelqu' un veuille bien les emporter pour participer à cette belle fête natale .
dans la maison de la famille Flori , les deux enfants , Maxime et Alice se chamaillent , se bousculent et s' impatientent car leurs parents ne sont pas tout à fait prêts pour descendre réveillonner chez les grands parents .
" maman , on peut sortir ? on voudrait regarder la neige , les lumières , les guirlandes des autres maisons ! " " allez , on peut s' il te plait ? on met nos manteaux , nos écharpes et on promet de faire attention à nos beaux habits de fête ! " dit Maxime .
c' est l' aîné , il a huit ans et sa soeur cinq ans .
hum , je ne sais pas si je peux vous faire confiance , je n' ai pas envie de vous retrouver trempés !
s' il te plait maman , c' est trop beau dehors et peut être que je vais voir passer le Père Noël et son traineau ou bien entendre les clochettes des rennes !
dit Alice .
bon d' accord , mais vous ne vous éloignez pas car il y a beaucoup de neige et je ne veux pas vous voir tout crottés pour aller chez Papy Mamie .
merci Maman !
les petits enfilent vite leurs manteaux , enroulent leurs écharpes et sortent ravis !
oh !
dit Maxime .
regarde la maison de Baptiste comme elle est illuminée , des guirlandes de toutes les couleurs sur le mur de sa maison et le Père Noël qui s' accroche à sa fenêtre !
moi , je préfère celle de Maëva , car elle a toutes les décorations du Père Noël : les rennes , le traineau , le Père Noël et sa hotte et des guirlandes de toutes les couleurs !
les enfants restent là , émerveillés .
maxime s' avance dans l' allée et observe tout ce qui l' entoure .
et là , blottis comme dans un écrin , les deux petits chaussons roses en porcelaine attendent ...
Alice , viens voir ce que j' ai trouvé !
oh !
comme ils sont mignons !
et si on les emportait avec nous , on les mettrait au pied du sapin chez Papy Mamie , un chausson pour chacun et je suis sure que le Père Noël sera encore plus gentil avec nous !
les petits chaussons , eux , n' en reviennent pas : ils avaient froid , ils étaient tristes , ils se sentaient abandonnés .
et les voilà transportés , caressés , réchauffés et ils sont l' objet de toutes les attentions .
peut être que c' est ça la magie de Noël !
alors Joyeux Noël à tous , et que cette nuit de Noël vous apporte le bonheur d' être aimé !