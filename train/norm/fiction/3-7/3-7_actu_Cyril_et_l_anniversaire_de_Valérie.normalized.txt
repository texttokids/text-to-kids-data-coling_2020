ce matin , comme tous les matins , Cyril arrive à l' école .
il retrouve tous ses copains dans la cour .
jean luc , son meilleur ami est là aussi .
jean luc est très chic aujourd'hui .
il a une jolie chemise , il est peigné et a même un bouquet de fleurs .
Cyril éclate de rire : " pourquoi tu es habillé en pingouin aujourd'hui ? "
jean luc lui répond : " c' est parce que c' est l' anniversaire de Valérie . je lui ai apporté des fleurs . et toi ? "
Cyril repense à Valérie .
comme elle est jolie , c' est la plus jolie fille de la classe .
il est très amoureux d' elle .
mais Cyril avait oublié que c' était l' anniversaire de Valérie !
il ne veut pas que Jean Luc lui fasse un plus beau cadeau que lui !
alors Cyril réfléchit pendant que tout le monde rentre en classe .
" pense , pense , Cyril , tu dois trouver une super idée qui fera que Valérie tombera amoureuse de toi ! "
c' est l' heure du gouter .
toute la classe a préparé des dessins , des bonbons pour Valérie .
chacun vient la voir et lui fait un cadeau .
Valérie est très contente .
mais c' est le tour de Cyril et il ne sait pas quoi faire !
tremblant , il s' approche de Valérie et se met à danser !
il fait la danse de l' épaule en tournant sur lui même pour faire sourire la jolie Valérie .
tout le monde est surpris , mais Valérie éclate de rire , applaudit et danse avec lui .
Cyril est vraiment un drôle de garçon , mais il lui a fait le cadeau le plus rigolo , Valérie l' aime beaucoup !