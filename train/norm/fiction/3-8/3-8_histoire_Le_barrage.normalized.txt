un matin , au coeur de la savane , Arthur le lion , Roi des animaux , est réveillé par le cri des singes .
" sire , Sire , tu dois venir " lui disent il .
" vite , vite ! "
" que se passe t il pour que vous veniez troubler mon royal sommeil ? " demande le Roi .
" la tempête a bloqué la rivière et les animaux n' ont plus d' eau ! " expliquent les singes .
" la rivière devrait être pleine , je ne comprends pas ! " dit le roi Arthur .
" je vais rendre visite aux hippopotames sur le champ ! "
en arrivant devant la rivière des hippos , plus une seule goutte d' eau .
Arthur demande à l' aigle d' aller voir ce qu' il se passe en amont de la rivière .
quelques minutes plus tard , l' aigle revient .
" la rivière est bloquée . un éboulement de pierres et de troncs d' arbres a causé un immense barrage . "
" il faut agir ! " dit Arthur .
" les hippos , les éléphants , les singes , venez nous aider ! nous devons débloquer l' eau de la rivière . "
tous les animaux entament une longue marche pour aller jusqu' au barrage naturel .
les éléphants parlent aux hippopotames , les singes avec les lions .
toute la savane est réunie .
Arthur , en bon chef , propose aux animaux un plan .
" les singes , vous allez récupérer des morceaux de lianes pour en faire des cordes . les éléphants , vous allez tirer les morceaux de bois et les pierres . les hippos , vous pousserez les pierres et aiderez les éléphants . "
" un , deux , trois , allez y ! " tous les animaux sont solidaires et font d' énormes efforts .
peu à peu , l' eau commence à couler à nouveau jusqu'à ce que les dernières pierres cèdent .
" bravo , Bravo ! " les cris de joie des animaux résonnent partout dans la savane .
la rivière coule à nouveau .
tous les animaux se désaltèrent tranquillement .
et Arthur , le Roi de la savane s' endort paisiblement à l' ombre d' un Baobab .