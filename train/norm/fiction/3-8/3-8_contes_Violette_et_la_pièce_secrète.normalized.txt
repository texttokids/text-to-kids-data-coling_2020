un après midi , Violette se promène dans le château .
elle emprunte plusieurs escaliers , traverse des couloirs qu' elle ne connait pas vraiment , et découvre une grande porte en bois .
elle pousse la porte , celle ci fait beaucoup de bruit en s' ouvrant , ce qui effraie un peu la princesse !
mais la curiosité de Violette est trop forte , elle veut savoir ce que cache cette pièce ...
elle passe la tête , la pièce est très sombre , il y a des draps blancs et de grandes ombres sur les murs .
violette fait un pas dans la pénombre mais un petit bruit de craquement la fait s' enfuir en courant .
violette veut savoir ce qui se cache dans cet endroit , mais elle n' est pas assez courageuse pour y aller seule .
elle réfléchit pour savoir qui pourrait l' accompagner sans être effrayé ...
" mais oui , GASPARD ! "
la princesse court à toute vitesse vers les écuries et raconte son histoire de pièce secrète à son ami Gaspard .
" je viendrai avec toi princesse , mais il faut que je finisse de nettoyer les boxes des chevaux avant ... " lui répond le garçon .
" je vais t' aider , on ira plus vite tous les deux " s' écrie Violette .
violette attrape de la paille et commence à en éparpiller dans le boxe .
" oups , pardon Gaspard je ne t' avais pas vu ... " s' excuse la princesse .
Gaspard relève la tête , il a plein de paille sur le dos .
" princesse c' est le boxe qu' il faut pailler , pas moi ! " plaisante t il .
les enfants rigolent et poursuivent leur travail .
" bien , nous avons terminé , allons voir la pièce secrète princesse ! " déclare Gaspard .
violette attrape Gaspard et une fois dans le château , ils se dirigent vers la grande porte en bois que Violette à découverte il y a maintenant quelques heures .
Gaspard pousse la porte , Violette se cache derrière lui .
le jeune garçon se dirige vers de grands rideaux et les tire , une lumière envahit la pièce et les ombres disparaissent aussitôt .
violette attrape un drap et le soulève pour dévoiler ce qui s' y trouve en dessous .
" ouah ! " les deux enfants ont les yeux et la bouche grands ouverts .
devant eux se trouvent un magnifique miroir orné d' or , Gaspard soulève un drap à son tour , une somptueuse coiffeuse en acajou puis une canne ornée de pierres précieuses ou encore un tas de draps de soie .
" le trésor du château ... " murmura Violette .
leur découverte est interrompue par la reine Velda qui se tient sur le pas de la porte .
" que faites vous là les enfants ? " demande t elle " regarde maman , c' est le trésor du château ! " annonce Violette les yeux toujours écarquillés par ce magnifique spectacle .
la reine Velda étouffe un petit rire .
" princesse , c' est ce qu' on appelle un héritage . ces meubles appartenaient à tes grands parents qui eux les ont reçu de tes arrière grands parents et un jour ils seront à toi et à ton mari ... ce sont des meubles qu' on transmet de famille en famille et ils sont très importants , nous les sortons seulement pour les grandes occasions , car nous ne voulons pas qu' ils soient abimés . " explique la reine .
en sortant de la pièce Violette regarde Gaspard et lui glisse à l' oreille : " un jour tous ses meubles seront à nous ... " et les joues de Gaspard se mirent à virer au rouge .