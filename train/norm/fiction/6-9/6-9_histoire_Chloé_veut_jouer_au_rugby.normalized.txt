depuis toute petite , Chloé est une petite casse cou .
pour le plus grand malheur de sa maman , elle passe son temps à grimper aux arbres et à se rouler dans l' herbe , en jouant à la bagarre avec ses copains de classe .
ses parents ont tout essayé : le judo , l' équitation , la natation , mais rien ne suffisait à canaliser toute son énergie .
mais il y a quelques mois , avec l' école , Chloé a découvert le rugby .
depuis , elle passe son temps à en parler .
à l' école au début , ses copains lui disaient : " mais le rugby c' est un sport de garçon ! " mais Chloé leur répondait : " si les garçons peuvent faire de la danse classique pourquoi je ne pourrais pas faire du rugby ! ? "
après de longues négociations , le papa de Chloé a réussi sa convaincre sa maman de l' inscrire dans un club .
depuis , elle s' entraine tous les mercredis après midi et fait même des matchs le week end avec son équipe .
si le Papa de la petite fille est ravi , sa maman , en revanche , a très peur .
mais Chloé , elle , elle n' a pas peur de rien !
parfois elle a des petits bobos , mais comme elle dit toujours : " un bisou de Maman et je n' ai plus mal ! "
et maintenant avec son Papa , ils ne loupent plus aucun match !
Chloé est une grande supportrice l' ASM de Clermont Ferrand alors que son papa supporte le Stade Toulousain .
quand les deux équipes jouent , dans le salon , il y a beaucoup d' ambiance !
la maman de Chloé , elle aussi , s' est mise à regarder les matchs de Clermont Ferrand .
mais La petite fille et son papa la suspectent de ne les regarder que pour apercevoir Wesley Fofana !