depuis plusieurs jours , il n' arrête pas de pleuvoir à Paris et un peu partout en France .
" c' est rigolo toutes ces flaques d' eau ! " s' exclame Louis qui s' amuse à sauter et s' éclabousser .
son papa lui dit : regarde Louis , le niveau de la Seine a monté , on ne distingue plus les quais qui sont inondés !
tu as vu cette voiture sous l' eau Papa ?
s' écrie Louis .
ce monsieur va avoir du mal à rentrer chez lui ...
de retour à la maison , le papa de Louis ne parvient pas à allumer les lumières .
papa , je ne vois rien du tout !
ne t' inquiète pas .
on va allumer des bougies et je vais appeler des techniciens .
quelques heures plus tard , on sonne à la porte .
ce sont deux grands messieurs en uniforme bleu et veste orange avec des grosses bottes de pluie .
bonjour !
nous venons réparer les installations électriques !
les techniciens se dirigent vers le compteur électrique et ouvrent leurs grosses boites à outils .
que faites vous ?
demande timidement Louis .
nous vérifions l' électricité .
quand il y a des inondations , il faut faire très attention que l' eau n' entre pas en contact avec l' électricité .
si c' est le cas , il faut couper le disjoncteur et on s' occupe de la suite !
tout d' un coup , la lumière réapparait dans la maison , les techniciens ont réussi à réinstaller l' électricité !
vous êtres forts !
s' exclame Louis .
moi aussi je veux devenir un super technicien !
merci !
de nombreuses familles nous attendent pour les aider , à bientôt !
louis regarde par la fenêtre .
tous les habitants du quartier sont dans la rue , et s' entraident pour sortir l' eau qui s' est infiltrée dans leurs maisons et leurs caves .
les pompiers sont là aussi , ils viennent secourir des personnes coincées chez elles .
heureusement , Louis a sa chambre à l' étage , ses jouets seront sauvés !
il faudra attendre encore plusieurs jours , puis le soleil reviendra et le niveau de l' eau redescendra .
ce sera enfin l' été !