il y a bien longtemps , avant que les hommes n' arrivent dans le pays , les arbres étaient capables de parler .
le bruissement de leurs feuilles était leur langage calme et reposant .
lorsqu' ils agitaient leurs branches en tous sens dans le vent violent , leurs paroles étaient des discours pleins de courage ou remplis de peur .
la forêt était peuplée d' une multitudes d' arbres de toutes sortes .
l' érable laissait couler sa sève sucrée pour les oiseaux assoiffés .
un grand nombre d' oiseaux nichaient dans ses branches .
les merles venaient déposer leurs petits oeufs bleus dans des nids bien installés .
l' érable les protégeait du vent et de la pluie , toujours prêt à rendre service .
il était respecté aux alentours .
pas bien loin de lui , un orme élevait ses longues branches vers le ciel .
l' orme aimait le soleil et chacune de ses branches s' élançaient vers ses rayons .
les orioles , des oiseaux ressemblant aux rouges gorge mais en plus petit , construisaient leurs nids balançoires dans sa ramure sachant qu' ils se trouvaient à l' abri dans les hauteurs .
plus loin encore , le thuya offrait durant l' hiver l' hébergement à des familles entières d' oiseaux .
lorsque le froid faisait rage , le thuya refermait ses épaisses branches sur eux et les gardait bien au chaud .
les oiseaux étaient si confortablement installés qu' ils mettaient du temps , le printemps venu , à quitter leurs logis dans le thuya .
le bouleau se tenait à peu de distance .
il était mince et élégant et son écorce douce et blanche le distinguait des autres .
ses bras souples et gracieux s' agitaient à la moindre brise .
au printemps , ses feuilles vert tendre étaient si fines qu' elles laissaient passer la lumière du soleil au travers .
quand les hommes arrivèrent dans ces lieux , ils se servirent de l' écorce du bouleau pour fabriquer des canots , des maisons et même les récipients dans lesquels ils cuisaient leurs aliments .
mais il arriva un jour que le bouleau , à cause de sa beauté , se mit à mépriser tout le monde .
le grand pin était le roi de la forêt .
c' est à lui que chaque arbre devait faire un salut en courbant la tête un peu comme on mani feste son obéissance au roi .
et ce roi était le plus grand , le plus majestueux , le plus droit de tous les arbres de la forêt .
en plus de sa taille , sa magnifique vêture vert foncé assurait son autorité .
un jour d' été , la forêt resplendissait des parfums et des cou leurs de milliers de fleurs et un éclatant tapis de mousse recouvrait les coins ombragés du sol .
une quantité d' oiseaux , des gros , des petits , des bleus , des gris , des jaunes et des rouges , n' arrêtaient pas de chanter .
les arbres bougeaient dou cement et agitaient leurs feuilles qui étaient des rires et des gais murmures de contentement .
l' érable remarqua que le bouleau ne participait pas à cette réjouissance collective .
es tu malade , bouleau ?
demanda le gentil érable .
pas du tout , répondit le bouleau en agitant ses branches de façon brusque .
je ne me suis jamais si bien senti .
mais pour quoi donc devrais je me joindre à vous qui êtes si ordinaires ?
l' érable , surpris de cette réponse , se dit que le roi Grand Pin ne serait pas content d' entendre de telles paroles .
car la pre mière tâche de Grand Pin était de faire respecter l' harmonie parmi ses sujets .
tais toi !
dirent les arbres au bouleau .
si le Grand Pin t' entend ...
tous les arbres étaient très solidaires les uns des autres comme le sont les frères et les soeurs qui s' entraident .
seul , le bouleau refusait l' amitié de ses compagnons .
il se mit à agiter ses branches avec mépris et déclara : je me fiche bien du roi .
je suis le plus beau de tous les arbres de la forêt et dorénavant je refuserai de courber la tête pour le saluer !
le grand pin , qui s' était assoupi , s' éveilla tout d' un coup en entendant son nom .
il secoua ses fines aiguilles pour les remettre en place et s' étira , s' étira en redressant son long corps .
bouleau , que viens tu de dire ?
lança t il .
tous les arbres se mirent à trembler car ils se doutaient bien que la colère grondait dans le coeur du grand pin .
mais le bouleau ne semblait nullement craindre sa colère .
il étala ses branches avec dédain , les agita dans un sens et dans l' autre et dit d' un ton hautain : je ne vais plus vous saluer , grand pin .
je suis le plus bel arbre de la forêt , plus beau que tous les autres , plus beau même que vous !
le grand pin se fâcha .
ses bras se mirent à s' agiter bruyamment .
et tous les arbres attendirent dans le plus grand silence la suite des évènements .
bouleau , lança le roi pin , tu es devenu vaniteux !
je vais t' apprendre une leçon que tu n' oublieras jamais .
le grand pin se pencha en direction du bouleau et frappa sa tendre écorce de toutes ses forces .
ses aiguilles lacérèrent la douce peau blanche du bouleau .
enfin , il dit : que tous apprennent par toi , bouleau , que l' orgueil et la vanité sont mauvais .
depuis ce jour , l' écorce de Bouleau est marquée de fines cica trices noires .
c' est le prix qu' il dut payer pour sa vanité .
tous les membres de sa famille , sans exception , ont gardé , marquée dans leur peau , la trace de la colère du roi grand pin .