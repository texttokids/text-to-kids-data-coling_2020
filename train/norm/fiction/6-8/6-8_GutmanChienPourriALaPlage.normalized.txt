pour Gaspard et Cécile C G
c' est l' été .
dans sa poubelle , Chien Pourri meurt de chaud , Chaplapla meurt de soif , et les mouches tombent comme des mouches .
devant un vieux prospectus , nos deux amis rêvent de vacances au bord de la mer :
si seulement nous pouvions partir sur la Côte d' Azur , Chien Pourri ...
ça ressemble à une côte de porc , Chaplapla ?
c' est encore mieux .
regarde Chaplapla , je m' entraîne " à la plage " !
chien Pourri fait la nage du petit chien dans le caniveau sous le regard dégouté des passants .
mais alors qu' il s' apprête à s' essuyer avec une serpillière , Chaplapla s' écrie :
il nous reste peut être une dernière chance !
avec la Colonie des oubliés ?
pour les chiens abandonnés et les chats écrasés ?
oui , Chien Pourri , pour tous les oubliés des vacances .
on pourra voir la Côte de Porc ?
ne rêve pas trop quand même , Chien Pourri .
devant la Colonie des oubliés , Chien Pourri et Chaplapla font la queue parmi les chats de gouttière et les chiens errants du quartier .
il parait qu' il ne reste plus que des séjours à la montagne , dit un chat à trois pattes .
oui , et dans des campings sans tente , ni eau , ni électricité , ajoute un chien sans queue .
moi , ça me va , dit Chien Pourri .
malheureusement pour cet animal aussi moche que stupide , les jours de chance ne sont pas fréquents et la Colonie des oubliés n' accepte plus personne .
revenez l' année prochaine , bande de lépreux !
crie un moniteur .
mais Chien Pourri et Chaplapa n' arrivent pas à partir , leurs pattes sont engluées dans du goudron .
tu me rappelles un tapis de bain que j' aimais bien , dit le moniteur en s' adressant à Chien Pourri .
finalement , j' ai peut être quelque chose pour toi .
et tandis que Chien Pourri imagine une boite de Canigou , Chaplapla s' attend au pire .
il me reste une place pour notre destination de luxe , la Côte d' Azur .
la tradition veut que l' on en réserve toujours une pour les plus démunis , dit le moniteur .
je pourrai amener mon chat ?
demande Chien Pourri .
pourquoi pas , plus il y a de monstres , plus on rit .
je t' accompagne , mais je ne me baigne pas , prévient Chaplapla .
en route pour la Côte de Porc C' est le départ pour la Colonie des oubliés .
devant l' autocar , Chien Pourri repère deux vieilles connaissances : le caniche à frange et le basset à petit manteau d' été qui discutent autour d' un réverbère .
salut les copains , qu' est ce que vous faites là ?
demande Chien Pourri .
notre maîtresse nous a abandonnés pour les vacances , dit le caniche à frange .
mais plutôt que de nous laisser sur le bord de la route , elle nous a O ert un " stage d' enfant " , dit le basset à petit manteau d' été .
c' est quoi ?
demande Chaplapla .
un stage de perfectionnement pour nous aider à trouver notre compagnon idéal .
quelle chance !
j' aimerais aussi trouver un enfant gentil .
pour toi , ce sera plutôt un enfant pourri , Chien Pourri !
ricane le basset .
le moniteur interrompt leur bavardage pour se présenter :
silence !
je suis votre Gentil Maître , mais vous pouvez m' appeler GM en toute simplicité .
bonjour , ô Grand Maître , dit le labrador .
hou , le fayot !
fait le pitbull .
j' en mange dans ma poubelle et j' adore ça , se vante Chien Pourri .
ça su T , dit le GM : les monstres dans la soute à bagages !
je ne veux pas que vous E rayiez vos camarades .
" quelle gentille attention , pense Chien Pourri , je serai beaucoup plus beau E ectivement une fois bronzé . " écrasés entre deux valises , Chaplapla regrette sa poubelle et Chien Pourri rêve de beignets aux pommes .
raconte moi encore la plage Chaplapla !
eh bien , il y fait très chaud et les plus chanceux vont au Club Biquet .
qu' est ce que c' est ?
un endroit où l' on gagne des boites de croquettes en faisant du trampoline .
ça a l' air chouette .
au bout de quelques heures d' autoroute , le car s' arrête enfin .
le Gentil Maître ouvre la soute et hurle :
allez vous dégourdir les pattes , les a reux !
Chaplapla , regarde tout ce sable , il y a même un toboggan et des toilettes !
c' est ça , le Club Biquet ?
non , c' est une aire d' autoroute , Chien Pourri .
allons voir à l' intérieur , s' il y a un distributeur de croquettes .
la cafétachien Devant une machine à café , Chien Pourri tente de récupérer quelques pièces .
notre maman nous a laissé de l' argent pour nous acheter des chips , dit le caniche à frange .
nous , on préfère les haricots verts , mais il n' y en a pas dans ce distributeur , répond Chien Pourri , de mauvaise foi .
je croyais que les pouilleux étaient interdits sur la Côte d' Azur , dit le basset à petit manteau d' été .
nous , on va sur la Côte de Porc , dit Chien Pourri .
je ne connais pas cette station balnéaire , dit le caniche à frange .
c' est surement pour les cochons , dit le basset à petit manteau d' été .
et tandis que le labrador s' achète de la crème solaire , Chien Pourri tente d' attraper une crème glacée dans une poubelle .
j' ai eu le bâton !
crie Chien Pourri .
alors , c' est le début des vacances , dit Chaplapla .
trois heures , et deux pauses pipi plus tard , nos amis arrivent à destination .
la plage ( Interdit aux chiens galeux , aux chats aplatis et aux mendiants )
enfin la plage , ses palmiers , ses mégots écrasés et son Club Biquet .
la Colonie des oubliés sonne le rassemblement devant le poste de secours .
c' est drapeau vert !
dit le moniteur .
vous êtes autorisés à chercher votre enfant .
choisissez le dur , choisissez le mou , mais n' oubliez pas de le ramener à son maître en fin de journée !
je vous rappelle que les gagnants auront droit à une entrée au Club Biquet .
bonne chance !
l' année dernière , le mien me lançait des balles , dit le basset à petit manteau d' été .
moi , il me faisait la raie de côté , dit le caniche à frange .
moi , il m' achetait des glaces , dit un bulldog en polo .
tu entends ça , Chaplapla ?
c' est merveilleux .
nous allons nous faire adopter par un enfant de la Côte de Porc .
ne rêve pas trop Chien Pourri , qui voudrait d' une serpillière et d' un chat qui ressemble à un frisbee ?
Chaplapla a raison , Chien Pourri reçoit les quolibets de vacanciers en colère .
il n' a même pas de maillot de bain , s' indigne une dame .
il faudrait le faire piquer par une méduse , suggère une autre .
" de qui parlent elles ? " se demande Chien Pourri qui se met en quête de son enfant idéal .
au loin , il aperçoit un petit garçon qui croque dans un sandwich .
" c' est lui , pense t il . il aime le pâté . " chien Pourri se place face à l' enfant .
malheureusement , celui ci se met aussitôt à sangloter .
maman , il y a un monstre qui m' embête !
pour échapper à la mère en colère qui tente de l' assommer avec une pelle , Chien Pourri s' enfouit dans le sable .
" je ne suis qu' une planche sans voile , une glace sans cornet , un thon sans boite . " mais tel un os tombé du ciel , un enfant en short bleu se penche sur lui .
bonjour vieux toutou , comment t' appelles tu ?
chien Pourri .
tu me rappelles mon rat , JeanFrançois , qui est mort l' année dernière .
oh le pauvre , de quoi ?
eh bien si on te le demande , tu diras que tu n' en sais rien , banane !
" quel humour ! pense Chien Pourri . j' ai trouvé mon enfant du stage , c' est Short Bleu . " mais Short Bleu n' est pas seul , d' autres enfants entourent la pauvre bête .
lançons lui du sable dans les yeux , propose un enfant en tee shirt rouge .
non , des galets , ça fait plus mal , ajoute un autre à casquette verte .
laissez le moi , dit Short Bleu , je l' ai trouvé en premier !
" mon GM sera fier de moi , je vais gagner mon entrée au Club Biquet " , se réjouit Chien Pourri .
mais dans la vie de ce brave toutou , rien n' est jamais facile et le papa de Short Bleu rappelle son fils à l' ordre :
au pied , mon fils !
pas toucher la serpillière !
" un maître comme je les aime : sévère mais juste , pense Chien Pourri . mais que veut dire son tee shirt ? ? ça me rappelle quelque chose ... oh ! mais bien sûr : croquettes Rares et Salées . quelle chance , le papa de Short Bleu est un vendeur ambulant ! "
on pourrait ramener cette serpillière à maman , suggère Short Bleu .
si dans un an et un jour , personne ne la réclame , nous aviserons , répond le CRS .
" les croquettes poussent dans la mer , c' est pour ça qu' elles doivent être salées , se dit Chien Pourri , mais pourquoi sont elles si rares ? " s' inquiète t il .
tu veux jouer au chien le plus moche de la plage ?
lui demande Short Bleu .
je ne connais pas ce jeu , c' est dur ?
pour toi , ça devrait être facile .
alors d' accord .
mais Chien Pourri n' a pas le temps de s' amuser , la Colonie des oubliés si E l' heure du pique nique .
à tout à l' heure copain , dit Chien Pourri .
c' est ça , rendez vous au Club Biquet , pour une course de sac à puces , vieux cabot !
le piquenichien À l' abri d' un parasol , la Colonie des oubliés déjeune , tandis que Chien Pourri et Chaplapla brulent au soleil en ramassant des miettes de chips .
désolé , il n' y avait pas de menu poubelle , ricane le basset à petit manteau d' été .
alors , qui a déjà trouvé son enfant ?
demande le Gentil Maître .
moi !
dit le labrador , il m' a même caressé deux fois et jeté un bâton .
trop fastoche , dit le caniche à frange , tout le monde sait que le labrador est l' ami des enfants .
ouais , c' est un fayot , ajoute le basset à petit manteau d' été .
moi , j' ai mordu un surfeur !
dit le pitbull .
ça ne compte pas , dit le GM .
et toi , Chien Pourri , t' es tu fait un copain ?
oui , il s' appelle Short Bleu et son papa est un vendeur de croquettes .
ce n' est pas bien de mentir , dit le moniteur .
à bas les chiens pourris , espèce de vieux débris !
chante le bulldog .
mais Chien Pourri dit vrai , et telle une croquette dans une poubelle , Short Bleu apparait derrière un parasol :
tiens , brave toutou , je t' ai apporté ta pâtée , lui dit il .
vous voyez : j' avais raison les copains .
et à quel parfum est elle ?
demande fièrement Chien Pourri .
au sable , banane !
c' est une spécialité de la Côte de Porc ?
chien Pourri a trouvé un enfant pourri !
déclare le caniche à frange .
il n' a pas l' air tendre , admet le labrador .
je peux le mordre ?
demande le pitbull .
non , qu' il retourne voir son maître , dit le moniteur .
" pauvre enfant , il est vexé et il s' enfuit " , pense Chien Pourri .
attends copain , je vais arranger ça !
lui crie t il de loin .
mais Short Bleu est déjà parti et le Gentil Maître donne une leçon à Chien Pourri .
chien Pourri , tu t' es fait avoir , tu es disqualifié .
les autres , pour vous consoler d' avoir un camarade aussi bête , je vous invite tous au Club Biquet .
chien Pourri , tu garderas les serviettes avec ton ami aplati .
seul un miracle pourrait sauver Chien Pourri d' une nouvelle déception .
heureusement dans sa vie de chien pourri , il su T parfois de lever la tête pour apercevoir la lumière .
dans le ciel , un avion tire une banderole : " concours de ramassage d' ordures ! à gagner : deux places au Club Biquet . " chien Pourri reprend espoir , les ordures , il connait .
" je vais nettoyer chaque grain de sable , à nous le Club Biquet , Chaplapla ! " pendant que Chien Pourri ramasse les vieux mégots , Chaplapla s' occupe des papiers gras .
mais Chien Pourri n' a jamais gagné un concours de sa vie et le sort s' acharne contre lui : c' est Short Bleu et son ami teeShirt Rouge qui sont déclarés vainqueurs .
ce n' est pas juste , on en a ramassé davantage , proteste Chien Pourri .
Chaplapla dépose une réclamation auprès des organisateurs du concours .
les jurés recomptent leurs déchets , lorsqu' ils tombent sur une ordure non identifiée .
quelle est cette chose dégoutante ?
demande une dame .
l' avez vous vraiment trouvée sur cette plage ?
oui , dit Chaplapla .
où ça ?
de quoi parlez vous ?
dit Chien Pourri .
oh , mais elle parle !
s' étonne une autre dame .
c' est incroyable .
nous devons consulter le règlement , dit un monsieur .
pour la première fois dans l' histoire de la Côte de Porc , un chien pourri et un chat aplati sont déclarés vainqueurs ex aequo pour avoir ramené un déchet inconnu et gagnent leur entrée au Club Biquet .
le Club Biquet
j' ai peur , dit Chien Pourri devant l' entrée .
de quoi ?
demande Chaplapa .
de ne pas pouvoir entrer , je ne suis pas une biquette .
et , c' est tout tremblant qu' il présente son ticket au gardien du Club Biquet .
vous l' avez trouvé dans une poubelle surprise votre ticket ?
j' en étais sûr Chaplapla , allonsnous en , dit Chien Pourri .
je plaisante , dit le surveillant , soyez les bienvenus mes biquets !
un toboggan , un trampoline , et des toilettes entourés de barbelés .
chien Pourri est déçu .
cela ressemble au parking de la station service , dit il .
non , c' est mieux , regarde , il y a une piscine , dit Chaplapla .
vous n' avez pas le droit d' être là !
dit le basset à petit manteau d' été .
je vais le dire à mon coi eur , dit le caniche à frange .
mon père va te mordre , dit le pitbull .
oh , laissez cette pauvre bête tranquille , dit le labrador .
toi le fayot , on ne t' a pas causé .
on devrait le renvoyer de la colo , propose le bulldog .
il ne garde pas nos serviettes .
mais Chien Pourri ne les écoute pas et se dirige droit vers le trampoline où se trouve Short Bleu .
Youhou , copain !
crie Chien Pourri .
tiens , c' est toi vieux cabot ?
tu veux jouer à trampo chien ?
qu' est ce que c' est ?
monte , tu verras bien .
à peine a t il posé la patte sur le trampoline que Short Bleu et ses copains lui sautent dessus .
ça su T , pas bouger !
crie le CRS .
c' est à moi que vous parlez ?
demande Chien Pourri .
non , à mon fils , dit le CRS .
sa leçon de natation en petit bassin va commencer .
tous les membres du Club Biquet s' installent près de la piscine gonflable pour un cours collectif .
je vais vous apprendre que lorsqu' on ne flotte pas , on coule , dit le CRS .
il me faudrait un volontaire pour faire le chien qui se noie .
moi , propose le labrador .
ne gâchons pas les chiens de qualité .
chien Pourri tu feras le chien qui coule .
montre nous comment tu nages , vieux toutou .
pendant que Short Bleu lui attache une pierre autour du cou , le CRS livre ses explications .
pour bien flotter , il faut mettre ses brassards et ne pas paniquer .
démonstration .
le chiot périlleux " je suis très fort pour retenir ma respiration dans ma poubelle , pense Chien Pourri , ça ne devrait pas être plus compliqué . " mais Chien Pourri coule à pic .
mes petits chiots , dit le CRS , ne nagez jamais sans la surveillance d' un adulte , c' est LA leçon du jour .
et maintenant , je vais aller récupérer votre camarade .
le CRS plonge et sauve Chien Pourri in extremis de la noyade sous les hourras du Club Biquet .
on pourrait essorer cette serpillière , propose Short Bleu .
non , mon fils , dit le CRS .
c' est l' heure du " temps calme " , je vais en profiter pour vous raconter mes exploits en mer .
" Rhôoo , ce n' est pas drôle " , râlent tous les membres du Club Biquet qui préfèrent se tourner vers une nouvelle attraction : le chat frisbee .
mais alors que le pauvre Chaplapla s' apprête à s' écraser contre un poteau , une voix s' élève contre ce jeu cruel :
laissez le tranquille , il ne vous a rien fait !
crie Short Bleu .
moi j' aime bien les chats .
je le savais !
dit Chien Pourri .
cet enfant n' est pas complètement pourri !
mais on ne faisait rien de mal , dit le caniche à frange .
on s' amusait juste , ajoute le basset à petit manteau d' été .
vous devriez avoir honte de maltraiter un animal , sermonne le labrador .
toi le fayot , on ne t' a pas sonné , disent tous les autres .
pour calmer les esprits échau és , le CRS propose un choco biquette , au bon lait de biquette , pendant qu' il contera ses aventures .
mais hormis Chien Pourri qui espère trouver une croquette dans son lait , le labrador qui fayote et Short Bleu qui écoute son papa , tous repartent vers la piscine .
un jour , j' ai sauvé trois enfants de la noyade , commence le CRS .
en leur vendant des croquettes ?
demande Chien Pourri qui ne comprend rien à rien .
un soir de brouillard , j' ai aidé un aveugle à traverser la plage , confie le CRS .
lui aussi voulait des croquettes ?
demande Chien Pourri .
mon fils , explique s' il te plait , ce chien est trop débile .
mais Short Bleu a disparu .
il est allé rejoindre les autres près de la piscine gonflable .
une nouvelle attraction fait fureur : " la pêche au Chaplapla " .
le pauvre chat va se noyer si personne n' intervient .
n' écoutant que son courage , et oubliant la leçon de son papa , Short Bleu se jette à l' eau sans ses brassards .
pendant ce temps , Chien Pourri se demande si les croquettes poussent aussi dans les piscines .
" menons l' enquête qui nous conduira jusqu' aux croquettes ! " chien Pourri quitte le CRS et plonge à son tour dans la piscine .
" oh , deux croquettes géantes , c' est le CRS qui va être content ! " mais , à la place , Chien Pourri remonte entre ses dents Short Bleu et Chaplapla .
" dommage , le CRS va être déçu . " au contraire , le CRS pleure de joie en retrouvant son fils .
ce n' est pas si triste , dit Chien Pourri en lui léchant les pieds pour le consoler .
on en trouvera d' autres des croquettes .
petit est ton cerveau , mais grand est ton coeur mon ami , dit Chaplapla , tout mouillé .
short Bleu promet de ne plus jamais faire de mal à un animal de sa vie et O re à son sauveur sa briquette au bon lait de biquette .
tu es un compagnon idéal , dit Short Bleu à Chien Pourri .
et toi un enfant pourri ...
euh chéri , répond celui ci tout ému .
pour finir en beauté cette belle journée , Chaplapla et Chien Pourri reçoivent la médaille de la Ville et un paquet de croquettes des mains du moniteur .
le caniche à frange , lui , leur O re une coupe chez son coi eur .
pour Chien Pourri et Chaplapla , hip , hip , hip , hourra !
reprend en choeur la Colonie des oubliés .
vive le Club Biquet !
dit Chaplapla .
et vive la Côte de Porc !
crie Chien Pourri .