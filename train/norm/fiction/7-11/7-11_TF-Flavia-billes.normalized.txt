Flavia , reine des billes
un .
Flavia est contente pour une fois : aujourd'hui , c' est enfin son tour de passer sa journée plaisir avec Grand Mère Léo .
même si les enfants ne savent jamais à l' avance ce que leur grand mère leur a concocté ce jour là , ils sont toujours ravis .
pourtant , cette fois , Léo a eu du mal à trouver une idée pour Flavia .
faire plaisir à cette éternelle boudeuse n' est en effet pas facile .
il n' y a qu' assise à son piano que Flavia semble débrancher son moteur à ronchonner .
Flavia est impatiente de découvrir ce que Léo lui a réservé comme surprise .
quand celle ci s' arrête devant un théâtre , sa petite fille a l' air étonnée .
euh ...
tu es sure que tu ne t' es pas trompée ?
les spectacles , c' est plutôt pour Cara .
non , ma chérie , regarde !
lui répond sa grand mère en lui tendant un programme .
c' est écrit tout petit et il n' y a qu' une photo en noir et blanc dessus .
on y voit un jeune garçon devant un immense piano .
Pff , peste Flavia .
encore un truc à lire !
tu n' es pas fatiguée de râler sans arrêt ?
j' ai beau t' adorer , ça , ne donne pas vraiment envie de te faire plaisir , tu sais ?
pardon , grommelle Flavia .
est ce que tu peux me lire ce qui est écrit s' il te plait ?
grand mère Léo lui explique qu' elles vont assister à un concert d' un prodige du piano .
c' est un jeune chinois que tout le monde considère comme " un petit Mozart " .
non seulement il joue parfaitement les partitions les plus compliquées , mais il compose lui même de la musique .
Léo s' est dit que ce pianiste donnerait envie à Flavia de poursuivre sa passion et peut être même d' en faire son métier .
mais quand Flavia découvre le jeune génie du piano , elle se sent comme écrasée .
il joue avec une telle facilité !
on dirait presque qu' il fait partie du piano .
il doit passer tout son temps sur son clavier .
même si Flavia répète sans s' arrêter pendant des centaines d' années , elle n' atteindra jamais son niveau .
il faudrait qu' elle vive toute seule avec son piano , ou dans son piano !
ce spectacle la déprime et la décourage .
grand mère Léo avait prévu de faire rencontrer le petit pianiste à Flavia , mais elle voit bien que Flavia n' en a pas envie .
elle est désolée pour sa petite fille .
pour l' instant , cette journée plaisir n' est pas vraiment une réussite .
pour consoler Flavia , elle lui propose de l' emmener déjeuner dans le restaurant de son choix .
qu' est ce que tu aimerais manger ?
Flavia hausse les épaules .
elle sait qu' elle a de la chance d' avoir une grand mère aussi gentille et généreuse , mais c' est plus fort qu' elle , elle se remet à rouspéter :
je sais pas .
j' ai pas faim .
Léo soupire .
Flavia est vraiment la plus difficile de tous ses petits enfants !
elle boude , elle râle et s' emporte : elle semble toujours en colère contre la terre entière .
Léo pense que Flavia aurait été différente si elle n' avait pas eu de petit frère : elle n' a pas supporté de se faire voler sa place de petite dernière .
perdue dans ses pensées , Grand Mère Léo n' a pas vu que Flavia s' était arrêtée devant la vitrine d' un magasin de jouets .
elle est surprise : Flavia n' a jamais aimé les jouets , sauf ceux de son petit frère qu' elle lui prend juste pour le faire enrager .
elle déteste les poupées , les figurines , les robots , les camions , les dinettes , les trucs en bois , les machins en plastique , les jeux de construction , les puzzles , sans parler de tous les jeux de société où il faut lire les règles et les respecter .
pourtant , Flavia semble avoir repéré quelque chose dans la vitrine .
qu' est ce que tu as vu , ma chérie ?
Flavia se contente de pointer du doigt ce qu' elle a vu .
tu es sure ?
Flavia fait oui de la tête .
puis elle ajoute :
s' il te plait .
dix minutes plus tard , Flavia serre son paquet contre elle : elle est ravie .
jamais Grand Mère Léo n' aurait cru qu' un cadeau si simple lui plaise à ce point .
elle a l' impression d' entendre Bella lui glisser à l' oreille que ce ne sont pas forcément les cadeaux les plus chers qui ont le plus de succès .
avec le sourire , Flavia retrouve aussi la parole et l' appétit .
à table , elle explique à sa grand mère son projet avec ses nouveaux jouets .
et comme ça , je suis sure de me faire plein d' amis !
conclut Flavia .
deux .
dès que Flavia rentre chez elle , sa fratrie lui tombe dessus :
qu' est ce que tu as fait ?
questionne Bella .
qu' est ce qu' il y a dans ton sac ?
veut savoir Dana .
des billes !
oh , il y en a plein !
tu crois que tu pourras nous en donner ?
s' intéresse Gabriel .
non , c' est à moi !
fait Flavia .
pourtant , Grand Mère Léo lui en a offert tout un tas en lui disant :
comme ça , tu pourras en faire cadeau à qui tu veux .
mais Flavia veut tout garder pour elle .
c' est son trésor .
tu sais y jouer , au moins ?
demande Anna .
à l' école , tout le monde a des billes .
à la récré , c' est la folie dans la cour .
mais Flavia n' a encore jamais fait de partie avec les autres .
si tu ne sais pas , je peux te montrer !
lui propose gentiment Gabriel .
il ne manquerait plus que ça !
Flavia ne va quand même pas demander à son imbécile de petit frère !
n' importe qui sauf lui !
tu ne peux pas m' apprendre , toi ?
demande t elle à Anna .
j' ai un peu oublié les règles .
mais Gabriel est un vrai champion !
grr !
Flavia fait comme si elle n' avait pas entendu .
elle demande à chacune de ses soeurs , mais elles la renvoient toutes vers Gabriel .
Flavia n' a pas le choix ...
elle s' installe avec son frère par terre dans le salon .
tu sais qu' on jouait déjà aux billes dans la Grèce antique , commence Gabriel .
les Romains y jouaient aussi et ...
pas la peine de me raconter toute tes salades sur les billes , banane !
montre moi juste comment on fait .
heureusement pour Flavia , Gabriel ne se vexe pas facilement .
d' accord .
on va commencer par la tic , ou tiquette , c' est ce qu' il y a de plus simple .
tu places ta bille au milieu .
je vais essayer de la toucher avec la mienne et si j' y arrive , elle est pour moi .
c' est de la triche !
non , ce sont les règles du jeu .
après , je placerai ma bille et tu essaieras de la toucher .
mais d' abord , je vais t' apprendre les différentes techniques de tir aux billes .
pas la peine : j' inventerai une méthode à moi .
on ne peut pas faire n' importe quoi !
je te montre le plus courant : le pointage .
Gabriel place son pouce derrière l' index et le détend comme une gâchette pour envoyer sa bille vers celle du centre .
à toi d' essayer , dit il .
je n' essaie rien du tout , on joue .
mets ta bille au milieu .
Gabriel place sa bille que Flavia touche du premier coup .
son frère est impressionné .
le piano a dû lui dégourdir les doigts .
excellent !
à ton tour de placer ta bille .
Flavia choisit la bille la plus laide au cas où elle devrait la perdre .
elle a quand même du mal à s' en séparer , comme si la bille faisait partie de sa chair .
Gabriel tire et la rate .
Flavia joue de nouveau et gagne sa deuxième bille .
Beginner' s luck !
commente Billy en passant .
ce n' est pas la chance des débutants , rétorque Flavia .
c' est le talent !
Dana vient ensuite se joindre à eux avec ses billes .
Flavia les gagne toutes !
c' est bien la première fois que Flavia a hâte d' aller à l' école le lendemain .
elle se voit déjà remporter les billes de toute la cour .
le lendemain , elle entre à l' école un gros sac de billes à la main .
aussitôt , Quentin , l' homme de ses rêves , s' avance vers elle .
il est en CE deux , dans la classe de Dana , et il lui a déjà écrit une lettre , pas exactement d' amour , mais une lettre quand même !
Flavia rêve qu' il la demande en mariage , mais il lui demande juste si elle veut jouer aux billes avec lui .
non pas elle !
proteste Mouad , un ami de Quentin .
pas un bébé de CP !
elle a beau être en CP , elle joue très bien , prévient Dana .
si on l' accepte , tous les CP vont vouloir jouer avec nous , insiste Mouad .
et alors ?
l' important , c' est de jouer , déclare Quentin .
et Flavia l' aime encore plus .
mais quand il s' agit de gagner , Flavia ne fait pas dans le sentiment : dès la première partie , elle remporte toutes les billes .
quand la sonnerie retentit , elle entre en classe comme une condamnée qui doit attendre une éternité avant la récré .
mais quand il est enfin l' heure d' aller dans la cour , les grands garçons du CE deux ne veulent plus jouer avec elle !
ils ne veulent pas risquer les billes qui leur restent ...
Flavia parvient tant bien que mal à rassembler quelques nouvelles victimes et gagne de nouveau .
elle n' a plus assez de place dans son sac et ses poches débordent de billes .
aux récrés suivantes , plus personne ne veut jouer avec elle .
Flavia se rabat sur ses soeurs à qui elle est obligée de donner une partie de son butin pour faire une partie ensemble .
il ne lui faut pas longtemps pour récupérer toutes ses billes .
quel dommage que ce ne soit pas un sport olympique !
trois .
en voyant Flavia bouder au gouter , Gabriel lui demande :
tu n' as pas eu de chance aux billes aujourd'hui ?
Flavia ne répond même pas : elle se contente de poser tout ce qu' elle a gagné sur la table .
oh là là !
c' est impressionnant !
pourquoi tu fais la tête alors ?
plus personne ne veut jouer avec moi dans la cour !
s' emporte Flavia .
même quand je réussis quelque chose , on me déteste !
c' est pas juste .
hum , hum , fait Anna .
quoi ?
grogne Flavia .
tu t' es vue quand tu joues ?
lui demande sa soeur .
tu es une joueuse pas joyeuse , commente Dana .
tu n' es pas une joueuse , corrige Élisa .
tu es une tueuse .
quand tu prends les billes des autres , tu pavoises , tu jubiles et tu ranges tes billes comme un banquier range ses billets de banque , surenchérit Cara .
je pa ...
quoi ?
je les ai gagnées honnêtement : je n' ai pas triché !
il faut être polie avec les autres joueurs , conseille Bella .
plutôt que de les traiter de " nuls " , de " minus " , ou de " moins que rien " , tu ferais mieux de les encourager en disant qu' ils ont bien joué .
ah !
tu veux que je dise : " vous êtes des incompétents mais vous avez bien joué quand même " , c' est ça ?
tu ne penses qu' à prendre les billes et tu t' en vas .
tu ne remercies même pas les autres d' avoir joué avec toi .
tu te plains toujours que tu n' arrives pas à te faire des amis , mais ce n' est pas étonnant si tu es toujours comme ça !
l' enfonce Anna .
tu aimerais , toi qu' on te traite de la même manière ?
Flavia se lève de table et va s' enfermer dans la salle de bain avec ses billes .
elle est à la fois triste et en colère .
ce n' est tout de même pas sa faute si elle est meilleure que les autres aux billes !
elle ne va quand même pas faire semblant d' être nulle pour se faire des amis !
un peu plus tard , quelqu' un frappe à la porte .
laissez moi tranquille !
râle Flavia .
c' est Grand père Mimi .
on m' a dit que tu étais une vraie championne des billes : j' aimerais bien voir ça !
Flavia fait entrer son grand père dans la salle de bain .
c' est sans doute de moi que tu tiens ton talent , confie t il avec un clin d' oeil .
je suis un sacré joueur , tu sais ?
oh tu veux bien faire une partie avec moi , s' il te plait ?
d' accord , si tu me promets de ne pas être mauvaise perdante , je ne supporte pas ça .
Flavia n' est pas une mauvaise perdante , c' est une mauvaise gagnante !
se moque Dana dans le couloir .
Flavia claque la porte et s' installe par terre avec Grand père Mimi .
elle lui donne un de ses sacs de billes et ils commencent leur partie .
mimi a un style et une méthode impeccables : l' une après l' autre , il remporte les billes de sa petite fille .
Flavia a perdu : elle se sent toute nue .
sa vie de championne n' aura pas duré longtemps ...
elle ne veut pas qu' on la voie pleurer , alors elle va se coucher directement , sans dire bonne nuit .
j' aurais peut être dû la laisser gagner , regrette Grand père Mimi .
non , tu as bien fait de jouer normalement : il faut lui apprendre ce que ressentent les perdants , dit Anna .
Bella ne peut s' empêcher d' avoir de la peine pour sa petite soeur .
le lendemain , elle s' arrange pour trouver des élèves de sa classe qui n' ont pas encore joué contre Flavia et elle organise une partie .
mais Flavia a perdu toute confiance en elle et joue très mal .
Quentin assiste à la partie où la multitude de billes de Flavia disparait à la vitesse d' un TGV .
il a pitié d' elle et lui écrit un petit mot qu' il lui glisse à la fin de la journée , avec une poignée de billes .
à la maison , Flavia déchiffre le message de Quentin :
" chère Flavia ,
des fois on perd , des fois on gagne , ce sont les règles du jeu .
l' important , c' est de jouer .
Quentin "
Flavia contemple les billes qu' il lui a données .
celles là , jamais elle ne les perdra !
quatre .
le lendemain matin , Flavia entre dans la cour de l' école en faisant tourner les billes de Quentin dans sa poche .
la directrice attend la sonnerie pour annoncer à tout le monde :
vu le succès des billes à la récréation , nous avons décidé d' organiser un tournoi dans toute l' école .
aujourd'hui , chaque classe devra choisir un champion ou une championne .
et demain , tous les champions s' affronteront lors du tournoi .
bonne chance à tous !
l' important , c' est de jouer , se répète Flavia avant de se lancer dans une partie avec les élèves de sa classe .
avec ses nouvelles billes , elle retrouve confiance en elle .
et maintenant qu' elle sait ce que ça fait de perdre et de se sentir nulle , elle se montre plus gentille avec ses adversaires .
si bien qu' à la fin de la partie , même si Flavia a de nouveau battu tout le monde , tous les perdants sont prêts à encourager la championne de la classe pour le tournoi du lendemain .
Gabriel est déçu de ne pas pouvoir assister au tournoi , alors il offre sa plus belle bille à sa soeur pour lui porter chance .
en la quittant devant l' école , il lui fait le V de la victoire .
Flavia entre dans la cour comme une conquérante , entourée de ses supporters .
Anna chante : " le jour de gloire est arrivé " et toutes ses soeurs la couvrent de bisous pour l' encourager .
la maîtresse de Flavia lui accroche un badge " " .
tous les autres champions sont prêts .
Flavia a l' impression de participer à des jeux internationaux .
la directrice a un micro et elle annonce l' ordre des rencontres .
puis elle se met à commenter les parties .
Flavia affronte d' abord la championne de CE un .
comme les élèves sont censés soutenir leur champion , les soeurs de Flavia ne peuvent pas rester avec elle .
il n' y a qu' Élisa qui assiste à la partie .
qu' est ce elle est contente de voir gagner sa petite soeur !
ensuite , Flavia tombe contre le champion de CE deux qui est aussi le champion de son coeur : Quentin .
la cérémonie veut qu' ils se serrent la main .
c' est la première fois qu' ils se touchent et Flavia se sent toute flagada .
mais elle se ressaisit : elle ne veut pas manquer l' occasion d' impressionner celui qu' elle aime .
elle le remercie pour son mot et son cadeau et ils commencent à jouer .
la partie est très serrée .
impossible de prédire qui va gagner .
mais soudain , Mouad lance à son ami :
tu ne vas pas te laisser avoir par cette bébête de CP !
Flavia se concentre et joue la bille de Gabriel .
c' est le coup qui lui permet de remporter la partie !
Flavia suit les conseils de ses soeurs et félicite Quentin .
ils se serrent de nouveau la main .
Flavia n' a pas envie de lâcher celle de Quentin !
elle lui glisse à son tour une poignée de billes .
malgré sa défaite , Quentin a le sourire .
elle prend facilement le champion nul du CM un .
il reste une dernière partie à jouer , contre la championne de CM deux .
Flavia commence à faiblir , mais elle ne veut pas décevoir sa famille ni sa classe .
Bella lui donne discrètement du chocolat pour la requinquer .
après une belle finale , Flavia est déclarée championne de l' école !
ses soeurs et les élèves de sa classe l' entourent et tout le monde la complimente .
Quentin aussi vient la féliciter .
Flavia est sur un petit nuage .
est ce que tu veux ...
est ce que ça te dirait de ...
bafouille t elle .
ce n' est pas encore aujourd'hui que je le demanderai en mariage , se dit Flavia .
demain , peut être ...
est ce que tu as envie de venir fêter la victoire à la maison ce soir ?
propose t elle .
oui , bien sûr !
et ce soir là , chez les Arthur , on improvise un immense circuit de billes dans le salon et tout le monde joue ensemble , sans perdants ni gagnants .
pour Flavia , c' est une des plus belles soirées de sa vie .