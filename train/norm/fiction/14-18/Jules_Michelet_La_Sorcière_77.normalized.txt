quelque crainte qu' elle eût de déplaire et de dégouter , elle dit la chose .
il vit , et il joua sa comédie , lui reprocha de vouloir guérir et de s' opposer à Dieu .
ce sont les célestes stigmates .
il se met à genoux , baise les plaies des pieds .
elle se signe , s' humilie , elle fait difficulté de croire .
girard insiste , la gronde , lui fait découvrir le côté , admire la plaie .
" et moi aussi je l' ai , dit il , mais intérieure . "
la voilà obligée de croire qu' elle est un miracle vivant .
ce qui aidait à lui faire accepter une chose si étonnante , c' est qu' à ce moment la soeur Rémusat venait de mourir .
elle l' avait vue dans la gloire , et son coeur porté par les anges .
qui lui succèderait sur la terre ?
qui hériterait des dons sublimes qu' elle avait eus , des faveurs célestes dont elle était comblée ?
girard lui offrit la succession et la corrompit par l' orgueil .
dès lors , elle changea .
elle sanctifia vaniteusement tout ce qu' elle sentait des mouvements de nature .
les dégouts , les tressaillements de la femme enceinte auxquels elle ne comprenait rien , elle les mit sur le compte des violences intérieures de l' Esprit .
au premier jour de carême , étant à table avec ses parents , elle voit tout à coup le Seigneur .
" je veux te conduire au Désert , dit il , t' associer aux excès d' amour de la sainte Quarantaine , t' associer à mes douleurs ... " elle frémit , elle a horreur de ce qu' il faudra souffrir .
mais seule elle peut se donner pour tout un monde de pécheurs .
elle a des visions sanglantes .
elle ne voit que du sang .
elle aperçoit Jésus comme un crible de sang .
elle même crachait le sang , et elle en perdait encore d' autre façon .
mais en même temps sa nature semblait changée .
à mesure qu' elle souffrait , elle devenait amoureuse .
le vingtième jour du carême , elle voit son nom uni à celui de Girard .
l' orgueil alors exalté , stimulé du sens nouveau qui lui venait , l' orgueil lui fait comprendre le domaine spécial que Marie ( la femme ) a sur Dieu .
elle sent combien l' ange est inférieur au saint , à la moindre sainte .
elle voit le palais de la gloire , et se confond avec l' Agneau !

pour l' omble d' illusion , elle se sent soulevée de terre , monter en l' air à plusieurs pieds .
elle peut à peine le croire , mais une personne respectée , Mademoiselle Gravier , le lui assure .
chacun vient , admire , adore .
girard amène son collègue Grignet , qui s' agenouille et pleure de joie .
n' osant y aller tous les jours , Girard la faisait venir souvent à l' église des Jésuites .
elle s' y trainait à une heure , après les offices , pendant le diner .
personne alors dans l' église .
il s' y livrait devant l' autel , devant la croix , à des transports que le sacrilège rendait plus ardents .
n' y avait elle aucun scrupule ?
pouvait elle bien s' y tromper ?
il semble que sa conscience , au milieu d' une exaltation sincère encore et non jouée , s' étourdissait pourtant déjà , s' obscurcissait .
sous les stigmates sanglants , ces faveurs cruelles de l' Époux céleste , elle commençait à sentir d' étranges dédommagements .
heureuse de ses défaillances , elle y trouvait , disait elle , des peines d' infinie douceur et je ne sais quel flot de la Grâce " jusqu' au consentement parfait " .
( P quatre cent vingt cinq , in douze .

elle fut d' abord étonnée et inquiète de ces choses nouvelles .
elle en parla à la Guiol , qui sourit , lui dit qu' elle était bien sotte , que ce n' était rien , et cyniquement elle ajouta qu' elle en éprouvait tout autant .
ainsi ces perfides commères aidaient de leur mieux à corrompre une fille très honnête , et chez qui les sens retardés ne s' éveillaient qu' à grand peine sous l' obsession odieuse d' une autorité sacrée .
deux choses attendrissent dans ces rêveries : l' une , c' est le pur idéal qu' elle se faisait de l' union fidèle , croyant voir le nom de Girard et le sien unis à jamais au Livre de vie .
l' autre chose touchante , c' est sa bonté qui éclate parmi les folies , son charmant coeur d' enfant .
au jour des Rameaux , en voyant la joyeuse table de famille , elle pleura trois heures de suite de songer " qu' au même jour personne n' invita Jésus à diner " .
pendant presque tout le carême , elle ne put presque pas manger , elle rejetait le peu qu' elle prenait .
aux quinze derniers jours , elle jeûna entièrement , et arriva au dernier degré de faiblesse .
qui pourrait croire que Girard , sur cette mourante qui n' avait plus que le souffle , exerça de nouveaux sévices ?
il avait empêché ses plaies de se fermer .
il lui en vint une nouvelle au flanc droit .
et enfin au Vendredi Saint , pour l' achèvement de sa cruelle comédie , il lui fit porter une couronne de fil de fer , qui , lui entrant dans le front , lui faisait couler sur le visage des gouttes de sang .
tout cela sans trop de mystère .
il lui coupa d' abord ses longs cheveux , les emporta .
il commanda la couronne chez un certain Bitard , marchand du port , qui faisait des cages .
elle n' apparaissait pas aux visiteurs avec cette couronne , on n' en voyait que les effets , les gouttes de sang , la face sanglante .
on y imprimait des serviettes , on en tirait des Véroniques , que Girard emportait pour les donner sans doute à des personnes de piété .