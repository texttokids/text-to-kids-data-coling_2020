dites moi , mon cher maréchal , si vous jugez à propos de faire exécuter ce mouvement à votre corps d' armée , afin que , si vous y consentez , je puisse faire serrer sur moi les troupes que j' ai sur ces divers points , et qui me seront très utiles pour resserrer et observer l' ennemi et l' empêcher de passer l' Elbe en corps d' armée .
je pense que le général Dalton se décidera enfin bientôt à envoyer d' Erfurth à Leipzig les troupes dont il peut disposer , et qui sont au nombre de douze mille hommes , et que dès lors Monsieur le duc de Padoue n' aura plus besoin de votre appui ni du mien pour conserver cette ville .
" nous ouvrons la tranchée devant la tête de pont de l' ennemi , entre la droite de la Mulde et la gauche de l' Elbe , et nous élevons des batteries : déjà tous ses postes sont rentrés , et nous sommes à quatre cents toises de ses ouvrages , j' espère que demain nous nous en serons approchés à deux cents .
lorsque cette opération sera terminée sur cette rive de la Mulde , je la ferai faire également sur la rive gauche .
je fais aussi établir sur cette rivière un pont de bateaux à six cents toises de la tête de pont , afin que mes troupes puissent rapidement passer d' une rive à l' autre et se soutenir au besoin .
on s' occupe également à retrancher les points principaux de Dessau , de manière à mettre cette ville à l' abri d' un coup de main et à en faire une espèce de tête de pont .
Woronzow et Czernitchef sont toujours entre Acken et Dessau avec quelques détachements d' infanterie .
mais ce ne sera que lorsque j' aurai mis l' ennemi dans l' impossibilité de déboucher par Roslau que je pourrai m' occuper de forcer ces partisans à évacuer le pays entre la Saale et la Mulde .
le camp principal de l' ennemi est toujours à Roslau et le quartier général du prince de Suède à Zerbst .
" maréchal prince De la moskowa . "
" Dresde , le trois octobre dix huit cent treize .
" mon cousin , tous les bruits que l' on fait courir sont controuvés .
il n' y a pas de corps d' armée ennemi sur Géra , il n' y en a pas sur Altenbourg : il n' y a de ce côté que le corps de l' hetman Platow et de Thielmann .
il faut mettre une grande circonspection dans vos mouvements .
avant tout , il faut soutenir le prince de la Moskowa .
le roi de Naples , avec le deuxième , le cinquième et le huitième corps , qui sont entre Freyberg , Chemnitz et Altenbourg , se trouve , dans l' ordre naturel , opposé à tout ce qui arriverait de Bohême .
d' ailleurs , un officier que vous m' enverriez en poste pourrait , en moins de vingt heures , vous rapporter ma réponse .
je vous le répète : couvrir Leipzig , puisque vous y êtes , empêcher le passage de l' Elbe de Wittenberg à Torgau , secourir Torgau , appuyer le prince de la Moskowa , voilà le premier but que vous devez vous proposer : le reste viendra après .
j' attends aujourd'hui des nouvelles du prince Poniatowski et l' arrivée de mes troupes à Chemnitz , ce qui me mettra à même de prendre un parti .
" Dresde , le trois octobre mille huit cent treize .
" mon cousin , le prince Poniatowski est arrivé à Altenbourg le deux octobre .
voici ce qui s' est passé : dans les premiers jours de septembre , le colonel Muensdorf est arrivé à Altenbourg avec un détachement de mille à onze cents chevaux .
thielmann est venu le rejoindre avec trois mille chevaux .
d' Altenbourg , ces troupes poussèrent des partis sur Zeitz , Borna , Freybourg , Weissenfels , Mersebourg et Géra .
le général Lefebvre Desnouettes les repoussa , les rejeta sur Altenbourg , et ensuite sur Zwickau .
mais , le vingt huit , l' hetman Platow déboucha sur Altenbourg avec ses Cosaques , trois mille hommes d' infanterie autrichienne et deux mille cavaliers autrichiens .
le général Lefebvre fut attaqué de front dans le temps que Thielmann le tournait sur Zeitz .
le vingt huit au soir , Platow était de retour à Altenbourg , le vingt neuf , Thielmann y était également revenu .
Platow rentra avec sa troupe à Chemnitz , en partie le vingt neuf et en partie le trente .
thielmann et le comte Muensdorf restèrent à Altenbourg , mais , le deux , au moment où ils faisaient leur mouvement de retraite sur Zwickau , la cavalerie du prince Poniatowski les chargea , leur sabra cinq à six cents hommes , et fit trois cents prisonniers .
en faisant ses adieux aux magistrats d' Altenbourg , Thielmann leur a dit qu' il jugeait que les français venaient sur lui , que la ville serait occupée par eux , et qu' il s' en allait .
il parait que l' infanterie autrichienne que Platow avait sous ses ordres était du corps de Klenau , que ce corps de Klenau n' est que de six mille hommes de cavalerie et au plus de quinze mille hommes d' infanterie , qu' il occupe Chemnitz , Marienberg et Augustenbourg .
le prince Poniatowski occupe Frohbourg et Windischleybe .
j' attends à chaque instant des nouvelles de l' entrée du roi de Naples à Chemnitz .
vous voyez donc que le mouvement de vingt mille autrichiens sur Altenbourg est controuvé .
faites mettre dans les journaux de Leipzig que le général Thielmann a été battu par le prince Poniatowski , qui lui a fait six cents prisonniers et lui a tué et sabré beaucoup de monde .
" Poetnitz , le trois octobre mille huit cent treize
" mon cher maréchal , je reçois votre lettre d' hier .