
) à la vérité , j' aimais Autisme ( I ) , mon souverain maître qui sait ce qui m' est expédient ( ii ) , et m' avait fort bien appasté ( iii ) , m' offrant trois vies , et plus largement mille dans chacune de celles là .
je n' avais alors casement ( iv ) , allant comme mendiant , mon cheval , mon luth et mon or si bien qu' il me vint , avec ma fortune amassée aux pirouettes parlantes et caressantes , de bâtir ribon ribaine ( V ) La Folie , la jolie petite maison ci avant narrée en sa composition au Tome Premier .
il est bel à deviner ( vi ) qu' il s' y passa moultes joyeusetés , que sa renommée secrète se porta au delà des frontières .
mon père l' admira pour sa beauté et sa grâce , fier de me voir si bellement instruit d' architecture , ne sachant que s' y mussait , au delà de chansons , agapes et disputes sur l' art , de diécule à crépuscule , d' hardies nuitées , jusqu'à ce que mon frère sot copieur du Père Garasse , lui souffla quelques mots et le fit témoin par quelque espion .

je laisse le préambule , file au hasard au Chapitre six .

) à la minuit , dans les nuits de lune pleine , à la demande de Monsieur de S , je secoue vaillamment la trenquefile de la petite cloche accrochée derrière la tenture aux anges , la mignonne tinte vaillamment de sa voix menue , ou bien je tape vivement dans les mains quelques coups rapides et enjoués et des têtes se lèvent , ces messieurs et ces dames s' en courent tous nus les uns , empêtrés de quelque passementerie d' aucuns , en voilà qui se pressent en s' entrebaisant , en voici qui se fouettent et se chevauchent en titubant , c' est cris , grognements , clameurs , glapissements et regoulements .
point de vérécondie aux élus : c' est l' appel pour la descente au terrier .
on est en son avertin , on tressue , voilà la demi douzaine chaude et frétillante tout gaudissant ( vii ) qui me suit à la queue leu leu , moi l' orteil sur le bouton de jade , le portail s' ouvre , je me tire vitement en arrière , et soudain après que , Monsieur de S à la tête , on y court on y vole on y glisse , c' est la descente à la desserte , c' est la descente aux enfants , les pieds de la troupe heurtent le vide , les genoux fléchissent , les jambes s' envolent , les corps s' affaissent délicieusement et diablassent tout d' une tirade le long de la glissière et avant !
zou !
dedans mousselines , gazes , plumages , voiles lestes et légers qui emmitonnent et déposent en les draps de soie noire .
l' on est tombé en grappe dans l' alcôve où s' effraient deux ou trois petiots et petiotes enlevés au loin à quelque nid par l' entremise de mon bon Armand , de mignons Jésus et Marie effarouchés à qui apprendre la leçon , à échauffer tout dru , mignardiser et consoler tant ils appellent mère et père , à eux de s' échapper de l' assiduité lufre s' ils le peuvent par le fauteuil suspendu pour eux seuls et à s' en retrouver dans le jardin où il se pourrait bien que vieilles hahas ou lubres courtisans y revinssent sus .

c' est trop .
trop de forfaiture .
trop d' horreur .
le narré me tombe des mains et tombe du ciel pour Roch Henri qui se trouve à six pas .
et la noble Idalie de Serre par dessus .
oui , j' étais là haut , c' est mon érable à lecture et figurez vous que ce fut celui de mon aïeul !
roch henri ramasse le velin calciné et la liasse de feuillets décousus éparpillés à terre .
il s' en noircit les doigts , regarde la Chose qui encombre les mains et embarrasse l' âme , il regarde l' emballement de mon menton qui tremble .
je suis là , n' y suis plus , suis hors de moi et claque des dents .
délices et passementeries , Tome Deuxième !
une belle éloquence pour dire ses ...
ses ...
un vicieux !
des enfants , des enfants ...

j' ai pour aïeul un corrompu et un barbare !
la Folie , savez vous à quoi il destina La Folie ?
le commandant est parti auprès de Monsieur le comte , votre arrière grand père .
je vous conduis chez Madame Lévi .
je vais prendre le temps de lire ce document .
roch henri porte dans les mains la masse carbonisée comme il tiendrait un cadavre de tout petit surgi du tréfonds de la folie , il porte les résidus de tortures subies par des enfants dont j' entends sourdre les voix .
elles exigent que l' on prenne place dans les malheurs qui ont eu lieu et que le corps d' un lecteur les traverse pour en subir l' horreur .
en passer par là .
je fouille dans ma poche de robe , en tire mon portable , appelle mon père , tente un bref préambule , bafouille , résume , pleure sous les yeux de Silka qui m' a poussée dans son logis , là où aucun monte plat ne transportera nos dires dans tout le Château .
Ida , viens en au but , je suis en Chine .
monsieur notre aïeul Siméon écrivit en parlant du sous sol dans le Tome Premier de ses Délices une expression qui paraissait mystérieuse à nos regards aveugles et qui ne l' était point , énigmatique , si l' on voulait l' entendre , qui clamait net de quoi il retournait : la desserte aux enfants .
j' avais dix sept ans , Papa , je venais de dénicher le calepin dans la bibliothèque de l' atelier de Maman , je t' avais questionnée .
après réflexion , m' avais tu dit , et après avoir pris langue avec des historiens et autres experts , Monsieur notre ancêtre avait pensé à des jeux de cache cache pour enfants , avec l' astucieux monte charge bon pour s' échapper gaiement , des jeux pour leur donner quelque frisson et les titiller afin de les mettre joyeusement à l' épreuve .
oui , je ne pouvais croire à autre chose .
je n' avais pas connaissance du Tome Deuxième et de ce que tu me dis qui s' y tient écrit .
mais cela sautait aux yeux !
toute La Folie consacrée aux ébats licencieux de ces ...
de ces ...
et le sous sol aurait fait exception ?
avec son grand baldaquin où loger une douzaine de ...
de ...
de plus , on n' y descendait que depuis le boudoir par le toboggan si merveilleusement organisé à des fins orgiaques .
c' est toi qui as fait percer la porte latérale .
on introduisait les enfants longtemps avant la nuit , c' est cela ?
on les conduisait à jouer d' abord puis on les retenait ensuite ?
en leur promettant quoi ?
de jouer à se faire peur ?
le paradis ?
c' est une abomination .
et le plus ignoble est que je t' ai cru , j' avais dix sept ans et je n' ai rien voulu voir ni savoir .
comme quoi , les mots disent ce qu' il y a à entendre et on ne les écoute pas .
cela me secoue autant que cela te ...
toutefois , crois moi , nous imaginions que les enfants s' y conduisaient eux mêmes en journée alors que dames et messieurs tenaient salon , boudoir , conversations , controverses , jouaient et prenaient collation .
pas un instant , nous n' avons pensé à ce que tu évoques .
je n' évoque rien , c' est écrit .
alors que je suis ici , Monsieur Vignal est occupé à lire la totalité du narré sous l' érable .
pense à ceci , Ida : alors que nous étions proches d' ouvrir les ateliers , tu raffolais de ce jeu de glissade et de remontée !
les tremblements reprennent , le thé brulant et sucré n' y peut rien .
j' ai voulu ouvrir La Folie pour la beauté du lieu , pour que Maman y loge son atelier et soit à son aise pour créer , l' épouse d' un Serre D' Agonès vit sur les terres des Serre D' Agonès , lancinait mon père .
son propre père , Simon de Combalusier , la houspillait de se tenir à sa place .
il fallait cacher l' évidence .
et Dyhia est arrivée peu après .
et que dit notre aïeul , noir sur blanc , dans son Tome Deuxième : c' est la descente à la desserte , c' est la descente aux enfants .
et puis , plus loin : ah , diantre , savourais les dames , les messieurs , seuls , entre tous , et plus et mieux .
puis , plus loin encore : du feu qu' elle mettait aux joues des dames , messieurs , damoiseaux , damoiselles , enfançon .
tu entends , papa , j' ai lu cela , je l' ai lu .
se pourrait il que les cadavres d' enfants et d' adultes trouvés en retournant la terre pour la réhabilitation ne soit pas ceux de petits manouvriers comme nous le pensions ?
je serai de retour dans cinq jours , Maman arrive dans quarante huit heures .
parle avec Raoul .
j' appellerai Monsieur Vignal .
glisse moi son adresse mail .
soutiens toi de l' amitié qu' il te porte .
et accorde confiance au Commandant Dousil .
il faut savoir pourquoi Dyhia est morte .
cela n' a peut être aucun lien avec ce que tu viens de découvrir .
il s' agira de savoir ce qui est survenu à Monsieur notre Arrière grand père .
négligence , perte de sang froid , escobarderie ?
je ne crois pas que Madame notre Grand mère sache quelque chose de ce que tu dis qui eut lieu ...
je te serre fort dans mes bras .
Silka s' assoie à mes côtés .
elle me tend son assiette aux pâtisseries .
offrir à boire et à manger avant même de parler est son art .
comme si nous restions éternellement des infans à choyer et consoler .
je la fixe .
c' est pour bien plus qu' elle fait signe de la sorte , elle est bien plus que mère .
comme si la vie était en permanence en péril et qu' il fallût la veiller assidument , l' encourager de ses meilleures forces , ne pas déroger , ne pas s' absenter , faire offrande à la déesse à chaque instant la soutenir .
la déesse a la voracité d' une ogresse , une exigence inassouvissable .
il y a eu une grande perte jadis , la vie ne doit avoir de cesse de triompher sans quoi l' envers du miroir se révèlerait .
je regarde Silka de derrière mes cheveux qu' elle avait relevés tout à l' heure et qui sont retombés dans une bousculade de mèches , je la regarde depuis mes larmes retenues , je la regarde avec mon oeil meurtri qui baigne dans une humidité désagréable , sous le battement qui frappe l' arcade sourcilière .
je la regarde et je souffre .
je souffre de la soumettre au fracas de ma découverte , d' avoir retourné le miroir et de lui en montrer crument la face monstrueuse , lui répéter noir sur blanc que la folie des hommes a frappé , et si proche , au plus proche , à l' endroit du repos trouvé , un temps trouvé , je souffre de lui flanquer à la figure une tâche supplémentaire , je lui inflige une cruelle vérité , laquelle l' oblige .
de nouveau faire face .
les temps archaïques et barbares perdurent , reviennent par à coups , soufflent à bas bruit sur les braises , le désir du repos demeure en butte à ce qui échappe et revient éreinter .
je souffre d' enlever Marraine à l' espérance .
Dyhia est morte martyrisée et l' ancêtre fut un abject trafiquant , manipulateur d' enfants .
qu' il participât aux saletés ou qu' il se tint à leur bord , qu' il se contentât de sa place de rabatteur et d' instigateur , qu' il se contentât de répondre à une injonction extérieure , de quelque manière qu' il se fût tenu , il y était .
justice n' aura pas été rendue .
le calme du Château est une illusion , une mascarade , un Carnaval de pacotille .
nul lieu d' apaisement ?
je saisis les poignets de Silka , les garde dans mes mains .
vivre exige de la hauteur .
et de la ruse , Roch Henri voit juste .
sans l' intelligence et la pensée , aucun salut .
goute un petit gâteau .
je chipote , je grignote , je mange du bout des dents .
une poussière dorée tombe sur ma robe , dessine une auréole que l' on dirait tracée avec un souffle ténu et cependant vivace , à l' image de la fragilité de toute chose et de son contraire , la puissance d' une conviction née d' expériences et d' épreuves , et plus profondément , de ce qui constitue l' humanité , la force inébranlable d' être et qui irradie les yeux de Silka .
ces messieurs le commissaire et le commandant trouveront ce qui s' est passé pour Madame El Bechery .
oui , Marraine , oui .
et nous nous soutiendrons de la vérité révélée .
et de mots .
la vie doit continuer .
pour le reste , Monsieur ton Arrière grand père fera ce qu' il faut .
il savait , il enterrait , il enterrera .
est ce cela qu' il faut choisir ?
c' est la part due aux ombres .
nous serons un peu plus nombreux à savoir .
et à nous taire .
excepté si le lien avec Dyhia est trouvé et que la chose s' ébruite .
chose qui peut se divulguer sans même qu' il y ait lien si quelqu' un l' évente .
monsieur Rodolphe saura faire face à son tour .
il rentre dans cinq jours !
toi aussi , tu t' en remets à moi .
Silka se penche , elle repousse mes mèches éparpillées , me natterait les cheveux comme lorsque j' étais enfant et qu' avec maman elles jouaient à coiffer la jolie petite poupée russe .
j' ai un jour laissé aller ma chevelure , j' ai réclamé mon droit à l' ombre , le droit aux secrets et à la séparation , le droit d' être .
je m' y suis livrée avec l' ardeur des D' Agonès à dissimuler , néanmoins dans une sorte de renversement .
eux avaient une chose cuisante à cacher et ont institué l' usage de l' escroquerie et l' usage devint loi : qui déroge à l' injonction se voit mis à nu sous leur regard .
je me cache pour ne pas être avalée .
pour ne pas fondre sous les regards .
le combat de papa au Château m' y invita et je m' en confortais .
je m' appuyais sur maman et ses créations , ses ouvrages artistiques , ce qui les provoquait , et les compositions qu' elle inventa dans l' ordinaire des jours devant ce qui lui échut d' intenable dans ses devoirs d' aristocrate .
les de Combalusier ne transigent pas .
plus les titres perdent de sens , plus les familles s' accrochent à du vent .
pourquoi serait ce moi qui ...

je n' ai aucune légitimité , Silka .
je ne suis pas une ainée , je ne porte aucun titre , je ...
je me lève , je ris , m' appuie à la table puis arpente .
je suis invisible et ne connais pas le monde , ceux qui décident , vois tu .
dans les salons , les soirées , les cérémonies , on me prend pour la stagiaire .
que dis tu !
il aurait fait exprès de ...
c' est Raoul qui doit ...
tu es perspicace , indocile , fantaisiste .
c' est une des autres facettes des Serre D' Agonès .
je n' ai pas envie d' occuper la place du fou du roi .
je ne dis pas cela , Idalie .
je m' arrête devant le chandelier à sept branches posé sur le buffet .
au dessus , sur la petite étagère en bois de cèdre , deux petits cadres argentés , deux photos fanées , le visage d' un homme et d' une femme jeunes , au sourire espiègle , deux images des années trente , pareilles à des icônes , usées par la fatigue des caresses , des fuites , des larmes , des fonds de portefeuille et de l' amour porté .
l' homme en vient à cela ...
au pire .
il y vient , oui .
peut être est il cela d' abord , peut être tente t il sans relâche d' y remédier , et cela resurgit .
tout ce qu' il est en apparence ne serait qu' une construction , un château de cartes ?
pour la plupart , le château de cartes tient bon .
au Château , les murs de pierre sont fragiles , lézardés par à coups