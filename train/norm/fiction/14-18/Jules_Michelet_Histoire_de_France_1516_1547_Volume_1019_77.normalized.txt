ceux de France et d' Angleterre sont charmés de la chose .
superbe occasion de faire contribuer le clergé , de sanctifier la guerre , d' accuser Charles Quint .
ainsi cette chose inouïe et terrible qui devait effrayer la terre et faire crouler le ciel , elle fait à peine sensation .
qu' est ce donc ?
ce sanctuaire est il comme les redoutés vases d' Éleusis qu' on n' osait regarder , mais , si l' on regardait , l' on ne découvrait que le vide ?
le vieil oracle virgilien : " à Rome , un Dieu réside , " s' est trouvé démenti .
le monde a eu la curiosité d' y aller voir , il demande : où donc est ce Dieu ?
et la peinture récente de Raphaël , la flamboyante épée de saint Pierre et saint Paul qui fait reculer Attila , elle n' a pas fait peur aux soldats de Frondsberg .
des salles de conclave , de concile , ils font écurie .
s' ils ont peur , c' est tout au contraire d' habiter ces voutes païennes , de loger , eux chrétiens , pêle mêle avec des idoles , dangereuse oeuvre du Démon .
n' est ce pas ce que tant de martyrs du Libre Esprit avaient dit au bucher contre la Babel du pape ?
n' est ce pas ce que les vrais patriotes italiens ( d' Arnoldo de Brescia jusqu'à Machiavel ) ont annoncé à l' Italie : qu' elle mettait sa vie dans la mort , et que la mort l' entraînerait ?
" Rome a mangé le monde , " disait le vieil adage .
cette fois , le monde a mangé Rome .
le génie italien , si longtemps captif et malade dans cette fatale fiction d' un faux empire du monde qui annula sa vitalité propre et fit avorter la patrie , le génie italien pourrait remercier cette grande calamité qui le délivre , repousser et nier cette communauté de la mort .
Rome est morte , vive l' Italie !
il n' en est pas ainsi .
ce n' est pas impunément que , toute une longue vie , l' esprit a endossé le corps , trainé cette chair de tentations , de péchés , de souillures .
quand il faut la jeter , et libre , déployer ses ailes , nous hésitons toujours .
telle l' Italie , qui si longtemps vivait dans cette forme , dans cette condition d' existence , fut accablée du coup , et il lui fallut des siècles pour s' en relever .
voyons comment les deux grands italiens ont pris la chose .
regardons un moment Michel Ange et Machiavel .
tous deux avaient erré .
tous deux , dans les illusions qui entourent des moments si sombres , avaient cherché l' espérance dans le désespoir , cru que l' on pourrait sauver le pays par les Médicis , faire la force avec la bassesse , mais non , il n' en est pas ainsi .
et Dieu punit de telles pensées .
d' abord le pape , qui était Médicis , accepta sa sentence , se mit plus bas encore que ne l' avait mis son malheur , montra que , pour être sorti de captivité , il n' était pas plus libre .
traité outrageusement comme un petit prince italien , il prouva qu' il n' était rien autre chose .
florence lui tenait au coeur bien plus que Rome .
et , pour avoir Florence , il s' humilia devant l' Empereur .
il y fut ramené par le prince d' Orange , le chef des brigands italiens qui , derrière les Barbares , traitreusement , avaient pillé Rome .
dans le moment si court de la lutte suprême de Florence , d' une ville contre le monde , ni Machiavel , ni Michel Ange ne manquèrent à la patrie .
machiavel y trouva appliqué son Arte di guerra , toute la jeunesse levée en légions , dans la forme qu' il avait tracée .
on prenait le système , mais on repoussait l' homme .
négligé , oublié , pas même persécuté .
l' indomptable vigueur de son esprit parait encore dans l' étrange description qu' il a faite de la peste de Florence , un mois avant sa mort , un mois après le sac de Rome .
cet homme , d' un malheur accompli , seul , vieux , pauvre , haï , méprisé , savez vous ce qu' il fait ?
parmi les litanies funèbres , sur le bord de sa fosse , il écrit une espèce de Pervigiliun Veneris du mois de mai .
c' est l' idylle de la peste .
dans la ville , il est fort à l' aise : il va en long , en large , au milieu des fossoyeurs qui crient : " vive la mort ! " comme c' était l' usage de chanter Mai et le printemps .
à travers les ténèbres , il croit voir passer la peste dans une litière .
c' est une jeune morte traînée par des chevaux blancs .
il s' en va sur la place où l' on élit les magistrats .
il n' y a plus de peuple .
des citoyens encore , mais allongés sur des civières qu' on porte .
au défaut de vivants , au vote on appelle les morts .
étonnant aspect des églises !
le clergé est mort , les moines sont morts .
tel reste pour confesser les femmes malades qui se trainent et viennent mourir là .
il est assis au milieu de la nef , les fers aux pieds , aux mains , pour empêcher qu' il ne les touche .
songez y , dans ce temps de morts , c' est tout d' être vivant .
trois dévots en béquilles , qui circulent dans l' église , lancent un regard d' amour à trois vieilles édentées .
machiavel , avec ses soixante ans , est sûr de plaire et de trouver fortune .
sur les tombes qui entourent l' église , il trouve une jeune femme échevelée qui se frappe le sein .
il avance , non sans quelque crainte , il console , interroge .
elle répond , s' épanche , elle conte en paroles hardies ( les morts n' ont peur de rien ) , en lamentations effrénées , les joies conjugales qu' elle n' aura plus .
ce disant , elle pâme .
est elle morte ?
pestiférée ou non , Machiavel la délasse et desserre , " quoiqu' elle ne fût pas très serrée . " elle revient alors , et jure qu' elle n' a plus souci d' elle , de moeurs ni de pudeur .
là dessus , un sermon équivoque du bon apôtre , qui prêche la décence des plaisirs secrets .