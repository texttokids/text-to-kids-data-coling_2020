la jeune femme que nos lecteurs ont vue au début de cette histoire , Madeleine de Soizé , était bien changée , quoique toujours belle , une pâleur maladive couvrait son visage , dans le regard et dans le sourire régnait une profonde tristesse , sur ses beaux traits on sentait que la douleur et la souffrance avaient passé .
l' on se souvient de l' état dans lequel était la malheureuse jeune fille lorsqu' elle vint , un soir d' orage , raconter à Pierre le terrible secret , c' est cette situation qui , la flétrissant à jamais , l' avait poussée à la cruelle vengeance qu' elle exécutait ...
sans espoir , elle voulait désespérer les autres .
depuis ce jour , le malheur sans cesse l' avait poursuivie .
lorsque , ne pouvant plus cacher sa faute , elle se jeta aux genoux de son père et lui raconta qu' elle avait été non une coupable , mais une victime , le vieux paralytique s' était levé superbe comme au jour où il marchait au feu , son regard avait eu l' éclair de mort des jours de combat , il aurait voulu trouver devant lui celui qui avait déshonoré son enfant .
il s' était levé , il avait voulu agir et il était retombé sur son fauteuil , épuisé , il avait avancé les mains sur la tête baissée de son enfant à genoux , à la contraction de rage de son visage avaient succédé le calme et la prière .
deux larmes avaient coulé de ses yeux , il s' était raidi et sa tête était tombée en arrière .
sa fille , toujours à genoux , sentant les mains de son père sur ses cheveux , n' avait entendu qu' une phrase qui était pour elle le pardon demandé :
ma pauvre enfant !
dieu juste , prenez moi , mais vengez la !
et elle n' osait lever les yeux , en sentant les mains plus lourdes de son père , elle relevait la tête et les bras retombèrent inertes de chaque côté du fauteuil ...
elle regarda son père , et jeta un cri en se dressant épouvantée .
le capitaine Antoine de Soizé était mort ...
folle de douleur , se reprochant la mort de son père , la malheureuse enfant criait , sanglotait et voulait mourir ...
les voisins , accourus à ses cris , cherchaient à la contenir , mais rien ne saurait dépeindre l' état dans lequel était la malheureuse jeune fille , dont nos lecteurs ont pu juger , au reste , l' ardeur et l' énergie .
elle se roulait sur son lit , arrachant ses cheveux , blasphémant , proférant des menaces , répétant un nom inconnu des femmes qui cherchaient à la consoler et qui se regardaient entre elles , effrayées de l' intensité de cette douleur .
la secousse produite par la mort de son père la força à prendre le lit le soir même , elle passa tout un jour dans les plus atroces douleurs : il semblait qu' un être refusait de naitre dans cet appartement occupé par la mort ...
à l' heure où , évitant de faire du bruit , on enlevait le corps du capitaine Antoine de Soizé pour le conduire à sa dernière demeure , Madeleine retombait presque mourante sur son lit en mettant au monde un fils qui mourut le soir même .
pendant dix jours , la malheureuse jeune femme fut entre la vie et la mort , et les soins ne lui manquèrent pas ...
c' est Pierre qui la faisait veiller , lorsqu' elle put sortir , il la fit aussitôt venir à Charonne , où elle acheva de se rétablir en s' occupant de la petite Jeanne ...
les terribles épreuves par lesquelles la malheureuse avait passé augmentèrent encore sa haine , et Pierre s' en réjouissait , car , dans ses moments de défaillance , c' était elle qui le poussait à la vengeance .
madeleine , le misérable va subir le châtiment , à l' heure où je vous parle , la punition commence ...
madeleine releva la tête , interrogeant , le sourcil froncé .
fernand , vous le savez , a continué sa vie épouvantable , ne reculant devant aucun moyen pour satisfaire à ses désirs ...
il aimait la vie grande , il l' a eue , il n' avait jamais aimé véritablement , il a aimé , il est fou d' amour .
je sais tout cela ...
, et la vengeance ?

hier , il est rentré chez lui au milieu de la nuit : je l' attendais dans sa chambre ...
vous !

il a reculé devant moi comme devant un spectre ...
, et j' ai soulevé les rideaux de son lit pour lui montrer sa femme , son idole , endormie dans les bras d' un autre .
eh bien ?
demanda Madeleine , l' oeil ardent .
il a jeté un cri épouvantable , pour se soutenir , il dut s' accrocher à la cheminée , le regard fixé sur les deux amants ...
ceux ci s' éveillèrent , et la femme coupable , celle qu' il aimait , criait à son complice : tue le !
tue le !
ah !
dieu juste , fit Madeleine , vous lui rendez ce qu' il a fait aux autres !
les amants se sauvèrent , et alors qu' il pouvait avoir l' espoir de se venger , ce plaisir âpre de ceux qui ont beaucoup souffert , on est venu l' arrêter comme faussaire ...
il est en prison , et chaque nuit il pensera que celle qu' il aime est avec l' autre .
en prison !

il sera jugé ...
et acquitté ?
fernand sera condamné , sa vie finira au bagne : il est à jamais perdu , et il aura dans son existence de condamné la pensée constante que celle qu' il aime le trompe , qu' elle se moque de lui ...
dans ses rêves , il les entendra rire , il a le châtiment auquel nous l' avons condamné , la vie avec la honte et le désespoir , l' amour , comme un vautour , lui déchirant le coeur ...
c' est sans regret , sans remords , que j' apprends sa peine ...
je ne sens en moi que de la haine .
la moitié de l' oeuvre est faite , à l' autre maintenant ...
monsieur Pierre , pour ...
ne prononcez pas son nom maudit ...
pour elle , sinon le pardon , au moins l' oubli ...
non ...
est ce que vous avez oublié , vous ?
moi , j' aurais pu avec le temps oublier s' il n' était venu s' ajouter , à la faute commise par moi , la mort de mon père , le brave et loyal soldat , emportant dans l' éternité son nom flétri par son enfant ...