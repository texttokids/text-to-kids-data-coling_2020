le docteur regardait toujours par la fenêtre .
d' un côté de la vitre , le ciel frais du printemps , et de l' autre côté le mot qui résonnait encore dans la pièce : la peste .
le mot ne contenait pas seulement ce que la science voulait bien y mettre , mais une longue suite d' images extraordinaires qui ne s' accordaient pas avec cette ville jaune et grise , modérément animée à cette heure , bourdonnante plutôt que bruyante , heureuse en somme , s' il est possible qu' on puisse être à la fois heureux et morne .
et une tranquillité si pacifique et si indifférente niait presque sans effort les vieilles images du fléau , Athènes empestée et désertée par les oiseaux , les villes chinoises remplies d' agonisants silencieux , les bagnards de Marseille empilant dans des trous les corps dégoulinants , la construction en Provence du grand mur qui devait arrêter le vent furieux de la peste , Jaffa et ses hideux mendiants , les lits humides et pourris collés à la terre battue de l' hôpital de Constantinople , les malades tirés avec des crochets , le carnaval des médecins masqués pendant la Peste noire , les accouplements des vivants dans les cimetières de Milan , les charrettes de morts dans Londres épouvanté , et les nuits et les jours remplis , partout et toujours , du cri interminable des hommes .
non , tout cela n' était pas encore assez fort pour tuer la paix de cette journée .
de l' autre côté de la vitre , le timbre d' un tramway invisible résonnait tout d' un coup et réfutait en une seconde la cruauté et la douleur .
seule la mer , au bout du damier terne des maisons , témoignait de ce qu' il y a d' inquiétant et de jamais reposé dans le monde .
et le docteur Rieux , qui regardait le golfe , pensait à ces buchers dont parle Lucrèce et que les Athéniens frappés par la maladie élevaient devant la mer .
on y portait les morts durant la nuit , mais la place manquait et les vivants se battaient à coups de torches pour y placer ceux qui leur avaient été chers , soutenant des luttes sanglantes plutôt que d' abandonner leurs cadavres .
on pouvait imaginer les buchers rougeoyants devant l' eau tranquille et sombre , les combats de torches dans la nuit crépitante d' étincelles et d' épaisses vapeurs empoisonnées montant vers le ciel attentif .
on pouvait craindre ...
mais ce vertige ne tenait pas devant la raison .
il est vrai que le mot de " peste " avait été prononcé , il est vrai qu' à la minute même le fléau secouait et jetait à terre une ou deux victimes .
mais quoi , cela pouvait s' arrêter .
ce qu' il fallait faire , c' était reconnaitre clairement ce qui devait être reconnu , chasser enfin les ombres inutiles et prendre les mesures qui convenaient .
ensuite , la peste s' arrêterait parce que la peste ne s' imaginait pas ou s' imaginait faussement .
si elle s' arrêtait , et c' était le plus probable , tout irait bien .
dans le cas contraire , on saurait ce qu' elle était et s' il n' y avait pas moyen de s' en arranger d' abord pour la vaincre ensuite .
le docteur ouvrit la fenêtre et le bruit de la ville s' enfla d' un coup .
d' un atelier voisin montait le sifflement bref et répété d' une scie mécanique .
Rieux se secoua .
là était la certitude , dans le travail de tous les jours .
le reste tenait à des fils et à des mouvements insignifiants , on ne pouvait s' y arrêter .
l' essentiel était de bien faire son métier .
le docteur Rieux en était là de ses réflexions quand on lui annonça Joseph Grand .
employé à la mairie , et bien que ses occupations y fussent très diverses , on l' utilisait périodiquement au service des statistiques , à l' état civil .
il était amené ainsi à faire les additions des décès .
et , de naturel obligeant , il avait consenti à apporter lui même chez Rieux une copie de ses résultats .
le docteur vit entrer Grand avec son voisin Cottard .
l' employé brandissait une feuille de papier .
les chiffres montent , docteur , annonça t il : onze morts en quarante huit heures .
Rieux salua Cottard et lui demanda comment il se sentait .
grand expliqua que Cottard avait tenu à remercier le docteur et à s' excuser des ennuis qu' il lui avait causés .
mais Rieux regardait la feuille de statistiques :
allons , dit Rieux , il faut peut être se décider à appeler cette maladie par son nom .
jusqu'à présent , nous avons piétiné .
mais venez avec moi , je dois aller au laboratoire .
oui , oui , disait Grand en descendant les escaliers derrière le docteur .
il faut appeler les choses par leur nom .
mais quel est ce nom ?
je ne puis vous le dire , et d' ailleurs cela ne vous serait pas utile .
vous voyez , sourit l' employé .
ce n' est pas si facile .
ils se dirigèrent vers la place d' Armes .
Cottard se taisait toujours .
les rues commençaient à se charger de monde .
le crépuscule fugitif de notre pays reculait déjà devant la nuit et les premières étoiles apparaissaient dans l' horizon encore net .
quelques secondes plus tard , les lampes au dessus des rues obscurcirent tout le ciel en s' allumant et le bruit des conversations parut monter d' un ton .
pardonnez moi , dit Grand au coin de la place d' Armes .
mais il faut que je prenne mon tramway .
mes soirées sont sacrées .
comme on dit dans mon pays : " il ne faut jamais remettre au lendemain ... "