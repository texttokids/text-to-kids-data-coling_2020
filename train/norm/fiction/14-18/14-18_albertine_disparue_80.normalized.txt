" alors Monsieur de Villeparisis ne va pas tarder à descendre .
depuis un mois qu' ils sont ici ils n' ont mangé qu' une fois l' un sans l' autre " , dit le garçon .
je me demandais quel était celui de ses parents avec lequel elle voyageait et qu' on appelait Monsieur de Villeparisis , quand je vis , au bout de quelques instants , s' avancer vers la table et s' asseoir à côté d' elle son vieil amant , Monsieur de Norpois .
son grand âge avait affaibli la sonorité de sa voix , mais donné en revanche à son langage , jadis si plein de réserve , une véritable intempérance .
peut être fallait il en chercher la cause dans des ambitions qu' il sentait ne plus avoir grand temps pour réaliser et qui le remplissaient d' autant plus de véhémence et de fougue , peut être dans le fait que , laissé à l' écart d' une politique où il brulait de rentrer , il croyait , dans la naïveté de son désir , faire mettre à la retraite , par les sanglantes critiques qu' il dirigeait contre eux , ceux qu' il se faisait fort de remplacer .
ainsi voit on des politiciens assurés que le cabinet dont ils ne font pas partie n' en a pas pour trois jours .
il serait , d' ailleurs , exagéré de croire que Monsieur de Norpois avait perdu entièrement les traditions du langage diplomatique .
dès qu' il était question de " grandes affaires " il se retrouvait , on va le voir , l' homme que nous avons connu , mais le reste du temps il s' épanchait sur l' un et sur l' autre avec cette violence sénile de certains octogénaires qui les jette sur des femmes à qui ils ne peuvent plus faire grand mal .
me de Villeparisis garda , pendant quelques minutes , le silence d' une vieille femme à qui la fatigue de la vieillesse a rendu difficile de remonter du ressouvenir du passé au présent .
puis , dans ces questions toutes pratiques où s' empreint le prolongement d' un mutuel amour : êtes vous passé chez Salviati ?
oui .
enverront ils demain ?
j' ai rapporté moi même la coupe .
je vous la montrerai après le diner .
voyons le menu .
avez vous donné l' ordre de bourse pour mes Suez ?
non , l' attention de la Bourse est retenue en ce moment par les valeurs de pétrole .
mais il n' y a pas lieu de se presser étant donné les excellentes dispositions du marché .
voilà le menu .
il y a comme entrée des rougets .
voulez vous que nous en prenions ?
moi , oui , mais vous , cela vous est défendu .
demandez à la place du risotto .
mais ils ne savent pas le faire .
cela ne fait rien .
garçon , apportez nous d' abord des rougets pour Madame et un risotto pour moi .
un nouveau et long silence .
" tenez , je vous apporte des journaux , le Corriere della Sera , la Gazzetta del Popolo , et caetera Est ce que vous savez qu' il est fortement question d' un mouvement diplomatique dont le premier bouc émissaire serait Paléologue , notoirement insuffisant en Serbie ? il serait peut être remplacé par Lozé et il y aurait à pourvoir au poste de Constantinople . mais , s' empressa d' ajouter avec âcreté Monsieur de Norpois , pour une ambassade d' une telle envergure et où il est de toute évidence que la Grande Bretagne devra toujours , quoi qu' il arrive , avoir la première place à la table des délibérations , il serait prudent de s' adresser à des hommes d' expérience mieux outillés pour résister aux embuches des ennemis de notre alliée britannique que des diplomates de la jeune école qui donneraient tête baissée dans le panneau . " la volubilité irritée avec laquelle Monsieur de Norpois prononça ces dernières paroles venait surtout de ce que les journaux , au lieu de prononcer son nom comme il leur avait recommandé de le faire , donnaient comme " grand favori " un jeune ministre des Affaires étrangères .
" dieu sait si les hommes d' âge sont éloignés de se mettre , à la suite de je ne sais quelles manoeuvres tortueuses , aux lieu et place de plus ou moins incapables recrues ! j' en ai beaucoup connu de tous ces prétendus diplomates de la méthode empirique , qui mettaient tout leur espoir dans un ballon d' essai que je ne tardais pas à dégonfler . il est hors de doute , si le gouvernement a le manque de sagesse de remettre les rênes de l' État en des mains turbulentes , qu' à l' appel du devoir un conscrit répondra toujours : présent . mais qui sait ( et Monsieur de Norpois avait l' air de très bien savoir de qui il parlait ) s' il n' en serait pas de même le jour où l' on irait chercher quelque vétéran plein de savoir et d' adresse ? à mon sens , chacun peut avoir sa manière de voir , le poste de Constantinople ne devrait être accepté qu' après un règlement de nos difficultés pendantes avec l' Allemagne . nous ne devons rien à personne , et il est inadmissible que tous les six mois on vienne nous réclamer , par des manoeuvres dolosives et à notre corps défendant , je ne sais quel quitus , toujours mis en avant par une presse de sportulaires . il faut que cela finisse , et naturellement un homme de haute valeur et qui a fait ses preuves , un homme qui aurait , si je puis dire , l' oreille de l' empereur , jouirait de plus d' autorité que quiconque pour mettre le point final au conflit . " un monsieur qui finissait de diner salua Monsieur de Norpois .
ah !
mais c' est le prince Foggi , dit le marquis .
ah !
je ne sais pas au juste qui vous voulez dire , soupira Madame de Villeparisis .
mais parfaitement si .
c' est le prince Odon .
c' est le propre beau frère de votre cousine Doudeauville .
vous vous rappelez bien que j' ai chassé avec lui à Bonnétable ?
ah !
Odon , c' est celui qui faisait de la peinture ?
mais pas du tout , c' est celui qui a épousé la soeur du grand duc N ...