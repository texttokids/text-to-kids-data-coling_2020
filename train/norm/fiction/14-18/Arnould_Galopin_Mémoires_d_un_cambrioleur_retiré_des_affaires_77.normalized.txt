la loi anglaise est bien dure pour ceux qu' elle frappe , et il ne serait pas trop tôt qu' elle s' humanisât un peu et marchât avec le progrès ...
je crois que cela arrivera lorsque les juges renonceront enfin à siéger en perruque poudrée et remiseront parmi les curiosités des siècles défunts leurs oripeaux ridicules .
Y renonceront ils jamais ?
le code britannique aurait certes besoin d' être remanié , car il retarde vraiment trop .
au moment où , dans le monde entier , tout est en marche vers un système social plus en rapport avec les moeurs actuelles , où chez tous les peuples les lois ont été " retouchées " , pourquoi l' Angleterre continue t elle à marquer le pas avec tant d' indolence ?

le nouveau régime auquel j' avais été soumis , grâce au docteur Murderer , apaisa un peu mes nerfs , les tremblements qui m' agitaient devinrent moins violents , mais les étourdissements persistèrent , et c' étaient eux qui m' inquiétaient le plus , car je craignais qu' ils ne me prissent au moment où je serais en train de tourner le " moulin " .
de plus , je faisais de la neurasthénie , ce qui n' a rien d' étonnant avec un régime pareil , et la confiance que j' avais eue en l' avenir m' abandonnait peu à peu ...
ce qu' il m' eût fallu , c' était une cure d' air mais peut être deviendrait elle inutile si mon incarcération se prolongeait .
un matin que j' étais exempt de Tread Mill , la cloche de notre chapelle se mit à sonner tristement , à petits coups étouffés , comme honteuse d' avoir encore à annoncer la mort d' un détenu ...
c' est effrayant ce qu' il mourait de monde à Reading , depuis quelques semaines !
en entendant ce glas , je me mis à pleurer .
pourquoi ?
je n' aurais pu le dire .
ce n' était pas la première fois que j' entendais tinter la " gloomy " ( c' est ainsi que l' on appelait la cloche du temple ) et jamais je ne m' étais senti ému comme ce jour là .
Etait ce pressentiment , crainte ou pitié ?
je n' aurais pu le dire .
ce qu' il y a de certain , c' est que j' étais troublé au delà de toute expression et que je souffrais le martyre .
quand le geôlier vint m' apporter ma pitance , je ne pus résister au désir de l' interroger .
c' était un brave garçon qui ne dédaignait pas , à certains moments , de tailler une bavette avec moi .
il avait fait la guerre en Afghanistan et aimait à raconter ses exploits , comme la plupart des militaires qui ont vu le feu de près ou même de loin .
savez vous qui est mort ce matin , lui demandai je .
oui , dit il à voix basse ( car Oeil de Crabe rôdait dans les environs ) , c' est le numéro trente quatre ...
le trente quatre ?

oui , celui qui était votre voisin de cellule , il y a quelques jours encore ...
il parait qu' il a eu une mort affreuse ...
il était devenu comme fou et on a été obligé de lui mettre la camisole de force ...
ah !
certes , le pauvre diable est plus heureux " comme ça " ...
au moins , il ne souffre plus ...
et le geôlier qui avait encore conservé la faculté de s' émouvoir , sortit en disant :
il avait pourtant l' air d' un bon garçon !

c' était doux comme une petite fille ...
et si poli !

sûr qu' on lui pardonnera là haut !
je me jetai sur mon lit et me mis à sangloter ...
pauvre Crafty !

pauvre Crafty !

ainsi , c' était lui !

ah !
je m' expliquais maintenant pourquoi cette maudite cloche m' avait tant troublé !

il y avait entre Crafty et moi un lien que la mort elle même n' était point parvenue à rompre , et , par une sorte de télépathie indéniable , nos deux âmes communiaient étroitement dans la religion du souvenir ...
sa pensée était venue à moi , à travers les murs de la prison , et la mienne maintenant allait à lui !

pauvre Crafty !

il ne me trompait pas quand il disait qu' il n' en avait plus pour longtemps ...
et me suppliait de " hâter notre évasion " .
il sentait déjà venir la mort et croyait l' éviter en fuyant , comme si l' on échappait jamais à la " rôdeuse " de Reading , lorsqu' elle vous a une fois marqué de son doigt fatal !
ainsi , mon camarade était mort , mort sans que je pusse rien faire pour lui , moi qui aurais tant désiré lui être utile !
et c' était à ce malheureux que je devais ma fortune .
c' était grâce à lui que j' avais retrouvé mon diamant !

à quelques jours de là , le directeur , suivi du surveillant général et d' un gardien , entra dans ma cellule .
ces trois visiteurs avaient la mine sévère et je vis tout de suite qu' il allait se passer quelque chose ...
fouillez partout , commanda le directeur .
immédiatement , le surveillant et le gardien se mirent à bouleverser mon lit , à palper ma paillasse et mon traversin , puis , ils examinèrent le parquet , introduisant la lame de leur couteau entre chaque rainure .
ensuite , ils cherchèrent derrière la planche qui garnissait la fenêtre et je tremblais qu' ils ne découvrissent mon diamant , mais il était tellement bien dissimulé qu' il échappa à leurs regards .
le directeur vint alors se planter devant moi et demanda d' un ton dur :
où sont vos bottines ?
je feignis le plus profond ahurissement , puis , ôtant une de mes pantoufles , je la lui montrai , en disant :
voilà , monsieur le Directeur ...
il eut un haussement d' épaules :
ce ne sont pas vos sandales que je veux voir , ce sont vos bottines ...