quelques jours plus tard , j' ai arrêté le baby sitting .
j' étais trop occupée , je faisais plein de choses .
complètement overbookée .
plus le temps de m' occuper d' une gosse .
désolée .
nan , en fait je garde plus Sarah parce que c' est Hamoudi qui le fait à ma place .
comme il travaille à la cité , et qu' il finit à quatre heures , il peut aller chercher la petite .
c' est bien .
ouais .
en plus , ça fait comme une vraie famille .
je vois Hamoudi quand je vais faire les courses en bas .
il me parle devant Malistar en déchargeant quelques cartons de riz .
au bout de cinq minutes , comme j' ai l' impression de déranger , je m' en vais .
il faut dire qu' il me parle comme à tous les autres .
c' est plus l' Hamoudi du hall trente deux .
et ça , il le sait .
l' autre jour , j' ai trouvé un petit mot dans la boite aux lettres , accompagné d' un billet de vingt euros .
c' était signé " Moudi " .
un surnom .
c' est nul comme surnom .
j' aurais surement fait mieux , moi .
quand je pense qu' Hamoudi disait qu' il trouvait ça ridicule les surnoms .
maintenant il signe carrément " Moudi " .
elle aurait quand même pu trouver autre chose Lila .
Moudi .
mais " mou dit quoi ? " .
il dit rien , plus rien du tout .
les surnoms , ça fait couple bourgeois : " tu veux encore du lapin , mon canard ? " ça marche aussi dans l' autre sens .
trop pourrave .
bref , comme pour se donner bonne conscience parce qu' il culpabilisait de m' avoir un peu laissée tomber , il m' a mis ce petit mot dans la boite aux lettres avec un billet de vingt euros .
il croit que de l' argent ça compense un manque ou quoi ?
faut qu' il arrête de lire les dossiers psycho des magazines féminins posés sur la table basse chez Lila .
même ce qu' il a écrit c' était nul : " si t' as besoin de moi , tu sais où me trouver ... " ouais eh ben ce que je sais , Hamoudi , c' est que t' es plus dans le hall trente deux .
tu nous as laissés tomber Rimbaud et moi .
lâcheur .
tous pareils de toute façon .
des lâcheurs .
même Madame Burlaud , si elle était pas payée pour me voir à heure fixe une fois par semaine , je suis sure qu' elle m' aurait lâchée .
en passant devant le bar tabac du centre ville , j' ai remarqué un papier collé sur la vitrine .
il était écrit : " la française des jeux LOTO : ici un gagnant : soixante cinq euros . " à chaque fois , ils mettent " ici un gagnant " .
mais ils marquent jamais qui c' est .
les buralistes , c' est des types braves , pas des balances .
ils crament jamais les blases .
sauf que là , je sais qui est l' enfoiré qui a eu le bol de remporter le gros lot .
c' est notre Shérif international .
il va surement passer à la télé et devenir célèbre .
comme ça , il va échapper aux contrôles d' identité .
ouais , s' il est connu , on n' aura plus besoin de savoir comment il s' appelle .
en tout cas , il le méritait .
ça fait tellement longtemps qu' il essaie .
je suis curieuse de savoir ce qu' il va faire de son argent .
changer de casquette ?
de jean ?
d' appartement ?
de cité ?
de pays ?
peut être qu' il va acheter une villa à Tunis , s' installer là bas et trouver une femme experte en couscous ...
tiens , à propos de mariage , j' ai grillé ma mère .
elle est amoureuse du maire de Paris .
elle kiffe Bertrand Delanoë depuis qu' elle l' a vu à la télé poser la plaque de commémoration à Saint Michel .
c' était en souvenir des algériens balancés dans la Seine pendant la manifestation du dix sept octobre dix neuf cent soixante et un. j' ai emprunté des bouquins sur ça à la bibliothèque de Livry Gargan .
maman a trouvé Bertrand très bien de faire ça pour la mémoire du peuple algérien .
très digne , très classe .
maintenant qu' elle est célibataire , je pense à faire un appel à Bertrand Delanoë .
une grande campagne d' affichage avec la photo de Maman ( celle en noir et blanc qui est dans son passeport ) et en dessous une inscription : " je te kiffe grave , monsieur le Maire , call me ... " il va devenir fou Bertrand s' il voit l' affiche .
en plus je crois que lui aussi est seul dans la vie .
c' est vrai ça , on l' a jamais vu s' afficher avec des meufs .
en plus , ma mère , c' est comme le tiercé : " on a tout à y gagner . " elle cuisine , fait le ménage et même qu' elle tricote .
je parie que personne lui a encore jamais tricoté un calebard en laine à " monsieur le Maire j' ai l' honneur de vous informer que ... " .
il sera super content l' hiver .
l' autre soir , j' ai croisé Hamoudi à côté de la trieuse des poubelles .
il m' a dit qu' il me cherchait justement .
Pff .
c' était même pas vrai .
j' ai bien vu qu' il allait vers chez Lila .
Hamoudi , espèce de mytho !
non , en fait j' ai pas dit ça .
j' ai juste dit : " ah bon ... " on a discuté un peu .
il m' a dit qu' il était désolé de ne plus être aussi présent qu' avant ...
en gros , il m' a fait comprendre qu' il avait une nouvelle vie maintenant et j' ai aussi compris que je n' en faisais plus vraiment partie .
Hamoudi , je te préférais quand tu étais un voyou et que tu faisais des bras d' honneur aux gardiens de la paix .
non , en fait j' ai pas dit ça .
j' ai juste dit : " ouais , d' accord . "
avec Lila , ils ont même des projets de mariage .
c' est la mère d' Hamoudi qui doit être contente .
elle aura réussi à marier tous ses enfants .
" dernier niveau atteint . bonus . vous êtes un winner . " elle a rempli sa mission la daronne .
et puis ça arrive au bon moment .
vingt huit ans , c' est bien , c' est juste avant que sa mère ne commence à se poser des questions ...
" ya Allah , mon Dieu , peut être mon fils c' est une pédale ? ! Hchouma ... "