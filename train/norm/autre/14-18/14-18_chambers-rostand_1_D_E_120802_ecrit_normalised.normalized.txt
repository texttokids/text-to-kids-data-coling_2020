le temps des autres On sait , au fur et à mesure que le temps nous blesse , qu' on peut vivre l' hiver au milieu de l' été .
mais d' ici à ce que le climat se mette au diapason de notre coeur !
la nature du dehors n' est jamais en phase avec notre nature du dedans .
mais il y a des exceptions .
comme dit la chanson , " de la croix du sud au septentrion " le climat est en ce moment à contre saison .
à croire qu' une main maligne a retourné le grand sablier .
il parait que c' est la faute du Nino , un bien joli nom qui veut dire enfant , pour un phénomène atmosphérique qui l' est beaucoup moins , qui entraine ici des crues , là des coulées de boues , bref des débordements insensés où l' homme pèse le poids d' un roseau .
au regard des tempêtes que traversent des pays déjà bien éprouvés comme la Chine et la Russien nos vacances qui vont à l' eau sont de peu d' importance .
ce n' est pas que les tourmentes nous épargnent .
mais inondations et même incendies pour aussi consternants qu' ils soient , atteignent rarement chez nous la dimension apocalyptique qu' ils prennent dans d' autres continents .
la fureur du Nino n' explique pas tout , ne justifie pas tout .
la déforestation massive pratiquée dans les pays dits en développement , est bien pour quelque chose dans les cas d' inondations massives .
les responsables économiques épris de rendements immédiats ne sont pas des futurologues .
ils ont parfois la vue courte et mettent en danger la terre même dont ils tirent leur profit .
les Gaulois avaient raison d' avoir peur du tonnerre .
aujourd'hui nous savons fabriquer des paratonnerres , ce qui n' empêche pas la foudre d' être .
si le temps qu' il fait , qu' il a fait et qu' il fera , constitue l' ossature de nos conversations mondaines , c' est sans doute que nous prenons tous les jours conscience de la faiblesse de nos moyens devant cette maîtresse terrible qu' est la nature .
il n' entre pas dans nos attributs , à moins d' être cinéastes , de faire pleuvoir à volonté ou d' inviter le soleil à notre terrasse .
nous sommes soumis au temps dans tous les sens du terme .
nous essayons d' oublier cette sujétion , en partant à la rencontre d' autres climats et d' autres tropiques .
nous oublions qu' en voyage on n' emporte que soi .
c' est un bagage parfois lourd à porter .
mais qu' on grelotte à plusieurs sous le même soleil pâle , ou qu' on s' échauffe le sang au souffle du désert , les vacances qui nous font bouger et rêver ont une grande utilité : elles nous aident à nous persuader que le lieu où nous habitons n' est pas le centre du monde , que nous sommes solidaires des malveillances du temps .
que l' aile du papillon que nous chassons ici va provoquer un tremblement de terre là bas ...