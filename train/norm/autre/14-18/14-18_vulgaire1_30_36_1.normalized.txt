ça fait longtemps que je suis pas allé graille au McDo mais là n' est pas le sujet .
la dernière fois que j' y suis allé , ça m' a paru dégueu .
comme la dernière fois que je suis allé au Burger King d' ailleurs .
le Quick , n' en parlons pas .
le grec , y a que ça de vrai .
maintenant que le préambule est passé , parlons du film .
si ça a pas de sens , dites vous bien qu' il est tard , que j' ai écouté un son de Jul j' ai cru que c' était la BO de Akira et que j' essayais d' écrire et conceptualiser plus sérieusement mes projets de courts pour le Toronto Porn Film Festival donc c' est pas le moment de me les casser .
le Fondateur donc .
tous les atours du film de merde lambda .
le biopic classique .
l' histoire de merde .
le truc à la NWA quoi .
plus d' un an et demi qu' il est sorti et il me reste toujours en travers de la gorge celui là .
quand je pense que d' autoproclamés cinéphiles continuent de faire des tops de fin d' année comme s' ils étaient dignes de Télérama alors qu' ils avaient dit que NWA était un très bon film ...
ils feraient mieux de s' inscrire à Un Diner Presque Parfait .
donc ça a la gueule d' un biopic lambda et effectivement , on est là dedans pendant la majeure partie du film .
c' est démonstratif .
cousu de fil blanc .
un peu balourd mais pas trop .
ludique comme un biopic l' est souvent .
avec ce côté on raconte un truc chiant mais de façon cool et eh au passage vous apprenez quelque chose , vous pourrez briller devant vos fils de pute de collègue à la prochaine pause dèj .
tu savais toi que le premier PDG de McDonald' s c' était un gros fils de pute ?
oh non , tu m' en diras tant , j' aurais pas cru , t' as vu ce qu' il a fait Jojo de la compta ?
un quatre cent un , si c' est pour se taper ce genre de conversations , c' est pire qu' un cancer de l' amiante , croyez moi .
Y en a qui jouent les fous mais ils vont aller à la galette des rois de leur boite et kiffer même quand ils auront plein de miettes sur leur chemise Celio , les moches à couleur pour participer à un jeu de Nagui et avec des méga boutons d' une autre couleur histoire d' être sûr de te faire ressembler à un bug de Windows quatre vingt dix huit .
ce qu' il se passe d' intéressant dans Le Fondateur c' est qu' à mesure que le film avance , alors qu' il démarrait comme un biopic success story où tu kiffes de voir que l' affaire prend son essor et que ça marche ( alors qu' en vrai on est les premiers à souhaiter voir se casser la gueule les projets du voisin ) mais au fur et à mesure la fils de puterie du personnage principal se révèle par petites touches puis totalement ( et tu vois méga flou quand tu te rends compte qu' elle a toujours été là ) et je pense qu' on a le meilleur rôle de Michael Keaton qui se rend même pas compte d' être un fils de pute , c' est normal pour lui .
il fait ses trucs , ses zumbas , c' est une force qui va , il s' en BLC complet , il fait son truc et il encule tout le monde et le pire c' est qu' à la fin , tout le monde lui donne raison car ce mec est une incarnation de l' Amérique dans ce qu' elle a de plus sombre et infect .
quand t' as de la thune et que tu sais jouer avec le système : tu gagnes à tous les coups .
tu peux même finir président .
Y a des moments du film , même s' il est pas fou où tu vois flou à cause de ce qu' il filme .
entre les mains d' autres auteurs et d' un autre réal , ça aurait pu être vertigineux .
un truc digne de Social Network ou There Will Be Blood mais là , ça reste trop gentil , trop sympa , trop familial , trop fade , trop McDo quoi même si tu sens petit à petit toute la fils de puterie du truc prendre de l' ampleur mais c' est p' têt là que le film est fort .
il devient une incarnation du McDo en fait , c' est de la merde qu' on t' enrobe d' un joli paquet cadeau .
en ça , ça m' a plu .
et soyons honnêtes , je me suis pas trop fait chier , j' ai regardé l' heure que cinq ou six fois et j' ai dû déconnecter totalement qu' une demi heure sur les deux heures du film .
ça va .
et vraiment cette écriture en mode cheval de Troie , c' était pas mal du tout , le film est oubliable et je me rappellerai qu' il existe seulement quand il repassera sur Canal mais ça passe bien .
bravo à EuropaCorp pour la traduction des cartons à la fin et ce magnifique " il implenta " .
les places qu' on doit choisir dans les Gaumont Pathé , c' est vraiment de la merde .
Y a toujours des gens à qui ça tient à coeur qui viennent dire " c' est ma place " et font chier le peuple .
toi t' es là tu dois choisir ta place sur écran alors qu' en vrai le mieux c' est d' arriver dans la salle et voir à côté de qui il faut surtout pas s' asseoir rien qu' en regardant les gueules de trimards qui sont déjà sur leur téléphone luminosité maximale mais vu leur truc fasciste de placement , tu peux plus faire ça .
pour ce film , y a eu quatre incidents de " c' est ma place " et de zumbas derrière .
le pire c' est le fils de pute qui arrive quand le film a commencé et cherche sa place .
Y a des méningites qui se perdent .