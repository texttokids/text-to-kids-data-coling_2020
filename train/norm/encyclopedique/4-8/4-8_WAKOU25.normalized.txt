soigneur d' un jour au zoo de Beauval
occuper des animaux jour après jour , c' est le rôle soigneurs .
Damaris a passé matinée avec eux .
regarde !
je commence par préparer la nourriture pour les grands singes : des fruits et des légumes !
tous les matins , c' est Amandine qui pèse les rapaces pour vérifier qu' ils sont en parfaite santé !
cette maman gorille vient her à manger .
son bébé , et encore son lait .
de tous les singes , et peut être même de tous les animaux , l' orang outan est celui qui aime le plus les fruits !
ce manchot de Humboldt est blessé à l' aile .
il doit rester à l' écart , et j' accompagne Anaëlle , une soigneuse , qui vient le nourrir chaque jour .