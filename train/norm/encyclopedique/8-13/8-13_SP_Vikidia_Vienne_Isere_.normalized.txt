vienne , est une ville française de trente habitants , située dans la région Auvergne Rhône Alpes et le département de l' Isère .
la ville est construite au confluent du Rhône et de la Gère un .
elle est , avec La Tour du Pin , l' une des deux sous préfectures du département .
son agglomération compte soixante huit habitants et son aire urbaine compte cent dix habitants .
vienne , située au nord de la vallée du Rhône , est entourée de massifs montagneux : le Massif central à l' ouest et les Préalpes à l' est .
la ville est traversée par le Rhône et deux de ses affluents : la Gère et la Sévenne .
vienne est dominée par douze collines principales dont l' altitude varie de cent cinquante mètres à quatre cent huit mètres deux .
occupant une place privilégiée à la croisée de plusieurs routes , le site de Vienne , choisi par les Allobroges , est fermé par cinq collines , qui offrent un intérêt défensif .
le cours irrégulier du Rhône menace des terrasses inondables jusqu'à la fin du IIe avant Jésus Christ
durant le Haut Empire , Vienne connait une urbanisation spectaculaire et une parure monumentale lui rend compte de son importance .
la ville se développe à l' extérieur de l' enceinte , sur la rive gauche du Rhône , au sud , et sur la rive droite .
à la fin du IIIe siècle et au IVe siècle , la ville , repliée dans son centre , n' occupe plus qu' une vingtaine d' hectares .
les évêques prennent le relais des institutions civiles défaillantes .
vienne , cité sainte , voit le clergé affirmer son emprise , avec au premier rang les archevêques , les couvents des ordres mendiants s' ajoutent aux abbayes bénédictines .
un nouveau réseau de rues étroites est mis en place .
aux XIIIe et XIVe siècles , le quartier d' Outre Gère est protégé par des remparts , comme le quartier de Fuissin au sud .
avec la révolution industrielle , grâce à l' essor amorcé au XVIIIe siècle , l' économie viennoise prospère , avec de nombreuses usines liées aux activés textiles et à la métallurgie .
puis la ville , desservie par le chemin de fer , s' étend au nord , à l' est et au sud .
la ville de Vienne et son agglomération sont équipées d' un réseau de bus trois .
elle compte deux gares desservies par le réseau L' va : vienne centre et Vienne Estressin .
l' aérodrome de Vienne Reventin propose des activités aéronautiques quatre .
à l' époque gauloise , la confluence du Rhône et de la Gère est un carrefour entre le monde méditerranéen , les Alpes , la Gaule du nord et de l' ouest .
le Rhône inonde toute la plaine , le nom gaulois Vigenna pourrait signifier sources , marais .
au IIe siècle avant Jésus Christ , le Rhône devient plus étroit et plus profond , permettant l' occupation de terrasses autrefois inondables .
la ville est alors la capitale du peuple gaulois des Allobroges , qui tient aussi la rive droite du Rhône .
en cent vingt et un , les Romains triomphent dans la région et annexent l' Allobrogie dans la province de Gaule transalpine .
à l' époque gallo romaine , les urbanistes conquièrent d' importants remblais des terrains constructibles sur les deux rives du Rhône .
parmi ces nouveaux quartiers , les entrepôts fluviaux occupent quatre à six hectares sur la rive gauche .
cette étendue importante pourrait s' expliquer par la localisation à Vienne d' une partie du stockage de l' impôt impérial pour le ravitaillement du blé à Rome .
au IIIe siècle , la crise de l' Empire , politique , économique et démographique , touche Vienne , dont plusieurs quartiers sont abandonnés , comme Sainte Colombe et Saint Romain en Gal .
au début du IVe siècle cependant , la redistribution des provinces de l' Occident romain profite à Vienne : les empereurs lui accordent le rang de chef lieu de la province de Viennoise .
tout au long de ce siècle , la communauté chrétienne , mentionnée dès cent soixante dix sept , s' organise autour de son évêque cinq .
dès le Moyen Âge , Vienne est réputée pour l' ancienneté de sa communauté chrétienne .
la ville s' organise peu à peu autour du groupe cathédral , avec des églises funéraires et des cimetières et de monastères tels que Saint André le Haut .
aux Ve , VIe et VIIe siècles , des évêques jouent un rôle important , tant sur le plan politique que spirituel : Mamert , Avit et Didier sont les plus connus .
le rôle dominant des évêques tire parti des changements politiques au moment de la création des royaumes burgonde puis franc .
en huit cent soixante dix neuf , à la suite du démembrement de l' Empire carolingien , le comte Boson , comte gouverneur de Vienne , se fait proclamer roi de Bourgogne Provence et fait de Vienne sa capitale .
en mille vingt trois , le comté de Vienne est donné de la main du dernier roi de Bourgogne , qui garda le contrôle de la ville et partage le reste de son territoire entre le comte de Maurienne ( future Savoie ) et le comte d' Albon ( futur Dauphiné ) .
en mille trente deux , Vienne est intégrée au Saint Empire romain germanique .
les deux derniers siècles du Moyen Âge sont marqués par la guerre de Cent Ans , les difficultés économiques et les épidémies , dont la peste noire de treize cent quarante huit mille trois cent quarante neuf .
dotée de six habitants au début du XIVe siècle , Vienne ne possède plus que trois cinq cents habitants au XVe siècle .
le pouvoir du roi de France s' affirme en treize cent onze mille trois cent douze , à l' occasion d' un évènement de portée internationale .
durant le concile réuni à Vienne sous la présidence du pape Clément V , l' ordre religieux et militaire des Templiers est aboli , à la suite des pressions exercées par le roi de France Philippe le quatrième le Bel .
en treize cent cinquante cinq , Philippe le quatrième incorpore à son royaume Sainte Colombe ( qui était depuis l' Antiquité un quartier de Vienne ) , sur la rive droite du Rhône , et y construit la tour des Valois .
en quatorze cent cinquante , le dauphin Louis le deuxième ( futur Louis le onzième de France ) , impose définitivement le rattachement de Vienne au Royaume de France ( cent un ans après le rattachement du Dauphiné à la France ) .
ce rattachement eut deux conséquences majeures : l' archevêché perd son pouvoir temporel et la ville , n' étant plus en position frontière , est davantage éclipsée par Lyon cinq .
si la noblesse et la bourgeoisie de Vienne prospèrent à la Renaissance , la fin du XVIe siècle est marquée par les guerres de Religion .
en quinze cent soixante deux et mille cinq cent soixante sept , les troupes protestantes du baron des Adrets prennent Vienne .
plusieurs édifices religieux , dont la cathédrale , sont dévastés .
vienne est la dernière cité du Dauphiné à se soumettre à Henri le quatrième , en quinze cent quatre vingt quinze .
face à Grenoble , le rôle accordé à Vienne s' amoindrit .
en seize cent trente trois , les fortifications de Pipet et de La Bâtie sont démantelées sur ordre de Richelieu .
au début du XVIIe siècle , le renouveau catholique se traduit par la restauration des édifices des anciens ordres religieux , la construction de l' abbaye Saint André le Haut et l' arrivée de nouveaux ordres .
la ville prend un nouvel élan dès les années dix sept cent vingt grâce au développement de manufactures consacrées au textile et à la métallurgie , installées dans la vallée de la Gère .
en dix sept cent soixante douze , Pierre Schneyder se voit confier le soin de dresser un plan d' alignement des rues afin d' aérer la ville et d' améliorer la circulation , rendue difficiles par l' étroitesse des rues héritées du Moyen Âge .
ce projet sert de schéma directeur pour les modifications ultérieures , privant de nombreuses maisons de leurs façades .
l' année mille sept cent quatre vingt dix constitue un tournant .
l' archevêché est supprimé et la cathédrale est réduite au rang d' église paroissiale .
la division du Dauphiné en trois départements s' effectue aux dépens de Vienne , qui devient chef lieu de district , puis d' arrondissement et sous préfecture de l' Isère .
la délimitation entre département de l' Isère et du Rhône crée une séparation définitive entre Vienne et les communes de la rive droite du Rhône .
la vente des Biens nationaux à partir de mille sept cent quatre vingt onze et le regroupement des paroisses réduisent très fortement l' emprise religieuse sur la ville cinq .
à la fin du XVIIIe et au début du XIXe siècle , Vienne se dote d' équipements culturels : bibliothèque , musée , salle de spectacle .
la cité redécouvre en même temps son passé antique sous l' impulsion de Schneyder , puis Delorme .
avec eux les premières restaurations commencèrent .
l' inscription de cinq édifices viennois sur la première liste de protection des Monuments historiques en mille huit cent quarante donne une ampleur nouvelle cette politique .
la volonté de sauvegarder le patrimoine aboutit à la création de la Société des amis de Vienne en mille neuf cent quatre .
quant à la gastronomie , elle est enrichie par la famille Point , par l' acquisition , notamment , du restaurant de la Pyramide ( en mille neuf cent vingt trois ) .
marquée à partir des années mille neuf cent cinquante par la crise de ses industries , Vienne s' est tournée vers des activités tertiaires .
durant la deuxième moitié du XXe siècle , l' habitat social s' est étendu dans les quartiers d' Estressin , de l' Isle , de la Vallée de Gère et de Malissol .
plusieurs changements majeurs concernant les voies de communication marquent le paysage urbain depuis les décennies mille neuf cent soixante mille neuf cent soixante dix .
l' année mille neuf cent soixante cinq a vu la conclusion des travaux de la traversée dans la ville de l' autoroute À sept .
face aux effets néfastes de la circulation de cet axe , l' autoroute a été déviée sur la rive droite du Rhône en mille neuf cent soixante quinze .
en mille neuf cent soixante sept , on recouvrit le confluent de la Gère et du Rhône pour permettre la liaison par viaduc à la route de Grenoble cinq .
sous préfecture de l' Isère , Vienne est le siège d' une communauté d' agglomération .
entendue depuis deux mille deux de dix sept à trente communes , elle est à cheval entre les départements rhodaniens et isérois .
appelée autrefois Communauté d' Agglomération du Pays Viennois ( puis ViennAgglo ) , au premier janvier deux mille dix huit, elle a pris le nom de " vienne Condrieu Agglomération " six .
le Jardin du huit mai dix neuf cent quarante cinq( ou Jardin de Ville ) a été acquis en dix sept cent quatre vingt onze par la commune de Vienne , qui l' a transformé en une vaste esplanade , le Champ de Mars .
la création d' un jardin public ne remonte qu' à dix huit cent quatre vingt quinze dix huit cent quatre vingt dix sept .
le temple d' Auguste et de Livie est placé au centre ville , près du forum de l' époque gallo romaine .
il a été construit dans les années vingt cinq dix avant Jésus Christ Le temple est parvenu jusqu'à nous quasiment intact .
il a été érigé sur une plate forme aménagée .
des portiques à colonnades délimitaient autour du temple une aire sacrée reconnue au cours de fouilles anciennes .
vers l' est s' étendait l' esplanade du forum , fermée par un bâtiment public auquel appartiennent les deux arcades visibles dans le jardin de Cybèle .
le tout formait un vaste ensemble monumental public clos qui exprimait , dans de majestueuses architectures à colonnades , les valeurs civiques ou les fonctions administratives de la ville gallo romaine .
à partir du début du Christianisme , soit la fin de l' empire romain , le temple est reconverti en église ( Notre Dame de la vie ) puis en tribunal pendant la Révolution française .
ce temple dédié au culte de Rome et d' Auguste dans la cité gallo romaine , aussi célèbre que la Maison Carrée de Nîmes , doit sa survie à ces transformations .
l' Autel en pierre reconstruit est une restauration dont nous n' avons aucune attestation archéologique , mais néanmoins possible .
cet autel servait a pratiquer le culte et aux sacrifices d' animaux aux dieux .
derrière les colonnes se trouve une grande porte en bois qui donne sur une pièce qui s' appelle la cella où se trouvait la statue du dieu .
c' est un des deux seuls temples romains hexastyles de France , avec la Maison Carrée de Nîmes .
le Théâtre antique , construit entre quarante et cinquante après Jésus Christ , a ses gradins sont adossés à la colline du Pipet .
il a été abandonné et les pierres de ce monument en ruine ont été réutilisées pendant de nombreux siècles pour fabriquer des maisons sur les vestiges de ce théâtre .
il a une capacité d' environ treize spectateurs .
on utilisait le théâtre pour des spectacles de tous genres , mais aussi pour pour des rassemblements civiques ou officiels .
en dix huit cent trente quatre , l' archéologue Claude Thomas Delorme réussit à convaincre l' inspecteur général des monuments historiques qu' au pied de la colline Pipet se trouvaient des vestiges qui ressemblaient à ceux d' un théâtre romain et non à ceux d' un amphithéâtre : on doit attendre les fouilles de dix neuf cent huit à dix neuf cent trente huit pour dégager et restaurer le monument .
l' Odéon , visible sur le flanc nord de la colline , a longtemps été pris pour un théâtre .
on a découvert dans les vestiges un bloc de marbre portant l' inscription ODEV ( odeum ) qui définit la fonction de l' édifice .
l' odéon est une construction d' origine grecque , qui se présente la plupart du temps comme un petit théâtre couvert : ces petits édifices étaient destinés à l' audition de spectacles de chants ( ôdè , chant , en grec ) , de concerts et de lectures .
la présence d' une toiture permettait une meilleure acoustique .
ils étaient souvent richement ornés .
seuls deux odéons sont connus en Gaule romaine : à Lyon et à Vienne .
celui ci a été découvert en dix neuf cent soixante treize .
la Pyramide ( ou Pyramide de l' obélisque ) , s' élève aujourd'hui presque intacte au sud de Vienne .
elle décorait le centre de la barrière ( spina ) d' un cirque de près de quatre cent soixante mètres de long , construit à la fin du IIe siècle .
cet édifice était destiné à accueillir des courses de chars .
mais à Vienne , en l' absence d' amphithéâtre et de stade , on y organisait peut être des jeux athlétiques , des combats de gladiateurs ou des chasses d' animaux sauvages .
grâce à des fouilles réalisées en dix huit cent cinquante trois , dont les relevés sont d' une précision exceptionnelle , nous connaissons en partie l' architecture de ce cirque .
les vestiges du Jardin de Cybèle appartiennent à trois ensembles gallo romains : le forum , une salle d' assemblée , un quartier d' habitations .
deux arcades du forum sont encore en élévation au nord du jardin archéologique .
la Primatiale Saint Maurice ( ou ancienne cathédrale Saint Maurice ) , édifice mélange d' art roman et gothique , fut réduit au rang d' église paroissiale , en dix sept cent quatre vingt dix , après la suppression de l' archevêché ,
la Chapelle de Notre Dame de Pipet , haut lieu consacré par les Viennois à la Vierge Marie concrétisé par l' inauguration en dix huit cent soixante d' une statue en pierre de Volvic , puis par la construction en dix huit cent soixante treize d' une chapelle de pèlerinage en l' honneur de Notre Dame de la Salette ,
l' ancienne Église Saint Pierre , fondée au VIe siècle dans un cimetière occupant les ruines d' un quartier d' habitation gallo romain ,
l' Église de Saint André le Bas , fondée au VIe siècle sur des soubassements gallo romains , l' ancienne abbaye domine encore le confluent du Rhône et de la Gère ,
l' ancienne Abbaye de Saint André le Haut , fondé probablement au VIe siècle , par Ansemond , pour sa fille Remila , c' était un monastère de femmes ,
la Chapelle Saint Théodore , son portail était autrefois surmonté de trois blasons , la chapelle a été construite à la fin du XVIe siècle par le chanoine Claude de Virieu ,
l' Église de Saint André le Haut , chapelle , placée sous le patronage du roi Saint Louis , est construite entre seize cent quatre vingts et dix sept cent vingt cinq conformément au style jésuite .
le Château de la Bâtie , édifié par l' archevêque Jean de Bernin au XIIIe siècle sur le Mont Salomon , en même temps qu' une nouvelle enceinte , s' appuyant sur des ruines romaines ,
le Centre d' art contemporain , se trouvant sur l' emplacement de l' ancienne Halle des bouchers ,
le Théâtre municipal , est l' emplacement de l' ancienne maison des consuls , que Pierre Schneyder construit à ses frais , en dix sept cent quatre vingt un dix sept cent quatre vingt deux , une salle de spectacle ,
l' Hôtel de ville , après sa réorganisation en dix sept cent soixante huit , le corps municipal acquiert en dix sept cent soixante et onze l' hôtel particulier du marquis de Rachais pour en faire l' hôtel de ville ( l' entrée se faisait alors rue Marchande par une porte cochère ) .
le festival Jazz à Vienne a lieu chaque année pendant deux semaines , fin juin et début juillet ,
la Foire de Vienne a lieu chaque année en octobre ,
la Fête des Lumières a lieu chaque huit décembre à Vienne , c' est une variante de la fête lyonnaise .
au Moyen Âge , à Vienne , on parlait le " viennois " , qui est une variante locale de l' arpitan .
dès la Renaissance , dans la ville , on parle une variante locale du français , appelé " parler viennois " , un dialecte arpitan influencé par le " lyonnais " et le " dauphinois " .
cette langue était encore parlée au début du XXe siècle .
de nos jours , on parle français , mais avec encore des mots et expressions venant du parler viennois , ainsi que du parler lyonnais .
le Musée archéologique Saint Pierre , restaurée dans la seconde moitié du XIXe siècle , l' église abrite depuis mille huit cent soixante douze une partie des collections archéologiques de la ville de Vienne ,
le Musée des beaux arts et d' Archéologie , au premier étage d' un bâtiment surélevé en dix huit cent soixante quinze .
ce musée expose une autre partie des collections archéologiques de la ville ,
le Musée du cloitre de Saint André le Bas , d' abord restauré en dix neuf cent trente huit , en vingt cent dix vingt cent onze , ses galeries sont nettoyées et restaurées afin de rendre vie et lumière au cloitre ,
le Musée de la Draperie , sert de mémoire à l' industrie textile , qui a marqué Vienne du XVIIIe siècle au XXe siècle ,
le Musée gallo romain de Saint Romain en Gal , situé sur la rive droite du Rhône , dans le département du Rhône .
dans ce musée , mosaïques , peintures murales , céramiques sont associées à de nombreuses maquettes pour comprendre la ville antique .
il y a , à Vienne , de nombreux clubs sportifs amateurs dont le plus connu est celui de rugby : le Club sportif de Vienne rugby est un club de rugby en fédérale un .
ce club joue au Stade Jean Etcheberry .