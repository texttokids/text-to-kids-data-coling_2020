Rome , surnommée la " ville éternelle " , est la capitale de l' Italie , chef lieu de la région du Latium .
elle compte environ deux , cinq millions d' habitants .
c' est la plus grande ville d' Italie .
capitale de l' Empire Romain pendant l' Antiquité et siège du Vatican , Rome a exercé une grande influence dans l' histoire du monde .
le Risorgimento la fait devenir la capitale de l' Italie et le siège des institutions publiques .
Rome est une des grandes places économiques de l' Italie ( avec Milan ) et de nombreuses entreprises y ont leur siège .
c' est aussi un important centre médiatique du pays ( journaux , radios , chaines de télévision ...

Rome est visitée par près de douze millions de touristes chaque année .
Rome est située dans la région du Latium , au centre de l' Italie .
elle est traversée par le Tibre , à vingt deux kilomètres de la mer Tyrrhénienne .
sa superficie est d' environ un deux cent quatre vingt cinq kilomètres carrés , ce qui en fait la commune la plus grande d' Italie , dépassant Paris , Londres et New York en termes de superficie .
il existe vingt neuf communes limitrophes autour de Rome .
le site de Rome comprend sept collines : le Capitole , le Palatin , le Viminal , le Quirinal , l' Esquilin , le Caelius et l' Aventin .
les collines de Rome rassemblent encore tout le centre ville .
au début de la cité ( au VIe siècle avant Jésus Christ ) , elle comptait probablement dix sept habitants .
à l' apogée de l' Empire romain , Rome était la cité la plus grande et la plus peuplée au monde avec un million d' habitants ( environ ) .
cependant après les divers sacs suivant la chute de l' Empire Romain , elle traversa une période assez sombre .
au XVIe siècle , elle comptait cent habitants .
après le Risorgimento , la population passa les cinq cents habitants , puis ce nombre s' est élevé au long du XXe siècle .
des années dix neuf cent quatre vingts aux années vingt cent zéro , la population stagna et même recula un peu .
de vingt cent deux à vingt cent sept , la population augmenta , passant deux huit cents à deux huit cent quatre vingt dix habitants .
l' immigration ( particulièrement celle de l' Europe de l' Est ) a toujours constitué une grande communauté ( environ trois cent soixante étrangers " légaux " ) .
aujourd'hui , elle est la quatrième ville la plus peuplée en Europe après Londres , Berlin et Madrid .
selon la légende , Rome a été fondée en sept cent cinquante trois avant Jésus Christ sur le mont Palatin par Romulus , fils du dieu de la guerre ( Mars ) .
Romulus avait été recueilli et élevé , avec son frère Rémus , par une louve .
depuis , la louve est un des symboles de Rome .
entre le VIIe siècle avant Jésus Christ et le Ie siècle , l' État de Rome qui n' était au début qu' un regroupement de villages sur le site des sept collines de Rome , étend énormément son territoire jusqu'à devenir l' Empire romain , dont Rome reste la capitale .
son régime est une monarchie , puis une république et un Empire après Jules César .
sous l' Empire , la ville antique comptait à peu près un demi million habitants ( la plus grande ville du monde de l' époque ) .
de nombreux monuments sont bâtis : des temples , le Forum romanum , le Colisée .
partout dans l' Empire , les Romains construisaient des villes romaines imitant leur capitale .
le christianisme , qui se développe à Rome sous l' Empire , devient la religion officielle au IVe siècle .
avec les invasions des Barbares sur l' Europe , Rome est prise au Ve siècle avant Jésus Christ , ce qui provoque sa destruction partielle .
suivent mille ans durant lesquels l' activité et la puissance de la ville sont réduites .
la ville reste néanmoins le siège des papes , sauf de mille trois cent neuf à mille quatre cent vingt , quand les papes ont siégé en Avignon , deux ou trois personnes prétendant être le pape dans plusieurs villes en même temps .
par la suite , les papes redonnent à Rome une bonne partie de sa splendeur , faisant venir les plus grands artistes , comme Michel Ange ou Raphaël .
les grandes familles aristocratiques construisent des palais comme le Palais Farnèse ou le Palais Borghese .
Rome a été le centre majeur de l' Antiquité , et plus tard , durant la Renaissance , cette situation centrale réapparait grâce à l' installation du Vatican .
Rome est annexée par Napoléon Bonaparte au royaume d' Italie qu' il a créé .
après sa défaite , en dix huit cent quinze , Rome repasse sous le contrôle du pape .
au milieu du XIXe siècle , avec le Risorgimento , elle devient la capitale du nouveau royaume d' Italie puis de la république à partir de dix neuf cent quarante six .
elle est donc le siège des institutions de l' État .
le fascisme donne à Rome une place centrale .
de nouveaux bâtiments sont construits : Mussolini veut qu' elle devienne la capitale du régime fasciste .
les quartiers périurbains et les banlieues sont aménagés .
l' exposition universelle de dix neuf cent quarante deux , qui devait s' y dérouler , est annulée .
après la guerre , la ville continue à s' étendre avec la création de nouveaux quartiers .
Rome utilisa son exceptionnel patrimoine antique pour le mettre à profit aux évènements qui furent organisés dans la ville ( Olympiades , Coupe du monde de football , et caetera ) .
l' architecture religieuse est très présente à Rome , elle est le témoin du passé glorieux de la ville .
les premiers lieux de culte furent les temples romains .
il ne servaient pas aux cérémonies , mais ils étaient utilisés pour accueillir la statue cultuelle de la divinité .
il devait y avoir environ une centaine de temples à Rome .
les églises sont une partie constituante du patrimoine de la ville .
on les compte par centaines et leur histoire reflète le passé économique , culturel et politique de Rome .
certaines d' entre elles étaient d' anciens temples romains ( comme le Panthéon ) .
la cathédrale de Rome est la basilique Saint Jean de Latran , qui fait partie des quatre basiliques patriarcales et papales majeures .
des millions de pèlerins venant du monde entier effectuent chaque année ce pèlerinage .
voir aussi : basilique Saint Pierre , Basilique Saint Jean de Latran , Basilique Sainte Marie Majeure , Basilique Saint Paul hors les Murs
il existe également de nombreux tombeaux ( notamment le long de la Via Appia ) , des mausolées et on peut visiter quelques catacombes ( dont celles de Saint Sébastien ) .
Rome étant la capitale de l' Empire romain , elle disposait de nombreux édifices imposants .
l' enrichissement de la ville était assuré par le commerce et les taxes levées dans les colonies de l' Empire .
le Colisée est le plus grand des amphithéâtres , avec ses cinquante spectateurs et ses cinquante mètres de haut .
construit sous Vespasien en soixante douze et inauguré en quatre vingts par son fils Titus , il accueillait les jeux du cirque ( combat de gladiateurs , fauves , naumachies ...

le forum romain formait le coeur politique , commercial et judiciaire de la ville .
on y trouvait de nombreux temples et des institutions ( la Curie , les basiliques et caetera ) .
quelques empereurs firent construire leur propre forum ( César , Trajan et caetera ) .
la Via Sacra était une voie empruntée lors des processions religieuses qui se dirigeaient vers le Capitole .
le marché de Trajan comptait plus de cent cinquante boutiques à l' époque .
les arcs ( de triomphe ou pas ) servaient à glorifier l' empereur qui les avait construits et étaient empruntés lors des cérémonies religieuses ( les arcs situés sur la Via Sacra étaient des arcs de triomphe ) .
les thermes de Caracalla pouvaient recevoir plus de quinze cent zéro personnes .
le Circus Maximus était le lieu où se déroulaient les courses de chars .
cependant , certains monuments romains furent transformés en d' autres édifices ( églises , mairie et caetera ) .
voir aussi : mur d' Aurélien , Porte Majeure , Aqueduc de Claude
l' architecture civile de la ville forme un ensemble architectural assez hétéroclite , des palais seigneuriaux du Moyen Âge aux villas papales luxueuses en passant par le Colisée carré , emblème de l' Italie fasciste ou encore aux constructions modernes et contemporaines de l' EUR .
de nombreuses places sont très célèbres .
la Piazza Venezia est le centre de la Rome d' aujourd'hui et d' hier ( il y a plusieurs bâtiments de différentes époques ) .
la place Saint Pierre qui se trouve devant la basilique Saint Pierre .
la place d' Espagne ou piazza di Spagna , où se trouvent ses fameux escaliers .
la place Navone fut construite sur un ancien cirque : c' est la cause de sa forme ovale , puis le Bernin y exécuta une de ses plus belles oeuvres : la fontaine des Quatre Fleuves .
les villas et les palais des familles seigneuriales , papales ou cardinales sont visitables .
la plupart d' entre elles ont été décorées ou construites par des artistes très connus ( comme Raphaël , Bramante ou Michel Ange ) .
lors de l' annexion de Rome au Royaume d' Italie , son statut de capitale du pays fit construire de nombreux édifices destinés aux besoins de l' administration : le palais du Quirinal , siège du gouvernement , le palais Madame , siège du Sénat , le palais Montecitorio , siège de la Chambre des députés ou encore le Palais Koch , siège de la banque d' Italie .
lors de la prise de pouvoir par Mussolini , de nombreuses choses changèrent .
on détruisit de nombreux quartiers historiques afin de construire de larges avenues et des bâtiments .
l' architecture fut au service du régime .
les paysans durent rester à l' écart du centre ville car ce que redoutait le plus Mussolini , c' est qu' ils deviennent des habitants des villes : chômeurs ou ouvriers .
ces derniers étaient ses pires ennemis , car ils adhéraient souvent aux mouvements politiques socialistes et communistes , pensées contraires au fascisme .
le symbole de cette nouvelle architecture est le Colisée carré , situé dans la banlieue de Rome ( dans le quartier de l' EUR ) .
hors de la Rome urbanisée , on peut découvrir de très belles villas ( Villa d' Este , d' Hadrien ...

le Palais Farnèse , aujourd'hui siège de l' ambassade de France
on peut se promener dans les nombreux parcs de la ville .
il existe de nombreux obélisques rapportés d' Égypte par les Romains ( comme celui du Vatican ) ou construits intentionnellement .
il y a également quatorze colonnes monumentales , dont la colonne Trajane .
Rome est le berceau de la Rome Antique , les empereurs y édifièrent de très nombreux bâtiments et firent venir des artistes venus de tout l' Empire .
le statuaire romain est très inspiré du statuaire grec .
en effet , on a retrouvé de nombreuses copies romaines en marbre de sculptures grecques ( en bronze ou en pierre ) .
la caractéristique de ce type de statuaire est principalement le réalisme .
cependant les Romains se démarquent des Grecs sur de nombreux points .
le corps est " idéalisé " et " parfait " .
le visage chez les Romains est très important .
c' est grâce à cela qu' on reconnait un empereur à un autre .
c' est pour cela qu' il pouvait décider comment il devait être représenté .
la symbolique était très présente : tout était choisi pour que ça représente quelque chose de bien précis .
après l' officialisation du christianisme , le prestigieux art romain fut délaissé .
l' art paléochrétien fait son apparition ( même s' il existait déjà depuis deux cents environ ) .
on peut en trouver de très beaux exemples dans les catacombes de Saint Sébastien .
il est caractérisé par le foisonnement de symboles images ( la colombe , le Chrisme ou encore le poisson ...

le Moyen Âge est marqué par la construction de nombreuses tours par les seigneurs locaux pour étendre leur pouvoir .
Rome devient également aux XV et XVIe siècles , un des principaux centres de diffusion de la Renaissance .
les papes et les aristocrates attirent les artistes les plus réputés de l' époque .
certains y viennent étudier les antiques et l' art romain .
michel ange construit la cathédrale Saint Pierre et décore la Chapelle Sixtine , Raphaël peint de nombreuses fresques ...
le baroque est la continuité de la Renaissance , il reflète le désir d' impressionner les fidèles dans les églises et donc de limiter l' influence croissante de la Réforme ( le protestantisme ) .
le mouvement et les formes sont privilégiées .
le Bernin est un exemple d' artiste baroque .
au XVIIe siècle , Rome perd son rôle de capitale européenne , la production artistique est en déclin .
avec son nouveau statut de capitale du Royaume d' Italie en mille huit cent soixante dix , Rome dut s' adapter aux évolutions du monde moderne .
le fascisme rénova le visage de la ville .
les édifices qui furent construits à cette époque étaient au service du régime fasciste et profondément modernes par rapport à l' histoire antique de la cité .
Rome possède un grand nombre de musées .
situés sur le Capitole , les musées Capitolins constituent une grande collection d' antiquités romaines et étrusques .
la villa Borghese rassemblent un important fonds de sculptures ( dont plusieurs oeuvres du Bernin ) .
il existe également de nombreux musées consacrés à l' art moderne et contemporain .
les Musées du Vatican accueillent chaque année plusieurs millions de touristes .
laocoon et ses fils , groupe antique conservé aux Musées du Vatican .
statue de Marc Aurèle , conservé aux Musées du Capitole
Rome a été le décor d' une multitude de films célèbres .
elle possède son grand complexe cinématographique , Cinecittà , où furent tournés entre autres Gladiator , Ben Hur ou encore Gangs of New York de Martin Scorsese .
Federico Fellini , réalisateur romain très connu , utilisa Rome pour certains de ses films : son film La Dolce Vita et la scène où Anita Ekberg et Marcello Mastroianni se baignent dans la Fontaine de Trevi ( alors à l' époque peu connue ) sont particulièrement célèbres .
Sergio Leone y tourna Le Bon , la Brute et le Truand .
de nombreux films sont liés à Rome ( même si certains ne sont pas tournés sur place ) , ceci est principalement dû à son histoire : le péplum comme Gladiator ou Ben Hur , le genre religieux et ésotérique comme Anges et Démons .
d' autres cinéastes ont tourné ou joué dans la ville : Dario Agento , Alberto Sordi , Vittorio de Sica , Sergio Leone , et d' autres encore .
la cuisine romaine à l' origine est celle de la Rome antique ( voir les aliments dans la Rome antique ) .
la cuisine traditionnelle romaine de notre époque est composée de plats aux produits du terroir ( légumes , fruits , fromages , viandes , fruits de mer ...

on peut y déguster de nombreux types de pâtes dont la célèbre " pasta alla carbonara " , préparée avec du lard , de la crème et du jaune d' oeuf .
les antipasti et les bruschette accompagnent le repas , typiquement méditerranéen .
la première religion pratiquée à Rome était celle des Romains .
la religion romaine est polythéiste et constituée d' un ensemble de mythes et une mythologie très détaillée .
il existait un " cycle " de cérémonies qu' ils suivaient scrupuleusement ( les Ides , les Lupercales etc ...

les " pénates " , créatrices de la richesse domestique ) et les " lares " , protecteurs de la maison .
mais avec l' expansion de l' empire , de nouveaux dieux étrangers furent intégrés et les cultes à mystères apparurent ( Mithra ...

au Ier siècle , le christianisme commença à se répandre .
cependant , les chrétiens furent martyrisés pour le refus d' adorer les dieux Romains ( parce que le christianisme est monothéiste ) et de célébrer le culte de l' empereur .
pour certains , ils étaient dangereux pour la cohésion de l' Empire ( pour cette raison Marc Aurèle laissa faire les exécutions de chrétiens ) .
avec la conversion de Constantin Ier en trois cent treize et l' officialisation de la religion chrétienne dans l' Empire , commença l' expansion du christianisme .
rapidement la religion romaine fut abandonnée et de nombreux temples furent mis à sac et transformés en églises .
les grands prédicateurs de l' époque voulaient supprimer toute trace de cette époque " païenne " .
les premières églises " officielles " furent construites .
le spectaculaire de l' art baroque et la Renaissance firent resplendir la religion chrétienne .
depuis juin vingt cent seize, le maire de la ville est Virginia Raggi deux noeuds , du Mouvement cinq étoiles deux noeuds .
Rome possède une économie plutôt dynamique et diversifiée .
mais l' industrie ne s' est développée qu' après la Seconde Guerre mondiale .
ceci explique un faible pourcentage d' emploi dans ce secteur ( environ vingt pour cents ) .
il concentre principalement les industries lourdes , de luxe , du cinéma , de l' imprimerie ( et de l' édition ) .
plus récemment , elle se tourne vers les hautes technologies .
le secteur tertiaire concentre soixante quinze pour cents de la population active .
le coeur des quartiers d' affaires est le quartier Esposizione Universale di Roma ( EUR ) .
sur le plan national , Rome se place à la première place en Italie pour la croissance économique .
elle commence à dépasser sa rivale lombarde , Milan .
les deux principaux aéroports sont l' aéroport Léonard de Vinci de Rome Fiumicino et l' aéroport de Ciampino .
la capitale possède plusieurs gares ferroviaires dont la plus importante est la gare Termini , implantée au centre de la ville .
Rome possède deux lignes de métro ( À et B ) qui ont une correspondance à la gare Termini .
des travaux sont prévus pour la construction de deux autres nouvelles lignes .
les autocars interurbains ont leur terminus à Tiburtina .
le Tourisme est un des atouts majeurs de la capitale .
Rome est la première ville visitée en Italie pour le nombre de touristes et la deuxième ville au monde ( après Paris ) .
le centre historique concentre la majorité des visites ( quatre vingt dix sept pour cents ) .
Rome n' est jumelée qu' avec une seule ville : paris .
le slogan de ce jumelage est : " seule Rome est digne de Paris et seul Paris est digne de Rome " .
mais elle possède de nombreux partenariats et des pactes d' amitié avec des villes du monde entier .
Rome est un important centre universitaire et possèdes de nombreuses écoles .
on peut y trouver cinq cent vingt huit écoles primaires , cent cinquante deux lycées ou encore trois instituts d' art .
elle est à la première place en Italie pour le nombre d' écoles maternelles , primaires , de lycées et de collèges .
l' université La Sapienza , est une des plus anciennes un et des plus grandes universités au monde ( et la plus grande d' Europe ) pour le nombre d' étudiants ( cent cinquante ) .
Rome a accueilli les Jeux Olympiques en dix neuf cent soixante et la première édition des Jeux Paralympiques la même année .
en mai , lors de la troisième semaine , se déroulent les Masters de Rome , deuxième tournoi de terre battue en importance après le tournoi de Roland Garros .
le marathon de Rome , quand à lui , traverse les nombreux lieux historiques de la cité ( le Forum Romain , Saint Pierre ...

l' Associazione Sportiva Rome ( AS Rome ) et la Società Sportiva Lazio ( SS Lazio ) , les deux clubs de football de la cité , ces deux clubs s' affrontant régulièrement en Championnat d' Italie de football durant le derby de Rome .
ils jouent dans le stade olympique de Rome .
les Gladiateurs de Rome , club de football américain .
le stade olympique de Rome ( dit stade olimpico ) est le plus grand stade de Rome avec ses soixante treize places assises .
la PalaLottomatica , le Palais des Sports de Rome est utilisé pour des concerts et des matchs de sports .
le Stade Flaminio , utilisé pour des rencontres de rugby .