l' iguane marin , ou iguane marin des Galápagos ( nom scientifique : Amblyrynchus cristatus ) est un iguane , qui vit sur les côtes des îles Galápagos .
c' est le seul lézard marin au monde .
l' iguane marin a un corps allongé , avec une longue queue , et une grosse tête , couverte d' écailles dures formant une sorte de casque .
il a aussi des épines qui forment une crête tout le long de son dos et de sa queue .
en dix huit cent trente cinq , quand Charles Darwin a débarqué aux îles Galápagos , il a décrit ainsi l' iguane marin : les rochers de lave noire sur la plage sont fréquentés de grands ( soixante à quatre vingt dix centimètres ) lézards , patauds et répugnants .
ils sont aussi noirs que les roches poreuses sur lesquelles ils rampent , et cherchent leurs proies dans la mer certains les appellent " lutins des ténèbres " un
en fait , les iguanes marins ne sont noirs que quand ils sortent de l' eau .
ils vont alors sur les rochers , pour se réchauffer au soleil , et leur peau devient rouge .
quand ils se sont bien réchauffés , ils retournent à la mer , pour se nourrir .
Darwin s' est en fait trompé : ces animaux ne sont pas carnivores , et s' ils vont en mer , c' est pour y trouver des algues dont ils se nourrissent , et qu' ils broutent sous l' eau .
d' ailleurs , il sont loin d' être " patauds " , et nagent même très bien .
quand ils nagent , ils gardent la tête hors de l' eau , et ne plongent que pour se nourrir .
l' iguane marin ne se nourrit que de certaines espèces d' algues , qu' il va chercher en mer , et broute sur les rochers , sous l' eau .
à marée basse , les iguanes marins peuvent également brouter les algues qui sont sur les rochers hors de l' eau , ce qui est beaucoup moins fatigant que de nager .
son intestin contient des bactéries qui vivent en symbiose avec l' iguane , et l' aident à digérer les algues dont il se nourrit .
les iguanes se nourrissent notamment d' algues vertes , comme les ulves qui poussent près de la surface de l' eau , mais ils préfèrent cependant les algues rouges , plus nutritives , mais qui poussent un peu plus profondément .
seuls les grands mâles plongent très profondément pour se nourrir , les autres iguanes restent plus près de la surface , ou bien attendent la marée basse .
lors des plus grandes marées , le niveau de la mer est si haut que les algues rouges se retrouvent trop profondément sous l' eau pour la plupart des iguanes , et ils sont obligés de manger des ulves , qui poussent plus près de la surface deux .
lorsqu' il fait plus chaud , l' été , l' eau se réchauffe , et les algues rouges sont moins nombreuses .
les iguanes sont alors obligés de manger plus d' ulves .
certaines années , à cause de El Ninyo , l' eau se réchauffe beaucoup , et même les algues vertes disparaissent .
elles sont remplacées par des algues brunes , qui sont beaucoup moins nourrissantes pour les iguanes .
sur certaines îles , les iguanes se nourrissent également d' une plante terrestre qui pousse sur la plage , la Batis maritima .
cette plante est beaucoup moins nourrissante que les algues , mais comme on en trouve tout le temps , quelle que soit la saison , les iguanes qui en mangent sont assurés de toujours trouver de quoi se nourrir trois .
les iguanes qui vivent sur ces îles et qui consomment ces plantes supportent donc mieux que les autres les années où El Ninyo réchauffe le climat .
l' iguane marin nage parfaitement bien , il est capable également de plonger , jusqu'à vingt mètres de profondeur , et de rester environ un H sous l' eau .
afin d' économiser du dioxygène , la fréquence cardiaque de l' iguane marin ralentit beaucoup lorsqu' il plonge .
son coeur peut même s' arrêter de battre pendant trois minutes !
une autre particularité de l' iguane marin , c' est que comme il ne se nourrit que d' algues marines , qui contiennent beaucoup plus de sel que les plantes terrestres , il mange en quelque sorte " trop salé " , ce qui pourrait être dangereux pour sa santé , s' il ne trouvait pas un moyen de l' évacuer de son corps .
en effet , comme d' autres animaux marins , comme les tortues marines , par exemple , les iguanes marins ont une glande à sel , un organe spécial qui leur permet d' éliminer le sel en trop dans leur organisme .
celle de l' iguane marin se trouve dans son nez , ce qui lui permet d' évacuer le sel par les narines , tout simplement en éternuant !
ce mode de vie est très fatiguant , et le corps de l' iguane se refroidit très vite dans l' eau .
en effet , la température de son corps varie avec celle de son milieu , et l' eau est beaucoup plus froide que l' air .
quand il a suffisamment mangé , l' iguane doit alors vite retourner à terre , pour se réchauffer .
il est alors épuisé , et il se déplace nettement moins vite quand il arrive sur les rochers .
c' est probablement cette scène à laquelle Darwin a assisté , il y a près de deux cents ans , et qui lui a donné à penser que ces animaux étaient " patauds " .
quand la température de son corps atteint trente cinq , cinq degrees Celsius , l' iguane marin change de position , afin d' éviter de trop chauffer .
s' il a vraiment trop chaud , il plonge dans la mer pour se rafraichir .
le soir , les iguanes se réchauffent avec la chaleur du soleil couchant , puis se blottissent les uns contre les autres pour se protéger du froid durant la nuit .
l' iguane marin atteint sa maturité sexuelle à douze ans .
la reproduction a lieu entre décembre et avril , ce qui correspond à la saison chaude , dans l' hémisphère sud ( où se situent les îles Galápagos ) .
les femelles ne se reproduisent qu' un an sur deux .
la femelle creuse un terrier dans le sable , pas trop près du bord de mer ( elles peuvent aller pour cela jusque deux kilomètres à l' intérieur des côtes ) , de cinquante à quatre vingts centimètres de profondeur .
elle y dépose entre trois et cinq oeufs , avant de reboucher le nid .
elle reste à proximité pour surveiller le nid , et s' assurer que les oeufs ne soient pas déterrés par un prédateur , ou par une autre femelle en train de creuser son nid à elle .
les bébés iguanes naissent trois mois après , et creusent vers la surface pour quitter le nid .
ils mesurent alors seulement vingt centimètres , et sont des proies faciles pour de nombreux animaux
l' iguane marin est la proie d' autres animaux , comme la buse des Galápagos , qui s' attaque aussi bien aux adultes qu' aux bébés .
les bébés iguanes marins sont aussi victimes d' autres prédateurs , comme le serpent Alsophis biseralis .
un petit crabe , le sally pied léger ( Grapsus grapsus ) vit en symbiose au côté des iguanes marins .
il monte sur le dos des iguanes et leur mange les peaux mortes , et les tiques .
les iguanes les laissent faire , car cela leur permet de se faire nettoyer facilement .
certains oiseaux , comme les pinsons de Darwin , ou les moqueurs se nourrissent également parfois sur le dos des iguanes .
il existe quatre espèces de moqueurs , qui vivent sur les différentes îles des Galápagos , comme le moqueur des Galápagos , ou le moqueur d' Espagnola .
les moqueurs sont des oiseaux omnivores , qui se nourrissent parfois également des tiques sur le dos des iguanes .
cependant , contrairement aux crabes , les moqueurs ne vivent pas toujours en symbiose avec les iguanes : quand la nourriture manque , s' ils ne trouvent pas de tiques sur le dos des iguanes , ils n' hésitent pas à les piquer avec leur bec , pour les blesser , et ensuite boire leur sang , comme de véritables parasites !
l' iguane marin est un proche cousin des iguanes terrestres des Galápagos : l' iguane terrestre des Galápagos , l' iguane terrestre de Santa Fe , et l' iguane rose des Galápagos .
ils sont apparus dans l' archipel des Galápagos , où leurs ancêtres sont arrivés il y a plusieurs millions d' années .
les analyses génétiques montrent que les ancêtres des iguanes des Galápagos sont arrivés sur l' archipel il y a environ cinq , sept millions d' années , et c' étaient des iguanes terrestres .
ils sont probablement arrivés là sur des troncs d' arbres ou des branchages flottants , qui ont dérivé jusqu' aux Galápagos au cours de tempêtes .
si la plupart des iguanes des Galápagos sont restés terrestres , les ancêtres de l' iguane marin ont commencé à se nourrir d' algues poussant sur les côtes , peut être à marée basse comme le font encore les iguanes marins actuels .
petit à petit , grâce à la sélection naturelle , ils ont accumulé des adaptations à la vie en milieu marin , jusqu'à arriver à leur apparence actuelle .
comme la plupart des espèces endémiques des Galápagos , l' iguane marin est une espèce vulnérable et fragile .
son habitat est limité , et s' il lui arrivait quelque chose , cela risquerait d' être très grave pour l' iguane marin .
lorsque le phénomène El Ninyo réchauffe la mer aux Galápagos , les algues disparaissent , et les iguanes marins , qui s' en nourrissent , deviennent beaucoup plus rares ( jusque quatre vingt cinq pour cents des iguanes disparaissent ) .
en vingt cent un , le naufrage du pétrolier Jessica a provoqué une marée noire , et entrainé la disparition de quatre vingt cinq pour cents des iguanes marins à Santa Fe , l' une des îles de l' archipel
pour toutes ces raisons , l' iguane marin est une espèce protégée .
il est classé comme " vulnérable " sur la liste rouge de l' UICN , et inscrit sur l' annexe II de la convention de Washington