la chouette des cactus , ou chevêchette des saguaros ( nom scientifique : Micrathene whitneyi ) , est la plus petite espèce de chouette au monde , plus légère encore que ses cousines , la chevêchette nimbée et la chevêchette du Tamaulipas .
elle vit dans les régions arides d' Amérique du Nord , et niche dans les cactus , ce qui lui a valu son nom , le saguaro étant son cactus préféré .
on écrit saguaro , mais on prononce " sah wah roh " : c' est un mot d' origine amérindienne .
aux États Unis , cette chouette est aussi appelée chevêchette elfe , du fait de sa petite taille .
la chevêchette des saguaros a le corps tacheté de gris et de roux , avec deux rangées de taches blanches sur les ailes .
elle n' a pas d' aigrettes , ce qui fait qu' on la qualifie de " chouette " plutôt que de " hibou " .
ses yeux sont jaunes , et elle a deux gros sourcils blancs , qui lui donnent un air sérieux .
une chevêchette des saguaros adulte pèse entre trente cinq et cinquante cinq grammes ( quarante grammes en moyenne ) , ce qui en fait la plus petite chouette du monde .
les femelles sont légèrement plus grosses que les mâles .
ses rémiges sont très longues , elles dépassent presque de sa queue .
la chevêchette a également de longues pattes , qui lui permettent de se poser sur les cactus sans se piquer .
elle vit entre le sud des États Unis ( notamment dans les états de Californie , Arizona , Nouveau Mexique et Texas , durant le printemps et l' été .
c' est une espèce migratrice : l' hiver , elle s' envole vers le Mexique , où il fait plus chaud .
elle reviendra au printemps pour se reproduire .
la chevêchette des saguaros vit dans les grands cactus cierge , comme les saguaros , et les autres arbres des régions arides .
les cactus fournissent de l' ombre , et de la fraicheur , ce qui permet de mieux supporter le climat aride .
elle ne creuse pas elle même son trou : elle choisit un ancien trou de pic des saguaros , ou d' un autre oiseau , abandonné , et orienté au nord ( pour qu' il y fasse plus frais ) pour y faire son nid et y pondre ses trois oeufs blancs , qui éclosent au début de l' été .
la chevêchette chasse surtout au crépuscule et juste avant l' aube .
elle se nourrit principalement de différentes sortes d' insectes : durant la saison sèche , elle mange surtout des grillons , et en été , durant la saison des pluies , de scarabées .
elle capture ses proies , notamment , dans les fleurs de cactus , elle peut aussi attraper des papillons en plein volume Parfois , elle mange aussi d' autres petits animaux , comme de petits lézards , des serpents aveugles , des rats kangourous , et des scorpions , dont elle coupe le dard venimeux avant de les manger .
parfois , la chevêchette cache des proies dans un trou d' arbre ou de cactus , afin de pouvoir revenir les manger plus tard .
la chevêchette rapporte parfois à ses petits des serpents aveugles , mais certains s' échappent , et survivent dans le nid : ils ne sont alors pas vraiment perdus , car ils vont se nourrir des fourmis et des asticots qui mangent la nourriture des poussins : de cette façon , les serpents aveugles qui vivent dans les nids sont très utiles aux chevêchettes .
la chevêchette des saguaros est elle même la proie d' autres animaux , comme d' autres rapaces plus grands ( le grand duc d' Amérique et l' épervier de Cooper , notamment ) , des serpents , et des mammifères comme le bassari rusé .
quand elles sont attaquées par un prédateur , les chevêchettes appellent leurs voisines vivant dans les trous à proximité , et se rassemblent par groupe pouvant aller jusqu'à cinq chouettes , pour attaquer le prédateur à la tête afin de le faire fuir .
dans Les Gardiens de Ga' Hoole , Gylfie un est une jeune femelle chevêchette elfe , et la meilleure amie du héros , Soren .
évadée , comme lui , de la Pension saint Aegolius , elle fera partie de la Petite Bande de Soren , dont elle deviendra la navigatrice .
elle apparait dans la plupart des livres de la série , ainsi que dans le film .
dans le dessin animé Rango , qui se passe dans le désert de Sonora , le générique , ainsi que plusieurs musiques du film , sont interprétées par un trio de petits maracachi chevêchettes des saguaros .
plusieurs autres personnages du film sont des chevêchettes des saguaros .