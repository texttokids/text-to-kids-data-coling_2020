le Portugal , officiellement République portugaise , en portugais República Portuguesa est un État situé au sud ouest de l' Europe et membre de l' Union européenne .
c' est le pays le plus à l' ouest de la péninsule Ibérique ainsi que du continent européen .
en tout , il possède une superficie de quatre vingt douze trois cent quatre vingt onze kilomètres carrés .
il est délimité au nord et à l' est par l' Espagne et au sud et à l' ouest par l' océan Atlantique .
le pays comprend des régions autonomes qui sont les archipels des Açores et de Madère .
enfin , le pays possède un climat océanique au Nord et un climat méditerranéen au Sud .
aux XVe et XVIe siècles , le Portugal établit des colonies en Amérique , en Afrique et même en Asie .
cela lui a permis de développer une économie mondialisée , une grande culture et un empire colonial durable .
depuis mille deux cent cinquante cinq , sa capitale est Lisbonne .
de mille huit cent huit à mille huit cent vingt et un , durant l' époque des guerres napoléoniennes , la capitale du Portugal a été transférée à Rio de Janeiro ( au Brésil ) .
le Portugal , politiquement stable depuis dix neuf cent soixante quatorze , fait partie des pays développés et possède un indice de développement humain très élevé .
l' histoire du Portugal est partagée avec celle de la péninsule Ibérique .
la région a été peuplée par des pré celtes et des celtes , elle est à l' origine de peuples comme les Gallaeci , les Lusitaniens et les Cinii .
elle a été visitée par les Phéniciens et les Carthaginois .
ce qui est maintenant le Portugal a été une partie de l' Empire romain qui fut ensuite envahie par les Suèves , les Búri et les Wisigoths , puis conquise par les Maures .
en huit cent soixante huit , pendant la Reconquista , s' est formé le Comté Portucalense .
bien avant que le Portugal ne soit indépendant , il y avait déjà eu quelques tentatives pour accéder à un statut plus autonome .
pour en finir avec ce climat indépendantiste de la noblesse locale concernant le domaine Leonês , le roi Alphonse le sixième de Castille a livré le gouvernement du Comté de la Galice un au Comte Raymond de Bourgogne .
après beaucoup d' échecs militaires de Raymond de Bourgogne contre les Maures , Alphonse le sixième décida , en dix cent quatre vingt seize , de donner au cousin de celui ci le gouvernement des terres plus au sud du Comté de la Galice , en établissant ainsi le Comté Portucalense .
avec le gouvernement du comte Henri de Bourgogne , le Comté Portucalense a connu non seulement une politique militaire plus efficace dans la lutte contre les Maures , mais aussi une politique indépendantiste plus active , bien qu' il n' ait jamais réussi à accéder à l' indépendance .
c' est seulement après son décès , quand son fils Alphonse Ier de Portugal est arrivé au pouvoir , que le Portugal est devenu indépendant , avec la signature en onze cent quarante trois du traité de Zamora .
alphonse Ier de Portugal a alors conquis d' importantes localités , comme Santarém , Lisbonne , Palmela et Évora , qui appartenaient aux Maures .
la Reconquista dans le territoire portugais prend fin en douze cent quarante neuf .
l' indépendance du royaume portugais est alors menacée à plusieurs reprises par le royaume de Castille
une première fois dans la séquence de la crise de succession de D Fernando un de Portugal , crise qui a culminé dans la bataille d' Aljubarrota , en treize cent quatre vingt cinq où Joao d' Aviz bat le roi de Castille Jean Ier .
le Portugal obtient son indépendance .
avec la fin de la guerre , le Portugal a engendré un processus d' exploration et d' expansion rendue célèbre par des découvertes , dont les principaux acteurs sont l' infant D Henri le Navigateur ( dit le Navigateur ) et le roi D Jean le deuxième de Portugal .
Ceuta a été conquis en quatorze cent quinze .
le cap Bojador a été franchi par Gil Eanes en quatorze cent trente quatre et l' exploration de la côte africaine a continué jusqu'à ce que Bartolomeu Dias vérifie , en quatorze cent quatre vingt huit , la communication entre les océans Atlantique et indien en traversant le cap de Bonne Espérance .
on a successivement et rapidement découvert des itinéraires et des territoires en Amérique du Nord et en Amérique du Sud , pour la plupart pendant le règne de D Manuel un .
Cela s' est caractérisé par une expansion dans l' est du continent , surtout grâce aux conquêtes d' Alphonse d' Albuquerque qui , pendant la première moitié du XVIe siècle , ont concentré presque tous les efforts du Portugal , bien que la colonisation du Brésil ait déjà été initiée en mille cinq cent trente par D João trois .
cette période constitue le siècle d' or du pays .
néanmoins , on compte quelques évènements tragiques : dans la bataille d' Alcácer Quibir en quinze cent soixante dix huit , le jeune roi D Sébastien et une partie de la noblesse portugaise mourirent .
Henri Ier de Portugal monta sur le trône , mais mourut deux ans plus tard , cette mort engendra la crise de succession de quinze cent quatre vingts , qui s' acheva avec le couronnement de Philippe le deuxième d' Espagne , premier de trois rois espagnols .
cette dynastie philippine a pris fin le premier décembre seize cent quarante : la noblesse nationale a fait un coup d' État .
D João IV fut nommé roi du Portugal .
le premier novembre dix sept cent cinquante cinq , jour de la Toussaint , le Portugal , et en particulier la ville de Lisbonne , furent frappés par un séisme qui creusa des fissures de cinq mètres de profondeur , puis par un raz de marée , avec des vagues de cinq à dix mètres de haut ( l' une d' entre elles atteignit quinze mètres ) deux .
Lisbonne ne fut pas la seule victime , il y eut également l' Algarve , toute l' Europe deux ressentit les secousses , ainsi que l' Afrique du Nord , la Martinique et Barbade ( Amérique du Sud ) .
Lisbonne est comme rayée de la carte avec plus de quatre vingt cinq pour cents de la ville détruite deux , y compris les plus célèbres de ses palais et bibliothèques , même les bâtiments d' architecture manuéline du XVIe siècle .
plusieurs bâtiments relativement épargnés par le séisme lui même furent détruits par les incendies qui s' ensuivirent , ainsi , le Palais royal est détruit .
les soixante dix volumes de la bibliothèque royale ont été perdus , comme toutes les oeuvres d' art et le compte rendu détaillé des grandes explorations réalisées par Vasco de Gama et d' autres navigateurs .
quelques quartiers de Lisbonne furent épargnés , comme Belém ou encore l' Alfama .
une dictature est mise en place dans les années trois mille trente quatre et est dirigée de longues années par Salazar .
en dix neuf cent soixante quatorze , Marcelo Caetano est au pouvoir et gouverne le pays d' une main de fer , aidé par la police secrète et la police militaire .
les colonies portugaises sont en révolte depuis le début des années soixante .
empêtré dans des guerres en Guinée Bissau , en Angola et au Mozambique , le gouvernement fasciste en place , écrase le peuple portugais avec des taxes destinées au maintien des troupes armées dans ses colonies , au détriment d' une modernisation du pays .
en outre , beaucoup de jeunes ouvriers portugais , menacés par un service militaire d' une durée de quatre ans , préfèrent fuir le pays .
la répression policière ne parvient pas à éradiquer la lutte ouvrière qui , bien qu' inutile , dure depuis quinze ans .
le pays était figé socialement et économiquement .
la population et une partie de l' armée sont lasses et espèrent un changement de régime .
dès dix neuf cent soixante treize , des officiers d' extrême gauche conspirent contre le pouvoir en place .
le vingt cinq avril dix neuf cent soixante quatorze, à minuit et vingt cinq minutes , la radio nationale diffuse une chanson révolutionnaire de Zeca Alfonso cinq .
c' est le signal de l' insurrection .
le Mouvement des Forces Armées s' empare des points stratégiques militaires et politiques et forcent Caetano à la capitulation .
seule la police politique oppose une résistance armée qui entraine la mort de six personnes .
il est à noter que ce furent les seules victimes de la révolution .
des milliers de portugais descendirent dans les rues , au mépris des consignes militaires .
à Lisbonne , le point de rassemblement était le marché aux fleurs .
certains militaires mettent un oeillet dans le canon de leur fusil , d' où le nom donné plus tard à cette révolution .
le mouvement de contestation populaire qui suivit la prise du pouvoir par les putschistes n' avait été ni prévu , ni souhaité .
les militaires se retrouvent vite dépassés par la suite de cette révolution .
dans le temps des conquêtes et des découvertes , le pays fut l' un des plus riches pays du monde , grâce à l' or importé du Brésil , les épices d' Inde et d' autres produits venus d' Afrique .
depuis dix neuf cent quatre vingt cinq , le pays est entré dans un processus de modernisation dans un environnement suffisamment stable et il rejoint l' Union européenne en dix neuf cent quatre vingt six .
le Portugal a développé une économie de plus en plus fondée sur les services et a été l' un des onze membres fondateurs de la monnaie européenne en dix neuf cent quatre vingt dix neuf .
le pays a commencé à faire circuler l' euro depuis le premier janvier vingt cent deux, avant l' euro , le Portugal utilisait l' escudos portugais .
la croissance économique portugaise a été au dessus de la moyenne de l' Union européenne pendant la majeure partie de la décennie de dix neuf cent quatre vingt dix .
depuis la crise économique des années vingt cent zéro , il est l' un des plus pauvres des douze pays de la CEE , au niveau de l' Espagne , de la Grèce ou encore de l' Islande .
malgré un passé majoritairement agricole , la structure de l' économie se base aujourd'hui sur les services et l' industrie , qui représente respectivement soixante sept , huit pour cents et vingt huit , deux pour cents de l' économie nationale .
l' agriculture est bien adaptée au pays en raison du climat , du relief et des sols favorables .
pendant les dernières décennies , la modernisation de l' agriculture s' est intensifiée , bien qu' environ douze pour cents de la population active travaille dans l' agriculture .
les oliviers ( quatre kilomètre carré ) , les vignobles ( trois sept cent cinquante kilomètres carrés ) , le blé ( trois kilomètre carré ) et le maïs ( deux six cent quatre vingts kilomètres carrés ) sont produits dans des secteurs suffisamment vastes .
les vins ( spécialement le vin de Porto et le vin de Madère ) et les huiles ( huile d' olive , huile de tournesol ) portugaises sont très appréciées en raison de leur qualité .
le Portugal est également producteur de fruits , notamment les laranjas algarvias ( orange de l' Algarve ) , la pêra rocha de l' Ouest , la cerise du Gardunha et la banane de Madeira .
d' autres productions font partie de l' horticulture ou de la floriculture , comme la betterave rouge sucrée , l' huile de tournesol et le tabac .
le liège a une production significative , car le Portugal produit cinquante quatre pour cents de la production mondiale de liège .
les ressources minérales plus importantes au Portugal sont le cuivre , le lithium , le tungstène , l' uranium , le sel et le marbre .
les forêts couvrent environ trente quatre pour cents du pays et sont notamment composées de :
en deux mille cinq , le taux de chômage était de sept , six pour cents , mais il a subi jusqu'à atteindre dix , deux pour cents en deux mille seize .
le pays exporte du textile , des voitures , des produits manufacturés , de l' informatique , de l' électronique et du matériel de construction .
le commerce extérieur du Portugal se concentre essentiellement dans l' Union européenne .
aujourd'hui , quatre vingts pour cents des exportations portugaises sont à destination des pays de l' Union européenne , cinq pour cents pour l' Amérique du Nord , les pays lusophones ( dont la langue est le portugais , comme le Brésil ) représentent quatre pour cents des exportations .
le Portugal est un pays qui est bordé à l' ouest et au sud par l' océan Atlantique , au nord et à l' est par son seul voisin , l' Espagne .
le territoire portugais inclut également les régions autonomes de Madère des Açores , Madère est située dans le continent africain et les Açores se localisent dans l' océan Atlantique .
le pays est situé à l' extrémité de l' Europe occidentale , le territoire portugais est de forme rectangulaire large de cent soixante kilomètres en moyenne et long de cinq cent soixante kilomètres .
avec une superficie de quatre vingt huit sept cent quatre vingt dix kilomètres carrés ou ( quatre vingt onze huit cent trente et un kilomètres carrés en ajoutant les archipels ) , c' est un pays qui est six fois plus petit que la France .
l' estuaire du Tage est nommé la mer de Paille , elle est située à Lisbonne .
au sud du Tage , les trois quarts des terres ont une altitude qui est inférieure à deux cents mètres , les plaines sont vastes et peu ondulées , elles continuent jusqu'à l' océan Atlantique et les surfaces d' érosion de la Meseta andalouse .
au contraire , au nord du Tage , il y a des sommets peu élevés ( comme la serra da Estrela , la seconde plus haute chaîne de montagnes du pays , qui atteint à peine deux mètre ) mais le relief est plus montagneux , puisque le tiers seulement se trouve à moins de deux cents mètres , entaillé de profondes vallées , ce relief a toujours rendu les déplacements difficiles .
il est utilisé pour la culture du raisin six .
Lisbonne est la capitale du Portugal depuis le XIIe siècle , la plus grande ville du pays , le principal pôle économique ( avec le principal port maritime et l' aéroport portugais ) et est la ville la plus riche du Portugal avec un PIB par habitant supérieur à celui de la moyenne de l' Union européenne .
il y a d' autres villes importantes comme celle de Porto , seconde plus grande ville et le port maritime , Aveiro ( considérée comme la Venise portugaise ) , Braga ( ville des archevêques ) , Chaves ( ville historique et millénaire ) , Coimbra ( avec la plus ancienne université du pays et une des plus anciennes d' Europe ) , Guimarães ( ville berceau du pays ) , Évora ( ville musée ) , Setúbal et Viseu .
dans le secteur métropolitain de Lisbonne existent des villes avec une grande densité de population comme Agualva Cacém et Queluz ( commune de Sintra ) , Amadora , Almada , Amora , Seixal , Barreiro , Montijo et Odivelas .
dans le secteur métropolitain de Porto , les communes les plus peuplées sont Vila Nova de Gaia , Maya , Matosinhos et Gondomar .
dans la région indépendante de Madère , la principale ville est Funchal .
dans la région indépendante des Açores existent trois villes principales : ponta Delgada ( dans l' île de São Miguel ) , Angra do Heroísmo ( dans l' île Terceira ) et Horta ( dans l' île du Faial ) .
le pays compte dix huit districts et deux régions autonomes , chaque district porte le nom de sa capitale .
depuis mille neuf cent soixante seize , les régions autonomes ne sont plus des districts .
le Portugal possède un territoire en Afrique , c' est l' archipel de Madère qui , depuis dix neuf cent soixante seize , n' est plus un district , mais une région autonome , pour éviter l' indépendance de Madère ( même chose pour Açores ) .
la majorité des fleuves portugais naissent en Espagne et se jettent dans l' océan Atlantique , à l' exception d' Ave , Mondego , Vouga , Zêzere et de Sado qui prennent leur source au Portugal .
le fleuve Tâmega se jette dans le Douro et le fleuve Zêzere dans le Tage .
les fleuves , dans leur grande partie , ne permettent pas la navigation des bateaux .
seuls le Douro , le Tage et une partie du Guadiana sont navigables .
il est ainsi possible de faire des croisières au Douro jusqu' en Espagne .
grâce aux barrages hydro électriques , les fleuves portugais sont d' importantes sources d' énergie hydraulique , une énergie renouvelable distribuée sous forme d' électricité .
en vingt cent un , le pays comptait dix trois cent cinquante six cent dix sept habitants sept .
l' origine de la population portugaise vient notamment des Celtes , des Ibères et des Lusitaniens .
il y a encore moins de cinquante ans , surtout entre les années mille neuf cent soixante et mille neuf cent soixante quatorze , le Portugal fut un pays d' émigration ( les habitants quittent leurs pays pour un autre , par exemple : canada , France , ...
) sept .
aujourd'hui le Portugal est un pays d' immigration , il accueille des africains ( une grande partie des anciennes colonies portugaises ) , des Brésiliens et des Européens de l' Est huit .
le Portugal est un pays qui vieillit , comme l' Italie .
il n' y a pas assez de naissances pour renouveler la population .
grâce à l' immigration , le Portugal renouvelle sa population de Brésiliens , ukrainiens et d' autres nationalités neuf .
en deux mille dix , la population portugaise était composée de seize , neuf pour cents d' enfants de zéro à quatorze ans , de soixante sept , trois pour cents de quinze soixante quatre ans et de quinze , huit pour cents de plus de soixante cinq ans huit .
l' espérance moyenne de vie était de soixante seize , quatorze ans .
quatre vingt sept , quatre pour cents des Portugais savent lire et écrire , avec taux d' analphabétisme décroissant au cours du temps .
il est l' un des pays dont la mortalité infantile est la plus faible : cinq , quatre pour mille huit .
parmi les écrivains et les poètes portugais , on peut citer Luis de Camões , Fernando Pessoa , José Saramago ( Prix Nobel de littérature en dix neuf cent quatre vingt dix huit ) , Ruben A et bien d' autres encore .
le Portugal possède des musiques que l' on ne trouve nulle part ailleurs , comme le fado et d' autres musiques du folklore portugais .
le fado est une musique nostalgique dont la " reine " était Amália Rodrigues .
la musique folklorique se joue avec divers instruments comme l' accordéon , les castagnettes , les mains , les pieds , la voix , et caetera , et s' accompagne de danses et de costumes .
durant l' immigration , Linda de Suza ( de vrai nom Teolinda Lança ) quitte le Portugal pour venir s' installer en France avec son bébé , elle fut une grande chanteuse franco portugaise dans les années mille neuf cent quatre vingts à mille neuf cent quatre vingt dix , elle chanta Le Portugais , qui fut un grand succès auprès de toute la communauté portugaise en France .
le rock est de plus en plus écouté au Portugal .
Tony Carreira est aussi un grand chanteur en France , il a déjà vécu en France , mais il est retourné au Portugal .
Linda de Suza un dix , Amália Rodrigues et Tony Carreira un onze ont déjà chanté à l' Olympia ( Paris ) .
la cuisine portugaise est très riche et très variée , chaque région ayant ses spécialités .
la gastronomie portugaise est en grande partie faite de produits de la mer ( poissons , crabes , ...
) , la viande y est largement appréciée aussi .
il y a aussi de nombreux fromages comme le Queijo da Serra qui vient de la Serra da Estrela .
l' ail , l' huile d' olive et le vinaigre sont des ingrédients très souvent utilisés dans la conception des plats de viandes et de poissons .
Vasco de Gama et les autres explorateurs , qui ont ramené des épices et de nouveaux produits ( fruits , légumes , tabac , et caetera ) , ont révolutionné les spécialités culinaires du monde occidental , donc du Portugal .
parmi les spécialités portugaises de poissons figurent les crevettes et la morue .
on y trouve par exemple de la morue diversement accommodée , de la pieuvre , de la caldeirada et des echinoideas .
beaucoup de plats sont préparés avec de la viande comme la feijoada à Transmontana , la carne de porco à Alentejana , le poulet grillé et le cochon de lait .
les soupes sont très présentes au Portugal , avec beaucoup de recettes différentes .
parmi elles , il y a le gaspacho , la sopa da pedra , le traditionnel caldo verde , les papas de sarrabulho ou encore la canja de galinha .
les gâteaux et les pâtisseries sont nombreux et chaque région possède sa pâtisserie .
en voici quelques unes : le gâteau de miel , la torta de laranja ( tarte à l' orange ) , la tarte aux amandes , le bolo rei , le riz au lait , les pasteis de nata , les pasteis de feijão ou encore le pudim caseiro .
le Portugal est un pays touristique : chaque année , il accueille environ douze millions de touristes .
l' Espagne , qui représente quarante neuf pour cents des touristes qui visitent le Portugal , en est la principale source , suivie du Royaume Uni ( quatorze pour cents ) .
le pays possède des plages sur l' océan Atlantique , mais aussi deux grandes villes touristiques : Lisbonne et Porto .
les régions les plus visitées sont Lisbonne , l' Algarve , Porto , Madère et les Açores .
il y a d' autres régions visitées , comme la Serra da Estrela , Coimbra et la vallée du Douro .
le climat et la température de l' eau de la mer sont les principaux facteurs qui contribuent à la grande croissance du tourisme dans la région de l' Algarve .
Lisbonne attire aussi beaucoup de touristes par son histoire ou ses monuments .
elle a été capitale européenne de la culture en dix neuf cent quatre vingt quatorze , a accueilli l' exposition internationale de dix neuf cent quatre vingt dix huit ( Expo' quatre vingt dix huit ) et plusieurs jeux de l' Euro vingt cent quatre .
des points fortement touristiques sont le parc des nations , le château de São Jorge , la cathédrale Sé , le monastère des Hiéronymites et la tour de Belém .
la ville mariale de Fátima est aussi très fréquentée en raison du sanctuaire présent dans la ville , le sanctuaire de Notre Dame de Fátima .
le Portugal possède une certaine notoriété chez certains sports , comme au football où Cristiano Ronaldo est le plus célèbre .
son équipe nationale ( la Seleção portuguesa de futebol ) ne détient cependant aucune victoire en Coupe du monde , ceci malgré la qualité des joueurs portugais au fil des générations .
en effet , de grands joueurs ( Eusébio , Cristiano Ronaldo , Luís Figo , Deco .
) mènent toujours leur pays aux phases les plus proches d' un titre mondial ou continental , mais perdent toujours le match le plus important , notamment celui contre la Grèce en finale de l' Euro deux mille quatre .
finalement , le Portugal remporte l' Euro vingt cent seize , défaisant la France un zéro sur un but d' Éder .