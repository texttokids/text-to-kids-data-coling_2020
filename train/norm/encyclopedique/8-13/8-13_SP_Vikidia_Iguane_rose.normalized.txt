l' iguane rose des Galápagos , ou iguane terrestre rose des Galápagos , ou encore rosada en espagnol un ( nom scientifique : Conolophus marthae ) , est une espèce d' iguane terrestre de couleur ...
rose , et vivant , comme son nom l' indique , aux îles Galápagos , plus précisément sur une partie de l' île Isabella .
il n' a été découvert que très récemment .
il n' y a pas si longtemps , on ne connaissait que trois espèces d' iguanes aux Galápagos : l' iguane marin ( Amblyrynchus cristatus ) , et les deux espèces d' iguanes terrestres " jaune " : l' iguane terrestre des Galápagos ( Conolophus subcristatus ) et l' iguane terrestre de Santa Fe ( Conolophus pallidus ) .
l' iguane rose a été aperçu pour la première fois sur les pentes du volcan Wolf , au Nord de l' île d' Isabella , en dix neuf cent quatre vingt six .
jusque là , on ignorait s' il s' agissait d' une anomalie , ou bien d' un hybride entre plusieurs espèces d' iguanes déjà connues , et qui vivent au même endroit .
récemment , en vingt cent neuf , des scientifiques ont démontré qu' il s' agissait en fait d' une troisième espèce d' iguane terrestre , à laquelle ils ont donné le nom scientifique de Conolophus marthae .
l' iguane rose ressemble beaucoup à ses cousins , mais il se reconnait facilement par la couleur particulière de son corps , rose orné de bandes noires .
la tête et les pattes avant sont entièrement roses , des rayures noires couvrent l' arrière de son corps , et se rejoignent pour former un motif .
la queue est entièrement noire .
on ne sait pas très bien à quoi est due la coloration rose , mais ce phénomène peut probablement s' expliquer au moins en partie par la circulation du sang dans la peau et les écailles .
le dos de l' iguane rose , comme celui de ses cousins , est parcouru par une crête ornée de piquants .
en revanche , sur la nuque , l' iguane rose n' a pas de piquants , juste une sorte de bosse arrondie .
ce sont notamment des analyses génétiques qui ont permis de déterminer que l' iguane rose est bien une nouvelle espèce , et non pas une variété d' iguane terrestre , ou bien un hybride entre l' iguane terrestre et l' iguane marin .
ces analyses ont également permis de classer l' iguane rose parmi les autres animaux , s' il n' est pas un iguane terrestre , il en est cependant le proche cousin : l' iguane terrestre des Galápagos , l' iguane jaune des Galápagos , et l' iguane rose des Galápagos font tous les trois partie du genre Conolophus , dont ils sont les seuls représentants .
parmi les trois , l' iguane terrestre et l' iguane jaune sont plus proches entre eux que de l' iguane rose , qui est semble t il l' espèce qui pourrait être apparue en premier ...
autrement dit , les premiers iguanes à avoir peuplé les Galápagos pourraient avoir été ...
roses , et non pas jaunes !
les analyses génétiques révèlent que l' iguane rose est apparu sur les Galápagos il y a très longtemps , alors que la plupart des îles des Galápagos n' étaient même pas encore formées !
plus étonnant encore , le volcan sur lequel il vit est , au contraire , l' un des plus jeunes de tout l' archipel !
cela pourrait vouloir dire que peut être , l' iguane rose est apparu ailleurs dans l' archipel , et qu' il n' est venu sur ce volcan que plus tard .
il vivait peut être par le passé ailleurs dans l' archipel , sur une zone plus étendue , d' où il semble avoir disparu ...
ou peut être pas , d' ailleurs , car le fait que l' on ait pas encore trouvé d' iguane rose ailleurs dans les Galápagos ne veut pas dire qu' il n' y en ait pas ...