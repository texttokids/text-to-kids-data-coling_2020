saturne est une planète gazeuses et aussi Saturne est la sixième planète tournant autour du Soleil .
d' un diamètre neuf fois supérieur à celui de la Terre , Saturne est très connue pour ses anneaux spectaculaires composés de morceaux de glace .
c' est la plus lointaine planète visible à l' oeil nu , et la dernière connue jusqu'à ce qu' Uranus soit découverte au XIXe siècle .
en anglais , Saturne a donné son nom à Saturday , ce qui signifie samedi , le sixième jour de la semaine .
il s' agit d' une planète gazeuse , Saturne n' a par conséquent pas de surface solide : un astronaute ne pourrait donc pas y marcher .
le nom de Saturne vient du dieu romain du même nom .
pour les Grecs , Saturne s' appelle Cronos .
en raison de sa vitesse de rotation rapide ( dix heures ) , Saturne est légèrement aplatie au niveau des pôles .
son rayon équatorial est de soixante deux cent soixante huit kilomètres , alors que son rayon polaire est de cinquante quatre trois cent cinquante neuf kilomètres , soit près de six kilomètres de moins .
son volume est très important , sept cent soixante fois celui de la Terre mais sa masse seulement quatre vingt quinze fois .
la densité de Saturne est donc très faible , environ zéro , sept ( près de huit fois moins que celle de la Terre ) , la seule planète dont la densité soit inférieure à celle de l' eau .
son atmosphère est composée principalement d' hydrogène et d' hélium .
elle se divise en deux couches , la troposphère et la stratosphère : la stratosphère ressemble à celle de la Terre , sauf bien sûr qu' elle n' a pas la même composition .
la troposphère elle même contient trois couches :
la première couche est celle visible depuis l' espace .
ses nuages sont composés d' ammoniac , la température est de deux cent cinquante degrees Celsius et la pression atmosphérique se situe entre zéro , cinq et deux bars .
la seconde couche contient des nuages d' hydrosulfure d' ammonium , la température est de soixante dix degrees Celsius et la pression devient assez importante , de deux , cinq à neuf , cinq bars .
dans la troisième , la température peut atteindre zéro degree Celsius voire soixante dix degrees Celsius , et la pression est de dix à vingt bars .
l' atmosphère de Saturne est composée de quatre vingt treize pour cents d' hydrogène , cinq pour cents d' hélium , zéro , deux pour cents de méthane , zéro , un pour cent de vapeur d' eau , zéro , un pour cent d' ammoniac , zéro , cinq pour cents d' éthane et zéro , un pour cent d' hydrure de phosphore .
un
vue de loin , Saturne parait calme , sans signe particulier , à part ses anneaux .
en réalité , le climat de cette planète est très violent : il s' y produit beaucoup de phénomènes météorologiques .
tous les trente ans , une tempête géante appelée " grande tache blanche " apparait et fait le tour de l' équateur , avec des vents pouvant dépasser les un cinq cents kilomètres par heure .
cette tempête est précédemment apparue en dix neuf cent quatre vingt dix , la prochaine devrait se produire vers vingt cent vingt .
il arrive par moment que des tempêtes formant une tache blanche apparaissent , mais sans faire le tour de la planète .
c' est ce qui est arrivé en deux mille onze , avec une tempête qui a atteint dix sept kilomètre de long .
cette tempête , tout comme celle qui se produit tout les trente ans , ne ressemble pas du tout aux cyclones sur Terre : en effet , ils ne présentent aucun oeil .
au pôle Nord de Saturne , il existe un étrange phénomène .
il pourrait s' agir d' une tempête en forme d' hexagone .
cette tempête hexagonale a été observée la première fois par les sondes Voyager un et Voyager deux .
elle ressemble beaucoup plus à une tempête terrestre , puisqu' elle possède un oeil comme les cyclones terrestres .
la nature exacte de cette tempête n' est pas encore connue .
il pourrait aussi s' agir d' une aurore polaire de type inconnu .
cette tempête est énorme , son périmètre étant de treize kilomètre .
au pôle Sud , il existe également une tempête , mais elle n' est pas hexagonale comme celle du pôle Nord .
elle ressemble donc beaucoup plus à une tempête terrestre .
sur Saturne , on peut aussi observer la formation d' orages , mais encore une fois , ce ne sont pas des orages comme sur la Terre .
ils sont extrêmement violents , le plus long a même duré huit mois .
c' est l' orage le plus long jamais observé .
ces orages peuvent s' étendre sur trois kilomètre .
ils se produisent généralement dans une région de Saturne nommée " allée des tempêtes " .
les éclairs de Saturne sont également des milliers de fois plus fort que sur notre planète .
vus de loin , ses anneaux donnent à Saturne ce profil particulier où ils semblent formés d' une matière continue .
en réalité , ils sont constitués de blocs de glace et de roches d' une taille variant entre celle d' un grain de sable et celle d' une montagne .
ces blocs se déplacent très rapidement autour de la planète , et une sonde spatiale qui traverserait les anneaux serait probablement détruite .
en deux mille quatre , la sonde Cassini est passée à travers les anneaux , elle s' en est bien sortie , mais elle n' était pas totalement intacte .
les anneaux on des noms de lettre : de l' Anneau À jusqu'à l' anneau F
les anneaux de Saturne s' étendent sur plus de cinq cents kilomètre de large , mais sur moins d' un kilomètre d' épaisseur .
l' anneau de Saturne est le plus grand anneau d' une planète dans le Système Solaire .
chaque planète a une limite sphérique que l' on appelle " limite de Roche " .
si un objet d' une taille plus ou moins grosse franchit cette limite , il éclate et ses débris forment un anneau autour de la planète .
si , pour la Terre , la Lune quittait son orbite et franchissait cette limite sans s' écraser sur la Terre , elle se disloquerait et formerait un anneau autour de la Terre .
la Lune étant un grand satellite , presque aussi grand que Mercure , le nombre de débris permettrait la formation d' un grand anneau par rapport à la taille de la Terre .
voilà comment se sont formés les anneaux de toutes les planètes gazeuses .
par contre , si un nombre assez important de débris sortent de cette limite , ils sont capables de s' assembler pour former un corps céleste plus grand .
une des hypothèses sur la création de notre Lune serait qu' un gros astéroïde ait percuté la planète , ensuite , les débris de cet astéroïde et ceux de la Terre ayant subi l' impact , en sortant de cette limite , se seraient rassemblés pour donner naissance à la Lune .
les anneaux finiront probablement par disparaitre .
il se pourrait qu' un objet ( comète où astéroïde de grande taille ) traverse ses anneaux , sa force de gravitation les déformerait et les détruirait .
autre possibilité , sur plusieurs millions d' années , les anneaux de Saturne se rapprochant de la planète , ils pourraient finir par s' écraser sur elle .
par contre , l' un de ses anneaux est en permanence alimenté par la matière éjectée par les geysers de l' une de ses lunes , Encelade .
la structure interne de Saturne est surprenante , elle possède quatre couches :
en dessous de son atmosphère se trouve un manteau gazeux d' hydrogène et d' hélium , qui représente la plus grande partie du volume de Saturne ,
en dessous , il y a une forme étrange d' hydrogène , puisqu' il est liquide ,
encore plus bas , il y a de l' hydrogène métallique , une forme encore plus étrange de ce gaz , celui ci est également liquide , mais aussi sous forme de métal .
il génère le champ magnétique de la planète ,
le noyau , lui , est rocheux , mais sa température atteint les douze degree Celsius .
le noyau de Saturne est si chaud qu' elle génère sa propre chaleur , en fait quatre vingts pour cents plus d' énergie qu' elle n' en reçoit du soleil .
saturne est connue depuis la préhistoire , car elle est visible à l' oeil nu .
pendant l' Antiquité , elle a été associée à plusieurs mythologies .
elle a été baptisée Saturne d' après le dieu romain des récoltes , qui est l' équivalent de Cronos pour les Grecs .
les Babyloniens , eux , ont observé le mouvement de Saturne .
le savant grec Ptolémée est parvenu à calculer son orbite .
la première observation de Saturne a été faite par l' astronome italien Galilée en seize cent dix .
en voyant les anneaux , Galilée ne comprend pas ce qu' il voit , il pense que Saturne a des oreilles .
en seize cent douze , l' astronome observe Saturne , mais les anneaux ont disparu , puis ils réapparaissent l' année suivante .
en réalité , la Terre était passée dans le plan des anneaux , et comme ils sont très fins , ils étaient alors invisibles depuis la Terre .
ce phénomène se produit régulièrement .
en mille six cent cinquante cinq , le néerlandais Christian Huygens découvre un point brillant autour de Saturne : il s' agit de son plus grand satellite naturel , Titan .
l' année suivante , il constate que la planète possède un anneau : il pense qu' il est solide , et qu' on pourrait marcher dessus .
plus tard , en seize cent soixante quinze , l' astronome Jean Dominique Cassini observe que l' anneau de Saturne est en réalité composé de plusieurs anneaux concentriques .
près de deux cents ans plus tard , en dix huit cent cinquante neuf , l' astronome James Clerk Maxwell pense que les anneaux sont composés de particules , son hypothèse est confirmée en dix huit cent quatre vingt quinze par les observations de James Keeler .
durant le XXe siècle , plusieurs sondes spatiales ont survolé Saturne et ont permis de faire de nombreuses découvertes .
la première sonde à avoir atteint Saturne est Pioneer onze le premier septembre dix neuf cent soixante dix neuf. elle passe à vingt et un kilomètre de la planète , et prend quelques photos à basse résolution de Saturne et certains de ses satellites .
les photos ont permis de découvrir l' anneau F de Saturne , qui se situe à l' écart des autres anneaux , mais la qualité n' était pas suffisante pour avoir des détails de sa surface .
en novembre mille neuf cent quatre vingts, la sonde Voyager un est passée près de Saturne , à cent vingt quatre kilomètre .
la sonde a pu envoyer les premières images de bonne qualité de Saturne et a également permis la découverte d' une atmosphère sur son plus gros satellite , Titan .
la sonde Voyager deux a survolé Saturne en dix neuf cent quatre vingt un pour continuer les études .
malheureusement , la caméra est coincée pendant deux jours , ce qui a empêché la sonde de prendre certaines photographies correctement .
en vingt cent quatre , la sonde Cassini se met en orbite autour de Saturne .
elle emporte à bord l' atterrisseur Huygens , qui se pose sur Titan .
cette sonde a découvert de nouveaux anneaux , des lacs d' hydrocarbure sur Titan , et a fait un très grand nombre de photos de Saturne et de ses lunes .
elle a aussi pu observer des tempêtes comme celle de vingt cent onze .
à l' origine , la mission devait durer jusqu'à deux mille huit , mais elle a été prolongée plusieurs fois , et s' est terminée en le quinze septembre deux mille dix septen brulant dans l' atmosphère de Saturne .
saturne possède de nombreux satellites naturels .
on en connait soixante deux pour l' instant , dont trente quatre mesurent moins de dix kilomètres de diamètre .
quatorze autres mesurent entre dix et cinquante kilomètres de diamètre .
le plus grand satellite de Saturne , Titan , est aussi gros que la planète Mercure .
il est aussi le seul satellite du système solaire possédant une atmosphère , celle ci étant composée essentiellement d' azote et d' un peu de méthane .
il y fait si froid ( deux cents degrees Celsius ) que le méthane peut devenir liquide : il peut donc y avoir des pluies , et des lacs peuvent se former .
les autres satellites de Saturne ne possèdent pas d' atmosphère , mais ils restent très intéressants .
encelade possède des geysers crachant de la glace : cela signifie qu' il y a de l' eau liquide sous sa surface gelée , qui pourrait être réchauffée par un noyau brulant .
l' eau sortant des geysers gèle immédiatement à cause du froid .
il se peut qu' il y ait de la vie dans l' eau d' Encelade , mais si elle existe , ce seront des organismes unicellulaires , comme des microbes et des bactéries .
une autre de ses lunes , Mimas , est connue pour son cratère .
la cause est l' impact d' un astéroïde .
si celui ci avait été un peu plus grand , Mimas aurait pu être détruite et serait devenue un nouvel anneau autour de Saturne .
saturne est très connue dans la fiction en raison de ses anneaux qui ont inspiré les écrivains .
le cinquième mouvement de l' orchestre Les Planètes de Gustav Holst est " saturne , celui qui apporte la vieillesse " .
le livre Les anneaux de Saturne d' Isaac Asimov se passe sur les lunes de Saturne , notamment sur Titan .
il est sorti en mille neuf cent cinquante huit , à l' époque où l' on ne connaissait que neuf lunes autour de Saturne , et il s' agit du dernier livre publié par Asimov .
dans un autre de ses livres , The Martian Way , les colons , en train de coloniser Mars , utilisent un morceau de glace provenant des anneaux de Saturne pour le faire fondre et obtenir de l' eau .
Isaac Asimov a aussi créé des livres scientifiques vulgarisés , dont un qui parle de Saturne , Saturne et sa parure d' anneaux .
certains épisodes de Star Trek se passent autour de Saturne .
dans l' épisode Demain est hier , ils dirigent la première sonde spatiale ayant pour but d' atteindre Saturne .
cet épisode est sorti en dix neuf cent soixante sept .
depuis , des sondes spatiales ont déjà atteint Saturne .
dans Le premier devoir , l' histoire se déroule près de Saturne , avec un centre d' évacuation d' urgence sur sa lune Mimas .
dans le film Star Trek un , l' USS Entreprise se cache derrière Titan , le champ magnétique de Saturne lui servant de bouclier protecteur .