la première organisation des jeunes du Parti nazi est fondée le treize
mai mille neuf cent vingt deuxà Munich sous le nom de Jungsturm Adolf Hitler , plus
ou moins traduisible en français par " jeune brigade d' assaut Adolf
Hitler " .
elle est alors rattachée au Sturmabteilung et dirigée par
Gustav Adolf Lenk .
l' Organisation est interdite en mille neuf cent vingt trois , en même
elle est refondée le quatre juillet mille neuf cent vingt sixlors du deuxième
congrès du Parti nazi , sous le nom de Hitlerjugend .
la raison d' être des Jeunesses hitlériennes est la formation de futurs
surhommes " aryens " et de soldats prêts à servir loyalement le
troisième Reich .
il s' agit de contourner les clauses très
contraignantes du Traité de Versailles qui interdisent à
l' Allemagne de posséder une armée puissante et de préparer une
génération physiquement et mentalement apte à être , au plus tôt , lancée
dans une guerre contre toutes les puissances ennemies du Reich .
dans
les Jeunesses hitlériennes , l' entraînement physique et militaire
passait bien avant l' instruction scolaire et scientifique .
l' apprentissage comprenait le maniement des armes , le développement de
la force physique , la stratégie militaire et un endoctrinement
antisémite .
après la dissolution des organisations de scouts dans
tous les Laender d' Allemagne , les Jeunesses hitlériennes s' approprièrent
beaucoup de leurs activités , bien que les objectifs et le contenu ne
soient pas les mêmes .
la cruauté des plus grands envers les plus jeunes
encourageait à éliminer les plus faibles et à s' endurcir .
uniforme des Jeunesses hitlériennes dans les années dix neuf cent trente .
les membres des Jeunesses hitlériennes portaient des uniformes
comparables à ceux du Parti nazi et utilisaient un système de
grades militaires similaires aux grades et insignes des
Sturmabteilung .
beaucoup des activités proposées aux garçons
ressemblaient à un entrainement militaire : ramper sous des fils
barbelés , apprendre à plonger en mer depuis des sautoirs et apprendre
comment lancer des grenades factices .
ils avaient un poignard fabriqué
par la firme Zwilling J A Henckels , sur le plat de la lame duquel
figurait l' inscription " Blut und Ehre ! " , ( sang et honneur ) .
le
fixés par les directives de Von Schirach , les programmes à
inculquer à la jeunesse étaient précis , se voulant l' armature
idéologique de la nouvelle génération , et comportaient un certain
nombre de thèmes centrés autour du parti nazi , de Hitler , de
l' Allemagne et du peuple allemand , et des directives pour les chants à
garçons de la jeunesse hitlérienne s' exerçant au tir .
l' encadrement des Jeunesses hitlériennes était assuré par des adultes ,
souvent militants du parti nazi au sein d' un corps d' armée .
de plus ,
ces chefs d' escouades , à la pédagogie de sergent instructeur
exigent , au Fuehrerprinzip une obéissance totale .
le gros
des membres comprenait des garçons âgés de huit à dix huit ans .
dès
dix neuf cent trente six , les Jeunesses hitlériennes devinrent officiellement une
filière obligatoire pour tous les jeunes allemands .
le groupe servait
aussi de base de recrutement pour des groupes paramilitaires du parti
nazi : la Schutzstaffel deux noeuds s' y intéressait particulièrement .
les
membres des HJ étaient particulièrement fiers de se voir accorder la
sieg rune ( rune de la victoire ) , par les SS .
les SS utilisaient
deux sieg runes accolées comme emblème , et cette récompense liait
elles étaient organisées dans les villes et villages en cellules
locales .
ces groupes se réunissaient chaque semaine : un dirigeant
adulte y enseignait la doctrine nazie .
au niveau régional , les
responsables organisaient des rassemblements et des manoeuvres auxquels
plusieurs cellules participaient .
le groupe national se réunissait en
général une fois par an à Nuremberg , pour le rassemblement
les Jeunesses hitlériennes avaient également créé des " académies "
d' entraînement comparables aux lycées .
de telles académies étaient
considérées comme les bases de la relève du parti : seuls les élèves
les plus dévoués et les plus radicaux pouvaient prétendre devenir de
quelques sections visaient à entrainer ceux de ses membres à devenir
officier de la Wehrmacht .
de tels groupes s' appliquaient à former
le jeune disciple dans la spécialité qu' il espérait exercer en tant
qu' officier .
ainsi , les Jeunesses hitlériennes de la Marine étaient
la section la plus nombreuse et servaient d' auxiliaires à la
dès juillet mille neuf cent trente trois, Schirach organisa les Jeunesses hitlériennes au moyen
( Jeunes allemands ) , regroupaient les garçons de dix à quatorze
chargé de l' encadrement des filles de dix à quatorze ans ,
quatorze à dix huit ans , plus tard l' âge maximal des membres du
werk " Glaube und Schoenheit " rassembla les jeunes filles
les quatre principales branches des HJ ( Deutsches Jungvolk ,
Hitlerjugend , Jungmaedel , Bund Deutscher Maedel ) , quoique comportant
quelques variations minimes , étaient structurées de manière similaire .
la direction de la Jeunesse du Reich ( Reichsjugendfuehrung ( en ) )
avait organisé sur le territoire du Reich des directions régionales ,
représentant entre vingt et quarante deux Gebiete ( pour les DJ ou les HJ ) , et autant
d' Obergaue pour les JM et le BDM .
ces structures étaient elles mêmes
redécoupées de manière plus fine , jusqu'à des groupes d' une dizaine de
membres , par lieu de résidence , désignés sous les termes de
Jungenschaft deux noeuds , Kameradschaft ( HJ véritable ) , Jungmaedelschaft deux noeuds
Deutsches Jungvolk Hitlerjugend Jungmaedel Bund Deutscher Maedel
Jungstamm Stamm Jungmaedelring Maedelring
Faehnlein Gefolgschaft Jungmaedelgruppe Maedelgruppe
Jungzug Schar Jungmaedelschar Maedelschar
Jungenschaft Kameradschaft Jungmaedelschaft Maedelschaft
pour les niveaux Jungstamm Stamm ...
et inférieurs , chaque unité
coordonnait les membres de quatre unités du niveau hiérarchique
inférieur .
au niveau Bann Untergau , l' organisation se faisait de
manière géographique , chacune de ces unités présidant à quatre ou six
unités du niveau inférieur , tandis que chaque Gebiet ou Obergau
regroupait environ vingt Bannen ou Untergauen .
une unité du niveau
Jungstamm Stamm ...
regroupait en moyenne environ six cents membres , un Bann ou
en mille neuf cent trente quatre , le Reich était ainsi organisé en cinq Obergebiete et dix neuf
Gebiete ou Obergaue .
l' Anschluss de mille neuf cent trente huit provoqua la création d' un
sixième Obergebiet .
en mille neuf cent quarante deux , le nombre de Gebiete et d' Obergaue
culmina à quarante deux , dont quatre Befehlsstellen pour les pays
occupés ( Protectorat de Bohême Moravie , Gouvernement général ,
pays bas , ainsi qu' Osten pour l' Europe de l' Est et du Sud ) .
pour le
territoire du Reich , les limites entre Gebiete suivaient grosso modo
l' unité de base des Jeunesses hitlériennes était la Bann , l' équivalent
d' un régiment militaire .
on comptait plus de trois cents de ces Banne ,
dispersées dans toute l' Allemagne , chacune d' entre elles comptant
environ six jeunes .
chaque unité avait un drapeau avec un dessin
pratiquement identique , mais chaque Bann était identifiée par son nom ,
inscrit en noir sur un ruban jaune , au dessus de la tête de l' aigle .
ces drapeaux mesuraient deux cents centimètres de long et cent quarante cinq centimètres de haut .
l' aigle au
centre faisait référence à l' Empire allemand ( aigle prussien ) .
il
maintenait dans ses serres une épée blanche et un marteau noir .
ces
symboles furent utilisés sur le premier drapeau officiel présenté aux
jeunesses hitlériennes , au Congrès national du NSDAP , en août mille neuf cent vingt neuf, à
Nuremberg .
l' épée était censée représenter le nationalisme alors que le
marteau était le symbole du socialisme .
les mâts utilisés avec ces
drapeaux étaient en bambou , surmontés d' une boule en fer blanc et d' une
les drapeaux portés par les Gefolgschaft des HJ , l' équivalent d' une
compagnie de cent cinquante jeunes , montrait l' emblème utilisé par les groupes
armés des HJ : trois bandes horizontales ( rouge blanc rouge ) au centre
desquelles un carré blanc tenant sur un sommet et contenant une croix
gammée noire en son sein .
le drapeau des Gefolgschaften mesurait cent quatre vingts centimètres
de long par cent vingt centimètres de hauteur avec chaque bande de quarante centimètres .
pour
distinguer chaque Gefolgschaft et la branche des Jeunesses hitlériennes
coloré , en haut à gauche .
le bandeau était d' une couleur précise ,
propre à chaque unité .
par exemple , un bandeau bleu clair avec un
numéro d' unité en blanc et une couture blanche était réservé pour les
examen sanitaire dans un camp d' été , juillet dix neuf cent quarante. l' embrigadement vise
en dix neuf cent vingt trois , l' organisation comptait un millier de membres .
en dix neuf cent vingt cinq , le
nombre de membres s' élevait à cinq .
cinq ans plus tard , les Jeunesses
hitlériennes dépassait les vingt cinq sympathisants , et à l' arrivée des
nazis au pouvoir en dix neuf cent trente trois , elles comptaient un effectif de
deux deux cent cinquante membres .
cette augmentation étant due en grande partie aux
membres des autres organisations de jeunesse avec lesquelles les
jeunesses hitlériennes avaient fusionné ( avec plus ou moins de
consentement ) , incluant l' importante evangelische Jugend
en décembre dix neuf cent trente six, l' effectif dépassa les cinq millions de membres .
le même mois , l' organisation devint la seule organisation de jeunesse
autorisée dans laquelle toutes les autres devaient se fondre ( Gesetz
dix neuf cent trente neuf avec le Jugenddienstpflicht .
l' appartenance pouvait même être
proclamée contre l' avis des parents .
à partir de là , la plupart des
adolescents allemands furent incorporés dans les Jeunesses
hitlériennes : dès dix neuf cent quarante , l' organisation avait atteint un effectif
de huit millions de membres .
plus tard , les statistiques de guerre sont
difficiles à lire , dès le moment où l' on considère que la conscription
obligatoire et l' appel à la lutte ( chez des enfants à partir de dix ans )
signifie que pratiquement tous les jeunes allemands étaient , dans une
certaine mesure , reliés aux Jeunesses hitlériennes .
le gros de la " génération des Hitlerjugend " était né entre les
années dix neuf cent vingt et dix neuf cent trente .
ils formèrent la génération adulte de
l' après guerre et des années mille neuf cent soixante dix et mille neuf cent quatre vingts .
il n' était donc
pas rare pour les anciens dirigeants de la République démocratique
allemande et de l' Allemagne de l' Ouest d' avoir eu un passé chez
les Jeunesses hitlériennes .
du fait que l' organisation était devenue
obligatoire dès mille neuf cent trente six , il n' y eut pas de volonté de bannir les
politiques qui avaient servi dans les Jeunesses hitlériennes , à partir
du moment où l' on considérait qu' ils n' avaient pas eu le choix .
l' exemple le plus patent fut celui de Manfred Rommel , fils
d' Erwin Rommel , qui devint maire de Stuttgart en dépit du
fait qu' il a fait partie des Jeunesses hitlériennes .
mais aussi , le
ministre allemand des Affaires étrangères Hans Dietrich Genscher ,
le philosophe Juergen Habermas , et le Prince consort des Pays Bas
Claus von Amsberg .
en outre , le dix neuf avril deux mille huit, les
médias annoncèrent que le pape de l' Église catholique romaine
benoit seize ( de son nom civil à la naissance Joseph Ratzinger )
avait servi contre son gré dans les Jeunesses hitlériennes à l' âge de
quatorze ans .
cette information suscita une polémique selon laquelle une
personne qui avait été liée d' une manière ou d' une autre au
nazisme ne devrait pas devenir pape .
cependant , les faits
révélèrent que Joseph Ratzinger ne partageait pas l' idéologie des nazis
cependant , rapidement , le caractère subversif des Jeunesses
hitlériennes disparait , et cette organisation devient impopulaire au
sein même des groupes qu' elle est censée encadrer .
en effet , comme pour
le KdF , les membres des Jeunesses hitlériennes utilisent les
infrastructures pour la satisfaction de leurs besoins et désirs , les
activités d' embrigadement , les veillées , le camping , pratiqué de
manière militaire , et la collecte de dons sont particulièrement
l' endoctrinement de la jeunesse , s' il se voulait totalitaire , rencontre
des réserves au sein de la société allemande .
tout d' abord auprès du
public que cette organisation est censée encadrer , puis au sein de la
obligatoire à partir du décret Gesetz ueber die Hitlerjugend du
transforme en structure bureaucratique , ce qui détourne beaucoup de
jeunes de ses rangs .
de plus , le caractère militaire de
l' encadrement et des activités proposées jouent un rôle non négligeable
dans la désaffection des jeunes à l' égard de l' organisation : dans le
meilleur des cas , ils s' ennuient dans les veillées , ne participent pas
en outre , l' application du Fuehrerprinzip finit par éloigner de
l' organisation un nombre de plus en plus croissant de jeunes :
obéissance inconditionnelle aux ordres , même lorsqu' ils semblent
absurdes , et châtiments sans appel semblent la règle et incitent de
auprès de la population , les jeunesses hitlériennes jettent le trouble
au sein des familles : séparés de leur famille , les enfants sont
souvent utilisés comme informateurs par le NSDAP .
au sein de
la société , lorsqu' ils sont en groupes , les membres sont souvent
grossiers et sans gêne à l' encontre des gens qu' ils peuvent
croiser .
de plus , indisciplinés et jouissant d' une
quasi impunité de fait , les jeunes militants de la Hitlerjugend mènent
suscite de fortes réserves dans le corps enseignant .
en mille neuf cent quarante , Artur Axmann prend la tête des Jeunesses
hitlériennes pour transformer l' organisation en une force auxiliaire
utile dans un contexte de guerre .
les Jeunesses hitlériennes assistent
les pompiers et l' effort de reconstruction des villes lors des
bombardements alliés .
elles accomplissent des missions dans le
radiodiffusion et servent dans les équipes de défense antiaérienne .
vers dix neuf cent quarante trois , les chefs nazis transforment les Jeunesses hitlériennes
en une réserve militaire où ils puisent des troupes à la suite des
pertes importantes et croissantes dues à la guerre .
ainsi la
douze E Panzerdivision SS Hitlerjugend sous le commandement de
fritz Witt est entièrement composée de jeunes garçons entre seize
et dix huit ans .
cette division est déployée pendant la bataille
de Normandie contre les forces canadiennes et britanniques au nord de
Caen .
pendant les mois qui suivent , la division obtient une
réputation de férocité et de fanatisme .
quand Fritz Witt est tué par
l' artillerie alliée , le SS Brigadefuehrer Kurt Meyer en prend le
commandement et devient le plus jeune commandant de division à l' âge de
lors de l' invasion de l' Allemagne par les Alliés , la
Wehrmacht recrute des membres des Jeunesses hitlériennes de plus
en plus jeunes .
en dix neuf cent quarante cinq , la Volkssturm engage dans des
combats meurtriers et sans espoir des membres des Jeunesses
pendant la bataille de Berlin , les Jeunesses hitlériennes
constituent une part importante des forces allemandes et se battent
avec fanatisme ( Alfred Czech ayant même été le plus jeune
soldat décoré par Adolf Hitler ) .
le commandant de la ville , le
général Helmut Weidling ordonne à Artur Axmann de dissoudre
les unités combattantes des Jeunesses hitlériennes , cet ordre n' est
jamais appliqué à cause de la confusion de la bataille de Berlin .
dissous les Jeunesses hitlériennes comme partie intégrante du
parti nazi .
des membres des Jeunesses hitlériennes furent accusés
de crime de guerre mais , dans la mesure où l' organisation était
constituée de mineurs , les efforts pour faire aboutir les poursuites
bien que les Jeunesses hitlériennes ne fussent jamais déclarées
avait commis des crimes contre la paix en corrompant les jeunes
esprits allemands .
de nombreux cadres de haut niveau furent jugés par
les Alliés , à l' instar de Baldur von Schirach condamné à vingt ans