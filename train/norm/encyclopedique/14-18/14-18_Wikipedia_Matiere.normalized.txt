du latin " materia " ( bois , matériaux de construction ) correspondant au
grec " hyle " ( matériaux forêt , jungle , bois , construction ) .
en langue
française , on trouve historiquement du XII E siècle jusqu' au
xx E siècle le concept de substance matérielle tangible .
en général , avec le terme matière des philosophes ont fait référence à
la réalité sensible , tout ce qui peut être objet d' expérience .
la matière peut se retrouver dans plusieurs états ou phases .
les quatre états les plus connus sont solide , liquide ,
gazeux , et plasma .
il existe aussi d' autres états un peu plus
bose einstein , superfluide et fluide supercritique .
lorsque la
matière passe d' un état à l' autre , elle effectue une transition de
phase .
attention : un changement d' état n' est pas une transformation
chimique !
ce phénomène est étudié en thermodynamique via les
diagrammes de phase .
la transition de phase se produit lorsque
certaines caractéristiques de la matière changent : pression ,
la matière au niveau fondamental est constituée de quarks et de
leptons .
les quarks se combinent pour former des hadrons ,
principalement des baryons et des mésons via la force
forte , et sont présumés toujours confinés ainsi .
parmi les baryons
se trouvent le proton ( dont la charge électrique est positive )
et le neutron ( de charge électrique nulle ) , qui eux se combinent
pour former les noyaux atomiques de tous les éléments chimiques
du tableau périodique .
normalement , ces noyaux sont entourés d' un
nuage d' électrons ( de charge électrique négative et exactement
opposée à celle du proton ) .
l' ensemble formé par un noyau et un nuage
qui comprend autant d' électrons négatifs que de protons positifs
présents dans le noyau est un atome .
il est électriquement neutre ,
sinon , c' est un ion .
les atomes peuvent s' agencer entre eux
pour former des structures plus grosses et plus complexes , telles que
les molécules .
une quantité de particules de matière s' exprime avec
la chimie est la science qui étudie comment se combinent les
noyaux et les électrons pour former divers éléments et molécules .
chaque particule élémentaire et , par extension , toute particule
composite est associée à une ( anti ) particule d' antimatière ( par
exemple électron positron ou proton antiproton ) .
une
particule d' antimatière se distingue de sa partenaire par le fait que
sa charge électrique soit opposée .
en outre , les nombres
baryoniques et leptoniques sont conservés .
toutefois , de
bien que les lois fondamentales de la physique n' indiquent pas une
préférence pour la matière par rapport à l' antimatière , les
observations cosmologiques indiquent que l' Univers est
constituée de vide .
c' est ce que l' on appelle une structure lacunaire ,
qui a été prouvée par l' expérience de Lord Ernest Rutherford .
les
noyaux ( donc la matière à proprement dit ) de deux atomes qui
constituent la " matière " sont séparés par une grande distance de
les travaux d' Albert Einstein en relativité restreinte nous
ont légué la fameuse équation E égale mc deux , où E est l' énergie au
repos d' un système , M est sa masse et C est la vitesse de la
lumière dans le vide .
cela implique donc que la masse est
ainsi par exemple lorsque plusieurs particules se combinent pour former
des atomes , la masse totale ( au repos ) de l' assemblage est plus petite
que la somme des masses des constituants ( au repos ) car en fait une
partie de la masse des constituants est convertie en énergie de
liaison , nécessaire pour assurer la cohésion de l' ensemble .
on
ce même physicien a établi le lien entre la courbure de
l' espace temps et de la masse énergie grâce à la théorie
de la relativité générale : la masse ( l' inertie ) de la matière ( ou une
géodésiques à suivre , les trajectoires possibles .
ainsi , en
relativité générale , la matière et l' énergie sont regroupées
sous la même bannière et une façon d' en mesurer la quantité est
d' observer la courbure de l' espace temps qui les contient .
devenue progressivement le fond indifférencié , le réceptacle .
devenue
pur concept , elle est atteinte par une opération de l' esprit et
correspond à ce qui pourrait subsister si l' on faisait abstraction de
toutes les qualités particulières d' une chose .
elle n' est pas un simple
matériau passif mais manifeste une certaine nécessité interne qui
autorise à la faire figurer parmi les causes ou même les
c' est Aristote qui porte cette notion au statut de concept
chaque chose , d' où une chose advient et qui lui appartient d' une façon
immanente et non par accident Les êtres sensibles sont des composés
de matière et de forme , et la matière est le substrat du
il y a dualisme entre matière et esprit .
l' une est solide , rigide ,
tangible et immobile de même que limitée , alors que l' esprit est
fait de mélanger esprit et matière faisait partie du plan de Dieu .
l' homme est ainsi à la frontière entre le monde créé qui comporte les
trois premiers règnes ( minéral , végétal et animal ) et le monde
créateur .
or , de cette situation l' homme sur cette terre se voit
attribuer une tâche , une finalité à savoir élever la matière ,
créer un lien entre le monde créé et le monde créateur , ceci afin que
la Vie puisse circuler librement dans toute la création .
en d' autres
termes , l' Esprit jouerait un rôle primordial sur la matière , selon John
Eccles qui reçut le prix Nobel de physiologie et de médecine en
Teilhard de Chardin affirmait que nos pensées sont des énergies
vivantes , des énergies psychiques qui imprègnent le milieu .
ainsi ,
mais de personne .
l' esprit n' est plus indépendant de la Matière , ni
opposé à elle , mais émerge laborieusement d' elle sous l' attrait de Dieu
cette vision des choses expliquerait notamment les phénomènes de
télépathie et l' affirmation selon laquelle les lieux possèderaient une
mémoire , et qu' ainsi on trouve des lieux dits " maudits " de même que
Teilhard qui inventa le principe de noosphère , sorte de milieu
d' énergies psychiques qui envelopperait la terre , affirme ainsi que
selon la nature des pensées humaines bonnes ou mauvaises , ces dernières
imprégnant le milieu , seraient , entre autres , capables de dégénérer les
organismes de vie , viendraient alors les maladies .
cette thèse rejoint
en fait la parabole du jardin d' Éden dont Adam et Ève furent chassés
par leur péché .
dans ce cas , la matière n' est rien d' autre qu' une pâte
création dans notre charge .
la matière , au sens propre comme au sens
dans l' économie moderne , on produit de plus en plus
d' informations , qui sont stockées et diffusées sur des
les principaux supports d' information sont le papier , et , de plus
en plus , les équipements électroniques qui stockent de
l' information ( matériels informatiques , réseaux , bases
de données , systèmes de gestion électronique des documents ,
systèmes de gestion de contenu ) ou la diffusent ( réseaux ) .
dans ce qu' on appelle quelquefois l' économie de l' immatériel , on
fait souvent passer l' information du support papier au support
que les termes " économie de l' immatériel " et " dématérialisation "
sont peu appropriés , car en réalité on ne fait que changer le support
de l' information .
le nouveau support , électronique , est lui aussi
on présente quelquefois la dématérialisation comme un avantage du point
de vue du respect de l' environnement et du développement
durable .
mais en réalité les choses ne sont pas si simples , car
l' utilisation des deux types de supports d' information consomme de
l' énergie et des ressources naturelles ( bois pour le
papier , métaux pour les équipements électroniques ) et génère des
déchets ( les vieux papiers et les déchets d' équipements
le bilan global n' est donc pas si simple à établir du point de vue de