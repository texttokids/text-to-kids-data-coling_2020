carte élémentaire montrant les limites de la commune , les communes
barcelonnette et les communes voisines ( cliquez sur la carte pour
les communes limitrophes de Barcelonnette sont Enchastrayes ,
faucon de barcelonnette , Uvernet Fours , Saint Pons .
Barcelonette se trouve dans les Alpes du sud de la France , dans le
nord est du département des Alpes de Haute Provence , en région
provence alpes côte d' Azur .
située dans la zone la plus large de
la vallée de l' Ubaye , elle est reliée par celle ci à la région de
gap et à la vallée de la Durance , et par des cols à
l' Italie voisine et au département des Alpes Maritimes .
barcelonnette se trouve à un cent trente cinq mètres d' altitude , près du confluent de
lors des deux dernières grandes glaciations , la glaciation de Riss
et la glaciation de Wuerm , la vallée est envahie par le glacier de
la vallée de l' Ubaye est encaissée entre des montagnes dont certains
sommets dépassent trois mètre d' altitude .
son sommet le plus élevé
le point culminant de la commune de Barcelonnette se situe au
chapeau de Gendarme à deux six cent quatre vingt deux mètres d' altitude , en rive gauche de
l' Ubaye .
en rive droite , l' altitude ne dépasse pas un neuf cent soixante douze mètres au lieu dit
du Rocher Blanc .
son altitude la plus faible est rencontrée à la
confluence de l' Ubaye et du Bachelard , à un cent vingt deux Monsieur
la commune compte six cent trente neuf hectares de bois et forêts , soit trente neuf pour cents de sa
superficie .
elle est située aux portes du parc national
du fait de son relief et de sa situation géographique , la vallée de
l' Ubaye compte une " abondance d' espèces animales et
plus remarquables sont l' Aigle royal , le Gypaète barbu ,
le Bouquetin .
la flore , très diversifiée , comprend
arrondissements de France ) était dans les siècles passés dénudé sur
les pentes exposées au midi , mais encore très boisé au nord ,
présentant pour les forestiers " sur les versants septentrionaux
d' assez beaux restes de forêts peuplées d' essences de choix et dans
d' excellentes conditions de végétation et de régénération .

xix E siècle " le mélèze entre pour moitié environ dans la
composition du peuplement ( ...
) les quinze hectares de bois
communaux soumis au régime forestier , existant dans
l' arrondissement de Barcelonnette , renferment de nombreux
spécimens de vieux mélèzes de grandes dimensions , dont on
tirerait de précieux produits si les moyens de transport ne
faisaient presque totalement défaut .
ces arbres magnifiques sont
ou laissés inexploités par les communes , ou abandonnés par les
en mille huit cent soixante huit , Duhamel propose d' en exploiter la résine par le
résinage ( inconnu en France pour le mélèze mais pratiqué en
Autriche , dont au Tyrol , où la résine est vendue sous le nom de
térébenthine de Venise .
selon lui un mélèze de cinquante à soixante ans
pouvait rapporter trois à cinq kilogrammes an de térébenthine durant environ quarante
production possible de cette térébenthine pour l' arrondissement
de Barcelonnette , qui aurait pu ainsi gagner une quantité
importante d' argent sans avoir ni à abattre ces arbres , ni
surtout à construire de couteuses " routes de vidanges "
pour difficilement sortir les gros bois ou grands arbres de la
barcelonnette ) se fait financer un voyage au Tyrol pour y
l' avènement de la pétrochimie réduit rapidement et
fortement l' intérêt commercial des résines naturelles .
la vallée de l' Ubaye a un climat montagnard , " au carrefour du
Dauphiné , de la Provence et de l' Italie " .
les vents sont doux
en raison de la présence du relief , bien que les hivers
restent rigoureux en raison de l' altitude de la commune .
en
tout , on y compte près de " trois cents jours de soleil et seulement sept cents Madame de
aucune des deux cents communes du département n' est en zone de risque sismique
nul .
le canton de Barcelonnette est en zone un B ( risque faible )
selon la classification déterministe de dix neuf cent quatre vingt onze , basée sur les
séismes historiques , et en zone quatre ( risque moyen ) selon la
classification probabiliste EC huit de vingt cent onze .
la commune
barcelonnette est également exposée à quatre autres risques
la commune de Barcelonnette est également exposée à un risque d' origine
technologique , les transports routiers de matières dangereuses pouvant
le plan de prévention des risques naturels prévisibles ( PPR ) de la
commune a été approuvé en dix neuf cent quatre vingt quinze pour les risques d' avalanche ,
d' inondation , de mouvement de terrain et de séisme mais le
la commune a été l' objet de plusieurs arrêtés de catastrophe
naturelle : en dix neuf cent quatre vingt quatorze et en vingt cent huit pour des inondations et des
coulées de boue , et en dix neuf cent quatre vingt seize et en dix neuf cent quatre vingt dix neuf pour des glissements
de terrain .
les séismes les plus violemment ressentis
sont ceux du cinq avril dix neuf cent cinquante neuf( épicentre à Saint Paul sur Ubaye ,
intensité de six , cinq à Barcelonnette ) et du dix sept février dix neuf cent quarante sept( épicentre
selon Albert Dauzat et Charles Rostaing , la forme la plus ancienne est
Barcilona , est attestée vers douze cent zéro , mais le lien avec
Barcelone n' est pas évident puisque la nouvelle cité a été fondée
en douze cent trente et un par Raimond Béranger V , comte de Provence et de
Catalogne .
il s' agit probablement d' un double oronyme
préceltique Bar Cin désignant des rochers ou des hauteurs , et
attesté à plusieurs endroits dans la région ( Barcillonnette et
vivaro alpin , il est dit Barcilona de Provença ou plus rarement
Barciloneta selon la norme classique , Barcilouna de Prouvença ou
Barcilouneto selon la norme mistralienne , en valéian :
un toponyme antérieur à Barcilona est recensé en dix sept cent soixante trois par
jean joseph Expilly , dans son Dictionnaire géographique ,
historique et politique des Gaules et de la France , " Barcino
nova " nom latin qui signifie " nouvelle Barcino " , Barcino étant le
nom romain de la Barcelone de Catalogne depuis sa fondation par
changement d' appellation en Barcelona au Moyen Âge .
ses habitants sont appelés les Barcelonnettes , en valéian lous
si ce bandeau n' est plus pertinent , retirez le .
cliquez ici pour
cet article ou cette section provient essentiellement de ( voir
utilisation de la balise référence obsolète , ou recopie cette
source , en partie ou en totalité .
cette source est dans le domaine
public , mais elle peut être trop ancienne ou peu objective ( septembre
vingt cent seize ) .
améliorez la pertinence et la neutralité de l' article
la région était peuplée de Ligures depuis le I er millénaire avant Jésus Christ
d' après Polybe , ils étaient " à peu près sauvages , se nourrissant
du lait de leurs brebis et du produit de leur chasse , sans lois , sans
industrie , vivant dans des cabanes informes , couvertes de chaume et de
quelques siècles plus tard , l' arrivée des Celtes dans la région donna
naissance à un peuple celto ligure " très nombreux " selon
Polybe , belliqueux mais néanmoins civilisé et commerçant , dont
gaules place aussi les Ésubiens dans la vallée de
le territoire de Barcelonnette , désormais romain , fut intégré à la
Narbonnaise .
en trente six après Jésus Christ , Barcelonnette fut intégrée par
la principale ville antique de cette " cité " était la civitas
Rigomagensium , donc Rigomagus , aujourd'hui Chorges .
elle est alors la capitale d' une civitas ( subdivision de
province ) jusqu'à la fin du IV E siècle avant d' être
rattachée à Eburodunum ( Embrun ) .
aucune monnaie romaine n' a
toutefois été découverte dans le canton de Barcelonnette .
raimond bérenger IV de Provence , comte de Provence .
selon
Charles Rostaing , l' acte de fondation de la ville , et qui accorde des
privilèges à la cité , est un ordre de reconstruction d' une ville
détruite , Barcelone , mentionnée dès mille deux cent ( villa Barcilona ) , et
dont le nom serait formé de deux racines oronymiques ( désignant une
montagne ) BAR , et CIN ( que l' on retrouve dans Mont Cenis ) .
selon
Ernest Nègre , le nom est un diminutif de Barcelone , la ville de
Catalogne , .
la ville est dotée d' un consulat
non féodale très particulier à l' époque et souvent réservé à des villes
plus grandes , un régime d' exception plutôt Provençale .
le découpage
cadastral du coeur de la ville qui est de cette époque signe le modèle
bastide , non pas au sens occitan tardif d' une Bastide
majoritairement développée dans le sud ouest .
cette période
locale particulière dans le paysage historique a aussi subi l' influence
politique déjà ancienne de Gênes qui a une structure autonome de
la mort de la reine Jeanne I re ouvre une crise de succession à la
tête du comté de Provence , les villes de l' Union d' Aix
d' Anjou .
barcelonnette fait partie de l' Union d' Aix , et quand l' Union
hommage à Marie de Blois , régente de Louis le deuxième d' Anjou , la
cité montagnarde reste indéfectiblement fidèle à Charles de
Duras .
cet engagement se double aussi de vieilles rivalités
contre les villes voisines , que vient apaiser une trêve avec Sisteron
la ville a été disputée entre les comtes de Savoie et les comtes de
provence : en mille trois cent quatre vingt huit , au moment du départ du comte de Provence
louis II d' Anjou pour reconquérir le royaume de Naples , le comte
de Savoie Amédée huit s' empare de la ville .
elle redevient un fief
provençal en treize cent quatre vingt dix , les d' Audiffret en sont seigneurs .
après Louis
deux , en quatorze cent dix sept , la ville revient au duc de Savoie .
elle est reprise
par le comte de Provence René d' Anjou en quatorze cent soixante et onze .
elle est de
nouveau reprise par le duc de Savoie au début du XVI E siècle alors que
depuis la mort de Charles le cinquième d' Anjou en quatorze cent quatre vingt un , le comté de
m z Isnard signale que Bertrand et Guillaume Rodulphi étaient
coseigneurs de Barcelonnette et de La Bréole en treize cent neuf .
au moment de l' invasion de la Provence par les armées de Charles
quint , en quinze cent trente six , François I er envoie dans la vallée les
six lansquenets du comte de Furstenberg pour la ruiner par une
politique de terre brulée .
la ville et la vallée de l' Ubaye restent
sous la souveraineté du roi de France jusqu' au second traité de
une partie " non négligeable " de ses habitants s' est , au XVI E siècle ,
convertie au protestantisme , et a été réprimée lors des guerres de
en quinze cent quatre vingt huit , les troupes de Lesdiguières s' emparent de la ville
et incendient l' église et le couvent au cours de ses combats contre le
duc de Savoie .
en seize cent zéro , après le traité de Vervins , le combat
reprit entre Henri le quatrième et le duc de Savoie .
Lesdiguières reprit
barcelonnette jusqu'à la conclusion du Traité de Lyon le dix sept
en seize cent vingt huit , pendant la guerre de succession de Mantoue , Jacques
du Blé d' Uxelles voulant faire passer son armée en Italie pour aider le
duc de Mantoue s' empare de Barcelonnette , la pille et la brûle
comme la plupart des villes de la vallée .
la ville est reprise en
la ville est de nouveau prise en seize cent quatre vingt onze par les troupes du marquis
de Vins pendant la guerre de la Ligue d' Augsbourg .
dès seize cent quatorze et jusqu' en dix sept cent treize , Barcelonnette est le siège de
l' une des quatre préfectures du ressort du Sénat de
Nice .
à cette époque , la communauté parvient à racheter la
seigneurie , mise aux enchères par le duc de Savoie .
elle devient ainsi
son propre seigneur , avec pouvoirs de justice .
en seize cent quarante six ,
la viguerie de Barcelonnette ( comprenant aussi Saint Martin et
Entraunes ) a été rattachée à la France lors d' un échange de territoires
avec la Savoie lors des traités d' Utrecht un .
la ville
est ensuite le siège d' une viguerie jusqu'à la
Révolution .
parmi les autres administrations présentes à la
fin de l' Ancien Régime , un bureau de la poste royale est
installé à Barcelonnette .
un arrêt du conseil d' État du vingt cinq
décembre dix sept cent quatorze réunit Barcelonnette au gouvernement général de la
au gouvernement de Dauphiné , sous la Régence , en dix sept cent dix sept .
barcelonnette est un des rares bourgs de Haute Provence à accueillir
en mars dix sept cent quatre vingt neuf , des émeutes dues à la crise frumentaire ont
la nouvelle de la prise de la Bastille est accueillie
favorablement , cet évènement annonçant la fin de l' arbitraire royal et ,
peut être , des changements plus profonds dans l' organisation de la
France .
immédiatement après l' arrivée de la nouvelle , un grand
phénomène de peur collective s' empare de la France , par crainte du
complot des aristocrates désirant recouvrer leurs privilèges .
des
rumeurs de troupes en armes dévastant tout sur leur passage se
propagent à grande vitesse , provoquant des prises d' armes ,
l' organisation de milices et des violences anti nobiliaires .
cette
grande Peur , venant de Gap et appartenant au courant de la
l' agitation perdure dans la vallée : une nouvelle révolte éclate le quatorze
juin mille sept cent quatre vingt onze , et la disette se déclare en avril mille sept cent quatre vingt douze .
la
société patriotique de la commune fait partie des vingt et un premières
créées dans les Basses Alpes , au printemps mille sept cent quatre vingt douze , par les envoyés
de l' administration départementale .
environ un tiers de la
population masculine la fréquente .
l' agitation connait un
nouvel épisode violent en août mille sept cent quatre vingt douze .
la ville est chef lieu du
district de Barcelonnette de mille sept cent quatre vingt dix à mille huit cent , avant de
en décembre mille huit cent cinquante et un , la ville abrite un mouvement de résistance
républicaine au coup d' État du secondécembre de Napoléon trois .
quoique minoritaire , le mouvement se déclenche le dimanche sept décembre ,
dès le lendemain du jour où la nouvelle du coup d' État arrive .
les
autorités sont arrêtées , les gendarmes désarmés : tous sont conduits à
la maison d' arrêt .
un comité de salut public est constitué le huit .
le neuf , les habitants de Jausiers et des environs forment une
colonne , sous la direction du conseiller général Brès et du maire ,
Signoret , de Saint Paul .
celle ci s' arrête cependant le dix avant
d' atteindre Barcelonnette , le curé de la sous préfecture s' étant commis
comme négociateur .
le onze , plusieurs fonctionnaires s' évadent et
trouvent refuge à Largentière , au Piémont .
l' arrivée de troupes le seize
décembre met fin à la résistance républicaine sans effusion de sang .
après l' échec de l' insurrection , une sévère répression poursuit ceux
qui se sont levés pour défendre la République : cinquante deux habitants de
barcelonnette ( autant qu' à Digne , alors deux fois plus peuplée )
sont traduits devant la commission mixte , la majorité étant condamnés à
la déportation en Algérie ( compte non tenu des condamnations
barcelonnette , fut entre mille huit cent cinquante et mille neuf cent cinquante , le creuset d' une forte
plusieurs maisons ou villas de " style colonial " érigées par des
fortune faite .
ces maisons furent construites à Barcelonnette et à
le vingt neuf décembre mille neuf cent sept, une panique s' empare des Barcelonnettes , qui
croient voir un énorme dirigeable survoler la ville .
cet OVNI
chasseurs alpins devant la mairie de Barcelonnette en mai mille neuf cent soixante dix.
la commune est durement touchée par la Première Guerre mondiale ,
avec quatre vingt un morts .
une souscription publique est lancée afin de
financer la construction du monument aux morts .
une autre
souscription , de fin mille neuf cent dix neuf à mille neuf cent vingt et un , a lieu dans toute la vallée de
l' Ubaye ( avec Allos ) et permet de financer un monument aux cinq cent neuf
morts de la vallée , érigé à Barcelonnette par Paul
Landowski .
outre les morts tués par les allemands , un
barcelonnette natif de Revel est fusillé pour l' exemple , son
nom figure sur le monument aux morts de Barcelonnette .
enfin , un certain nombre d' habitants de Barcelonnette ayant émigré au
Mexique , une plaque commémorative évoque la mort de dix citoyens
mexicains , venus s' engager durant la Première Guerre mondiale .
au début de la Seconde Guerre mondiale , Barcelonnette est protégée
de l' invasion italienne par la ligne Maginot des Alpes .
l' effondrement
des armées françaises lors de la campagne de France devant la
Wehrmacht permet cependant à l' Italie de satisfaire ses exigences : la
ligne violette passe à l' ouest de Barcelonnette , qui se trouve
ainsi en zone démilitarisée : les chasseurs alpins doivent changer de
garnison .
elle subit également les visites de la Délégation de contrôle
du dispositif militaire des Alpes , qui cherche à contrôler les
armements français , afin de neutraliser les fortifications en cas de
la quatre vingt neuf E compagnie de travailleurs étrangers , internant et
soumettant au travail forcé des étrangers jugés indésirables par
la Troisième République et par Vichy , a été établie à
Barcelonnette .
le vingt deux juillet , la gendarmerie reçoit l' ordre
de surveiller la résidence de Paul Reynaud , qui réside au Plan , à
l' écart de la ville .
Reynaud était président du Conseil jusqu' au
seize juin et partisan de la poursuite de la guerre , et Pétain
craignait qu' il quitte la France , voire rejoigne de Gaulle à
londres .
mais la zone étant démilitarisée , et Barcelonnette à quinze kilomètres
seulement de la ligne de démarcation de la zone d' occupation
italienne , un Reynaud en fuite serait difficile à rattraper .
le
dispositif est renforcé par degrés successifs , puis Reynaud est arrêté
le sept septembre pour être emprisonné au château de
la vallée de l' Ubaye a été l' un des centres de l' importante
mobilisation résistante qui a suivi le débarquement en Normandie , le six
juin dix neuf cent quarante quatre, dans la région provençale .
elle fut reprise par les
allemands à partir du treize juin .
à Barcelonnette , Paul Geay , Ernest
Gilly , Louis Lèbre , Léon Signoret , Émile Donnadieu ( résistants )
furent interceptés en mission dans la nuit du quatorze au quinze juin dix neuf cent quarante quatreet
exécutés sans jugement à vingt heures , dans la cour du lycée de
barcelonnette , le seize juin mille neuf cent quarante quatre. à la fin de la guerre , vingt six Juifs sont
arrêtés à Barcelonnette avant d' être déportés , puis les
ffi libèrent la ville à l' été mille neuf cent quarante quatre , avant l' arrivée des troupes
la commune a été décorée , le onze novembre mille neuf cent quarante huit, de la Croix de
le onze E bataillon de chasseurs alpins est en garnison à
blason de Barcelonnette Blason Parti : au premier palé d' or et de
gueules , au second d' argent à la clé renversée de gueules , le panneton
barcelonnette a été fondée en mille deux cent trente et un par le comte de Barcelone , qui était
aussi comte de Provence , sous le nom de Raimond Bérenger V La partie
dextre du blason rappelle la maison de Barcelone .
la partie senestre
le statut officiel du blason reste à déterminer .
article connexe : élections municipales de deux mille quatorze dans les
cette section est vide , insuffisamment détaillée ou incomplète .
article détaillé : liste des maires de Barcelonnette .
une école normale est créée à Barcelonnette en mille huit cent trente trois : elle y
fonctionne jusqu' en mille huit cent quatre vingt huit , lors de son transfert à Digne .
le lycée André Honnorat , succède au collège Saint Maurice ,
rebaptisé d' après le nom du ministre de l' instruction André
Honnorat en mille neuf cent dix neuf .
désiré Arnaud ( préfet ) , Pierre Gilles de
jusqu'à la Troisième République , les classes primaires sont
installées dans des bâtiments qu' elles partagent avec d' autres
activités : les garçons au collège Saint Maurice , avec l' école normale
de garçons , et les filles dans les locaux de l' hospice tenu par les
actuellement , trois écoles fonctionnent à Barcelonnette : une école
maternelle et une école élémentaire publiques , et une école privée
nationale ) .
l' école primaire de l' avenue des
trois frères arnaud est construite en mille huit cent quatre vingt deux mille huit cent quatre vingt trois pour accueillir les
classes primaires de filles .
dès mille huit cent quatre vingt quatre , les classes de garçons sont
installées au rez de chaussée , les filles occupant l' étage , un
cours complémentaire est ouvert pour les filles la même année ,
pour les garçons peu de temps après .
en mille neuf cent cinquante sept , l' école compte neuf classes ,
dont deux de maternelle , et dix en mille neuf cent quatre vingt treize .
de mille neuf cent soixante trois à mille neuf cent soixante treize ( création des
en deux mille dix , le collège André Honnorat de Barcelonnette ouvre un
internat dit " d' excellence " , destiné aux élèves doués mais de
condition sociale modeste .
cet internat est destiné à leur donner les
meilleures conditions d' études , .
il occupe les
locaux du Quartier Craplet , ancienne garnison du onze E bataillon de
chasseurs alpins puis du Centre d' instruction et d' entraînement au
barcelonnette est classée une fleur au concours des villes et
une brigade de gendarmerie chef lieu de communauté est implantée à
barcelonnette est dotée d' un consulat honoraire du
en deux mille neuf , la population active s' élevait à un deux cent quatre vingt onze personnes , dont
quatre vingt six chômeurs ( cent soixante dix sept fin deux mille onze ) .
ses travailleurs sont
principalement dans la commune ( soixante dix pour cents ) .
l' agriculture
compte encore dix établissements actifs , mais aucun salarié .
l' industrie et la construction emploient onze pour cents des actifs , et les
services et l' administration , un peu moins de quatre vingt dix pour cents .
le tissu économique est surtout composé de petites entreprises .
un
grand nombre d' entreprises de Barcelonnette dépendent du tourisme .
au
un er janvier vingt cent onze, les établissements actifs dans la commune sont
principalement des commerces et des services ( un vingt des
fin vingt cent dix , le secteur primaire ( agriculture , sylviculture , pêche )
comptait dix établissements au sens de l' Insee .
le nombre d' exploitations , selon l' enquête Agreste du ministère de
l' Agriculture , est tombé de neuf à six dans les années vingt cent zéro ,
après une perte de quatre exploitations de dix neuf cent quatre vingt huit à vingt cent zéro .
inversement , la surface agricole utile ( SAU ) a augmenté , passant
de six cent quarante deux à huit cent soixante et un hectares de dix neuf cent quatre vingt huit à vingt cent zéro , et reste à ce niveau
depuis .
l' essentiel de la surface est consacrée à
l' élevage bovin , qui concerne trois exploitations , les autres se
tournant vers l' élevage ovin et la polyculture .
fin vingt cent dix , le secteur secondaire ( industrie et construction ) comptait soixante sept
le tourisme soutient le secteur immobilier et de la
construction : la principale entreprise du secteur est la
sactp ( Société alpine de construction et travaux publics ) , qui emploie
une picocentrale hydro électrique est installée depuis l' automne
vingt cent treize sur la conduite d' adduction d' eau potable , en bas du captage des
sources du Riou Guérin et des Aiguettes .
si le débit est faible
permet d' avoir une pression de cinquante sept bars , et donc une bonne
productibilité .
la puissance nette délivrée par la
turbine Pelton est de deux cents W ( contre cent quatre vingt deux kilowatts estimés
initialement ) .
la prévision de production annuelle de un , sept million de
point wh devrait donc être dépassée .
l' exploitation est concédée à
Veolia en délégation de service public pour une durée de
la société Andélia , qui commercialise des bornes publiques d' accueil à
grands écrans tactiles fonctionnant avec point os , a reçu une Victoire
de l' entreprise ( décernée par le conseil général ) en vingt cent treize .
fin vingt cent dix , le secteur tertiaire ( commerces , service ) comptait trois cent quatre vingt trois
les cent dix huit établissements du secteur administratif , sanitaire et social et
d' après l' Observatoire départemental du tourisme , la fonction
touristique est importante pour la commune , avec entre un et cinq touristes
accueillis pour un habitant , l' essentiel de la capacité
d' hébergement étant non marchande .
plusieurs structures
d' hébergement à finalité touristique existent dans la commune :
classés deux étoiles , et deux classés trois étoiles , ayant au total
vingt cent sept ) et un autre classé trois étoiles , d' une capacité
enfin , les résidences secondaires apportent un important complément
dans la capacité d' accueil , avec mille cinq cent logements ( près la
moitié des logements de la commune sont des résidences
en vingt cent sept , le nombre total de logements est de trois cent soixante avec près de trente neuf pour cents de
ménages propriétaires de leur résidence principale en vingt cent sept .
ces logements se décomposent en trente pour cents de maisons individuelles et soixante cinq pour cents
d' appartements .
presque cinquante pour cents de ces logements sont des résidences
secondaires ( y compris les logements occasionnels ) , quarante trois pour cents des résidences
principales et huit , six pour cents des logements sont vacants .
le prix moyen de
l' immobilier neuf et ancien s' établit autour de trois cent quatre vingt dix sept euros le mètre carré .
le prix moyen a diminué ces dernières années et s' est rapproché de
carré en septembre deux mille dix. côté location , le mètre carré est de douze , vingt deux euros
par mois .
la Place Manuel est le quartier le plus prisé de la ville .
articles connexes : histoire du recensement de la population en
en deux mille dix sept en diminution de un , trente sept pour cents par rapport à deux mille douze , la commune de
barcelonnette comptait deux mille cinq cent quatre vingt dix huit habitants .
à partir du XXI E siècle , les
recensements réels des communes de moins de dix habitants ont lieu
tous les cinq ans ( deux mille sept , deux mille douze , deux mille dix sept pour Barcelonnette ) .
les autres
chiffres sont des estimations .
barcelonnette n' a pas connu d' exode
l' évolution du nombre d' habitants est connue à travers les
recensements de la population effectués dans la commune depuis
mille sept cent quatre vingt treize .
à partir de deux mille six , les populations légales des communes sont
publiées annuellement par l' Insee .
le recensement repose désormais
sur une collecte d' information annuelle , concernant successivement tous
les territoires communaux au cours d' une période de cinq ans .
pour les
communes de moins de dix habitants , une enquête de recensement
portant sur toute la population est réalisée tous les cinq ans , les
populations légales des années intermédiaires étant quant à elles
estimées par interpolation ou extrapolation .
pour la
commune , le premier recensement exhaustif entrant dans le cadre du
diminution de un , trente sept pour cents par rapport à deux mille douze ( Alpes de Haute Provence :
mille sept cent quatre vingt treize mille huit cent mille huit cent six mille huit cent vingt et un mille huit cent trente et un mille huit cent trente six mille huit cent quarante et un mille huit cent quarante six mille huit cent cinquante et un
deux cinquante deux cent quatre vingt deux deux quatre vingts deux cent trente deux cent quarante quatre deux cent cinquante quatre deux deux cent soixante sept deux deux cent soixante dix deux deux cent quarante deux
mille huit cent cinquante six mille huit cent soixante et un mille huit cent soixante six mille huit cent soixante douze mille huit cent soixante seize mille huit cent quatre vingt un mille huit cent quatre vingt six mille huit cent quatre vingt onze mille huit cent quatre vingt seize
deux cent cinquante trois deux vingt six deux un neuf cent dix neuf deux quatre vingt deux deux trois cent trois deux deux cent trente quatre deux neuf deux deux cent quatre vingt six
mille neuf cent un mille neuf cent six mille neuf cent onze mille neuf cent vingt et un mille neuf cent vingt six mille neuf cent trente et un mille neuf cent trente six mille neuf cent quarante six mille neuf cent cinquante quatre
deux trois cent soixante trois deux quatre cent cinq deux cinq cent trente deux deux deux cent seize deux sept cent cinq deux sept cent vingt trois deux neuf cent quatre vingt sept trois sept trois
mille neuf cent soixante deux mille neuf cent soixante huit mille neuf cent soixante quinze mille neuf cent quatre vingt deux mille neuf cent quatre vingt dix mille neuf cent quatre vingt dix neuf deux mille six deux mille sept deux mille douze
deux quatre cent trente deux deux quatre cent soixante seize deux six cent vingt six deux sept cent trente cinq deux neuf cent soixante seize deux huit cent dix neuf deux huit cent dix huit deux sept cent soixante six deux six cent trente quatre
de mille neuf cent soixante deux à mille neuf cent quatre vingt dix neuf : population sans doubles comptes , pour les dates
en mille quatre cent soixante et onze , la communauté de Barcelonnette ( qui comprenait plusieurs
paroisses des alentours ) comprenait quatre cent vingt et un feux .
en mille sept cent soixante cinq , elle
comptait six six cent soixante quatorze habitants .
les migrations jusqu'à la Grande
guerre , notamment au Mexique , ont un impact sur la ville .
selon le recensement de la population deux mille sept , Barcelonnette compte près
de deux sept cent soixante six ( population municipale ) ou deux neuf cent trente neuf ( population
totale ) habitants répartis sur seize , quarante deux km deux .
la ville se
caractérise par une faible densité de population .
entre dix neuf cent quatre vingt dix neuf et vingt cent sept , le taux de croissance annuel moyen était de
dix neuf cent quatre vingt dix neuf .
en effet , en vingt cent neuf , on y a compté vingt naissances
la station de Pra Loup est à dix kilomètres de Barcelonnette ,
celle du Sauze à quatre kilomètres .
saint anne la Condamine est une petite
station agréable et moins touristique que les deux autres .
tous les étés depuis dix neuf cent quatre vingt quatorze a lieu à Barcelonnette le Festival du jazz
les remparts n' ont laissé leur trace que dans le tracé des rues du
la mairie est construite dans les années dix neuf cent trente , après destruction
de la chapelle Saint Maurice ( en juillet dix neuf cent trente quatre ) .
son fronton
provient lui de l' ancien couvent des dominicains , classé en
bien que l' architecture des maisons anciennes soit archaïque , elles ne
datent pour les plus anciennes que du XVIII E siècle , la ville
ayant été reconstruite après l' incendie de seize cent vingt huit .
l' ancienne gendarmerie , place Manuel , construite pour abriter la
sous préfecture en dix huit cent vingt cinq , et actuellement transformée en logements , est
de style néoclassique sa façade , qui occupe tout un côté de la place ,
est percée de portes en plein cintre .
les pierres à bossages animent la
façade .
la place Manuel est nommée en honneur de l' homme
politique de la Restauration , Jacques Antoine Manuel , la
fontaine qui en occupe le centre porte son portrait sculpté par
la sous préfecture est installée depuis dix neuf cent soixante dix huit dans une des villas des
mexicains , la villa l' Ubayette , construite en dix neuf cent un dix neuf cent trois .
de nombreuses maisons construites par les Barcelonnettes revenus du
Mexique sont classées monument historique .
ayant émigré en masse , entre
dix huit cent cinquante et dix neuf cent cinquante au Mexique , ils ont détenu le monopole du
commerce et de l' industrie textile tout en y découvrant " l' importance
de l' architecture et son pouvoir de représentation dans ce siècle de
l' Industrie " ( François Loyer ) , en particulier sous le
gouvernement de Porfirio Diaz .
leur position sociale leur a permis
de devenir les promoteurs d' une architecture monumentale liée à la
création de leurs grands magasins .
de retour du Mexique , ils reprirent
pour l' édification un style directement issu de l' art industriel qu' ils
de leurs grands magasins mexicains à leurs villas de la vallée de
l' Ubaye , pour les Barcelonnettes les références culturelles sont
restées identiques .
les villas de Barcelonnette et de Jausiers ont
les mêmes architectes , décorateurs et fournisseurs spécialisés .
l' objectif de ceux ci et de leurs commanditaires fut " d' exprimer avant
tout l' image du progrès et de la réussite sociale " .
article détaillé : villas mexicaines de la vallée de l' Ubaye .
l' église paroissiale Saint Pierre ès Liens est construite au Moyen
reconstruite , trop vite , en seize cent trente quatre seize cent trente huit , puis rebâtie en seize cent quarante trois seize cent quarante quatre .
celle ci est à nouveau démolie , en dix neuf cent vingt six dix neuf cent vingt sept , pour laisser la place à
l' église actuelle , commencée en dix neuf cent vingt trois .
son clocher date de la
reconstruction du XVII E siècle .
en seize cent cinquante trois , il est augmenté d' un
pyramidions et de gargouilles , et surmonté en dix huit cent soixante d' un campanile en
fer forgé portant une statue de Vierge en métal doré .
plusieurs tombes du cimetière sont signalées par Raymond Collier pour
l' église Saint Pons comporte deux porches ( sud et ouest ) , tous les deux
abondamment illustrés , compte tenu de la pauvreté des décors en style
l' église est décorée d' un tableau de saint Sébastien
la chaire est ornée de nombreux personnages ( classée ) .
l' Autel et le retable Saint Joseph sont classés .
les vitraux sont contemporains de la dernière construction .
vifs et
colorés , ils représentent le Christ et sa mère ( choeur ) , et les saints ,
la tour Cardinalis ou " tour de l' horloge " , haute de
quarante deux mètres , est un des plus beaux clochers du
c' est une tour carrée , construite en mille trois cent seize selon la DRAC ( ou après
mille trois cent soixante dix huit d' après Luc Thévenon car un acte signale que le terrain de la tour
est nu à cette date ) , ouverte de baies géminées , surmontée d' une
pyramide de tuf , encadrée de quatre pyramidions .
elle est
construite comme clocher du couvent des dominicains .
le couvent a été bâti grâce à un legs d' Hugues de Saint Cher , fait
cardinal en mille deux cent quarante quatre avant le I er concile de Lyon , mort en mille deux cent soixante trois ,
avec l' appui de Raimond le troisième de Medullion ( ou Raimond de Mevolhon ) ,
archevêque d' Embrun , qui étaient tous les deux dominicains .
en très mauvais état après les guerres du début du XVII E siècle , elle
est rapidement reconstruite .
le parement de pierres de taille de la
partie inférieure date du XIX E .
des gargouilles ornent ses
le Mexique , musée de la Vallée de Barcelonnette .
le musée de la Vallée expose en différentes salles les objets qui ont
marqué la vie des habitants de la vallée .
objets usuels , objets
rapportés du Mexique ou objets religieux , il abrite entre autres
l' autel et le retable de la chapelle Saint Maurice , détruite pour
la construction de la mairie en mille neuf cent trente quatre , avec les portraits des douze
parmi les cadrans solaires de la ville , les plus anciens sont :
dans les écarts , les cadrans suivants sont remarquables :
légende en patois " gavot l' es pas qu vouo " .
prix Nobel de physique mille neuf cent quatre vingt onze , a passé son enfance à Barcelonnette .
hôpitaux , titulaire de la chaire d' ORL à la faculté de
barcelonnette et député à l' Assemblée Législative
française .
championne du monde de géant à Morioka en mille neuf cent quatre vingt treize , elle est
la skieuse française la plus titrée de l' histoire en coupe du monde