l' Insee distingue dix secteurs d' activités dans l' industrie
un .
la transformation et la conservation de la viande et la
préparation de produits à base de viande : huit trois cent quatre vingt dix neuf entreprises
employant cent dix neuf cent soixante seize salariés ( chiffres relatifs à l' année deux mille quinze ) ,
deux .
la transformation et la conservation de poisson , de
crustacés et de mollusques : trois cent quatre vingts entreprises , employant
trois .
la transformation et la conservation de fruits et légumes :
quatre .
la fabrication d' huiles et graisses végétales et animales
cinq .
la fabrication de produits laitiers : un trois cent seize entreprises
six .
le travail des grains , la fabrication de produits amylacés :
sept .
la fabrication de produits de boulangerie pâtisserie et de
pâtes alimentaires : trente neuf deux cent vingt neuf entreprises employant
huit .
la fabrication d' autres produits alimentaires : cinq cinquante et un entreprises
neuf .
la fabrication d' aliments pour animaux quatre cent cinquante six entreprises
dix .
la fabrication de boissons : quatre cent treize entreprises employant
dans le cadre de ces activités , cette industrie génère de
ce secteur économique comprend de puissants fabricants de produits de
consommation tels que Nestlé , Danone , Lactalis , Pernod
Xinjiang Chalkis Co .
Ltd ( numéro deux mondial de la tomate
en France , l' industrie agro alimentaire est l' un des premiers
secteurs d' activités en chiffre d' affaires , avec cent quarante milliards
d' euros .
le secteur compte dix entreprises représentant un effectif
c' est le premier employeur industriel d' Aquitaine , avec dix neuf pour cents de
le secteur concerne treize pôles de compétitivité .
il est
représenté et défendu par l' Association nationale des industries