les chars récents équipant l' armée rouge au moment de
l' invasion allemande : de gauche à droite , un BT sept , le
prototype A moins vingt , le T trente quatre modèle dix neuf cent quarante et le T trente quatre modèle dix neuf cent quarante et un .
la
découverte du T trente quatre , non signalée par les services de renseignement de
l' Abwehr , sera une surprise si frappante pour l' État major
allemand que le général Von Kleist le considèrera comme " le
meilleur char du monde " , l' URSS en produira trente trois huit cent cinq avant la
l' usine N O cent quatre vingt trois de Kharkov reçut l' ordre , le quatorze octobre dix neuf cent trente sept, de
construire un successeur au BT sept .
il en résulta le prototype
chenilles ou sur ses roues , une fois les chenilles ôtées .
les
retours d' expériences des tankistes républicains utilisant des " "
lors de la guerre d' Espagne décidèrent les autorités soviétiques à
produire un prototype roulant uniquement sur ses chenilles , le
dix neuf cent quarante , le ministre de la Défense soviétique donne son accord pour la
production de cent cinquante exemplaires du " A moins trente quatre secondes , sous le nom de T trente quatre .
Mikhaïl Kochkine est considéré comme étant l' ingénieur concepteur
du T trente quatre , après avoir déjà fortement contribué au développement des BT .
l' arrangement général du véhicule est classique , avec le moteur à
l' arrière .
tous les côtés de la caisse sont très inclinés par rapport à
la verticale .
cette caisse est fabriquée avec des plaques d' acier
homogènes soudées entre elles .
le profil bas du T trente quatre en fait une cible
difficile à neutraliser , au contraire du M quatre Sherman qui , souffrant
d' un profil haut et d' un blindage latéral vertical , est une cible plus
le char d' assaut soviétique est de fabrication rustique et solide .
son confort est sommaire , voire spartiate .
sur certains modèles , les
tankistes soviétiques devaient parfois changer les vitesses au maillet
lorsque les mécanismes étaient récalcitrants .
mais qu' il
fasse quarante degrés au dessus ou en dessous de zéro degree Celsius , le T trente quatre démarre au
quart de tour .
ses larges chenilles s' adaptent parfaitement aux
terrains du front de l' Est .
il est à l' aise sur route , autant que
dans la neige et la boue : la " raspoutitsa " tant crainte par la
Wehrmacht et ses panzers aux chenilles étroites qui
s' embourbent .
les premières versions , connaitront de graves défauts de
fiabilité et la grande majorité sera abandonnée , apparemment intacts ,
mais en panne , lors de l' invasion allemande en dix neuf cent quarante et un .
les filtres à air
de mauvaise qualité par exemple abaissent la durée de vie du moteur à
cinquante H .
mais il sera constamment amélioré , et les défauts progressivement
corrigés , jusqu'à obtenir un des chars les plus sobres et fiables de la
développé à l' usine de locomotives de Kharkov , avec l' aide de
l' Institut technique de cette ville , le moteur diesel V deux devait
beaucoup à des conceptions étrangères .
l' importance relative de ces
emprunts a longtemps fait débat .
d' abord considéré comme une
adaptation de l' Hispano Suiza douze Y , dans sa version construite
en URSS par Klimov , le V deux se vit ensuite prêter une ascendance
italienne , comme dérivant du moteur diesel Fiat AN un .
en fait , il
mille dix sept T , un modèle BMW VI produit sous licence en application d' un
contrat passé en mille neuf cent vingt six .
le moteur Mikulin le millième trente neuf qui équipait le
fameux avion d' assaut Il deux Sturmovik étant dérivé du même modèle
allemand , ce contrat de cession de licence passé par BMW illustre les
risques que comportent les accords de transfert de technologie
le toit et le dessous du char sont constitués de plaques épaisses de vingt
et vingt deux Madame , il possède une trappe pour l' évacuation d' urgence sous le
le blindage de caisse des T trente quatre soixante seize est épais de quarante cinq Madame de face , de quarante Madame
sur les côtés et l' arrière .
la caisse du T trente quatre quatre vingt cinq est épaisse de quarante cinq Madame
de tous côtés .
les T trente quatre modèle mille neuf cent quarante et mille neuf cent quarante et un sont dotés d' une tourelle
Stalingrad , adopte un blindage frontal de caisse renforcé de
soixante Madame .
sur le modèle mille neuf cent quarante trois , le blindage passe à soixante dix Madame pour la face de la
tourelle .
le T trente quatre quatre vingt cinq adopte une grande tourelle de quatre vingt dix Madame d' épaisseur de
face , de soixante quinze Madame en latéral et de cinquante deux Madame à l' arrière .
sa caisse reste
l' épaisseur du blindage de la caisse du T trente quatre semble relativement faible
avec seulement quarante cinq Madame .
cependant , le blindage des quatre côtés de la
caisse est incliné , ce qui a pour effet d' augmenter la protection
contre les impacts horizontaux .
l' avant du T trente quatre est incliné à soixante degrés par
rapport à la verticale ( ou trente degrés par rapport à l' horizontale ) , cet angle
double l' épaisseur réelle du blindage face à un projectile de
trajectoire horizontale .
de quarante cinq Madame , l' épaisseur effective du char passe
t trente quatre de quatre vingt dix Madame est presque identique , à dix Madame près , à celui du char
lourd Tiger un doté d' un blindage frontal de cent Madame non incliné .
notons que le blindage latéral de quarante cinq Madame du T trente quatre est incliné à quarante degrés par
rapport à la verticale , ce qui lui confère un blindage latéral effectif
de soixante Madame .
ce blindage bien pensé lui permet , tout au moins pendant la
première année de la guerre , d' être impénétrable par la majorité des
antichars et tanks allemands , dans la plupart des situations de combat .
cette conception réussie lui permettra d' être maintenu en production
pendant tout le conflit , et de faire bonne figure sur le champ de
bataille .
les ingénieurs soviétiques amélioreront en permanence les
processus de fabrication , permettant une production en très grande
série .
en se concentrant sur un petit nombre de modèles , ils compensent
les faiblesses inhérentes de l' économie soviétique quand il s' agit
d' atteindre des volumes de production similaires à ceux des démocraties
occidentales , au moins pour les matériels de combat ( mais tout en
restant dépendant du prêt bail pour nombre d' autres matériels ,
tels que les camions ou les locomotives ) .
tandis que le Reich attend le
désastre de Stalingrad pour mobiliser son économie de guerre , et
multiplie les modèles produits en petites séries , l' Union soviétique
produit près de soixante exemplaires de T 34 en seulement quatre ans .
cependant , le T 34 n' est pas exempt de défauts .
le principal d' entre eux
est sa tourelle , qui ne comporte que deux membres d' équipage ( un
chargeur et un chef de char canonnier ) .
ce dernier ne peut à la fois
observer le champ de bataille et utiliser le canon principal ,
contrairement à ses adversaires allemands , qui ont trois hommes en
tourelle .
de plus , les optiques du char sont de très mauvaise qualité ,
rendant l' observation du champ de bataille difficile , et les radios
armés : le Tigre un de cinquante sept tonnes , qui ne sera produit qu' à
un trois cents unités , tandis que le Tigre le deuxième de soixante dix tonnes ne dépassera
pas cinq cents unités produites .
le Panzer le cinquième Panther de quarante cinq tonnes arrive
trop tard et en trop petit nombre .
en réponse , le T 34 verra sa tourelle
modifiée pour recevoir un canon de quatre vingt cinq Madame , et surtout pour accueillir
trois équipiers , corrigeant ainsi le principal défaut du char .
la caisse est divisée en quatre compartiments distincts .
de l' avant
un .
le poste de pilotage , situé juste derrière la plaque de blindage
inclinée du glacis , avec le pilote à gauche et l' opérateur
radio à droite .
le pilote dispose d' une trappe rectangulaire
d' assez grande dimension , surmontée de trois épiscopes .
devant
compte tours .
il dispose de trois pédales ( embrayage
principal , frein et accélérateur ) et de quatre leviers ( deux
commandant les embrayages latéraux , un frein de parking et un
changement de vitesses ) .
il a également accès aux bouteilles du
système de démarrage d' urgence du moteur à air comprimé , au
tableau électrique du véhicule et au système d' interphonie TPU .
l' opérateur radio dispose , lui , d' une petite meurtrière en
protubérance où est montée sur rotule une mitrailleuse de type DT .
deux .
le poste de combat est surmonté par la tourelle qui embarque le
canon et sa mitrailleuse coaxiale DT .
le chef de char se trouve à
gauche et le pourvoyeur de la pièce à droite .
une grande partie de
l' espace disponible dans la tourelle est pris par la culasse du
canon et la zone de recul de celui ci , la plupart des munitions
obus sont disposés sur les côtés du char ( trois perforants à droite et
six à fragmentation à gauche ) , les munitions de mitrailleuse sont
disposées sur le côté droit et au sol entre le pilote et le radio .
le char standard embarque quarante six chargeurs de soixante trois coups , les modèles
sans radio construits au début de la production en ont
vingt neuf supplémentaires à la place de la radio , ce qui donne
respectivement deux huit cent quatre vingt dix huit et quatre sept cent vingt cinq coups disponibles .
le toit de la
tourelle possède une seule et unique écoutille à l' arrière , servant
aux deux hommes de la tourelle .
elle s' ouvre vers l' avant et inclut
sur sa gauche un épiscopes d' observation .
devant , à gauche , se
trouve le épiscope de visée PT six et à droite est placé le
ventilateur d' extraction de fumées sous son dôme blindé .
trois .
le compartiment du moteur est séparé par une cloison amovible du
compartiment de combat .
le moteur V deux y est monté
longitudinalement , encadré par les deux radiateurs et les deux
réservoirs avant .
il est surmonté par le filtre à air du type
quatre .
le logement de la transmission contient la boite de vitesses à
quatre rapports avant et un arrière , l' embrayage principal à
friction couplé à un grand ventilateur et , sur les côtés , deux
embrayages latéraux .
de plus , il contient les deux réservoirs
arrière , avec , au dessus d' eux , le démarreur électrique ST sept cents .
le
générateur GT quatre mille cinq cent soixante trois À délivre un kilowatt , six batteries STE cent vingt huit
l' assistent .
deux tensions sont disponibles ( douze et vingt quatre volts ) pour tous
les éléments actionnés électriquement à savoir : le démarreur , le
moteur de rotation de la tourelle ( à partir de mille neuf cent quarante quatre ) , le
ventilateur de celle ci ainsi que la dynamo de la radio .
le " t trente quatre modèle mille neuf cent quarante secondes possède un canon de soixante seize Madame L onze deux noeuds
aux performances antichars modestes .
un essai se fait avec une pièce
antichar plus performante de cinquante sept Madame ZiS deux puis ZiS quatre
permettant de perforer quatre vingt quatorze Madame de blindage à cinq cents mètres sous une
incidence de quatre vingt dix degrés .
le " t trente quatre cinquante sept secondes ne sera toutefois produit qu' à
cent trente trois exemplaires , l' obus explosif utilisé étant peu performant contre
l' infanterie .
est adopté alors le soixante seize Madame F trente quatre litres quarante deux sur le " t trente quatre
modèle mille neuf cent quarante et un secondes , avec obus antichar BR trois cent cinquante À , permettant de perforer à
cinq cents mètres cinquante sept Madame de blindage incliné à trente degrés , ou encore quarante sept Madame à
un mètres , trente deux Madame à deux mètres .
faute de mieux , ce canon sera
utilisé sur les T trente quatre modèle mille neuf cent quarante deux , modèle mille neuf cent quarante trois et modèle mille neuf cent quarante trois mille neuf cent quarante quatre .
ce
dernier modèle peut toutefois utiliser une munition nouvelle , l' obus
sous calibré BR trois cent cinquante P capable de percer quatre vingt douze Madame de blindage à trente degrés à
cinq cents mètres , cinquante huit Madame à un mètres .
pour conclure , la puissance des
canons montés sur les T trente quatre soixante seize et T trente quatre cinquante sept était suffisante pour percer
les blindages des chars allemands de mille neuf cent quarante et un , mais incapable de faire jeu
notamment au modèle Panzer le quatrième Ausf .
G dont le blindage de tourelle
et châssis , bien que verticalement monté , atteignait quatre vingts Madame ) .
ce constat
sera d' autant plus vrai avec l' apparition ultérieure du
Panzerkampfwagen le sixième " Tiger " fin mille neuf cent quarante deux .
la meilleure chance de
survie et de réussite pour les chars soviétiques de type T trente quatre est donc ,
début dix neuf cent quarante trois , la chasse en meute ( la masse de blindés devant palier leurs
déficiences conceptuelles ) .
les résultats des grandes offensives autour
Jupiter ) montreront toutefois les limites de l' organisation
tactique blindée des soviétiques .
celle ci ayant du mal à
maintenir une cohésion d' ensemble sur le long terme , la faute notamment
au manque de radios et de chef de char dans la tourelle .
tirant les leçons de la bataille de Koursk à l' été dix neuf cent quarante trois où les
canons de soixante seize , deux Madame ont montré leurs limites , les soviétiques lancent la
production du T trente quatre quatre vingt cinq armé du canon de quatre vingt cinq Madame ZiS cinq quatre vingt cinq deux noeuds ,
dont les performances restent encore insuffisantes pour concurrencer le
tigre et le Panther .
au cours de l' hiver dix neuf cent quarante trois dix neuf cent quarante quatre , ce canon
est vite remplacé par le quatre vingt cinq Madame D cinq T aux performances bien plus
convaincantes .
le canon de quatre vingt cinq Madame modèle D cinq T inverse la tendance avec
une perforation de cent dix Madame à un mètres de distance ce qui lui permet
de se mesurer aisément à la majorité des chars allemands ( dont le
Tiger un qu' il peut désormais pénétrer frontalement à une distance
entre huit cents et un mètre ) et lui fait prendre un avantage décisif sur la
dernière version du Panzer le quatrième , la version H Finalement , le canon
Zis cinquante trois est préféré au D cinq T car plus efficace et plus simple à produire
dès mars dix neuf cent quarante quatrejusqu' à la fin de la guerre , c' est le T trente quatre quatre vingt cinq modèle
le T trente quatre quatre vingt cinq redevient dangereux pour les blindés allemands , car , en plus
de l' amélioration du canon et du blindage , la nouvelle tourelle permet
d' accueillir un membre d' équipage supplémentaire , optimisant la
répartition des tâches , comme sur les blindés allemands .
de nouvelles
radios améliorent la coordination des chars entre eux , et , enfin de
nouvelles optiques de tir , apparues à la fin de l' année dix neuf cent quarante trois et
inspirées des modèles allemands , améliorent la probabilité de premier
coup au but .
celles ci restent cependant inférieures à celles des chars
allemands , d' autant plus que les équipages soviétiques sont entraînés
hâtivement pour faire face aux pertes très importantes , et n' ont pas le
la compétition entre Panther et T trente quatre quatre vingt cinq devient féroce , ce dernier
pouvant maintenant l' engager et le détruire de face à une distance de
huit cents mètres .
la qualité des blindages allemands décroissant au fil des
années jouera aussi , des rapports de combats de la fin de la guerre
indiquant des perforations au delà de neuf cents à un mètres .
départ de chars T trente quatre de l' usine Ouralmach de Iekaterinbourg
seuls cent quinze T trente quatre sont produits pendant l' année dix neuf cent quarante .
le nouveau char pose
en effet de nombreux problèmes de fabrication , tant sur le plan de la
mise en place des chaines d' assemblage que par des défauts de jeunesse
dix neuf cent quarante , remplaça le L onze , dès le quatre cents E exemplaire de la série ,
au cours de l' année dix neuf cent quarante et un , la production réussit alors à atteindre des
septembre , l' usine N O cent quatre vingt trois produit un cinq cent soixante chars en travaillant en
construits à Stalingrad sont identifiables par leur tourelle coulée
juillet dix neuf cent quarante deux, en produit cent soixante et un ( dont cent soixante treize exemplaires d' une
variante à moteur essence M dix sept T ( du fait de la rareté du V deux ) ) ,
au total , en dix neuf cent quarante et un , deux huit cents chars sortent de chaines de montage .
cependant , au vu des pertes et de la situation de l' Armée rouge , c' est
le département KB cinq cent vingt , évacué dès le dix neuf septembre , entreprend alors en
urgence de réétudier toutes les parties du char , afin de faciliter la
production de masse et d' économiser les matières premières stratégiques
comme le caoutchouc et les métaux non ferreux : à cette occasion ,
sept cent soixante cinq composants sont économisés sur chaque exemplaire .
la soudure et les
pièces moulées sont utilisées au maximum .
des améliorations
interviennent comme le montage de filtre à air du type cyclone ,
remplaçant les précédents d' une construction déplorable , prolongeant
grandement la durée de vie des moteurs .
une boite de vitesses à cinq
rapports est choisie , plus fiable et autorisant une plus grande vitesse
en août , une tourelle plus spacieuse de forme hexagonale est adoptée .
elle est , là encore , construite soit d' un bloc , soit en plusieurs
trappe des premiers modèles et adopte deux écoutilles indépendantes ,
plus légères à ouvrir et évitant au tireur et au chef de char d' avoir à
se pencher pour regarder vers l' avant du char .
le canon peut être
maintenant démonté directement par l' avant de la tourelle .
la standardisation de la production en souffre parfois .
on trouve des
expédients sommaires , pour contrer les manques de matériaux et la
d' urgence du moteur par des munitions réformées .
mais ce travail de fond paie rapidement , le char devenant très
ont été construits , soit cent dix neuf , sept pour cents de la production prévue .
pour
remplacer la production de Stalingrad , trois nouvelles usines sont
une variante à canon de cinquante sept Madame à haute vitesse initiale ZIS quatre est aussi
réalisée à quelques exemplaires , mais le prix prohibitif de ce canon et
son manque d' efficacité sur l' infanterie limita son usage .
en mille neuf cent quarante trois , sept quatre cent soixante six T trente quatre soixante seize sortent des chaines .
on voit apparaître une
coupole pour le chef de char , là encore de deux modèles , soudée ou
coulée , selon le lieu de production .
l' usine UZTM abandonne , elle ,
la production de chars pour produire des chasseurs de chars dérivés du
fin mille neuf cent quarante trois , la production du T trente quatre quatre vingt cinq est lancée .
le char connait sa plus
grande transformation .
la tourelle devient bien plus spacieuse et mieux
protégée , elle accueillera trois membres d' équipage ( dont un chef de
char ) contre deux auparavant .
le blindage de la tourelle est augmenté à
quatre vingt dix Madame , le canon de quatre vingt cinq Madame D cinq T puis Zis cinquante trois est bien plus efficace que le
soixante seize , deux Madame .
près de vingt trois unités de T trente quatre quatre vingt cinq sont produites jusqu'à la fin
de la guerre sur un total d' environ quatre vingt quatre chars T trente quatre tous modèles
canon automoteur SU cent vingt deux équipé de l' obusier cent vingt deux Madame le millième mille neuf cent trente huit .
en mille neuf cent quarante et un , les soviétiques furent confrontés au StuG III
allemand .
l' idée germa alors de développer , eux aussi , un canon
automoteur et , en avril mille neuf cent quarante deux, ordre fut donné aux différents
bureaux d' études de développer des projets d' un tel véhicule armé avec
un canon de cent vingt deux Madame .
deux projets furent acceptés par l' Armée
rouge , le SG cent vingt deux et le U trente cinq .
le premier , utilisant les châssis capturés
de Stug ou de Panzer le troisième , fut assez rapidement abandonné .
le
second , combinant un châssis de T trente quatre avec une superstructure à l' avant
et un obusier M trente de cent vingt deux Madame devint le SU cent vingt deux dans l' Armée rouge .
l' équipage comprenait cinq membres dont deux chargeurs , le canon ,
abrité par la superstructure blindée à quarante cinq Madame , possédait un champ de tir
au début dix neuf cent quarante trois , l' apparition des nouveaux modèles allemands démontra que
l' obusier de cent vingt deux Madame , même s' il était capable de les mettre hors de
combat , manquait , pour les contrer efficacement , de pouvoir de
perforation et surtout d' une trajectoire droite synonyme de précision .
en outre , sa cadence de tir était insuffisante .
on décida donc , le cinq
mai dix neuf cent quarante trois, de monter le canon antiaérien S dix huit de quatre vingt cinq Madame sur le châssis
du SU cent vingt deux .
l' adaptation rencontra alors un obstacle , le recul très
supérieur du nouveau tube .
deux projets virent le jour :
parallèlement , on essaya le canon D cinq S , comme solution temporaire sous
le nom de SU quatre vingt cinq II .
ce canon , à la suite des tests balistiques , se
révéla aussi bon que le S dix huit et finalement ce fut la solution
temporaire qui fut choisie et produite sous la désignation SU quatre vingt cinq .
ce
nouvel automoteur était dépourvu de mitrailleuses de défense , faute de
place dans l' habitacle , et des meurtrières obturables furent donc
ouvertes sur l' avant et les côtés de la superstructure pour permettre
le tir des pistolets mitrailleurs de l' équipage , autorisant ainsi une
certaine autodéfense contre l' infanterie adverse .
très rapidement , on
monta la coupole de char du T trente quatre soixante seize modèle dix neuf cent quarante trois et des prismes optiques
fournissant une vision panoramique autour du véhicule .
cependant , l' armement semblant toujours insuffisant , surtout avec
l' arrivée du T trente quatre quatre vingt cinq armé d' un canon équivalent , on étudia la
possibilité de monter un canon de calibre cent Madame .
le canon envisagé , le
s trente quatre de la marine , se révéla évidemment trop lourd et trop encombrant
pour le châssis dans sa forme initiale .
les efforts du TsAKB ( bureau
central d' étude de l' artillerie ) pour essayer de l' adapter donnèrent
naissance au SU cent deux qui fut écarté au profit du projet de l' usine
Uralmarsh , qui , plus pragmatique , avait demandé à l' équipe de FF
Petrov de dessiner un nouveau canon plus léger et petit , le D dix ,
nécessitant donc moins de modifications sur le véhicule .
les essais
menés face au SU cent deux en mars , puis en juin , se révélant
satisfaisants , la production en grande série fut décidée ( environ
deux trois cents unités produites pendant la guerre ) .
le blindage avant de la
superstructure avait été porté de quarante cinq à soixante quinze Madame , le canon de cent Madame , avec
une vitesse initiale de huit cent quatre vingt quinze mètres par seconde , pouvait percer un Panther ou un
tigre à un cinq cents mètres et avait une dotation de trente trois obus .
le canon D dix , trop jeune , souffrait de quelques défauts .
pire , sa
munition perforante , la BR quatre cent douze B , se révélait difficile à produire .
en
conséquence , la production d' un modèle transitoire armé avec le D cinq S de
quatre vingt cinq Madame fut lancée jusqu' en décembre , moment où le SU cent put enfin lui
le T trente quatre fut utilisé pendant toute la Seconde Guerre mondiale en
nombre sans cesse croissant .
la variante T trente quatre quatre vingt cinq semble encore utilisée
dans certains pays ( Cuba , Corée du Nord , Angola , et caetera ) .
au moment de l' opération Barbarossa , un millier de ces chars est
disponible .
les allemands ne connaissent que vaguement les matériels
blindés des soviétiques : kv un , KV deux et les T trente quatre n' étaient
connus que des plus hauts gradés de la Wehrmacht .
bien que
supérieurs à tout ce que les allemands pouvaient leur opposer , ils
souffraient principalement du manque d' entraînement de leurs équipages
et de la désorganisation de l' Armée rouge à cette époque .
en
effet , par suite des décisions contradictoires et irréalistes du
l' arme blindée soviétique , pourtant longtemps pionnière , était
complètement incapable de mener une guerre générale .
les unités , de
formation trop récente , manquaient de cohésion .
le matériel et les
hommes , en nombre insuffisant , étaient dispersés .
le corps des officiers a été sérieusement affaibli lors des
grandes Purges .
Staline avait fait massacrer les meilleurs
cadres de l' Armée rouge , les plus talentueux , expérimentés
l' arme blindée comme Mikhaïl Toukhatchevski .
lors de la guerre ,
Joukov a brillamment appliqué la stratégie des " opérations
en profondeur " ou " art opératif " développés par Toukhatchevski
dans l' entre deux guerres , les T trente quatre étant chargés de l' exploitation en
les officiers ayant survécu aux purges étaient souvent inexpérimentés
et incapables de mener une guerre moderne .
pour corser le tout , par
mesure d' économie , le personnel ne s' entraînait pas sur le matériel
récent , mais sur des chars démodés comme les chars T vingt six ou
bt deux qui avaient peu de rapport avec leurs futures machines de
temps de guerre .
de plus , les corps mécanisés étaient déployés à des
centaines de kilomètres de la frontière , ce qui , combiné aux défauts de
jeunesse des nouveaux modèles , à l' absence de matériel de dépannage
adapté ( on utilisait surtout des tracteurs agricoles réquisitionnés ) et
l' omniprésence de la Luftwaffe provoqua de nombreuses pertes avant
même que le combat s' engageât .
l' un des principaux défauts du T trente quatre est
l' absence de radio à bord , ce qui rend plus difficile la coordination
des manoeuvres .
l' autre faiblesse majeure du T trente quatre ( corrigée par le
T 34 quatre vingt cinq modèle dix neuf cent quarante quatre ) était l' absence de chef de char , alors que les
par exemple , le plus grand engagement de chars qui eut lieu pour
contrer la percée du Panzergruppe un , avec ses sept cent quatre vingt dix neuf panzers , est une
contre offensive lancée par le front du Sud Ouest entre les
cinq E et six E armées soviétiques , rassemblant les deux cent cinquante six chars
dix neuf E deux noeuds corps mécanisés , dont plus de la moitié
t trente quatre combattaient .
la Bataille de Moscou fut surtout gagnée avec
des chars légers , comme le T soixante , seuls quarante cinq T trente quatre y participèrent .
au
cours de l' hiver dix neuf cent quarante deux dix neuf cent quarante trois , le T trente quatre est engagé en masse lors des
offensives géantes dites des " quatre planètes " ( Uranus ,
Stalingrad .
ce ne fut qu' en dix neuf cent quarante trois que le char devint majoritaire dans
l' Armée rouge , époque à laquelle sa puissance de feu devenait
insuffisante .
son nombre et son endurance permirent de tenir jusqu'à
l' arrivée fin dix neuf cent quarante trois de la variante dotée d' un quatre vingt cinq Madame qui pouvait
combattre efficacement les meilleurs blindés allemands .
le T trente quatre quatre vingt cinq a été le fer de lance de l' opération Bagration ,
opération en profondeur par excellence , à l' été dix neuf cent quarante quatre puis de
toutes les grandes offensives de l' Armée rouge jusqu'à la
bataille de Berlin .
excellent à la manoeuvre , sa grande polyvalence
donna l' avantage à l' Armée rouge et permit d' exploiter dans la
profondeur les dispositifs défensifs de la Wehrmacht au cours de
l' année dix neuf cent quarante quatre , en Ukraine et en Biélorussie , puis en dix neuf cent quarante cinq à
travers la Pologne , lors de l' Offensive Vistule Oder jusqu'à
par la suite , six cent soixante dix T trente quatre quatre vingt cinq constituèrent le bélier qui enfonça
l' armée impériale japonaise en Mandchourie , au mois d' août
et l' invasion de la Mandchourie constituent des chefs d' oeuvre de
l' art opératif de Toukhatchevski appliqués notamment par
Joukov , Rokossovski et Vassilievski .
offensives au cours
desquelles les T trente quatre quatre vingt cinq jouèrent un rôle fondamental dans l' exploitation
l' usage du T trente quatre ne cessa pas avec la fin de la guerre : il constitua le
char de combat principal du Pacte de Varsovie jusqu'à l' arrivée en
nombre du T cinquante quatre et fut employé lors de nombreux conflits comme la
guerre de Corée , où il se révéla être à la hauteur face aux
Sherman et Chaffee des États Unis mais deux cent cinquante six au moins sont
perdus entre juillet et novembre dix neuf cent cinquante surtout à cause du
manque d' entrainement des équipages .
l' URSS envoya aux
révolutionnaires cubains près d' une centaine de T trente quatre quatre vingt cinq ,
ils ont servi en dix neuf cent soixante et un pour repousser avec succès l' invasion de la
baie des cochons .
il a aussi été utilisé lors des guerres
israélo arabes jusqu' en dix neuf cent soixante treize .
il a également servi dans plusieurs
conflits africains , comme en Angola où les forces du MPLA et
cubaines l' ont employé lors de la guerre civile angolaise
sud africaine .
le char a servi lors de la guerre de Bosnie pendant
les années dix neuf cent quatre vingt quatorze et dix neuf cent quatre vingt quinze , cinquante ans après sa mise en service .
selon le récit de Viktor Kutsenko , quelques T trente quatre quatre vingt cinq sont même utilisés
par les afghans lors de la guerre d' Afghanistan contre les
soviétiques lors des combats de Zhawar deux noeuds , mais ils y sont
pour l' anecdote , le six mai deux mille quatorzependant la crise ukrainienne , un
t trente quatre descendu d' un socle près d' un mémorial officiel puis remis en état
de marche mais démilitarisé a été exhibé dans une rue de Louhansk
on fait état de leur utilisation dans la guerre du Yémen où au
moins deux d' entre eux ont été détruits en deux mille dix neuf par les
en janvier deux mille dix neuf, l' armée russe à reprit trente T trente quatre à l' Armée
populaire lao pour les remettre en état et participer au
les allemands s' intéressent de près au T trente quatre qu' ils réussissent à
capturer intact et l' envoient à la Heeres Versuchsstelle Kummersdorf ,
au sud de Zossen , un centre de recherche de l' Armée , chargé
d' inspecter et de tester le matériel de guerre ennemi saisi sur le
champ de bataille .
le centre de Zossen reçoit les cinq premiers T trente quatre
décembre mille neuf cent quarante et un. les chenilles et les barbotins d' un de ces T trente quatre
sont remplacés par des chenilles et des barbotins prélevés sur un
panzer I Ausf .
F ou un Panzer le deuxième Ausf .
J Un autre servira au tournage
d' un documentaire diffusé pour les troupes en mille neuf cent quarante deux , Nahbekaempfung
russicher Panzer , consacré à la lutte contre les chars .
l' inspection des T trente quatre envoyés à Kummersdorf est remise dans un rapport
signé par l' Oberst Dpl .
Ing .
esser .
d' après lui , les atouts du T trente quatre
résident dans son canon F trente quatre , très efficace contre les Panzer III
et IV , et son blindage incliné qui le protège très bien des obus
de la Panzerwaffe , l' un de ses autres atouts réside dans son
moteur Diesel , combiné avec de larges chenilles qui lui offre une assez
a contrario , les défauts du T trente quatre relevés dans le rapport sont
l' étroitesse de la tourelle qui ne peut accueillir que deux personnes ,
la boite de vitesses ainsi que l' embrayage qui sont
défectueux , le filtre à air de mauvaise qualité , ainsi que des moyens
de communications largement insuffisants , la mauvaise qualité de
certaines pièces est également relevée .
cela n' empêche pas qu' Esser est
impressionné par les chars soviétiques , le rapport d' Esser constitue
probablement une des raisons de la conception d' un nouveau blindé
inspiré du T trente quatre le Vk trois mille deux décibels , qui sera remplacé par le VK
trois mille deux man , le prototype du Panzer le cinquième , qui reprend lui aussi des
au début de l' opération Barbarossa durant l' été mille neuf cent quarante et un , l' Ostheer
capture de nombreux T trente quatre , la plupart en bon état car abandonnés par
leur équipage .
malgré les difficultés de leur nouveau propriétaire à
régler les problèmes d' embrayage et de boite de vitesses , plusieurs de
ces chars de prise ( Beutepanzer ) désignés sous la nomenclature
t trente quatre sept cent quarante sept deux noeuds sont incorporés dans les un .
, huit .
, dix .
et
production des usines STZ de Stalingrad .
nombre d' exemplaires
greffe de poignées de maintien pour le transport de fantassins sur
trappes circulaires sur le toit , qui vaut au char le surnom de
des obus antichars sous calibrés , tourelleau de type " allemand "
pour le chef de char .
nombre d' exemplaires inconnu .
dite " Sormovo " contenant deux membres d' équipage .
environ trois cents
la tourelle " Sormovo " .
près de vingt trois unités produites de mars
une superstructure avant .
près de deux mille trois cent unités produites dès
longueur six , quatre vingt quatorze mètres six , quatre vingt douze mètres six , soixante quinze mètres huit , quinze mètres six , quatre vingt quinze mètres huit , treize mètres neuf , quarante cinq mètres
hauteur deux , quarante et un mètres deux , quarante cinq mètres deux , soixante mètres deux , soixante douze mètres deux , vingt quatre mètres deux , quinze mètres deux , vingt quatre mètres
canon L onze de soixante seize , deux Madame F trente quatre de soixante seize , deux Madame F trente quatre de soixante seize , deux Madame S cinquante trois de quatre vingt cinq Madame
munitions soixante dix sept coups soixante dix sept coups cent coups cinquante six coups quarante coups quarante huit coups trente quatre
mitrailleuses DT X deux DT X deux DTM X deux DTM X deux sans sans sans
type V deux trente quatre de cinq cents V deux trente quatre de cinq cents V deux trente quatre de cinq cents V deux trente quatre M de cinq cent vingt
V deux trente quatre de cinq cents V deux trente quatre de cinq cents V deux trente quatre de cinq cents
réservoirs L
vitesse sur route cinquante trois kilomètres par heure cinquante trois kilomètres par heure cinquante trois kilomètres par heure cinquante kilomètres par heure cinquante cinq kilomètres par heure quarante sept kilomètres par heure
autonomie quatre cents kilomètres quatre cents kilomètres quatre cents kilomètres trois cents kilomètres trois cents kilomètres quatre cents kilomètres quatre cents kilomètres
autonomie tactique deux cent soixante kilomètres deux cent soixante kilomètres deux cent soixante kilomètres cent vingt kilomètres cent cinquante kilomètres deux cents kilomètres cent quatre vingts kilomètres
blindage quinze quarante cinq Madame quinze cinquante deux Madame quinze soixante quinze Madame vingt quatre vingt dix Madame dix quarante cinq Madame vingt soixante quinze Madame
caption : production du T trente quatre pendant la Seconde Guerre mondiale
au U un er décembre dix neuf cent quarante et un1942 dix neuf cent quarante trois mille neuf cent quarante quatre Du un er janv .
est une série télévisée polonaise en noir et blanc basée sur
le livre de Janusz Przymanowski deux noeuds .
elle est composée
de vingt et un épisodes de cinquante cinq minutes chacun , répartis sur trois saisons .
l' action se déroule en dix neuf cent quarante quatre et dix neuf cent quarante cinq , durant la Seconde Guerre
mondiale et suit les aventures d' un équipage et de leur char T trente quatre
l' histoire commence en dix neuf cent quarante et un où l' armée allemande capture un char
t trente quatre soviétique et son équipage , une suite d' évènements permet aux
l' histoire se situe en dix neuf cent quarante .
elle relate les péripéties d' une
petite équipe d' ingénieurs devant relier secrètement Kharkiv à
Moscou ( six cent cinquante kilomètres ) , pour présenter un nouveau modèle de tank