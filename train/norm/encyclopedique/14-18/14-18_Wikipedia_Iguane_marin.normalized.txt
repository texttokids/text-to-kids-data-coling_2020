cette espèce est endémique des îles Galápagos .
lors de sa visite aux îles , Charles Darwin trouva fort déplaisante
l' apparence de ces animaux , écrivant : " les pierres de lave noire de
la plage sont très fréquentées par de grands ( deux trois pieds ) et
dégoutants lézards maladroits .
ils sont aussi noirs que les roches
poreuses sur lesquelles ils rampent .
je les appelle les " lutins
en fait , Amblyrhynchus cristatus est polychrome et n' est pas toujours
noir : il peut présenter des coloris bruns , ocres , bleurs verts , roses
ou rouges .
les jeunes ont une bande dorsale de couleur plus claire et
quelques spécimens adultes sont de couleur grise .
chez les mâles
adultes , la coloration varie avec la saison : à la saison de
reproduction , les mâles des îles du sud ( Île Espanyola , île
Floreana , .
) sont les plus colorés et arborent des couleurs rouge et
bleu vert , sur l' île Santa Cruz , ils sont rouge brique et noirs de
jais , et sur l' île Fernandina , rouge brique ou verdâtres .
la raison pour laquelle les tons sombres dominent , est que l' espèce
doit absorber rapidement la chaleur afin de minimiser la période de
léthargie due au froid , après avoir émergé de l' eau .
les couleurs
sombres absorbent plus d' énergie solaire et favorisent ainsi le
réchauffement de l' animal qui partage sa vie entre l' océan
pacifique , froid autour des îles en raison du courant de Humboldt ,
et les roches de lave du rivage où , étant ectotherme , il doit se
réchauffer au soleil .
les Iguanes marins ont souvent la tête blanchie
par les dépôts de sel expulsé par leurs glandes nasales .
une autre différence entre les sous populations d' iguanes marins est
leur taille , puisque celle ci diffère en fonction de l' île .
les iguanes
vivant sur les îles Fernandina et Isabela sont les plus grands .
les plus petits se trouvent sur l' île Genovesa .
les mâles adultes
mesurent jusqu'à un , soixante dix mètre , et les femelles un mètre .
les mâles peuvent
sur terre , l' iguane marin est un animal plutôt maladroit , mais dans
l' eau , c' est un nageur gracieux .
cela est dû à des adaptations à la vie
marine qu' il ne partage pas avec les autres espèces d' iguanes .
sa queue
aplatie latéralement et ses piquants dorsaux lui permettent de nager
plus vite , tandis que ses longues griffes acérées lui permettent de
s' accrocher aux roches et autres matériaux qui l' entourent , luttant
ainsi contre les courants violents et la force des vagues .
deux auteurs ont rapporté des cas d' hybridation , sur l' île
Plaza Sud , entre Amblyrhynchus cristatus et Conolophus
subcristatus , un iguane purement terrestre apparenté à l' iguane
en nageant seulement dans les eaux peu profondes autour des îles , les
iguanes marins sont capables de survivre à des plongées allant jusqu'à
une demi heure à des profondeurs de plus de quinze M .
Darwin écrit
d' ailleurs qu' un membre de l' équipage avait immergé un iguane pendant
une heure puis , en le tirant avec une corde , l' avait trouvé toujours en
les plongées types ne durent cependant qu' une dizaine de minutes à des
après ces plongées , les animaux reviennent sur leur territoire de bord
de mer pour se reposer et se réchauffer au soleil .
quand le froid
l' engourdit , l' iguane n' est pas en mesure de se déplacer efficacement ,
ce qui le rend vulnérable à la prédation .
si on cherche alors à le
saisir , il peut devenir agressif , compensant son manque de mobilité par
des morsures .
l' espèce est également vulnérable au moment de l' éclosion
des juvéniles , guettés par des prédateurs aviens comme la buse
des Galápagos , ou ophidiens comme le serpent des Galápagos .
au cours de la saison de reproduction , les mâles deviennent
territoriaux tandis que les femelles s' assemblent en grands groupes que
les mâles courtisent , empêchant d' autres iguanes mâles de s' approcher .
pour manifester leur force , les mâles secouent vigoureusement la tête
de haut en bas .
si cette intimidation ne suffit pas , des bagarres
la taille des iguanes marins dépend des ressources alimentaires .
pendant les épisodes El Ninyo , les ressources en algues peuvent
diminuer sensiblement , le taux de croissance des iguanes aussi , et la
taille de ces générations sera de vingt pour cents moindre que celle des
générations précédentes .
lorsque les conditions alimentaires reviennent
antérieure .
les iguanes de taille et d' appétit standard confrontés au
phénomène El Ninyo supportent la sous alimentation en perdant de la
masse et du volume : on pense que leurs os raccourcissent par un
retrait du tissu conjonctif via une hormone de stress
particulière .
la réduction de taille serait également due à
l' avantage qu' ont les petits iguanes , comparativement aux plus gros ,
pour se réchauffer plus vite au soleil , leur permettant de retourner
plus rapidement dans l' eau pour s' y nourrir .
toutefois , si la
disette se prolonge , la mortalité des iguanes les plus gros et les plus
crabes rouges des Galápagos , plus omnivores que ceux de
l' Atlantique qui sont végétariens .
leur disparition permet au tapis
l' iguane marin a un régime alimentaire strictement herbivore tout
au long de sa vie : il se nourrit d' algues , notamment de la famille des
Ulvales , prélevées sur le littoral rocheux des îles , soit sur
l' estran , soit en pleine eau .
sur l' estran , il se contente de
prélever les algues exondées à basse mer .
mais il peut également nager
autour des îles et plonger pour s' alimenter sur les champs d' algues
comme souvent dans les archipels , des sous espèces propres à une île ou
isolées sur ces îles , en raison de leur faible nombre ou absence .
jiménez uzcategui , Quezada , Vences et Steinfartz , vingt cent dix sept
jiménez uzcategui , Quezada , Vences et Steinfartz , vingt cent dix sept
jiménez uzcategui , Quezada , Vences et Steinfartz , vingt cent dix sept
jiménez uzcategui , Quezada , Vences et Steinfartz , vingt cent dix sept
jiménez uzcategui , Quezada , Vences et Steinfartz , vingt cent dix sept
A C .
albemarlensis est reconnue en vingt cent dix sept comme étant synonyme d' A .
C .