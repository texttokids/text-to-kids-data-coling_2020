marcel Petiot nait à Auxerre le dix sept janvier dix huit cent quatre vingt dix sept , dix
ans avant son frère Maurice , né à Auxerre , le vingt décembre dix neuf cent six( et
décédé à Charenton le seize juin dix neuf cent quarante sept) .
il est le fils ainé de Félix
Irénée Mustiole Petiot , alors âgé de trente ans , employé des postes et
télégraphes d' Auxerre , et de Marthe Marie Constance Joséphine
bourdon , alors âgée de vingt deux ans , sans emploi , mariés , locataires d' un
appartement situé cent rue de Paris à Auxerre , au dernier étage .
un oncle maternel par alliance est professeur de mathématiques
au collège d' Auxerre .
marcel Petiot est issu de la petite
bourgeoisie bourguignonne , son père étant devenu receveur des postes de
dès son enfance , il manifeste une grande intelligence à cinq ans , il
lit comme un enfant de dix ans , et une forte précocité mais il
manifeste des signes de violence : il serait allé jusqu'à
distribuer des images obscènes en cours , tirer au revolver sur des
chats ou à en étrangler un après lui avoir plongé les pattes dans l' eau
bouillante .
ces récits ultérieurs sur sa délinquance précoce ne
sont cependant pas attestés et ont peut être été inventés pour un
internée à Sainte Anne pour une pathologie psychiatrique , sa mère
meurt lorsqu' il a douze ans .
il suit alors son père à Joigny où ce
dernier vient d' être nommé receveur .
il est par la suite renvoyé de
plusieurs écoles pour indiscipline .
à dix sept ans , il est arrêté pour
avoir fracturé des boites aux lettres , non pour voler les mandats mais
pour y lire les lettres et cartes postales .
il n' est pas condamné , un
psychiatre l' ayant déclaré inapte à être jugé , estimant qu' il a une
personnalité que l' on qualifierait aujourd'hui de
abandonnant ses études de médecine pendant la Première Guerre
mondiale , il devance l' appel et s' enrôle dans l' armée le onze janvier
mille neuf cent seize .
il est blessé au pied d' un éclat de grenade le vingt mars mille neuf cent dix sept.
accusé de vol de couverture à l' hôpital où il est soigné , il fait un
premier séjour à la prison militaire d' Orléans avant d' être transféré
dans le service psychiatrique de l' hôpital de Fleury les Aubrais
où les psychiatres le déclarent neurasthénique , déséquilibré
mental , dépressif paranoïaque et sujet à des phobies .
il est tout de
même renvoyé au front en mille neuf cent dix huit , blessé une nouvelle fois , et réformé
pour troubles psychiatriques .
la commission militaire de réforme de
sens fixe son invalidité à quarante pour cents et le déclare réformé temporaire .
un an
plus tard en mille neuf cent vingt , à Orléans , une autre commission fixe à cent pour cents cette
incapacité , ramenée à cinquante pour cents en mars mille neuf cent vingt et un. le dix huit juillet mille neuf cent vingt trois, une
dernière commission conclut à une " psychose mélancolique " , une
les anciens combattants bénéficiant d' un accès facile aux études et
d' une procédure accélérée , il obtient en trois ans son diplôme de
mille neuf cent vingt et un , avec mention très bien , sa thèse porte sur le
photographie du mariage de Marcel Petiot et Georgette Lablais , mille neuf cent vingt sept .
en mille neuf cent vingt deux , il ouvre un cabinet médical à Villeneuve sur Yonne ,
où il devient rapidement populaire en offrant aux indigents
consultations et vaccinations .
mais il se fait aussi remarquer par des
il épouse le quatre juin mille neuf cent vingt sept, à Seignelay , Georgette Valentine
Lablais , fille d' un commerçant local propriétaire du
restaurant parisien " chez Marius " , situé cinq , rue de
Bourgogne , leur fils unique Gerhardt nait le dix neuf
avril mille neuf cent vingt huit. rapidement , il est cité devant les tribunaux pour plusieurs
délits ( fausses déclarations à l' assurance maladie , détournements de
fonds ) .
son avocat , maître René Floriot , lui évite à chaque fois
la prison ferme .
révoqué de son mandat de maire , en mille neuf cent trente et un , il se
fait élire conseiller général mais est définitivement privé de
tout mandat électif , en mille neuf cent trente quatre , pour avoir trafiqué son compteur
dès ce moment , plusieurs affaires inexpliquées suscitent des rumeurs ,
dont la disparition de sa bonne Louisette et l' incendie de la laiterie
où est morte M me Debove , patronne de l' entreprise .
poursuivi par la justice pour divers délits , il part s' installer à
premier étage du soixante six rue de Caumartin au dessus d' un magasin
d' objets de piété .
son entreprise est d' autant plus florissante qu' il
organise une grande publicité , digne d' un charlatan , pour
s' attirer des patients souffrant des maux les plus divers .
il vante sa
pratique de l' électrothérapie .
il se prétend aussi spécialiste en
désintoxication , ce qui lui permet de délivrer des ordonnances de
complaisance à des toxicomanes ou morphinomanes sans risquer d' être
en dix neuf cent trente six , il est arrêté pour vol à l' étalage à la librairie
Gibert Joseph , dans le Quartier latin .
il affirme à ses juges
qu' " un génie ne se préoccupe pas de basses choses matérielles " .
déclaré aliéné mental , il échappe à la prison mais est interné d' office
et irresponsable " mais un second conclut à " un individu sans
rendu à la liberté le vingt février dix neuf cent trente sept, il reprend tranquillement ses
le onze août dix neuf cent quarante et un , il acquiert un hôtel particulier , à Paris ,
travaux : il fait surélever le mur mitoyen pour empêcher toute vue sur
la cour et transforme les communs en cabinet médical .
lors de fouilles
ultérieures , la police découvrira une cave intégralement aménagée
l' agonie des victimes après leur avoir administré une dose mortelle
de gaz ou de poison , sous prétexte d' un vaccin ,
des personnes craignant d' être poursuivies par la Gestapo .
les
candidats à l' évasion sont invités à se présenter chez lui , de nuit ,
munis d' une valise contenant bijoux , espèces et argenterie .
sous le nom
de " docteur Eugène " , il recrute deux rabatteurs : un coiffeur , Raoul
fourrier , et un artiste de music hall , Edmond Pintard .
les prétendants
au voyage disparaissent mystérieusement sans atteindre l' Amérique du
sud pas même Yvan Dreyfus , un prisonnier envoyé par la Gestapo pour
une première victime disparait le deux janvier dix neuf cent quarante deux. il s' agit de Joachim
Guschinow , un fourreur juif voisin de Petiot .
il aurait apporté
l' équivalent de trois cents euros en diamants .
visant d' abord les personnes
seules , Petiot s' en prend bientôt à des familles entières , en leur
proposant des " tarifs de groupe " .
les victimes sont essentiellement
des Juifs , mais aussi des malfrats désireux de se faire oublier .
parallèlement à ces disparitions , d' autres individus connaissant le
docteur , et risquant de le dénoncer , s' évanouissent dans la
les services allemands découvrent le réseau grâce à un second
indicateur , un Français nommé Beretta .
petiot est arrêté et torturé
pendant huit mois à la prison de Fresnes mais il n' avoue rien car
il n' entretient aucun lien avec la Résistance .
il est libéré le treize
janvier dix neuf cent quarante quatre, faute de preuves .
il décide alors de faire disparaître
plan des locaux sis au vingt et un , rue Le Sueur et photographies de la
des voisins incommodés , depuis plusieurs jours , par des odeurs
pestilentielles provenant d' une cheminée de la maison à l' abandon
située vingt et un , rue Le Sueur .
ils fracturent une fenêtre et pénètrent
dans l' immeuble .
ils sont vite alertés par les émanations et le
ronflement d' une chaudière .
descendus dans la cave , ils découvrent sur
le sol des corps humains dépecés , dont certains brulent dans une des
deux chaudières à bois d' où provient la fumée .
serait arrivé à bicyclette tirant une remorque recouverte d' une
toile , se serait fait passer pour son frère Maurice ou
pour un Résistant , aurait constaté les faits ( affirmant que
tous les corps étaient ceux de nazis ou de collaborateurs que
son frère ou lui même avait tués ) , mystifiant ainsi les policiers
commissaire Georges Massu , accompagné des inspecteurs principaux
schmitt et Battut , se serait présenté à son cabinet du soixante six , rue
Lauriston , au siège de la Gestapo française .
son chef supposé ,
Henri Lafont , aurait découvert ses crimes et , par chantage ,
l' aurait contraint à travailler pour son compte avant de lui
lors de perquisitions ultérieures rue Le Sueur , la police découvre au
fond de la cour , dans un débarras , des dizaines de cadavres rongés par
la chaux vive dans l' ancienne fosse septique , ainsi que
soixante douze valises et six cent cinquante cinq kilos d' objets divers dont un sept cent soixante pièces
d' habillement , parmi lesquelles : vingt et un manteaux de laine , quatre vingt dix robes , cent vingt
jupes , vingt six sacs à main , vingt huit complets d' hommes , trente trois cravates , cinquante sept paires de
chaussettes , quarante trois paires de chaussures , Une culotte de pyjama d' enfant
appartenant au jeune René Kneller , disparu avec ses parents .
marcel Petiot barbu sous l' identité du " capitaine Valéry " ,
photographié après son arrestation fin octobre mille neuf cent quarante quatre.
photographie d' identité judiciaire du docteur Petiot , vingt deux novembre
en fuite , Petiot s' engage dans les Forces françaises de
l' intérieur sous le nom de " capitaine Valéry " .
lors de
son procès , il expliquera que son propre réseau , nommé " Fly Tox "
marque alors très connue d' une pompe à main insecticide et allusion
ironique à la chasse aux mouchards avait été démantelé par les
allemands .
devenu médecin capitaine , il est affecté à la caserne
de Reuilly où il est devenu le D R Wetterwald , officier de Sécurité
militaire chargé de l' épuration des traitres et des
en septembre mille neuf cent quarante quatre, Jacques Yonnet , lieutenant à la DGER ,
publie dans le journal Résistance un article provocateur intitulé
réponse .
en adressant au journal une lettre manuscrite , il tombe dans
un piège .
le journal n' étant diffusé que dans la capitale et en
banlieue , la police en déduit qu' il se cache à proximité , au sein de la
résistance .
elle compare son écriture facilement identifiable
finalement , l' inspecteur Henri Soutif ( qui a remplacé Georges Massu ,
alors emprisonné ) l' arrête le trente et un octobre mille neuf cent quarante quatreà la
station de métro Saint Mandé Tourelle .
on trouve dans ses poches
un revolver neuf Madame , une carte des Milices patriotiques et plusieurs
cartes d' identité .
l' Enquête met au jour la complicité de son frère
Maurice , commerçant rue du Pont à Auxerre , de sa femme Georgette , de
sa belle fille et maîtresse Léonie Arnaux , d' Albert Neuhausen ,
marchand de cycles à Courson les Carrières , chez qui ont été
l' avocat René Floriot et son client Marcel Petiot durant le
alors que l' épouse du docteur Petiot et Albert Neuhausen sont accusés
de recel et son frère Maurice d' homicide involontaire , le juge Goletti
arrivé au bout de son enquête rend une ordonnance de non lieu .
petiot , que la presse baptise " docteur Satan " , est jugé seul , du
de la Seine , pour homicides volontaires avec vol , guet apens et
préméditation .
il lui est reproché d' avoir commis , entre dix neuf cent quarante deux et dix neuf cent quarante quatre ,
vingt sept assassinats , dont ceux de douze Juifs et de quatre
proxénètes accompagnés chacun de leur prostituée .
dès le deuxième jour du procès , par fanfaronnade , Petiot revendique
soixante trois meurtres .
il affirme qu' il s' agit de cadavres de
traitres , de collaborateurs et d' Allemands .
jusqu' au bout , il
prétend avoir tué " pour la France " .
toutefois , il reste incapable
d' expliquer pourquoi un pyjama d' enfant figure dans les affaires
dérobées à ses victimes , ni comment les corps retrouvés sont ceux
durant les auditions , il montre une attitude désinvolte et va même
jusqu'à s' endormir .
cependant , l' expertise psychiatrique ne lui
décèle pas de maladie mentale .
le docteur Génil Perrin témoigne
aucun trouble mental et nous avons conclu à sa pleine
malgré la plaidoirie de six heures prononcée par son avocat ,
rené Floriot , il est condamné à mort pour vingt quatre
après son arrestation , Petiot est incarcéré à la prison de la
santé en novembre dix neuf cent quarante quatre. dans l' attente de son procès , il écrit un livre
sur le jeu , les probabilités et la recherche de martingales : " le
hasard vaincu " , ouvrage dans lequel il s' adonne aussi à quelques
aphorismes sur la vie .
petiot dédicace son livre lors de
suspensions d' audience à son procès .
suite à sa condamnation
prison de la Santé , où le prisonnier est soumis à une surveillance
constante pour éviter qu' il ne se suicide .
petiot partage son
temps entre la lecture et la confection de broderies .
au matin de l' exécution , quand l' avocat général Pierre Dupin le
réveille en lui disant : " ayez du courage , Petiot , c' est l' heure " , il
rétorque : " tu me fais chier " .
et quand , au dernier moment ,
il lui demande s' il a quelque chose à déclarer , il répond : " je suis
un voyageur qui emporte ses bagages " .
ces paroles rappellent
d' avouer ses crimes : " cela , Maître , c' est mon petit bagage ... " .
soucieux de laisser une bonne image à maître Floriot , il lui demande de
ne pas regarder l' exécution , affirmant : " ça ne va pas être beau ! " .
le vingt cinq mai dix neuf cent quarante six, à cinq heures sept , il est guillotiné
jules henri Desfourneaux .
d' après les témoins , il meurt avec
détachement , un sourire aux lèvres .
il est ainsi le premier
en marge de son acte de naissance , la mention de décès est erronée
quant à l' arrondissement ( il est inscrit : " décédé le vingt cinq mai dix neuf cent quarante six à
il est inhumé au Cimetière parisien d' Ivry , dans le carré des
suppliciés .
le terrain ayant été repris par l' administration après
l' abolition de la peine de mort en dix neuf cent quatre vingt un , les corps sont
relevés à la fin des années mille neuf cent quatre vingt dix si bien qu' on ignore ce que sont
la fortune indument amassée par Petiot est restée introuvable .
selon
certaines estimations , elle aurait atteint quelque trente millions d' euros
hôtel particulier du vingt et un rue Le Sueur et le démontèrent pierre par
bâtiment est détruit et remplacé par un immeuble neuf dans les années
sa femme Georgette tenta de refaire sa vie en travaillant dans une
boulangerie .
d' après des sources incertaines , elle serait partie
rejoindre son fils Gérard en Amérique du Sud à la fin des années
reporter photographe , tome sept : la Fête à Boro , Paris , Fayard , deux mille sept
retrouver le trésor de Petiot par les victimes spoliées est le
les colonnes de France Soir , dans la série Le crime ne paie pas .
paris , Glénat , deux mille sept deux mille douze .
personnage secondaire dans les tomes deux ,
le Vol noir des corbeaux un , et trois , Honneur et Police un .
Hugo Fregonese .
bien que l' action se passe à Marseille , le
personnage du Docteur Martout est visiblement inspiré par le Docteur Petiot .
sont glissés dans la salle ?
fait référence au docteur Petiot :
petiot , c' est pas sûr ...
le docteur Petiot , c' est ce médecin
parisien qui a démontré en mille neuf cent quarante quatre que les juifs étaient solubles dans
l' acide sulfurique .
petiot n' est pas un médecin juif .
Léon
Schwartzenberg , si .
d' ailleurs il n' y a aucun rapport entre Petiot
et Schwartzenberg .
je veux dire que Schwartzenberg , lui , il fait