bonjour !
tu me reconnais ?
mais si !
réfléchis bien ...
je suis la France !
un formidable pays , toujours en mouvement .
et je vous présente mes petites protégées : mes régions !
avant , j' en avait beaucoup , vingt deux , sans compter l' outre mer !
du coup , c' était très animé , et un peu brouillon ...
chaque région possède son caractère , ses habitudes , sa manière de faire , son ADN comme on dit .
les régions étaient trop nombreuses .
alors pour simplifier , j' ai imaginé une nouvelle organisation .
comment ?
avec " le découpage des régions " .
eh bien j' ai réfléchi avec mon cerveau , que l' on appelle le gouvernement .
je me suis creusée la tête pour trouver comment rendre les choses plus simples ...
nous avons établi des équipes !
les voici , elles vont se présenter : bonjour !
nous sommes le Nord Pas de Calais et Picardie , nous représentons un peu le Nord de la France .
nous représentons la Haute Normandie et la Basse Normandie et on est vraiment très heureuses d' être dans la même équipe .
salut , nous sommes dans le sud ouest , tout près de l' Espagne , Midi Pyrénées et Languedoc Roussillon .
bonjour , nous , on représente Bourgogne et Franche Comté .
dans notre région , il y a de délicieux fromages !
ne nous oublie pas , nous représentons l' Auvergne et le Rhône Alpes .
nous sommes trois !
poitou charentes , Aquitaine et Limousin !
l' Équipe de choc !
nous aussi , tiens !
champagne ardenne , Lorraine et Alsace !
de mon côté , je me la joue solo !
je suis la fière Bretagne .
moi , je suis au soleil presque toute l' année , la région Provence Alpes Côte d' Azur et moi , je suis la belle Île de France !
pour ma part , j' ai de beaux châteaux , je suis Pays de la Loire moi aussi , dans ma région il y a de très belles demeures !
je suis Centre Val de Loire !
et moi , ne m' oubliez pas , je suis petite mais l' une des plus jolie !
la célèbre " corse " .
je fais de nombreux produits , de la charcuterie , de la chataîgne et mon amie est la mer !
vous êtes toutes merveilleuses , mes belles régions !
grâce à ces nouvelles équipes , nous allons redessiner la France .
vous pourrez prendre des décisions ensemble , les gens auront encore plus envie de venir vous découvrir .
préparez vous à fusionner dès deux mille seize pour " la réforme territoriale " !
en route !