documents
pas la violence !
des manifestations pacifiques Comme Gandhi quelques années plus tôt , Martin Luther King veut prouver qu' il est possible de faire changer les lois injustes en désobéissant collectivement et sans violence .
après le boycott réussi des bus de Montgomery , les actions non violentes se multiplient .
en janvier dix neuf cent soixante, en Caroline du Nord , quatre étudiants noirs s' installent dans un restaurant réservé aux Blancs .
au lieu d' être servis , ils sont injuriés et chassés .
malgré cela , ils reviennent chaque jour .
la méthode du sit in ( voir P trente deux ) se répand et des marches pacifiques se multiplient .
des manifestants sont frappés et jetés en prison mais ils sont prêts à résister jusqu'à ce que les interdictions soient levées .
la presse et la télévision diffusent ces images choquantes .
l' opinion publique est touchée .
les autorités sont déstabilisées et interviennent : dès dix neuf cent soixante trois , la ségrégation disparait progressivement dans les magasins et les cafétérias .
la lutte pour les droits civiques progresse .
mais le chemin vers l' égalité est encore long ...
un an après son acte de résistance , en décembre dix neuf cent cinquante six, Rosa Parks choisit ici sa place à l' avant d' un bus de Montgomery .

trois grandes victoires dix neuf cent soixante quatre : interdiction de la ségrégation dans les lieux publics .
dix neuf cent soixante quatre : droit de vote des Noirs .
dix neuf cent soixante huit : fin de la discrimination des Noirs face au logement .
dès dix neuf cent cinquante cinq , Martin Luther King décide de résister de façon pacifique aux pratiques racistes des États du Sud .
il s' inspire de la philosophie d' un grand leader indien , Gandhi .
cette couturière noire , née en dix neuf cent treize
en Alabama , devient célèbre lorsque , le premier décembre dix neuf cent cinquante cinq, elle refuse de céder sa place à un Blanc dans un bus .
rosa Parks milite déjà contre la ségrégation et sait que son refus est contraire aux lois racistes du comté de Montgomery ( Alabama ) .
elle obtient le soutien de Martin Luther King , qui organise le boycott de la société de bus .
un an après , c' est la victoire .
les Noirs peuvent s' asseoir où ils veulent dans les bus .
mais Rosa Parks reçoit des menaces de mort et doit déménager .
lors de son décès le vingt quatre octobre vingt cent cinq, la compagnie de bus de Montgomery lui rendit hommage en ces termes : " à la femme qui s' est tenue debout en restant assise . "
le père de la non violence Né en mille huit cent soixante neuf en Inde , Gandhi est à la fois un guide spirituel hindou , un avocat et un homme politique .
il milite dès mille neuf cent dix huit pour obtenir l' indépendance de son pays , qui est alors une colonie britannique .
il organise la désobéissance civile de ses concitoyens en appelant , par exemple ,
à ne pas payer ses impôts ou encore à ne plus acheter de produits britanniques .
la RÉVOLTE DES CHAMPIONS Lors des jeux Olympiques de Mexico , en mille neuf cent soixante huit , les sportifs noirs manifestent leur soutien au combat de Martin Luther King , assassiné quelques jours plus tôt : au moment de recevoir leurs médailles , ils lèvent le poing et refusent de regarder le drapeau américain .
cette image a fait le tour du monde !
les années ont passé ...
en deux mille neuf , Barack Obama devient le premier président noir des États Unis .
ici , lors de la cérémonie d' investiture à sa réélection en deux mille treize , il jure sur la Bible de Martin Luther King .
la non violence est une méthode d' action collective qui consiste à refuser d' utiliser la violence contre un adversaire : on essaie ainsi de réveiller sa conscience pour lui faire comprendre que ce qu' il fait est mal .