
En 1905, le zoologiste allemand Max Hilzheimer décrivait le tigre de
Chine méridionale comme aussi haut que le tigre du Bengale, mais
ayant un crâne et une fourrure différents. Leurs canines et
leurs molaires sont plus courtes, leur crâne est plus court,
leurs cavités orbitaires plus rapprochées et leur processus
orbitaire est plus large. Leur fourrure est plus claire et plus
orangée, les rayures sont plus fines, plus nombreuses et plus


Le tigre de Chine méridionale est une sous-espèce assez petite, mais il
est cependant plus grand que les sous-espèces des îles de la Sonde
tel que le tigre de Sumatra. Les mâles mesurent entre 230 et 265 cm
et pèsent entre 130 et 175 kg. Les femelles sont quant à elles plus
petites et mesurent entre 220 et 240 cm et pèsent entre 110 et 115 kg.
Chez les mâles, la longueur du crâne peut atteindre entre 318 et



Les crânes décrits par Hilzheimer sont originaires
d’Hankou^. La répartition historique des tigres de Chine
méridionale s’étend sur un large territoire de 2 000 km d’est en ouest
et de 1 500 km du nord au sud de la Chine. À l’est, ils se
rencontraient dans les provinces de Jiangxi et Zhejiang et à
l’ouest dans celles de Guizhou et Sichuan. Au nord, des
Monts Qinling à la région du fleuve Jaune et au sud dans les



Au début des années 1950, on estimait la population du tigre de Chine
méridionale à plus de 4 000 individus à l’état sauvage. C’est à ce
moment-là qu’il est devenu la cible des campagnes massives
avant" de Mao Zedong. Aux effets de la chasse incontrôlée se sont
ajoutés ceux de la vaste déforestation et probablement la
diminution des proies disponibles et également la relocalisation
massive des populations urbaines vers les zones rurales, menant à la
fragmentation des populations de tigres et augmentant ainsi leur
vulnérabilité à l’extinction locale. En 1982, seulement 150 à 200
tigres de Chine méridionale subsistaient encore à l’état sauvage selon


En 1987, la population restante de tigres de Chine méridionale était
estimée à 30-40 individus, le danger d’extinction était donc
imminent^. Lors d’une étude menée en 1990, des traces de tigres
de Chine méridionale ont été découvertes dans 11 réserves dans les
montagnes des provinces de Sichuan, Guangdong, Hunan, Jiangxi et
Fujian, mais ces données n’étaient pas suffisantes pour estimer la
taille de la population. Aucun tigre n’a été directement observé, les
preuves se résumaient à des traces, des griffures et des témoignages


En 2001, des études ont été menées sur le terrain dans huit zones
protégées s’étendant sur un territoire total de 2 214 km^2, dans cinq
provinces de la Chine centrale et méridionale à l’aide de pièges
photographiques, de GPS et d’études étendues de signes de leur
présence. Mais aucune trace de tigre n’a été trouvée. Aucune des fumées
découvertes sur le terrain n’a pu être prouvée comme appartenant à des
tigres. Des espèces pouvant servir de proies aux tigres ont été


Des habitants de la région ayant rapporté avoir vu des traces dans la
Qizimei Mountains Nature Reserve, dans la province du Hubei et
dans le comté de Yihuang de la province de Jiangxi, certains individus
pourraient toujours subsister à l’état sauvage. En mai 2007, le
gouvernement de Chine a indiqué à la CITES qu’il ne pouvait pas
confirmer sa présence, et a déclaré vouloir réintroduire le tigre de



Les tigres sont exclusivement carnivores. Ils préfèrent chasser
les grands ongulés, se nourrissant fréquemment de sangliers
et occasionnellement de cerfs cochons, de muntjacs et de
langurs. Les petites proies telles que les porcs-épics, les
lièvres et les paons constituent une toute petite part de
leur régime alimentaire. À cause de l’empiètement de l’Homme sur leur



Le territoire initial des tigres de Chine méridionale comprenait des
muntjacs, des sangliers, des saros, des cerfs huppés et des


La plupart du temps, les tigres s’approchent au maximum de leur victime
par le côté ou par derrière et l’attrape par la gorge pour la tuer. Ils
trainent ensuite la carcasse à l’abri, parfois sur plusieurs centaines
de mètres, et s’en nourrissent. Leur façon de chasser et la
disponibilité des proies font qu’ils passent souvent d’une abondance de
viande à la famine : ils consomment donc entre 18 et 40 kg de viande à



En 1973, les tigres de Chine méridionale étaient classifiés comme
protégés par une chasse contrôlée. En 1977, ils étaient classifiés
comme protégés et il était interdit de les chasser^.

Les tigres sont inscrits sur l’Annexe I de la CITES, qui interdit
le commerce international. Les pays et États sur lesquels s’étend le
territoire des tigres et dans lesquels il existe un marché de
consommation ont également banni le marché domestique^.

L’organisation non gouvernementale Save China’s Tigers, avec le
soutien de l’Administration forestière chinoise, a mis au point un plan
pour réintroduire les tigres de Chine méridionale nés en captivité dans
de grands enclos au sud de la Chine. Le principal obstacle à leur
réintroduction concerne la disponibilité d’un habitat approprié et de
proies adéquates, ainsi que la forme physique de la population en
captivité. Il sera nécessaire de protéger leur habitat naturel et
d’augmenter le nombre des populations d’herbivores dont ils se
nourrissent. L’objectif final envisagé est de constituer au moins trois
populations, chacune d’entre elles étant constituée d’au moins 15 à 20
tigres vivant sur un territoire naturel de 1 000 km^2. Des études sur
le terrain et des séminaires ont été menés de façon coopérative afin
d’identifier des sites de réintroduction appropriés^.

Lors de la 14^e conférence des parties à la CITES en 2007, l’arrêt de
l’élevage en captivité des tigres et du commerce de leurs produits





Chine méridionale pure race dans leurs collections, dont 23 mâles et 14
femelles qui étaient nés à l’état sauvage. Tous étaient des descendants
de la troisième ou quatrième génération d’une femelle sauvage de Fujian
et de cinq tigres de Guizhou. Des problèmes notables venaient du manque
d’équilibre entre le nombre de mâles et de femelles et des couples qui

En 2005, la population captive de tigres de Chine méridionale était
constituée de 57 individus montrant des signes de consanguinité, dont
une diversité génétique réduite et un faible taux de reproduction
réussie^. En 2007, la population captive totale était
constituée de 72 individus ; quelques tigres se trouvant hors de
Chine^. Peu d’entre eux semblent être des tigres de Chine
méridionale pure race étant donné que l’on peut trouver des preuves
génétiques de croisement avec d’autres sous-espèces^.

Un tigron est né dans une réserve d’Afrique du Sud en novembre
2007, il était le premier à être né en dehors de la Chine. Depuis,


Les tigres de Chine méridionale en captivité en Chine sont désormais
tous consignés dans un studbook central. Avant la création d’un
studbook, on pensait que cette population captive ne comptait pas assez
d’individus et manquait de diversité génétique pour qu’un programme de
repeuplement puisse être un succès, mais depuis le début de
l’enregistrement central, de plus en plus de tigres de Chine
méridionale ont été identifiés dans les zoos de Chine.



Un tigre mâle de Chine méridionale du projet Save China's Tigers


L’organisation Save China’s Tigers, en association avec le Wildlife
Research Centre de l’Administration forestière chinoise et le Chinese
Tigers South African Trust, est parvenu à un accord concernant la
réintroduction des tigres de Chine méridionale dans la nature.
L’accord, signé à Pékin le 26 novembre 2002, demande la mise en
place d’un modèle de conservation du tigre de Chine méridionale,
par le biais de la création d’une réserve pilote en Afrique du Sud où
la vie sauvage indigène, y compris le tigre, sera réintroduite. Save
China’s Tigers a pour objectif de réintroduire l’espèce en danger
critique d’extinction en amenant quelques individus en captivité en
Afrique du Sud pour leur fournir en entrainement afin qu’ils se
réhabituent à la vie sauvage et qu’ils retrouvent leur instinct de
chasse. Parallèlement, une réserve pilote est montée en Chine et les
tigres y seront relocalisés et relâchés lorsqu’elle sera prête. Les
petits des tigres entrainés seront relâchés dans la réserve pilote de
Chine, tandis que les parents resteront en Afrique du Sud pour
continuer à se reproduire^. L'Afrique du Sud a été choisie car
elle peut fournir l’expertise et les ressources : le territoire et le
gibier aux tigres. Les tigres de Chine méridionale du projet ont
et d’assurer leur propre survie^. Ce projet rencontre
tigres réintroduits : 14 tigrons sont nés dans le cadre du projet et 11
ont survécu. Ces tigrons de la deuxième génération devraient être
capables d’acquérir leurs techniques de survie directement de leurs


On espérait qu’en 2012 la première deuxième génération de tigres nés en
Afrique du Sud pourrait être relâchée dans la nature^.


Les principaux défenseurs de l’environnement ne sont pas impressionnés.
Le WWF a déclaré que l’argent était mal dépensé et que les chances
de survie du tigre de Sibérie étaient meilleures^.

Récemment, les scientifiques ont confirmé le rôle de la réintroduction
de populations captives pour sauver le tigre de Chine méridionale. Un
séminaire a été animé en octobre 2010 dans la Laohu Valley Reserve en
Afrique du Sud afin d’évaluer les progrès du programme d’entrainement
et de réintroduction de Save China’s Tigers. Parmi les experts présents
on comptait le D^r Peter Crawshaw du Centro Nacional de Pesquisa e
Conservacão de Mamiferos Carnivoros, Cenap/ICMBIO, le D^r Gary Koehler,
le D^r Laurie Marker du Cheetah Conservation Fund (en), le
D^r Jim Sanderson de la Small Wild Cat Conservation Foundation, le D^r
Nobuyuki Yamaguchi du département des sciences biologiques et
environnementales de l’université du Qatar et le D^r David Smith
de l’université du Minnesota mais également des scientifiques du
gouvernement chinois et des représentants de Save China’s Tigers.

Les tigres en questions étaient nés en captivité, dans des cages, et
leurs parents sont tous en captivité et incapables de se nourrir
eux-mêmes dans la nature. Les tigrons ont été envoyés en Afrique du Sud
dans le cadre du projet Save China’s Tigers afin de les réhabituer à la
vie sauvage et de s’assurer qu’ils réintègreraient les compétences
nécessaires à un prédateur pour survivre dans la nature.

Les résultats du séminaire ont confirmé l’importance du South China
Tiger Rewilding Project pour la conservation du tigre. « Ayant vu les
tigres chasser dans un environnement ouvert à la Laohu Valley Reserve,
je crois que ces tigres réhabitués à la vie sauvage sont capables de
chasser dans n’importe quel environnement », a fait remarquer le D^r
David Smith. De plus, Save China’s Tigers a rétabli l’habitat naturel à
la fois en Chine et en Afrique du Sud durant sa tentative de
réintroduction du tigre de Chine méridionale dans la nature^.

L’objectif de préparer les tigres nés en captivité au milieu sauvage de
Chine dans lequel ils vivaient autrefois semble tout à fait possible
dans un futur proche selon le succès du programme de



Depuis 2001, l’équipe sud-africaine de Save China’s Tigers travaille
avec l’Administration forestière chinoise à l’identification de sites
pour la réintroduction des tigres de Chine méridionale réhabitués à la
vie sauvage. Neuf sites de quatre provinces différentes ont été étudiés
sur la base de 36 paramètres écologiques. Deux sites candidats ont été
sélectionnés à Jiangxi et Hunan au début de l’année 2005.
L’Administration forestière a donné son accord fin 2005. Grâce aux
progrès remarquables du projet de Save China’s Tigers en Afrique du
Sud, les autorités chinoises étaient encore plus motivées et ont décidé
de rechercher des sites dans les réserves naturelles, dans
lesquelles il y aurait moins de problème de relocalisation des
populations humaines, afin d’accélérer le retour du tigre de Chine
méridionale. Au début de l’année 2010, une équipe de scientifiques
gouvernementaux a identifié un site de test et trois sites finaux, qui
attendent désormais l’accord du département du gouvernement central
concerné. L’équipe de scientifiques de Save China’s Tigers travaille
avec les autorités chinoises à la préparation de clôtures, de
réapprovisionnement des proies et de mise en place d’une expertise en



Le 5 octobre 2007, un tigre supposé être un tigre de Chine méridionale
a attaqué une vache et le 13 septembre, le corps d’un ours noir
d’Asie qui aurait pu être tué et dévoré par un tigre de Chine
méridionale a été découvert. Les deux attaques se sont déroulées à


En octobre 2007, un chasseur a publié plusieurs clichés d’un tigre de
Chine méridionale qu’il disait avoir été pris dans les montagnes
Daba (en). Un mois après cela, le poster d’un tigre a fait
surface. Cela a déclenché une controverse au sujet de l’authenticité
des photos qui auraient pu être des copies du poster. Cependant, après
analyse de toutes les photos, il a été conclu que le tigre des clichés
vivant a été photographié dans la montagne. En comparant le poster avec
les photos, on s’est aperçu que le poster était un faux et qu’il était
en fait une copie modifiée des photos. Bonne nouvelle : le tigre de


Un villageois d’Ankang de la province de Shaanxi a déclaré
avoir risqué sa vie en prenant plus de trente photos digitales d’un
tigre. Le Bureau forestier de la province de Shanxi a par la suite tenu
une conférence de presse, soutenant les dires de Zhou. Si cela avait
de tigres de Chine méridionale à l’état sauvage dans les montagnes


Cependant, beaucoup se sont mis à douter de l’authenticité de ces
photos^. Un habitant de Panzhihua a remarqué que le tigre
qu’il avait en poster chez lui ressemblait grandement à celui des
photos de Zhou, jusqu’aux détails des rayures de l’animal. Le
concepteur du poster a été identifié comme étant le Yi Wei Si Poster
and Packaging Company de la province de Zhejiang, qui avait publié
l’image cinq ans auparavant^^,^. Dans une déclaration
faite le 23 novembre 2007, le Bureau forestier de la province de
Shaanxi a indiqué qu’ils étaient toujours « intimement convaincus » que
des tigres de Chine méridionale étaient encore présents dans la
province^. Cependant, le 4 février 2008, le Bureau forestier
de la province de Shaanxi a présenté ses excuses au sujet de ses
précédentes déclarations mais sans toutefois renier l’authenticité des
photos en déclarant : « Nous avons immédiatement publiée la découverte
du tigre de Chine méridionale sans avoir de preuves concrètes, ce qui
fait état de notre maladresse et de notre manque de discipline ».
Néanmoins, la déclaration ne précisait pas si le Bureau continuait de


En juin 2008, les autorités ont annoncé à la presse qu’elles avaient la
preuve que toutes les photos qui avaient été publiées étaient
falsifiées, et que les officiers qui avaient été liés à ces
publications avaient été punis, et même dans certains cas retirés de
leurs postes. Le photographe, Zhenglong Zhou, a été arrêté pour
suspicion de fraude^. Ceci a officiellement mis fin au
scandale autour du tigre de Chine méridionale mais les inquiétudes du
public concernant la corruption au sein du Bureau et du gouvernement de
Shaanxi pourraient perdurer. Beaucoup pensent que Zhou n’était qu’un
pion, et que les officiers locaux rassemblant des fonds du gouvernement
central au nom de la recherche et de la préservation du tigre et
attirant les touristes dans la région sont ceux qui tirent les


Bien que le gouvernement de Shaanxi ait officiellement reconnu la
fraude, certaines personnes pensent toujours que Zhenglong Zhou a
risqué sa vie et a découvert la preuve que le tigre de Chine
méridionale existe toujours à l’état sauvage^. Liyuan Liu,
professeur à l’université de Pékin, a déclaré qu’il ne croirait jamais
que les photos aient été falsifiées. Il a ajouté que Zhenglong Zhou
n’avait pas pu prendre les photos des empreintes en utilisant les
accessoires de la police de Shaanxi^. La première personne qui
avait déclaré avoir trouvé le poster a indiqué l’avoir acheté avant le
printemps 2001^. De plus, de nombreuses preuves indiquent que


