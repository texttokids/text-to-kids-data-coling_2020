
Selon Raponda-Walker, le nom d’Ogowe est d’origine



L'Ogooué à l'étiage dans la province d'Ogooué-Ivindo (août


L'Ogooué, d’une longueur avoisinant les 900 km, prend sa source en
République du Congo, dans les Monts Ntalé, à une altitude de
840 mètres. Il se jette dans l’océan Atlantique par un delta
marécageux au sud de Port-Gentil, après avoir suivi un cours
principalement d'est en ouest. Il constitue une frontière naturelle










Le débit du fleuve a été observé pendant 46 ans (1930-1975) à
Lambaréné, ville située à 134 kilomètres de son débouché dans


période a été de 4 689 m^3/s pour une surface prise en compte de
207 065 km^2, soit plus de 92 % du bassin versant du fleuve.

La lame d'eau écoulée dans le bassin atteint ainsi 714 millimètres
par an, ce qui peut être considéré comme très élevé.

L'Ogooué est un fleuve abondant, bien alimenté en toutes saisons et
donc assez régulier. Le débit moyen mensuel observé en août (minimum
d'étiage) atteint 1 930 m^3/s, soit quatre fois moins que le débit
moyen du mois de novembre, ce qui montre une irrégularité saisonnière
réduite. Sur la durée d'observation de 46 ans, le débit mensuel minimal
a été de 979 m^3/s, tandis que le débit mensuel maximal s'élevait à






L'Ogooué est navigable de Ndjolé à la mer. Il est utilisé pour le
transport du bois jusqu'à Port-Gentil. Axe de pénétration essentiel au
XIX^e siècle, alors qu'il n'y avait aucune route, l'Ogooué fut utilisé
par les explorateurs européens (dont Pierre Savorgnan de Brazza)
pour découvrir le Gabon. Les différentes tribus installées le long de
son cours préservaient jalousement leur monopole du transport des
marchandises sur la portion du fleuve qu'elles contrôlaient. L'Ogooué a
perdu de son importance comme voie de communication depuis la
construction de routes carrossables et depuis l'inauguration du


