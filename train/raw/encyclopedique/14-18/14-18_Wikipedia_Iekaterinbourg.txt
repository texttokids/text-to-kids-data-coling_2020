

Iekaterinbourg est située sur le pan asiatique de l'Oural, à
un carrefour ferroviaire important entre les liaisons provenant de
toutes les parties de l'Oural et le reste de la Russie, sur la ligne du
Transsibérien. Iekaterinbourg se trouve à 1 417 km à
l'est-nord-est de Moscou. La différence est de +2 heures avec
l'heure de Moscou et de +6 heures avec l'heure UTC.


Iekaterinbourg se trouve au pied du versant oriental du massif de
l'Oural. Au niveau de la ville, qui se situe à une altitude
moyenne de 237 mètres, l'altitude du massif est très basse avec des
cols situé à 400 mètres d'altitude. C'est donc un site de passage
naturel entre la Russie européenne à l'ouest et la Sibérie à l'est. En
raison de sa position, la ville est traversée par les deux grands axes
de circulation reliant les deux parties de la Russie : la Route de
Sibérie et le Transsibérien (train). La ville est traversée par le
cours supérieur de la rivière Isset, elle-même affluent de la
rivière Tobol. La ville est entourée de collines boisées, en
partie défrichées pour l'agriculture, et de petits lacs.


Iekaterinbourg subit un climat continental caractérisé par une
forte amplitude des températures saisonnières. La neige recouvre le sol
en moyenne 166 jours par an de la mi-octobre à la mi-avril. La hauteur
de neige peut atteindre 80 cm à la fin de l'hiver. L'été est bref et
dure en moyenne 65 à 70 jours. En été les chutes de neige ne sont pas
si exceptionnelles que ça. Le temps est très capricieux. Il connaît de
brusques variations d'un jour sur l'autre et d'une année sur l'autre.
La localisation de la ville et l'existence d'une grande variété de type
de vents pouvant souffler sur celle-ci sont à l'origine de ces sautes


CAPTION: Relevé météorologique d'Iekaterinbourg

Mois jan. fév. mars avril mai juin jui. août sep. oct. nov. déc. année
Température minimale moyenne (°C) −17 −15,6 −8,6 −0,3 5,3 11,2

Température moyenne (°C) −13,6 −11,6 −4,2 4,4 11,1 16,9 18,5 15,3 9,5

Température maximale moyenne (°C) −10 −7,1 0,8 10 16,8 22,6 23,8 20,5

Précipitations (mm) 23 19 16 28 44 69 92 68 55 39 30 25 508
Source : Le climat à Iekaterinbourg (en °C et mm, moyennes mensuelles)





La ville est fondée en 1723 par Vassili Tatichtchev,
responsable des forges, pour devenir une capitale régionale. Son
emplacement a été retenu car il se situe dans le prolongement de la
route Moscou - Kazan et à proximité d'un des cols les plus bas
franchissant l'Oural^. La ville prend le nom de la femme de
l'empereur Pierre le Grand, l'impératrice Catherine, future
impératrice régnante sous le nom de Catherine I^re (en


L'Oural est un massif métallifère particulièrement riche et de
nombreuses exploitations minières sont ouvertes pour exploiter les
gisements de fer, de cuivre ainsi que de charbon. Iekaterinbourg
devient le centre administratif de l'industrie minière de l'Oural et de
la Sibérie. La ville se spécialise dans la production de la fonte et du
cuivre, la réalisation de canons et la fabrication d'acier. Un atelier
monétaire est ouvert en 1735 qui frappera jusqu'à 80 % de la monnaie en
cuivre du pays. À partir de 1763 on commence à construire la Route de
Sibérie qui relie Iekaterinbourg à la Chine en passant par la Sibérie.
Les premières entreprises mécaniques sont créées à compter du début du
XIX^e siècle. Entre 1878 et 1888, un réseau de voies ferrées est
construit reliant la ville aux villes voisines de Perm, Tioumen et
Tcheliabinsk. Les liaisons ferroviaires dynamisent l'activité. Des

Nicolas II en captivité à Tsarskoïe Selo en 1917.

Peu après la révolution d'Octobre, l'empereur Nicolas II, sa
femme Alexandra Fedorovna et leurs enfants les grandes-duchesses

tsarévitch Alexis furent assassinés dans les caves de la

suite d'analyses ADN, le laboratoire de la faculté de médecine de
l'université du Massachusetts a publié ses résultats, confirmant que
tous les membres de la famille Romanov ont bien été exécutés. Le
peloton d'exécution était sous les ordres du chef de la Tchéka
locale, Iakov Iourovski. En l'honneur de Iakov Sverdlov qui
aurait ordonné l'exécution collective ou solitaire, la ville fut


En 1941, l'Armée rouge est rapidement en grande difficulté face à
l'armée allemande, qui a atteint les portes de Moscou. Après
avoir repris le contrôle de la situation militaire, le gouvernement
dirigé par Staline décide d'évacuer vers l'Est toutes les usines
d'armement de la région moscovite, par des dizaines de milliers de
trains. Ces usines sont transférées à Iekaterinbourg et plus
généralement dans tout l'Oural, hors de portée des bombardiers
allemands. À partir de cette époque la ville et sa région prennent un
essor considérable dans le cadre du complexe militaro-industriel. Au
centre d'un complexe militaro-industriel bâti à l'ère soviétique,
la ville est interdite aux étrangers de 1960 à 1990 (ville


C'est dans la banlieue de Iekaterinbourg que l'avion-espion américain



charbon^^,^ (en anglais : anthrax) en avril et mai
1979. Les autorités soviétiques de l'époque l'attribuent à de la
viande contaminée. Cependant, les autorités américaines pensent que les
habitants ont peut-être inhalé des spores échappées accidentellement
d'une installation militaire de production d'arme biologique.
L'accident est officiellement reconnu en 1992 par Boris Eltsine,
ancien secrétaire général du parti communiste de la ville et de la
région (oblast). Il semblerait que d'autres fuites (au moins une) se
soient produites, mais le silence le plus total est encore maintenu à


Iekaterinbourg retrouve son ancien nom en 1991 après la chute du

Roizman (en), opposant à Vladimir Poutine soutenu par la
Plate-forme civique de l'oligarque Mikhaïl Prokhorov, est élu
maire. Son élection compense pour l'opposition la défaite controversée
d'Alexeï Navalny à Moscou^. Roizman démissionne en
2018 et est remplacé par Aleksandr Vyssokinskii (ru).


Comme presque toutes les grandes villes russes, Iekaterinbourg a connu
une croissance démographique spectaculaire sous le régime soviétique
passant entre 1917 et 1989 de moins de 100 000 à plus de 1,3 million
d'habitants. L'éclatement de l'Union soviétique entraine en 1990 une
chute de la population qui se poursuit durant quelques années. Au début
des années 2000 la ville renoue avec la croissance mais l'augmentation
est beaucoup plus modérée qu'avant 1990. En 2015, les naissances
l'emportaient sur les décès avec 15,8 naissances contre 11,2 décès pour
1 000 personnes entrainant une augmentation naturelle de 6 749
personnes par an. Cette année-là les apports migratoires étaient encore
plus importants avec un solde positif de 9 580 personnes provenant à
hauteur de 54,1 % de la région de Sverdlosk, 18,2 % d'autres
régions de la Russie, le solde provenant de pays étrangers (Ukraine,
Kazakhstan,...)^. En avril 2017, Iekaterinbourg comptait près
de 1,5 million d'habitants, en faisant la ville la plus peuplée de
l'Oural et la quatrième ville la plus peuplée de Russie après














1959    1967     1970      1979      1989      1990
778 602 961 000 1 025 045 1 211 172 1 364 621 1 304 000



1998      2002      2010      2012      2013      2014
1 272 000 1 293 537 1 349 772 1 377 738 1 396 074 1 412 346







De nos jours, Iekaterinbourg est une grande ville industrielle. Elle
produit des machines-outils pour l'industrie mécanique et la
métallurgie (usine Ouralmach), de l'acier, des trains
fabriqué à Ekaterinbourg) des produits chimiques, des pneus, et
relance son industrie plasturgique. Le travail des pierres précieuses
est une industrie légère bien développée. Grâce à sa production


L'arrondissement Akhademia de la ville (raïon) est un peu isolé à
environ 5 km au sud du centre. Son nom est lié au fait qu'il regroupe
divers instituts de l'Académie des sciences de Russie. Pour parvenir à
ce raïon il faut franchir l'autoroute périphérique et traverser un bois
de sapins et de bouleaux. L'accès à cet arrondissement isolé était
strictement contrôlé pour des raisons de sécurité, durant l'époque
soviétique. Un très vaste projet d'extension de la ville y a été
décidé. Ses promoteurs, le gouvernement local et le groupe pétrolier et
d'aluminium Renova, veulent en faire un exemple tant architectural
qu'environnemental dans une région durement touchée par des pollutions
industrielles et militaires. Cette extension devrait s'étendre sur




En février 2009, après trois ans de travaux l'agence parisienne
Valode & Pistre et le groupe français Bouygues ont terminé la
construction de l'hôtel Hyatt d'Ekaterinbourg. Haut de 85 m, il
s'agit du premier bâtiment achevé d'EkatCity, nouveau quartier au cœur
de la capitale de l'Oural qui comprendra quatre tours dont une de
400 m. L'établissement offre une surface de 35 000 m^2. Drapées
derrière une façade convexe entièrement vitrée, référence selon
Jean Pistre aux concours de sculptures de glace très populaire
dans cette région, les 300 chambres donnent toutes sur la rivière
Isset ou sur la cathédrale. Pour éviter l'effet « paroi froide »
une succession de trois lames de verre entre lesquelles est pulsé de



Ekaterinbourg est à la fois un carrefour routier et ferroviaire entre
la Russie européenne et asiatique. La ville dispose d'un réseau de
transports en commun qui utilise toute la gamme des moyens de
transport : métro, tramway, trolleybus, bus et train de banlieue.


Une route fédérale relie Ekaterinbourg à Perm et, au-delà, à
Kazan. Comme toutes les villes russes, la ville doit faire face à
une augmentation très rapide de son parc automobile (progression
annuelle comprise entre 6 et 14 %) qui a porté à saturation le réseau



La ville est reliée par le train à tous les arrêts du Transsibérien de
Moscou à Vladivostok. C'est par ailleurs un nœud ferroviaire pour la
région avec des dessertes régulières circulant sur 7 lignes desservant
notamment Perm et Tcheliabinsk. La gare d'Iekaterinbourg



Le principal moyen de transport en commun d'Iekaterinbourg est le
tramway. Le réseau comprend 185 kilomètres de lignes qui ont
transporté en 2013 128 millions de passagers. La fréquentation est en
chute libre : en 2003 ce chiffre était de 245 millions passagers. La
ville dispose depuis 1991 d'une ligne de métro unique longue de
12,7 kilomètres comprenant 9 stations. Le métro a transporté environ 50
millions de passagers en 2015. La ville dispose également d'un réseau
de trolleybus (168 km et 78 millions de passagers transportés en 2006)
et un réseau de bus (93 lignes et 115 millions de passagers en 2007).


La ville est desservie par un aéroport, l'aéroport de Koltsovo.
Pour accueillir les spectateurs qui assistent aux rencontres de la
Coupe du monde de football de 2018 des travaux ont été effectués à
l'aéroport portant sur la reconstruction du quai et de la deuxième
piste d'atterrissage avec tout l'équipement nécessaire. En outre, des
travaux de préparation du terminal de passagers, de modernisation de
l'infrastructure technique ont été effectués et un hangar pour
l'aviation commerciale a été mis en exploitation. La capacité de
l'aéroport a été portée à deux mille personnes par heure^.


La ville abrite de nombreux établissements d'enseignement secondaire et
universitaire, dont la fameuse université des Mines de l'Oural,





Un grand batiment blanc, façade avec trois portes, trois niveaux

L'opéra de Iekaterinbourg, avec les muses à son sommet. Mai 2019.

La ville est un centre culturel important pour la région de l'Oural
ainsi que pour la Russie. Elle possède notamment de nombreuses
universités, un conservatoire, une Alliance française, et des
instituts dans les domaines polytechniques, miniers (dont la fameuse
université des Mines de l'Oural), forestiers, agricoles, de droit,
de médecine et d'enseignement. La branche de l'Oural de l'Académie des
sciences, avec plus de 70 instituts ainsi que de nombreux autres

Le patrimoine culturel comprend de nombreux musées et théâtres. Le
musée géologique de l'Oural présente tout sorte de roches de la
région. L'Opéra a été construit quelques années après le Bolchoï
de Moscou en utilisant pour sa partie technique les mêmes plans
améliorés. La Philharmonie accueille un grand orchestre et sa chorale
de 80 choristes. Cet orchestre philharmonique international a été
classé par les instances fédérales comme étant, par sa qualité, le
deuxième de Russie. Il a participé à Paris à l'inauguration de la
salle Pleyel rénovée, et il se produit dans le monde entier. Le
palais Kharitonov (1794), sur la colline de l’Annonciation, est la
plus grande résidence princière de l'Oural. Le Clavier (2005) est



Iekaterinbourg abrite le centre présidentiel Boris Eltsine. À
l'image des bibliothèques présidentielles américaines, il s’agit
d’un centre culturel et muséal qui tente de résumer l’héritage des
années 1990 et la présidence du premier président russe élu
démocratiquement, Boris Eltsine, dont la vie est liée à la ville. Le
bâtiment qui abrite le centre a été conçu par l'architecte russe



L'église de Tous-les-Saints, construite à l'emplacement de la

entre 2000 et 2003 sur le lieu de l'assassinat de la famille


Il existe de nombreuses églises orthodoxes dans cette grande métropole
de l'Oural, dont la plus ancienne (1770-1789) est l'église de
l'Ascension, offrant un beau panorama de la ville. L'église
Saint-Alexandre-Nevski (monastère de ND de Tikhvine) offre un bel
exemple de classicisme et l'église de la Transfiguration (XIX^e siècle
est tout à fait pittoresque. L'immense église de style
néo-byzantin Saint-Maximilien (ou la grande église de
Saint-Jean-Chrysostome, selon le nom d'une de ses chapelles latérales),
datant des années 1840 et détruite dans les années 1930 vient d'être
reconstruite et une première cérémonie liturgique s'y est tenue en août
2008. La cathédrale Saint-Jean-Baptiste, consacrée en 1860, était la
seule église orthodoxe ouverte pendant la période soviétique. De
nouvelles églises voient le jour comme Saint-Pantéleimon, la petite
jolie église blanche et bleue de la Nativité du Seigneur (2000). Quant
elle a été restaurée pour servir de siège au diocèse orthodoxe
restaurée fut celle du Sauveur (1876) en 1989. Une paroisse catholique









Lors de la Coupe du monde de football de 2018, le stade Central,
reconstruit pour l'occasion, accueille 4 rencontres.












