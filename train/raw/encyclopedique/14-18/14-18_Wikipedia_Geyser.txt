


L'activité des geysers, comme celle de toutes les sources chaudes, est
liée à une infiltration d'eau en profondeur. L'eau est chauffée par sa
rencontre avec une roche, elle-même chauffée par le magma ou par
l'action du gradient géothermique (le fait que la température et la
pression augmentent avec la profondeur), c'est pourquoi il est
possible de trouver des sources d'eau chaude et des geysers dans les
régions non volcaniques. Cette eau, chauffée et mise sous pression,
jaillit alors vers la surface par effet de convection. Les geysers
diffèrent des simples sources chaudes par la structure géologique
souterraine. L'orifice de surface est généralement étroit, relié à des
conduits fins qui mènent à d'imposants réservoirs d'eau souterrains.

L'intensité des forces en jeu explique la rareté du phénomène. Autour
de nombreuses zones volcaniques, on peut trouver des sources chaudes
accompagnées de fumerolles (île Sainte-Lucie, Java, Dallol,
etc.). Mais souvent, les roches sont trop friables, ce qui engendre une


L'activité d'un geyser est assez fragile et capricieuse ; certains se
sont éteints parce qu'on y avait simplement jeté des déchets, d'autres
parce qu'on y avait exploité l'énergie géothermique^.

Il faut distinguer un geyser d'autres phénomènes paravolcaniques :

ou un lac géothermique très chaud (comme le Prismatic Spring

des sédiments à sa surface (boue, argile, matériaux volcaniques…






De l'eau s'introduit dans le réservoir du geyser (qui est proche d'une
poche magmatique) par infiltration puis, en s'accumulant dans le
réservoir, elle monte dans le conduit étroit, résistant et
haut^. Il faut rappeler que la pression ne dépend pas du volume
mais de la hauteur, et que plus la pression est grande plus la
température d'ébullition est élevée. Ainsi l'eau du conduit va faire
pression sur l'eau du réservoir et augmentera la température
d'ébullition. Au bout d'un certain temps, la poche magmatique aura
suffisamment chauffé pour vaporiser une partie de son eau, créant ainsi
une bulle de vapeur qui va remonter vers la surface. Or, le seul
chemin de sortie est le conduit, où la bulle va donc s'engouffrer. Elle
va pousser vers le haut l'eau du conduit, qui n'exercera donc plus la
pression sur l'eau du réservoir. Cette dernière va entrer en


L'éruption se termine par épuisement du réservoir. Un nouveau cycle
accumulé suffisamment d'eau dans le réservoir pour que l'eau monte et
commence à se vaporiser. La durée de chaque éruption et le temps
séparant deux éruptions varient d'un geyser à l'autre, et leur

Séquence éruptive du geyser de Strokkur, en Islande : geyser
inactif, arrivée des bulles de gaz, naissance du geyser puis vidange de



Il existe deux types de geysers. Le geyser dit « en cône » est terminé
par un cône étroit, avec un conduit très fin. Lorsqu'une éruption se
produit et qu'une colonne d'eau jaillit, elle est en fait expulsée par
la pression due à l'étroitesse du conduit. C'est le cas par exemple
d'Old Faithful, dans le Parc national de Yellowstone, aux

L'autre type de geyser est le geyser dit « fontaine ». Il s'agit
généralement d'une source chaude qui, lorsque du gaz est expulsé, fait
remonter les bulles d'eau qui explosent au contact de la surface et qui
créent une large colonne d'eau, souvent de courte durée. C'est le cas








Les geysers sont relativement rares car dépendants de conditions
climatiques et géologiques que l'on ne retrouve qu'en peu d'endroits
sur terre. Il existe de par le monde cinq zones principales de geysers



geysers détruite partiellement à la suite d'un glissement de
terrain survenu le 4 mai 2007, mais toujours bel et



Il existe des geysers dans le Nevada, aux États-Unis. Ils sont
aujourd'hui éteints, à cause de l'industrie thermale, mais il en
subsiste toujours un dans l'État voisin de l'Utah, le Fly



Yellowstone est de loin la zone la plus active au monde avec près
de 400 geysers recensés^. Le parc possède en outre les deux
spécimens les plus imposants dont le célèbre Old Faithful.
Fichier:Old Faithful (California).ogv Lire le média




Le site Dallol (Éthiopie), est célèbre pour ses concrétions
de soufre et de sel fondu, et ses petits geysers gazeux.

Aux Açores, à São Miguel ou à Terceira, il s'agit surtout
d'eaux très chaudes mélangées à de l'oxyde de fer fondu.

Au Kenya, c'est au lac Bogoria^, situé dans le même axe
volcanique que le fameux lac Turkana, que l'on peut trouver des
dizaines de sources chaudes bouillonnantes et des geysers, gazeux et en


mares de boues. De petits geysers sont aussi présents mais ne



Puits géothermique jaillissant du Old Faithful of California à

Le geyser d'Andernach (vallée du Rhin) est le plus haut geyser d'eau



Dans des sites où existe une activité géothermique, l'homme a
parfois foré le sol, bâti des conduits étanches qui permettent à des
sources chaudes de jaillir comme des geysers. Ces geysers artificiels
sont appelés « puits géothermiques jaillissants ». Le résultat peut
Faithful du Parc de Yellowstone, véritable geyser celui-là). Sur le
même principe est construit le Strókur (avec un seul k,


On appelle aussi jaillissement perpétuel (en anglais perpetual
spouter), une source chaude ou un puits géothermique dont l'eau jaillit
en permanence. Appelé également geysers, ils n'en font pas vraiment


Un autre cas surprenant de geysers partiellement dus à la nature est
celui du jaillissement du Fly Geyser en plein milieu du
désert de Black Rock (Nevada, États-Unis). Ce geyser,
encore actif aujourd'hui, apparu 50 ans après le forage d'un puits en
1916 par les propriétaires d'un ranch, est dû à une source géothermique
ayant rencontré un point faible dans la maçonnerie : les minéraux
par trois cônes, d'où sort ce geyser rouge et orange ainsi juché sur
une plate-forme de calcaire qui continue à grandir.


Il arrive également que des sources d'eau froide jaillissent à la
manière d'un geyser par la pression du dioxyde de carbone dissous
dans l'eau. Il ne s'agit pas de geysers, bien qu'on les appelle souvent
le Crystal Geyser (en) dans l'Utah (États-Unis) et
le geyser d'Andernach (en) en Allemagne. En France, on
trouve des geysers d'eau froide, de petite taille (jets de quelques
dizaines de centimètres à quelques mètres de haut), notamment à
Bellerive-sur-Allier, près de Vichy, à Vals-les-Bains, en
Ardèche, ou encore en Auvergne (la Gargouillère à Lignat, commune de
Saint-Georges-sur-Allier, à Mirefleurs, geyser Brissac). Bien
que les projections puissent atteindre quelques mètres, le terme de


Il peut aussi s'agir d'un puits artésien, mais dans ce cas la
pression provient de la configuration géologique de la nappe d'eau, la
sortie de l'eau étant un point bas de la nappe aquifère.




Le geyser maritime est un trou souffleur littoral où des jets
d'eau de mer sont propulsés. Il s'agit donc d'un faux geyser. Le trou
souffleur littoral communique avec une grotte marine ou une
galerie sous-marine, où la force des vagues comprime une certaine
quantité d'air qui, au ressac, subit une ré-expansion rapide permettant
d'expulser l'eau hors de la cavité. Ce phénomène peut être observé
fréquemment sur des sites de grottes marines, entre autres avec le
spectaculaire geyser maritime de Kiama, en Nouvelle-Galles du Sud,





Les sources chaudes et les geysers peuvent abriter des
archéobactéries résistant à l'eau très chaude et au manque
d'oxygène. Les couleurs rouges, jaunes, bleues et vertes des bassins
thermaux, les filaments blancs ou les sortes de « feuilles » brunes et
gluantes entourant la mare d'eau chaude d'un geyser ou d'une mofette
sont en fait des organismes thermophiles (ou des types distincts
de cyanobactéries), vivant dans l'eau soufrée et surchauffée


C'est seulement dans les années 1960 que les scientifiques purent
démontrer l'existence de ces archéobactéries vivant dans l'eau des
sources chaudes, et en particulier l'espèce Thermus aquaticus,
décrite par Thomas Brock en 1969, notamment grâce aux études
menées au Yellowstone, et en particulier dans le bassin du Grand
Prismatic Spring. Cette découverte a été très utile pour la communauté
scientifique car elle a démontré le rôle joué par les cyanobactéries
dans l'apparition de l'atmosphère terrestre : pendant le
précambrien, l'eau de mer était à environ 80 °C, c'est-à-dire à la
même température que les sources chaudes d'aujourd'hui, et les
archéobactéries abondaient dans les eaux des océans. Rappelons que ce
sont les cyanobactéries qui sont à l'origine de la photosynthèse et qui



La Terre n'est pas la seule planète à posséder des geysers. Trois


Ces geysers sont différents des nôtres en bien des points, que ce soit
leur température, leur composition ou leur géologie externe :
liquide ont été observés. On ne connait pas l'origine de ces
geysers mais l'action du Soleil y est sans doute prédominante.
Sous la surface glacée (et relativement transparente) du satellite,
la glace d'azote fondrait aux niveaux où la lumière serait le plus
absorbée, puis la pression montant, le fluide s'échapperait par des
fissures. Dès la sortie, dans le vide presque parfait, le liquide
se transforme en un aérosol de gaz et de cristaux. Ces panaches des
geysers d'azote montent jusqu'à 8 000 mètres dans l'atmosphère


spatial le plus actif du système solaire, avec son volcanisme
soufré, la sonde Galileo a remarqué des panaches de soufre de
plusieurs kilomètres projetés dans la stratosphère par des volcans


remarqué à sa surface des petits geysers lâchant des panaches de




