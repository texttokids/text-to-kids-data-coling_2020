
Couverture du Petit Vingtième publié le jeudi 13 mai 1930
Couverture du Petit Vingtième publié le jeudi 15 mai 1930, montrant


Les Aventures de Tintin sont publiées pour la première fois le 10
janvier 1929 dans Le Petit Vingtième, supplément
hebdomadaire pour enfants du journal belge Le Vingtième Siècle. La
dernière parution dans ce journal date du 9 mai 1940 – jour où les


publiée dans Cœurs vaillants. À partir du 3 septembre
1932, elle l'est également, avec quelques modifications, dans
l'hebdomadaire catholique suisse L’Écho illustré^

Aventures de Tintin reprend dans Le Soir jeunesse, supplément du
journal Le Soir. Hergé y commence Le Crabe aux pinces


d'édition du Soir jeunesse, conséquence du rationnement en papier, la
publication des Aventures de Tintin est directement intégrée dans les
pages du journal Le Soir. Cette publication s'y poursuivra
jusqu'au 1^er septembre 1944, date à laquelle débute la


d'interdiction de publication, Les Aventures de Tintin sont
prépubliées dans le Journal de Tintin avec Le Temple du


Les Aventures de Tintin sont aussi publiées en 24 albums, dont un
inachevé, aux éditions Casterman. Elles se terminent avec la mort



Tintin illustrant le thème de la plongée dans les couloirs du
Centre sportif de Blocry à Louvain-la-Neuve avec des scènes
tirées de l'album Le Trésor de Rackham le Rouge.


sert pour mêler son personnage à plusieurs événements d'actualité : la
révolution bolchevique en Russie, les prémices de la Seconde
Guerre mondiale, la conquête lunaire, etc. Hergé a créé autour de
Tintin un univers aux détails stylisés, mais réaliste. Il a obtenu cet
effet en s'inspirant d'une importante collection de photographies.

Les Aventures de Tintin suivent une trame très linéaire — une énigme

présente avec son sens de l'humour caractéristique. De plus, il y


l'attention est captée, s'attachent parfois plus qu'aux


Le dessinateur a également particulièrement bien compris les mécanismes
de la bande dessinée, en particulier concernant le rythme. Ce sens du
rythme est flagrant dans Les Bijoux de la Castafiore, album dont
l'action se déroule dans une atmosphère tendue, alors qu'il ne s'y
passe aucune action aventureuse. La publication en planches
hebdomadaires imposait en effet que chaque page fasse avancer l'action
suffisamment et se termine sur un point d'humour ou de surprise qui
donnait envie d'acheter la suite. Même si le dessin peut sembler
simpliste, voire caricatural pour les visages, il va devenir de plus en
plus précis sur les décors au fil du temps et des rééditions^.

Hergé a, dans les premiers temps, créé Les Aventures de Tintin en
improvisant, ne sachant pas à l'avance de quelle manière le héros se
sortirait de toutes ses mésaventures. Il n'a été amené à se documenter
et à prévoir ses scénarios qu'après avoir terminé Les Cigares du
pharaon. L'impulsion est venue de Zhang Chongren (Tchang
Tchong-jen, ou Tchang), un étudiant chinois qui, en apprenant
qu'Hergé allait envoyer Tintin en Chine pour sa prochaine aventure, l'a
incité à ne pas colporter les idées reçues qu'avaient les Européens de
l'époque. Hergé et Zhang ont ainsi travaillé ensemble sur l'épisode
suivant de la série : Le Lotus bleu, ouvrage repris dans les
100 livres du siècle, classement français des livres considérés comme
les cent meilleurs du XX^e siècle, établi au printemps 1999
dans le cadre d'une opération organisée par la Fnac et Le
Monde. Hergé y expose la manipulation conduisant à l'occupation de la
Mandchourie par le Japon et l'incapacité de la Société des Nations

Des événements extérieurs obligent également Hergé à effectuer d'autres
changements dans sa manière de créer ses bandes dessinées.

La Seconde Guerre mondiale et l'invasion de la Belgique par les
armées d'Hitler entraînent la fermeture du Petit Vingtième. Hergé
travaillait à ce moment-là sur Tintin au pays de l'or noir.
L'histoire est interrompue et ne sera reprise qu'en septembre 1948.
Malgré la raréfaction du papier, Hergé peut poursuivre Les Aventures de
Tintin en publiant, d'abord dans Le Soir-Jeunesse puis dans
Le Soir, Le Crabe aux pinces d'or, L'Étoile mystérieuse,
Le Secret de La Licorne et Le Trésor de Rackham le Rouge. De
1941 à 1944, ces mêmes histoires paraîtront en album. Paraissent
L'Île Noire et de L'Oreille cassée. Les Sept Boules de
cristal ne paraîtront que partiellement dans le journal Le Soir.
Enfin, les versions de Tintin au Congo et Tintin en Amérique
en néerlandais paraissent dans le journal Het laatste Nieuws.

Pendant et après l'occupation allemande, Hergé est accusé par les
autorités d'épuration d'être un collaborateur, car le journal Le
Soir était contrôlé par les nazis, et il est brièvement incarcéré
qu'il avait tout simplement fait son métier pendant l'Occupation,
comme l'auraient fait un plombier ou un charpentier. Les histoires nées
durant cette période, contrairement à sa production d'avant et
d'après-guerre, sont dans l'ensemble politiquement neutres, sans
référence avec la situation de l'Europe en guerre. Néanmoins, Le
Sceptre d'Ottokar dénonce bien le déroulement d'un Anschluss,
commandité par un dictateur nommé Musstler (contraction de

Par ailleurs, l'apocalyptique album L'Étoile mystérieuse
traduit la peur de l'avenir éprouvée par l’auteur durant cette époque
de guerre et distingue, dans sa version originale, un groupe ami de
scientifiques issus de pays neutres ou occupés par l'Allemagne, d'un
groupe concurrent conduit par un banquier juif américain. Mais les
histoires sont souvent tournées vers les voyages, l'aventure et la
chasse aux trésors, comme dans Le Secret de La Licorne, Le
Trésor de Rackham le Rouge ou Les Sept Boules de cristal.

La pénurie de papier née de la guerre entraîne un changement de format
des Aventures. Hergé avait pour habitude de donner à ses albums le
nombre de pages nécessaire au développement de ses scénarios en
produisant 2 planches contenant chacune 6 cases réparties en 3 lignes
de 2 colonnes. Petit à petit, la place réservée aux aventures se réduit
jusqu'à se limiter à un simple strip de 4 à 5 cases. Le 5 février
1942^, la maison d'édition Casterman persuade Hergé de
passer à la couleur grâce aux machines offset que possède
l'imprimeur. Mais cela suppose de dessiner des planches plus petites et
d'adopter une longueur de 62 pages par album. En effet, un album est
constitué de 4 cahiers de 16 pages, soit 64 (62 + page de titre +
verso). Hergé agrandit donc son équipe (les dix premiers albums ont été
conçus par lui-même et sa femme), qu'il finit par regrouper en studio.

L'adoption de la couleur permet à Hergé de donner une plus grande
envergure à son œuvre. Sa manière de l'utiliser est plus subtile que

rendues à l'impression, permettant l'emploi de la quadrichromie
et, de ce fait, une approche cinématographique de la lumière et des

pour remplir des demi-pages ou tout simplement pour détailler et mettre
en avant une scène. L'emploi de la couleur fait ressortir les détails
importants. Hergé insiste sur ce point en affirmant : « Je considère
mes histoires comme des films. Donc, pas de narration, pas de
description. Toute l'importance, je la donne à l'image. »^

La vie personnelle d'Hergé a également influencé la série. Par exemple,
Tintin au Tibet est fortement marqué par sa dépression ; ses
cauchemars, qu'il aurait décrits comme étant « tout blancs », trouvent
un écho dans les paysages enneigés de l'album. L'intrigue est basée sur
les recherches menées par Tintin pour retrouver Tchang, qu’il
avait rencontré dans Le Lotus bleu. Cet épisode ne met en scène
aucun bandit, et Hergé, qui s'abstient de tout jugement de valeur, se
refuse à qualifier l'homme des neiges (le yéti) « d'abominable ».

Hergé aura aussi dénoncé dans ses ouvrages l'exploitation des minorités
le trafic d'armes et les dictatures en Amérique du Sud (L'Oreille
cassée, Tintin et les Picaros) et en Europe (L'Affaire


Les Aventures de Tintin se sont terminées avec la mort d'Hergé le 3
mars 1983. La vingt-quatrième aventure, Tintin et l'Alph-Art, est
restée inachevée. Dans cet album, Tintin évolue dans le monde de l'art
moderne, et l'histoire se termine sur une scène où il risque la mort,
enfermé dans du plexiglas et exposé comme une œuvre d'art.



Les neuf premiers albums ont d'abord été publiés en noir et blanc.
Hergé, à l'aide de son équipe, a fait par la suite une version en
couleurs de tous ces premiers albums (qu'il a redessinés et dont il a
plus ou moins modifié le scénario), à l'exception de Tintin au
pays des Soviets ; L'Île Noire a même fait l'objet d'une troisième
version. Bien qu'initialement publiés en couleur, les albums
L'Étoile mystérieuse et Tintin au pays de l'or noir ont
politiquement : publiée pendant la Seconde Guerre mondiale, la première
version de L'Étoile mystérieuse a pu être interprétée
comme une œuvre de propagande en faveur de l'Axe Rome-Berlin,
tandis que la première version de Tintin au pays de l'or noir fait
explicitement référence au conflit israélo-palestinien.

Les dates mentionnées ci-dessous sont celles de la première édition en
album. Les trois premiers albums ont été publiés aux éditions du
Petit Vingtième, à Bruxelles, et les autres, chez Casterman,
les 13^e et 14^e albums, la couleur est d’Edgar P. Jacobs.

Parmi ces vingt-quatre albums, seul Tintin et l'Alph-Art ne fut


Les 22 albums canoniques (de Tintin au Congo à Tintin et les Picaros)
représentent au total 15 000 cases et 1 364 planches^.


désirait traiter la problématique des Indiens d'Amérique avec
des éléments plus sérieux que dans Tintin en Amérique.

Greg de lui écrire un scénario. Celui-ci a finalement été
abandonné, Hergé préférant la liberté de créer seul ses histoires.
Greg, du projet des Pilules, reprenant la trame de ce dernier.
dizaine de planches crayonnées ont été dessinées.
locaux parisiens de Qantas, compagnie aérienne australienne,
Jacques Bergier, qui a inspiré le personnage de Mik
Ezdanitoff, propose à Hergé un sujet le remettant en scène : « On
apprendrait un jour que Tournesol a remplacé Einstein à
l'université de Princeton, et qu'il a là une chaire de
sémiologie, la science de la science, la science de
l'expression. Je présenterais le professeur Tournesol en lui
apportant mon hommage, et ce pourrait être le point de départ de
nouvelles aventures à la découverte de la science

projet d'aventure se déroulant uniquement dans un aéroport,
fréquenté par un bon nombre de personnages pittoresques. Le
scénario prévoyait que la lecture pouvait commencer à n'importe
quelle page de l'album et s'achever 61 pages plus loin^.



film en prises de vues réelles homonyme. Constitué de photos
extraites du film, suivi d'une version BD entièrement dessinée

prises de vues réelles homonyme (sorti le 18 décembre 1964).
Constitué de photos extraites du film, suivi d'une version BD

broché de 50 pages, édité par Publiart (Guy Decissy) et
Casterman, adapté du court-métrage d'animation homonyme produit par
Belvision la même année (cases extraites du film^).
d'animation homonyme. Scénario de Greg. Les cases des planches
sont extraites du film. Il existe une version entièrement dessinée

Steven Spielberg, adapté du film homonyme et constitué de photos







Scénario : Jacobs, Jacques Martin, Bob de Moor, Roger Leloup -






Tintin est un jeune reporter belge qui se trouve mêlé à des affaires
dangereuses dans lesquelles il passe héroïquement à l'action pour
sauver la mise. Pratiquement toutes les aventures montrent Tintin
accomplissant avec enthousiasme ses tâches de journaliste
d'investigation, mais, à l'exception du premier album, on ne le voit
jamais en train d'écrire des articles. C'est un jeune homme adoptant
une attitude plus ou moins neutre ; il est moins pittoresque que les
seconds rôles de la série. À cet égard, il est à l'image de
Monsieur-tout-le-monde (Tintin signifie d'ailleurs littéralement en


Milou, un fox-terrier blanc, est le compagnon à quatre pattes de
Tintin. Ils se sauvent régulièrement l'un et l'autre de situations
périlleuses. Milou « parle » fréquemment au lecteur par l'intermédiaire
de ses pensées (affichant souvent un humour pince-sans-rire),
lesquelles sont censées ne pas être entendues par les autres
personnages. Comme le capitaine Haddock, Milou adore le whisky
Loch Lomond. Les quelques fois où il en boit lui attirent des
ennuis, tout comme le fait sa violente arachnophobie. Le nom de
Milou est généralement considéré comme une référence indirecte à un
amour de jeunesse de Hergé, Marie-Louise Van Cutsem, dont le surnom

On peut expliquer autrement les origines des deux personnages. Certains
ont prétendu que Robert Sexé, un reporter-photographe dont les
exploits ont été racontés dans la presse belge du milieu à la fin des
années 1920, avait inspiré le personnage de Tintin. Il est célèbre pour
sa ressemblance avec ce dernier, et la Fondation Hergé a reconnu qu'il
n'était pas difficile d'imaginer que les aventures de Sexé aient pu
influencer Hergé. À ce moment-là, Sexé avait parcouru le monde sur une
moto fabriquée par Gillet et Herstal. René Milhoux est un champion et
recordman de moto de l'époque. En 1928, alors que Sexé était chez
Herstal en train de parler de ses projets avec Léon Gillet, Gillet le
mit en contact avec son nouveau champion, Milhoux, qui venait de
quitter les motos Ready pour l'équipe Gillet-Herstal. Les deux hommes
se lièrent rapidement d'amitié et passèrent des heures à parler de
motos et de voyages, Sexé demandant à Milhoux de lui transmettre ses
connaissances sur la mécanique et les motos poussées au-delà de leurs
limites. Grâce à ce mélange d'érudition et d'expérience, Sexé a mené un
grand nombre de voyages à travers le monde ; il en a publié de nombreux


Le secrétaire général de la fondation Hergé a admis qu'on pouvait
facilement imaginer que le jeune Georges Remi ait pu être inspiré par
les exploits médiatisés des deux amis, Sexé avec ses voyages et ses
documentaires, et Milhoux avec ses victoires et ses records, pour créer
les personnages de Tintin, le fameux journaliste globe-trotter, et de


Le psychanalyste Serge Tisseron émet l'hypothèse qu'enfant George
Remi avait apprécié le roman Sans famille d'Hector Malot dont le
héros est un jeune garçon appelé Rémi et qui possède un petit chien




La partie centrale du château de Cheverny a inspiré le


Le capitaine Archibald Haddock, un commandant de marine à l'ascendance
incertaine (peut-être d'origine anglaise, française ou belge), est le
meilleur ami de Tintin. Il apparaît pour la première fois dans Le
Crabe aux pinces d'or. Haddock est initialement dépeint comme un
personnage instable et alcoolique, mais il est devenu plus respectable
par la suite. Il se transforme en véritable héros sur la piste du
trésor de son ancêtre François de Hadoque, dans Le Secret de
La Licorne et Le Trésor de Rackham le Rouge, et même en
personnalité mondaine lors de l'exposition de ce trésor dans le château
de son ancêtre. Le côté humain et bourru, les sarcasmes du capitaine
viennent tempérer l'héroïsme incroyable de Tintin. Il est toujours
prompt à asséner un commentaire tranchant à chaque fois que le jeune
reporter semble trop idéaliste. Le capitaine Haddock vit dans le
luxueux château de Moulinsart. Haddock emploie une palette colorée
d'insultes et de jurons pour exprimer sa mauvaise humeur, tels que :
expression qui soit réellement considérée comme une grossièreté.

Haddock est un buveur invétéré, amateur inconditionnel de whisky
Loch Lomond. Ses moments d'ivresse sont souvent utilisés pour provoquer


Hergé affirmait que le nom de famille de Haddock était inspiré d'un
l'aiglefin fumé – ou haddock – qu'il appréciait
particulièrement^. Haddock est resté sans prénom jusqu'au
dernier album complet paru, Tintin et les Picaros, où le prénom



Article détaillé : Liste des personnages des Aventures de Tintin.

Les principaux personnages secondaires des Aventures de Tintin sont :
dur d'oreille, est un personnage d'importance secondaire — mais
récurrent — aux côtés de Tintin, de Milou et du capitaine Haddock.
Il est apparu pour la première fois dans Le Trésor de Rackham
le Rouge. Tournesol est en partie inspiré d'Auguste Piccard
principaux, sa nature généreuse et ses compétences scientifiques
lui ont permis de nouer des liens durables avec eux, en particulier

n'ayant apparemment aucun lien de parenté (du moins, c'est ce que
laisse à penser le fait qu'ils n'ont pas le même nom), semblent
de leur moustache^. Ils contribuent en grande partie au
comique des Aventures de Tintin par leur tendance chronique à faire
des contrepèteries et leur incompétence flagrante. Les deux
détectives sont inspirés, entre autres, du père et de l'oncle
d'Hergé, des jumeaux qui portaient tous les deux un chapeau melon


D'autres personnages jouent aussi un rôle de façon plus ou moins





Hergé s'est représenté lui-même dans certains de ses albums. On le
reconnaît facilement à des signes distinctifs : une silhouette grande
et maigre, une chevelure blonde, courte et abondante, un visage long et


Les apparitions d'Hergé dans Les Aventures de Tintin^ sont les


palais royal (p. 38) et parmi les invités à la cérémonie de




Les créateurs du dessin animé ont tenu à garder ce concept et ont fait



Les paysages représentés dans Tintin ajoutent de la profondeur aux
vignettes dessinées par Hergé. Il y mélange des lieux réels et
imaginaires. Le point de départ de ses héros est la Belgique, avec
dans un premier temps le 26, rue du Labrador, puis le château de
Moulinsart. Le meilleur exemple de la créativité d'Hergé en la
matière est visible dans Le Sceptre d'Ottokar où Hergé invente
deux pays imaginaires (la Syldavie et la Bordurie) et invite
le lecteur à les visiter en insérant une brochure touristique au cours


Hergé a donc dessiné plusieurs milieux différents (des villes, des
déserts, des forêts et même la Lune), mais, pour amplement démontrer le
talent d'Hergé, on notera trois grands espaces : la campagne, la mer et




En 1946-1947, les albums sont traduits en néerlandais à
destination du lectorat flamand et des Pays-Bas ; depuis 1947, les
albums paraissent simultanément en français et néerlandais. En 1952,
Tintin est traduit en anglais, espagnol et en allemand : Casterman
souhaite développer les éditions internationales. « Le succès ne sera
pas au rendez-vous, la diffusion restera confidentielle et cette
tentative ne connaîtra pas de lendemain sauf en allemand »^.
En 1958, une nouvelle tentative de traduction est lancée, en Espagne,
Royaume-Uni et États-Unis ; les premières traductions en portugais
sortent au Brésil en 1961. En 1960, plusieurs albums de TIntin sont
traduits en suédois, danois, puis en finnois l'année suivante.
Les couvertures des albums des Aventures de Tintin dans de nombreuses


Le succès international de Tintin arrive véritablement dans les années
1960. De nouvelles traductions paraissent en italien, grec, norvégien
malais, indonésien, iranien, hébreu... « Depuis lors, le succès est au
rendez-vous et la diffusion ne s'est plus arrêtée »^. Plus de
2700 titres ont ainsi été publiés, toutes langues confondues^.
En Asie, des traductions pirates de Tintin sont imprimées, notamment en

Turquie (où, dès 1950, des feuillets de Tintin paraissent dans des
magazines)^. Ce phénomène est devenu marginal car Casterman a
conclu de nombreux accords avec les différents éditeurs pirates afin de
régulariser la situation^. En Iran, les éditions officielles
se sont arrêtées après la révolution islamique de 1979, laissant


En 2001, une traduction en mandarin de l'album Tintin au Tibet
provoque une polémique : le titre était « Tintin au Tibet chinois », ce
qui a provoqué les vives protestations de Fanny Rodwell, qui
menace de cesser toute collaboration avec l'éditeur chinois^.
10 000 exemplaires sont imprimés, avant que l'album ne soit retiré de
la circulation^ et réédité sous son titre original^.

Tintin est également traduit dans de nombreuses langues régionales
de France ou de Belgique. La plupart des traductions en sont réalisées
par des associations culturelles, qui se chargent du financement et de
la promotion de l'album avec l'aval de Casterman^.

En 2009, depuis 1929, plus de 230 millions d'albums de Tintin ont
les Aventures de Tintin ont été traduites en plus de 100


Les noms des personnages ont également été traduits ou adaptés
selon les langues, pour des raisons de prononciation^ et pour





Afrikaans (1973) - Allemand (1952) - Anglais américain

Catalan (1964) - Chinois mandarin (2001) - Cingalais

Indonésien (1975) - Islandais (1971) - Italien (1961) -

Letton (2006) - Lituanien (2007) - Luxembourgeois (1987)
Norvégien (1972) - Persan (1971) - Polonais (1994) -
Portugais (1936) - Portugais brésilien (1961) - Romanche

Alghero (1995) - Allemand (Bernois, 1989) - Alsacien (1992) -
Anversois (2008) - Asturien (1988) - Basque (1972) -
Borain (2009) - Bourguignon (2008) - Breton (1979) -
Bruxellois (2007) - Bruxellois (2004) - Cantonais (2004)
Flamand (Ostende, 2007) - Francoprovençal (Bresse)
Francoprovençal (unifié, 2007) - Frison (1981) -


Néerlandais (Hasselts, 2009) - Monégasque (2010)^ -
Néerlandais (Twents, 2006) - Occitan (1979) - Picard
Provençal (2004) - Tahitien (2003) - Vosgien (2008) -


Wallon (Ottignies) (2006) - Français québécois (2009).



Hergé mène ses premières recherches documentaires approfondies pour
l'album Le Lotus bleu, ce qu'il confirme lui-même : « C'est à cette
intérêt pour les gens et les pays dans lesquels j'envoyais Tintin,
accomplissant une sorte de devoir de crédibilité auprès de mes
lecteurs. » La documentation d'Hergé et son fonds photographique l'ont
aidé à construire un univers réaliste pour son héros. Il est allé
jusqu'à créer des pays imaginaires et à les doter d'une culture
politique qui leur était propre. Ces contrées fictives sont largement
inspirées par les pays et les cultures de l'époque d'Hergé.

Pierre Skilling affirme qu'Hergé voyait la monarchie comme « une forme
légitime de gouvernement », remarquant au passage que « les valeurs
démocratiques semblent absentes dans ce type de bande dessinée
classique franco-belge »^. La Syldavie, en particulier, est
décrite avec beaucoup de détails, Hergé l'ayant dotée d'une histoire,
de coutumes et d'une langue qui est en fait du dialecte flamand
bruxellois. Il situe ce pays quelque part dans les Balkans et il
s'inspire, de son propre aveu, de l'Albanie. Le pays se retrouve
agressé par sa voisine, la Bordurie, qui tente de l'annexer dans
Le Sceptre d'Ottokar. Cette situation rappelle évidemment celle de
la Tchécoslovaquie ou de l'Autriche face à l'Allemagne


On peut citer à titre d'exemple les mois de préparation nécessaires à
Hergé pour imaginer l'expédition lunaire de Tintin, décrite en deux
parties dans Objectif Lune et On a marché sur la Lune. Ces
travaux ont conduit à la réalisation d'une maquette détaillée de la
fusée lunaire, permettant de placer sans erreur les personnages dans le
décor. Les recherches préalables à l'élaboration de son scénario ont
considérables entreprises par Hergé lui ont permis de créer une tenue
spatiale très proche de celle qui serait utilisée pour les futurs
voyages lunaires, même si sa fusée était bien différente de ce qui a
existé par la suite. »^ Pour cette dernière, Hergé s'est



Hergé admirait, dans sa jeunesse, Benjamin Rabier. Il a avoué que
de nombreux dessins de Tintin au pays des Soviets reflétaient
cette influence, en particulier ceux représentant des animaux. Le
travail de René Vincent, le dessinateur de mode de la période
Art déco, a également eu un impact sur les premières aventures de
Tintin : « On retrouve son influence au début des Soviets, quand mes
dessins partent d'une décorative, une ligne en S, par exemple (et le
personnage n'a qu'à se débrouiller pour s'articuler autour de ce
S !). »^ Hergé reconnaîtra aussi sans honte avoir volé l'idée
des « gros nez » à l'auteur de bandes dessinées américain George
McManus : « Ils étaient si drôles que je les ai utilisés sans


Au cours des nombreuses recherches qu'il a menées pour Le Lotus bleu,
Hergé a également été influencé par le dessin chinois et japonais, et
par les estampes. Cette influence est particulièrement visible
dans les paysages marins d'Hergé, qui rappellent les œuvres de


Hergé a aussi reconnu que Mark Twain l'avait influencé, même si
son admiration l'a conduit à se tromper en montrant des Incas ne
sachant pas ce qu'était une éclipse solaire, lorsque ce phénomène
a lieu dans Le Temple du Soleil. T. F. Mills a rapproché cette
erreur de celle de Mark Twain décrivant des « Incas craignant la fin du
monde dans Un Yankee à la cour du roi Arthur »^.


Certains critiquent les premières Aventures de Tintin, considérant que
celles-ci contiennent de la violence, de la cruauté envers les animaux,
des préjugés colonialistes et même racistes, présents entre autres dans
la description qui y est faite des non-Européens. Néanmoins, beaucoup
considèrent ces critiques comme étant totalement anachroniques.

Tintin paraît à l'origine dans le journal Le Petit Vingtième. Même
si la Fondation Hergé met ces éléments sur le compte de la naïveté
de l'auteur et que certains chercheurs comme Harry Thompson prétendent
que « Hergé faisait ce que lui disait l'abbé Wallez (le directeur
du journal) »^, Hergé lui-même sent bien que, vu ses origines
sociales, il ne peut échapper aux préjugés : « Pour Tintin au Congo,
tout comme pour Tintin au pays des Soviets, j'étais nourri des préjugés
du milieu bourgeois dans lequel je vivais.  Si j'avais à les
refaire, je les referais tout autrement, c'est sûr. »^

Dans Tintin au pays des Soviets, les bolcheviques sont dépeints
comme des personnages maléfiques. Hergé s'inspire du livre de Joseph
Douillet, ancien consul de Belgique en Russie, Moscou sans voile, qui
est extrêmement critique envers le régime soviétique. Il remet
cela dans le contexte en affirmant que, pour la Belgique de l'époque,
nation pieuse et catholique, « tout ce qui était bolchevique était
athée »^. Dans l'album, les chefs bolcheviques ne sont motivés
que par leurs désirs personnels, et Tintin découvre, enterré, le
attribué les défauts de ce premier album à « une erreur de
jeunesse »^. Mais aujourd'hui, avec la découverte des archives
soviétiques, sa représentation de l'URSS, bien que caricaturale,
possède quelques éléments de vérité. En 1999, le journal The Economist,
de tendance libérale, écrira que « rétrospectivement, la terre accablée
par la faim et la tyrannie dépeinte par Hergé était malgré tout

On reproche à Tintin au Congo de représenter les Africains
comme des êtres naïfs et primitifs. Dans la première édition de
l'album, on voit Tintin devant un tableau noir, donnant la leçon à des
enfants africains. « Mes cher amis, dit-il, je vais vous parler
aujourd'hui de votre Patrie : la Belgique. » En 1946, Hergé redessine
l'album et transforme cette leçon en un cours de mathématiques. Il
s'est par la suite expliqué sur les maladresses du scénario original :
l'époque : « Les Nègres sont de grands enfants… Heureusement pour eux
que nous sommes là ! », etc. Et je les ai dessinés, ces Africains,
d'après ces critères-là, dans le plus pur esprit paternaliste qui était


L'auteur Sue Buswell résume en 1988, dans le journal britannique Mail
on Sunday, les problèmes posés par cet album, en soulignant deux
référence à la manière dont sont dessinés les Africains dans l'album,
et aux animaux qui y sont tués par Tintin]. »^ Néanmoins,
Thompson pense que cette citation est mise « hors de son
contexte »^. L'expression « animaux morts » est une allusion à
la chasse au gros gibier, très en vogue à l'époque de la première
d'André Maurois Les Silences du colonel Bramble (1918), Hergé
présente Tintin comme un chasseur de gros gibier, abattant quinze
antilopes, alors qu'une seule serait nécessaire pour le dîner. Ce
nombre important d'animaux tués conduit l'éditeur danois des Aventures
de Tintin à demander quelques modifications à Hergé. Ainsi, une planche
où Tintin tue un rhinocéros en perçant un trou dans le dos de
l'animal et en y insérant un bâton de dynamite est jugée excessive.
Hergé la remplace par une autre planche montrant le rhinocéros
accidentellement touché par une balle du fusil de Tintin, alors que ce
chasseur d'une autre époque est embusqué derrière un arbre.

En 2007, un organisme britannique, la Commission pour l'égalité
raciale, demande, à la suite d'une plainte, que l'album soit retiré des
rayonnages des librairies, en affirmant : « Cela dépasse l'entendement
qu'à notre époque, un vendeur de livres puisse trouver acceptable de
vendre ou faire la promotion de Tintin au Congo .»^ Le 23
juillet 2007, une plainte est déposée par un étudiant de
République Démocratique du Congo à Bruxelles, celui-ci
estimant que l'ouvrage constitue une insulte envers son
peuple^. En réaction, une institution belge, le Centre
pour l'égalité des chances et la lutte contre le racisme, met en garde
contre « une attitude hyper-politiquement correcte »^ dans ce
dossier. Le 5 décembre 2012 la cour d'appel de Bruxelles rend son
jugement : elle considère que l'album ne contient pas de propos
racistes et que « Hergé s'est borné à réaliser une œuvre de fiction
dans le seul but de divertir ses lecteurs. Il y pratique un humour
candide et gentil », confirmant ainsi le jugement de première instance


Plusieurs des premiers albums de Tintin ont été remaniés pour être
réédités, le plus souvent à la demande des maisons d'édition. Par
exemple, à la demande des éditeurs américains des Aventures de Tintin,
la plupart des personnages noirs de Tintin en Amérique ont été
recoloriés pour devenir blancs ou d'origine indéterminée^.
Dans L'Étoile mystérieuse, on trouvait à l'origine un « méchant »
américain nommé Monsieur Blumenstein (un patronyme juif), ce qui était
tendancieux, d'autant plus que le personnage avait un faciès
correspondant exactement aux caricatures de Juifs. Hergé attribue par
la suite à son personnage un nom jugé moins connoté – Bohlwinkel – et
le fait habiter dans un pays sud-américain imaginaire, le São Rico.
Hergé a découvert bien plus tard que Bohlwinkel était également un nom



La société Moulinsart SA est chargée de la gestion et de la
perception des droits des œuvres d'Hergé depuis 1987. Fondée par sa
femme en qualité de légataire universelle, cette société est
actuellement dirigée par Nick Rodwell, qui en est le gestionnaire
délégué. Très regardante sur la gestion des droits moraux et sur leur
perception financière, la société provoque régulièrement la controverse
en interdisant toute utilisation d'une image de Tintin sans son


Ainsi, à l'heure d'internet, toute forme de parodie, détournement ou
réutilisation est fortement combattue par la société Moulinsart SA. De
même, le fait de poster une simple bulle d'une BD de Tintin est
réprimandé selon les lois des différents pays^.

Cette attitude inquiète certains passionnés de culture et de BD
franco-belges qui se demandent comment ils vont pouvoir faire en sorte
que Tintin reste dans le patrimoine culturel francophone si la
diffusion de son image est aussi fortement restreinte dans les médias


Cependant, une décision rendue par la Cour de La Haye au début de
l'année 2015 a esquissé un changement. La justice néerlandaise a donné
raison à l'association Hergé Genootschap, qui s'était vu réclamer
plusieurs dizaines de milliers d'euros par Moulinsart SA pour
l'utilisation de vignettes originales dans ses publications, après y
avoir longtemps été autorisée. En effet, la Cour a retenu que la
légataire universelle d'Hergé, Fanny Rodwell, n'avait jamais remis en
cause un contrat de 1942 stipulant explicitement la cession de
l'intégralité des droits sur les textes et vignettes des albums d'Hergé
précédent dans la jurisprudence, ouvrant ainsi la porte à des
réparations éventuelles de Moulinsart SA envers les associations ayant
dû verser des sommes d'argent pour l'utilisation d'une ou plusieurs



L'œuvre d'Hergé entrera dans le domaine public le 1^er janvier 2054.
Mais Moulinsart SA, qui perdrait ainsi de substantielles sources de
revenus, envisage d'en empêcher l'utilisation et la publication par
tous en publiant une nouveauté Tintin en 2052. Cette opération ferait
face à des limites juridiques et à un certain problème moral^.


Les Aventures de Tintin ont été diffusées dans de nombreux médias venus
s'ajouter à la bande dessinée originale. Certaines sont des œuvres
originales, d'autres des adaptations. Hergé était favorable aux
adaptations de Tintin, et il encourageait ses équipes à participer à
des projets d'animation de la série. Après sa mort, les studios
Hergé sont devenus la seule institution habilitée à donner son accord



Boullock a disparu, pièces originales écrites par Hergé et

Tintin en Amérique et jouée par le Unicorn Theatre Company du
18 décembre 1976 au 20 février 1977 au Unicorn Theatre Arts Centre,

Case, adaptée de l'album L'Île Noire et mise en scène par Tony
Wredden, jouée à Londres. Une version française a été produite au

Christian Rauth, jouée au théâtre de Poche-Montparnasse

homonyme, mise en scène par Dominique Catton et jouée à

Frank van Laecke, musique de Dirk Brossé, adaptée de l'album Le
Temple du Soleil,avec Tom Van Landuyt (Tintin). Créée à Charleroi
en 2002 sous le titre Tintin : Le Temple du Soleil dans une

Tintin au Tibet et Le Lotus bleu, mise en scène par Rufus
Noriss et jouée par la Young Vic Theatre Company au Barbican
Theatre pendant les périodes de Noël, avec Russell Tovey


Entre 1959 et 1963, la radiodiffusion-télévision française
présente un feuilleton radiophonique des Aventures de Tintin de près de
500 épisodes, produit par Nicole Strauss et Jacques Langeais. Adaptés
sous la forme de disques 33 tours, ces épisodes rencontrent un


En 2015, une adaptation des Cigares du pharaon est coproduite par
France Culture, Moulinsart SA et la Comédie-Française. Elle
est diffusée en février 2016 sur les ondes de France Culture. Quatre
autres adaptations sont annoncées : Le Lotus bleu, Les 7 Boules de
cristal, Le Temple du Soleil et L’Affaire Tournesol^.


Tintin a été adapté au cinéma, à la fois en prises de vues réelles
et en animation. Certains films sont cependant des œuvres originales ;
c'est le cas pour Tintin et le Mystère de la Toison d'or, Tintin et les

image par image de Claude Misonne, adapté de l'album
homonyme. Le film ne fut projeté qu'une seule fois au cinéma ABC de

d'invités. À la suite de la faillite du producteur Wilfried
Bouchery, le film fut saisi. Il y a quelques années, il a été

prises de vues réelles de Jean-Jacques Vierne avec
Jean-Pierre Talbot (Tintin) et Georges Wilson (Haddock).
Tintin et Haddock sont à Istanbul et sont menacés
par une organisation turque voulant s'emparer du bateau La Toison
d'or que l'ami du capitaine Haddock, Témistocle Paparanic, lui a

vues réelles de Philippe Condroyer avec Jean-Pierre
Talbot (Tintin) et Jean Bouise (Haddock). Tintin et le
capitaine Haddock sont à la recherche du professeur Tournesol,
victime d'un enlèvement à la suite de sa découverte sur des oranges

d'animation de Raymond Leblanc produit par Belvision,


Raymond Leblanc, produit par Belvision. Sur un scénario original de
Greg, les héros tentent de démasquer une bande de malfrats
voulant s'emparer de la dernière invention de Tournesol.
En 1983, Steven Spielberg prend une option sur les droits de
Tintin peu avant la mort d'Hergé. Cependant, il n'était à ce
moment-là pas certain qu'il réalise lui-même l'adaptation au
cinéma, d'où le refus d'Hergé de signer un quelconque
contrat^. En novembre 2002, Dreamworks achète les
droits cinématographiques de tous les albums et décide de produire
une trilogie en images de synthèse et capture de mouvement.
Les Aventures de Tintin : Le Secret de La Licorne, réalisé par
Steven Spielberg et adapté des albums Le Secret de La
Licorne, Le Crabe aux pinces d'or et Le Trésor de Rackham
le Rouge, sort en 2011. L'acteur britannique Jamie Bell prête
son jeu et sa voix à Tintin. Deux autres films sont prévus, sans
date de sortie annoncée pour le moment. Le premier pourrait être
réalisé par Peter Jackson et adapté des albums Les Sept
Boules de cristal et Le Temple du Soleil^, tandis que
le second pourrait être coréalisé par Steven Spielberg et Peter


Les photos des films sortis ont été reprises dans plusieurs albums et
sous forme de strips pour Le Lac aux requins (cf. ci-dessus).


prévu, censé se dérouler en Inde, mais fut finalement annulé.
refit surface. Plusieurs réalisateurs furent pressentis puis
démentis, notamment Jaco Van Dormael, Jean-Pierre Jeunet,
Roman Polanski, tous trois tintinophiles avérés. Si dans
la plupart des cas il s'agit avant tout de rumeurs, Jeunet fut
réellement intéressé par le projet, mais en 2002 il annonça qu'il y
renonçait : « Le verrouillage des héritiers d'Hergé rend tout trop
compliqué, je les ai rencontrés et j'ai compris qu'ils allaient me










pilotage spatial pour Atari ST (cassette et disquette),
Amiga et Commodore 64, ainsi que sur la console Amstrad

pour Super Nintendo puis sur Game Boy (1995) ainsi que
sur GameBoy Color en 2001. Jeu de plates-formes (Infogrames).








Il existe des magasins consacrés à Tintin. Ils sont appelés « The
Tintin Shop ». Le premier a ouvert en 1984 à Londres (Covent
Garden) et les autres sont situés à Bruxelles (La Boutique de
Tintin), Bruges, Toulouse, Cheverny et Montpellier.
On peut y acheter tous les livres dans une variété de langues, des
T-shirts, des tasses et beaucoup d'autres choses à thème.


Articles connexes : Pastiches et éditions pirates de Tintin et


Il existe plusieurs parodies de Tintin, dont Tintin en Suisse, publiée
aux éditions Sombrero, qui utilise des mots obscènes et de la
pornographie^. La série Les Aventures de Saint-Tin et son
ami Lou, initiée par Gordon Zola et complétée par Bob Garcia
et Pauline Bonnefoi, est également un pastiche de la bande dessinée.

Tintin est aussi parodié dans le film d'animation Le Chat du





La compagnie aérienne belge Brussels Airlines a signé un
partenariat avec Moulinsart SA afin de parer l'un de ses avions de
l'image du héros d'Hergé. L'Airbus A320 immatriculé OO-SNB,
rebaptisé Rackham pour l'occasion, a ainsi reçu une livrée unique
montrant Tintin et Milou dans une scène extraite de l'album Le
Trésor de Rackham le Rouge, qu'il portera de 2015 à 2019. À bord,
l'album Le Trésor de Rackham le Rouge est mis à la disposition des
passagers en français, néerlandais et anglais^^,^.

