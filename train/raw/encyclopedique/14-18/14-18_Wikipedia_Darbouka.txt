
Elle est traditionnellement faite en terre cuite ou céramique, mais des
versions en métal (aluminium) ou plus rarement en bois sont
apparues du fait de sa fragilité. D'une taille moyenne de 30 à 60 cm de
hauteur pour 15 à 40 cm de diamètre, elle se décline en des tailles
très variables. Elle est recouverte d'une peau animale (chèvre ou
poisson) ou de plastique. Les premières nécessitent d'être chauffées
avant utilisation. Dans les pays africains, certaines personnes
utilisent le sable (qui absorbe l'humidité) pour tendre la peau des


Alors que les corps en céramique sont souvent considérés comme
produisant le meilleur son, les corps métalliques et en peaux
synthétiques sont généralement préférés par les professionnels, du fait
de leur solidité (donc longévité) et de l'indépendance de leur sonorité
vis-à-vis des conditions climatiques (humidité). De plus, les darboukas
en matériaux modernes produisent un son plus puissant et plus clair, ce
qui les rend plus attrayantes dans les musiques modernes. Enfin, elles









Jeune fille jouant de la darbouka à Constantine (Algérie).

La darbouka se joue en général debout, l'instrument étant soit placé
sous le bras gauche, soit placé sur l'épaule gauche, mais la position
assise est plus confortable pour des techniques plus complexes.
L'instrument se place en position horizontale à cheval sur la jambe
gauche, le coude gauche bloquant l'arrière du corps de la darbouka
contre la jambe, le bord de la paume de la main gauche épousant le bord
de la tête de l'instrument, laissant les doigts libres pour frapper la
peau. L'axe de la main droite doit être à peu près perpendiculaire à
celui du bras qui repose sur l'instrument. En se fiant au cadran d'une
horloge, dans le cas des joueurs droitiers, la main droite doit être
placée à neuf heures, et la main gauche à midi. Les deux bras et
poignets doivent être souples, légers voire un peu lâches pour arriver

En pratique, les drebkis (joueurs de darbouka) utilisent des
instruments différents, tantôt plus traditionnels, tantôt plus
modernes, en fonction du contexte musical et du timbre désiré. La
darbouka accompagne en effet les musiques les plus variées : sacrées,
savantes, traditionnelles, folkloriques et modernes.

Concernant les techniques de frappes, il en existe trois de base et de
nombreuses autres dépendant du style régional et du type de son


l'aide du majeur (ou de l'annulaire pour des ornements
complémentaires), le but étant de toucher la peau le plus à
l'extérieur possible pour obtenir un son bref et aigu ;
la main gauche au bord de la peau. Le « S » équivaut également à un
silence. Il peut être joué ou pas. Les bases de rythme étant jouées
avec la main forte (en général la droite), le « S » revient donc à
orner un rythme et ne fait donc pas sa signature ;
droitiers) est effectué avec l'ensemble des doigts regroupés, et
légèrement pliés à la manière d'une « gifle », appelé KEF ou tak




Il existe plusieurs techniques de jeu, qui se rejoignent sur certains
points, mais qui permettent de distinguer les écoles arabes des écoles
turques. Il y a en outre des variations régionales importantes, comme
en Égypte (où l'instrument est appelé tabla). Ainsi dans les
techniques de roulements et ras, figures de style importantes, les
coups devant être très rapides, il y a une certaine technique à
appliquer pour éviter les crispations... Tout est basé sur la façon de
doubler les notes de chaque main qu'il est utile d'étudier car n'ayant
pas la même sonorité, le jeu en sera d'autant plus « coloré » :
du contrôle stick des batteurs), mais comme il ne s'agit pas de
baguettes, on peut imaginer la souplesse extrême que ça demande,
car il faut sentir le poids de ses doigts (voir Hossam


frappes étant assurées alternativement par l'index et l'annulaire
ou le petit doigt. Le majeur est donc l'axe du mouvement. En
pratique, il faut s'imaginer tenant un tournevis et dévissant
horizontalement. Gardant ce mouvement de poignet, on déplie ses
doigts souplement et on tente de taper sur la peau en balançant la
main de gauche à droite (index en premier). Là aussi, il faut être
rapide mais détendu et sentir le poids de ses doigts (voir


Chaque pays a ses rythmes préférés et sa manière propre de les jouer et
de les composer avec d'autres percussions ou instruments mélodiques. On
pourra remarquer aussi une différence de style entre un musicien turc
et égyptien, mais aussi entre un musicien populaire et un autre plus
académique. Certains musiciens bulgares utilisent aussi une fine


Les rythmes les plus connus sont Masmoudi, Maksoum, Malfouf, Zindali,



D'autres percussions orientales très populaires sont souvent jouées
avec la darbouka : le bendir (tambour sur cadre), le riqq




















