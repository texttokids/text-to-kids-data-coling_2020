
attestée dès 1223 faire la madeleine, c'est-à-dire affecter le
repentir avec une extériorisation feinte à but d'émouvoir ses juges
ou ses pairs. Pleurer comme une madeleine, une expression populaire
attestée dès 1833, mais probablement antérieure, est synonyme de

bouches profanes ou hypocrites a gardé longtemps une connotation de
péché de chair ou de prostitution. Le dictionnaire
d'Antoine Furetière édité en 1690 nomme Madelonnette une femme

passage de l'œuvre de Marcel Proust où le goût d'une madeleine






o L'agglomération urbaine des Îles-de-la-Madeleine,
territoire équivalent à une Municipalité régionale de






longue, qui se trouve à Cayenne en Guyane française.



une culture préhistorique du Paléolithique supérieur.


Loire-Atlantique, situé sur les communes de Guérande et













Alpes-Maritimes, désormais partie intégrante de la commune de

désormais partie intégrante de la commune de Bergerac.

Meurthe-et-Moselle, désormais partie intégrante de la commune

fusionnée en 2000 avec la commune de Bagnoles-de-l'Orne (cette








Madeleine », un temple napoléonien emblématique du










de Marie-Madeleine ou Marie de Magdala, personnage du Nouveau

Jeanne de Bourbon, mère de la reine de France Catherine de


Marie-Thérèse, grande maîtresse de l'Ordre de la Croix




Pour les articles sur les personnes portant ce prénom, consulter la



Madeleine est également un nom de famille français, localisé
essentiellement en Normandie (Calvados et Manche).




principal du roman Les Misérables de Victor Hugo.


de belle séductrice ou de repentante. La chanson et le cinéma
continuent cette veine bien au-delà de cette tradition :




folklorique se déroulant tous les ans le dimanche le plus près du
22 juillet (jour où l'on fête Sainte Marie-Madeleine) dans la






1. ↑ Charles Sadoul, « La cuisine lorraine », Le Pays
Lorrain, Imprimerie-Éditeur Léon Heck, Art Graphique Nancy,
vol. 28^e année, n^o 1,‎ janvier 1936 (lire en ligne),

2. ↑ Lucile Escourrou, « À la recherche de la madeleine perdue »,

3. ↑ S. G. Sender et Marcel Derrien, La Grande Histoire de la
pâtisserie-confiserie française, Minerva (rachat de Minerva par La


