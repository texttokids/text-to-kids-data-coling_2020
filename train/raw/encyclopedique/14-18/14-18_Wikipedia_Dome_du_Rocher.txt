
Le Rocher de la Fondation à l'intérieur de l'édifice.

Comme cela est noté dans l'une des inscriptions en arabe qui
courent dans le bâtiment, le dôme du Rocher est construit en l'an 72 de
l'Hégire, c’est-à-dire en 691 ou 692 de l'ère
chrétienne, sous le règne d'Abd al-Malik^^,^. Il
s'élève sur le Haram al-Charif. Le premier Temple des Juifs
aurait été édifié à cet endroit par le roi biblique Salomon, et le
second, au VI^e siècle av. J.-C., agrandi au I^er siècle av.
J.-C. par Hérode I^er le Grand. L'emplacement exact du temple
n'est plus connu avec certitude. Il existe plusieurs opinions
différentes à ce sujet (le temple serait plus au nord, ou plus au sud
selon une étude de l'architecte Tuvia Sagiv^). Ce temple
est finalement détruit en l'an 70 de l'ère chrétienne, sur ordre de
Titus. l'esplanade était restée inoccupée, jalonnée seulement de


Le calife et compagnon de Mahomet Omar ibn al-Khattab, aurait
nettoyage et y aurait prié. Selon la même source, il ordonne la
construction d'une mosquée à cet emplacement. Certains historiens
médiévaux, en particulier le chroniqueur byzantin Théophane
le Confesseur indique que cette action de Omar ibn al-Khattab est


En plus du Dôme, le Haram al-Charif, appelé aujourd'hui
al-Silsila). Haram al-Charif signifie en langue arabe « le Noble


de nombreuses reprises. Dès le début du IX^e siècle, le calife
abbasside Al-Mamun faisait ainsi effacer le nom d'Abd
al-Malik pour le remplacer par le sien sur l'inscription^.
Ensuite, chaque dynastie maîtresse de Jérusalem depuis les
Fatimides jusqu'aux Ottomans a cherché à poser sa marque sur
l'édifice, tout en conservant sans doute le plan et les proportions


Durant les Croisades du XII^e siècle, l'édifice est
transformé en église sous le nom de « Templum Domini »^,
tandis que la mosquée al-Aqsa toute proche est transformée en
palais par Baudouin de Boulogne. Les deux monuments sont tous deux
rendus au culte musulman en 1187 après la prise de


Néanmoins, de nombreux éléments ont été remplacés, dans les
mosaïques intérieures, où l'on note en particulier des
restaurations mamelouks maladroites, dans la coupole, de
nombreuses fois reconstruite, ou encore dans les plafonds peints, dont
les motifs peuvent être datés du XIII^e siècle. Cependant, c'est
sans doute le décor extérieur qui est le plus marqué par ces
restaurations : au milieu du XVI^e siècle (deux dates sont
inscrites, équivalent à 1545 et 1551/1552), sur ordre de Soliman
le Magnifique, il a été complètement remplacé par un revêtement de

XVIII^e siècle et le début du XX^e siècle, le monument a fait
l'objet d'au moins quatre campagnes de restauration : en 1720-1721 à la
demande du sultan Ahmed III ; en 1817 pour Mahmoud II ; dans
le troisième quart du XIX^e siècle (1853-1874), à l'initiative
d'Abdülmecid, mais terminée par Abdülaziz ; entre 1918 et


Monument majeur de l'art islamique, le dôme du Rocher a très tôt fait
l'objet d'études. Dès 1900, l'archéologue suisse Max van Berchem a
relevé les inscriptions^, et Marguerite van Berchem
publie une étude sur les mosaïques en 1932 dans l'ouvrage de KAC
Creswell, Early muslim architecture^, qui lui-même propose une
analyse approfondie du monument. Mais le scientifique qui s'est le plus
penché sur le monument est sans contestes Oleg Grabar, qui publie
ses premières hypothèses sur sa signification en 1959. Ses articles
constituent le plus important corpus sur ce sujet, sur lequel de








Le dôme du Rocher est situé sur une plate-forme artificielle
rectangulaire ouverte par huit escaliers, deux sur les côtés sud et
ouest, un sur les flancs nord et est. Situé un peu à côté du centre de
cette estrade, il suit un plan centré autour du point focal qu'est le
Ce plan se décompose en un premier anneau au centre constitué par une
première colonnade autour du Rocher, supportant la coupole, cernée
d'une seconde, octogonale. Ces deux colonnades définissent un premier
déambulatoire, tandis qu’un second se situe entre la seconde
colonnade et les murs extérieurs, eux aussi à huit pans. L'édifice est
ouvert par quatre portes, donnant en direction des quatre points
cardinaux, l'une -celle qui regarde vers la mosquée Al-Aqsa, et
donc vers la qibla- étant magnifiée par un portique plus important



Ce plan n'est pas nouveau : il s'inspire visiblement de ceux des
martyria, communs dans l'architecture paléochrétienne et
byzantine, dont il reste de rares vestiges comme Santa
Costanza et Saint-Étienne-le-Rond à Rome. L'ancien
Saint-Sépulcre à Jérusalem, à la fin du VII^e siècle, était
essentiellement constitué d'une rotonde autour du tombeau du Christ,
comme l'indique le pèlerin Arculfe, bien qu'assez différent, c'est
plan central dans cette ville. Le baptistère du Latran de Rome et
la basilique Saint-Vital de Ravenne présentent un plan octogonal.
Des parallèles peuvent également être tissés avec les vestiges de
martyria disparus comme ceux de Capharnaüm ou Césarée, ou
l'église de la Kathisma de la Vierge, près de Jérusalem^.

Sous le Rocher se trouve une grotte, qui possède deux mihrabs et dont
la forme originelle est impossible à définir étant donné les nombreuses




Cette section ne cite pas suffisamment ses sources (août 2019).
Pour l'améliorer, ajoutez des références vérifiables [comment
faire ?] ou le modèle {{Référence nécessaire}} sur les passages


Deux types de supports sont utilisés : des colonnes de marbre de
remploi coloré (marbre bleu notamment) et des piliers maçonnés. Pour la
colonnade octogonale, la proportion est de deux colonnes reliées par
des arcs surhaussés entre chacun des huit piliers ; dans celle
circulaire, ce sont trois colonnes qui scandent l'espace entre quatre
piliers, elles supportent des arcs en plein cintre à claveaux
de couleurs alternées. Chaque colonne est surmontée d'un chapiteau à
feuilles d'acanthe et des tirants de bois les relient, pour plus
de solidité. Cette bande de poutres est continue et se place entre les
chapiteaux et les arcs pour les colonnes et entre les piliers et


Le mur extérieur est percé de nombreuses fenêtres à claustras ; on
en compte sept sur les pans non-ouverts par une porte. Les portes,
quant à elles, sont marquées par deux colonnes soutenant une grande
arcade pour trois d'entre elles et par huit colonnes alignées, dont les
deux centrales soutiennent aussi une arcade, pour celle qui regard vers
la qibla. Seize fenêtres à claustras se retrouvent également sur le
tambour du dôme central. Les doubles grilles qui meublent toutes ces
ouvertures, tant sur le mur extérieur que sur la façade du tambour,
datent de la période ottomane. Les fenêtres devaient être
originellement en dentelle de marbre à l'intérieur et en fer forgé à






Deux types de couvrements se combinent dans le monument. Au centre, une
coupole surmonte le Rocher, située sur un haut tambour à deux étages,
un plein en partie inférieure et un percé de seize fenêtres dans la
zone supérieure. Elle s'élève à 25 m de hauteur pour un diamètre de
20 m et est constituée de deux coques de bois, dont l'extérieure est




Selon Al-Muqaddasi (vers 985) et Al-Wâsitî un peu plus
tard^, la coupole avait originellement été dorée avec
l'or qui restait en surplus après la construction, mais il y a tout
lieu de croire que ce récit n'est qu'une légende. Dans la toile
d'Auguste de Forbin, Vue de Jérusalem, prise de la vallée de
Josaphat, présentée au salon de 1831, le dôme est nettement
doré^, de même que sur une peinture de Vassili Polenov
datée de 1882^ et sur une de Charles Fouqueray datée
1919^ ; cependant, une peinture de Salomon Bernstein
datée de 1928^ et des photos datant de la deuxième moitié du
XIX^e et de la première moitié du XX^e siècle le montrent
sans dorure^, comme le sont encore actuellement les dômes
d'al-Aqsa ou du Saint-Sépulcre. Il aurait été redoré en

de celle des toits du déambulatoire ou de la coupole elle-même,


Au-dessus des déambulatoires, c'est un toit en pente qui assure le
couvrement. Il est caché à la vue par un haut parapet qui surmonte la
façade extérieure. Contigu au tambour du dôme, il s'attache juste
au-dessous des fenêtres à claustra dont celui-ci est percé. À
l'intérieur, le plafond est daté du XIII^e siècle. La couverture
des toits pentus à l'origine en plomb^, est désormais en



Le décor intérieur du dôme du Rocher est de trois types : des tirants
de bois couverts de bronze, des placages de marbres sur les murs
et les piliers et des mosaïques à fond d'or dans les parties
hautes (écoinçons, soffites), que les restaurations n'ont
visiblement pas altérées et qui s'étalent sur plus de 280 m2. Le décor
devait d'ailleurs être plus ou moins identique à l'extérieur^,
mais a été remplacé par des céramiques polychromes de très belle
qualité à la période ottomane^. On ne sait pas si les
mosaïques recouvraient toute la surface ou étaient organisées en
bandeaux ou en panneaux. Par contre, le revêtement de marbre extérieur




Du point de vue de l'iconographie, on note l'absence totale de
représentations figurées : ni humains, ni animaux ne sont représentés
dans les mosaïques. On trouve par contre de nombreux motifs végétaux
réels et imaginaires, rosettes), et, à l'intérieur des colonnades
circulaire et octogonale et du tambour du dôme, des bijoux sous la
forme de couronnes sassanides et byzantines, de pectoraux et


La symbolique du décor du dôme a soulevé bien des questions et
interprétations ; il semble en effet, étant donné son agencement et son
iconographie, que celui-ci n'avait pas pour seule vocation d'être
ornemental et chatoyant. Oleg Grabar pense que les bijoux peuvent être
interprétés comme des symboles de nations défaites suspendus comme des
trophées sur les murs (spolia)^, ainsi qu'on en trouvait à la
Kaaba, mais beaucoup d'autres lectures, contradictoires ou

Décor de mosaïque : rinceaux végétaux et vase incrusté de perles

On a souvent rapproché les mosaïques du dôme de mosaïques chrétiennes,
comme celles de la Basilique de la Nativité de Bethléem, par
exemple. En effet, il est assez probable que leurs réalisateurs soient
des artistes chrétiens ou musulmans récemment convertis,
et formés dans des traditions chrétiennes ou juives. Les mosaïques
tirent d'ailleurs leurs motifs de l'antiquité tardive. Toutefois, on
remarque une adaptation au modèle musulman, notamment dans la
disparition de la figuration, ce que l'on retrouvera quelques années

Damas^. Oleg Grabar note par ailleurs la naissance dans
le dôme du Rocher de deux grands principes spécifiques à l'art
islamique : l'utilisation de formes réalistes à des fins non-réalistes
variation infinie sur un même thème, en arrangeant différemment des
motifs semblables. De plus, contrairement aux bâtiments de l'antiquité
classique, le décor du dôme n'est pas subordonné à l'architecture et ne
cherche pas à mettre en valeur la structure du bâtiment, mais au
contraire couvre tout le bâtiment, comme pour créer une atmosphère
particulière, un lieu unifié sans architecture réellement






Le dôme du Rocher constitue le premier bâtiment où se déploie un
programme d'inscriptions mûrement réfléchi. Trois sont umayyades :
une, longue de 240 m, se situe au-dessus des arches de la colonnade
octogonale extérieure, les deux autres se trouvant sur les portes est
et nord. Il s'agit à chaque fois d'inscriptions religieuses issues du
Coran, mis à part le nom du commanditaire, Abd al-malik

Lues depuis 1900^, elles font principalement référence à la
grandeur et à l'unicité de Dieu, s'attardent sur les missions
prophétiques et notamment le rôle de Jésus comme prophète,
font allusion au paradis. On peut y voir une affirmation de la
grandeur de l'islam à la fois en direction des nouveaux


montré aussi les tenants eschatologiques afin d'appuyer sa thèse
selon laquelle le dôme du Rocher est une préfiguration de la
Jérusalem céleste^. Karl-Heinz Ohlig contredit ces
interprétations en considérant que les inscriptions du Dôme du Rocher
montrent l'absence d'une religion musulmane indépendante aux
VII^e-VIII^e siècle, estimant au contraire qu'il s'agit alors d'une



Cette section ne cite pas suffisamment ses sources (août 2019).
Pour l'améliorer, ajoutez des références vérifiables [comment
faire ?] ou le modèle {{Référence nécessaire}} sur les passages



Architecture, plan, matériaux (les colonnes, avec leurs chapiteaux et
leurs bases, sont récupérés des ruines de l'esplanade), méthodes de
construction et techniques de décor puisent grandement dans le
vocabulaire de l'Antiquité tardive méditerranéenne, notamment à
Byzance et à Rome. Il s'y ajoute déjà une influence persane
sassanide, notamment dans la géométrisation rigoureuse, le jeu des
surfaces planes, ainsi que dans le décor extérieur d'origine, comme les
palmettes ailées. Cette association entre l'art byzantin et l'art
persan sassanide formera la base de l'art islamique, inauguré par le



De ce fait, on remarque qu'aucun bâtiment paléochrétien ni
byzantin ne ressemble exactement au dôme du Rocher. Celui-ci puise
certes son inspiration dans la tradition chrétienne préislamique, mais
y ajoute d'autres influences et un vocabulaire formel propre pour
arriver à un monument typique de l'islam. Ceci se remarque tout d'abord
dans la géométrisation simplifiée et rigoureuse de l'édifice : les
côtés mesurent autant que le diamètre de la coupole, et chaque point de
l'édifice dérive du rocher central. Quelques éloignements sont à noter,
notamment dans la disposition des colonnes, mais ils obéissent à une
considération visuelle : l'observateur peut voir le Rocher à quelque
endroit qu'il se trouve, son regard n'étant jamais bloqué par une
colonne ni un pilier. La conception du décor, aniconique, avec de
longues inscriptions arabes et des bijoux est aussi nouvelle, même
s'ils prennent souvent la place d'éléments anciens tels que les


Enfin, il faut noter la grande richesse dans les coloris des mosaïques,
dont il reste peu d'exemples de l'Antiquité tardive et du début de
la période byzantine, comme celles des monuments de Ravenne, elles
s'en différencient notamment par l’absence de représentation



Il est difficile à l'heure actuelle de connaître précisément les
facteurs qui poussèrent le calife omeyyade Abd al-Malik à
commander la construction du dôme du Rocher, dont la fonction précise
n'est pas déterminée par sa forme ni par ses inscriptions. Plusieurs
facteurs, à la fois politiques et symboliques, peuvent être cités.


Dès le IX^e siècle, on trouve dans les sources l'idée que le
calife souhaitait détourner vers Jérusalem le hajj, pèlerinage
rituel de La Mecque, alors occupée par un rival, Ibn
az-Zubayr. Cette hypothèse est mentionnée^ par l'historien
d'Alexandrie, Eutychius (mort en 940), qui s'appuient
sur des sources visiblement différentes. Néanmoins, la plupart des

cette explication est fausse, et donnent à cela plusieurs raisons :
l'absence de mention d'un détournement du pèlerinage dans la plupart
des sources historique (notamment Tabari, Baladhuri et
Maqadasi), et le « suicide politique » que ce changement radical dans
le dogme musulman aurait constitué pour Abd al-Malik, déjà mal
considéré sur le plan religieux par son appartenance à la famille
umayyade. De plus, on trouve, dans un texte de Baladhuri, la
preuve que le pèlerinage s'est poursuivi visiblement sans problèmes
durant les problèmes politiques qui avaient alors lieu. Autre élément
important, la pratique du pèlerinage à Jérusalem semble assez difficile
pour des raisons d'espace^. Enfin, si l'on accepte la thèse de
Sheila Blair selon laquelle le dôme a été construit dans la seconde
partie de l'année 692, les dates ne concordent plus, et l'empire se
trouvait alors dans un moment d'apaisement après la victoire du général


Ce dernier fait peut offrir une seconde lecture du bâtiment, monument
de victoire de la dynastie umayyade^. Par la même occasion, le
dôme célèbre aussi l'Islam triomphant, au centre d'une ville
majoritairement chrétienne et à forte communauté juive. Le
dôme aurait ainsi mis en valeur la victoire de l'islam, complétant la
révélation des deux autres religions monothéistes, et aurait permis à
l'État nouveau de rivaliser en magnificence avec les grands sanctuaires
chrétiens de Jérusalem et de Syrie. Plusieurs arguments appuient
cette interprétation : la taille du dôme, sa position théâtrale dans la
ville et son ancien revêtement brillant, de céramiques à fond d'or
prouvent qu'il était fait pour être vu de loin. De plus, son plan
centré, donne l'impression que le monument irradie dans toutes les
directions, concourant également à un effet scénique^.
Toujours selon Oleg Grabar, le programme d'inscriptions peut être lu
comme un manifeste de la supériorité de l'islam sur le christianisme,
quoique tous les chercheurs, notamment Myriam Rosen-Ayalon, ne soient
pas tout à fait d'accord avec cette interprétation. Les bijoux et les
couronnes ornant l'intérieur du bâtiment seraient alors des trophées,
peut-être en référence à ceux accrochés autour de la Kaaba. Cet
usage serait corroboré par les poésies et les déclarations officielles


Le choix du lieu lui-même est extrêmement symbolique : lieu sacré
juif, où restent encore des ruines des temples hérodiens, laissé à
l'abandon pendant des siècles puis par les chrétiens pour marquer leur
triomphe sur cette religion, il est à nouveau utilisé sous l'Islam,
marquant alors la victoire sur les Chrétiens et, éventuellement, une
continuité avec le judaïsme. D'ailleurs, comme l'a montré Priscila
Soucek, le lieu est associé à David et à Salomon, deux
souverains exceptionnels dans la tradition biblique, dont le prestige
est censé rejaillir sur le calife qui s'installe sur leurs traces.
Enfin, l'historien Al-Maqdisi, au X^e siècle, écrit que le
dôme a été réalisé dans le but de dépasser le Saint-Sépulcre, d'où


De cette analyse on a pu conclure que le dôme du Rocher peut être
considéré comme un message des Omeyyades en direction des chrétiens,

récemment convertis (attirés par les déploiements de luxe des églises
chrétiennes) pour marquer le triomphe de l'Islam. Mais ce n'est pas la
seule hypothèse, Dr Milka Levy-Rubin propose une troisième théorie,
selon laquelle le dôme du Rocher avait été construit comme une
transformation et en continuation du Temple de Jérusalem mais
aussi en conséquence à la rivalité avec Constantinople et sa
byzantins et la reconnaissance de Constantinople comme « nouvelle
Jérusalem » servent de motifs à la construction du dôme du Rocher. Le
culte juif y était pratiqué jusqu'à a la fin du VII^e siècle
lorsque le sanctuaire prend un caractère essentiellement



D'autres explications, plus symboliques et pas forcément
contradictoires, ont été avancées après l'analyse des traditions liées

Dans la Genèse (22-2), le mont Moriah est désigné comme le site du
sacrifice d'Isaac : « Dieu dit : prends ton fil unique que tu
aimes, Isaac ; et rends-toi au pays de Moriah, où tu l'offriras en
holocauste sur une des hauteurs que je t'indiquerai. » Le texte
précise que le trajet dura trois jours : « Le troisième jour en levant
les yeux, il vit l'endroit de loin ». Le Temple de Salomon
fait reconstruire au même endroit. Des restes archéologiques assez
importants, en particulier le mur des Lamentations, témoignent
encore de ce passé. Néanmoins, dans la Bible, le Rocher appelé
aussi « Rocher de la Fondation » n'est jamais mentionné, et ne
semble pas jouer un rôle prépondérant dans le Temple.

Des traditions situent donc à cet endroit la Ligature d'Isaac ou
situer également sur le rocher le lieu depuis lequel Dieu quitta la
Terre après la Création pour retourner au ciel^. Une
coutume plus tardive, même si le Coran ne mentionne jamais Jérusalem,
associe aussi le Rocher à l'isra, le voyage nocturne de
Mahomet, et au miraj, son ascension, durant laquelle il
aurait visité les sept cieux et reçu de Dieu les cinq prières
journalières de l'islam. Le rattachement de ces événements à Jérusalem
apparaît assez tôt, dès le VIII^e siècle dans les textes, mais ce
n'est que vers le XII^e - XIII^e siècle que les sources
islamiques mentionnent réellement le Rocher comme point de départ du
miraj. Cet amalgame n'existait probablement pas au temps de la
construction du dôme, quoiqu'il ait pu être ancré bien plus tôt dans


Une autre analyse a été fournie par Myriam Rosen Ayalon^ qui,
après avoir étudié de manière détaillée les inscriptions coraniques et
les décors de mosaïque, estime que le dôme avait une vocation
paradisiaque et eschatologique, et devait être considéré comme une
sorte de préfiguration de la Jérusalem céleste. Cette thèse,
existe déjà chez al-Watisi au XI^e siècle, qui, dans sa
description du dôme, fait usage de métaphores bibliques à vocation
apocalyptique. Plusieurs parallèles ont été établis, dont un avec
le saint-Sépulcre de plan semblable, et qui possède en son centre,
outre le tombeau du Christ, un rocher, comme le dôme. L'eau
représentée dans les mosaïques et dans les veines du marbre, la forme
octogonale du bâtiment, le rocher qui pourrait rappeler le tombeau du
Saint-Sépulcre par sa disposition, les quatre portes
constitueraient ainsi autant de références au paradis. Oleg Grabar note
d'ailleurs que, dès 70, c’est-à-dire juste après la destruction du
temple d'Hérode, s'était développé un pèlerinage à vocation
eschatologique. Récemment, Carolanne Mekeel-Matteson a développé cette
lecture eschatologique du dôme, et sa vocation de lieu de
pèlerinage^. Priscilla Soucek, quant à elle, associe le dôme
au Temple et surtout au palais de Salomon, réputé dans la
tradition coranique pour ses richesses (d'où les bijoux et les
couronnes). Elle estime que, dans une vision plus large de la lecture
coranique du mythe de Salomon, on peut identifier ce palais au


Edouard-Marie Gallez dans sa thèse Le Messie et son
prophète^, fournit une autre hypothèse. Les guerriers arabes
qui prennent Jérusalem sont accompagnés de « judeonazaréens » qui
les ont convertis à leur croyance. Les judeonazaréens sont des
judeo-chrétiens messianiques persuadés que la reconstruction du Temple,
détruit par les Romains en 70, déclenchera le retour du
peuple élu du nouveau royaume. De ce fait, dès la prise de
Jérusalem, les prêtres judeonazaréens se hâtent de construire un cube
en pierre et en bois^ aux mêmes dimensions que le Saint
des saints du Temple juif initial^, pour y pratiquer les rites
qui doivent faire revenir le messie. Le cube sera ensuite remplacé par
une mosquée, le Dôme du rocher, sous Abd-Al-Malik.


Le Rocher abrite une grotte, à laquelle on accède par un escalier.
Attestée comme mosquée en 902-903, elle est pourvue d'un
mihrab dont la datation fait débat : K.A.C. Creswell, suivi par
Klaus Brisch et Géza Féhévari, estime qu'il est contemporain du dôme,
mais Eva Baer, sur des critères stylistiques, a remis en cause cette
datation, estimant que l'œuvre ne peut dater d'avant le
IX^e siècle, et qu'elle aurait été commandée par un membre de la


En 1911, le capitaine Montagu Brownlow Parker, jeune officier
britannique animé par la recherche du « trésor de Salomon », entreprit
de creuser clandestinement dans cette grotte après avoir tenté durant
deux ans d'atteindre le dôme par un système de souterrains ; mais
rapidement découvert, il dut s'enfuir. Cet incident donna lieu à une
véritable crise diplomatique^‌, et plus tard, à de nombreuses






Son accès est interdit aux non-musulmans alors que jusqu'en 1998, il
leur était autorisé s'ils étaient munis d'un billet d'entrée.

Le 12 mai 2009, Benoît XVI se rend au Dôme du Rocher^,


Jusqu'au milieu du XIX^e siècle, l'accès à la zone était également
interdit aux non-musulmans. À partir de 1967, ceux-ci se sont vu
accorder un droit d'accès restreint, mais les prières non musulmanes


En 2000, la visite considérée comme provocatrice du Premier Ministre
israélien Ariel Sharon sur l'Esplanade des Mosquées déclencha
la Seconde Intifada. Cet évènement entraîna le retour de


En 2006, la zone fut rouverte aux visiteurs non musulmans, sauf le
vendredi et pendant les jours fériés pour les musulmans. En 2009, la
zone est toujours ouverte aux non musulmans, mais l'accès aux mosquées


Israël n'interfère pas sur la gestion de l'Esplanade des Mosquées,
laissée en 1967 au Waqf, fondation religieuse islamique contrôlée
par la Jordanie, mais depuis ces dernières années, se réserve d'en
interdire l’accès occasionnellement aux moins de 50 ans pour des

Panorama de l'Esplanade des mosquées avec la mosquée al-Aqsa et le




