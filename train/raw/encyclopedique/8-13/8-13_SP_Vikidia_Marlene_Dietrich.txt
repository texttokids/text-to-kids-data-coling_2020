
Marlène Dietrich (sans l'accent en anglais et en allemand), née le 27 décembre 1901 à Berlin  et morte le 6 mai 1992 (à 91 ans) à Paris, est une actrice et chanteuse allemande naturalisée américaine.




Marlène Dietrich naît le 27 décembre 1901. Sa mère est Josefine von Loschand1 et elle a une sœur aînée, Elisabeth, née en 1900. Elles sont les filles d'un officier prussien (la Prusse est un pays qui, depuis, a été intégré à l'Allemagne). Son prénom, Marlène (ou Marlene), est la contraction de Marie Magdalene, qui est son prénom officiel. Son père meurt quand elle a 7 ans2. Sa mère se remarie avec un officier de cavalerie qui se trouvera être un excellent père adoptif pour les deux sœurs.
Pendant son enfance, elle développe son talent pour le violon et le chant. Elle commence à prendre des cours de violon3. Son beau-père meurt à son tour, en 1916, pendant la Première Guerre mondiale. Elle continue ses études à Weimar.


Elle effectue ses premières études entre 1907 et 19194, d'abord à l'école pour filles Auguste-Viktoria jusqu'en 19175, puis au lycée Victoria-Luise-Schule dont elle est diplômée en 19186. Elle commence à apprendre le théâtre en 1921. Jusqu'en 1922, c'est-à-dire avant d'être connue, elle vit en faisant des concerts de violon7 et des représentations de chant dans des bars8. En 1922, elle débute une carrière théâtrale au théâtre de Berlin9. Elle obtient son premier rôle mineur au cinéma en 19234.
Le 17 mai 1923, elle épouse Rudolf Sieber, un réalisateur de films américain4,10.
À la fin des années 1920, elle enregistre ses premiers albums de musique.
En 1929, elle tourne pour la première fois un rôle important au cinéma dans le film allemand L'Énigme4. La même année, en octobre, elle passe une audition pour L'Ange bleu4. Le 1er avril 1930, le film sort au Gloria Palast, à Berlin4. Son rôle dans ce film fait d'elle une actrice célèbre. Malgré cela, l'importance de ses rôles demeure variable par la suite11.
Le 2 avril 1930, le lendemain de la première de l'Ange Bleu, Marlène Dietrich quitte l'Allemagne pour l'Amérique4.




Après le succès de L'Ange bleu, qui est le premier film parlant allemand, Marlène Dietrich quitte l'Allemagne et s'installe à Hollywood. Le 14 novembre 1930 sort son premier film américain4 : Cœurs Brulés, qui lui vaut une présélection aux Oscars du cinéma. Dans les années 1930, elle connaît la gloire : elle enchaîne les réussites au cinéma, et écrit de nouvelles chansons qui deviennent de grands succès.
En 1935, le film La Femme et le Pantin sort en salles. C'est le dernier film réalisé par Josef von Sterberg dans lequel elle joue ; ce réalisateur a été le mentor de Marlène Dietrich ; il a réalisé L'Ange bleu et est ainsi à l'origine de sa gloire11.


Le 6 mars 1939, elle est naturalisée américaine ; elle prend la nationalité américaine pour faire comprendre qu'elle s'oppose au régime politique allemand : le nazisme12.
En 1939, la Seconde Guerre Mondiale commence ; elle s'engage alors à défendre les idées anti-nazies au travers de ses chansons. Pendant la guerre, elle rencontre à Hollywood l'acteur français Jean Gabin, avec lequel elle a une relation amoureuse pendant quelques années. Tous deux sont les vedettes du film Martin Roumagnac, tourné en France en 1946.
De janvier 1942 à septembre 1943, elle parcourt les États-Unis pour chanter devant les soldats américains, le plus souvent revenus blessés du combat13. Au cours des années 1944 et 1945, elle divertit des soldats américains en Europe et en Afrique du Nord (Algérie, Italie, Royaume-Uni et France) au plus près des lignes pour leur montrer son soutien en leur remontant le moral4,13.


En 1945, elle reçoit la Médaille de la Liberté de la part du gouvernement américain12, puis, en 1950, la Légion d'Honneur de la part du gouvernement français4,14. Elle monte deux fois en grade avec cette récompense : sous Georges Pompidou et sous François Mitterrand4.
En 1953, elle est recrutée par le Sahara Hotel, à Las Vegas15. Son salaire atteint alors 30 000 dollars (environ 23 000 euros) par semaine16. Son numéro, au cours duquel elle chante quelques-unes de ses chansons15, est très court mais est un grand succès, à tel point qu'elle est engagée par le Café de Paris (à Londres) en 1954, et que son contrat à Las Vegas est renouvelé17.
Au milieu des années 1950, elle engage Burt Bacharach pour la conseiller sur sa manière de chanter ; celui-ci lui recommande d'élargir son répertoire, de jouer sur l'aspect dramatique de son comportement sur scène et lui fait changer la manière d'utiliser sa voix18. Entre 1957 et 1964, ils enregistrent quatre albums qui deviennent rapidement très connus19. Son image est alors due à ses choix de vêtements, à son maquillage et aux perruques qu'elle utilise beaucoup20. Son personnage est ensuite mis en valeur grâce aux éclairages de la salle où elle joue17.
En 1960, sa carrière marque un tournant important. Cette année-là, elle retourne en Allemagne pour donner des concerts. Elle est très mal reçue car la plupart des allemands considèrent qu'elle les a trahis en devenant américaine21. Grâce à une autre partie de la population qui admire Marlène Dietrich, la tournée est un triomphe artistique, mais demeure un échec financier21. Elle profite de son succès pour abandonner le cinéma et se concentrer sur la chanson : entre 1957 et 1964, ce qu'elle écrit devient immédiatement mondialement reconnu, notamment grâce à Burt Bacharach19.


Dans les années 1960 et 1970, la santé de Marlène Dietrich décline : elle survit à un cancer du col de l'utérus en 196522 et souffre de problèmes de circulation sanguine20. Elle devient alors dépendante aux anti-douleurs et à l'alcool20.
En 1968, elle gagne un Tony Award, une grande récompense23.
En 1973, dans le Maryland (un état américain), elle fait une chute qui la blesse à la jambe gauche24. Toujours en 1973, elle annonce qu'elle ne continue sa carrière que pour l'argent25. L'année suivante, en août, elle se casse la jambe droite26. Elle effectuera sa dernière représentation dans un cabaret peu de temps après4.


En 1975, à Sydney, en Australie, elle fait une chute lors d'un concert, et se casse le col du fémur27. C'est cet évènement qui la décide à quitter la scène définitivement. Elle fera une dernière apparition dans un film, en 1978, alors qu'elle n'a pas tourné depuis 14 ans.
Après cet épisode, elle reste enfermée chez elle. En 1979, elle publie une autobiographie, Nur mein Leben (« Juste ma vie »)4.
Le 4 mai 1992, deux jours avant sa mort, elle a une hémorragie cérébrale. Elle est sauvée, mais le 6 mai, elle demande un somnifère à sa secrétaire. Elle meurt au cours de la nuit, pendant son sommeil4 ; elle s'est probablement suicidée en demandant délibérément une trop forte dose de somnifère. Mais c'est difficile à prouver et il est aussi possible qu'elle soit morte d'insuffisance rénale1. 1 500 personnes sont présentes à son enterrement, dont plusieurs ambassadeurs, le 14 mai à Paris1. Le 16 mai, son cercueil est finalement transporté à Berlin, où elle voulait reposer1.


Le 24 octobre 1993, ses biens sont transférés à la Deutsche Kinemathek (« cinémathèque allemande ») de Berlin4. Ils sont composés, entre autres, de 3 000 pièces de textile (dont ses costumes de scène), de 15 000 photographies d'elle, et de 300 000 pages de documents de toutes sortes28.
Le contenu de son appartement de Manhattan est vendu aux enchères le 1er novembre 199729. L'appartement lui-même est vendu en 1998 pour le prix de 615 000 dollars (environ 470 000 euros)30.


