
Bellérophon (en grec : Βελλεροφῶν) est un héros de la mythologie grecque. Il est le fils de Eurymone et de Poséidon ou Glaucos selon les versions. Si l'on estime qu'il est le fils de Glaucos, alors son grand-père serait Sisyphe. Il est aussi le roi de Tirynthe et est souvent représenté accompagné de Pégase, le célèbre cheval ailé. Son ennemi le plus grand est la Chimère.




Hipponoüs chasse avec son frère, Belléros. Cela fait 2 jours que tous deux cherchent à tuer un sanglier. C'est alors que Hipponoüs le voit, ce sanglier. Alors, il tire. Un cri déchire le silence. Et Belléros tombe, mort. Alors, Hipponoüs décide de changer de nom.
Il ne s'appellera plus Hipponoüs mais Bellérophon, ce qui veut dire « l'assassin de Belléros ».
Maintenant, comme le veut la loi, il doit quitter Corinthe, cette ville qui l'a vu grandir. Il doit s'exiler, jusqu'à ce qu'une ville lui offre son hospitalité. Alors, il sera purifié de son meurtre. Alors, il va voir Proétos, le roi de Tirynthe, car il pense que ce roi l'acceptera dans sa cité.


Arrivé là-bas, Proétos accepte de le recueillir, comme l'avait prévu Bellérophon. Le roi de Tirynthe lui présente toute sa cour, y compris sa femme, Sténébé.

Le soir même, Sténébé se glisse dans sa chambre et lui déclare son amour. Mais Bellérophon se refuse à elle et la prie de quitter sa chambre. La femme du roi se retire. Mais dans son regard se lit l'humiliation, et dans son cœur, l'amour s'est transformé en haine.
Le lendemain, Sténébé annonce publiquement que Bellérophon a essayé de la violer. 
Alors, pris au dépourvu, Proétos est pris dans un dilemme : trahir les lois de l'hospitalité (qui interdisent de tuer un hôte) ou laisser cette offense impunie... Alors, lui vient une idée : il demande à Bellérophon d'aller porter une lettre à Iobatès, son beau-père, roi de Lycie. Bellérophon accepte, heureux de s'en tirer à si bon compte.


Arrivé en Lycie, Bellérophon est accueilli par une fête. Durant les 9 premier jours de son séjour là-bas, ce n'est que danse et rires, théâtre et vin. Ce n'est que le 10e jour que Iobatès ouvre la lettre. Ce qu'il y découvre l'horrifie : 
Le roi de Lycie est embarrassé : il ne peut pas tuer un hôte.Alors, il lui propose un défi : un monstre nommé Chimère menace son royaume, il lui suffira de la tuer.Bellérophon accepte.


Il faut savoir que la Chimère était un monstre redoutable, dans l'antiquité, avec un corps et une tête de lion, un serpent pour queue ; une autre tête, mais cette fois de chèvre, sortait de son dos.Et en plus de cela, toutes ses têtes, y compris celle du serpent qu'elle avait pour queue, crachaient du feu.
Alors, Bellérophon va voir un devin pour qu'il lui prédise comment vaincre le monstre. Le devin lui dit : "La Chimère ne sera vaincue que par les airs."
Bellérophon n'a plus qu'une seule solution : il doit trouver Pégase, le mythique cheval ailé réputé indomptable, et le dompter.Comme le devin lui dit que Pégase vit avec les Muses, sur le mont Elycon, Bellérophon y va ; il parcourt routes et forêts, plaines et lacs. Mais nulle trace du cheval ailé. Alors, il continue de marcher, jusqu'à ce que ses pas le mènent près du temple d'Athéna, où il tombe de sommeil, épuisé par sa marche.
C'est ce moment que choisit Athéna pour lui parler, en rêve. Elle lui dit qu'il trouvera Pégase près de la fontaine de Pyrène car le cheval aime s'y abreuver. Elle lui explique aussi qu'elle lui a fait présent d'une bride en or qui lui permettra de dompter Pégase.
Effectivement, au réveil de Bellérophon, la bride est là. Alors, il va jusqu'à la fontaine de Pyrène.Le cheval ailé est là, comme l'avait annoncé Athéna. Bellérophon lui passe la bride et l'enfourche. Alors, le cheval se cabre sans le renverser et s'envole en direction de l'antre de la Chimère.


Bellérophon se bat, mais ses flèches ne blessent pas la Chimère, les coups de sabots non plus ne blessent pas le monstre. 
C'est alors que Bellérophon a une idée: il met un morceau de plomb à l'extrémité de sa lance, il la lance dans la gueule de l'animal, celui-ci crache du feu et fait fondre le plomb, qui s'écoule dans sa bouche et le tue.


Arrivé dans le palais de Iobatès, Bellérophon va annoncer la nouvelle à ce dernier : mais celui-ci ne manifeste pas sa joie et l'envoie tuer les Solymes, des guerriers sanguinaires. Bellérophon n'a plus peur depuis qu'il a accompli l'impossible : tuer la Chimère. Il enfourche donc Pégase, qui s'envole et l'emmène sur le champ de bataille.
Il y fait un massacre ; après le combat, ce ne sont partout que ruines et cadavres.
Iobatès commence à se poser des questions sur les origines de son hôte invincible : ne serait-il pas le fils de Poséidon, le dieu de la mer ? Mais pour un tel outrage, il doit mourir.
Alors que Bellérophon vient tout juste de rentrer, le roi de Lycie l'envoie combattre les Amazones, ces femmes guerrières filles d'Arès.
Encore une fois accompagné du fidèle cheval ailé, il bat les guerrières à plate couture.
Une fois arrivé chez Iobatès, celui-ci ne le remercie toujours pas et lui envoie ses gardes avec l'ordre non seulement d'arrêter Bellérophon mais en plus de le tuer...Pour Bellérophon c'en est trop : il tue les gardes et part en direction de la salle du trône avec la ferme intention de demander des explications à Iobatès. 
Le roi lui avoue tout, et lui montre la lettre. Puis, il lui dit qu'il est convaincu que cela est faux et lui accorde sa fille Philioné, puis le désigne comme son successeur. 
Mais, il lui faut punir Sténébé, celle qui a menti et qui est la cause de toutes ces épreuves. C'est Pégase qui s'en charge : le jour même, Sténébé quitte le royaume de son mari, découvrant que son stratagème a échoué. Pégase arrive au moment où elle sort de la ville. Il lui propose de monter sur son dos et s'envole. Mais, une fois dans les airs, il se cabre et hennit. Alors, Sténébé bascule et s'écrase, plusieurs dizaines de mètres en contrebas.


Bellérophon vit désormais comblé : sa femme, Philonoé, lui a donné trois enfants et il est désormais roi de Lycie. Mais Bellérophon est mécontent : ses exploits sont de plus en plus mis sur le compte de sa parenté avec Poséidon. Alors, un jour, sa colère explose : il détruit le temple de Poséidon en répétant : Les dieux ne sont rien, les dieux ne sont rien. Bellérophon a désormais l'impression d'être l'égal des dieux. Alors, se dit-il, pourquoi ne pas avoir ma place dans l'Olympe ?
Il enfourche Pégase et lui demande de le conduire sur l'Olympe. Celui-ci entame alors son ascension. Mais Zeus, qui, de son trône, a tout observé, lance un taon géant à l'attaque de Pégase. Le cheval ailé, excité par le taon de Zeus, se cabre et Bellérophon tombe. Il atterrit dans des buissons épineux, qui lui crèvent les yeux. Mais l'assassin de Belléros n'est pas mort car Zeus ne l'a pas voulu. Le roi des dieux a trouvé cela trop peu de choses par rapport à l'acte orgueilleux de Bellérophon. Alors celui-ci se relève et, aveugle et boiteux, il se met en route. Nul ne sait où il s'est rendu ni quand ni comment il a fini ses jours.




Bellérophon est symbolisé sur certaines pièces de monnaies antiques, ou sur des vases en céramique. Une partie d'un bois de cyprès lui est consacrée à Corinthe. Son combat contre la Chimère est lui aussi représenté à l'entrée su temple de Delphes, sur le trône d'Asclépios à Epidaure et sur celui d'Apollon à Amyclée.


- La légende de Bellérophon est présente dans la littérature antique : Homère la raconte dans l'Iliade, chant VI, vers 150-205
- et Pindare dans la treizième ode des Olympiques, vers 118-132.
- Dans La tempête de Shakespeare, le roi confie sa fille à un ennemi tout désigné malgré une vengeance.
- Jean de La Fontaine fait allusion à ce personnage au début de sa fable L'ours et l'Amateur des jardins :


Un papillon, nommé Eurytides bellérophon, vit en Amérique du Sud.


En 1679, le dramaturge Thomas Corneille, frère de Pierre Corneille, et son neveu Fontenelle ont créé, sur une musique de Jean-Baptiste Lully, un opéra se nommant Bellérophon.


À la fin du XIXe siècle, le peintre anglais Walter Crane a été inspiré par les mythes de l'antiquité, en particulier par celui de Bellérophon.


- La chaîne de télévision Arte a présenté un épisode des Grands Mythes se nommant : Bellérophon, l'homme qui voulait être dieu.
- Dans le scénario du film Planète interdite de Fred M. Wilcox, la navette spatiale des scientifiques naufragés porte le nom de Bellérophon.
- Dans le film Mission Impossible 2 de John Woo, les personnages principaux sont aux prises avec une organisation qui a créé un virus exterminateur qui porte le nom de la Chimère ; le seul remède s'appelle le Bellérophon.
Il faut être très cultivé pour comprendre ces allusions à ce héros peu connu de la mythologie gréco-romaine.
