
Une collision continentale se produit lorsque deux plaques tectoniques continentales se rencontrent.

Histoire des connaissances sur la collision continentale
Dans l'Antiquité, les savants grecs constatent que certaines pierres ont une forme de coquillage, ce qui leur fait déduire que les montagnes ont, à l'origine, été recouvertes par la mer1 Ils comprennent aussi que la pluie doit jouer un rôle dans l'érosion des montagnes1.
Au XVIIe siècle, le philosophe René Descartes pense que les montagnes sont aussi vieilles que la Terre (ce qui est vrai pour les montagnes en général, mais par exemple pas pour les Alpes) et qu'elles se sont formées par écroulement de la surface de la Terre (ce qui est faux)1.
Au XIXe siècle, deux théories sur la formation des montagnes s'affrontent : pour le « neptunisme », les montagnes se seraient formées au fond des mers1. Pour le « plutonisme », ce sont des roches liquides qui ont été injectées sous la surface ; en durcissant, elles seraient devenues des montagnes1.
Au XXe siècle et jusque dans les années 1970, avec le « modèle géosynclinal », on pense que dans des cuvettes sous-marines se sont déposées des couches, qui ont formé les montagnes en se superposant1.
Depuis les années 1970, la formation des montagnes est expliquée par la tectonique des plaques, qui présente aussi l'avantage d'expliquer le volcanisme (aussi bien en surface que dans la mer), les tremblements de terre et la dérive des continents1. Finalement, les montagnes sont donc issues de la déformation de la croûte à cause de la rencontre de deux plaques qui se poussent l'une l'autre1.


Le plus souvent, une plaque s'enfonce sous une autre plaque par différence de densité entre les deux (cela s'appelle « subduction »)2. Il ne faut pas qu'il y ait une différence de densité trop importante entre les deux plaques qui se rencontrent, sinon la plus dense plongera directement dans l'asthénosphère, la partie plastique du manteau (la deuxième couche, après la croûte) sans occasionner de changement géologique3
La limite entre deux plaques s'appelle « suture » et elle est souvent difficile à repérer2. La suture est toutefois assez visible lors d'une « obduction », c'est-à-dire le chevauchement d'une plaque océanique et d'une plaque continentale2.
Selon le modèle de la tectonique des plaques, la croûte est divisées en plusieurs parties appelées « plaques tectoniques » qui se déplacent lentement1. Il existe deux types de plaques : les plaques qui font partie des continents font partie de la croûte continentale, essentiellement composée de granite, et les plaques au fond des océans qui sont plutôt constituées de basalte1. Les continents ont toujours été présents à la surface de la Terre depuis que les océans existent, mais ils peuvent coulisser, s'écarter ou se rapprocher1.
Le plus souvent, c'est la plaque océanique qui plonge sous la plaque continentale car celle-là est plus dense1.


Une collision continentale peut se mesurer sur le court terme ou sur le long terme : les séismes sont une preuve d'une collision continentale. En effet, quand deux plaques se poussent l'une l'autre, ce n'est que de quelques centimètres par an1. Mais parfois, quelque chose casse en profondeur : c'est un tremblement de terre, aussi appelé séisme.
C'est à la limite entre deux plaques que se produisent le plus de séismes.
Si on prend un espace de plusieurs millions d'années pour mesurer les conséquences d'une collision continentale, on verra que c'est à cause de ça que se forment les montagnes : quand une plaque se soulève sous l'effet de la poussée d'une autre plaque.
Ce n'est pas parce qu'une montagne est basse qu'elle est vieille ; c'est même parfois le contraire : le Jura n'a par exemple « que » trois millions d'années, alors que les Alpes, qui sont beaucoup plus hautes, se sont formées il y a plus de 15 millions d'années.


La collision continentale présente des avantages en géologie, car elle « mélange » les roches, les compresse et en fait affleurer d'autres, ce qui rend leur étude plus facile4.
Ce phénomène influe aussi sur l'épaisseur de la croûte, car elle est déformée sous l'effet du chevauchement des plaques34. C'est cette déformation qui crée les montagnes (ce phénomène s'appelle « orogenèse »)3.
Elle peut même faire se superposer les deux sortes de croûte, la croûte océanique et la croûte continentale, qui ne sont pas composées des mêmes roches4.





