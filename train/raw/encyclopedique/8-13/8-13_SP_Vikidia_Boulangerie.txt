
Une boulangerie est un atelier et une boutique où travaille le boulanger : on y fabrique, et on y vend, du pain, et, traditionnellement, des viennoiseries (croissants, pains aux chocolat...). De nos jours, beaucoup de boulangeries sont aussi des pâtisseries.
Comme le pain qui y est fabriqué, la boulangerie est l'un des symboles de la France : dans les villes françaises, c'est une des boutiques les plus populaires : on en trouve une dans pratiquement toutes les villes : il y a plus de 8001 boulangeries à Paris, et même à Moncontour2, l'une des plus petites communes de France3. Mais on trouve aussi des boulangeries dans beaucoup d'autres pays, qui font différentes sortes de pain et viennoiseries, comme le schwarzbrot (pain noir) et le bretzel allemands, les kanelbullar (pains à la cannelle) suédois ou les muffins anglais...



De l'origine du métier à la création des boulangeries
Le pain a toujours été un aliment de base en Europe et au Moyen-Orient, et le métier de boulanger existe depuis l'Antiquité ; cependant, la fabrication du pain est longtemps restée une activité que l'on pratique à la maison, et la boulangerie, telle qu'on la connaît aujourd'hui, n'est apparue que bien plus tard. Dans l'Égypte antique, les boulangers faisaient cuire leur pain le matin, et allaient ensuite le vendre sur les marchés. Les riches romains avaient des boulangers parmi leur personnel : ils préparaient le pain dans une boulangerie qui faisait partie de la maison, mais n'avaient pas de « boutique » en ville. 
En Europe, dans les villages, était souvent construit un four communal, autour duquel les gens se rassemblaient le matin, pour venir faire cuire leur pain. Parfois, il y avait un boulanger, qui cuisait le pain sur place.
C'est durant le Moyen-Âge que les boulangeries apparaissent. Rapidement, on ne fabrique plus le pain à la maison, on vient l'acheter à la boulangerie, et la boulangerie devient un élément très important du village. 

Diversification des préparations et des activités
Par la suite, le métier de boulanger se diversifie : dans les différents pays et régions, il existe différentes variétés de pains et préparation. C'est au XVIsiècle qu'apparaît la brioche, en Normandie. Les pains et les préparations des boulangers de Vienne, en Autriche, étaient très réputées. On dit que c'est Marie-Antoinette d'Autriche, originaire de Vienne, qui aurait fait connaître les pains viennois à la cour française.
Durant la Révolution française, les boulangers sont accusés de faire du profit sur le pain, et les boulangeries doivent être grillagées, pour se protéger de la foule. Afin d'assurer les approvisionnements, et d'éviter les émeutes, Napoléon Bonaparte imposera des tarifs fixés sur le pain. En réaction, les boulangers vont inventer tout un tas de nouvelles variétés de pains, non prévues, et dont le prix n'était pas imposé...
Vers 1838, un autrichien, August Zang, ouvre à Paris la Boulangerie viennoise. Il fait connaître ce que l'on appellera désormais le pain viennois, et le kifli, l'ancêtre du croissant. Toutes ces préparations vont être désormais connues sous le nom de « viennoiseries ». On les consomme au petit-déjeuner. Durant le XIXème siècle, les viennoiseries se développent dans toutes les boulangeries, et de nombreuses nouvelles recettes apparaissent. Certaines, comme les pains aux raisins, les pains au chocolat, ou les célèbres chouquettes, deviennent rapidement incontournables. Le croissant, la plus célèbre des viennoiseries, devient au XXsiècle un symbole de la gastronomie française.


De nos jours, il existe des boulangeries très modernes. Au XXème siècle, des boulangeries se constituent en grandes entreprises : il s'agit, par exemple, de boulangeries familiales, qui se sont développées :
- en 1880, Phillibert Jacquet rachète la Boulangerie viennoise à August Zang4. Il inventera ensuite le pain grillé, et sera à l'origine de la création de l'entreprise Pains Jacquet
- à peu près à la même époque, un autre boulanger, Charles Heudebert, est à l'origine de la création de la société Heudebert. Selon la tradition de la marque, il serait l'inventeur de la biscotte.
- d'autres marques de boulangeries vont ainsi se créer, comme la société Brioche Pasquier, créée en 1974 par la famille Pasquier.
Aujourd'hui, de nombreuses boulangeries sont devenues des usines, dans lesquelles le pain est produit de façon industrielle. Dans les grandes surfaces, on trouve souvent des ateliers de boulangeries, dans lesquels on cuit le pain. Certains magasins ont une vraie boulangerie, tandis que d'autres achètent du pain précuit, produit par des industries agro-alimentaires : il suffit de le faire cuire, il n'y a donc pas besoin de tout le matériel que l'on trouve normalement dans une boulangerie pour pétrir la pâte, etc. Ces ateliers permettent aux supermarchés de proposer du pain fraîchement cuit, à faible prix, sans avoir besoin de trop de place.
De nos jours, il existe des boutiques qui proposent un type de pain nouveau, très bon marché : ces boutiques disposent d'un four, mais pas d'un laboratoire ou d'un atelier pour préparer la pâte : la pâte est achetée toute faite, surgelée, il n'y a plus qu'à la faire cuire. Cela permet de faire gagner beaucoup de temps, car un boulanger est normalement obligé de se lever très tôt pour préparer son pain : là, ce n'est plus la peine. En France, la loi du 25 mai 19985 interdit à ces boutiques de se faire appeler « boulangerie » : une boulangerie est un endroit où le pain est entièrement préparé et cuit sur place, y compris la pâte, et seuls les commerçants qui le font ont le droit de s'appeler boulangerie. Ceux qui font cuire du pain qu'ils achètent surgelé choisissent souvent de s'appeler « point chaud », ou, parfois, « viennoiserie »6
D'autres sociétés développent des franchises de boulangeries, comme Paul, ou la Brioche dorée, qui combinent boulangerie et restauration rapide : ces magasins ressemblent davantage à des boulangeries traditionnelles, et vendent du pain et des viennoiseries, mais aussi des sandwichs, des pizzas...


La fabrication du pain en grande quantité nécessite du matériel et des équipements. Dans une boulangerie, on trouve en général un atelier, équipé d'un certain nombre de machines :
Pour fabriquer du pain, il faut de la pâte, une pâte levée, c'est à dire une pâte que l'on a laissé reposé, le temps que les micro-organismes présents dans la levure ou le levain la fassent gonfler. Pour cela, le boulanger dispose d'une chambre de pousse, une pièce gardée à la bonne température, dans laquelle il peut déposer ses pâtons de pâte, le temps qu'ils lèvent.
Beaucoup de boulangeries modernes ont aussi une chambre froide, une pièce réfrigérée dans laquelle on conserve les aliments. La farine et les céréales peuvent également être conservés au froid : en effet, le froid protège les aliments, et permet de se débarrasser des parasites, comme le charançon du blé, par exemple.
Le boulanger doit d'abord préparer sa pâte, en mélangeant les différents ingrédients : c'est le pétrissage. La pâte à pain, constituée principalement de farine et d'eau, est très élastique, notamment en raison de la présence de gluten. De plus, compte tenu de la quantité à préparer, elle est assez lourde. Le pétrissage de la pâte est un travail long, difficile, et fatiguant, que l'on réalise dans un pétrin. Aujourd'hui, beaucoup de pétrins sont électriques, ce qui facilite beaucoup le travail.
Enfin, pour pouvoir faire cuire le pain, il faut un four. Le four à pain est beaucoup plus grand que le four que l'on a dans sa cuisine, par exemple. Il était autrefois en pierre, avec une cheminée, car il chauffait grâce à un feu de bois : le four était divisé en deux, par une dalle horizontale. En dessous, le feu de bois faisait chauffer la dalle, sur laquelle, au dessus, on pouvait faire cuire le pain. Ce type de cuisson, chauffé par le bas, est appelé un four à sole : c'est ce qui permet de donner au pain sa texture particulière, car le dessous du pain va être plus cuit que le dessus, ce qui va permettre de former une croûte plus épaisse.
De nos jours, il existe encore des fours à bois traditionnels, mais dans la plupart des boulangeries modernes, ce sont plutôt des fours électriques.
Les fours à pain sont très profonds : on peut y enfourner un grand nombre de pains à la fois, qui vont cuire en même temps. Pour enfourner ces pains, ou pour les sortir, il est nécessaire d'utiliser une pelle à pain, traditionnellement en bois. Aujourd'hui, beaucoup de fours modernes permettent de faire entrer directement un chariot, sur lequel sont posés les différents pains à cuire.
-Le boulanger pèse ses ingrédients à l'aide d'une balance.
-Il verse ensuite ses ingrédients dans un pétrin, et rajoute de l'eau pour former la pâte.
-Une fois sortie du pétrin, la pâte est divisée en pâtons tous de taille égale, au moyen d'une diviseuse.
-Le boulanger façonne ensuite ses baguettes au moyen d'une façonneuse...

-Les pâtons sont ensuite déposés sur une échelle...
-...puis emmenés en chambre de pousse, le temps que la pâte lève.
-Une fois les pâtons sortis de la chambre de pousse, ils sont déposés sur la couche, une toile en lin.
-Le grignage consiste à effectuer des petits traits de découpe sur le pain, de façon à ce que, en cuisant, la croûte se craque à cet endroit.

-Une fois cuits, il ne reste plus qu'à les sortir, au moyen d'une pelle.
La fabrication du pain prend du temps, car la pâte doit reposer avant d'être cuite. C'est pourquoi le boulanger doit se lever très tôt, avant que le jour soit levé, pour que son pain soit prêt pour le vendre le matin.
Autrefois, le boulanger travaillait une partie de la nuit, puis il allait dormir durant la journée : c'était souvent sa femme qui vendait le pain qu'il avait préparé. Aujourd'hui, les boulangers ont souvent des employés, qui vendent le pain pour eux, et plusieurs équipes peuvent se relayer, aidées par des machines, pour travailler plus efficacement.


En plus de faire du pain, dans une boulangerie moderne, on propose généralement d'autres produits. Les viennoiseries sont les plus traditionnelles : les recettes de viennoiseries, souvent à base d'œufs, ou de sucre, ressemblent plus à des recettes de pâtisserie, mais les techniques et le matériel employés pour les faire ressemble plus à de la boulangerie : les pâtes de viennoiseries sont souvent des pâtes levées, elles sont d'abord pétries dans le pétrin, puis mises en pousse, comme pour le pain.
En revanche, beaucoup de boulangeries sont aujourd'hui aussi des pâtisseries : on y fabrique, et on y vend, des gâteaux.
Les boulangeries peuvent également proposer d'autres aliments, comme des pizzas, ou des sandwiches.


La présence d'une grande quantité de farine, et de céréales, dans une boulangerie, a toujours attiré de nombreux animaux, pas toujours bienvenus, (dans les boulangeries). Ainsi, les rats et les souris étaient autrefois fréquents : ils vivaient dans la boulangerie, perçaient les sacs de farine, et occasionnaient de gros dégâts dans les provisions.
C'est pour cette raison que les boulangeries accueillaient très souvent des chats : ces animaux aimaient se prélasser près de la chaleur du four, et trouvaient de quoi manger en chassant les rongeurs, évitant ainsi des pertes trop importantes. Les boulangers (comme d'ailleurs, les meuniers, pour les mêmes raisons), avaient donc souvent des chats dans leur boulangerie.
Plusieurs insectes apprécient également les céréales, et les farines, ce qui fait qu'on pouvait les trouver abondamment dans les boulangeries. Le ver de farine s'est si bien adapté aux habitats humains qu'il y est beaucoup plus fréquent que dans la nature. C'est en fait la larve d'un petit insecte, le ténébrion meunier, appelé ainsi car il adore la farine, et qu'il était fréquent de le trouver dans les moulins... Ou les boulangeries. 
Le charançon du blé est un autre insecte coléoptère, minuscule, qui perce un petit trou dans les grains de blé pour y pondre ses œufs. Il se nourrit aussi d'autres céréales, de farine, de pain... et peut occasionner de gros dégâts dans une boulangerie.
Aujourd'hui, les méthodes d'hygiène ont permis d'éliminer tous ces animaux des boulangeries. On ne trouve plus de rats dans les boulangeries, ni de chats non plus, d'ailleurs.
-Des rats noirs, sur un sac de grains, au Muséum d'Histoire naturelle de Berlin.
-Des ténébrions meuniers, dans des grains de blé
-Des vers de farine, les larves du ténébrion meunier, dans du son
-Un minuscule charançon du blé, très fortement grossi (en réalité, il ne mesure que quelques millimètres)


- La Femme du boulanger est un film de Marcel Pagnol, sorti en 1938. Le boulanger Aimable, et sa femme, font partie des personnages principaux, et, bien entendu, plusieurs scènes ont lieu dans la boulangerie. On y découvre, notamment, quelques scènes de la vie quotidienne dans une boulangerie, comme lorsque Aimable sort son pain du four, ou encore la présence du chat, Pompon, et de sa chatte, Pomponnette, qui chassent les rats dans la boulangerie.
-Le film a été tourné au Castellet, dans le Var. La boulangerie où le film a été tourné n'existe plus7 aujourd'hui, mais l'actuelle boulangerie du village porte le nom du film : La Femme du boulanger.
- Paris mange son pain est un court-métrage de Pierre Prévert (le petit frère de Jacques Prévert), en 1958. On y découvre la fabrication, puis la consommation du pain.
- l'humoriste français Fernand Raynaud a écrit un sketch célèbre, Le douanier, dans lequel il critique un « étranger » qui « vient manger l'pain des français ». On apprend à la fin du sketch que l'« étranger » est en fait... Le boulanger. Ce sketch est une dénonciation du racisme.
- La Meilleure Boulangerie de France est une émission de télévision diffusée sur M6. C'est un grand concours organisé entre les boulangeries de France.
-L'émission est l'adaptation de l'émission anglaise, Britain's Best Bakery.
