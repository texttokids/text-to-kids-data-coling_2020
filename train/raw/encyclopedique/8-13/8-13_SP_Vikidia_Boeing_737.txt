
Le Boeing 737 est un avion de ligne à réaction moyen-courrier construit par Boeing dans les années 1960, il s'agit de l'avion de ligne le plus vendu, avec 8 000 appareils de ce type construits1, certaines personnes fortunées l'ont aussi utilisé comme jet privé. Cet avion est l'un des premiers avions de ligne à réaction, il est donc peu fiable, mais il a été amélioré jusqu'à aujourd'hui, et une nouvelle version est en construction.


Dans les années 1960, Boeing commence à étudier la conception d'un avion court-courrier à deux réacteurs pour compléter le Boeing 727, la conception débute en mai 1964, l'objectif était de construire un avion capable de transporter 60 passagers sur une distance de 1 200  kilomètres, mais sera finalement plus grand que ça, et pourra donc transporter 100 passagers, le premier prototype est sorti de l'usine en décembre 1966 et fera son premier vol quelques mois plus tard à l'aéroport Boeing-Comté de King (qui s'appelait à l'époque Boeing Field).


La première génération comprend les premières versions du Boeing 737, leurs cockpits ne possédaient aucun appareil électronique. Aujourd'hui, il n'est plus que très peu utilisé.
Le Boeing 737-100 est la première version de cet avion. Il a fait son premier vol le 9 avril 1967. Il s'agissait de l'un des premiers avions de ligne à réaction. Il est entré en service en février 1968 chez la compagnie allemande Lufthansa. Seuls 30 exemplaires du Boeing 737-100 ont été construits.
Le Boeing 737-200 est une version légèrement allongée qui est entré en service quelques mois après le Boeing 737-100 chez la compagnie américaine United Airlines. Il existe aussi une version cargo, le Boeing 737-200F ainsi qu'une version un peu allongée, le Boeing 737-200ADV. Le Boeing 737-200 a connu un bien meilleur succès que le Boeing 737-100, avec 1000 exemplaires construits.
Ces avions anciens servent encore pour quelques compagnies aériennes, au Canada, dans certains aéroports, les pistes ne sont pas toujours supportables pour certains appareils, mais cette ancienne version possède des trains d’atterrissage adaptés.


Il s'agit d'une version modernisée du Boeing 737 contenant quelques écrans dans le cockpit. Ils étaient encore petits mais c'était une révolution pour l'époque, même si le cockpit était encore assez différent des appareils possédant des technologies de pointe.
Le premier Boeing 737-300 a fait son premier vol en février 1984, et reçoit, après plusieurs mois de test, son autorisation de voler. Il entre en service chez USAir quelques jours plus tard.
1100 appareils de cette version ont été construits. Lorsqu'elle est devenue ancienne, la compagnie aérienne Southwest Airlines a décidé de moderniser ses Boeing 737-300.
Le Boeing 737-400 est une version ayant presque la même taille que le Boeing 737-300 mais avec quelques avantages. L'une des nouveautés de cet avion est le sabot de queue lui permettant d'éviter que la queue touche le sol au décollage, et causant ainsi des dégâts sur l'appareil. 500 Boeing 737-400 ont été construits.
Le Boeing 737-500 est une version très courte et moins populaire. Il a fait son premier vol commercial en 1990, et 400 ont été construits.


Les Boeing 737 de dernière génération (aussi appelé "Boeing 737NG" pour "NextGen") sont des avions très utilisés de nos jours. Plus de 4 000 appareils de ce type ont été construits, et il en existe quatre versions différentes : le Boeing 737-600, le Boeing 737-700, le Boeing 737-800 et le Boeing 737-900. Il possède un cockpit typique de notre époque, avec des systèmes modernes et fiables.
Le Boeing 737-700 possède une version légèrement améliorée, le Boeing 737-700ER, qui peut parcourir une distance plus grande.


Actuellement, Boeing crée le Boeing 737 MAX, dont le premier vol a eu lieu en 2014, près de 50 ans après le lancement du Boeing 737-100. Il y a 500 appareils de cette version déjà en commande, et son premier vol commercial devrait avoir lieu en 2017.


- Le Boeing 737BBJ : il s'agit d'une version d'affaires avec des aménagements de luxe pour les personnes fortunées et les personnes importantes.
- Le Boeing P-8 "Poseidon" : une version utilisée par les militaires. Il s'agit d'un avion de patrouille maritime.
- Le Boeing 737 AEW&amp;C : une autre version militaire possédant un radar.
- Le Boeing T-43 : il s'agit encore une autre version militaire qui sert d'avion d'entrainement militaire chez l'armée américaine. Elle est basée sur le Boeing 737-200.


Le poste de pilotage a bien évolué entre les premiers Boeing 737 et ceux d'aujourd'hui.
En effet, sur les deux premières versions du Boeing, il n'y avait aucun écran de navigation et uniquement des systèmes anciens. Les premiers instruments modernes que l'on trouvait à bord possédaient de nombreux bogues une fois sur deux. Sur les Boeing 737 de seconde génération, des écrans apparaissent. Ils ne sont pas très grands mais c'était une révolution pour l'époque, et les instruments étaient tout de même plus fiables. Aujourd'hui, ces avions possèdent de multiples écrans et des systèmes électroniques très sophistiqués.


Les Boeing 737 de dernière génération sont en concurrence directe avec les avions de la famille de l'Airbus A320, qui est très similaire, le Boeing 737-600 étant l'équivalent de l'Airbus A318, tandis que le Boeing 737-700 est celui de l'Airbus A319. Le Boeing 737-800 et le Boeing 737-900 rivalisent avec l'Airbus A320 et l'Airbus A321.
Le Boeing 737 possède également d'autres concurrents, comme :
- Le McDonnell Douglas MD-80, qui peut transporter un nombre similaire de passagers, et possède un cockpit similaire.
- Le Fokker F100, appareil du même type que le McDonnell Douglas MD-80.
- L'Embraer 190, également un avion du même style que les précédents, mais avec un cockpit "high-tech".
- Le Bombardier CSeries, nouvel avion ultra-moderne qui pourrait concurrencer le futur Boeing 737MAX.
Il existe également un autre appareil comparable, le Boeing 717, mais un Boeing peut pas être concurrent avec un autre Boeing, car ils ont été construits par la même entreprise.


Depuis sa création, le Boeing 737 a été construit en 8 600 exemplaires, ce qui en fait l'avion de ligne le plus populaire au monde. Il reste encore plus de 4000 appareils de ce type à construire, ce qui fera près de 13 000 lorsque toutes les commandes en cours seront satisfaites. Mais d'ici là, il y en aura sans doute eu de nouvelles commandes.


Un accident fatal sur cet appareil s'est produit en moyenne une fois tous les 3 millions de vols en Boeing 737, mais les crashs sont presque 8 fois plus rares sur les versions récentes que sur les versions anciennes.


- Vol Aloha Airlines 243, le 28 avril 1988 : l'avion éclate en vol. L'accident ne fit qu'une seule victime. Pour éviter que ce genre d'accident ne se reproduise, une consigne de sécurité a été mise en place : quand l'avion devient trop vieux, il n'a plus le droit de voler.
- Vol Helios Airways 522, le 20 octobre 2005 : suite à un mauvais réglage, l'avion n'est pas pressurisé. Lorsque l'avion a pris de l'altitude, le pilote n'a pas correctement réagi. Toutes les personnes s'endorment. Plus personne ne pouvant donc contrôler l'avion, celui-ci s'écrase près d'Athènes lorsque le kérosène a été épuisé.
- Le 13 avril 2013 : un Boeing 737 de la compagnie aérienne Lion Air s'écrase lors de son atterrissage à Bali, en Indonésie. L'avion se brise en deux sous le choc, mais il n'y a eu aucune victime.


Le Boeing 737 est un avion mythique. Il existe en effet depuis 50 ans tandis que les versions récentes sont en exploitations. Les versions anciennes peuvent être exposées dans des musées d'aviation.


