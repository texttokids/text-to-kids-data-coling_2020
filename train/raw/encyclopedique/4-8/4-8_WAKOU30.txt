PICKPOCKETS DE TOUT POIL
Certaines mangeoires à oiseaux possèdent une protection contre les écureuils trop gourmands!
Les ratons laveurs sont de vrais cambrioleurs, ils entrent dans les maisons pour manger les croquettes des chats. L'un d'entre eux a même volé la couronne de la reine d'Angleterre ! Qui veut du poisson frais ? Les pygargues à tête blanche, rapaces d'Amérique du Nord, attrapent les poissons que les pêcheurs tiennent encore au bout de leur ligne. Plus près de nous, on aperçoit régulièrement des goélands s'envoler avec un cornet de glace dans le bec. Ziip, les goélands aiment la glace, maintenant ! Toi aussi ?
Raton laveur ou raton voleur ? Cet animal très intelligent n'a pas froid aux yeux !
