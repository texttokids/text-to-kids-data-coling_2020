L'ours brun aime tellement le miel et les larves d'abeilles qu'il peut faire de gros dégâts dans les ruches.
LES OUTILS DES DEGOURDIS
Le repas est difficile à atteindre ou a une coque trop dure ? Pas de souci, certains futés utilisent des outils. Le chimpanzé introduit une brindille dans la fourmilière, pour capturer les fourmis et les gober toutes crues. Le pinson des Galápagos utilise une épine de cactus pour piquer les insectes cachés dans les troncs. La loutre de mer fait la planche dans l'eau, pose une pierre à plat sur son ventre et frappe le coquillage dessus pour casser sa coquille. Le casse-croûte est prêt !
Pour se nourrir, le chimpanzé invente des outils : ici, il utilise une pierre en guise de casse-noix.
 L'abeille et le papillon t le nectar des fleurs, un sirop l'énergie. La roussette géante, chauve-souris d'Asie, sort la nuit savourer des bananes molles mûres, un vrai délice ! Le renard apprécie les baies, les myrtilles, et le raisin. Qui est le goulu le plus célèbre ? L'ours brun, qui n'hésite pas à affronter les abeilles pour piller leur miel.

