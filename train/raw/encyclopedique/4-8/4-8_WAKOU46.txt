DES BÉBÉS AFFAMÉS
Beaucoup de bébés animaux naissent au printemps : il fait doux et ils ont plein de choses à manger. Ainsi, ils vont pouvoir grandir, être forts et survivre. Tous ces nouveau-nés ont un gros appétit. Dans leur nid, les poussins des mésanges réclament sans cesse à manger en ouvrant grand le bec, tchiiip ! Ils épuisent leurs parents ! Au creux de la tanière, les louveteaux tètent leur maman. Ces petits goulus se disputent la meilleure mamelle, celle qui donne le plus de lait. Dans la mare, les canetons gobent les têtards. Ces bébés grenouilles, eux, se gavent d'algues.
Après avoir hiberné, le hérisson a très faim et il a besoin de forces pour se reproduire. Alors il mange beaucoup !
Quel appétit, ces bébés mésanges! Chaque jour, leurs parents apportent à chacun environ 50 petites bêtes à manger.

