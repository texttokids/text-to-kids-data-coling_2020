Un soir que Nasreddine revenait de son travail dans les champs avec des vêtements sales et crottés, il entendit chanter et rire et il comprit qu'il y avait une fête dans les environs.
Or, chez nous, quand il y a une fête, tout le monde peut y participer. Nasreddine poussa donc la porte de la maison et sourit de bonheur, une bonne odeur de couscous se dégageait de la cuisine.
Mais il ne put aller plus loin: il était tellement mal habillé qu'on le chassa sans ménagement. En colère, il courut jusqu'à sa maison, mit son plus beau manteau et revint à la fête.
Cette fois, on l'accueillit, on l'installa confortablement et on posa devant lui à manger et à boire. Nasreddine prit alors du couscous, de la sauce et du vin, et commença à les verser sur son manteau.
Et il disait : " Mange, mon manteau! Bois, mon manteau! " L'homme assis à son côté lui dit: " Que fais-tu, malheureux ? Es-tu devenu fou? "
" Non, l'ami, lui répondit Nasreddine. En vérité, moi je ne suis pas invité; c'est mon manteau qui est invité. "
