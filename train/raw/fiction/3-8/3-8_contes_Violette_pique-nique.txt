﻿Aujourd'hui, la princesse Violette a organisé un pique-nique avec ses amis. Elle veut déjeuner au bord du lac et y passer l'après-midi.
Le papa de Gaspard termine d'atteler les chevaux et tous les enfants montent dans la carriole. Il y a Ronan, Zena et Chloé, assis aux côtés de Violette, tandis que Gaspard joue au cocher. Il attrape les rênes et lance un petit claquement de langue pour faire avancer les chevaux.
" Princesse vous oubliez votre panier!" s'époumone la cuisinière sortant du château en courant. " Mince, merci ...", dit Violette en se mettant à rire avec ses amis. Une fois le panier bien calé, Gaspard lance à nouveau les chevaux en direction du lac.
Arrivés au bord du lac, Gaspard installe les chevaux à l'ombre et leur apporte à boire. Violette installe un grand drap sur l'herbe pour que tout le monde puisse s'y installer et déjeuner confortablement. "Il y a plein de bonnes choses à manger... Les cuisines nous ont gâtés !" se ravie la princesse.
Les amis s'amusent beaucoup pendant le repas : une abeille embête Ronan pour avoir un morceau de son gâteau à la framboise, Zena a une coccinelle qui s'est posée sur son nez pendant que Gaspard leur imite les bruits de plusieurs animaux. Violette est très contente que ses amis se sentent bien.
Elle décide d'organiser une petite balade à pieds autour du lac. Tous se lèvent, et entament leur promenade.
Violette marche à côté de Gaspard, il lui raconte sa journée d'hier. Lui et son père sont partis acheter de nouveaux animaux pour la ferme du château. Il invite Violette à venir les voir quand elle le souhaite.
La fin de l'après-midi approche, les enfants décident de retourner au château. Ronan détache les chevaux pour les atteler à la carriole, mais un cheval s'échappe ! "Attention princesse !" crie Gaspard en se mettant devant Violette pour attraper le fugueur.
Heureusement tout se fini bien, Gaspard réussit à ramener le cheval et l'attache. Violette remercie Gaspard en lui déposant un baiser sur la joue.
Après cette belle journée et de retour au château, les enfants se saluent et chacun rentrent chez soi, des souvenirs plein la tête.
FIN
