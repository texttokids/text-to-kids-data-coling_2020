Oscar aimait beaucoup sa petite peluche ours blanc. En effet c’était son cadeau de noël. La peluche était très blanche et son visage très mignon. Il pouvait même dire « Tout va bien ». Oscar nomma son nouvel ami Bobo!
Il l’emmenait partout avec lui. Pour manger.
Pour dormir.
Pour jouer.
Et même pour prendre un bain.
Il voulait même l’amener à l’école mais c ‘était interdit. Oscar était triste. Il était tout seul dans sa classe. « Tu me manques, Bobo… »
Une petite fille vient devant Oscar. « Salut! pour quoi tu es tout seul? » « Ma peluche m’manque. » « Moi aussi, comment elle s’appelle? » « Bobo, c’est un ours blanc » « Son nom est très mignon! Ma peluche s’appelle Vivi. Je veux voir Bobo? » « Tu veux venir chez moi? Je t’invite! »
Après l’école, la petit fille alla chez Oscar avec sa maman et Vivi. Ils jouaient ensemble dans la chambre d’Oscar. Et maintenant il n’est plus seul à l’école même si Bobo ne peut pas l’accompagner.
