Aujourd’hui c’est samedi, et Lily n’a pas école. Elle accompagne donc son papa qui va chercher du pain pour le déjeuner.
Sur le chemin, Papa explique à Lily : “Il y a un peu de circulation aujourd’hui, alors tiens-moi bien la main ma chérie, et on va bien rester sur le trottoir”. “D’accord” répond Lily qui obéit avec attention.
Arrivés près de la boulangerie, Lily et son papa doivent traverser la route. Lily commence à avancer quand son papa lui retient le bras.
“Attention Lily !” s’écrie-t-il,” Tu ne peux pas traverser comme ça. Avant, il faut bien s’assurer qu’il n’y a pas de danger.” “Mais le petit bonhomme était vert Papa !” explique Lily en montrant le feu du doigt. “Je sais ma chérie, mais il faut quand même vérifier autour de soi pour écarter tout danger”.
Papa explique à Lily les bons réflexes à avoir avant de traverser la route. “On commence par regarder le bonhomme, puis on vérifie à gauche et à droite. Ensuite on peut traverser tout en se tenant bien la main”, explique Papa. La petite fille écoute et fait pareil que son Papa.
Arrivés à la boulangerie, Lily s’adresse à la vendeuse : “Bonjour Madame, on veut une baguette de pain.”
“Tu ne crois pas qu’il manque le mot magique ?” insiste son papa. “S’il vous plaît…” dit la petite fille, toute gênée. “Avec plaisir demoiselle” répond la boulangère. “Ça fera 1€”.
Le Papa de Lily lui tend une pièce. Elle remarque le chiffre “1” sur la pièce. “Un euro !” dit-elle en donnant la pièce à la boulangère.
La boulangère sourit, prend la pièce et donne la baguette à la petite fille.
Papa et Lily quittent la boulangerie, sans oublier de remercier et de dire “au revoir”.
Lily est contente de sa sortie, elle a appris à être prudente et polie. Papa est fier d’elle, elle arrive même à lire les chiffres sur les pièces de monnaie.
