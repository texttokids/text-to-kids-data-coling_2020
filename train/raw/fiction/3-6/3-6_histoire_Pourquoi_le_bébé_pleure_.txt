Pourquoi le bébé pleure ?
Parce qu’elle est très triste ?
Parce qu’elle a peur ?
Parce qu’elle n’aime pas sa peluche ?
Pour nous casser les oreilles ?
Ta petite sœur pleure car elle ne sait pas encore parler.
Elle veut peut-être dire : j’ai faim, j’ai soif !
Ou : j’ai sommeil !
Quand elle aura appris à parler, comme toi, elle pourra nous dire les choses.
Pas trop de choses quand même, sinon elle va me casser les oreilles !
