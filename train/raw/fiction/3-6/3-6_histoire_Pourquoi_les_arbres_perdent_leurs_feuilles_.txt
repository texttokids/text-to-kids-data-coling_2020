Pourquoi les arbres perdent leurs feuilles ?
Parce qu’ils sont malades ?
Parce que le vent souffle trop fort ?
Parce que les oiseaux les mangent ?
Parce qu’on leur a volé pendant la nuit ?
Les feuilles, ce sont comme des habits pour les arbres. Il y en a de toutes les couleurs, de toutes les formes et de toutes les tailles.
Tu te souviens de ton petit pull rouge que tu aimais ? Tu as grandi et il est trop petit pour toi et usé, du coup on l’a jeté et je t’en ai acheté un autre, tout neuf.
Les arbres grandissent, changent aussi et leurs feuilles s’usent tous les ans. Alors ils les laissent tomber, ils restent tous nus le temps que de nouvelles feuilles poussent.
Ils sont heureux car ils ont de nouvelles feuilles toutes belles pour le printemps qui commence, et même parfois des fleurs.
Brrrr, moi je resterai pas tout nu en attendant mon nouveau pull !
