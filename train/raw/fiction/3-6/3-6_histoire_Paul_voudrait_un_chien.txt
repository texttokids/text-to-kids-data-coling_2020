Paul est parti passer quelques jours chez son cousin Pipo.
Ensemble ils ont joué avec le chien de Pipo, le superbe Marvin.
Un chien Saint Bernard plein de poils qui aime les câlins.
De retour chez lui, Paul demande à ses parents « Pourquoi nous n’avons pas de chien ? »
Son Papa répond qu’ils ne peuvent pas avoir un chien car cela demande du temps et de la disponibilité. "Il faut pouvoir s’occuper d’un chien comme celui-ci. Marvin a besoin de place, d’un jardin pour se dépenser et nous n’en n’avons pas."
Paul demande alors si un plus petit compagnon pourrait vivre à la maison. « Un chien plus petit Papa ? »
Son père lui explique qu’il est parfois compliqué d’avoir des animaux à la maison. « Ce sont des êtres vivants tu sais, comme toi ils ont besoin d’attention. Il faut les sortir tous les jours et partir en vacances avec eux ».
Paul comprend qu’il est pour l’instant impossible pour lui d’avoir un chien. Sa maison est trop petite et un gros chien serait malheureux à la maison. Pourtant son Papa lui propose quelque chose.
"Lorsque nous partirons de cette maison, quand ton petit frère sera plus grand, nous penserons à acheter un chien, même un petit." Paul sait que son Papa trouvera toujours une solution pour son petit garçon.
