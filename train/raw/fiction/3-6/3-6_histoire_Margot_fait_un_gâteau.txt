Comme tous les dimanches, Margot demande à son Papa de faire un gâteau en famille.
Son petit frère Victor aussi adore faire les gâteaux en famille. Tous les deux prennent des chaises pour arriver à la hauteur du plan de travail.
Margot, Victor et leur papa choisissent une recette.
Ils regardent la photo et les deux enfants s’écrient « Un gâteau au chocolat ! »
Leur Papa prend tous les ingrédients nécessaires et les enfants les mélangent. Le chocolat fondu dans la casserole et le beurre fondu ravissent Margot et Victor.
Ils lèchent la cuillère et ont plein de chocolat autour de la bouche. Papa regarde ses enfants en riant et leur dit « Je vais vous appeler Moustaches ! ».
Tout le monde rit beaucoup en faisant ce gâteau. Lorsqu’il sort du four, les enfants et toute la famille le dégustent avec plaisir.
