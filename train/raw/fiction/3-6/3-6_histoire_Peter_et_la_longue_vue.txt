﻿La mer est calme et paisible cet après-midi, mais Peter le pirate est anxieux. Cela fait plusieurs jours qu’il n’a pas vu de terre à l’horizon.
Aujourd’hui le brouillard s’est levé. Peter aperçoit alors un vol de mouettes. Et qui dit mouette dit que la terre n’est pas loin !
Le pirate décide donc de sortir sa longue vue et observe l'horizon.
“C’est bizarre”, se dit Peter, “les mouettes semblent plus petites, elles volent pourtant toujours au dessus du bateau.”
Peter regarde alors son gouvernail avec la longue vue. “Étrange, le gouvernail est lui aussi tout petit.”
Peter est troublé. Il regarde sa longue vue. Mais bien sûr ! Peter a mis sa longue vue à l’envers ! Au lieu de voir les choses en grand, tout s’est rétréci dans la lunette de l’objectif.
Cela l’amuse beaucoup et il continue à regarder à travers sa longue vue du mauvais côté. Ses mains, ses pieds, son chapeau, son bateau, tout devient minuscule.
Peter continue à jouer quand il entend des cris de mouettes de plus en plus fort. Le pirate se retourne et aperçoit une île à l’horizon. Peter est heureux, il va enfin pouvoir accoster quelques heures.
Arrivé sur l’île, il prend sa longue vue et regarde du mauvais côté.
Les gens, les arbres, les fruits, tout lui apparaît tout petit.
Peter a trouvé un nouveau jeu aujourd’hui. Il est content de pouvoir regarder le monde d’un oeil nouveau.
FIN
