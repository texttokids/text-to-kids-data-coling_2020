Pourquoi Papa habite dans une autre maison ?
Il ne veut plus être avec moi ?
Il n’aimait plus notre maison ?
Il voulait une chambre pour lui tout seul ?
Papa est parti de la maison pour habiter dans sa maison à lui.
Papa et Maman ne s’entendent plus, et se disputent beaucoup, alors Papa va vivre ailleurs.
Mais tu auras ta chambre aussi là bas, tu auras la maison de Papa et la maison de Maman.
Papa et moi, on se sépare, mais nous sommes tes parents et nous t’aimons pour toute la vie.
Parfois l’amour entre les parents peut s’arrêter, mais pas l’amour pour les enfants.
Moi aussi je vous aime pour toute la vie.
