Cette après-midi, le soleil est au rendez-vous. Peter le pirate a jeté l’ancre et décide de faire une sieste sur le pont de son bateau.
Au bout d’un moment, Peter est réveillé par des pleurs. Il regarde dans la mer. Rien à l’horizon.
Peter se retourne et aperçoit une silhouette sur un rocher.
Il lève l’ancre et navigue vers elle.
C’est une sirène qui pleure en tenant sa nageoire serrée. “Que vous arrive-t-il ? Pourquoi pleurez-vous ?”
La sirène prend peur et plonge dans l’eau. “Non n’ayez pas peur, je veux seulement vous aider.”
La sirène sort de l’eau et dit “Les pirates n’aident pas les sirènes, regardez ce qu’ils m’ont fait”. Elle montre sa nageoire dans laquelle s’est pris un hameçon.
“Je ne suis pas un méchant pirate”, explique Peter, “ce qui m’intéresse se sont les trésors, pas les sirènes. Remontez sur le rocher je vais essayer de retirer ce vilain crochet”.
La sirène hésite et finit par remonter. Peter attrape la nageoire et tortille l’hameçon qui finit par sortir.
“Vous avez réussi, et vous ne m’avez même pas fait mal !” s’étonne la sirène.
Elle attrape son collier sur lequel se trouve une perle qu’elle tend à Peter. “Prenez mon collier, pour vous remercier”.
Elle donne le collier et plonge aussitôt dans l’océan. Peter a gagné un nouveau trésor, et la fierté d’avoir sauvé une si jolie sirène.
