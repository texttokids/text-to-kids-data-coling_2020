
Ils gémissent, se palpent, se constatent, puis réclament du café.

En l'éclusant face à face, ils se visionnent de leurs yeux pochés. Ronchonnent pour commencer. Et les voilà qui éclatent de rire.
- J'ai cru qu' tu m'arrachais la tronche, mec, admet Béru. T'es fort comme un truc, tézigue, dans ton genre. Balaize, av'c d' la techenique ! Y n' t' manque que ce moste qu' j' possède, moi : la vraie rogne qui t' monte des burnes, mec. Quand c'est qu'é m' biche, j' sus t'invincib', postiv'ment. Un n'ouragan, tu I' contrôles pas. Faut qu'y passe, le n'ouragan. Faut qu'y détruive. Lorsqu'y s' produit, t'as plus qu'à foute l'nez dans le cul dans un ang' de mur et attendre.

Il avale une gorgée de caoua et fait la grimace.
- J' sais pas où ces braves z'Irlandais font pousser leur café, mais j' lu trouve un goût d' merde, Sana. Passe-moi la boutanche de vouiski qu' j' le requinque !

Je souscris à sa requête, après quoi, je leur expose mon plan.
- Nous allons prendre le barlu pneumatique des Hollandais et dénicher l'ouverture de la galerie. Quand nous l'aurons découverte, nous la remonterons jusqu'à la grille signalée par M. Blanc. J'ai acheté une scie à métaux et des cisailles pour neutraliser la chaîne qui la maintient fermée. Mon objectif est de délivrer les hommes que Jérémie a entendus parler et que, vraisemblablement, les habitants de la grande maison retiennent prisonniers.

Je descends sa fougue en flamme.
- Pas toi, Gros. Tu vas rester en couverture pour le cas où nous aurions turbin.

Alors là, il en pousse une pas belle, mon gros Sac Tyrolien.
- Que je restasse à faire d' la pâtisserie, du temps que vous allez à la chicorne, les deux ? Non mais tu m' prends pour qui est-ce-t-il, l'artiss ?
- On serait dingues de foncer tout les trois bille en tête, Alexandre-Benoît ! Tu le sens bien. J'emmène M. Blanc parce qu'il connaît déjà les lieux et que toi, s'il y a un os, tu seras plus compétent et plus crédible que lui pour organiser une caravane de secours.
- Merci pour la crédibilité, gronde mon pote sombre. Veux-tu dire qu'un sale branque de nègre...
- Meeeeeeeeeeeeerde ! hurlé-je à m'en faire cracher le sang ! On ne va pas continuer de se foutre sur la gueule après chacune des phrases qu'on prononce ! C'est plus une vie, les gars ! Tu charognes, mon grand ! Tu nous pompes l'air et la bite avec tes complexes de melon ! Nous fais pas un opéra avec ta carrosserie bronzée ! Trempe-toi dans l'eau de Javel si t'as des nostalgies blafardes !

Ça mutisme dans les rangs.

Le littoral irlandais, côté Atlantique, les gens le croivent pas (comme dit Béru) mais y a des phoques. Ils viennent se mettre la fourrure à sécher sur les rochers. P't' être que ce sont des otaries, note bien, ou alors des éléphants de mer. J'y connais que pouic et en plus je m'en branle, toujours est-il qu'il s'agit de pinnipèdes, quoi ! On va pas tourner un documentaire sur la question !

Ils poussent des cris en nous voyant embarquer sur le Zodiac du cornard, comme s'ils nous soupçonnaient de le voler. Je lance le petit moteur Johnson et nous voilà à tanguer sur les flots gris. Ça remue vachement, crois-moi. Faut pas craindre la gerbe pour naviguer sur cette capote anglaise.
- Peut-être que l'entrée de la galerie est immergée lorsqu'elle est haute ? D'après la topographie que j'ai en tête, c'est même probable.

Selon les repères que j'ai pris en tirant une ligne droite de la maison à la côte, nous devrions trouver l'entrée de cette foutue grotte à moins de cinq cents mètres de notre point de départ. Vue depuis les flots, la falaise est impressionnante, grandiose avec sa découpe aiguÃ qui lui donne des allures de cathédrale gothique. Elle nous surplombe à pic, roide comme un mur d'un gris sombre terriblement hostile. A la longer, on ressent de l'effroi. Il nous semble qu'elle va s'abattre sur nous, d'un coup, comme s'écroule un immeuble dûment miné. Moi, quand j'assiste à ces destructions de gratte-ciel, je suis terrifié. Ils ne "tombent" pas comme s'abat un chêne malrauxien, mais s'anéantissent sur leurs fondations. Ils meurent debout. Ils "disparaissent".

Il me montre un grand orifice noir, aux lèvres déchiquetées, percé dans la falaise et envahi par le flot.

Combien de siècles, de millénaires a-t-il fallu pour que l'eau force la roche, la fouille, la touille, la fouaille, l'investisse ? Combien de temps pour pratiquer ce tunnel naturel, avec des élargissements imprévus, des rétrécissements, des espèces de cavernes spacieuses ? Quand j'ai dit au cornard batave que j'étais passionné par la géologie, je ne lui mentais qu'à demi. Le sol me fascine. Il est ma maman, mon papa, il est moi. Je voudrais tout savoir de lui.

Nous nous présentons à l'orée de la grotte.

Selon mon estimation, il reste deux mètres d'air libre au-dessus de nos têtes. La marée risque-t-elle de monter encore beaucoup ? J'étudie son mouvement en tournant en rond devant l'entrée. Les repères sont malaisés à prendre car le flot est impétueux et se jette en force dans la brèche.
- T'as peur qu'on l'ait dans le prose si on entre, hein, vieux ? ricane M. Blanc.
- Il ferait pas bon se laisser coincer dans ce trou à rats. Tu nous imagines, collés au plafond comme deux bigorneaux dans notre petit barlu ?
