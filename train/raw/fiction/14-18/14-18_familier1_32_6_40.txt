
Elle murmure après une minute de silence dédiée au souvenir de Ted, voire à sa mémoire :
- J'aimerais pouvoir t'affirmer le contraire, mais ce serait manquer de franchise. Tu as quelqu'un chez qui aller, cette nuit ?

Bérurier se racle la gargane.
- J' te vas emmener chez moi, ma crotte, décide-t-il ; just'ment, ma bourgeoise est en voiliage d'agrément et y a une place libre dans not' plumard.
- Faut rien laisser perd', dit-il, en vrai fils de paysan français.

Je l'approuve d'un hochement de tête positif. Je suis certain que ça se passera très bien, les deux. Leurs conneries, même combat. Il la convaincra vite qu'elle doit céder à ses instances et quand elle en découvrira les dimensions, pour lors, le gars Ted sera relégué dans les brumes londoniennes.
- Filez les premiers, je vais rester encore un moment ici pour faire le constat.

Une fois seul, je referme la lourde, empile quelques coussins contre les débris de l'ancien canapé et m'installe à la M

Récamier pour gamberger.

Une question me taraude la coiffe : pourquoi m'intéressé-je à cette histoire ? Qu'est-ce qui m'accroche dans cette aventure de petits paumés plus ou moins loubards, à la lisière de la truanderie ? Qu'en ai-je à cirer qu'on les enlève et qu'on démolisse leur gîte ? Pauvre équipe, en vérité : Mandoline et Pâquerette, deux graines de pute maquées avec des demi-sels. De la came pour idéal... L'autre con de rouquin avec sa mèche genre Hun sur le sommet de la tronche, qui bricole à regrouper des caddies de grande surface pour gagner de quoi acheter sa dose quotidienne et à qui, tout à coup, on propose le pactole. C'était quoi, son extra de gala à l'English paumé ? Pourquoi l'a-t-on embarqué au sortir du casino de Montreux ? Quelle pauvre arnaque avait-il pu commettre, ce pâle enfoiré ? Faux gros bras à qui tu fends la gueule à l'aide d'un tesson de boutanche et que tu allonges d'un coup de latte dans ses pauvres roustons tristounets genre pickles ? Une chiffe, un dur en mie de pain. La crasse pour folklore. Un locdu incapable de se lever une gonzesse potable et qui s'attelle à ce boudin tourné de Pâquerette ! On est venu perquisitionner chez lui dans quel but ? Récupérer un truc bidule chose qu'il avait détourné ? Pour y chercher de la came ? Ou en représailles, histoire de filer les grelots à sa brebis ?

Mais tout ça, San-Antonio, en quoi ça peut bien te toucher, mon chérubin ? T'as d'autres chats à fouetter, d'autres chattes à déguster ! Et même qu'un quidam encore anonyme soit scrafé au curare chez tes amis helvètes, c'est pas non plus ton frometon à toi, béby ! Faut chasser sur tes terres, pas sur celles des copains vaudois ! Qu'à ce moment-là, ils n'auraient plus qu'à débouler à la Grande Taule et prendre possession des lieux, non ?

Eh bien pourtant, logique ou pas, bizarre ou non, me voilà ensuqué par cette petite sauterie suisse. Avec la démange d'en savoir plus.

C'est pas émouvant, quelque part, cette marotte de vouloir tout savoir et tout payer ? La maladie du chien de chasse ! Chronique ! De me gratter n'y fait que pouic ! Faut que je ronge l'os, tu comprends. Que je le brise de mes crocs pour en dégager la substantifique moelle.

Pourquoi m'attardé-je dans cet intérieur crapoteux où flottent des relents de merde, de friture froide et de patchouli ?

Et pourquoi ont-ils déféqué sur la photo de M

Ralousse mère ? Là, ça fleure la vengeance à trois balles. Du tout premier degré. Ça fait descente de grande banlieue, quelque part, tu trouves pas toi ? Les gars de la bande de La Courneuve contre celle de la République ! Y a de la chaîne de vélo dans l'air !

Au sol, des flaques d'alcool montrent qu'on a vidé les bouteilles, manière de ne pas risquer de mauvais éclats en les brisant. Un camembert est plaqué contre le mur. Le poste de téloche n'a plus de tube catholique (comme dirait le Gros). Jusqu'à quelques livres de la prestigieuse collection Colombine que l'on a émiettés. Le vandalisme sur toute la ligne, mais un vandalisme mesquin ! De plus en plus je me convaincs qu'il est l'œuvre d'un commando de minables. Devait y avoir de la moto en stationnement devant l'immeuble !

J'en suis là de mes cogitations (et j'en suis las également) lorsqu'un vacarme de musique pop éclate dans l'appartement voisin. Ils y vont plein tube, les neighbours. A t'en faire exploser les manettes ! Le moment est venu de me trisser. Un rétablissement, hop ! Et je gagne la sortie.

Sur le palier, la viorne est insoutenable. Elle fait trembler la vitre fêlée de la tabatière éclairant (de jour) ce terminus de la cage d'escadrin. Jusqu'à la rampe mal fagotée qui frémit sous ma main.

Je commence à descendre lorsqu'il me vient une idée acidulée. Rebroussant chemin, je me pointe jusqu'à la porte voisine de celle de Pâquerette. Un papier arraché à un bloc de correspondance annonce le blaze de l'heureux locataire épris de musique paroxystique "M. et M

Opération sonnette. Me faut carillonner un bout avant que le timbre se faufile à travers le tumulte jusqu'aux cages à miel des occupants.
