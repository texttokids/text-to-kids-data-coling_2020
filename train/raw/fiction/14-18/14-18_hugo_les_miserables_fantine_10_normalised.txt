
Il avait à sa fenêtre un antique rideau de grosse étoffe de laine qui finit par devenir tellement vieux que, pour éviter la dépense d'un neuf, madame Magloire fut obligée de faire une grande couture au beau milieu. Cette couture dessinait une croix. L'évêque le faisait souvent remarquer.
- Comme cela fait bien ! disait-il.

Toutes les chambres de la maison, au rez-de-chaussée ainsi qu'au premier, sans exception, étaient blanchies au lait de chaux, ce qui est une mode de caserne et d'hôpital.

Cependant, dans les dernières années, madame Magloire retrouva, comme on le verra plus loin, sous le papier badigeonné, des peintures qui ornaient l'appartement de mademoiselle Baptistine. Avant d'être l'hôpital, cette maison avait été le parloir aux bourgeois

. De là cette décoration. Les chambres étaient pavées de briques rouges qu'on lavait toutes les semaines, avec des nattes de paille tressée devant tous les lits. Du reste, ce logis, tenu par deux femmes, était du haut en bas d'une propreté exquise. C'était le seul luxe que l'évêque permit. Il disait :
- Cela ne prend rien aux pauvres.

Il faut convenir cependant qu'il lui restait de ce qu'il avait possédé jadis six couverts d'argent et une grande cuiller à soupe que madame Magloire regardait tous les jours avec bonheur reluire splendidement sur la grosse nappe de toile blanche. Et comme nous peignons ici l'évêque de Digne tel qu'il était, nous devons ajouter qu'il lui était arrivé plus d'une fois de dire :
- Je renoncerais difficilement à manger dans de l'argenterie.

Il faut ajouter à cette argenterie deux gros flambeaux d'argent massif qui lui venaient de l'héritage d'une grand'tante. Ces flambeaux portaient deux bougies de cire et figuraient habituellement sur la cheminée de l'évêque. Quand il avait quelqu'un à dîner, madame Magloire allumait les deux bougies et mettait les deux flambeaux sur la table.

Il y avait dans la chambre même de l'évêque, à la tête de son lit, un petit placard dans lequel madame Magloire serrait chaque soir les six couverts d'argent et la grande cuiller. Il faut dire qu'on n'en ôtait jamais la clef.

Le jardin, un peu gâté par les constructions assez laides dont nous avons parlé, se composait de quatre allées en croix rayonnant autour d'un puisard ; une autre allée faisait tout le tour du jardin et cheminait le long du mur blanc dont il était enclos. Ces allées laissaient entre elles quatre carrés bordés de buis. Dans trois, madame Magloire cultivait des légumes ; dans le quatrième, l'évêque avait mis des fleurs. Il y avait çà et là quelques arbres fruitiers.

Une fois madame Magloire lui avait dit avec une sorte de malice douce :
- Monseigneur, vous qui tirez parti de tout, voilà pourtant un carré inutile. Il vaudrait mieux avoir là des salades que des bouquets.
- Madame Magloire, répondit l'évêque, vous vous trompez. Le beau est aussi utile que l'utile.

Il ajouta après un silence :
- Plus peut-être.

Ce carré, composé de trois ou quatre plates-bandes, occupait M. l'évêque presque autant que ses livres. Il y passait volontiers une heure ou deux, coupant, sarclant, et piquant çà et là des trous en terre où il mettait des graines. Il n'était pas aussi hostile aux insectes qu'un jardinier l'eût voulu. Du reste, aucune prétention à la botanique ; il ignorait les groupes et le solidisme ; il ne cherchait pas le moins du monde à décider entre Tournefort et la méthode naturelle ; il ne prenait parti ni pour les utricules contre les cotylédons, ni pour Jussieu contre Linné. Il n'étudiait pas les plantes ; il aimait les fleurs. Il respectait beaucoup les savants, il respectait encore plus les ignorants, et, sans jamais manquer à ces deux respects, il arrosait ses plates-bandes chaque soir d'été avec un arrosoir de fer-blanc peint en vert.

La maison n'avait pas une porte qui fermât à clef. La porte de la salle à manger qui, nous l'avons dit, donnait de plain-pied sur la place de la cathédrale, était jadis armée de serrures et de verrous comme une porte de prison. L'évêque avait fait ôter toutes ces ferrures, et cette porte, la nuit comme le jour, n'était fermée qu'au loquet. Le premier passant venu, à quelque heure que ce fût, n'avait qu'à la pousser. Dans les commencements, les deux femmes avaient été fort tourmentées de cette porte jamais close ; mais M. de Digne leur avait dit :
- Faites mettre des verrous à vos chambres, si cela vous plaît.

Elles avaient fini par partager sa confiance ou du moins par faire comme si elles la partageaient. Madame Magloire seule avait de temps en temps des frayeurs. Pour ce qui est de l'évêque, on peut trouver sa pensée expliquée ou du moins indiquée dans ces trois lignes écrites par lui sur la marge d'une bible : « Voici la nuance : la porte du médecin ne doit jamais être fermée ; la porte du prêtre doit toujours être ouverte. »

Sur un autre livre, intitulé Philosophie de la science médicale

, il avait écrit cette autre note : « Est-ce que je ne suis pas médecin comme eux ? Moi aussi j'ai mes malades ; d'abord j'ai les leurs, qu'ils appellent les malades ; et puis j'ai les miens, que j'appelle les malheureux. »
