
Petit Toomai eut peur. Il ne savait pas grand-chose des Blancs, mais Petersen Sahib représentait pour lui le plus grand homme blanc du monde : il était le chef de toutes les opérations dans le keddah
- celui qui prenait tous les éléphants pour le Gouvernement de l'Inde, et qui en connaissait plus long que personne au monde sur les us et coutumes des éléphants.
- Quoi ! qu'est-ce qui peut arriver ? dit Petit Toomai.
- Ce qui peut arriver ! le plus mauvais, tout simplement. Petersen Sahib est un fou : autrement, pourquoi traquer ces démons sauvages ?… Il peut même te forcer à devenir chasseur d'éléphants, à dormir n'importe où, dans ces jungles fiévreuses, à te faire un jour, en fin de compte, fouler à mort dans le keddah

. Il est heureux que cette sottise se termine sans accident. La semaine prochaine, la chasse sera finie, et nous autres, des plaines, nous regagnerons nos postes. Alors, nous marcherons sur de bonnes routes, et nous ne penserons plus à tout cela. Mais, fils, je suis fâché que tu te sois mêlé de cette besogne : c'est affaire à ces gens d'Assam, immondes rôdeurs de jungle qu'ils sont. Kala Nag ne veut obéir à personne qu'à moi, aussi me faut-il aller avec lui dans le keddah

. Mais il n'est qu'un éléphant de combat, et il n'aide pas à lier les autres ; c'est pourquoi je demeure assis à mon aise, comme il convient à un mahout
- non pas un simple chasseur ! - un mahout

, dis-je, un homme pourvu d'une pension à la fin de son service. Est-ce que la famille de Toomai des Éléphants est bonne à se faire piétiner dans l'ordure d'un keddah

 ? Méchant ! Vilain ! Fils indigne ! Va-t'en laver Kala Nag, fais attention à ses oreilles, et vois s'il n'a pas d'épines dans les pieds ; autrement, Petersen Sahib t'attrapera, bien sûr, et fera de toi un traqueur sauvage…, un de ces fainéants qui suivent les éléphants à la piste, un ours de la Jungle. Pouah ! Fi donc ! Va !

Petit Toomai s'en alla sans mot dire, mais il conta ses griefs à Kala Nag, pendant qu'il examinait ses pieds.
- Cela ne fait rien, dit Petit Toomai, en retournant le bord de l'énorme oreille droite, ils ont dit mon nom à Petersen Sahib et peut-être… peut-être… qui sait ?…Aie ! vois la grosse épine que je t'enlève là !

Les quelques jours suivants furent employés à rassembler les prises, à promener entre deux éléphants apprivoisés les animaux nouvellement capturés, pour éviter trop d'ennuis avec eux en descendant au sud, vers les plaines, puis à réunir les couvertures, les cordes et tout ce qui aurait pu se gâter ou se perdre dans la forêt. Petersen Sahib arriva sur le dos de son intelligente Pudmini : il revenait de compter leur paye à d'autres camps dans les montagnes, car la saison tirait à sa fin ; et maintenant un commis indigène, assis à une table sous un arbre, réglait leurs gages aux cornacs. Une fois payé, chaque homme retournait à son éléphant et rejoignait la colonne prête à partir. Les traqueurs, les chasseurs, les meneurs, tous les hommes du keddah

régulier, qui passent dans les jungles une année sur deux, se tenaient sur le dos des éléphants appartenant aux forces permanentes de Petersen Sahib, ou bien adossés au tronc des arbres, leur fusil en travers des bras, ils plaisantaient les comacs en partance, et riaient quand les nouvelles prises rompaient l'alignement pour courir de tous côtés. Grand Toomai se dirigea vers le commis avec Petit Toomai en arrière, et Machua Appa, le chef des trappeurs, dit à mi-voix à un de ses amis :
- Voilà de bonne graine de chasseur qui s'envole ! C'est pitié d'envoyer ce jeune coq de jungle muer dans les plaines.

Or, Petersen Sahib avait des oreilles tout autour de la tête, comme le doit un homme qui passe sa vie à épier le plus silencieux des êtres vivants - l'éléphant sauvage. Il se retourna sur le dos de Pudmini, où il était étendu de tout son long, et dit :
- Qu'est-ce donc ? Je ne savais pas qu'il y eût un homme parmi les chasseurs de plaine assez malin pour lier même un éléphant mort.
- Ce n'est pas un homme, mais un enfant. Il est entré dans le keddah

, la dernière fois, et a jeté la corde à Barmao que voilà, pendant que nous tâchions d'éloigner de sa mère ce jeune éléphant qui a une verrue à l'épaule.

Machua Appa désigna du doigt Petit Toomai.

Petersen Sahib le regarda, et Petit Toomai salua jusqu'à terre.
- Lui, jeter une corde ? Il n'est pas plus haut qu'un piquet… Petit, comment t'appelles-tu ? dit Petersen Sahib.

Petit Toomai avait trop peur pour desserrer les dents, mais Kala Nag était derrière lui ; le gamin fit un signe, et l'éléphant l'enleva dans sa trompe et le tint au niveau du front de Pudmini, en face du grand Petersen Sahib. Alors, Petit Toomai se couvrit le visage de ses mains, car il n'était qu'un enfant et, sauf en ce qui touchait les éléphants, aussi timide qu'enfant au monde.
- Oh ! oh ! dit Petersen Sahib en souriant sous sa moustache, pourquoi donc avoir appris à ton éléphant ce tour-là ? Est-ce pour t'aider à voler le blé vert sur le toit des maisons, quand on met à sécher les épis ?
- Pas le blé vert. Protecteur du Pauvre… les melons, dit Petit Toomai.
