
Mon intention n'est point de retracer ici tous les incidents de ce proces. J'ai assez longuement rappele toutes les etapes de l'affaire pour ne point imposer aux lecteurs le defile nouveau des evenements entoures de leur mystere. J'ai hate d'arriver au moment vraiment dramatique de cette journee inoubliable. Il survint, comme maitre Henri-Robert posait quelques questions au pere Mathieu, qui, a la barre des temoins, se defendait, entre ses deux gendarmes, d'avoir assassine "l'homme vert". Sa femme fut appelee et confrontee avec lui. Elle avoua, en eclatant en sanglots, qu'elle avait ete "l'amie" du garde, que son mari s'en etait doute; mais elle affirma encore que celui-ci n'etait pour rien dans l'assassinat de son "ami". Maitre Henri-Robert demanda alors a la cour de bien vouloir entendre immediatement, sur ce point, Frederic Larsan.

"Dans une courte conversation que je viens d'avoir avec Frederic Larsan, pendant la suspension d'audience, declara l'avocat, celui-

ci m'a fait comprendre que l'on pouvait expliquer la mort du garde autrement que par l'intervention du pere Mathieu. Il serait interessant de connaitre l'hypothese de Frederic Larsan."

Frederic Larsan fut introduit. Il s'expliqua fort nettement.

"Je ne vois point, dit-il, la necessite de faire intervenir le pere Mathieu en tout ceci. Je l'ai dit a M. de Marquet, mais les propos meurtriers de cet homme lui ont evidemment nui dans l'esprit de M. le juge d'instruction. Pour moi, l'assassinat de Mlle Stangerson et l'assassinat du garde "sont la meme affaire".

On a tire sur l'assassin de Mlle Stangerson, fuyant dans la cour d'honneur; on a pu croire l'avoir atteint, on a pu croire l'avoir tue; a la verite il n'a fait que trebucher au moment ou il disparaissait derriere l'aile droite du chateau. La, l'assassin a rencontre le garde qui voulut sans doute s'opposer a sa fuite.

L'assassin avait encore a la main le couteau dont il venait de frapper Mlle Stangerson, il en frappa le garde au coeur, et le garde en est mort.

Cette explication si simple parut d'autant plus plausible que, deja, beaucoup de ceux qui s'interessaient aux mysteres du Glandier l'avaient trouvee. Un murmure d'approbation se fit entendre.

"Et l'assassin, qu'est-il devenu, dans tout cela? demanda le president.
- Il s'est evidemment cache, monsieur le president, dans un coin obscur de ce bout de cour et, apres le depart des gens du chateau qui emportaient le corps, il a pu tranquillement s'enfuir."

A ce moment, du fond du "public debout", une voix juvenile s'eleva. Au milieu de la stupeur de tous, elle disait: 

"Je suis de l'avis de Frederic Larsan pour le coup de couteau au coeur. Mais je ne suis plus de son avis sur la maniere dont l'assassin s'est enfui du bout de cour!"

Tout le monde se retourna; les huissiers se precipiterent, ordonnant le silence. Le president demanda avec irritation qui avait eleve la voix et ordonna l'expulsion immediate de l'intrus; mais on reentendit la meme voix claire qui criait: 

"C'est moi, monsieur le president, c'est moi, Joseph Rouletabille!"

Il y eut un remous terrible. On entendit des cris de femmes qui se trouvaient mal. On n'eut plus aucun egard pour "la majeste de la justice". Ce fut une bousculade insensee. Tout le monde voulait voir Joseph Rouletabille. Le president cria qu'il allait faire evacuer la salle, mais personne ne l'entendit. Pendant ce temps, Rouletabille sautait par-dessus la balustrade qui le separait du public assis, se faisait un chemin a grands coups de coude, arrivait aupres de son directeur qui l'embrassait avec effusion, lui prit "sa" lettre d'entre les mains, la glissa dans sa poche, penetra dans la partie reservee du pretoire et parvint ainsi jusqu'a la barre des temoins, bouscule, bousculant, le visage souriant, heureux, boule ecarlate qu'illuminait encore l'eclair intelligent de ses deux grands yeux ronds. Il avait ce costume anglais que je lui avais vu le matin de son depart - mais dans quel etat, mon Dieu! - l'ulster sur son bras et la casquette de voyage a la main. Et il dit: 

"Je demande pardon, monsieur le president, le transatlantique a eu du retard! J'arrive d'Amerique. Je suis Joseph Rouletabille! ..."

On eclata de rire. Tout le monde etait heureux de l'arrivee de ce gamin. Il semblait a toutes ces consciences qu'un immense poids venait de leur etre enleve. On respirait. On avait la certitude qu'il apportait reellement la verite... qu'il allait faire connaitre la verite...

Mais le president etait furieux: 

"Ah! vous etes Joseph Rouletabille, reprit le president... eh bien, je vous apprendrai, jeune homme, a vous moquer de la justice... En attendant que la cour delibere sur votre cas, je vous retiens a la disposition de la justice... en vertu de mon pouvoir discretionnaire.
- Mais, monsieur le president, je ne demande que cela: etre a la disposition de la justice... je suis venu m'y mettre, a la disposition de la justice... Si mon entree a fait un peu de tapage, j'en demande bien pardon a la cour... Croyez bien, monsieur le president, que nul, plus que moi, n'a le respect de la justice... Mais je suis entre comme j'ai pu..."
