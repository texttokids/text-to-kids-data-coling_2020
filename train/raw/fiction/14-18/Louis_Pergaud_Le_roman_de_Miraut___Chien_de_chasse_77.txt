
Songez-y! Pour un chien, c'est quelque chose.
-Lisée, supplia sa femme, changeant d'attitude et les larmes aux yeux, pour l'amour de Dieu, aie pitié de nous, aie pitié de moi!

Jamais tu ne retrouveras peut-être une telle occasion; songe à la vache qu'il faudra vendre, dix litres de lait par jour! Songe que ce ne serait sûrement pas tout, que les gardes t'en veulent, que les gendarmes t'épient, qu'ils nous feront tout vendre, qu'ils nous ruineront jusqu'au dernier liard.
-Vous en retrouverez un autre facilement, insista l'acheteur.

Une larme, qu'il essaya de refouler, monta aux yeux de Lisée; il se moucha bruyamment tandis que l'autre concluait: 
-Allons, topez là, et serrez-moi la main, c'est une affaire entendue. Allons boire un verre à l'auberge où j'ai laissé mon cheval.
-Il faut au moins que vous le voyiez, afin qu'il vous connaisse déjà un peu pour partir! Lisée va vous conduire à sa niche, proposa la Guélotte.
-Je le connais déjà, moi, répondit l'acquéreur.

Débarricadant les portes lentement, le cerveau lourd, sans penser, en homme accablé, Lisée arriva avec son compagnon à la remise où Miraut, attaché, sommeillait, son entrave au cou.
-Le voilà! annonça-t-il en le désignant du geste.

Et il s'approcha de l'animal qu'il caressa de la main et auquel il parla affectueusement.

L'étranger, le nouveau maître, suivait Lisée et ce fut sur lui que se porta d'instinct le regard du chien.

Tout d'abord, en apercevant Lisée, il ne s'était pas levé, se contentant de soulever la tête, de le regarder avec de grands yeux tristes et, ce qui témoignait chez lui de l'indécision, de frapper de sa queue, à coups réguliers et assez vifs, la paille de sa litière. Mais, dès qu'il aperçut cet autre humain, habillé différemment des gens qu'il avait coutume de voir, un chapeau sur la tête, un manteau sur le bras, l'inquiétude sourdement l'envahit. Une prescience vague lui dénonçait un danger et, Lisée restant malgré tout son protecteur naturel, ce fut vers lui qu'il se réfugia, vite debout, se frottant à son pantalon, lui léchant les mains et lui parlant à sa manière.

De même que les corbeaux et les chats chez qui la chose n'est pas douteuse, et sans doute tous les grands animaux sauvages, les chiens ont un langage articulé ou nuancé et se comprennent entre eux parfaitement. Miraut se faisait également entendre de Mique, de Mitis et de Moute, et ces derniers aussi lui tenaient assez souvent des discours brefs dans lesquels on se disait tout ce que l'on voulait se dire et rien que ça.

Sans que Lisée eût parlé, car s'il eût émis la moindre phrase relative à une séparation, le chien, qui comprenait tout ce qui se rapportait à lui, l'aurait certainement saisie dans tous ses détails, il sentit, rien qu'à son air triste, de même qu'à la volonté de l'autre de se faire bien voir, qu'il y avait entre eux deux un pacte secret le concernant.

Instinctivement il fuyait les caresses de l'étranger, se contentant de le regarder avec des yeux inquiets, agrandis par la tristesse et l'étonnement.

Les compliments que l'autre lui adressa, pour sincères que les sentît Miraut, ne réduisirent point sa méfiance et il refusa froidement un bout de sucre qui lui fut tendu en signe d'alliance.

Lisée ayant ramassé le morceau tombé le décida tout de même à le croquer, mais il le cassa sans enthousiasme et l'avala sans le sentir.
-Je vais toujours lui ôter l'entrave, décida l'acheteur qui s'était nommé M. Pitancet, rentier au Val.

Mais ce geste libérateur qui, pensait-il, lui concilierait les bonnes grâces et lui attirerait l'amitié du chien, ne réussit qu'à accentuer sa méfiance et à confirmer ses soupçons.

Le nez humide et les yeux brillants, il se collait de plus en plus aux jambes de son ancien maître qui ne se lassait de le cajoler, de le tapoter, triste jusqu'à la mort de la séparation prochaine.

Après une dernière embrassade, une dernière caresse, on laissa Miraut sur sa litière et, pour régler définitivement l'affaire, les deux hommes se rendirent à l'auberge.
-Comment avez-vous su que mon chien était à vendre? questionna Lisée.
-Ma foi, répliqua l'autre, à vous dire la vérité, je n'en ai été à peu près sûr qu'en arrivant à Velrans où l'aubergiste m'a confirmé la chose. Je vous avouerai toutefois que je me doutais bien qu'un jour ou l'autre vous seriez obligé de vous en débarrasser, car je me suis trouvé par hasard au tribunal à tous vos procès et je puis bien, entre nous, vous dire que les juges se sont montrés avec vous de fameuses rosses. Depuis longtemps je connais de réputation votre chien et, comme j'ai l'intention de chasser cet automne, je me suis dit: «Puisque tu n'es pas très habile ni très connaisseur, un bon animal au moins t'est nécessaire.» C'est pourquoi, après votre dernière condamnation, j'ai décidé à tout hasard que je monterais jusqu'ici au-dessus. On m'a bien prévenu, à Velrans, qu'il serait assez dur de vous décider, mais que votre femme, elle, ne voulait plus entendre parler de le garder, et je suis venu.
-Mon pauvre Miraut! gémit Lisée.
-Soyez tranquille, le rassura M. Pitancet, il sera bien soigné chez moi; nous n'avons à la maison ni chat ni gosses et ma femme ne déteste pas les chiens.
-Une si bonne bête! reprenait Lisée.
