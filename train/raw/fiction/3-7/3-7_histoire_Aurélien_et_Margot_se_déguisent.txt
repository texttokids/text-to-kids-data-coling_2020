﻿Aujourd’hui Aurélien et Margot passent l’après-midi ensemble. Ils cherchent quelle activité faire tout les deux.
Quand Margot à l’idée d’aller fouiller dans la grosse malle à l’étage. C’est là où sont rangés tout les déguisements Aurélien !
En fouillant dans la malle Aurélien trouve un super costume de pirates et Margot un joli costume de fée avec une belle baguette magique.
En continuant de chercher ils découvrent un costume de prince et princesse, avec de jolies couronnes. Tout les deux sont très fiers.
Ensuite, Aurélien et Margot dénichent un déguisement de cow-boy et de danseuse vahiné. Aurélien adore son étoile de shérif et Margot son collier de fleurs.
Plus au fond dans la malle, Margot trouve un beau déguisement d’éléphant, ce qui fait beaucoup rire Aurélien, qui lui à trouver une superbe cape de super-héros.
Pour finir Margot et Aurélien trouvent deux autres costumes : un de vampire et l’autre de clown. Grâce à de vieux déguisements ils ont pu passer une superbe après-midi !
