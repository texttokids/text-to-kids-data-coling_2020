N°27 - 13 au 19 octobre 2017
l'indépendance de la Catalogne
L’Espagne, un pays de l’Union européenne situé juste au sud de la France, connaît en ce moment une grosse crise. L’une de ses régions, la Catalogne, ne veut plus faire partie de ce pays. Mais l’Espagne n’est pas d’accord. Pourquoi la Catalogne veut-elle être un pays à part ? Qu’est-ce que cette région ? Et est-ce la seule en Europe à réclamer plus de pouvoirs ? Je t’explique tout ce qu’il se passe.


Uma, 9 ans, habite en Catalogne

Uma, 9 ans, vit en Catalogne. Elle habite avec sa famille à Sabadell, une grande ville dans la banlieue de Barcelone, la capitale de cette région espagnole.
Dans son école publique, elle parle presque tout le temps en catalan, la langue de sa région. Le lundi après-midi, elle a des cours d’espagnol, la langue de son pays. Avec sa famille et la plupart de ses amis, Uma parle catalan. Même si elle s’exprime beaucoup plus en catalan, Uma est bilingue, ça veut dire qu’elle parle parfaitement les deux langues. «Chaque semaine, je parle aussi espagnol avec Rita, une amie de ma maman qui vient de Madrid, la capitale de l’Espagne», raconte-t-elle.
En Catalogne, certains habitants veulent avoir leur propre pays, séparé de l’Espagne. Le 1er octobre, les responsables politiques de la Catalogne ont organisé un vote. «Les gens devaient dire s’ils voulaient que la Catalogne s’en aille de l’Espagne», explique Uma. Un bureau de vote a été installé dans son école. Mais comme le vote était interdit par la justice espagnole, des policiers sont venus pour empêcher les gens de voter. Ils ont cassé des portes pour entrer dans le bâtiment. «Je suis un peu triste, parce que ce n’est pas drôle quand on casse des choses dans ton école», dit Uma.
Pour elle, ce qu’il se passe en Catalogne est «un sujet compliqué, un sujet de grandes personnes». Ses parents veulent que la région ne fasse plus partie de l’Espagne. Uma, elle, dit que son pays, c’est la Catalogne. D’ailleurs, ses fêtes préférées sont catalanes. Elle aime beaucoup la «Festa Major», une fête de village où les hommes, les femmes et les enfants forment des pyramides impressionnantes en montant les uns sur les autres. Parfois, ils arrivent ainsi à créer des tours humaines de 10 étages !
Sabadell, la ville où habite Uma en Catalogne


Que se passe-t-il en Espagne ?

Mardi 10 octobre, Carles Puigdemont, le président du gouvernement de Catalogne, a déclaré que sa région avait «gagné le droit d’être un Etat indépendant» et de quitter ainsi l’Espagne, un pays situé au sud de la France. Ça ne se fera pas tout de suite parce qu’il veut d’abord reprendre le dialogue avec le gouvernement espagnol qui n’est pas du tout d’accord avec cette décision.
Carles Puigdemont a fait cette annonce après avoir organisé le 1er octobre un référendum, c’est-à-dire un vote, pour demander à sa population si elle était pour ou contre l’indépendance de la Catalogne. Le «oui» l’a emporté avec 90% des voix mais seulement 42,3% des électeurs ont voté, dans des conditions très difficiles.
Ce référendum a en effet été organisé alors qu’il était interdit par le gouvernement espagnol. Pourquoi ? Parce que le gouvernement dit que l’Espagne ne peut pas être divisée, comme c’est écrit dans la Constitution espagnole, le texte qui fixe les lois du pays. Malgré l’interdiction, des milliers de Catalans sont allés voter mais la police espagnole est intervenue pour les en empêcher et ça a été très violent. Il y a eu des centaines de blessés.
Des manifestations ont ensuite été organisées en Catalogne par ceux qui sont pour l’indépendance puis par ceux qui sont contre.
Et le gouvernement espagnol a fait comprendre qu’il pourrait aller jusqu’à prendre le contrôle de la Catalogne pour éviter qu’elle ne devienne indépendante.


Qu'est-ce que la Catalogne ?

La Catalogne est une région située au nord-est de l’Espagne, un pays situé au sud de la France. Elle compte 7,5 millions d’habitants. Sa capitale, Barcelone, est la deuxième ville la plus peuplée du pays après Madrid, la capitale de l’Espagne. La Catalogne est l’une des régions les plus riches du pays grâce au tourisme et à l’industrie (fabrication de voitures, de vêtements…).
L’Espagne a 17 régions qui sont ou plus ou moins autonomes, c’est-à-dire qui ont plus au moins de pouvoirs par rapport au gouvernement. La France compte, elle, 13 régions qui ont beaucoup moins de pouvoirs qu’en Espagne. Chacune des régions espagnoles gère de nombreux domaines comme les écoles, l’agriculture, la santé… La Catalogne est l’une des 4 régions qui a le plus de pouvoirs avec le Pays basque, la Galice et l’Andalousie.
Elle a un Parlement et un gouvernement dont le président s’appelle Carles Puigdemont. Elle s’occupe de l’éducation, de la santé, de la culture, des transports ou de la justice sur son territoire. Elle a même sa propre police. En revanche, ce n’est pas elle mais le gouvernement espagnol qui choisit les règles concernant la défense, les relations internationales et les impôts.
Dans cette région, les habitants parlent deux langues officielles : l’espagnol (la langue de tout le pays, qu’on appelle aussi le castillan) et le catalan. A l’école, la grande majorité des cours sont donnés en catalan mais tout le monde parle très bien les deux langues.
* Traduction : «Bonjour, comment ça va ?»


Pourquoi veut-elle être indépendante ?

Certains indépendantistes pensent que l’Espagne profite de la Catalogne, qui est une région riche. En effet, le gouvernement espagnol collecte la majorité des impôts puis répartit ensuite l’argent entre les 17 régions. Les régions les plus riches donnent plus d’argent qu’elles n’en reçoivent. Certains Catalans pensent donc qu’ils ont plus à perdre qu’à gagner en restant dans l’Espagne.
Mais ce n’est pas qu’une question d’argent. Les indépendantistes veulent que la Catalogne soit reconnue comme une «nation». «Pour eux, l’Espagne est un Etat plurinational, c’est-à-dire avec plusieurs nations à l’intérieur», explique Stéphane Michonneau, professeur d’histoire spécialiste de l’Espagne à l’université de Lille. Sauf que, pour le gouvernement espagnol, il n’y a qu’une seule nation possible : la nation espagnole.
Dans cette région, les habitants sont fiers de leur identité. Les Catalans ont leur propre langue, leur propre drapeau, leur histoire. La Catalogne a déjà été autonome en 1932 : elle avait son propre gouvernement. Mais la dictature du général Franco, de 1939 à 1975, l’a supprimée et «la répression culturelle a été féroce : les habitants ne pouvaient plus parler catalan et tous leurs symboles, comme leur drapeau, ont été interdits», raconte Stéphane Michonneau, historien, spécialiste de l’Espagne.
Puis la Catalogne est devenue une région autonome. En 2006, un vote au Parlement espagnol la définit comme une «nation» dans l’Etat espagnol. Mais en 2010, à la demande du nouveau gouvernement au pouvoir, la justice du pays a annulé ce vote. De nombreux Catalans se sont sentis humiliés et ont manifesté à Barcelone. «C’est à ce moment-là que l’indépendantisme a décollé», remarque Stéphane Michonneau.
Depuis, le dialogue entre le gouvernement espagnol et les indépendantistes semble impossible.


Est-ce la seule région à demander plus de pouvoirs ?

La Catalogne n’est pas la seule à réclamer son indépendance. Plusieurs autres régions des pays de l’Union européenne souhaitent plus d’autonomie. Voici quelques exemples.
L’Ecosse est considérée par tout le monde comme une nation mais ce n'est pas un Etat indépendant. Elle fait en effet partie du Royaume-Uni et a son Parlement, son gouvernement. Lors d’un vote organisé en 2014, les habitants ont voté non à l’indépendance à 55,4%. Mais le gouvernement écossais veut leur poser une nouvelle fois la question depuis que le Royaume-Uni a décidé de quitter l’Union européenne (on appelle ça le Brexit) alors que de nombreux Ecossais veulent continuer à en faire partie.
Ce territoire se partage entre l’Espagne et la France. Côté espagnol, comme ici sur la carte, le Pays basque est une région autonome dans laquelle il y a deux langues officielles : l’espagnol et l’euskara (la langue basque). Cette région a beaucoup de pouvoirs : sa propre police, son Parlement, son gouvernement… Certaines personnes réclamaient l’indépendance par la violence, regroupées dans l’organisation terroriste ETA, qui a fait 850 victimes entre 1968 et 2011. Aujourd’hui, la majorité des Basques souhaitent plus d’autonomie plutôt que l’indépendance.
C’est une région française différente des autres parce que c’est une île mais aussi parce que le gouvernement français lui a donné plus de compétences pour calmer les indépendantistes qui avaient choisi la violence (le mouvement terroriste Front de libération national de la Corse, ou FLNC). La Corse s’occupe donc par exemple des routes, des lycées, des collèges et des universités. Certains habitants communiquent en corse, une langue qui est aussi parlée dans quelques écoles primaires. La majorité des Corses semble plus favorable à davantage d’autonomie qu’à l’indépendance.
Ces deux riches régions situées dans le nord de l’Italie vont chacune organiser un référendum ce mois-ci pour savoir si les habitants veulent que leur région soit plus autonome. Si la participation est importante, ça donnera plus d’importance au parti de la Ligue du Nord, qui dirige ces deux régions et qui est un parti xénophobe.
C’est l’une des trois régions de la Belgique. C’est la plus peuplée et la plus riche du pays. Ses habitants parlent le flamand. Elle bénéficie d’une autonomie assez importante dans le pays avec un Parlement et un gouvernement. Certains partis réclament encore plus d’autonomie, sans forcément aller jusqu’à l’indépendance.


