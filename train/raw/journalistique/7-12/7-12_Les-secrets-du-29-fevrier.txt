N°6 - février 2016
Les secrets du 29 février 
Cette année, c’est exceptionnel, il y aura un 29 février. Ce n’était pas arrivé depuis quatre ans ! 2016 compte en effet 366 jours au lieu de 365 : c’est ce qu’on appelle une année bissextile. Mais pourquoi cette bizarrerie ? «Le P’tit Libé» vous explique tout.


Le portrait de Malo, né un 29 février

Environ 4 millions de personnes dans le monde sont nées un 29 février. C’est le cas de Malo, 12 ans, qui habite dans la région parisienne. Cette année, il va fêter son anniversaire le jour de sa naissance, ce qui ne lui était arrivé que deux fois dans sa vie : à 4 ans et à 8 ans. Ses copains disent en riant qu’il va donc avoir trois ans cette année ! Et ça l’amuse aussi.
Quand sa maman montre sa carte d’identité aux gens, on lui dit souvent que son fils n’a pas de chance d’être né un 29 février. Malo, au contraire, est assez fier de sa particularité.
D’habitude, il souffle ses bougies le 28 février au soir, avec ses parents, et ça ne le dérange pas. Comme personne d’autre dans son entourage ne fête son anniversaire le 29 février, tout le monde pense à le lui souhaiter le jour d’avant ou celui d’après. Ça prolonge donc le plaisir !
Malo dit qu’il n’attend pas particulièrement cette grande journée cette année, mais il va quand même partir à l’étranger pour l’occasion, avec ses parents : ce sera à Rome, en Italie, ou dans le sud du Portugal, il ne sait pas encore. Et sa mère lui prépare aussi une petite surprise au retour des vacances...


Qu'est-ce qu'une année bissextile ?

Lorsque le calendrier compte un 29 février, c’est qu’on est dans une année bissextile. C’est assez exceptionnel, puisque ça n’arrive que tous les quatre ans. Il est très spécial, le mois de février : déjà, en temps normal, il ne compte que 28 jours alors que tous les autres mois de l’année ont 30 ou 31 jours ; mais en plus, c’est à lui qu’on ajoute un jour supplémentaire.
Pour comprendre cette histoire bizarre de 28 et 29 février, il faut savoir que l’on mesure le temps d’après les mouvements de la Terre dans l’espace. Un jour, c’est le temps que met notre planète à tourner sur elle-même. Et une année, c’est le temps qu’il lui faut pour faire le tour du Soleil.
Entre le moment où la Terre quitte son point de départ et le moment où elle y revient après avoir fait un tour complet, elle a eu le temps de faire 365 tours sur elle-même, plus un quart de tour supplémentaire. Dans une année, il y a donc 365 jours et quart.
Ça ne tombe pas sur un chiffre rond, ce qui est un peu embêtant. Si on avait un calendrier qui respecte exactement l’année solaire, cela décalerait le passage à l’année suivante d’un quart de journée (soit 6 heures) à chaque fois. Une fois, on fêterait le nouvel an à minuit, l’année suivante à 6 heures du matin, puis l’année d’après à midi… Et au bout de mille ans, l’hiver arriverait en plein mois d’août. N’importe quoi !
Il est plus pratique de forcer notre calendrier à avoir un chiffre rond. Dans l’Antiquité, à l’époque des Romains, il a donc été décidé qu’une année durerait 365 jours tout pile. Et une fois tous les quatre ans, on compense l’avance qu’on a prise en ajoutant au calendrier les quatre quarts de journée laissés de côté, soit une journée entière. Cette journée est le 29 février (nous verrons pourquoi un peu plus loin).


Qui a inventé le 29 février ?

L’année bissextile est une idée de Jules César. A l’époque de la Rome antique, il y a plus de 2 000 ans, on avait remarqué que le calendrier était décalé par rapport à l’année solaire. Normal : les Romains avaient découpé l’année en douze mois de 29 et 31 jours, ce qui fait que le calendrier ne comptait que 355 journées (alors que la Terre tourne autour du Soleil en 365 jours et quart). Le calendrier romain a donc pris de l’avance sur la position réelle de la Terre autour du Soleil. De plus en plus d’avance… Jusqu’à 90 jours. Quand il y a un décalage aussi grand, les saisons sont décalées : il fait encore un froid hivernal quand on arrive en avril !
Pour rééquilibrer tout ça, les Romains organisaient parfois de petits rattrapages de calendrier en rajoutant des jours par-ci par-là. Mais les citoyens qui habitaient loin de la capitale, Rome, n’étaient souvent même pas au courant, car personne ne leur avait apporté la nouvelle ! Personne n’y comprenait plus rien. L’an -46 était un tel bazar que les Romains l’ont appelé «l’année de la confusion». Alors, il a été décidé de rattraper tout le décalage d’un seul coup. Il a fallu inventer deux nouveaux mois, complètement artificiels, et les ajouter à la fin de l’année : octobre, novembre, «premier mois intercalaire», «deuxième mois intercalaire», décembre. Cette année-là a été très longue : 445 jours !
Ensuite, il fallait trouver un système qui fonctionne mieux. Jules César a demandé l’aide de l’astronome grec Sosigène pour créer un nouveau calendrier. Sosigène ne s’est pas beaucoup cassé la tête : il a proposé de copier sur les Egyptiens, qui avaient un calendrier à 365 jours. Il suffisait d’y ajouter un jour supplémentaire tous les quatre ans pour rester en phase avec l’année solaire. On a appelé ce nouveau système le calendrier julien en l’honneur de Jules César. Et tant pis pour les Egyptiens, qui étaient les vrais «inventeurs» de ce jour supplémentaire, mais qui n’ont pu le mettre en place que vingt ans plus tard à cause de disputes entre les prêtres qui prenaient ce genre de décisions importantes.
Sous Jules César, une grande fête de dix jours était organisée chaque année en l’honneur du dieu de la guerre, Mars. On appelait cette fête les «calendes de mars». Elle a donné son nom au mois de mars durant lequel elle se déroulait, et elle était tellement importante qu’elle marquait aussi le début de l’année. L’année allait ainsi de mars à février. Quand il a fallu ajouter un jour supplémentaire dans le nouveau calendrier julien, les Romains l’ont donc placé à la fin de l’année. Ils ont choisi de créer un deuxième 23 février : en latin, on disait ante diem bis sextum Kalendas Martias, c’est-à-dire «sixième jour bis avant les calendes de mars». Un peu compliqué, n’est-ce pas ? Pour aller plus vite, on disait le «sixième jour bis», en latin bis sextus. C’est de là que vient le mot «bissextile».
Beaucoup plus tard, en 1582, on a décidé que le jour supplémentaire des années bissextiles ne serait plus le 23 mais le 29 février. Comme c’était une initiative du pape Grégoire 13, on a changé le nom du calendrier pour l’appeler calendrier grégorien.
Depuis, presque rien n’a changé. On a encore connu un nouveau calendrier à l’époque de la Révolution française. Quand les révolutionnaires ne voulaient plus avoir de roi et ont proclamé à la place une République, en 1792, ils ont marqué le coup en inventant de nouveaux noms de mois : «floréal» pour la saison des fleurs (en mai), «pluviôse» pour la saison de la pluie (en février)... Mais le calendrier républicain n’a pas duré longtemps : la France est revenue au bon vieux calendrier grégorien en 1806.


Le 29 février , un jour à part ?

Puisque le 29 février est rare, il est lié à de nombreuses histoires et traditions originales.
Naître un 29 février, c’est déjà rare. Mais avoir des frères et sœurs nés aussi un 29 février, c’est exceptionnel ! Et pourtant ça existe. En Norvège, une maman a eu trois enfants, en 1960, en 1964 et en 1968, et ils sont tous nés ce jour si particulier. Cette femme dit avoir un petit secret pour accoucher à cette date, mais elle ne veut pas le révéler. C’est un record mondial, codétenu par une famille américaine : dans l’Utah, une mère a accouché trois fois un 29 février, en 2004, en 2008 et en 2012.
Selon une vieille légende irlandaise, les deux patrons des catholiques du pays, sainte Brigitte et saint Patrick, ont un jour conclu un accord : les femmes auront le droit de faire leur demande en mariage aux hommes tous les quatre ans, le 29 février. Traditionnellement, c’étaient les hommes qui faisaient leur demande.
C’est devenu une coutume au Royaume-Uni. Pour que cela soit légal, une loi a même été adoptée en 1228 en Ecosse. Si un homme avait le malheur de dire non à une femme, il devait payer une amende ou, dans certaines régions, offrir une paire de gants. La femme célibataire pouvait ainsi cacher le fait qu’elle n’ait pas de bague de fiançailles au doigt.
Aujourd’hui encore, même si chacun fait comme il veut, on encourage les femmes du Royaume-Uni à déclarer leur flamme le 29 février.
Selon un proverbe paysan, les années bissextiles sont mauvaises pour les récoltes : «En l’année bissextile, garde du blé pour l’an qui suit.» En Ecosse, les superstitieux pensent qu’un enfant né le 29 février aura la poisse (un peu comme un vendredi 13). Et en Grèce, se marier ce jour-là porterait malheur.
Un journal humoristique ne paraît que tous les quatre ans depuis 1980 : la Bougie du sapeur. Son nom a été choisi en hommage au héros de bande dessinée le sapeur Camember, un soldat un peu bête né un 29 février. En trente ans d’existence, seuls neuf numéros sont donc sortis. Cette année, ce sera le dixième.
Quand on travaille, on gagne un salaire chaque mois. Mais que le mois de février dure 28 ou 29 jours, la somme d’argent ne change pas. Et le dessinateur français Lukino, qui fête son anniversaire tous les quatre ans, ne trouve pas ça normal : si on travaille un jour de plus, on devrait être payé un jour de plus ! Il demande donc, avec humour, que le 29 février devienne un jour férié. Bon, ce n’est pas gagné : sa pétition n’a recueilli que 135 signatures.
Aux Etats-Unis, la petite ville d’Anthony organise une grande parade dans les rues chaque 29 février pour fêter les anniversaires de toutes les personnes nées ce jour-là. L’idée est venue de deux habitants de la ville qui voulaient une journée spéciale pour leur anniversaire. Des gens de tout le pays et du monde entier viennent y participer.
C’est un moyen de s’en souvenir : les Jeux olympiques d’été, qui ont lieu tous les quatre ans depuis 1886, tombent toujours sur une année bissextile. Cette année, les JO d’été auront lieu à Rio de Janeiro au Brésil du 5 au 21 août.
Et cela tombe aussi en même temps que l’Euro de foot (le championnat d’Europe de football), organisé tous les quatre ans depuis 1960. Cette année, il se déroulera en France du 10 juin au 10 juillet.


Que va-t-on faire de cette journée ?

On l’aura compris, 2016 compte un jour supplémentaire par rapport à 2015. Et il s’en passe des choses en une journée !
Puisque ça tombera un lundi, certains passeront le 29 février à l’école. Tous ne seront peut-être pas ravis, mais pas de quoi s’arracher les cheveux pour autant : chaque personne perd déjà 60 cheveux par jour, en moyenne. Heureusement, ils repoussent !
Et puis pour les gourmands, ça fera un goûter en plus. Rien qu’en France, ce jour-là, on avalera 32 millions de baguettes de pain et plus de 200 000 kilos de Nutella.
Pas étonnant que les toilettes fonctionnent autant : les Français utiliseront, comme tous les jours, près de 274 millions de litres d’eau en tirant la chasse.
Les enfants qui habitent dans la zone C (à Paris, Montpellier ou Toulouse, par exemple) seront, eux, en vacances le 29 février. L’occasion de faire du tourisme, par exemple en allant visiter la tour Eiffel à Paris, comme 19 178 personnes le font en moyenne chaque jour...
… et d’envoyer une photo souvenir par texto à leur famille. Elle voyagera avec les 567 millions de messages échangés par téléphone chaque jour en France.
Plusieurs de ces messages annonceront un heureux événement, car 2 243 bébés naissent quotidiennement dans notre pays.
En grandissant, ils deviendront peut-être serveurs, comptables ou musiciens, mais peut-être y aura-t-il dans le lot un astronaute ? Aujourd’hui, ils sont six dans la Station spatiale internationale, et ils font 16 fois le tour de la Terre chaque jour.
Entre deux expériences scientifiques, les astronautes peuvent surfer sur Internet et envoyer des mails. Ils sont les seuls à le faire depuis l’espace, mais leurs messages se mélangeront aux plus de 200 milliards de mails envoyés en un jour dans le monde.


Pourquoi tout le monde n'est pas en 2016 ?

Selon le calendrier persan, qui est notamment utilisé en Iran, on est aujourd’hui en 1394. Selon le calendrier bouddhiste, utilisé dans certains pays d’Asie, nous sommes en 2558. Ça peut paraître surprenant, mais il y a une explication.
Tous les calendriers du monde n’ont pas été créés en même temps, donc il y a des décalages entre eux. On a fait démarrer le calendrier grégorien (le nôtre) quand Jésus est né (même si en fait on ne sait pas précisément en quelle année c’était), mais chaque calendrier utilise un événement différent comme point de départ. En plus, tous ne fonctionnent pas de la même façon : la plupart des pays utilisent le calendrier grégorien, qui est un calendrier solaire (il se fonde sur la position de la Terre par rapport au Soleil), mais il existe aussi des calendriers lunaires (dont les dates sont fixées en fonction de la Lune) et des calendriers luni-solaires, c’est-à-dire définis à la fois selon le Soleil et la Lune.
Voici trois autres exemples de calendriers différents du nôtre.
Il est utilisé par les musulmans du monde entier, car il sert à fixer la date des fêtes religieuses. Dans la plupart des pays, les croyants utilisent aussi le calendrier grégorien pour la vie quotidienne. Mais dans certains autres, comme l’Arabie Saoudite, le calendrier musulman est le calendrier officiel.
En 622, le prophète (c’est-à-dire le porte-parole de Dieu) Mahomet est parti de La Mecque pour aller à Médine, deux villes d’Arabie Saoudite. C’est un événement tellement important dans l’islam, la religion des musulmans, qu’il a été choisi comme point de départ de leur calendrier. Résultat, en ce moment, si on suit le calendrier musulman, nous sommes en 1437, et non en 2016.
Les mois s’appellent par exemple mouharram, safar ou ramadan.
Il s’agit d’un calendrier lunaire : chaque mois correspond à une lunaison (la période entre le moment où la Lune apparaît et celui où elle disparaît), c’est-à-dire environ 29 jours. Chaque année est donc plus courte qu’une année solaire, et le nouvel an «avance» d’environ dix jours chaque année par rapport au calendrier grégorien. L’an dernier, l’année commençait le 14 octobre, et cette année ce sera autour du 3 octobre. L’an prochain, elle débutera en septembre.
C’est le calendrier des juifs. Il est utilisé en Israël pour fixer les dates des fêtes religieuses. Dans ce pays, on utilise aussi le calendrier grégorien pour tout ce qui n’est pas religieux.
Le calendrier hébraïque démarre en -3761. Selon les croyances juives, c’est cette année-là que le monde a été créé. Si on suit le calendrier hébraïque, on est en 5776, et non en 2016.
Les mois s’appellent tichri, kislev ou encore tamouz.
Chaque semaine commence le dimanche, et non le lundi, et se termine le samedi. Les juifs pratiquants ne travaillent jamais le samedi.
Au quotidien, la Chine utilise le calendrier grégorien. Mais pour célébrer certaines fêtes religieuses ou traditionnelles, comme le nouvel an, elle a aussi un calendrier chinois. Dans celui-ci, chaque année porte le nom d’un animal. Ainsi, 2016 est l’année du singe. 2015 était l’année de la chèvre et 2017 sera celle du coq.
Cette année, le nouvel an chinois a lieu le 8 février. La date change tous les ans, mais la nouvelle année démarre toujours entre fin janvier et fin février.
Les mois s’appellent par exemple zhengyuè, siyuè ou bayuè.


