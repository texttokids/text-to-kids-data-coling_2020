N°57 – 11 mai au 17 mai 2018
Mai 68
Il y a 50 ans, en mai 1968, des millions de personnes ont arrêté de travailler et ont manifesté dans toute la France. Cet événement s’appelle Mai 68. Il a été lancé par des étudiants puis a touché toutes les professions. Ces personnes voulaient une vie meilleure et rejetaient le monde dans lequel elles vivaient : elles le trouvaient injuste. Que s’est-il passé exactement ? Pourquoi en parle-t-on encore aujourd’hui et est-ce que ce grand mouvement de révolte a changé des choses ? Cette semaine, je te fais revivre ce grand événement qui a secoué le pays.


Florence avait 10 ans en mai 1968

Florence a 60 ans. En mai 1968, elle avait donc 10 ans. Elle habitait à ce moment-là avec ses parents et ses trois sœurs près de la tour Eiffel, dans le VIIe (7e) arrondissement de Paris.
A l’époque, il n’y avait pas Internet, pas de téléphone portable et peu de gens avaient la télévision. Florence allait la regarder chez ses grands-parents qui habitaient deux étages plus haut, dans le même immeuble qu’elle. C’est devant leur télé qu’elle a découvert ce qu’il se passait à Paris en mai 1968. Elle se souvient des images en noir et blanc (il n’y avait que deux chaînes à l’époque, une seule était en couleur) qui montraient des étudiants parisiens affronter des policiers. Elle se souvient de la fumée, des manifestations. «Ça me faisait un peu peur, ça avait l’air brutal.»
Ces affrontements se déroulaient surtout dans le Ve (5e) arrondissement de Paris. Ça lui paraissait loin : Florence ne s’aventurait pas en dehors de son quartier. «Je ne me sentais pas menacée, parce que je ne me sentais pas concernée. Autour de moi, la vie continuait normalement.» Le discours de son père la rassurait aussi. Ce professeur en école d’ingénieur trouvait «formidable» ce qui se passait. «Il en parlait beaucoup. Il trouvait ça bien que les étudiants cherchent à faire bouger les choses.»
En septembre 1968, Florence est entrée en sixième. C’est à ce moment-là qu’elle a observé des changements, alors que les manifestations de Mai 68 étaient terminées. «Les rapports avec les enseignants ont changé : les élèves leur répondaient plus facilement, alors que tout le monde était très discipliné avant.»
En cours, les élèves devaient porter des blouses. Mais là, «les plus grandes ont commencé à ouvrir un bouton de leur blouse, puis deux, puis trois. Trois mois plus tard, plus personne ne portait de blouse. Pour moi, c’est vraiment une conséquence de Mai 68», remarque-t-elle aujourd’hui.
A l’époque, comme on peut le voir sur la photo ci-dessus, Florence avait cours avec des filles seulement. Il y avait des écoles pour les filles et des écoles pour les garçons. Mais, un an après 1968, des garçons sont arrivés dans le même collège que Florence. Pour elle, c’était «une révolution» !
Paris, la ville où habitait Florence en Mai 68


Que s’est-il passé en France il y a 50 ans ?

Il y a 50 ans, en mai 1968, la France a été secouée par un grand mouvement de révolte. Environ 10 millions de personnes se sont mises en grève et il y a eu des manifestations dans tout le pays jusqu’au mois de juin. Ce mouvement a été lancé par des étudiants qui voulaient changer les choses. Ils protestaient notamment contre les inégalités, refusaient l’autorité des adultes et souhaitaient être plus libres.
Le 22 mars 1968, des étudiants de l’université de Nanterre (près de Paris) ont occupé des bureaux parce qu’ils demandaient de tous pouvoir circuler à la fois dans les bâtiments des filles et des garçons, ce qui n’était pas autorisé à l’époque.
Face à cette agitation, l’université a été fermée et les étudiants en colère ont occupé une autre université située en plein Paris : la Sorbonne. Le 3 mai, la police est intervenue et a arrêté des centaines d’étudiants. Plus de 2 000 d’entre eux, aidés par d’autres jeunes, ont alors affronté les policiers toute la nuit. Ils utilisaient des gaz lacrymogènes contre les manifestants. Ces derniers leur lançaient des pavés.
Le Quartier latin, où se situe la Sorbonne, était devenu «un champ de bataille. On ne pouvait plus circuler en voiture, les arbres avaient été arrachés tout comme les pavés qui servaient de projectiles. Les étudiants avaient pris la rue», raconte l’historien Eric Alary, spécialiste de la vie quotidienne des Français.
Dans la nuit du 10 au 11 mai, il y a eu de nouveaux affrontements entre la police et les étudiants, qui ont fait de nombreux blessés.
Les étudiants qui manifestaient ont rapidement été rejoints par des ouvriers. Ils se sont mis en grève, ont arrêté de travailler pour réclamer de meilleures conditions de travail. De nombreux travailleurs, de quasiment tous les métiers, ont aussi fait grève juste après.
«Les gens se sont réunis pour prendre la parole et réfléchir à la façon dont ils pouvaient améliorer la vie», raconte l’historienne Ludivine Bantigny.


Pourquoi en parle-t-on encore aujourd’hui ?

On parle encore beaucoup de Mai 68 d’abord parce que les jeunes de cette époque étaient nombreux et vivent toujours aujourd’hui (ils sont en grande majorité à la retraite). En 1968, plus d’1 Français sur 3 avait moins de 21 ans. Beaucoup d’enfants étaient en effet nés juste après la Seconde Guerre mondiale (1939-1945) parce que les Français étaient heureux que la guerre soit finie.
Ces jeunes des années 60 «ont vécu dans un monde en reconstruction d’après-guerre, qui n’était pas facile. Ils souhaitaient une autre vie que celle de leurs parents, qui travaillaient beaucoup, notamment dans les usines ou les mines», raconte l’historien Philippe Artières, directeur de recherche au Centre national de la recherche scientifique (CNRS).
A cette époque, la majorité n’était pas à 18 ans, mais à 21 ans. Les jeunes avaient envie de liberté. Ils en avaient marre de devoir obéir aux adultes. Ils ne se sentaient pas représentés par ceux qui dirigeaient le pays, qui étaient beaucoup plus âgés.
Dans les années 60, les jeunes écoutaient la musique sur des vinyles, des disques de plastique de 30 centimètres de diamètre. La musique rock était notamment très écoutée, avec des groupes anglais comme les Beatles ou les Rolling Stones. Ils avaient envie de danser, de s’amuser. Ce sont donc de jolis souvenirs pour celles et ceux qui l’ont vécu.
Mai 68 représente «un grand moment de bonheur pour ceux qui y ont participé, explique l’historienne Ludivine Bantigny. Les gens ont tout à coup cessé le travail, le train-train quotidien. Il y avait vraiment de la joie, de la poésie. Les manifestants avaient le sentiment que quelque chose pouvait changer.» Ils rejetaient toute forme d’autorité : celle des parents sur les enfants, des patrons sur les salariés ou encore des maris sur leurs femmes.
A l’inverse, d’autres s’en souviennent de façon moins positive : «La Seconde Guerre mondiale n’était pas loin et des Français avaient peur d’une guerre civile. Des gens faisaient des réserves de sucre, de pâtes, de riz…» rappelle Ludivine Bantigny.
Comme les éboueurs étaient aussi en grève, il y avait des poubelles un peu partout dans les rues. Les trains, les métros et les bus se sont arrêtés et il était par exemple difficile de trouver de l’essence dans le pays à la fin du mois de mai. Des magasins étaient dévalisés et des gens faisaient la queue dans les banques pour récupérer leur argent parce qu’ils étaient inquiets. Le 30 mai, il y a d’ailleurs eu une grande contre-manifestation à Paris pour soutenir le président français.
Cinquante ans après, les personnes qui ont vécu Mai 68 gardent donc des souvenirs très forts de cette période, qu’ils soient positifs ou non.


Qu’est-ce que ça a changé concrètement ?

Avec Mai 68, il y a eu des changements dans le travail, dans l’enseignement et au gouvernement.
Le 27 mai 1968, le gouvernement et les syndicats ouvriers ont réussi à trouver un accord. On a appelé ça les accords de Grenelle, parce que ça a été signé dans la rue de Grenelle, à Paris. Résultat :
A l’époque, les gens travaillaient énormément, parfois 46 heures ou même 50 heures par semaine, alors qu’aujourd’hui la durée légale du travail est de 35 heures. Après Mai 68, «on considérait que les ouvriers n’étaient pas qu’une force de travail, qu’il fallait faire attention à eux», raconte l’historien Philippe Artières.
«Globalement, après Mai 68, on ne pouvait plus voir l’école comme avant, remarque l’historien Eric Alary. Il y a eu une vraie réflexion sur la façon d’enseigner.» Ça n’a pas changé tout de suite, mais l’école est devenue moins stricte.
En 1969, le général de Gaulle, le président de la France, a proposé un référendum, c’est-à-dire un vote, pour savoir si les Français étaient d’accord avec ses décisions ou non. La majorité des gens a voté non. Le général de Gaulle a alors quitté le pouvoir et le Premier ministre, Georges Pompidou, a été élu président.
«Mai 68 est un échec parce que le gouvernement est devenu encore plus à droite après, alors que les manifestants voulaient qu’il soit plus à gauche», pense l’historien Pascal Ory. A l’inverse, l’historien Philippe Artières considère que Mai 68 est «une victoire parce que les étudiants et les ouvriers ont obtenu des choses. Le sens de Mai 68, ce n’était pas de se retrouver autour d’une table mais de renverser la table.»


Qu’est-ce qui a changé dans l'esprit des gens ?

Beaucoup de personnes se sont mises à dire non, à contester les règles. Un des slogans de Mai 68 résume bien ce que pensaient les manifestants : «Il est interdit d’interdire.» 1968 a été une année agitée pas seulement en France, mais un peu partout dans le monde. Des jeunes se sont mobilisés dans de nombreux pays : aux Etats-Unis, au Mexique, en Allemagne, en Italie ou encore au Japon. «Les jeunes voulaient un autre avenir que celui qu’on leur construisait, explique l’historien Eric Alary. Ils luttaient contre le capitalisme, la société de consommation, la puissance des Etats-Unis et son intervention dans la guerre du Vietnam.»
C’est à cette époque qu’est né, aux Etats-Unis, le mouvement hippie. Il luttait contre le racisme et défendait l’amour et la paix avec un slogan comme «Faites l’amour, pas la guerre». Les hippies défendaient aussi l’égalité entre les femmes et les hommes.
Dans les années 60, «l’homme avait autorité sur la femme. La majorité des femmes ne travaillait pas, pour s’occuper de la maison et des enfants», raconte l’historien Philippe Artières. En France, les femmes n’avaient eu le droit de voter qu’en 1944 ! Avec Mai 68, elles se sont fait entendre. Elles ont pris conscience qu’elles pouvaient prendre la parole. «Les étudiantes qui manifestaient ont montré aux autres femmes qui les voyaient à la télé qu’elles pouvaient se mobiliser et n’étaient pas obligées d’obéir sagement à leur mari», explique l’historien Eric Alary.
De nombreuses femmes n’acceptaient plus que les hommes décident à leur place. Elles réclamaient le droit de vouloir ou non des bébés, et de les garder ou non si elles tombaient enceintes. «Le combat pour l’avortement, qui a été autorisé plus tard, en 1975, vient de Mai 68», remarque Philippe Artières.
Avec Mai 68, on a aussi commencé à parler du problème de la pollution liée à la multiplication des usines, notamment. Enfin, les gens ont eu davantage envie de profiter des choses. Ils se sont dit qu’il n’y avait pas que le travail qui comptait dans la vie.


