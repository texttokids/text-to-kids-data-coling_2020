N°84 - 13 décembre 2018
l'attentat de Strasbourg
Mardi 11 décembre, il s’est passé quelque chose de très grave à Strasbourg, au nord-est de la France. Un homme armé a tué cinq personnes et en a blessé 11 autres. «Le P’tit Libé» te donne les clés pour comprendre ce qui est arrivé. Si tu as peur et si tu entends des choses que tu ne comprends pas ou qui t’inquiètent, il faut en parler à un adulte. 


Que s’est-il passé à Strasbourg ?

Mardi 11 décembre, un peu avant 20 heures, un homme armé d’un couteau et d’une arme à feu a attaqué des gens à Strasbourg, une ville située au nord-est de la France. Il a tué cinq personnes et en a blessé 11. Ces attaques ont eu lieu dans des rues commerçantes du centre historique de Strasbourg, dans le secteur du marché de Noël, qui attire chaque année deux millions de touristes venant du monde entier.
Les personnes qui ont été tuées sont des adultes : un Français, un Afghan et un Polonais qui habitaient dans la région, un touriste venant de Thaïlande et un Italien.
Des militaires sont intervenus pour essayer d’empêcher cet homme d’attaquer d’autres personnes. Ils lui ont tiré dessus et l’ont blessé. Mais il a réussi à s’échapper en prenant un taxi. Il a demandé au chauffeur de le déposer dans un autre quartier dans le sud de la ville, puis il a réussi à s'enfuir.
La police s'est tout de suite lancée à sa recherche. Elle a lancé un appel à témoins pour retrouver cet homme : elle a demandé à toute personne ayant des informations permettant de savoir où il était d’appeler un numéro de téléphone mis en place spécialement. Jeudi après-midi, une femme l'a vu et l'a dit aux policiers. Ils ont retrouvé le tueur à Strasbourg, dans le quartier du Neudorf. Au moment de l'arrêter, l'homme leur a tiré dessus. Les policiers ont tiré aussi et l'ont tué.
Strasbourg, la ville où a eu lieu l’attentat


Que sait-on du tueur ?

L’homme qui a attaqué des gens est un Français de 29 ans, qui est né à Strasbourg. Il s’appelle Chérif Chekatt. Il a déjà été condamné plus de 20 fois par la justice, notamment pour des vols et des actes violents. Il est allé plusieurs fois en prison.
Les personnes qui travaillent en prison ont remarqué que cet homme disait des choses très violentes, qu’il défendait les terroristes, et qu’il essayait d’encourager d’autres prisonniers à penser comme lui et à agir de façon violente. Pour cette raison, il a été «fiché "S"». Le S veut dire «sûreté de l’Etat». Ça signifie qu’il était surveillé parce qu’il pouvait être dangereux pour le pays.
Le matin même de son attaque, des gendarmes étaient allés chez lui pour l’arrêter pour une autre histoire, mais il n’était pas là. Ils avaient retrouvé une grenade et un pistolet.
On ne sait pas pour quelle raison Chérif Chekatt a tué et blessé des gens à Strasbourg. Mais la justice pense que son acte est terroriste. Les terroristes sont des personnes violentes qui font du mal aux gens pour semer la terreur et imposer leur façon de voir le monde. Le groupe terroriste Etat islamique a d'ailleurs dit que Chérif Chekatt était un de ses «soldats».


Quelles décisions ont été prises ?

Ce qui s’est passé à Strasbourg est très grave. Alors les mesures de sécurité ont été renforcées. Ainsi, la ville est très surveillée par des policiers et des militaires.
Partout en France, le plan Vigipirate est passé au niveau maximum, appelé «urgence attentat». Le plan Vigipirate est un ensemble de mesures prises pour lutter contre le terrorisme et pour protéger la population. Ça veut dire qu’il y a plus de policiers et de militaires dans les lieux publics (les rues, les gares ou les aéroports). Les écoles sont plus protégées que d’habitude, pour faire attention aux personnes qui y entrent.
En plus du plan Vigipirate, 1 800 militaires supplémentaires vont être mobilisés, en complément des 7 000 déployés en permanence pour mieux protéger certains bâtiments.
Le marché de Noël de Strasbourg, où l’attaque a eu lieu, a été fermé pendant deux jours. Toutes les illuminations de la ville ont été éteintes, y compris celles du grand sapin de la place Kléber, la plus importante de la ville. Les animations et les événements qui étaient prévus dans l’espace public ont été annulés et les marchés en extérieur fermés.
Samedi, les «gilets jaunes», ces personnes qui manifestent avec un gilet fluorescent sur le dos, ont prévu d’organiser une nouvelle journée de mobilisation. Le gouvernement leur demande d’annuler, parce qu’il pense que ça pourrait être dangereux de réunir plein de gens au même endroit vu ce qui s’est passé à Strasbourg.
A Strasbourg, des passants ont décidé de rendre hommage aux victimes. Ils ont écrit «Je suis Strasbourg», ils ont déposé des bougies et des pétales de roses, pour montrer qu’ils étaient tristes et qu’ils pensaient aux victimes et à leurs familles. Des professionnels qui aident les gens à aller mieux dans leur tête (comme des psychologues) sont là spécialement pour écouter les personnes qui sont choquées, et les aider à se sentir mieux.


