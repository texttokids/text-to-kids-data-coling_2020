N°3 - novembre 2015
Les attentats à Paris 
Vendredi soir, tu as dû entendre qu’il s’est passé quelque chose de très grave. «Le P’tit Libé» te propose de te donner les clés pour comprendre ce qui est arrivé. Mais n’hésite pas à poser des questions à tes parents si tu n’es pas sûr d’avoir bien compris.


Que s'est-il passé le 13 novembre ?

Des hommes très violents, des terroristes, se sont organisés pour attaquer Paris, la capitale de la France, et une ville juste à côté, Saint-Denis. Ils ont commis ce qu’on appelle des attentats. Pour imposer leur vision du monde, ils sont violents et font du mal aux gens, et ils espèrent faire peur à tout le monde.
Vendredi soir, le 13 novembre, ils ont fait exploser trois bombes près de Paris, à côté du Stade de France où se déroulait un match de foot de l’équipe de France. Une personne a été tuée par une explosion. Au même moment, d’autres terroristes sont allés dans un quartier de l’est de Paris, populaire et très fréquenté, et ils ont tiré avec un fusil d’habitude utilisé pour faire la guerre. Certains sont aussi entrés dans une salle de spectacle, le Bataclan, pendant un concert. Ils ont tiré sur des spectateurs et ont fait exploser une bombe qu’ils portaient sur eux quand la police est arrivée. Les policiers sont intervenus pour protéger les gens.
Au total, les terroristes ont tué 130 personnes. Les secours se sont occupés rapidement des blessés, car il y en a beaucoup. Ils sont à l’hôpital et on s’occupe d’eux. Sept terroristes sont morts vendredi soir.
Mercredi, le 18 novembre, les policiers ont encerclé très tôt le matin un appartement de la ville de Saint-Denis, qui est juste à côté de Paris. Ils voulaient surprendre les personnes qui dormaient là parce qu’ils pensent qu’elles ont participé aux attentats. Les policiers ont tiré dans l’appartement et deux de ces personnes sont mortes, dont un homme qui serait le chef des attaques. Sept personnes ont été arrêtées et sont interrogées par la police.


Pourquoi les terroristes ont-ils attaqué ?

Les terroristes peuvent avoir des buts différents. Certains veulent posséder un bout de pays déjà occupé par d’autres gens, d’autres souhaitent forcer tout le monde à suivre leur religion ou à avoir les mêmes idées qu’eux.
Les terroristes de vendredi soir font partie d’une organisation qui s’appelle l’Etat islamique. On dit parfois aussi «Daech». Ce n’est pas un pays. C’est un groupe de terroristes qui a été créé il y a 10 ans dans un pays qui s’appelle l’Irak. Aujourd’hui, ils réussissent à diriger plusieurs villes en Irak et en Syrie.
Les terroristes disent avoir choisi de commettre des attentats à Paris pour se venger de la France. Ils sont très fâchés parce qu’avec d’autres pays, la France envoie des bombes sur la Syrie. Là-bas, c’est la guerre depuis plus de quatre ans, la situation est très compliquée parce que le gouvernement est aussi violent envers sa population. La France, avec d’autres pays, essaie d’empêcher l’Etat islamique d’avoir des armes et de s’organiser.
Ces terroristes ont une religion, l’islam. On dit qu’ils sont musulmans. Beaucoup de gens ont la même religion, et ils sont très tranquilles, ils aiment la paix. Mais ceux qui ont fait les attentats appliquent l’islam d’une façon particulière et refusent que les autres gens ne fassent pas comme eux. Par exemple, ils veulent que les femmes soient entièrement voilées, ils ne veulent pas que les gens boivent de l’alcool ou écoutent de la musique. Pour imposer tout ça à l’ensemble de la société, ils utilisent la violence. Ces terroristes sont des jihadistes. Ils disent qu’ils se battent au nom de l’islam, mais leurs actes n’ont rien à voir avec cette religion.
Vendredi soir, on a aussi parlé de «kamikazes». Ce sont des terroristes qui se tuent en même temps qu’ils tuent d’autres gens. Ils font exploser les bombes qu’ils ont sur eux pour mourir, on dit qu’ils se suicident. C’est pour cela que l’on parle d’attentats-suicides.


Qu'est-ce que ça change dans le quotidien ?

La télévision parle beaucoup des attentats, et tous les adultes aussi. C’est normal : quand on est choqué, quand on est triste ou inquiet, il faut en parler. Ce sont des moments difficiles où on a envie d’être ensemble et de partager ce qu’on ressent.
En plus de ça, le quotidien est un peu chamboulé. Vendredi soir, François Hollande, le Président, a annoncé que la France était en «état d’urgence». Ça veut dire que les règles de vie habituelles sont un peu différentes : la police a plus de pouvoir, elle a le droit de forcer des gens à rester chez eux. Par prudence, la ville de Paris a décidé de fermer les lieux publics pendant deux jours, comme les écoles, les bibliothèques ou les piscines. Des magasins aussi sont restés fermés. Cela ne veut pas dire que c’est dangereux de sortir, simplement qu’il faut faire attention.
On fait aussi une minute de silence : ça signifie que dans tout le pays, on arrête ce qu’on est en train de faire et on se regroupe. Pendant une minute, on ne parle pas et on pense aux gens qui ont été touchés, on leur rend hommage. C’est une façon de se recueillir et de sentir qu’on est tous solidaires et donc qu’on est plus forts.
On peut allumer des bougies pour se souvenir de ceux qui sont morts, et déposer des fleurs pour eux à certains endroits. Dans le monde entier, plein de gens montrent leur soutien à la France en portant le drapeau bleu blanc rouge et en disant «Je suis Paris».
Les adultes peuvent aussi aller donner un peu de leur sang à l’hôpital pour aider les blessés. Chacun peut participer pour se soutenir, par exemple en faisant des dessins, ou tout simplement en se souriant dans la rue.


Et maintenant que va-t-il se passer ?

Les policiers n’ont pas réussi à empêcher ces terroristes d’agir, parce que ces derniers étaient très bien organisés. Mais aujourd’hui, il y a beaucoup plus de policiers et de soldats qui surveillent le pays pour nous protéger. Même si ce qui s’est passé est très triste et très difficile, les attentats restent très rares. Mais pour l’instant, on ne peut pas dire qu’il n’y en aura plus.
Quand il se passe des événements dramatiques comme ceux-là, la peur peut aussi créer de mauvaises réactions. Par exemple, certaines personnes vont penser que ceux qui sont musulmans sont forcément violents. Bien sûr, ce n’est pas du tout le cas.
Ce qui est le plus utile pour ne pas laisser gagner les terroristes, c’est de continuer à vivre normalement. C’est normal d’avoir peur, et si tu entends des choses que tu ne comprends pas ou qui t’inquiètent, il faut en parler à un adulte, comme tes parents ou ton professeur.


