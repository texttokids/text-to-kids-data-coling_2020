
Jeudi 8 février 2018
Grand Corps Malade va bien, merci pour lui. Après un passage par le cinéma avec son film Patients, qui a rencontré un joli succès, le chanteur est de retour. Il sort son 6e album. Dans cet album, il aborde tous les thèmes, de l'amour aux relations entre les humains. Mais aussi des sujets plus difficiles comme celui des migrants. Toujours avec poésie et douceur. Un vrai plaisir de le retrouver.
Prenez quatre riders [pratiquants de sports extrêmes] équipés de patins à glace, une piste de 340 mètres glacée avec un dénivelé de 20 m [différence d'altitude entre le départ et l'arrivée] et des pointes de vitesse à 80 km/h ! Vous assisterez alors à une course à mi-chemin entre le patinage de vitesse et le skicross. Le Crashed Ice consiste à gagner cette descente de vitesse parsemée de sauts, de bosses et de virages serrés. 

-D'où vient cette méche rebelle que Titeur n'arrive pas à coiffer ? 

Dans les années 1970, j'avais des cheveux longs et bouclés que je n'aimais pas coiffer. Ma mère voulait que je les aie bien lissés. Mais il y avait toujours une mèche qui se rebellait. Alors elle me la lissait avec sa salive. J'avais horreur de ça ! Et puis en tant que dessinateur, j'avais un problème car je n'aimais pas dessiner les cheveux. On passe toujours beaucoup de temps dessus. J'ai toujours essayé de trouver des trucs pour que mes personnages aient des coupes simples à dessiner.

Après avoir vaincu la reine et le roi des Neiges, Gerda n'est toujours pas satisfaite. La Princesse des Glaces rêve de revoir ses parents et son frère Kai, enlevés autrefois par le Vent du Nord. Elle part dans un voyage plein de rebondissements pour retrouver les siens. Sur son chemin, elle découvre une pierre ayant appartenu à des trolls : la pierre aux souhaits. La légende liée à cette pierre de feu et de glace va chambouler sa vie et rien ne va se passer comme elle l'avait prévu...

La légende raconte que des guerriers immortels protègent le monde des ténèbres depuis la nuit des temps. Un adolescent appelé Will découvre qu'il est le dernier de cette longue lignée. Le jeune garçon comprend qu'il a une mission : affronter un mystérieux cavalier noir au service des ténèbres. Pour cela, il va devoir voyager dans le temps et trouver des signes de lumière qui le guideront au bout de sa quête.

- Dans vos BD, vous parlez de tout. Rien n'est tabou ? Les enfants voient tous ces sujets et en entendent parler. Ils y sont confrontés. Donc il n'y a pas de raison de les ignorer.
Les autres personnages sont aussi importants dans vos BD ? En 25 ans, il commence à y avoir une belle galerie. Moi j'aime bien Manu. C'est le personnage auquel je me suis attaché avec le temps. Au début, il n'était que le faire-valoir de Titeuf, celui qui le suit. Mais je trouve qu'il a de belles valeurs. Il a du mérite, parce qu'être le pote de Titeuf ce n'est pas toujours facile. Manu est toujours là pour lui.

C'est l'histoire de tous les champions olympiques français de ski et de snowboard qui ont, un jour, obtenu cette médaille d'or olympique tant désirée. C'est l'histoire de cette journée, si particulière, qui a conduit chacun d'entre eux à monter sur la première marche du podium. Il s'agit de 23 histoires passionnantes à lire durant ces Jeux Olympiques d'hiver organisés en Corée du Sud, à PyeongChang.
En ce moment, je n'ai pas envie de faire du sport... En 52 cartes, ce jeu réunit enfants, parents, grands-parents et arrières grands-parents. Chacun raconte ses souvenirs et fait appel à sa mémoire. 

On sait que vers 7 ans, les filles et les garçons se séparent. C'est comme s'ils avaient changé de continent, c'est très compliqué. Et puis après à l'adolescence ils doivent se retrouver.C'est un gros travail. Ils ont peur de ce qu'ils vont se dire, de ce qu'ils pensent... Il y a beaucoup de questions. Ce n'est pas lié à une époque ou à une culture. C'est quelque chose qui est lié à nos gènes.

La BD Titeuf est vendue dans 25 pays, y compris en Chine. Plus de 21 millions d'exemplaires se sont écoulés à travers le monde. Le plaisir de dessiner et raconter des histoires est-il toujours là ? Oui, sans pression. J'ai mes carnets. Quand j'ai suffisamment de matière, là j'ai envie d'y aller. Quand j'arrête de travailler sur une BD de Titeuf, il y a un moment où ça me manque. Donc j'ai envie de le retrouver. Ça arrive souvent après 1 an et demi ou 2 ans. Alors je redémarre une nouvelle aventure.

Arty deviendra-t-il l'un des Tinters?.

Premiers jours de pâtisserie pour Arty. La loi de Phinéa exige d'apprendre un vrai métier pour devenir un bon citoyen. Seulement, à 16 ans, le garçon rêve à quelque chose de plus grand. Face à son maître autoritaire, d'étranges et puissants pouvoirs se réveillent en lui. Serait-il l'héritier de la Tinta, cette énergie magique qui dirige toute loi sur Phinéa ? Pour percer le secret, Arty va développer son pouvoir pour offrir au monde une nouvelle ère de liberté... ?

Pour fêter les 20 ans de la saga «Mari Party », Jumpman (Mario est appelé ainsi par les joueurs) et ses amis s'offrent une compilation réunissant le 100 meilleurs mini-jeux. Tous les styles sont représentés, qu'il s'agisse de spoi de réflexe, d'action, de hasard, de course... Il est possible d'incarner huit personnages et de jouer en multijoueurs. Les fans de Mario auront plaisir à le découvrir ou le redécouvrir. 
