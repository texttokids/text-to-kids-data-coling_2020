Jeudi 16 novembre 2017
Après avoir fait arrêter des princes et des ministres, le prince héritier d'Arabie Saoudite a affirmé très clairement que l'Iran était l'ennemi de son pays.
La tension grandit dans la région.
Chaque année, de nouveaux mots font leur apparition dans le dictionnaire.
À l'inverse, d'autres disparaissent et tombent dans l'oubli.
C'est normal.
Ce mot fait partie de ceux qui ont disparu du dictionnaire "Le petit Larousse" depuis sa création en 1856.
Les dictionnaires, comme "Le petit Larousse" ou "Le Robert", comptent environ 60 000 mots. Pour prendre en compte des mots nouveaux, ces dictionnaires sont réédités chaque année [corrigés et réimprimés]. Pour laisser la place aux nouveaux mots, d'autres sont obligés de s'en aller.
Les mots les moins utilisés disparaissent.
Depuis la première édition du « Petit Larousse » parue en 1856, près de 4000 mots ont disparu.
Pendant ce temps, 24 000 ont fait leur entrée.

Chaque année, des mots nouveaux font leur entrée dans le dictionnaire.
Ils sont par exemple 150 dans l'édition 2018 du "Petit Larousse" parue en juin dernier.
 
L'Arabie Saoudite et l'Iran sont les deux pays les plus puissants du Moyen-Orient. Ils sont aussi ennemis. Le 9 novembre, le président français Emmanuel Macron est allé voir le prince Ben Salmane pour discuter de la situation actuelle.


Le week-end des 4 et 5 novembre, des princes, ministres et hommes d'affaires sont arrêtés en Arabie Saoudite sur ordre du prince Mohamed Ben Salmane.
Tous sont emmenés dans un hôtel de luxe de la capitale Riyad.
Les clients de l'hôtel ont été priés de partir.
Seules demeurent les personnes en état d'arrestation, ceux qui les gardent et les interrogent.
Mais les princes, ministres et hommes d'affaires ne profitent pas pour autant du luxe de l'hôtel.
Selon les informations diffusées par un journal américain, ils ne dorment pas dans les chambres, mais dans une salle de réception transformée en dortoir.
Lundi, à l'heure où nous terminions ce journal, nous ne savions pas quel sera leur sort.
La plupart des mots sortis du dico sont tombés dans l'oubli. 
Comme le verbe "déprier" qui signifie retirer une invitation ou "velche" qui désigne un homme sans goût. Certains désignent des métiers ou des outils d'autrefois comme "galonnier" (fabricant de galons) ou "peigner" (qui vend des peignes) ou encore des fruits disparus (les "mouille-bouches" étaient une ancienne variété de poires). Aujourd'hui, ces vieux mots prennent leur revanche dans un livre, "Les mots disparus de Pierre Larousse", publié à l'occasion des 200 ans de la mort de l'inventeur du dictionnaire.

La semaine dernière, un jugement a beaucoup choqué. Un homme, qui avait eu des relations sexuelles avec une jeune fille de 11 ans a été acquitté. C'est-à-dire qu'il a été reconnu non coupable. Pour les juges, il ne l'avait pas forcée.
Une loi va donc être proposée pour définir ce qu'on appelle le consentement sexuel des mineurs. Autrement dit, l'âge en dessous duquel un jeune ne peut pas accepter des relations sexuelles. Quel sera cet âge ? 13 ans ? 15 ans ? En tout cas, en dessous de cet âge, le jeune sera automatiquement considéré comme ayant été agressé ou violé.

Mais la jeune fille était-elle capable de comprendre ce qui se passait? A 11 ans, n'est-on pas de toute façon trop jeune pour accepter des relations sexuelles ?

La loi devra dire clairement en dessous de quel âge un jeune ne peut pas avoir accepté librement des relations sexuelles.

