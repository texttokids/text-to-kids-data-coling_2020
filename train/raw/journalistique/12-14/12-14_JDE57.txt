Jeudi 16 novembre 2017
Le dossier de la semaine Le 17 novembre, c'est la Journée mondiale de la Prématurité. Un enfant sur 10 naît prématuré dans le monde, c'est-à-dire qu'il quitte trop tôt le ventre de sa maman. Le JDE est allé à la rencontre de ceux qui soignent ces tout petits bébés.
Naître trop tôt, un défi à la vie
une grossesse [temps durant lequel le bébé se développe dans le ventre le la mère] dure environ 40 semaines. Un bébé est prématuré s'il nait avant 37 semaines de grossesse.



«Les bébés sont dans des incubateurs... comme des Pokémon ! » m'explique l'infirmière en me faisant entrer dans la salle de l'unité de réanimation néonatale de l'hôpital de Mulhouse (Alsace). C'est là que sont soignés les bébés nés trop tôt ou qui présentent des problèmes à la naissance. Dans ces machines, aussi appelées couveuses, ils vont finir de grandir hors du ventre de leur maman. L'incubateur tient le bébé au chaud et humidifie l'air, qui sinon, est trop sec pour sa peau fragile.

Quels sont les risques ?

Système immunitaire [pour se défendre contre les maladies] qui ne fonctionne pas bien.

La maman va être très surveillée. Parfois, elle va rester à l'hôpital. On peut par exemple lui donner un traitement pour que les poumons du bébé se développent plus vite.
Quand le bébé est là, les parents font partie du traitement. Ils sont invités à prendre le bébé contre eux, pour ce qu'on appelle le peau à peau. Les spécialistes ont observé
que cette pratique permet de limiter les changements de rythme cardiaque trop importants du bébé, que cela lui donne un meilleur sommeil...

Et pour les parents, c'est un moyen de faire connaissance avec leur bébé d'apprendre à le toucher, le manipuler. Quant aux grands frères et grandes soeurs, ils ont le droit de venir voir le bébé, à condition d'être sages et de pas faire trop de bruit. Pour jouer avec lui, il faudra encore patienter un peu .

Des incubateurs, il y en a six, disposés dans la grande salle. Chacun est relié à des machines. Je m'approche. Emmitouflée dans un léger drap blanc, calée par des tissus roulés qui forment une espèce de cocon pour rappeler sa position dans le ventre de sa maman, il y a une petite fille. 



Mini-couche, mini-tétine, tout est mini dans la vie de ces tout petits bébés si fragiles.

A partir de quand décide-t-on d'aider un bébé prématuré à survivre ? Jusqu'à il y a une quinzaine d'années, la limite était fixée à 28 semaines de grossesse. La loi a décidé que désormais, la limite était de 22 semaines, pour un bébé d'un poids minimum de 500 grammes. Néanmoins, c'est à partir de 24-25 semaines que la plupart des bébés prématurés sont aidés à vivre.

J'ai commencé à exercer [travailler comme médecin] en 1968. Des progrès énormes ont été faits dans la prise charge des bébés pendant et après l'accouchement. Aujourd'hui, des bébés peuvent naître un peu plus de la moitié de la grosses et survivre. Le nombre de bébés prématurés qui décèdent a énormément baissé, mais il reste des limites.

Les progrès de la science ont permis de mettre au point des matériels plus adaptés pour ces tout petits êtres. Par exemple, avant, la machine appelée ventilateur soufflait de l'oxygène et forçait le bébé à respirer. Aujourd'hui, elle détecte le début de respiration et le complète, pour que le bébé puisse respirer à son rythme. Les bébés reçoivent aussi des produits pour avoir moins mal. Quand ils reçoivent des soins, l'infirmière leur donne un petit tuyau à attraper, pour les rassurer. Cela s'appelle le grapping.

Dans la salle où sont placés les incubateurs [machines où sont soignés ies bébés prématurés], il y a un panneau en forme d'oreille. Quand il y  a trop de bruit, elle devient rouge. Il ne faut pas faire trop de bruit car les bébés y sont très sensibles. Cette oreille qui surveille est un symbole des progrès qui ont été réalisés dans les soins des prématurés. Désormais, les incubateurs sont recouverts d'un drap, pour que les yeux  fragiles des bébés soient protégés.

A l'hôpital de Mulhouse, une passerelle a été construite pour pouvoir amener le plus vite possible le bébé très prématuré dans l'unité de réanimation néonatale après l'accouchement. Chaque minute compte.
Auparavant, le bébé a été pesé, car tout se décide selon son poids. Une fois dans l'incubateur, une prise de sang est faite et analysée. Le médecin met en place ce qu'on appelle une voie, c'est-à-dire un tuyau qui passe par la veine du cordon ombilical et qui nourrit le bébé comme dans le ventre de sa maman.
Si le bébé ne respire pas seul, on place un masque adapté sur son visage ou alors on l'intube [on insére un tuyau qui va jusqu'à l'entrée des poumons]. Des pastilles bleues sont placées sur son corps pour contrôler les battements de son coeur, sa respiration...
Une radio des poumons est faite pour déterminer quel traitement il faut faire. On regarde également une image du cerveau pour voir s'il fonctionne normalement. Cela permet aussi de déterminer précisément l'âge du bébé. Tant que le bébé restera dans le service de réanimation néonatale, il sera branché à ces machines et recevra des soins toutes les 3 à 4 heures.
Le nombre de naissances prématurées augmente ces dernières années.

Cela à cause de plusieurs raisons : 
- l'âge des mamans augmente. Plus on a un bébé tard, plus il y a de risques qu'il soit prématuré;
- quand la grossesse arrive avec l'aide de la médecine, il y a souvent plusieurs bébés qui se développent dans le ventre, donc plus de risques de prématurité;
- les mamans qui ont un travail penible;
- la pauvreté et le manque de soins;
- les maladies dont souffre la maman;
Mais dans 4 cas sur 10, on ne sait : pas pourquoi le bébé est né prématuré.

Est-ce que tous les médecins doivent suivre les mêmes règles? Chaque médecin doit se demander s'il est raisonnable de tenter d'aider un bébé prématuré à vivre. Il décide en fonction de plusieurs critères [conditions] notamment pour les bébés nés entre la 22e et la 25e semaine.
Par exemple, son service est-il assez bien équipé en matériel pour réanimer et bien accueillir le bébé prématuré ? Y a-t- il assez de personnel ? Le bébé donne-t- il des signes de vitalité ? Et puis cela dépend aussi de la situation de la maman. Si elle a essayé toute sa vie d'avoir un bébé et que c'est sa dernière chance, tout sera tenté pour aider le bébé à survivre.
Sous le pied de ce bébé prématuré, un capteur surveille sa santé.

- Pensez-vous que l'on arriver sauver des bébés nés encore plus tôt?
Il est possible de réanimer des bébés nés trop tôt, mais il ne faut pas que cela au détriment de leur qualité de c'est-à-dire que cela ne provoque des séquelles [problèmes] ou des handicaps graves dont ils souffriront en grandissant.


Propos recue par Caroline Gaer
