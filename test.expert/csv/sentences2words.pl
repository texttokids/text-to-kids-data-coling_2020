#!/usr/bin/perl

while (<>) {
    chomp;
    my $line = $_;
    my ($text_id,$min,$max,$sentence_id,$sentence) = split(/\t/, $line);
    my @words = split(/ /, $sentence);
    for (my $i = 0; $i < @words; $i++) {
        print join("\t", ($text_id,$min,$max,$sentence_id,$i,$words[$i]))."\n";
    }
}
