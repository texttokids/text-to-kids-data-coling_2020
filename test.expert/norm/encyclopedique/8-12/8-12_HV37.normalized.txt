le débarquement de Normandie
radio londres
en mille neuf cent quarante , le maréchal Pétain , devenu chef de l' État français , reconnait la défaite et choisit de collaborer avec les allemands .
mais le dix huit juin , le général de Gaulle lance un appel à la radio depuis Londres .
l' appel du dix huit juin mille neuf cent quarante: une version simplifiée du discours prononcé à la radio par le général de Gaulle a été publiée sous forme d' affiche .
à tous les franÇAIS
la France a perdu une bataille !
mais la France n' a pas perdu la guerre !
des gouvernants de rencontre ont pu capituler , cédant a la panique , oubliant l' honneur , livrant le pays à la servitude .
cependant , rien n' est perdu !
rien n' est perdu , parce que cette guerre est une guerre mondiale .
dans l' univers libre , des forces immenses n' ont pas encore donné .
un jour , ces forces écraseront l' ennemi .
il faut que la France , ce jour là , soit présente à la victoire .
alors , elle retrouvera sa liberté et sa grandeur .
tel est mon but , mon seul but !
voilà pourquoi je convie tous les français , ou qu' ils se trouvent , à s' unir à moi dans l' action , dans le sacrifice et dans l' espérance .
notre patrie est en péril de mort .
luttons tous pour la sauver !
le débarquement a été très meurtrier .
près de quarante mille Alliés , plus de cent allemands , environ vingt mille civils ont été tués .
avec des plages dévastées et quatre cents villes détruites , la Normandie gardera longtemps les traces de ces affrontements .
le général de Gaulle demande aux français de refuser la défaite et de continuer le combat en entrant dans la Résistance .
pour lui , la France a perdu une bataille , mais pas la guerre .
le pays peut compter sur les Alliés anglais et américains .
quatre ans plus tard , le six juin mille neuf cent quarante quatre, il annonce la nouvelle du débarquement par un message radio : " après tant de combats , de fureur , de douleurs , voici venu le choc décisif , le choc tant espéré .
c' est la bataille de France et c' est la bataille de la France .
le général de Gaulle débarque à son tour en Normandie le quatorze juin .
la population libérée l' accueille comme un héros .
canadiens normands
" j' irai revoir ma Normandie , c' est le pays qui m' a donné le jour ... "
les Normands ont été surpris d' entendre cette vieille chanson française
dans la bouche de soldats qui venaient de débarquer .
il s' agissait de Québécois , des Canadiens dont les ancêtres venaient de France .
près de vingt et un mille Canadiens ont participé au débarquement .
anniversaire bataille de normandie