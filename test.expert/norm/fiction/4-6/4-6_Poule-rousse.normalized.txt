près du bois , il y a un jardin .
dans de jardin , il y a une maison : c' est la maison de Poulerousse .
dans la cuisine et dans la chambre , tout est propre et bien rangé .
Poulerousse est une bonne ménagère : pas un grain de poussière sur les meubles , de fleurs dans les vases , et aux fenêtres de jolis rideaux bien repassés .
c' est un plaisir d' aller chez elle .
son amie la tourterelle vient la voir tous les jours .
toc , toc , toc ...
elle frappe doucement à la porte .
les deux amies s' embrassent .
ce sont des " cot , cot , cot , cot " et des " oucourou , oucourou " à n' en plus finir .
elles ont beaucoup de choses à se dire .
elles s' assoient l' une en face de l' autre .
elles boivent un tout petit verre de vin sucré , croquent des gâteaux secs .
elles chantent et jouent aux dominos , ou bien ...
elles travaillent en bavardant .
la tourterelle tricote .
Poulerousse aime mieux coudre ou raccommoder .
du reste , elle a toujours dans sa poche une aiguille tout enfilée , un dé et de ciseaux .
et elle est toujours prête à rendre services aux uns ou aux autres , en raccommodant un accroc ici ou là .
aussi tout le monde dit du bien d' elle .
et le renard , qui dresse ses oreilles pointues à tous les vents entend un jour :
quelle bonne petite poule , cette Poulerousse !
et comme elle est belle et grassouillette !
toute grassouillette !

grassouillette ...
se dit le renard .
on !
aïe !
aïe !
toute grassouillette !
l' eau lui vient à la bouche et il court tout droit chez lui .
il entre en dansant et en chantant :
grassouillette !
grassouillette !
elle est toute grassouillette !
mais que t' arrive t il donc ?
demande la renarde .
tu es fou !
Tra la la !
il y a une poule rousse près du bois .
une poule comme il faut , et grasse à point .
je vais l' attraper .
et tout de suite .
vite , donne moi un sac .
prépare la marmite .
fais bouillir de l' eau .
nous allons la faire cuire et la manger , cette poule rousse !
quel renard tu es !
quel amour de renard !
s' écrie la renarde toute joyeuse .
et elle lui tend le sac .
le renard file comme le vent .
il voit la maison de Poulerousse , s' approche doucement , se cache derrière un arbre .
au même moment , la porte s' ouvre .
cot , cot , cot , au revoir chère tourterelle , à demain .
à demain , ma Poulerousse .
au revoir !
la tourterelle s' envole .
Poulerousse va chercher du bois au bucher .
alors , houp !
le renard saute dans la cuisine sans faire de bruit et se cache derrière la porte .
Poulerousse prend du bois et rentre tranquillement dans sa maison .
mais ha !
le renard l' attrape et la fourre dans son sac , si vite que Poulerousse n' a pas le temps d' ouvrir le bec .
je te tiens , je te tiens , ma belle !
le renard noue le sac , le jette sur son épaule et s' en va en sifflant .
Poulerousse est tout étourdie .
elle étouffe , elle se débat dans le sac et lance un " cot , cot " plein d' effroi .
mais ...
qui l' entendra ?
qui l' entendra ?
la tourterelle ...
elle est là tout près , sur une branche de pommier .
elle comprend que le renard emporte Poulerousse pour la manger .
son coeur bat très fort , ses ailes tremblent , elle a du mal à les ouvrir .
enfin , elle s' envole , pousse un petit cri et se pose à quelques pas du renard .
elle volette et sautille en trainant l' aile , comme si elle était blessée .
une tourterelle blessée !
quelle chance !
attends , ma petite .
il y a encore une place pour toi dans la marmite !
le renard pose le sac par terre et court après la tourterelle .
il croit l' attraper ...
hop !
elle saute et se pose quelques pas plus loin .
hop !
hop !

et , tout en sautillant , elle chante : " Oucourou , oucourou " .
cela veut dire : " courage , Poulerousse , sauve toi ! "
vite , vite , Poulerousse prend ses ciseaux dans sa poche .
crac , crac , elle coupe la toile , et pfutt !
la voilà libre .
puis elle pousse une grosse pierre dans le sac et recoud en un clin d' oeil , remet dans sa poche son aiguille tout enfilée , son dé , ses ciseaux , et court , court , court vers sa maison .
le renard court aussi .
il est très loin , tout essoufflé :
nom d' un rat !
il faut que je l' attrape cette sale bête !
là , cette fois ça y est !

Ouap !

rien !
la tourterelle s' envole juste assez haut pour voir Poulerousse entrer dans sa maison .
alors , rassurée , elle s' envole pour de bon , haut , très haut .
le renard reste bouche bée , et , furieux revient vers le sac , qu' il remet sur son épaule en grognant :
au moins , celle qui est là dedans ne se sauvera pas !
puis il rentre chez lui , bien fatigué .
le couvert est mis et l' eau bout dans la marmite .
l' as tu attrapée ?
demande la renarde en se jettant à son cour .
si je l' ai attrapée ?
tiens !
vois comme elle est lourde !
la renarde soupèse le sac .
hum !
quel déjeuner nous allons faire !
ils s' approchent tous les deux de la marmite , ouvrent le sac et le secouent au dessus de l' eau qui bout .
la pierre tombe .
l' eau bouillante jaillit sur eux et les brule si fort qu' ils se sauvent en hurlant dans les bois .
jamais ils ne sont revenus .
et depuis ce jour , Poulerousse te la tourterelle ne se quittent plus .
elles vivent ensemble dans la petite maison de Poulerousse .
elles sont très heureuses .