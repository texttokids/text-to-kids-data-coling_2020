Découvre sa vie !
Dans ma tribu, je suis gardienne du troupeau de chèvres qui appartient à ma maman. Car chez les Himbas, les troupeaux appartiennent aux femmes.
Chaque matin, je vais chercher mon troupeau dans son enclos.
Je trais une ou deux chèvres pour récolter le lait...
Voici mon village. Nous habitons une région où il fait chaud et où il y a peu d'eau : le désert du Kaokoland.
 Nos cases sont fabriquées avec de la bouse de vache et de l'argile. Elles gardent la chaleur pendant la saison des pluies et restent fraîches en saison sèche !
Avec les peaux de chèvres, on fabrique de belles jupes !

