La joie est une émotion, c'est-à-dire une réaction de ton corps à ce que tu es en train de vivre. Il existe d'autres émotions comme la surprise, la colère ou la peur.
Quand tu es joyeux, tu te sens léger. Tu as envie de chanter, de danser, de crier, et parfois même de sauter partout. C'est la fête !
Tu es joyeux à Noël mais d'autres événements peuvent t'apporter de la joie : une fête d'anniversaire ou un départ en vacances, par exemple !
La joie est souvent communicative : si tu es joyeux et que tu n'arrêtes pas de rigoler, tes amis aussi vont ressentir de la joie et vos liens vont se renforcer.
Même ton chien peut ressentir de la joie ! Regarde-le quand tu rentres à la maison : il te saute dessus, remue la queue et réclame des câlins. Youpi!

