Cette année, la louve dominante a donné naissance à 4 adorables louveteaux, et la famille s'est agrandie.
Pendant que les adultes chassent et surveillent leur territoire, les petits jouent pour savoir qui sera le plus fort !
Les louveteaux quittent leur tanière vers 3 semaines. Toute la meute s'occupe d'eux, les protège et les câline.
Patience, petit loup ! Dans un an, tu seras toi aussi un chasseur et un protecteur de la meute !
A Tomtoto Luarat Dhatod. NaturoDiuotration Apne Evrlouv
À l'aube, ce loup est parti se régaler et rapporte aussi de la nourriture aux petits.
