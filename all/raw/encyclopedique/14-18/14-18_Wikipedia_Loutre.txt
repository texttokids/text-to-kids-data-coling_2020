
Grâce à de puissantes pattes palmées (avant et arrière), la loutre est
une excellente nageuse, mais elle se déplace aussi volontiers à terre,


La loutre est un animal souvent solitaire. Sa fourrure se compose de
poils qui s'emboîtent les uns dans les autres. Les
petits loutrons restent avec leur mère jusqu'à l'âge de six


Contrairement à l'ours polaire ou au dauphin, la loutre ne
dispose pas d'une épaisse couche de graisse sous la peau. C'est son
pelage, composé de poils courts et longs emboîtés qui l'isole du froid.
Une étude thermographique a montré que par température excessive, la
loutre européenne dissipe sa chaleur plutôt par les pattes, alors que
la loutre amazonienne géante le fait par tout le corps et notamment la


Elle peut vivre jusqu'à 20 ans en captivité. Mais en milieu naturel son



La loutre peut rester en apnée jusqu’à huit minutes sous l’eau.

La plupart des espèces ne vivent qu'en eau douce, cependant, la
loutre de mer vit, comme son nom l'indique, dans l'eau salée bien
qu'elle ait besoin d'eau douce pour le toilettage et l'entretien de sa


La Hongrie abrite le plus grand nombre d'individus d'Europe,


La loutre est un mammifère majoritairement piscivore. Son régime
alimentaire est constitué à 50 % à 90 % de poisson, le reste se
composant de batraciens, petits mammifères, crustacés et parfois même
d’oiseaux. La loutre pêche principalement en solitaire même si de temps
700 grammes de nourriture par jour, et les adultes

En région tempérée, la loutre n'hiberne pas. Dépourvue de réserves
importantes de graisse, elle doit aussi chasser en hiver.


C’est un animal très joueur qui s’amuse souvent avec ses proies. Il les
entraîne dans de petites baies peu profondes pour en venir à bout. En
plongée, les oreilles et les narines obstruées, elle perd l'odorat et
l'ouïe, ce qui handicape sa chasse. Cependant, elle est dotée de
vibrisses (moustaches rigides) fort sensibles aux vibrations. Elles lui
permettent de détecter une proie aux ondulations que provoque sa fuite


La loutre n’a pas de responsabilité dans la raréfaction du poisson car
elle ne s’attaque généralement qu'aux proies malades ou les plus
abondantes^. La loutre de mer se sert de galets ou pierres comme
outil pour briser les coquillages trop résistants en en les frappant
sur son abdomen. Cela en fait l'un des rares animaux à se servir





La population des loutres connaît en très forte régression sur la
presque totalité de son aire de répartition et, pour cette raison, elle
bénéficie du statut d'espèce protégée dans la plupart des pays.

La loutre a régressé puis disparu d'une très grande partie de son aire
de répartition à cause de la chasse et du piégeage, sa fourrure étant,
comme celle du castor, particulièrement recherchée. Chassée en
vénerie à pied avec des chiens, elle se réfugie sur les berges des
rivières où les chasseurs la capturent avec une fourche ou grâce à
leurs chiens. Parfois, elle s'attrappe avec des filets tendus autour de
son terrier (aussi appelé vulgairement catiche) ou avec divers
pièges en métal placés autour de son terrier et appâtés avec des


Bien que l'animal soit protégé, ses populations continuent à

Aux Pays-Bas, une surveillance par collier radio-émetteur a montré
que la première cause de mortalité des loutres dans ce pays était la
route ; les loutres sont souvent tuées ou blessées par des

Elles sont également victimes de la pollution de l'eau et/ou des
toxiques bioaccumulés dans leurs proies, ainsi que de la
réduction des zones humides. Cela a été démontré au Danemark par
l'analyse de la présence de cadmium dans leurs poils^.
L'évaluation du degré de contamination de leur nourriture peut aussi
cela a été fait par exemple en Slovaquie pour le cadmium et le
mercure, deux produits très toxiques, pour les reins


En France, on estimait 50 000 individus au début du XX^e siècle et
des espèces protégées, la population de loutres est remontée à
2 000 ou 3 000 individus en 2010, ce qui lui a permis de recoloniser



La sous-famille des loutres (Lutrinae) comprend les genres et


Genres selon Mammal Species of the World (version 3, 2005) (21 mai






Genres et espèces, selon Mammal Species of the World (version 3,

loutre à joues blanches du Congo (sous-espèce congica,
anciennement Aonyx congicus (Lönnberg, 1910)), etc.












Dans le calendrier républicain français, le 25^e jour du mois de
Thermidor est dénommé jour de la Loutre^. Ce qui


