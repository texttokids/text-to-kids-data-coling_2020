Lannion est une commune française située dans le département des Côtes-d'Armor en région Bretagne. Ses habitants sont appelés les Lannionnais.

La commune actuelle est formée de la fusion des communes de Lannion, Brélévenez, Buhulien, Loguivy-lès-Lannion et Servel en 1961.

Lannion est une ville pont sur le Léguer. La proximité de l'embouchure du Léguer fait que le niveau de la rivière au centre ville varie de plusieurs mètres en fonction des marées. Cette caractéristique a permis l'établissement d'un gué dans les temps les plus reculés au niveau du pont de Kermaria, point où s'arrête l'influence des marées (preuve existante à proximité de la rue Saint-Christophe-le-Passeur). Cette particularité sera mise à profit pour la construction en 1992 d'un stade d'eau vive marémoteur. Un barrage permet de retenir l'eau amenée par la marée montante et de la relâcher dans un parcours artificiel à marée descendante.

Lannion est localisée à l'extrémité occidentale du domaine nord armoricain, dans le Massif armoricain qui est le résultat de trois chaînes de montagne successives. Le site géologique de Lannion se situe plus précisément dans un bassin sédimentaire essentiellement briovérien limité au nord par un important massif granitique cadomien, le batholite nord-trégorrois. Ce pluton fait partie d'un ensemble plus vaste, le batholite mancellien.

L'histoire géologique de la région est marquée par la chaîne cadomienne. À la fin du Précambrien supérieur, les sédiments briovériens environnants (formations volcano-sédimentaires) sont fortement déformés, plissés et métamorphisés par le cycle cadomien, formant des schistes argileux, les schistes tuffacés de Locquirec, les brèches et tufs carbonifères du Dourduff. Cette chaîne montagneuse, qui devait culminer à environ 4 000 m, donne naissance à des massifs granitiques (dont le batholite côtier nord-trégorrois associé à un volcanisme d'arc insulaire et daté à 615 Ma).
Dans le domaine continental, l'épaississement consécutif à l'orogenèse cadomienne, provoque la fusion crustale à l'origine de la mise en place des dômes anatectiques (migmatites de Guingamp et Saint-Malo au sud-est de Lannion) qui est datée entre 560 et 540 Ma.

L'orogenèse hercynienne qui a donné une chaîne montagneuse atteignant 6 000 m, s'accompagne d'un métamorphisme et d'un magmatisme qui se manifeste par un important plutonisme : le chapelet nord de granites rouges tardifs (ceinture batholitique de granites individualisée pour la première fois par le géologue Charles Barrois en 1909), formant de Flamanville à Ouessant un alignement de direction cadomienne, contrôlé par les grands accidents directionnels WSW-ENE, datés de 300 Ma, correspond à un magmatisme permien. Le massif granitique de Plouaret au sud de Lannion, lié au fonctionnement du cisaillement nord-armoricain, fait partie de ce chapelet.

La commune se situe ainsi dans un pays de basses collines aux sommets aplanis, appartenant à un couloir topographique et tectonique qui va de la baie de Lannion à la baie de Saint-Brieuc. Creusée par l'érosion dans des formations schisteuses volcano-sédimentaires et métamorphiques, cette dépression est limitée au Nord, à l'Ouest et au Sud par trois massifs granitiques et des bordures escarpées
commandées par des failles.

Une promenade géologique dans la commune permet de découvrir l'utilisation de pierres proximales (privilégiées pour des raisons de coût), témoignant de la diversité de la palette lithologique que l'évolution géologique a conféré au territoire lanionnais. Depuis l'époque romane, ces constructions sont un marqueur de la richesse du substrat géologique local et déterminent pour partie le rang social des propriétaires.
Ces pierres proximales sont les schistes argileux qui fournissent de médiocres moellons à la teinte brunâtre ; des schistes tuffacés qui livrent des moellons et des ardoises très épaisses, sortes de lauzes, mais aussi de grandes dalles, recherchées naguère pour les recouvrements des sols et les plaques tumulaires ; et des granitoïdes dont le minéralogiste Beudant disait qu'on les utilise dans les œuvres dont « on veut éterniser la durée » (granite du Yaudet ; diorites et granodiorites issus du massif de Plouaret ; complexe granitique de Ploumanac'h sur la côte de granit rose dont le district de l'Île-Grande qui offre des granites de faciès variés, facilement transportables grâce au Léguer).
* Exemple de polylithisme, le porche sud de l'église de la Trinité de Brélévenez.
* Polylithisme accusé dans la façade occidentale de l'église Saint-Jean-du-Baly.
* Couvent des Ursulines en moellons de schistes tuffacés et granite blanc de l'Île-Grande.
* Calvaire dû à Yves Hernot (1867), en diorite de Lanvellec et kersanton.
* Manoir de Crec'h Ugien : murs et arcs de décharge en schistes tuffacés, encadrement et chaînage d'angle en granites de l'Île-Grande.

La ville de Lannion est desservie par une quatre voies la reliant à Guingamp sur la RN 12 (Paris-Brest). Elle est en outre dotée :
* d'un aéroport, Lannion-Côte de Granit, qui affichait un trafic annuel moyen de 30 000 passagers par an avant la fermeture de la liaison avec Paris en 2018 ;
* d'une gare TGV depuis juillet 2000, à la suite de l'électrification de la voie entre Plouaret et Lannion. En basse saison, le TGV assure une liaison le vendredi soir depuis Paris avec un retour vers la capitale le dimanche soir. En haute saison, une rotation par jour est assurée en plus de la desserte des weekends. La gare est desservie quotidiennement par des TER Bretagne.

Les transports sur la ville de Lannion et sa communauté d'agglomération sont quant à eux assurés par les TILT (Transports intercommunaux Lannion-Trégor), qui comportent 6 lignes :
* ligne A : Hôpital/Aéroport via Quai d'Aiguillon (principal pôle de correspondance du réseau) ;
* ligne B : Kerbabu/Coppens via Quai d'Aiguillon ;
* ligne C : Alcatel/Kérilis ;
* ligne Navéo : navette centre ville ;
* ligne F : lignes du marché (jeudi matin).

Lannion est située à :
* 35 km de Guingamp ;
* 38 km de Morlaix ;
* 66 km de landerneau
* 69 km de Saint-Brieuc ;
* 84 km de Brest ;
* 120 km de Quimper ;
* 166 km de Rennes ;
* 212 km de Fougères ;
* 515 km de Paris.

L'origine du nom Lannion vient de « lann », qui désigne un établissement religieux créé par les Bretons du haut Moyen Âge et, selon une hypothèse, un anthroponyme Yuzon. Ce nom de Lannyuzon a évolué en Lannuon, forme moderne en Langue bretonne et en « Lannion », variante évoluée puis administrativement cristallisée de la forme ancienne bretonne, devenue l'appellation officielle et administrative du duché de Bretagne, avant et après 1532, probablement aux alentours des XIIIe-XIVe siècles : Carte de l'Atelier cartographique Troadec du Conquet-Konk-Leon pour l’Histoire de la Bretagne de Bertrand d'Argentré de 1582 et 1588 : Lanion ; Carte de Nolin,1695 : Lanien ou Lanion ; puis de l'Administration française royale et républicaine et de la société francophone nobiliaire et marchande, du XVIIe siècle jusqu'à nos jours ; en résumé la forme « Lannyon » et ses variantes sont attestées de 1199 à 1516, où la forme « Lannion » apparaît (Erwann Vallerie, Traité de toponymie historique de la Bretagne, 1995), l'appellation de Lannuon étant usitée majoritairement par le peuple bretonnant.

Les mégalithes nombreux dans la région, témoignent de son passé ancien, sans compter les silex, haches et pierres polies appartenant aux âges préhistoriques. Les rivières nombreuses et l'or alluvionnaire dans le Massif armoricain, expliquent la présence et la richesse du développement des populations préhistoriques. Près de Lannion, le tumulus de la Motta comporte des bijoux de métal précieux.

En avril 2014, des archéologues de l'Inrap ont effectué des fouilles sur le site de Kervouric, préalablement à la construction de logements. Celui-ci surplombe une vallée encaissée du Léguer. On a ainsi retrouvé la trace de trois grandes maisons de bois et de torchis ainsi que de nombreux vestiges (céramiques, bracelets, silex) du Néolithique, vieux de 7000 ans.

Des monnaies et des débris de poteries attestent la présence gauloise plusieurs siècles avant le début de l’ère chrétienne. Quelques chemins et des fortifications façonnent leur territoire. Le promontoire du Yaudet, estuaire du Léguer, sert à la fois de défense militaire et d'étape pour les navires commerçants, d'après des pièces de monnaies phéniciennes retrouvées sur les lieux.

Le « menhir » du Crec'h à Servel et celui de Saint-Patrice, appelé aussi « menhir de justice » (rue des Frères Lagadec, inclus dans un mur), sont en réalité deux stèles gauloises. La seconde a été christianisée.

Lannion est un passage obligatoire pour franchir le Léguer au plus près de la côte, surtout à marée haute. Du temps des Romains, pour aller du Yaudet par la terre vers l'est, les routes passaient inévitablement par Lannion.

Quelques auteurs attribuent l'origine de Lannion à la destruction de Lexobie (l'actuel Yaudet ?) par les Danois en 836.

Le Léguer, comme les autres cours d'eau, était une voie de pénétration facile pour les envahisseurs, aussi Lannion est-elle dotée d'un château attesté dès le Moyen Âge.

Le premier barrage servant de piège à poisson construit à Servel, long de 190 mètres, formé de pieux reliés par des claies en bois, avec des plates-formes triangulaires ancrant la structure dans le sable et servant aussi de brise-lames, a été construit entre 613 et 615 (ces dates ont été retrouvées grâce à la datation du bois des chênes ayant servi à sa construction) dans l’estuaire du Léguer. À marée descendante, les poissons étaient piégés dans le bassin de retenue.

Lannion est impliqué, pendant la guerre de Cent Ans à la guerre de Succession de Bretagne (rue Geoffroy de Pontblanc, tué en 1346).

En 1587, le 22 mars, commencent les massacres perpétrés pendant les guerres de religion en France, avec l'attaque de Perros-Ploumanac'h, ayant fait allégeance à la Ligue, par les Royaux de La Rochelle. Les paroisses alentour se partagent entre ligueuses (Plestin-les-Grèves), ou royalistes comme Lannion. « Le troisième et septième jours de juillet 1590 fut brûlée et ravagée la paroisse de Plestin par ceux du parti du roi. Et au réciproque le 21 du même mois de juillet 1590 fut pareillement brûlée et ravagée la paroisse de Plouaret, Ploubezre et la ville de Lannion par ceux qui tenaient le parti du duc de Mercœur » a écrit le curé de Lanvellec. Malgré la conversion de Henri IV au catholicisme, Ligueurs et Royaux continuent leurs attaques et les destructions des villes du camp opposé. Mais le Trégor est bien affaibli et le roi ramène la paix, confirmée par l'Édit de Nantes de 1598.

Lannion fait partie de l'évêché de Tréguier avant la Révolution et le roi y contrebalance la puissance de l'évêque en faisant de Lannion le siège de sa juridiction.

Révolte des Bonnets Rouges en 1675. Un de ses habitants fut exclu de l'amnistie royale de février 1676.

Lors de l'ouverture de l'école publique de Lannion le 1er janvier 1882, il n'y a « ni tableaux noirs, ni encriers, ni livres, ni cahiers » ; tout le matériel scolaire se compose de trois petites tables prêtées par le collège.

Le port de Lannion contribue fortement à son essor avant la guerre 1939-1945. Mais au début de la deuxième moitié du XXe siècle, Lannion n'est qu'un « gros bourg ». À l'aube des années 1960, elle est choisie pour accueillir le Centre national d'études des télécommunications (CNET). La petite ville a alors besoin d'importantes réserves foncières pour l'installer, loger les familles des techniciens, d'autant que de nombreuses entreprises vont suivre cette arrivée. Le 25 avril 1961, un arrêté préfectoral scelle la fusion de Lannion avec les communes de Buhulien, Loguivy, Servel et Brélévenez. Au cours des deux premiers mandats suivant cette fusion, chaque ancienne commune continue d'élire ses représentants au conseil municipal du « grand Lannion ». Malgré les crises des années 1980 et 1990, la ville de Lannion a conforté « ses atouts dans les télécoms et la fibre optique » et, avec des sites importants d'Orange ou Nokia, elle est aujourd'hui qualifiée de « mini-Silicon Valley ».

Lannion est le chef-lieu de l'arrondissement de Lannion et fait partie de la cinquième circonscription des Côtes-d'Armor.

Elle est le chef-lieu du canton de Lannion depuis la Révolution. Dans le cadre du redécoupage cantonal de 2014 en France, ce canton voit sa composition modifiée, et Lannion en est le bureau centralisateur.

Lannion est à l'origine de la communauté d'Agglomération de Lannion-Trégor, créée en 2003 et renommée en 2008 Lannion-Trégor Communauté.

La ville a engagé une politique de développement durable en lançant une démarche d'Agenda 21. Cette section est vide, insuffisamment détaillée ou incomplète. Votre aide est la bienvenue ! Comment faire ?

Lannion qui participe depuis plusieurs années au concours des villes fleuries, possède quatre fleurs.

En septembre 2016, un projet d'extraction de sable coquillier dans la baie de Lannion a été stoppé par la Compagnie armoricaine de navigation (CAN), une filiale du groupe Roullier, en raison d'une forte opposition de la population locale. Le 11 septembre, une manifestation avait ainsi réuni entre 4 000 et 5 000 personnes qui rejetaient le projet.

Cette sous-section présente la situation des finances communales de Lannion.

Pour l'exercice 2013, le compte administratif du budget municipal de Lannion s'établit à 33 239 000 € en dépenses et 35 150 000 € en recettes :

En 2013, la section de fonctionnement se répartit en 23 529 000 € de charges (1 121 € par habitant) pour 25 834 000 € de produits (1 231 € par habitant), soit un solde de 2 305 000 € (110 € par habitant), :
* le principal pôle de dépenses de fonctionnement est celui des charges de personnels pour une valeur de 13 204 000 € (56 %), soit 629 € par habitant, ratio inférieur de 14 % à la valeur moyenne pour les communes de la même strate (733 € par habitant). Sur les 5 dernières années, ce ratio augmente de façon continue de 565 € à 629 € par habitant ;
* la plus grande part des recettes est constituée des impôts locaux pour une valeur de 9 196 000 € (36 %), soit 438 € par habitant, ratio inférieur de 21 % à la valeur moyenne pour les communes de la même strate (556 € par habitant). Sur la période 2009 - 2013, ce ratio augmente de façon continue de 399 € à 438 € par habitant.

Les taux des taxes ci-dessous sont votés par la municipalité de Lannion. Ils ont varié de la façon suivante par rapport à 2012 :
* la taxe d'habitation constante 18,50 % ;
* la taxe foncière sur le bâti sans variation 18,67 % ;
* celle sur le non bâti constante 59,09 %.

La section investissement se répartit en emplois et ressources. Pour 2013, les emplois comprennent par ordre d'importance :
* des dépenses d'équipement pour une valeur de 8 510 000 € (88 %), soit 406 € par habitant, ratio voisin de la valeur moyenne de la strate. Sur les 5 dernières années, ce ratio fluctue et présente un minimum de 294 € par habitant en 2011 et un maximum de 406 € par habitant en 2013 ;
* des remboursements d'emprunts pour une somme de 1 178 000 € (12 %), soit 56 € par habitant, ratio inférieur de 42 % à la valeur moyenne pour les communes de la même strate (96 € par habitant).

Les ressources en investissement de Lannion se répartissent principalement en :
* nouvelles dettes pour une valeur de 2 200 000 € (24 %), soit 105 € par habitant, ratio voisin de la valeur moyenne de la strate. Sur les 5 dernières années, ce ratio augmente de façon continue de 0 € à 105 € par habitant ;
* subventions reçues pour une valeur de 1 096 000 € (12 %), soit 52 € par habitant, ratio inférieur de 29 % à la valeur moyenne pour les communes de la même strate (73 € par habitant).

L'endettement de Lannion au 31 décembre 2013 peut s'évaluer à partir de trois critères : l'encours de la dette, l'annuité de la dette et sa capacité de désendettement :
* l'encours de la dette pour 9 634 000 €, soit 459 € par habitant, ratio inférieur de 58 % à la valeur moyenne pour les communes de la même strate (1 092 € par habitant). Depuis 5 ans, ce ratio fluctue et présente un minimum de 391 € par habitant en 2010 et un maximum de 459 € par habitant en 2013 ;
* l'annuité de la dette pour une valeur totale de 1 452 000 €, soit 69 € par habitant, ratio inférieur de 48 % à la valeur moyenne pour les communes de la même strate (133 € par habitant). Pour la période allant de 2009 à 2013, ce ratio fluctue et présente un minimum de 60 € par habitant en 2012 et un maximum de 80 € par habitant en 2009 ;
* la capacité d'autofinancement (CAF) pour une valeur totale de 3 518 000 €, soit 168 € par habitant, ratio voisin de la valeur moyenne de la strate. Sur la période 2009 - 2013, ce ratio diminue de façon continue de 231 € à 168 € par habitant. La capacité de désendettement est d'environ 2 années en 2013. Sur une période de 14 années, ce ratio est constant et faible (inférieur à 4 ans)

L'évolution du nombre d'habitants est connue à travers les recensements de la population effectués dans la commune depuis 1793. À partir de 2006, les populations légales des communes sont publiées annuellement par l'Insee. Le recensement repose désormais sur une collecte d'information annuelle, concernant successivement tous les territoires communaux au cours d'une période de cinq ans. Pour les communes de plus de 10 000 habitants les recensements ont lieu chaque année à la suite d'une enquête par sondage auprès d'un échantillon d'adresses représentant 8 % de leurs logements, contrairement aux autres communes qui ont un recensement réel tous les cinq ans,.

En 2017, la commune comptait 19 880 habitants, en augmentation de 2,58 % par rapport à 2012 (Côtes-d'Armor : +0,55 %, France hors Mayotte : +2,36 %).

La charte de la langue bretonne Ya d'ar brezhoneg a été votée par le conseil municipal le 23 octobre 2006. Le label de niveau 1 de la charte a été accordé à Lannion le 16 novembre 2006.

À la rentrée 2018, 540 élèves étaient scolarisés dans des écoles Diwan et dans les classes bilingues.

La ville possède 8 écoles maternelles publiques, 9 écoles primaires publiques, 2 collèges publics et un lycée public. Lannion possède également des établissements d'enseignement supérieur, tous associés à l'université de Rennes 1 : un IFSI-IFAS (institut de formation en soins infirmiers et aides-soignants), un IUT (Institut Universitaire de Technologie) et une école d'ingénieurs (École nationale supérieure des sciences appliquées et de technologie, ENSSAT). 

À Lannion, il y a 2 écoles maternelles privées, 2 écoles primaires privées, un collège privé et un lycée privé.

L'école Diwan Lannuon (maternelle et primaire) a été créée en 1978 et est située à Loguivy. À la rentrée 2019, 106 élèves y sont scolarisés dans 5 classes.

La ville de Lannion a de nombreux équipements sportifs dont un stade d'eau vive pour la pratique du canoë-kayak.

Le club de canoë-kayak est un des meilleurs nationaux en slalom. Il est régulièrement dans les 3 meilleurs clubs lors des Championnats de France des clubs de Slalom. Plusieurs athlètes se sont illustrés sur la scène internationale : Sébastien Combot (médaille d'or aux championnats du monde 2007), Philippe Quémerais - Yann Le Pennec (5e aux Jeux olympiques 2004).

Le 3 mars 2008, la ville de Lannion dispose d'un espace aquatique ludique baptisé « Ti Dour », remplaçant l'ancienne piscine municipale (qui a définitivement fermé ses portes le 15 février 2008) à la place de l'ancien stade. Elle se trouve à quelques centaines de mètres de l'ancienne piscine. Elle dispose d'un toboggan, d'un espace sportif et d'un espace ludique.

La ville dispose aussi d'un club d'handibasket, le CTH Lannion, évoluant en première division du championnat de France et participant à la Coupe d'Europe. L'équipe première compte dans ces rangs plusieurs internationaux français, dont Franck Etavard (en sélection masculine) et Agniès Etavard (en sélection féminine).

La ville a été sacrée en juin 2014 « ville la plus sportive de France » dans sa catégorie des villes de moins de 20 000 habitants. Le challenge est organisé chaque année par le journal L'Équipe.

En 1960, l'implantation du Centre national d'étude des télécommunications (CNET, devenu France Télécom R&D, maintenant Orange Labs), imprime une forte orientation à l'industrie de la ville. D'autres entreprises de hautes technologies emboîtent le pas : Alcatel, Sagem…

Cette affluence d'entreprises a aussi été propice à l'installation d'un IUT, l'IUT de Lannion, et d'une école d'ingénieurs en 1986 : l'ENSSAT (École nationale supérieure des sciences appliquées et de technologie). Lannion regroupe désormais une partie importante des activités de recherche en télécommunication en France, au sein de la technopole Anticipa, qui comporte plus d'une centaine de PME et PMI. Le siège du pôle de compétitivité à vocation mondiale, « Images et Réseaux » se trouve à Lannion, de même que la grappe d’entreprises Photonics Bretagne. La ville possède une antenne de la Chambre de commerce et d'industrie des Côtes-d'Armor. La synergie de ces organismes et associations permet au bassin de Lannion et du Trégor-Goëlo d'être une référence en matière d'innovation dans différents secteurs :
* TIC (95 entreprises) ;
* optique/photonique (20 entreprises) ;
* industrie agro-alimentaire ;
* industrie marine (31 entreprises).

Lannion est également la commune d'où le câble sous-marin « APOLLO undersea south cable » américain aboutit en France. Le site est considéré comme d'importance stratégique et vital pour les États-Unis selon un document secret émanant des révélations de télégrammes de la diplomatie américaine par WikiLeaks. Le câble sous-marin WASACE Nord (en) reliant la France aux États-Unis, mis en service en 2014, part également de Lannion.

Lannion se trouve dans une zone géographique stratégique pour son économie. À mi-chemin entre Brest et Rennes, ces deux pôles économiques sont très fortement liés à Lannion pour le développement des technologies.

D'azur à l'agneau pascal couché d'argent portant une croix haute d'or au guidon de gueules chargé de l'inscription LAUS DEO en lettres capitales aussi d'or.

Les armes de la ville reprennent la devise Laus deo soit « louange à Dieu ».
