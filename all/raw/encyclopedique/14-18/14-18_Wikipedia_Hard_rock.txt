

Jimmy Page interprète Stairway to Heaven avec une Gibson EDS-1275.

Le hard rock est issu du blues urbain, du rock britannique et
du British blues^. Il tire aussi ses influences du
rock 'n' roll et du garage rock, remontant à Elvis
Presley et les Beatles, tous des artistes également influencés par
le blues américain^. Les membres de Led Zeppelin, et
plus particulièrement le guitariste Jimmy Page, se sont inspirés
du blues américain de Muddy Waters et Skip James et du
blues traditionnel de Howlin' Wolf. Les compositions
d'AC/DC sont mesurées en mesure binaire et recourent aux
gammes pentatoniques. Autant les riffs que les solos de
guitares sont empruntés au blues ; d'ailleurs Angus Young cite des
joueurs de blues et de rock 'n' roll comme B.B. King,
Buddy Guy, Little Richard, John Lee Hooker et Jimi


Le blues constitue également la base de la musique de Jimi
Hendrix. En effet, il reprend certaines techniques des grands bluesmen
qui permettent de développer un jeu expressif, mais aussi leur langage
harmonique où l'ambiguïté majeur/mineur joue un rôle important. Il
fait, notamment, des reprises de blues de Albert King (Born

Elmore James (Bleeding Heart), Hubert Sumlin, le guitariste de
Howlin' Wolf (Killing Floor^) par exemple. Deep Purple
amène des influences classiques au hard rock, notamment à travers
l'organiste Jon Lord et le guitariste Ritchie Blackmore
apportant un côté épique. Le rock psychédélique joue un grand rôle
dans l'histoire du hard rock. Ce genre est représenté par les Pink
Floyd et les Doors et se caractérise par des sons de guitares
distordus, de longs solos et des compositions parfois complexes. Le
hard rock est en même temps une déviance ou une radicalisation du rock
psychédélique et de la culture hippie^. On peut y inclure
les groupes américains bruyants et répétitifs Ted Nugent,

pionniers du hard rock ont commencé comme membres de groupes de blues
psychédéliques, y compris Black Sabbath, Deep Purple,

rock comme Jeff Beck, Jimmy Page et Ritchie Blackmore
sont également passés par le rock psychédélique.

Précurseurs du protopunk, certains groupes de hard rock comme les
MC5 et les New York Dolls produisent un son volontairement
sale et de mauvaise qualité caractéristique du garage rock.
D'ailleurs, il n'est pas rare de retrouver cette caractéristique dans
les premières productions de groupes de hard rock.

Le rock sudiste, un style exclusivement nord-américain, développe
des sonorités le rapprochant parfois du hard rock, plus rarement du
metal pour certaines formations. Le groupe le plus célèbre est
celui des vétérans de Lynyrd Skynyrd, bien que d'autres formations
aient pu rencontrer un grand succès dans les années 1970 (Allman

musiques américaines (country, jazz, rock'n'roll, blues) auxquelles se
mêle un son typique de heavy rock, le rock sudiste se caractérise par
ses sonorités de slide guitar et l'alignement fréquent de trois à
quatre guitaristes de front, permettant des combinaisons harmoniques et
des dialogues très caractéristiques. Free Bird de Lynyrd
Skynyrd, ou Highway Song de BlackFoot sont des titres



Le hard rock résulte d'une évolution de la musique de l'époque, tant au
niveau de la technique qu'au niveau de son approche du son, au côté
notamment du punk rock, du blues rock, du heavy metal et
du glam rock. Son instrumentation se caractérise par l'emploi
d'une ou deux guitares électriques, d'une guitare basse, d'une
batterie ainsi que la voix. L'agressivité est due à la saturation
des guitares. Ce son particulièrement riche en harmoniques et qui
dégage une impression de puissance est obtenu en mettant le gain de
l'amplificateur plus fort que le volume, ce qui provoque un
maximale et est par conséquent tronqué, ce qui provoque une distorsion
du son. Jimi Hendrix travaillait avec son ingénieur pour
créer de nouveaux sons en modifiant l'électronique ; ceci a donné
naissance à des systèmes électroniques servant à modifier le son sans


Le hard rock repose essentiellement sur les power-chords, ou
accords de puissance en français. Par exemple, le morceau Highway to
Hell (La 5, La 7, Fa#, Sol 5) repose essentiellement sur ces
power-chords. La technique du slide est répandue aussi dans le
hard rock : elle consiste à faire glisser un doigt le long d'une corde
sur le manche, afin de produire un effet de glissando, montant ou
descendant. Le morceau I Love Rock 'n' Roll de Alan Merrill
et Jake Hooker du groupe The Arrows illustre cette technique
et fait également appel aux power-chords et aux bends, le tout


En raison de ces traits particuliers, une distinction spécifique s'est
lentement dessinée entre heavy metal et hard rock. Si les deux
s'appuient sur la mise en avant des guitares et sur une structure à
base de riffs, le heavy metal diffère tout particulièrement du hard
rock dans le fait que les structures harmoniques et mélodiques du
blues sont remplacées par des progressions modales et des
relations tonales instables (chromatisme, intervalles
dissonants, progressions d'accords noyant l'orthodoxie


Une autre particularité sont les célèbres power ballad.
Typiquement, une power ballad s'ouvre sur un air doux et mélodique, à
la guitare ou au synthétiseur. Plus tard dans la chanson,
souvent après le refrain, intervient la batterie. Le volume sonore
et l'effet dramatique qui l'accompagne augmente encore avec l'ajout
d'une guitare électrique en distorsion. La fin de la chanson peut


Le hard rock aborde de nombreux thèmes tels que la
science-fiction, la fantasy, la libre pensée mais aussi
des thèmes humanitaires, sociaux ou environnementaux (cf. par exemple
Rush et Jimi Hendrix). Les chansons de Queen et de
Led Zeppelin réemploient beaucoup d'airs qui traitaient de
romance, d'amour non partagé ou de conquête sexuelle, des thèmes
courants du rock, du pop et du blues. Certains textes, en particulier
ceux de Led Zeppelin, ont été considérés comme misogynes. Beaucoup
de chansons évoquent également la religion (notamment celles
d'Alice Cooper), la mort et la violence (If You Want Blood (You've
Got It) d'AC/DC), la guerre (Civil War des Guns N'
Roses) et les drogues, explicitement et implicitement comme



Durant les années 1970, à côté du hard rock, se développe un nouveau
genre de rock dit « heavy metal », représenté par Black
Sabbath puis notamment Judas Priest. En général, le hard rock
s'inspire directement du blues et utilise des gammes pentatoniques
mineures alors que le heavy metal s'oriente davantage vers des
rythmiques lourdes et puissantes qui l'éloignent du shuffle.
Certains groupes comme Judas Priest se sont radicalisés avec le
temps, alors que leur musique au début de leur carrière pouvait parfois
une confusion entre le hard rock et le heavy metal. La distinction est
parfois subtile. Beaucoup de groupes composent dans les deux genres
comme Def Leppard et Mötley Crüe, tandis que d'autres comme
AC/DC et Aerosmith restent fidèles au hard rock. La
distinction entre hard rock et heavy metal est débattue depuis le
milieu des années 1980 : l'expression « heavy metal » est

Creem^. Toutefois, cette distinction est sujette à
controverses ; Lester Bangs est décédé peu après avoir livré sa
définition et n'a eu pas le temps de la défendre et de l'affiner. Selon
lui, le hard rock se définit avant tout comme une musique de la période
1968-1976 ; le heavy metal, lui, se définit par un certain nombre
d'attributs stylistiques (look cuir, clous et badges) et par les thèmes
des chansons tels que l'horreur et la science-fiction.

Cependant, il existe des exceptions à la règle chronologique :
Guns N' Roses, groupe connu à la fin des années 1980, est
généralement classé dans la catégorie hard rock. Le premier album de
Black Sabbath, souvent considéré comme le premier album de heavy
metal, sort dès 1969. L. Bangs lui-même classe le groupe Scorpions
dans le heavy metal alors que ses membres ne possèdent aucun des
attributs qu'il évoque (les membres de Scorpions préféraient clairement
les foulards aux clous). De ce fait, comme le précisent très justement
Jean-Marie Leduc et Jean-Noël Ogouz, « la distinction entre hard


L’Encyclopedia of Rock & Roll de Rolling Stone ne fait pas de
distinction entre hard rock et heavy metal. Seule l'expression « heavy
metal » est définie et englobe les composantes hard rock et heavy metal
en faisant référence à la chanson de Steppenwolf, Born to Be
Wild (1968) : « heavy metal thunder ». L'encyclopédie de Rolling Stone
journaliste chez Rolling Stone), sans en tenir compte. L'expression


En France, le grand public ne connaissait jusque dans les
années 1990 que l'expression « hard rock », « heavy metal »
n'étant utilisé que par les connaisseurs. Depuis, le terme « metal »






Comme précédemment évoqué, l'une des influences majeures du hard rock
est la musique blues, particulièrement le blues britannique. Des
groupes de rock britanniques tels que Cream, The Rolling
Stones, The Beatles, The Yardbirds, The Who et The
Kinks modifièrent le rock and roll, ajoutant un son puissant, des
lourds riffs de guitare, une batterie imposante et des voix fortes. Ce
nouveau son posa les bases du hard rock. Les chansons You Really
Got Me des Kinks, puis Happenings Ten Years Time Ago des Yardbirds,
I Can See for Miles des Who et Helter Skelter des Beatles
sont parmi les premières relevant de ce nouveau genre. Dans le même
temps, Jimi Hendrix produisit une forme de blues influencée par le
rock psychédélique, combinée avec des éléments de jazz et de
rock and roll, créant un genre unique. Il fut l'un des premiers
guitaristes à expérimenter des effets de guitare comme le phasing, le
feedback et la distorsion, avec Dave Davies des Kinks,
Pete Townshend des Who, Eric Clapton de Cream et Jeff


Le hard rock émergea avec des groupes britanniques de la fin des
années 1960, notamment Led Zeppelin et Black Sabbath.
Ils mélangeaient la musique des premiers groupes de rock britanniques
avec des formes dures de blues rock et d'acid rock. Deep
Purple fit aussi partie des groupes pionniers avec les albums
Shades of Deep Purple (1968), The Book of Taliesyn (1968), et
Deep Purple (1969). Le premier album éponyme (1969) de Led
Zeppelin et le Live at Leeds (1970) des Who sont aussi des albums
qui ont marqué les débuts du hard rock. Le groupe allemand
Scorpions se fonde dès 1965, mais leur premier album ne voit le





En 1970, Black Sabbath sort ce qui est considéré comme le
premier album de heavy metal. La musique de Black Sabbath
révolutionne le rock par ses paroles sombres, ses riffs puissants et
son atmosphère lourde. Cette nouvelle approche transforme le hard rock
de l'époque en la première forme de heavy metal. Le troisième et
quatrième album de Led Zeppelin, Led Zeppelin III (1970) et
Led Zeppelin IV (1971) furent plus orientés folk rock que les
précédents. Le quatrième contient la chanson Stairway to Heaven,
souvent considérée comme épique et même comme un classique du genre.
Elle commence en acoustique pour progresser jusqu'à un hard rock
chanson tirée d'un album la plus jouée par les radios^.

Avec l'album In Rock, Deep Purple radicalise le hard rock et se
fait cette fois-ci supplanter par des inspirations classiques.
L'originalité vient des chorus orgue et guitare orientant le hard rock
vers plus de virtuosité. Ce titre contient un autre archétype du hard
rock, Child in Time. Le groupe continue avec Machine Head en
1972. Deux chansons de cet album ont un énorme succès : Highway
Star (souvent considérée comme la première chanson de speed metal)
et Smoke on the Water, dont le riff principal devint la signature
du groupe et l'un des riffs les plus célèbres du rock. L'album live
Made in Japan paru en 1972, est vu par certains comme le meilleur
album live de l'histoire du hard rock. En 1972 et 1973, le pionnier du
shock rock Alice Cooper connaît le succès avec ses albums
School's Out et Billion Dollar Babies, qui rentreront dans le
top 10 américain. Toujours en 1973, les groupes Aerosmith et
Queen sortent leurs premiers albums éponymes et démontrent une
direction plus large du hard rock avec un certain goût pour les power
ballads. En 1974, Bad Company sort son premier album, et Queen
sort son troisième album, Sheer Heart Attack, avec le morceau
Stone Cold Crazy. Ce dernier influencera les premiers groupes de
thrash metal nés au début des années 1980, tels que Metallica

glam rock, au heavy metal, au rock progressif et même à


Kiss sort ses trois premiers albums Kiss (1974), Hotter
Than Hell (1974) et Dressed to Kill (1975), achevant sa percée
commerciale avec le double album live Alive! (1975). Kiss enchaîne
ensuite les albums à succès : Destroyer (1976), Rock and Roll
Over (1976), Love Gun (1977). Deep Purple s'oriente vers un hard
rock teinté de blues et de soul avec l'album Burn en 1974. Dans le
milieu des années 1970, Aerosmith sort les albums Toys in the
Attic et Rocks, qui vont influencer des groupes aussi divers que



En 1975, Alice Cooper entame sa carrière solo avec son album culte
Welcome to My Nightmare, disque de hard rock jazzy très personnel
souvent cité comme le plus représentatif de l'artiste^.
Pendant ce temps, le groupe australien AC/DC, dont les trois
premiers albums sont sortis en 1975 et 1976, devient célèbre en
Australie et commence à se faire connaître en Europe. En 1976,
Deep Purple se dissout après la mort de Tommy Bolin, après
avoir été tenté par le funk avec l'album Come Taste the Band.
Ritchie Blackmore avec son groupe Rainbow dont le chanteur
est Ronnie James Dio sort Rising. Ce disque teinté
d'occultisme est nettement orienté vers ce qui deviendra le speed
metal. La même année, le groupe Boston instaure les bases du hard
FM en sortant son premier album, avec le titre More Than a Feeling. Le
groupe Heart ouvre le genre aux femmes avec la sortie de leur
premier album. En 1977, AC/DC sort Let There Be Rock avec le
nouveau bassiste Cliff Williams, grand succès au Royaume-Uni qui



En 1976, Kiss sort Destroyer qui se vend à environ 2 millions
d'exemplaires, rien qu'en précommande. L'année suivante, Kiss sort
Love Gun, un succès sans précédent dont la chanson éponyme est
aujourd'hui considérée comme un classique du groupe. En 1978, le
batteur de The Who, Keith Moon, meurt d'une overdose dans son
sommeil. Avec la montée du disco aux États-Unis et du punk
rock au Royaume-Uni, le hard rock commence à perdre sa popularité. Le
disco s'adresse à un public plus large, tandis que le punk attire le
public rebelle que séduisait jusqu'alors le hard rock. Van Halen
fait son apparition et devient un groupe incontournable du hard rock
avec leur premier album éponyme. Leur musique est basée
essentiellement sur les compétences du guitariste Eddie Van Halen,
qui introduit une nouvelle technique appelée le tapping.
Cette technique, qui influencera tant d'autres, est utilisé dans la
chanson Eruption. Le tapping est utilisé par quelques guitaristes
de l'époque dont Ace Frehley de Kiss, qui participe aux débuts de
la technique sur l'album Alive II (1978). La même année est formé
Great White à Los Angeles. Moins hard rock que Van Halen
ou AC/DC, le groupe se tourne vers un hard rock plus bluesy.

En 1979, Scorpions, qui a déjà sorti plusieurs albums, se sépare
de son guitariste virtuose Ulrich Roth et engage Matthias
Jabs au jeu moins baroque et plus moderne, proche de celui de Van
Halen. Il en résulte l'album Lovedrive, pivot de la carrière du
groupe, qui va le lancer sur la scène internationale et notamment aux
sans cesse en grandissant. Au même moment, Kiss sort Dynasty dont
la chanson I Was Made for Lovin' You est souvent considérée comme
du « hard-disco ». Cet album sera un hit planétaire et se vendra à plus
de 10 millions d'exemplaires seulement aux États-Unis.


La même année, le groupe australien AC/DC sort l'un de ses
plus grands albums, Highway to Hell, premier succès du groupe aux
qu'il y a à différencier hard rock et heavy metal. Leur musique est
tellement empreinte de blues qu'il est difficile de les classer dans le
heavy metal. Leur son hard rock deviendra toutefois de plus en plus dur
in Black frise le heavy metal traditionnel. Les deux albums suivants,
For Those About to Rock We Salute You (1981) et Flick of the
Switch (1983), sont encore plus durs, plus heavy mais seul The
Razors Edge, sorti en 1990, pourrait réellement être qualifié de heavy



Après la mort du batteur John Bonham, en 1980, Led Zeppelin
se sépare. De même, durant la même année, le chanteur d'AC/DC,
Bon Scott, perd la vie. Avec ces décès, la première vague des
groupes de hard rock « classiques » prend fin. Certains groupes, tels
que Queen, s'éloignent de leurs racines hard rock pour s'orienter
vers le pop-rock. AC/DC recrute un nouveau chanteur, Brian
Johnson, et sort l'album Back in Black, qui connaitra un succès
monde tous genre de musique confondus juste derrière Thriller de
Michael Jackson^. Ozzy Osbourne, l'ancien chanteur de
Black Sabbath, sort son premier album solo, Blizzard of Ozz, avec
la collaboration du guitariste américain Randy Rhoads.

En 1981, un groupe américain, Mötley Crüe, sort Too Fast for
Love, qui suscite un intérêt pour le style glam metal. Un an
après, le style se propage, mené par des groupes tels que Twisted
Sister et Quiet Riot. Pendant ce temps en Europe, Scorpions
enregistre l'album Blackout qui sortira en 1982, et sera nommé
hard rock mélodique et de guitares plus lourdes, qui va véritablement
les placer sur le devant de la scène hard rock. En 1983, Def
Leppard, un groupe anglais, sort l'album Pyromania, qui atteignit
la seconde place des charts américains. Leur musique était un mélange
de glam rock et de heavy metal et a influencé beaucoup de groupes de
glam rock et de hard rock des années 1980. La même année, Mötley Crüe
sort l'album Shout at the Devil, qui devient un énorme succès.

L'album de Van Halen, 1984, devint un énorme succès,
percutant ainsi la seconde place du Billboard album charts. En
particulier, la chanson Jump atteint la première place du
singles chart (où elle est restée pendant plusieurs semaines) et
est considérée comme l'une des chansons rock les plus populaires jamais
constantes et répétitives utilisations de claviers et de
synthétiseurs, marquant un éloignement de leurs origines basées
sur le jeu de guitare. Cette même année, Scorpions sort l'album
qui va le placer au summum de son succès: Love at First Sting, qui
contient la chanson qui deviendra un classique du hard rock et du heavy
metal, Rock You Like a Hurricane. La ballade Still Loving You
sera un succès planétaire, et peut être encore souvent entendue à la


Le milieu des années 1980 voit l'apogée de groupes de hard rock plus ou
moins glam, à caractère FM tels que Ratt ou Autograph, qui ne
connaîtront néanmoins qu'un succès mitigé en Europe. En effet, la
grande scène hard rock se déroule aux États-Unis, perçus comme une
terre promise pour la majorité des grands groupes de rock britanniques
ayant déjà profondément percé dans les charts.En 1984, Deep Purple se
reforme et connaît un grand succès avec l'album Perfect strangers.


La fin des années 1980 voit la période où le hard rock axé FM a le plus
de succès commercial. De nombreux albums hard FM sont classées dans le
top des charts. Un de ces hits était l'album Slippery When Wet
Billboard 200 et devint le premier album hard rock à frayer trois
singles du Top 10, dont deux ayant atteint la première place. En plus,
la chanson populaire The Final Countdown du groupe suédois
Europe sort en 1986. Après une série d'albums expérimentaux fort
peu médiatiques et une cure de désintoxication de l'alcool, Alice
Cooper entame son retour sur le devant de la scène en 1986 et 1987,
avec deux albums radicalement orientés hard rock (Constrictor et
Raise Your Fist and Yell). En 1987, les plus gros succès dans les
charts étaient Appetite for Destruction des Guns N' Roses,
Hysteria de Def Leppard, et Girls, Girls, Girls de Mötley
Crüe. Cette année est aussi marquée par le grand retour


En 1988 et 1989, les succès les plus notables étaient New Jersey

Mötley Crüe. New Jersey engendre cinq singles classés dans le Top
10 des singles, mais le record de singles pour un album de hard rock
est détenu par Hysteria (sorti en 1987) de Def Leppard, avec sept
singles. En 1989, Skid Row sort son premier album. Il





Le début des années 1990 fut dominé en premier lieu par les Guns
N' Roses et AC/DC. Le disque The Razors Edge de AC/DC en
1990, et les disques Use Your Illusion I et Use Your Illusion
II des Guns N' Roses en 1991 accentuent leur popularité, alors même que
l'album d'Alice Cooper, Trash, sorti en 1989, rencontre un
franc succès médiatique^ et qu'Aerosmith sort en 1993
Get a Grip, leur disque le plus vendu suivi d'une très grande
tournée à travers le monde. Crazy World est le dernier grand
succès de Scorpions, propulsé par le succès du hit Wind of
Change (numéro 1 dans onze pays). Mais cette popularité s'estompa,
rendant la musique et les attitudes plus décadentes et


En 1991, ce déclin fera éclater une nouvelle forme de rock dure dans la
scène musicale : le grunge, un combiné de rock alternatif,
punk hardcore et de heavy metal, utilise un son issu de
guitares lourdes à distorsion, du fuzz et du feedback. Bien que la
plupart des groupes grunge avaient un son qui contrastait nettement
avec le hard rock (par exemple Nirvana, Mudhoney et L7),
une minorité (par exemple Pearl Jam, Mother Love Bone,
Temple of the Dog et Soundgarden) était très influencée par
le rock et le metal des années 1970 et 1980. Cependant, tous les
groupes grunge esquivaient le machisme et le style axé sur la mode
du hard rock de l'époque. Parmi les plus grands groupes de hard rock
formés au début des années 2000, on peut compter les supergroupes
Velvet Revolver et Audioslave. Audioslave était composé des
instrumentistes du groupe de metal Rage Against the Machine et de
l'ancien chanteur de Soundgarden, jusqu'à sa dissolution en 2007.
Et Velvet Revolver était composé des ex-membres des Guns N' Roses
avec le chanteur de Stone Temple Pilots. Les années 2000 verront
aussi les réunions de plusieurs groupes de hard rock s'étant dissous
auparavant comme Led Zeppelin, Stone Temple Pilots, Living
Colour ainsi que les réunions d'anciennes formations de groupe de hard



Malgré tout, de nouveaux groupes sont apparus sur la scène entre 2000
et 2010, tels que le groupe australien Airbourne, considéré par
certains comme les descendants d'AC/DC et ayant sorti son premier
album, Runnin,Wild, en 2008. Avec des riffs accrocheurs, ils furent
largement acclamés par la presse mondiale. Un peu plus loin dans
le temps, on retrouve le groupe britannique The Darkness,
controversé car considéré comme un groupe comique imitant les traits du
glam metal et du hard rock : le groupe fut tout de même reconnu
par certains comme digne successeur du mouvement hard rock, d'autant
qu'il était capable de riffs très accrocheurs et heavy.
Malheureusement, le groupe se sépare suite aux problèmes de
drogues du leader Justin Hawkins. Le groupe Australien
Wolfmother est lui aussi considéré unanimement comme un digne
descendant des premiers groupes de hard rock tels que Deep Purple, Led



Quelques groupes de hard rock des années 1970 et 1980 réussissent à
maintenir le grand succès de leurs carrière dans toutes les années 1990
et 2000 en se ré-inventant constamment et en explorant différents
styles musicaux, à savoir Aerosmith, Bon Jovi, et AC/DC.
Depuis Pump en 1989, Aerosmith sort deux albums n^o 1 multi-platines :
Get a Grip en 1993 et Nine Lives en 1997. Get a Grip engendre
quatre singles dans le Top 40 et devient l'album le mieux vendu du
groupe dans le monde entier, passant aux ventes d'environ 20 millions
d'exemplaires. En plus, Aerosmith sor un album platine n^o 2, Just
Push Play (2001), lequel voit le groupe faire une incursion en outre
dans la pop, et un album de reprises de blues, Honkin' on Bobo,


En outre, depuis le début des années 1990, Aerosmith atteint huit
singles dans le Top 40 (incluant le hit n^o 1 I Don't Want to Miss a
Thing en 1998). Bon Jovi sort cinq albums qui atteignent le statut de
platine ou mieux et aussi atteint huit singles dans le Top 40 depuis
New Jersey en 1988. En outre, avec leur vrai retour au hard rock
avec des chansons comme Keep the Faith et It's My Life, Bon Jovi
atteint le succès dans le genre « adulte contemporain », avec les
ballades du Top 10 Bed of Roses (1993) et Always (1994) et également
dans le country avec Who Says You Can't Go Home, qui atteignent le
n^o 1 dans le Hot Country Singles chart en 2006 et l'album country/rock
Lost Highway qui atteint le n^o 1 en 2007. En 2009, Bon Jovi sort un
autre album n^o 1, The Circle. Depuis le multi-platine The Razors
Edge en 1990, AC/DC sort deux albums doubles platines #1,
Ballbreaker (1995) et Black Ice (2008), ainsi que le disque
de platine Stiff Upper Lip (2000). Tous ces groupes reçoivent des
Grammy Awards pour leur travail durant cette période de temps et
tous ces groupes continuent à faire des tournées à succès. En 2013,
Black Sabbath se reforme. Deep Purple continuant à tourner dans le
monde entier, en restant populaire voit le succès de leur album Now


