
Selon La Voile noire des pirates : « Installés en petites
communautés autonomes, principalement à Saint-Domingue et sur
l’île de la Tortue, les boucaniers vivaient de la chasse du bœuf et
du cochon sauvages, dont ils fumaient la viande et vendaient les peaux.
Constituées de marins déserteurs, de naufragés, de colons appauvris,
d’engagés, de renégats, d’esclaves en fuite et de flibustiers fatigués
de la course, ces communautés cosmopolites apparurent dès 1630 sur
Saint-Domingue. »^ Elles profitèrent de la décision de l’Espagne
d’abandonner, en 1606, les côtes nord et ouest de l’île pour en
supprimer les trafics des nations étrangères sur son « territoire ».
Particulièrement redoutables, les boucaniers poussèrent l'Espagne à se
doter de soldats spécialement chargés de les combattre, les


Reprenant à leur compte les techniques des Indiens pour fumer et
griller la viande, les boucaniers la faisaient cuire en fines lanières
sur un gril de bois (le « boucan ») fait de claies de branches
odorantes. Cette forme de cuisson fumée permettait une excellente
conservation de la viande. Quant aux peaux, arrosées de gros sel,



Par extension, le terme de boucanier a désigné un écumeur de mer, un
pirate, après que les boucaniers se sont associés aux flibustiers
sous le vocable de « Frères de la Côte ». Il s'agit alors
d'aventuriers, corsaires ou pirates, qui vivaient essentiellement du
produit de leur chasse et de contrebande. Certains flibustiers,
fatigués de la course, rejoignaient leurs rangs, alors qu'au contraire,
certains boucaniers prenaient temporairement la mer sur les navires
flibustiers de passage. Par la suite, la France les reconnut et


Les boucaniers furent ainsi à l’origine de la colonie française de
Saint-Domingue, qui deviendra quelque temps plus tard, sous le nom
d’Haïti, la première nation « noire » du monde moderne. Une nation
d’anciens esclaves, de parias et de rebelles, qui obtinrent leur
indépendance dans le sang et la révolte, tout comme ces Frères de


On nomma également boucaniers les coupeurs de bois de teinture qui
vivaient entre la baie de Campêche et le Honduras. Comme ceux
de Saint-Domingue, ils pratiquaient la chasse, le travail du cuir et de
la viande, et s’adonnaient à la flibuste lorsque l’occasion s’en
présentait. Au milieu du XVIII^e siècle, certains d’entre eux allèrent
jusqu’en Argentine et en Uruguay, où les bovins lâchés par les
Espagnols s’étaient multipliés, et y installèrent leurs abattoirs près
des côtes. Ils devinrent par la suite de simples commerçants, allant
jusqu’à acheter la viande de leur propre consommation à d’autres, par
exemple dans les saladeros créés par les Espagnols, en la payant



Remarquables chasseurs, les boucaniers étaient aussi très à l’aise
sur un bateau. De constitution solide et bien nourris, ils étaient
redoutables combattants. Par ailleurs, ils étaient tous porteurs d’un
fusil de quatre pieds de canon appelé le « fusil à giboyer », de
qualité inégalée, comme le précise Exquemelin. La précision de leur
tir permettait aux flibustiers de supprimer à distance une bonne partie
de l’équipage adverse, ce qui évitait l'abordage ou à se risquer trop
près des canons ennemis. Ces fusils se chargeaient d’une manière
exceptionnellement rapide pour l’époque et pouvaient tirer trois coups
pendant qu’un fusil militaire n’en tirait qu’un seul. La poudre, de
première qualité et également fabriquée tout exprès pour eux, venait de
Cherbourg ; on l’appelait « poudre de boucanier » et elle se
conservait dans des calebasses ou tubes de bambou bouchés à la
cire. Les flibustiers, souvent anciens boucaniers, étaient accrochés en
haut des mâts et décimaient leurs adversaires avec une facilité


Organisés en marge de la société, les boucaniers vivaient retirés, mais
non en autarcie. Ils étaient bien introduits dans le marché économique,
vendant viande, cuir et tabac, tandis qu’ils s’approvisionnaient en
armes, munitions, vêtements et autres. Certains d’entre eux
développèrent même leur propre réseau de correspondants en Europe, avec
des associés ou des parents, devenant ainsi de véritables entrepreneurs
de chasse. Très solidaires entre eux, ils n’eurent jamais de chefs ni



Au moment de leur apogée, vers 1665-1667, les boucaniers étaient entre
800 et 1000 à chasser sur Saint-Domingue, évoluant en symbiose avec
leurs frères flibustiers, au point qu’avec le temps ces deux termes en
vinrent à se mélanger sous l’appellation générique de Frères de la
Côte. En anglais, on désigne d’ailleurs les flibustiers sous le nom
générique de Buccaneers. À la fin du XVII^e et au tournant du
siècle, leurs aventures respectives finirent d’ailleurs par n’en faire
plus qu’une : dès 1694, la colonie de l'île de la Tortue se vida
complètement et les Espagnols trouvèrent enfin la parade pour se
débarrasser des boucaniers : ils décimèrent le gibier et les chasseurs
furent obligés de se convertir en flibustiers ou de se noyer dans la



Dans son livre La Voile noire, Mikhaïl W. Ramseier affirme que
s'il est difficile de considérer la piraterie européenne comme un
mouvement uni et solidaire, regroupé autour d’un idéal et d’une morale
libertaires, comme le prétendent des auteurs engagés tels Gilles
Lapouge ou Michel Le Bris, il est en revanche certain que s’il est
possible de parler d’une forme de contre-société organisée, ce sont les
Frères de la Côte, et en particulier les boucaniers, qui


