

Dans la Rome antique, on distinguait entre l'Ager publicus :
territoire qui appartient au peuple romain, propriété commune,
c'est-à-dire domaine public ; et l'Ager romanus : territoire qui


Si le concept est ancien, l'expression propriété publique en elle-même
est plus récente et a pris une signification particulière.


En 1773, le livre l' Alambic des loix critique les personnes/seigneurs
qui «jouissent de l'alégresse de la propriété publique sans crainte».

Peu avant la révolution française, l'expression de propriété publique
apparaît dans un ouvrage de 1775^. Il est alors écrit au sujet
de la Compagnie des Indes, qu'«un privilège exclusif, qui n'est ni
ne peut être en lui-même qu'une violation de la propriété publique de
tous les Citoyens ne saurait jamais former une véritable propriété


Avant la révolution, est édité un livre qui définit les «Chofes de


telles font les fontaines, les champs communs, dont l'usage peut
bien être limité aux habitans d'une ville d'un bourg ou d'un village
exclusivement aux habitans d'un autre endroit mais qui ne peut être
disputé à aucun de ceux qui composent la communauté pour laquelle




Le tome 4 de l' Encyclopédie méthodique, consacré à la jurisprudence,
indique à l'époque que la propriété publique est ce qui, n'appartenant


Avec la révolution, la thématique de la propriété publique sera par la
suite débattue dans le domaine du partage des biens du clergé, de la



Au Canada, la Loi constitutionnelle de 1867 introduit la notion de
propriété publique dans l'énuméré 1A de l'article 91, énuméré intitulé
cette loi, que celles-ci sont sous l’autorité législative exclusive du
parlement du Canada plutôt que sous celle de la Reine ou aux


Ainsi, si l'expression propriété publique dans le sens du
XVIII^e siècle est tombée en désuétude en France, l'expression est



En 2007 au Canada, il apparaît que le caractère fédéral de la propriété
publique lui permet de pas être soumise aux règlements locaux ^.

