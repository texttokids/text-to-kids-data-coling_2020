

cellule, ainsi que la machinerie nécessaire à la réplication
des chromosomes et à l'expression de l'information contenue
dans les gènes. Il est entouré par une double membrane appelée


replis et de tubules membranaires qui délimitent un compartiment
interne séparé du reste du cytosol. Certaines protéines, lors
de leur synthèse, transitent par le réticulum pour y être maturées.
Participent aussi à la synthèse des lipides (11).
cellulaires (protéines glycosylées en général et polysaccharides
non cellulosiques, en particulier chez les végétaux), et complété
par des vacuoles de condensation (absentes du schéma).
eucaryotes contenant leur propre génome, transmises par voie
femelle dans certains cas de reproduction sexuée.
phospholipidique simple, au contenu acide, remplies d'enzymes de
dégradation et chargées chez les cellules animales de la

membrane plasmique lors de l'absorption de molécules complexes,
et se combinant avec les lysosomes pour leur digestion par

membrane, contiennent des enzymes qui oxydent divers substrats en
leur arrachant de l'hydrogène qui est alors transféré à l'oxygène
pour former de l'eau oxygénée ; cette eau oxygénée est ensuite

certains protistes eucaryotes, de quelques mycètes et
d'au moins un métazoaire (loricifère)^. C'est un
organite fermentaire de Trychomonas et Blastocystis par exemple,

dont la physiologie énergétique est encore méconnue : fermentation
et capture directe d'ATP provenant de la cellule parasitée...


tonoplaste) assure des fonctions de dégradation cellulaire
l'espace (dans des grandes cellules végétales qui se gonflent d'eau




photosynthèse. Ils utilisent l'énergie du soleil grâce à des
pigments photosynthétiques tels que la chlorophylle et les
caroténoïdes pour arracher de l'hydrogène à l'eau et fixer
les atomes de carbones du gaz carbonique sous forme de carbone
organique, ce qui permet entre autres la synthèse de sucres
l'eau libère de l'oxygène. Les plastes dépourvus de pigments
sont des leucoplastes, dont certains sont spécialisés (absents
du schéma) dans le stockage d'amidon (amyloplastes), de lipides




la synthèse des protéines à partir de l’ARNm messager

réticulum endoplasmique (5) ou de l’appareil de Golgi
cytoplasme (le cytoplasme comprend aussi les organites) ;
endoplasmiques, présente uniquement dans les cellules animales,
l'ensemble forme le centrosome qui est toujours à proximité du

responsable de la dégradation des protéines, véritable centre de




espèces^ ; il est notamment utilisé pour la classification



En biologie, les organes sont définis comme des unités
fonctionnelles confiné au sein d'un organisme. L'analogie des organes
avec les organites microscopiques est évidente, de même que, dans les
premières œuvres, les auteurs de manuels scolaires donnaient rarement
des précisions sur la distinction entre les deux.

Pour établir une distinction entre les deux, les biologistes ont voulu

c'est pourquoi le zoologiste allemand Karl Möbius a utilisé en
août 1884 le terme organula (pluriel du nom latin organulum, diminutif


