

Le terme viendrait du latin impérial simius, plutôt que du


Les adjectifs se rapportant au singe sont simien et simiesque.


Article détaillé : Liste alphabétique des noms vernaculaires de


Les « singes du Nouveau Monde » et les « singes de l'Ancien
Monde » sont regroupés par la classification phylogénétique dans


Le terme de « grand singe » désigne toutes les espèces faisant
partie des hominidés, c'est-à-dire les espèces actuelles de

orangs-outans et hommes, ainsi que les espèces apparentées ou


En français, les différentes sortes de singes sont désignées par des
noms plus ou moins précis comme babouin, chimpanzé, gibbon,


Contrairement aux oiseaux, il n'existe pas, en français,
d'organisme reconnu qui propose des noms uniques pour les espèces de
singe. De ce fait, de nombreux singes, particulièrement en Amérique du
Sud, possèdent plusieurs noms communs, au sens « nom de
vulgarisation scientifique » en français. Les noms peuvent être calqués
sur les noms scientifiques comme les Lagotriche ou sur les


En outre, du fait de la ressemblance morphologique entre espèces,
beaucoup de noms vernaculaires désignent de fait plusieurs espèces, la
progression des connaissances ayant permis ultérieurement de faire la
différence entre elles. De plus, l'usage des noms vernaculaires a varié
au cours du temps. Ainsi le terme chimpanzé, quand il a été adopté
en français, désignait indistinctement deux espèces, qui, après
qu'elles furent différenciées, ont été nommées dans un premier temps




La première description « scientifique » des singes qui nous soit
parvenue date du IV^e siècle av. J.-C. et revient au philosophe
grec Aristote. Dans son Histoire des animaux, il décrit le
quadrupèdes ». Il ajoute que le cèbe « est un singe qui a une queue »
et que les cynocéphales leur ressemblent par leur forme, « sauf qu'ils
sont plus grands, plus forts et que leur face ressemble plutôt à un



l'Ancien, les singes sont les animaux qui, « par leur conformation,
ressemblent le plus à l'homme ». Dans L’Histoire naturelle, il
cite aussi les callitriches qui ne vivent que « sous le ciel
d’Éthiopie », les cynocéphales et les satyres^.
Ces derniers sont assimilés à des singes qui vivent « dans les
montagnes indiennes situées au levant équinoxial », dans un pays « dit
des Catharcludes ». Ils « courent tant à quatre pattes que sur leurs


Un siècle plus tard, le médecin grec Galien contourne
l'interdiction par le droit romain de disséquer des cadavres
humains en pratiquant la vivisection de différents animaux, dont
des singes magots. Il constate en effet que « de tous les animaux
le singe ressemble le plus à l'homme pour les viscères, les muselés,



Détail de la tapisserie La Dame à la licorne, fin du


Cette section ne cite pas suffisamment ses sources (juillet

Pour l'améliorer, ajoutez des références vérifiables [comment
faire ?] ou le modèle {{Référence nécessaire}} sur les passages


Au Moyen Âge, les singes acquièrent un statut d'animal de

cercopithèques sont importés très tôt en Europe où ils égaient les
cours des princes et des évêques, parfois vêtus de riches habits. Le
motif du singe est souvent repris dans les enluminures, les
fresques et les sculptures, et symbolise la folie et la
vanité de l'Homme. Leur représentation iconographique figure
invariablement un collier et une chaîne, laquelle est parfois reliée à
un billot de bois pour limiter les mouvements de l'animal dans la



Les descriptions antiques et médiévales étaient plus imaginatives que
précises et se basaient sur les observations de singes présents surtout
sur les pourtours du bassin méditerranéen et rapportées par les


Dès le XIV^e siècle, le marchand vénitien Marco Polo
rapporte qu'il y a dans le royaume d'Atjeh (sur l'île de
Java) des hommes « qui ont une queue qui a bien une paume de long et






Au XVIII^e siècle, le comte de Buffon publie une œuvre
monumentale pour les sciences animales. Dans l'Histoire naturelle,
il établit une « nomenclature des singes » qui sépare les animaux de


Le premier groupe est divisé en trois « familles », toujours selon les
critères aristotéliciens de la longueur de la queue^. La
première regroupe les « singes », c'est-à-dire les animaux « sans
queue, dont la face est aplatie, dont les dents, les mains, les doigts
et les ongles ressemblent à ceux de l'homme et qui, comme lui, marchent
debout sur leurs deux pieds », et inclut l'Orang-outan, le
Pithèque et le Gibbon. Les membres de la seconde famille sont
appelés « babouins » et se caractérisent notamment par leur queue
courte, leur face allongée et leur museau large et relevé. Buffon y

Mandrill et l'Ouandérou et le Lowando (qu'il suspecte
d'appartenir à la même espèce). La dernière famille est celle des
Elles comptent neuf espèces : le Macaque (et l'Aigrette), le




Les Singes du Nouveau Monde se séparent quant à eux entre


Saïmiri. Les seconds l'ont « entièrement velue, lâche et droite »
et comprennent le Saki, le Tamarin, le Ouistiti, le



Article détaillé : Liste des primates décrits par Carl von Linné.

Parallèlement aux travaux de Buffon, le naturaliste suédois
Carl von Linné jette les bases de la nomenclature binominale.
Dans la dixième édition de son Systema Naturae^, il
introduit le genre Simia qui regroupe tous les singes ainsi
qu'une espèce de tarsier. Il en fait l'un des quatre taxons de
l'ordre des Primates, aux côtés des genres Homo (l'espèce




Contrairement à Buffon, Linné ne fait pas de différences entre les
espèces de l'Ancien et du Nouveau Monde. La division interne
du genre Simia est en effet basée sur l'antique distinction entre les
animaux sans queue (les « singes des Anciens », Simiae veterum), ceux à
queue courte (les « babouins », Papiones) et ceux à longue queue (les

En 1777, le naturaliste allemand Johann Christian Erxleben reprend
cette première classification mais y inclut les travaux de Buffon en
séparant les singes du Nouveau Monde. Il réduit le genre
Simia aux seuls singes sans queue et crée quatre nouveaux genres
pour le reste : Papio pour les babouins, Cercopithecus pour
les guenons, Cebus pour les sapajous et Callithrix pour les
sagouins^. Il place en revanche l'unique espèce de
tarsiers décrite par Buffon dans le genre Lemur (Lemur
tarsier). On notera qu'afin de rester en accord avec les récits
antiques, Erxleben construit les noms des deux genres américains à
partir de racines greco-latines : Cebus évoque les « cèbes » d'Aristote
alors que Callithrix fait écho au « callitriche » de Pline.

La liste des genres s'allonge durant les décennies qui suivent,
grâce notamment aux travaux du comte de Lacépède à Paris




En 1812, Étienne Geoffroy Saint-Hilaire entérine la division
entre Ancien et Nouveau Monde en proposant la séparation
entre les Catarrhini (« singes de l'Ancien continent ») et les
Platyrrhini (« singes d'Amérique »). Il crée également plusieurs

Cercocebus (les mangabeys), Nasalis (les nasiques)


Parallèlement, le concept de famille s'impose peu à peu en
zoologie et c'est John Edward Gray, du British Museum de
Londres, qui propose le premier une division des mammifères selon
ce principe. Il sépare ainsi l'ordre des Primates en Hominidae,
Sariguidae, Lemuridae (prosimiens), Galeopithecidae
classées dans un groupe nommé « anthropomorphes » et correspondent


macaques) et Cynocephalina (babouins). Les « Sariguidae » se
divisent quant à eux en Mycetina (hurleurs), Atelina

C'est ainsi que dès les années 1830, la classification
scientifique des singes atteint, dans ses grandes lignes, l'ordre qui
prévaut encore au XXI^e siècle et recense les principaux groupes
d'espèces connus aujourd'hui. La principale exception à
ce constat est la place réservée à l'espèce humaine, qui y est
systématiquement rangée dans un groupe bien à part, les savants de
l'époque rechignant à faire tomber l'ancestrale barrière entre


Caricature de Faustin Betbeder représentant Charles Darwin et


Le premier scientifique à avoir soutenu que les autres primates
pouvaient être apparentés aux hommes est Giulio Cesare
Vanini^, avant Charles Darwin, dans les années 1600.
L'affirmation du fait que l'homme est un singe est aujourd'hui banale,
certains titres comme « L'homme est un singe comme les autres »





Les singes forment un infra-ordre (Simiiformes) qui
appartient, avec le groupe frère des Tarsiiformes, au
sous-ordre des Haplorrhini. Ces primates se distinguent en
effet des Lorisiformes et des Lemuriformes (sous-ordre




Cette section ne cite pas suffisamment ses sources (juillet

Pour l'améliorer, ajoutez des références vérifiables [comment
faire ?] ou le modèle {{Référence nécessaire}} sur les passages

En Afrique sub-saharienne (ici au Burkina Faso), les artistes
s'inspirent également du singe pour sculpter leurs masques
Statue égyptienne de babouin de l'époque ptolémaïque.

Dans les mythologies et les cosmogonies, le singe occupe une place
toute particulière et nombre de ses aspects symboliques se retrouvent


Dans la Roue de l'existence tibétaine, il symbolise la Conscience
versatile, celle qui, liée au monde sensible, se disperse d'un objet à
l'autre. Réputé être l'ancêtre des Tibétains, qui le considèrent comme
un Bodhisattva, il est, selon Si Yeou Ki, le fils du Ciel et
de la Terre. Il accompagne donc Xuanzang (Hiun-Tsang) dans son
voyage à la recherche des Livres saints du Bouddhisme. Il y apparaît
comme le compagnon facétieux, magicien taoïste de grande envergure. Le
Roi-Singe, dans l'art extrême oriental, évoque la sagesse, le
détachement. C'est pourquoi les célèbres Singes du Jingoro, au
temple de Nikko, sont représentés l'un se bouchant les oreilles,
le second se cachant les yeux, le troisième se fermant la bouche. Une
interprétation occulte plus ancienne tend à voir dans les trois sages
de Jingoro la représentation d'un Singe créateur de toutes choses ici
bas, conscient de l'illusion et de l'impermanence de la réalité.

Cette croyance se retrouve dans le panthéon égyptien, où le singe est
le scribe savant, celui qui possède la connaissance de la réalité. Il
note la parole de Ptah, le dieu créateur, comme celle
d'Anubis, qui pèse l'âme des morts. Il apparaît en Égypte
comme le magicien suprême, artiste, ami des fleurs, des jardins, des
fêtes, prestidigitateur puissant capable de lire les plus mystérieux
hiéroglyphes. Il est donc l'animal psychopompe par excellence,
reliant la Terre et le Ciel. Il y est représenté comme celui qui
gouverne les heures, le maître du temps privilégié. Lors du voyage des
morts de vie en vie, Champollion mentionne un singe vert
accompagnant le Dieu Pooh, dans une portion de l'espace située entre la
Terre et la Lune, lieu du séjour des âmes. Pooh y est représenté
grand initié qui doit être évité dans l'autre monde où il pêche les

Chez les Fali du Nord Cameroun, le singe noir est un avatar du
forgeron voleur de feu, devenant ainsi, par extension, le magicien et
maître de la technique. Indéniablement, le Singe est un initié.

Chez les indiens Bororo, Claude Levi-Strauss rapporte qu'il
est le héros civilisateur, l'inventeur de la technique, le malin
magicien qui masque ses pouvoirs et son intelligence rusée. Il convient
de ne pas rire de lui car le Singe aura le dessus.

Un singulier singe vert apparaît dans de nombreux contes traditionnels
africains, du Sénégal jusqu'en Afrique du Sud, et revêt les
caractéristiques symboliques du magicien rusé : celui qui vit en
lisière des forêts et connaît les secrets de la création du monde.

Dans la mythologie hindoue, l'épopée de Ramayana fait du singe le
sauveur de Dieu au moment du passage du « grand pont ». Rêver d'un
singe est un appel en faveur d'un développement de la personne lié au
mystère de la création à la puissance de la Nature.



Les géoglyphes de Nazca au Pérou représentent plusieurs



Deux singes sont particulièrement importants dans la littérature



dans Le Voyage en Occident (Si Yeou Ki) et a pu être influencé



Pour les Égyptiens de l'antiquité, c'était l'un des douze animaux
sacrés associés aux douze heures du jour et de la nuit en plus de l'un



Le singe est un des 12 animaux illustrant les cycles du
zodiaque chinois lié au calendrier chinois. On associe chacun
des animaux de ce zodiaque à certains traits de personnalité.





L'art martial du singe considère l'animal comme incarnant les
qualités suivantes : adresse, agilité, ruse, souplesse. Ses techniques
sont imprévisibles. Ses parades sont acrobatiques. Ses frappes sont
très courtes et très rapides, dans les points vitaux. Les grimaces du
singe y sont imitées. Aussi, il est utilisé pour stimuler le cœur, en







Certains singes du genre Cebus (Sapajous) sont dressés pour
pouvoir aider au quotidien les personnes diminuées dans leur capacité



Cette section est vide, insuffisamment détaillée ou incomplète.


