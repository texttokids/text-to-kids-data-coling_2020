
Ce discours est considéré comme l'un des meilleurs de Kennedy, mais
aussi comme un moment fort de la guerre froide. Il avait pour but
de montrer le soutien des États-Unis aux habitants de
l'Allemagne de l'Ouest, et notamment aux Berlinois de l'Ouest qui
vivaient dans une enclave en Allemagne de l'Est — au milieu de
territoires communistes, alors délimités depuis presque deux ans
par le mur de Berlin — et craignaient une possible invasion de la


Depuis le balcon de l'hôtel de ville de Schöneberg, qui était alors
le siège de la municipalité de Berlin-Ouest, située en secteur


civis romanus sum (« je suis citoyen romain »). Aujourd'hui,
dans le monde libre, la plus grande marque d’orgueil est de dire Ich
bin ein Berliner.  Tous les hommes libres, où qu'ils vivent,
sont des citoyens de Berlin. Par conséquent, en tant qu'homme libre,
je suis fier de prononcer ces mots : Ich bin ein Berliner! »


président français Charles de Gaulle avait prononcé, lors d'une
tournée solennelle en Allemagne, une allocution en langue
allemande, ce qui a probablement inspiré le président


Après l'attentat du 19 décembre 2016 à Berlin, la phrase s'est
popularisée sur Internet en inspirant les modèles « Je suis





Voici un extrait du discours prononcé par le président J. F. Kennedy le


qui prétendent ne pas comprendre quelle est la grande différence



Il y en a qui disent qu'en Europe et ailleurs, nous pouvons
travailler avec les communistes. Qu'ils viennent à Berlin ! Lass sie


Notre liberté éprouve certes beaucoup de difficultés et notre
démocratie n'est pas parfaite. Cependant, nous n'avons jamais eu
besoin, nous, d'ériger un mur  pour empêcher notre peuple de
s'enfuir.  Le mur fournit la démonstration éclatante de la
faillite du système communiste. Cette faillite est visible aux yeux
du monde entier. Nous n'éprouvons aucune satisfaction en voyant ce
mur, car il constitue à nos yeux une offense non seulement à
l'histoire mais encore une offense à l'humanité. 

Tous les hommes libres, où qu'ils vivent, sont des citoyens de
Berlin. Par conséquent, en tant qu'homme libre, je suis fier de



Selon une légende urbaine apparue au cours des années 1980 et
fréquemment reprise par les media francophones^, Kennedy aurait
commis une faute grammaticale qui aurait changé le sens de sa phrase.
Selon cette interprétation erronée, la phrase correcte aurait dû être
suis un Berliner », autrement dit une boule de Berlin, c'est-à-dire
un beignet. Cette interprétation a été reprise à l'époque par de
nombreux médias anglophones de bonne réputation (The New York
Times, BBC, The Guardian, etc.). En réalité, les deux formes
sont parfaitement correctes^^,^, et même si la phrase
part de Kennedy qui s'exprimait au sens figuré : en effet, « Ich bin
Berliner » aurait donné l'impression qu'il se présentait comme étant
véritablement originaire de Berlin^, ce qui n'est pas le cas.




2. ↑ http://fr.wikisource.org/wiki/en:Ich_bin_ein_Berliner.
3. ↑ Le discours tranche avec l'attitude peu engagée et assez
tiède des États-Unis au début de la crise berlinoise.
4. ↑ « Discours devant la jeunesse allemande - Ludwigsburg -

5. ↑ « Voyage en Allemagne », Aude Vassallo, Institut

6. ↑ ^a et b Vincent Mongaillard, « « Ich bien ein
Berliner » en soutien aux Allemands », sur Le Parisien, 19


http://www.leparisien.fr/faits-divers/ich-bin-ein-berliner-en-


https://www.lesechos.fr/tech-medias/medias/0211611927117-ich-b
in-ein-berliner-les-internautes-rendent-hommages-aux-victimes-de-be


http://www.vanityfair.fr/actualites/international/articles/ich

http://www.lalibre.be/debats/edito/edito-ich-bin-ein-berliner-


http://www.lefigaro.fr/international/2016/12/20/01003-20161220
ARTFIG00066-attentat-a-berlin-les-messages-de-solidarite-se-multipl


https://www.huffingtonpost.fr/2016/12/20/ich-bin-ein-berliner-

13. ↑ Le magazine français Le Point, dans le numéro
d'octobre-novembre 2009 de sa collection « Grand Angle », intitulé
Mur de Berlin : histoires, secrets, héros, reprend à son compte
l'interprétation. Il taxe même (p. 47) la phrase de « faute
grammaticale de taille » (sic) ; en 2019 le journaliste Lucas
Menget continue à présenter la pseudo-anecdote comme véridique dans

14. ↑ (en) David Emery, « The Truth Behind the JFK "Jelly
Doughnut" Myth », sur liveaboutdotcom, 16 juillet 2018.
15. ↑ ^a et b (en) Steve Coates, « Ich bin What? », sur


