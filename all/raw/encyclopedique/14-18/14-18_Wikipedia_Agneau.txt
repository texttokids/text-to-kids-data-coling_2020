
L'agneau est principalement élevé pour sa viande, mais également pour



Les premières traces d’élevage d’agneaux sont attestées au
Moyen-Orient et remontent à plus de 12 000 ans^. En
1667, Jean Baptiste Denis, un médecin français très réputé à
l’époque, médecin personnel de Louis XIV, est le premier à injecter
le sang d’un animal à un homme. Il injecte le sang d’un jeune agneau à
un homme atteint de typhus (gastro-entérite mortelle à cette
transfusion sanguine. La même année, Denis et son confrère Emmeretz
effectuaient la première transfusion d’homme à homme en reliant
l’artère d’un des sujets à la veine de l’autre.


Dans le cas des races bouchères, l'agneau doit avoir une bonne
conformation. On distingue l'agneau de lait (non sevré) de l'agneau
blanc (25 kilos) et de l'agneau broutard (35 kilos)^.

L'agneau de pré-salé est un agneau broutard qu'on laisse paître en
semi-liberté dans les prés-salés de la baie de Somme et la
baie du Mont-Saint-Michel, et dont la viande est la seule en Europe

Plusieurs régions bénéficient également du label IGP^. On
peut citer l'agneau de l’Aveyron, l'agneau de Lozère, de Pauillac, de
Sisteron, du Bourbonnais, du Limousin, du Périgord, du
Poitou-Charentes, ou encore l'agneau du Quercy et l'agneau des



L'agneau désigne également la viande très tendre de l’agneau. On
distingue l'agneau de lait ou agnelet non sevré (10 kg) ; le laiton,
appelé aussi agneau blanc (25 kg) en raison de la pâleur de la viande ;
le broutard (35 kg), qui est un agneau à chair rosée, qui a connu les
pâturages ; et enfin l'agneau de pré-salé, ou estran, qui est un
broutard dont la viande a une saveur particulière et une chair plus





Article détaillé : Viande d'agneau de prés salés.


En France, les principaux cheptels se situent au sud de la
Loire. Bien que troisième producteur européen de viande d'agneau
derrière l'Espagne et le Royaume-Uni, le pays n'est pas auto-suffisant
pour sa consommation. Les principaux pays européens exportateurs en


Au niveau Européen et mondial, il faut en effet distinguer les pays
producteurs des pays exportateurs. Ainsi, au niveau mondial, si les
principaux producteurs sont, dans l'ordre : la Chine, l'Australie, la
Nouvelle-Zélande et le Royaume-Uni ; les principaux exportateurs sont
l'Uruguay, la Nouvelle-Zélande, l'Irlande et l'Australie^.


Autrefois surtout utilisé en tant que parchemin (usage très répandu
au Moyen Âge), la peau de l'agneau sert encore aujourd'hui à la
fabrication et à la conception de nombreux produits et articles de

karakul qui transitait à l'origine par la ville d'Astrakhan
en Russie. Des défenseurs des droits des animaux affirment que la
demande de ce produit conduit aujourd'hui les éleveurs à tuer les



Le feutre des chapeliers est fait notamment avec du poil




L’agneau de Dieu sur une Bible du XVIII^e siècle

L'agneau est représenté depuis la nuit des temps comme un symbole quasi



L'agneau est souvent l'objet de sacrifices aux Dieux.


Des agneaux noirs étaient sacrifiés aux divinités grecques des



L'agneau, qualifié d'animal pur (cacher) peut être mangé dans la
religion juive. On retrouve dans l'Ancien Testament des références
avec des agneaux sans taches lors des grandes fêtes comme Pessa'h.


Dans l'iconographie chrétienne, l'agneau renvoie l'image de
l'Homme dont le prophète est le berger, mais peut également
désigner Jésus, qui est alors l'Agneau de Dieu, celui dont le
sacrifice enlève le péché du monde^. Dans les représentations
d'Adam et Ève après la chute, la présence d'un agneau rappelle que
le péché originel a été racheté par Jésus Christ, l'Agneau de Dieu. Les
illustrations du sacrifice d'Abel le montrent parfois portant un agneau
sur ses épaules. Des agneaux figurent dans les représentations de

C'est également l'attribut de Jean-Baptiste et de sainte


C'est ainsi que l'on retrouve régulièrement l'agneau dans les
bestiaires médiévaux, ainsi que dans les chants religieux
fête de Pâques, au cours de laquelle est traditionnellement servi

Louis IX en fera une monnaie d’or (métal pur par excellence),
l'Agnel ou agnel d'or ou mouton d’or, frappée au



En Islam, l'agneau est traditionnellement sacrifié lors des fêtes de
l'Aïd, en commémoration du sacrifice d'Abraham (Aïd al-Adha),
ou pour fêter la fin du mois de jeûne, le Ramadan (Aïd
el-Fitr). Il est en revanche prohibé de tuer un animal qui tète encore
sa mère. L'agneau au sens strict de jeune mouton non sevré ne peut donc



On retrouve trace de l'agneau en héraldique dans le blason de

Lannion...). Il est aussi l'emblème des corporations de drapiers.
Article connexe : Représentation des animaux dans l'art médiéval.


Dans les contes et la littérature, l'agneau garde souvent une valeur de
victime innocente : victime du loup dans les fables d'Ésope (dit
Isopet au Moyen Âge), que Jean de La Fontaine reprendra dans
le Loup et l'Agneau ; Thibault Agnelet, le berger de maître
Guillaume dans la Farce de Maître Pathelin, est tout naturellement
le naïf de la farce, mais un faux naïf qui réussira à tromper son
patron le drapier, son avocat Pathelin, et un juge. Une légende
savoyarde autour du pic de la Dent du Chat montre un agneau
servant d'appât pour traquer un chat sauvage qui terrorise les


Le titre du roman américain le Silence des agneaux, dont est tiré
le film du même nom réalisé par Jonathan Demme (1991), évoque


Dans la culture enfantine, l'agneau fait partie des personnages
inoffensifs mais vulnérables, ainsi Toudoux (Gentle Heart Lamb, en






pâtes farcies en forme de petites poches semi-circulaires.



