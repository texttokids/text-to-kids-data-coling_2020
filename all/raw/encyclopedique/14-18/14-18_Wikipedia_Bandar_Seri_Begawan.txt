
Le nom d’origine de la ville avant le 5 octobre 1970, date
de l’actuel sultan, était Bandar Brunei. Le premier terme, Bandar
dans la région via les langues indiennes, pour désigner une
sanskrit : le premier est l’équivalent de terme Sri, titre de
vénération donné aux dieux hindous, qui avait à la base une
signification d'émanence, de radiance, de diffusion lumineuse ; le
second procède du terme bhagavān (भगवान) signifiant « dieu ». Sri
Bhagwan peut donc être traduit par « le béni », ainsi « Bandar Seri



bâtie sur l'eau. Aujourd'hui les quartiers modernes sont construits sur
la terre ferme et la ville est devenue une mosaïque bigarrée où les
maisons sur pilotis côtoient autoroutes et buildings en béton. Au-delà
du fleuve Brunei, vers le sud, s'étend la cité lacustre de
Kampong Ayer, qui empiète d'environ 500 mètres sur la mer.


Bandar est habité depuis le VII^e siècle, sur l'île de Kampong
Ayer, cœur historique actuel de la capitale. Au XVI^e siècle, alors que
le Brunei occupe une place importante sur l'île de Bornéo, la


En 1906, la ville s'établit autour de Kampong Ayer, sur les conseils du
premier résident anglais au Brunei, McArthur. La ville devient
officiellement la capitale du pays en 1920 et le gouvernement de


Brunei passe sous possession japonaise pendant la seconde guerre
mondiale, une grande partie de la ville a été détruite par les
bombardements alliés. La prospérité de l'État permet une reconstruction


En 1956, la superficie officielle est de 12,87 km^2, ce qui correspond

La ville, qui s'appelait Bandar Brunei ou Brunei Town, est rebaptisée
en 1970 Bandar Seri Begawan en l'honneur du père du sultan, Sir


En 2007, le sultan Hassanal Bolkiah réorganise les limites de la
ville faisant ainsi passer sa superficie à 100,36 km^2.


La ville comporte des industries textile, de fabrication de meubles et
d'exploitation du bois. Le principal marché de la ville est celui de
Tamu Kianggeh situé sur le fleuve homonyme dans le centre-ville. On y
trouve essentiellement des étals d’artisanat ainsi que des marchands de
crevettes, de fleurs, de fruits et de légumes. Le complexe Yayasan est
le plus grand centre commercial du pays. Il est situé juste à côté de



D'après le dernier recensement officiel d'août 2001, la ville était
peuplée de 27 285 habitants. À la suite de l'extension des limites de
la ville jusqu'à l'aéroport en 2007, un certain nombre de villages ont
l'ouest, Madewa au sud, Subok, à l'est ou encore Manggis au nord. La
ville de Bandar Seri Begawan compte désormais 140 000 habitants, tandis
que son agglomération comprend environ 200 000 habitants^.

La ville de Bandar Seri Begawan s'étend sur 12 mukims (districts) :
Kianggeh, Gadong B, Kilanas, Berakas A, Berakas B, Kota Batu, Sungai
Kedayan, Tamoi, Burong Pingai Ayer, Peramu, Saba et Sungai Kebun. Les
mukims de Sungai Kedayan, Tamoi, Burong Pingai Ayer, Peramu, Saba et
Sungai Kebun correspondent à la cité historique de Kampong Ayer
ainsi qu'aux rives du fleuve Brunei et Kedayan. Kampong Ayer est peuplé




un dôme géant doré. Grandiose, elle est bâtie avec les plus beaux
et les plus coûteux matériaux du monde : les marbres viennent
d'Italie, les granits roses de Shanghai et les vitraux de
Londres. Jusqu'en 1995, elle était la seule mosquée au monde à
présente des expositions sur l'architecture des villages sur



Bandar Seri Begawan possède un aéroport international (code
AITA : BWN). L'aéroport est desservi par l'autoroute Lebuhraya
Sultan Hassanal Bolkiah, d'une longueur de 11 km, qui relie le


