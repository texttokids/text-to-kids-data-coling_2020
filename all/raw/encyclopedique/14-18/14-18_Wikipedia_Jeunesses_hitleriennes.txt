
La première organisation des jeunes du Parti nazi est fondée le 13
mai 1922 à Munich sous le nom de Jungsturm Adolf Hitler, plus
ou moins traduisible en français par « Jeune brigade d'assaut Adolf
Hitler ». Elle est alors rattachée au Sturmabteilung et dirigée par
Gustav Adolf Lenk. L'organisation est interdite en 1923, en même


Elle est refondée le 4 juillet 1926 lors du deuxième
congrès du Parti nazi, sous le nom de Hitlerjugend.


La raison d'être des Jeunesses hitlériennes est la formation de futurs
surhommes « aryens » et de soldats prêts à servir loyalement le
Troisième Reich. Il s'agit de contourner les clauses très
contraignantes du Traité de Versailles qui interdisent à
l'Allemagne de posséder une armée puissante et de préparer une
génération physiquement et mentalement apte à être, au plus tôt, lancée
dans une guerre contre toutes les puissances ennemies du Reich. Dans
les Jeunesses hitlériennes, l'entraînement physique et militaire
passait bien avant l'instruction scolaire et scientifique.
L'apprentissage comprenait le maniement des armes, le développement de
la force physique, la stratégie militaire et un endoctrinement
antisémite. Après la dissolution des organisations de scouts dans
tous les Länder d'Allemagne, les Jeunesses hitlériennes s'approprièrent
beaucoup de leurs activités, bien que les objectifs et le contenu ne
soient pas les mêmes. La cruauté des plus grands envers les plus jeunes
encourageait à éliminer les plus faibles et à s'endurcir.
Uniforme des Jeunesses hitlériennes dans les années 1930.

Les membres des Jeunesses hitlériennes portaient des uniformes
comparables à ceux du Parti nazi et utilisaient un système de
grades militaires similaires aux grades et insignes des
Sturmabteilung. Beaucoup des activités proposées aux garçons
ressemblaient à un entraînement militaire : ramper sous des fils
barbelés, apprendre à plonger en mer depuis des sautoirs et apprendre
comment lancer des grenades factices. Ils avaient un poignard fabriqué
par la firme Zwilling J. A. Henckels, sur le plat de la lame duquel
figurait l'inscription « Blut und Ehre ! », (sang et honneur). Le


Fixés par les directives de Von Schirach, les programmes à
inculquer à la jeunesse étaient précis, se voulant l'armature
idéologique de la nouvelle génération, et comportaient un certain
nombre de thèmes centrés autour du parti nazi, de Hitler, de
l'Allemagne et du peuple allemand, et des directives pour les chants à



Garçons de la jeunesse hitlérienne s'exerçant au tir.


L'encadrement des Jeunesses hitlériennes était assuré par des adultes,
souvent militants du parti nazi au sein d'un corps d'armée. De plus,
ces chefs d'escouades, à la pédagogie de sergent-instructeur^
exigent, au Führerprinzip une obéissance totale^. Le gros
des membres comprenait des garçons âgés de huit à dix-huit ans. Dès
1936, les Jeunesses hitlériennes devinrent officiellement une
filière obligatoire pour tous les jeunes allemands. Le groupe servait
aussi de base de recrutement pour des groupes paramilitaires du parti
nazi : la Schutzstaffel (SS) s'y intéressait particulièrement. Les
membres des HJ étaient particulièrement fiers de se voir accorder la
sieg rune (rune de la victoire), par les SS. Les SS utilisaient
deux sieg runes accolées comme emblème, et cette récompense liait


Elles étaient organisées dans les villes et villages en cellules
locales. Ces groupes se réunissaient chaque semaine : un dirigeant
adulte y enseignait la doctrine nazie. Au niveau régional, les
responsables organisaient des rassemblements et des manœuvres auxquels
plusieurs cellules participaient. Le groupe national se réunissait en
général une fois par an à Nuremberg, pour le rassemblement


Les Jeunesses hitlériennes avaient également créé des « académies »
d'entraînement comparables aux lycées. De telles académies étaient
considérées comme les bases de la relève du parti : seuls les élèves
les plus dévoués et les plus radicaux pouvaient prétendre devenir de


Quelques sections visaient à entraîner ceux de ses membres à devenir
officier de la Wehrmacht. De tels groupes s'appliquaient à former
le jeune disciple dans la spécialité qu'il espérait exercer en tant
qu'officier. Ainsi, les Jeunesses hitlériennes de la Marine étaient
la section la plus nombreuse et servaient d'auxiliaires à la



Dès juillet 1933, Schirach organisa les Jeunesses hitlériennes au moyen


DJ (Jeunes allemands), regroupaient les garçons de dix à quatorze

chargé de l'encadrement des filles de dix à quatorze ans ;

quatorze à dix-huit ans ; plus tard l'âge maximal des membres du

BDM-Werk « Glaube und Schönheit » rassembla les jeunes filles


Les quatre principales branches des HJ (Deutsches Jungvolk,
Hitlerjugend, Jungmädel, Bund Deutscher Mädel), quoique comportant
quelques variations minimes, étaient structurées de manière similaire.
La direction de la Jeunesse du Reich (Reichsjugendführung (en))
avait organisé sur le territoire du Reich des directions régionales,
représentant entre 20 et 42 Gebiete (pour les DJ ou les HJ), et autant
d'Obergaue pour les JM et le BDM. Ces structures étaient elles-mêmes
redécoupées de manière plus fine, jusqu'à des groupes d'une dizaine de
membres, par lieu de résidence, désignés sous les termes de
Jungenschaft (DJ), Kameradschaft (HJ véritable), Jungmädelschaft (JM)

Deutsches Jungvolk Hitlerjugend     Jungmädel    Bund Deutscher Mädel


Jungstamm          Stamm      Jungmädelring       Mädelring
Fähnlein      Gefolgschaft  Jungmädelgruppe     Mädelgruppe
Jungzug           Schar     Jungmädelschar       Mädelschar
Jungenschaft    Kameradschaft Jungmädelschaft     Mädelschaft

Pour les niveaux Jungstamm/Stamm/… et inférieurs, chaque unité
coordonnait les membres de quatre unités du niveau hiérarchique
inférieur. Au niveau Bann/Untergau, l'organisation se faisait de
manière géographique, chacune de ces unités présidant à quatre ou six
unités du niveau inférieur, tandis que chaque Gebiet ou Obergau
regroupait environ 20 Bannen ou Untergauen. Une unité du niveau
Jungstamm/Stamm/… regroupait en moyenne environ 600 membres, un Bann ou


En 1934, le Reich était ainsi organisé en cinq Obergebiete et 19
Gebiete ou Obergaue. L'Anschluss de 1938 provoqua la création d'un
sixième Obergebiet^. En 1942, le nombre de Gebiete et d'Obergaue
culmina à quarante-deux, dont quatre Befehlsstellen pour les pays
occupés (Protectorat de Bohême-Moravie, Gouvernement général,
Pays-Bas, ainsi qu'Osten pour l'Europe de l'Est et du Sud). Pour le
territoire du Reich, les limites entre Gebiete suivaient grosso modo







L'unité de base des Jeunesses hitlériennes était la Bann, l'équivalent
d'un régiment militaire. On comptait plus de 300 de ces Banne,
dispersées dans toute l'Allemagne, chacune d'entre elles comptant
environ 6 000 jeunes. Chaque unité avait un drapeau avec un dessin
pratiquement identique, mais chaque Bann était identifiée par son nom,
inscrit en noir sur un ruban jaune, au-dessus de la tête de l'aigle.
Ces drapeaux mesuraient 200 cm de long et 145 cm de haut. L'aigle au
centre faisait référence à l'Empire allemand (aigle prussien). Il
maintenait dans ses serres une épée blanche et un marteau noir. Ces
symboles furent utilisés sur le premier drapeau officiel présenté aux
Jeunesses hitlériennes, au Congrès national du NSDAP, en août 1929, à
Nuremberg. L'épée était censée représenter le nationalisme alors que le
marteau était le symbole du socialisme. Les mâts utilisés avec ces
drapeaux étaient en bambou, surmontés d'une boule en fer blanc et d'une


Les drapeaux portés par les Gefolgschaft des HJ, l'équivalent d'une
compagnie de 150 jeunes, montrait l'emblème utilisé par les groupes
armés des HJ : trois bandes horizontales (rouge-blanc-rouge) au centre
desquelles un carré blanc tenant sur un sommet et contenant une croix
gammée noire en son sein. Le drapeau des Gefolgschaften mesurait 180 cm
de long par 120 cm de hauteur avec chaque bande de 40 cm. Pour
distinguer chaque Gefolgschaft et la branche des Jeunesses hitlériennes
coloré, en haut à gauche. Le bandeau était d'une couleur précise,
propre à chaque unité. Par exemple, un bandeau bleu clair avec un
numéro d'unité en blanc et une couture blanche était réservé pour les



Examen sanitaire dans un camp d'été, juillet 1940. L'embrigadement vise



En 1923, l'organisation comptait un millier de membres. En 1925, le
nombre de membres s'élevait à 5 000. Cinq ans plus tard, les Jeunesses
hitlériennes dépassait les 25 000 sympathisants, et à l'arrivée des
nazis au pouvoir en 1933, elles comptaient un effectif de
2 250 000 membres. Cette augmentation étant due en grande partie aux
membres des autres organisations de jeunesse avec lesquelles les
Jeunesses hitlériennes avaient fusionné (avec plus ou moins de
consentement), incluant l'importante evangelische Jugend


En décembre 1936, l'effectif dépassa les cinq millions de membres.
Le même mois, l'organisation devint la seule organisation de jeunesse
autorisée dans laquelle toutes les autres devaient se fondre (Gesetz
1939 avec le Jugenddienstpflicht. L'appartenance pouvait même être
proclamée contre l'avis des parents. À partir de là, la plupart des
adolescents allemands furent incorporés dans les Jeunesses
hitlériennes : dès 1940, l'organisation avait atteint un effectif
de huit millions de membres. Plus tard, les statistiques de guerre sont
difficiles à lire, dès le moment où l'on considère que la conscription
obligatoire et l'appel à la lutte (chez des enfants à partir de 10 ans)
signifie que pratiquement tous les jeunes allemands étaient, dans une
certaine mesure, reliés aux Jeunesses hitlériennes.

Le gros de la « génération des Hitlerjugend » était né entre les
années 1920 et 1930. Ils formèrent la génération adulte de
l'après-guerre et des années 1970 et 1980. Il n'était donc
pas rare pour les anciens dirigeants de la République démocratique
allemande et de l'Allemagne de l'Ouest d'avoir eu un passé chez
les Jeunesses hitlériennes. Du fait que l'organisation était devenue
obligatoire dès 1936, il n'y eut pas de volonté de bannir les
politiques qui avaient servi dans les Jeunesses hitlériennes, à partir
du moment où l'on considérait qu'ils n'avaient pas eu le choix.

L'exemple le plus patent fut celui de Manfred Rommel, fils
d'Erwin Rommel, qui devint maire de Stuttgart en dépit du
fait qu'il a fait partie des Jeunesses hitlériennes. Mais aussi, le
ministre allemand des Affaires étrangères Hans-Dietrich Genscher,
le philosophe Jürgen Habermas, et le Prince consort des Pays-Bas
Claus von Amsberg. En outre, le 19 avril 2008, les
médias annoncèrent que le pape de l'Église catholique romaine
Benoît XVI (de son nom civil à la naissance Joseph Ratzinger)
avait servi contre son gré dans les Jeunesses hitlériennes à l'âge de
14 ans. Cette information suscita une polémique selon laquelle une
personne qui avait été liée d'une manière ou d'une autre au
nazisme ne devrait pas devenir pape. Cependant, les faits
révélèrent que Joseph Ratzinger ne partageait pas l'idéologie des nazis


Cependant, rapidement, le caractère subversif des Jeunesses
hitlériennes disparaît, et cette organisation devient impopulaire au
sein même des groupes qu'elle est censée encadrer. En effet, comme pour
le KdF, les membres des Jeunesses hitlériennes utilisent les
infrastructures pour la satisfaction de leurs besoins et désirs ; les
activités d'embrigadement, les veillées, le camping, pratiqué de
manière militaire, et la collecte de dons sont particulièrement



L'endoctrinement de la jeunesse, s'il se voulait totalitaire, rencontre
des réserves au sein de la société allemande. Tout d'abord auprès du
public que cette organisation est censée encadrer, puis au sein de la


Obligatoire à partir du décret Gesetz über die Hitlerjugend du

transforme en structure bureaucratique, ce qui détourne beaucoup de
jeunes de ses rangs^. De plus, le caractère militaire de
l'encadrement et des activités proposées jouent un rôle non négligeable
dans la désaffection des jeunes à l'égard de l'organisation : dans le
meilleur des cas, ils s'ennuient dans les veillées, ne participent pas


En outre, l'application du Führerprinzip finit par éloigner de
l'organisation un nombre de plus en plus croissant de jeunes :
obéissance inconditionnelle aux ordres, même lorsqu'ils semblent
absurdes, et châtiments sans appel semblent la règle et incitent de


Auprès de la population, les jeunesses hitlériennes jettent le trouble
au sein des familles : séparés de leur famille, les enfants sont
souvent utilisés comme informateurs par le NSDAP^. Au sein de
la société, lorsqu'ils sont en groupes, les membres sont souvent
grossiers et sans gêne à l'encontre des gens qu'ils peuvent
croiser^. De plus, indisciplinés et jouissant d'une
quasi-impunité de fait, les jeunes militants de la Hitlerjugend mènent
suscite de fortes réserves dans le corps enseignant^.




En 1940, Artur Axmann prend la tête des Jeunesses
hitlériennes pour transformer l'organisation en une force auxiliaire
utile dans un contexte de guerre. Les Jeunesses hitlériennes assistent
les pompiers et l'effort de reconstruction des villes lors des
bombardements alliés. Elles accomplissent des missions dans le

radiodiffusion et servent dans les équipes de défense anti-aérienne.

Vers 1943, les chefs nazis transforment les Jeunesses hitlériennes
en une réserve militaire où ils puisent des troupes à la suite des
pertes importantes et croissantes dues à la guerre. Ainsi la
12^e Panzerdivision SS Hitlerjugend sous le commandement de
Fritz Witt est entièrement composée de jeunes garçons entre seize
et dix-huit ans. Cette division est déployée pendant la bataille
de Normandie contre les forces canadiennes et britanniques au nord de
Caen. Pendant les mois qui suivent, la division obtient une
réputation de férocité et de fanatisme. Quand Fritz Witt est tué par
l'artillerie alliée, le SS-Brigadeführer Kurt Meyer en prend le
commandement et devient le plus jeune commandant de division à l'âge de


Lors de l'invasion de l'Allemagne par les Alliés, la
Wehrmacht recrute des membres des Jeunesses hitlériennes de plus
en plus jeunes. En 1945, la Volkssturm engage dans des
combats meurtriers et sans espoir des membres des Jeunesses


Pendant la bataille de Berlin, les Jeunesses hitlériennes
constituent une part importante des forces allemandes et se battent
avec fanatisme (Alfred Czech ayant même été le plus jeune
soldat décoré par Adolf Hitler). Le commandant de la ville, le
général Helmut Weidling ordonne à Artur Axmann de dissoudre
les unités combattantes des Jeunesses hitlériennes, cet ordre n'est
jamais appliqué à cause de la confusion de la bataille de Berlin.


dissous les Jeunesses hitlériennes comme partie intégrante du
Parti nazi. Des membres des Jeunesses hitlériennes furent accusés
de crime de guerre mais, dans la mesure où l'organisation était
constituée de mineurs, les efforts pour faire aboutir les poursuites


Bien que les Jeunesses hitlériennes ne fussent jamais déclarées
avait commis des crimes contre la paix en corrompant les jeunes
esprits allemands. De nombreux cadres de haut niveau furent jugés par
les Alliés, à l'instar de Baldur von Schirach condamné à vingt ans


