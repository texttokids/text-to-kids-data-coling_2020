
Les alvéoles se trouvent au milieu du corps, là où se trouve l'appareil
respiratoire des poumons. Les poumons sont précédés de la
trachée et sont constitués des bronches (primaires, secondaires
et tertiaires) et du tissu pulmonaire. On peut ajouter à cela les
alvéoles et tous les composants en relation directe avec celles-ci, à
savoir les veines et artères pulmonaires qui longent le conduit


Il existe des voies de communication entre les alvéoles, acinus,
lobules et même segments pulmonaires. On en retrouve trois sortes :





Les alvéoles sont des sacs aériens poreux très petits, dont la paroi


Ils ont une tendance innée à s'effondrer du fait de leur forme en bulle
et de leur grande courbure. Les phospholipides, qu'on appelle
surfactants car tensioactifs, et les pores aident à


Les parois alvéolaires contiennent des capillaires et un espace
interstitiel très petit. Dans certaines parois alvéolaires il y a des
pores entre les alvéoles. Il y a deux types majeurs de cellules

pneumocytes de type I formant la structure de la paroi alvéolaire,

type II qui sécrètent un surfactant qui abaisse la tension de
surface et se divisent pour produire les pneumocytes de type I.

L'intérieur de l'alvéole pulmonaire chez les humains est composé d'air
alvéolaire, ses parois sont imprégnées d'eau et d'air.


Les alvéoles sont petits avec des parois très minces. Ils ont un rayon
de 0,1 mm et une épaisseur de paroi d'environ 0,3- µm.

Les échanges gazeux pulmonaires sont motivés par la diffusion
osmotique et ne nécessitent pas d'ATP ou adénosine
triphosphate, alimenté à base d'enzymes de transport. Substances qui
passent d'une concentration élevée à une plus faible concentration.
Dans les alvéoles, cela signifie que l'oxygène dans les globules rouges
aura une plus faible concentration que dans l'air. Inversement, le
dioxyde de carbone aura une plus forte concentration dans les
globules rouges que dans l'air. Cela entraîne la diffusion de
l'oxygène dans le sang, oxygène qui se lie aux molécules des
protéines de l'hémoglobine. Ainsi, le dioxyde de carbone peut
dioxyde de carbone et l'oxygène soient les principales molécules
ce processus est que les molécules avec une haute affinité vis-à-vis de
l'hémoglobine, tels que le monoxyde de carbone, aient tendance à se
lier aux globules rouges. Le monoxyde de carbone se diffuse facilement
dans les alvéoles des poumons et dans les cellules sanguines. Cela
signifie que, si la concentration de monoxyde de carbone est assez

Les poumons contiennent environ 300 millions d'alvéoles, chacun
enveloppé dans une fine maille de capillaires. Les poumons sont
constamment exposés à l'air et à leurs agents infectieux ainsi que
des particules de poussière. L'organisme emploie de nombreux moyens de
défense pour protéger les poumons, y compris les petits poils
soutenir un flux constant de mucus hors des poumons, et qui
déclenchent des réflexes tels la toux et les éternuements
pour déloger le mucus contaminé avec des particules de poussière ou des
micro-organismes. Il existe aussi d'autres systèmes de défenses liés au
système immunitaire humain, car les poussières et autres particules ne
sont après tout que des antigènes. (Pour plus d'information. sur les
méthodes de fonctionnement de ce système, se reporter aux articles liés

La surface totale des alvéoles les uns à côté des autres est de : 2 x
300 000 000 (nombre total d'alvéoles dans les deux poumons) x
0,125 mm^2 (surface totale entre l'air et le sang d'un alvéole) =





sont de 105 mmHg et de 40 mmHg. À l'air normal, les pressions

160 mmHg et 0,10 mmHg. La pression alvéolaire de dioxygène est plus
faible parce que ce dernier entre dans les capillaires pulmonaires.
Le dioxyde de carbone alvéolaire a une pression plus élevée parce que
le dioxyde de carbone sort des capillaires pulmonaires.

Les facteurs qui déterminent les valeurs de pression alvéolaire en




L'hypoventilation existe lorsque le taux de dioxyde de carbone pour la
production de ventilation alvéolaire augmente. Il y a





Le sang qui entre dans le capillaire pulmonaire est le sang veineux
systémique qui entre dans les poumons par l'intermédiaire de


En raison des différences de pressions partielles à travers la membrane


Ainsi, le sang qui retourne au cœur a le même taux de dioxygène que
l'air alvéolaire. Plus il y a de capillaires pulmonaires qui




Pour être plus efficace, la proportion correcte de ventilation
alvéolaire et de perfusion capillaire doit être disponible à chaque


La réponse homéostatique dans les poumons minimise les erreurs de



maladie inflammatoire des poumons. Habituellement déclenché par
d'autres pathologies pulmonaires, l'inflammation incontrôlée
conduit à une diminution des échanges de gaz, inondations
alvéolaires et / ou effondrement, ainsi qu'une réponse du syndrome
systémique inflammatoire. Il exige habituellement une
ventilation mécanique dans une unité de soins intensifs.
Infant respiratory distress syndrome ou MMH pour Maladie des
membranes hyalines) est un syndrome causé par le manque d'agents
d'évacuation et de nettoyage dans les poumons des enfants

qui causent une augmentation du débit d'air dans les poumons, est
considérablement réduit. Elle peut être déclenchée par des
irritants dans l'air (fumées, brouillard, gaz) ainsi que des

la délicate paroi des alvéoles est en panne, ce qui réduit
grandement la surface efficace de diffusion. La perte graduelle de
la capacité des poumons à attirer l'oxygène dans le sang des
organes prive d'oxygène la personne. Le cœur tente alors de
pomper plus de sang dans l'organisme afin de satisfaire le besoin
du corps pour l'oxygène, ce qui, dans certains cas, peut provoquer

produit par les poumons. La production de cette substance se
produit naturellement lorsque le tissu pulmonaire est exposé à des
irritants. Dans la bronchite chronique, les passages d'air vers les
alvéoles, les bronchioles, se bouchent avec du mucus. Cela
induit une augmentation de la toux afin d'éliminer le mucus, et est
souvent le résultat de longues périodes d'exposition à la fumée de

génétique causée par le dysfonctionnement du régulateur de la
conductance transmembranaire, une protéine transmembranaire
responsable du transport des ions chlorure. Cela entraîne d'énormes
quantités de mucus à boucher les bronchiolites, et donc à simuler
la bronchite chronique. Le résultat est une toux persistante et la

entraînant la croissance incontrôlée de cellules dans le tissu
pulmonaire. Il est souvent difficile de l'empêcher une fois
commencé, en raison de la sensibilité des tissus pulmonaires à

causés par un virus ou une bactérie. Des toxines et des
liquides sont libérées par le virus sur la surface des poumons qui
est considérablement réduite. Si cela se produit à un tel degré que
le patient ne peut pas tirer suffisamment d'oxygène de son
environnement, il lui faut de l'oxygène d'appoint.

