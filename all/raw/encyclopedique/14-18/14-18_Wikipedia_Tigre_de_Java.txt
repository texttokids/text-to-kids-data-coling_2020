

Chasseur européen posant devant une peau de Tigre de Java.
Le pelage du Tigre de Java se caractérise par de nombreuses et fines


Le Tigre de Java se caractérisait par des rayures longues et fines,
légèrement plus nombreuses que celles du Tigre de Sumatra (P. t.
sumatrae). Le museau était long et étroit, le planum occipitale
notablement étroit et les carnassières longues en comparaison de
la taille du crâne^. Le diamètre des empreintes était plus
grand que celui du Tigre du Bengale (P. t. tigris)^.

Plus petit que les sous-espèces continentales, le Tigre de Java était
un peu plus grand que le Tigre de Bali (P. t. balica) et sa taille
approchait celle du Tigre de Sumatra. Le mâle mesurait 2,48 m de
longueur totale « entre piquets » et il n'existe pas de
données pour la femelle. La longueur du crâne était de 30,6 à 34,9 cm
pour le mâle et de 27,0 à 29,2 cm pour la femelle. Le poids variait de
100 à 141 kg pour le mâle et de 75 à 115 kg pour la femelle^.

La petite taille du Tigre de Java et des tigres des îles de la
Sonde en général suit la règle de Bergmann, qui établit une
corrélation entre la masse des animaux endothermes et la
température extérieure : plus la température est élevée, plus

thermorégulation^^,^. Par ailleurs, la taille des
proies disponibles à Java, qui sont plus petites que les cervidés
et les bovidés d'Asie continentale, est également un facteur
limitant la taille des prédateurs^. L'influence de
l'insularité peut être un des facteurs du « rétrécissement » du Tigre



Le Tigre de Java se nourrissait de Cerfs rusa (Rusa timorensis),
de Bantengs (Bos javanicus) et de sangliers (Sus scrofa) et
parfois d'oiseaux aquatiques et de reptiles^. Il est
probable que le Cerf rusa et le sanglier formaient à l'origine ses
proies principales. L'analyse des restes de repas dans une tanière et
des fèces montre que le Tigre de Java se rabattait également sur
des proies plus petites, notamment après la modification de son
habitat : macaques, oiseaux, porcs-épics et Civettes des
palmiers (Arctogalidia trivirgata) composaient le régime
alimentaire^. On ne connaît rien de la biologie reproductive et



Le Tigre de Java est l'une des trois sous-espèces dont l’aire de
répartition est limitée à une île, à savoir l'île de
Java^. Son habitat est difficile pour un grand prédateur comme
le tigre : les pluies sont très abondantes, la biomasse
terrestre est faible et l'île se situe en dehors des zones d'habitat







La lignée des panthères, les Pantherinae, a divergé il y a 10,8
millions d'années de l'ancêtre commun des Felidae^.
Panthera palaeosinensis, qui vivait au début du Pliocène,


Panthera zdanskyi est découvert en 2004 dans le gisement fossile
de Longdan dans la province de Gansu en Chine. Ce fossile est daté
d'il y a 2,55 à 2,16 millions d'années (début du Pléistocène).
L'analyse cladistique montre que P. zdanskyi est le taxon
frère du tigre et conduit à penser que le berceau du tigre moderne se
situe au début du Pléistocène dans le nord-ouest de la Chine^.
Les plus vieux fossiles de tigre sont des fragments de maxillaires
et de mandibules datés du Calabrien (milieu du Pléistocène)


Depuis la Chine, le territoire du tigre se serait ensuite étendu sur
les îles de la Sonde puis vers l'Inde^. Entre la fin
du Pliocène et le début du Pléistocène, le tigre est une
espèce très largement distribuée dans l'Asie. Les populations ont
cependant fortement varié durant les différentes glaciations du
quaternaire^. Il y a 73 000 ans, le tigre frôla l'extinction
en raison de l'éruption du volcan Toba à Sumatra, ce qui peut
expliquer la faible diversité génétique de l’espèce actuelle^.




La première description du tigre a été effectuée par Linné en 1758
dans son livre Systema Naturae. Le Tigre de Java est décrit par
Temminck en 1844, à partir d'une unique holotype, en même
temps que le Tigre de Sibérie (P. t. altaica)^. Les
sous-espèces du tigre étaient traditionnellement définies par la taille
du corps, la morphologie du crâne et la couleur et les motifs de la
robe^. Ainsi, à l'origine, Temminck différencie le Tigre de
Sibérie de celui de Java par la longueur de leur fourrure^.
Toutefois, les premières descriptions des sous-espèces se sont basées
sur un très petit nombre d'individus, sans prendre en compte la
variabilité morphologique naturelle entre les spécimens d'une même


Plusieurs modèles tendent à diminuer le nombre de sous-espèces et
consistent à séparer les tigres continentaux (Tigres de Sibérie,

d'Indochine et de la Caspienne) des tigres insulaires (Tigres


En 2006, Mazak et Groves proposent d'élever le Tigre de Sumatra et le
Tigre de Java au rang d'espèce (respectivement Panthera sumatrae et
Panthera sondaica) selon une étude basée sur les dimensions crâniennes.
Le Tigre de Bali est par ailleurs proposé comme une sous-espèce du
Tigre de Java (P. s. balica)^. Le Tigre de Java est cependant
plus usuellement considéré comme une sous-espèce du tigre^.


Les premiers fossiles de tigre à Java sont datés d'il y a un
million d'années. Plusieurs formes se sont succédé : Panthera

soloensis^. Une étude de 2004 basée sur l'analyse de trois
marqueurs moléculaires de 134 spécimens différents a permis
d'estimer que le dernier ancêtre commun à toutes les sous-espèces
de tigre vivait il y a environ 72 000 à 108 000 ans, ce qui est tardif
en comparaison des autres félins du genre Panthera^.

Les premières traces du Tigre de Java moderne sont datées de 80 000 ans
avant le présent. Les fossiles sont plus nombreux durant le
Mésolithique (10 000 à 5 000 ans avant le présent), lorsque l'île
de Java présentait un habitat plus ouvert que de nos jours. Selon Peter
Boomgaard, au Néolithique (5 000 à 3 000 ans avant le présent), le
nombre de tigres à Java aurait probablement diminué en raison de
l'accroissement des forêts et peut-être de la présence


Concernant les liens entre le Tigre de Java et celui de Bali, il est
possible que les tigres aient pu traverser le bras de mer séparant
les deux îles, qui ne mesure que deux kilomètres. De telles traversées
sont plausibles, et des témoignages semblent corroborer ce fait, mais







Un Tigre de Java abattu à Malingping (id) dans la


grande partie de l'île de Java^^,^. En 1822, dans le
Java oriental, les tigres sont si nombreux entre Panarukan et
Banyuwangi qu'ils représentent un danger pour le bétail^. La
déforestation au milieu du XIX^e siècle est engagée lors de la
migration interne de milliers de paysans du Java central et de
Madura. Les ressources forestières et cynégétiques sont rapidement


Le chercheur Peter Boomgaard a inventorié une trentaine d'histoires
d'attaques dues à des tigres entre 1633 et 1687^ et une

ces histoires montre que certaines sont plagiées ou enjolivées pour
maintenir l'intérêt du public, toutefois, il apparaît que les victimes
sont surtout des hommes, travaillant en extérieur, comme les
postiers ou les derniers travailleurs sur une ligne de
coolies^. Vers 1850, les Javanais vivant dans les zones
rurales le considèrent comme un fléau^. Certains travailleurs
refusent d'aller dans les plantations de cannes à sucre, d'hévéa, de
tabac ou de café, par peur de se faire attaquer^ et les
voyageurs se déplacent armés pour se prémunir d'une attaque^.
Le Tigre de Java s'attaque également au bétail, contraignant certains
villages très exposés à se barricader ou à être désertés^. Les
attaques sont plus nombreuses pendant et à la fin de la


Toutefois, selon Peter Boomgaard, il faut tempérer les témoignages
recueillis car le Tigre de Java a également servi de bouc
populations^. Ainsi le cas du tigre de Caringin est une
cause célèbre débattue jusqu'au parlement néerlandais : les
villageois de Caringin, agriculteurs sur brûlis ont été forcés à
quitter leur foyer, soi-disant à cause d'un tigre mangeur-d'hommes,
mais plus probablement pour repeupler les rizicultures
abandonnées^. De même, les travailleurs ont pu utiliser le
prétexte de la présence d'un tigre pour stopper leur activité dans les
plantations^. Il est également fréquent que la locution
nature pour désigner une forêt sauvage et peu peuplée^.

Lorsque le Tigre de Java a commencé à entrer dans les villages, son
image dans l'imaginaire javanais en est modifiée. En 1822, le
gouvernement emploie pour la première fois des chasseurs
professionnels contre le Tigre de Java^. Il est estimé
qu'entre 1830 et 1860, environ 1 250 tigres et léopards sont mis à mort
chaque année à Java. Les Tigres de Java sont quant à eux responsables
de 120 décès annuels en 1860 et 30 décès annuels dans les années




La déforestation a considérablement réduit l'habitat du Tigre de
Java. Ici, une plantation d'Hévéa au début du XX^e siècle dans le


Au début du XX^e siècle, 28 millions de personnes vivent sur l'île de
Java. La production annuelle de riz étant insuffisante pour
subsister et nourrir les populations futures, la surface
cultivable est accrue de 150 % en quinze ans. Entre 1938 et 1975, la
couverture forestière sur l'île passe de 23 % à 8 % tandis que la
population javanaise augmente pour atteindre 85 millions
d'habitants^. Dès le début du XX^e siècle, des forêts de
tecks (Tectona grandis), de café et d'hévéa (Hevea
brasiliensis)^ sont plantées dans toute l'île, hormis dans les
zones les plus reculées et inaccessibles où les Tigres de Java doivent
se retirer^^,^. Le Tigre de Java est absent sur une
grande partie de l'île à partir des années 1940^ : la


Durant les années 1960, les maladies déciment les populations du
Cerf rusa (Rusa timorensis), la proie principale du Tigre de
Java^. Par ailleurs, les sangliers, qui représentent également
une part importante du régime alimentaire, sont empoisonnés en masse
par des campagnes gouvernementales^. Après le mouvement du
30 septembre 1965 et les massacres qui s'ensuivent en Indonésie,
des groupes armés se cachent dans les réserves et tuent les derniers


En plus de la chasse, de la disparition de ses proies principales et de
la diminution de son habitat, le changement de biotope (de la
forêt primaire à la forêt exploitée) ne permet plus
d'entretenir les mêmes populations d'herbivores : l'absence de
végétation au sol favorise le développement des primates et
diminuent les populations de sangliers et surtout de cervidés, ne
laissant plus que les espèces les plus petites, comme le Muntjac.
Sur des proies de taille moyenne à petite, le Tigre de Java s'est
retrouvé en concurrence avec le Léopard de Java (Panthera pardus
melas), bien mieux adapté^ : c'est un cas d'exclusion



Carte de Java avec emplacement des parcs où le Tigre de Java vit

Emplacements des dernières observations du Tigre de Java.

Jusque dans le milieu des années 1960, le Tigre de Java survit dans
trois aires protégées fondées durant les années 1920 et 1930 : les

réserve de Leuweung Sancang. À la fin des années 1960, il est
toujours possible de tirer des tigres à Banyuwangi dans le
Java oriental^. Après les insurrections civiles de 1965,


En 1971, une vieille femelle est abattue dans une plantation près du
Meru Betiri, la plus haute montagne de l'île, culminant à 1 192 m, au
sud-est de Java^. En 1972, cette zone de 500 km^2 est

protégée par un petit contingent de garde-chasse et quatre projets
de conservation sont initiés. La réserve est cependant morcelée
par deux grandes plantations situées dans les vallées. Les proies
potentielles se réduisent à quelques Bantengs (Bos javanicus) près
des plantations ; le Cerf rusa, la proie principale du félin, est
absent. Les sangliers sont largement distribués mais à de faible
densité de population^. En 1976, des empreintes de trois à
cinq Tigres de Java sont relevées à l'est du parc^ ; toutefois
les proies disponibles dans le parc sont jugées insuffisantes pour


En 1980, Seidensticker et Suyono recommandent d'agrandir la réserve et
de supprimer les activités humaines perturbatrices, ce qui est mis en
petite réserve devient le parc national de Meru Betiri. Ces
derniers efforts arrivent cependant trop tard pour sauver la



Plage de sable au premier plan et forêt vierge en arrière plan.
Le parc national de Meru Betiri a fait l'objet de nombreuses
recherches pour retrouver les derniers Tigres de Java.

En 1979, des observations sont rapportées près du mont Slamet à la

occidental^. En 1987, un groupe de trente étudiants de
l'institut agronomique de Bogor mènent une expédition au parc
national de Meru Betiri. Par groupe de cinq, ils sillonnent l'aire
complète^ et trouvent des fèces, des marques de grattage et
des empreintes^. En 1990, un autre groupe d'étudiants ne


En automne 1992, la toute première étude avec des pièges
photographiques est menée au parc national de Meru Betiri en
collaboration avec le WWF d'Indonésie. Entre mars 1993 et mars
1994, des appareils-photographiques à déclenchement automatique
disséminés sur 19 sites n'apportent aucune photographie du félin. Par
ailleurs, aucune empreinte n'a pu être relevée durant la même
période^. La publication du rapport entraîne la déclaration
officielle de l'extinction de la sous-espèce^. L'Union
internationale pour la conservation de la nature (UICN) classe le Tigre


Les nombreuses rumeurs sur la survivance possible du Tigre de Java dans
le parc national de Meru Betiri conduisent le responsable du parc,
Indra Arinal, à lancer une autre étude. Avec le soutien du Sumatran
Tiger Project, treize membres des équipes du parc se sont entraînés à
poser des pièges photographiques et à localiser leurs observations. Des
appareils photographiques à infrarouge ont même été fournis par The
Tiger Foundation^. Après un an de travail, les photographies
ne montrent pas de tigre, mais seulement quelques herbivores et de



La croyance en la non-disparition du Tigre de Java fait partie des
légendes urbaines de Java. Ainsi, en 1991, deux tiers des lettres
reçues par le Surabaya Post ont pour objet ces rumeurs^. Des
témoignages sur la présence du Tigre de Java sont régulièrement
rapportés par des personnes qui pensent toujours que la sous-espèce est
vivante^, telles que des attaques de tigres^ ou de
prétendues observations d'empreintes de pas^. Par exemple, en
1997, des incendies de forêt auraient débusqués quatre tigres
accompagnés de deux jeunes sur le mont Merbabu (ou le mont
Merapi) dans le Java central^. En 2017, une vidéo montrant un
félin ressemblant à un tigre dans le parc national d'Ujung Kulon
circule largement sur les réseaux sociaux indonésiens^.
Toutefois, Wulan Pusparini de la Wildlife Conservation Society
explique que si un arrêt sur image peut faire penser aux rayures
d'un tigre, le félin est en réalité un Léopard de Java^.

Ces nombreuses observations peuvent pour partie s'expliquer par une
confusion linguistique en javanais, où le mot « macan » désigne à
la fois le tigre et le léopard^. Par ailleurs, la situation
Dans les années 1990, les populations urbaines comme rurales pensent
encore que le tigre est un animal commun des forêts de leur île : elles
ne se rendent pas compte qu'il y a déjà disparu^.

Ces témoignages n'ont jamais été confirmés. Rien n'indique que ces
félins soient des tigres, et encore moins des Tigres de
Java : il est toujours possible qu'une autre sous-espèce ait été
réintroduite intentionnellement et illégalement. Par ailleurs,
l'environnement de l'île, fortement modifié, n'offre plus l'habitat
nécessaire à une implantation pérenne de ce félin^. La
biomasse du parc national d'Ujung Kulon est par exemple estimée à
500 kg/km^2 en 1980 tandis que dans le reste de l'Asie, celle-ci varie
de 750 à 3 300 kg/km^2^. En 2017, lors de la résurgence d'une
rumeur de découverte du Tigre de Java, Wulan Pusparini, experte sur les
tigres, rappelle qu'elle aimerait que la population s'enthousiasme
autant pour la préservation des espaces naturels très dégradés de
l'Indonésie que pour la recherche d'un animal disparu depuis trois



Avant la Seconde Guerre mondiale, des Tigres de Java sont
maintenus en captivité dans des zoos indonésiens, mais ceux-ci ont été
fermés durant la guerre. La paix revenue, le Tigre de Java était déjà
si rare qu'il était plus facile de se procurer des Tigres de




Articles détaillés : Tigre dans la culture et Culture




Le mot « macan » désigne également le Léopard de Java.

En javanais, il y a une confusion linguistique pour différencier
les félins. En effet, le mot javanais pour désigner le tigre, « macan »
est également couramment utilisé pour désigner le léopard. « macan »
fait également partie des termes désignant toutes sortes de félins
comme la Panthère nébuleuse de Diard (Neofelis diardi) « macan
dahan » ou encore le lion (Panthera leo) « macan garambis ». À
Java, il est absolument nécessaire de demander des précisions lorsque
le mot « macan » est utilisé, de façon à bien identifier le félin. Par
ailleurs, le tigre est désigné par trois mots folkloriques : « macan
loreng », le tigre ordinaire, « macan sruni », le tigre dont les
rayures se trouvent essentiellement sur la croupe, « macan gembong »,
un félin de couleur grisâtre clair. En Indonésien, le tigre est
appelé « harimau » et dans le Java oriental, il peut être désigné
par le terme « singa », qui signifie « lion »^. En haut




En Asie du Sud-Est en général, le tigre et l'homme sont considérés
comme descendant d'un même ancêtre. La tradition est donc que « le
tigre a une âme humaine ». Ainsi, une fable javanaise raconte qu'à
l'origine, les tigres et les hommes étaient végétariens et
vivaient en égal. Les tigres mangeaient des feuilles de Pædérie
fétide (Paederia foetida) et de Durian (Durio zibethinus). Un
jour, lors de la préparation du repas, un homme se coupa le doigt et un
bout de chair se trouva dans le déjeuner des tigres : ceux-ci y prirent


Une autre légende de la côte nord du Java oriental fait descendre
les tigres et les crocodiles de Syeh Sayyidina Ali, un beau-fils de




Le Tigre de Java est étroitement associé à la forêt et aux forces
magiques qui y résident. Ici, forêt près de Bogor dans le


Les pouvoirs magiques attribués au tigre sont intimement liés à la
perception de la forêt dans la culture javanaise : un lieu
mystérieux, dangereux et empli de forces magiques maléfiques. La forêt
est l'antonyme de la civilisation^. Elle ne forme qu'un
avec le tigre. Ainsi, une fable javanaise raconte que le tigre protège
la forêt en faisant peur aux villageois et que la forêt protège le
tigre en lui fournissant un abri : pour détruire l'un, il faut détruire


En jeûnant et méditant pendant quarante jours dans une caverne dans la
forêt, le tigre peut y acquérir le kesaktian, des pouvoirs magiques. Le
kesaktian lui permet notamment de devenir invisible dans la forêt.
L'un des tabou pour un tigre serait d'ailleurs d'être aperçu par
un humain : il devrait alors à nouveau jeûner pendant quarante jours


Selon les traditions javanaises, afin d'assurer sa sécurité dans une
forêt, il est nécessaire de montrer son respect au tigre en lui
rappelant qu'il est un aussi « un fils d'Adam », puis de faire une
prière^. Le félin, en raison de son origine humaine,
comprendrait le langage javanais, et utiliser le mot « macan » dans la
forêt risquerait de l'attirer. Ainsi, le tigre est appelé nenek
caché »)^, « kiai » ou « kiaine » (objets ou personnes
magiques)^. Ces termes révèlent la relation de parenté avec
l'homme et d'animal magique qui caractérise la symbolique du tigre à





Les tombes des personnages historiques ou religieux peuvent être
protégées par un esprit-gardien tigre. Ici, tombes de sept souverains


Les esprits gardiens dhanyang protègent et assurent la fertilité
de leur domaine, qui peut être la forêt ou un village par exemple. Ce
sont les habitants des arbres ou des pierres, souvent confondus avec
les esprits des ancêtres^. Le dhanyang se matérialise
fréquemment sous la forme d'un tigre^. Ainsi, dans la forêt
autour du Baluran, les esprits prennent la forme d'un énorme


Dans la culture javanaise, les ancêtres fondateurs d'un village sont
considérés comme possédant de puissants pouvoirs magiques et notamment
celui de négocier avec les tigres. Selon une légende de Lumajang,
le fondateur a transformé une personne en tigre pour nettoyer la forêt
puis cette personne aurait eu des enfants qui devinrent des
tigres-garous : leurs descendants mâles devaient avoir les doigts
coupés après leur mort pour éviter ce désagrément. De même, en
Madura et dans le Java oriental, l'âme d'un ancêtre magicien
peut se réincarner en tigre si son pouvoir spirituel n'est pas


Les dhanyang protègent tout ce qui a un rapport avec le fondateur, et
notamment sa tombe — comme le mausolée de Syeh Maulana Isak — ou
les écoles religieuses qu'il a pu fonder de son vivant. Ainsi, l'école
islamique Tanjung Keraksaan près de Probolinggo est protégée par
un tigre bienfaisant, incarnation de la sainteté du fondateur de
l'école. Selon une autre tradition, le tigre peut être invoqué par un
kyai qui doit jeûner et suivre différentes règles avant
de prononcer une formule invoquant le nom de Dieu. Le tigre ainsi créé
est une manifestation des anges gardiens associés au Coran et



Le tigre est également le protecteur des chamans, qui, dans la
tradition d'Asie du Sud-Est, peuvent contacter les ancêtres, les
esprits gardiens et sont en contact avec la forêt. Les chamans ont
souvent des tigres comme familier, et sont associés à ses qualités,
notamment la possibilité de disparaître et de réapparaître^.
Ce familier, a, selon les croyances du Java oriental, la capacité de
marcher sur deux jambes, et est parfois utilisé comme moyen de
transport par son chaman. Il est invoqué en brûlant de


Une dernière forme du tigre-esprit est celle d'une incarnation
d'une personne décédée de causes non naturelles ou d'une personne
punie. Ces revenants-tigres sont en général perçus comme bienveillants
et aident souvent leurs descendants. Selon la tradition javanaise, le
revenant apparaît quarante jours après la mort. Entre le premier et le
septième jour, l'âme devient fantôme, puis tigre. Au Situbondo, la
croyance veut que le tigre peut être invoqué en brûlant de l'encens, en
faisant une offrande, puis en jetant un galet^. Les tigres
invoqués sont appelés macan onjangan : il protège le foyer et ne peut



Collier de perles blanches avec un pendentif formé de deux griffes

Collier javanais en perles de noix de coix et griffes de


Le corps et l'âme du tigre ont des vertus médicales dans les croyances
de Java occidental. L'âme humaine du félin peut être invoquée grâce à
la méditation et au jeûne pour soigner. La littérature médicale
préconisant l'utilisation de représentations ou de parties du corps de
tigre, est nombreuse. Les dents protègeraient de la magie noire.
Les moustaches seraient empoisonnées et le simple fait d'en transporter
avec soi permettrait de paraître dangereux et féroce. À l'Est de Java,
on pense que le tigre possède un os en forme d'étoile appelé sengkel,
qui contient sa force. Cet os serait situé dans l'épaule gauche et
disparaîtrait une heure après la mort. Utilisé comme amulette, il


Le tigre est donc puissant, et son corps contient cette puissance. Des
témoignages de tigre littéralement réduits en pièce pour la fabrication
d'amulettes ou d'armes trempées dans le sang sont fréquents. Ce
comportement a été notamment rapportés après les rampog





Le tigre-garou, appelé macan gadhungan, est une légende très répandue
dans le Kediri en particulier et dans le Java oriental en
général^. La figure du tigre-garou est considérée comme
maléfique et doit donc être dissociées des différents esprits incarnés
en tigre ou des protecteurs des chamans^. Le tigre-garou est
souvent considéré comme la réincarnation d'un aristocrate
décédé en exil dans la forêt de Lodoyo. La transformation
en tigre est issue d'un savoir qui se transmet de génération en
génération par des mantras, le premier tigre-garou étant, selon la
légende, un homme ayant vécu dans la forêt de Lodoyo à l'époque du roi


Le tigre-garou se reconnaîtrait par une rainure sur sa lèvre
supérieure, et par ses pattes arrière aux talons inversés. Lors de la
transformation, la tête de l'homme deviendrait la queue du tigre. En
tant qu'homme, le tigre-garou mangerait peu et ne pourrait pas devenir
riche, mais vivrait très longtemps. Le tigre-garou est très populaire
dans la littérature et le septième art du Sud-Est asiatique. Lorsqu'un
fait divers survient à propos d'un macan, la rumeur de la présence
d'un tigre-garou se répand toujours à Java, même à la fin du




Les tigres ont été utilisés pour l'exécution des rebelles et des
criminels par la cour javanaise à la fin du XVIII^e siècle comme une
forme d'ordalie. Ce châtiment n'était pas toujours fatal : ainsi,
deux hommes accusés d'être entrés sans autorisation dans le kraton
ont été placés armés de bâtons dans la cage de trois tigres choisis
pour leur férocité. Survivants à ce combat, ils ont été condamnés à




Lithographie avec un tigre entouré d'hommes armés de piques
Un rampog macan, cérémonie au cours de laquelle était sacrifié un
tigre à Java (lithographie tirée d'une peinture de L. H. W. M. de


Le rampog macan est une cérémonie sacrificielle javanaise ayant cours
du XVII^e siècle au début du XX^e siècle. À l'origine réalisé dans
l'alun-alun des cours royales javanaises, le rampog macan était
constitué en deux parties : le sima-maesa, un combat dans une cage
entre un buffle domestique (Bubalus bubalis) et un
tigre, et le rampogan-sima, où plusieurs tigres étaient
positionnés dans un cercle d'hommes armés de piques, et se tuaient en
tentant de s'échapper^^,^. Le rituel a évolué au
cours des siècles, et entre la fin du XVIII^e siècle et le milieu du
XIX^e siècle, le rampog macan est amputé du combat entre le tigre et le
buffle et son caractère sacré s'amoindrit pour devenir un spectacle mis
en scène pour satisfaire les visiteurs européens^. Les félins
utilisés, capturés en avance et maintenus en captivité par
l'organisateur jusqu'à la réalisation du rituel, étaient en majorité
des Tigres de Java ou des Léopard de Java (Panthera pardus melas)


Le rampog macan a été perçu par les observateurs européens comme la
mise à mort par la communauté du mal, personnifié par le
tigre^. Toutefois, une interprétation plus fine voit la lutte
victorieuse du pouvoir royal javanais représenté par le buffle contre
le chaos et les forces chthoniennes représentés par le
tigre^^,^, voire comme une purification symbolique
contre le chaos du royaume tout entier^. Au XIX^e siècle, le
rituel est devenu une mise en scène du pouvoir et de la richesse des
priyayi sur la noblesse princière déclinante^, puis la
représentation d'une lutte politique symbolique entre la compagnie
néerlandaise des Indes orientales (le tigre) et les autorités de Java





Le tigre est un animal à la symbolique polysémique dans la
culture javanaise. Dans la tradition javanaise, le tigre est très
fortement associé à la royauté, sans y être parfaitement assimilé.
Historiquement, les priyayi ont cherché à se réapproprier les
symboles que sont le rampog macan et les chasses au tigre. Ces
initiatives ont amoindri la symbolique uniquement régalienne du
tigre. Par ailleurs, les croyances d'Asie du Sud-Est en général, et de
Java en particulier, font du tigre un animal ambivalent associé aux
esprits et qui vit symboliquement à la lisière entre la


Le rôle du tigre est également celui du gardien de l'ordre moral. Cette
symbolique est particulièrement visible dans les légendes qui
impliquent les chamans et les ancêtres, très souvent confondus à Java
et garants du bien-être moral de la communauté. Le tigre-esprit
ancestral est un gardien de la vertu^. Par ailleurs, le
rôle de gardien du lion, traditionnel dans la culture
islamique, a été endossé par le tigre sur l'île de Java^.

Lorsque les contacts entre les populations javanaises et le tigre se
sont accrus en raison de la disparition des forêts, la symbolique
ambivalente du tigre (animal-gardien, mais également animal sauvage,
image ni positive ni négative) s'est peu à peu atténuée au profit de la
réputation d'un animal dangereux et d'un tueur vicieux^. Cette
réputation est largement injustifiée puisqu'à cette époque, l'éléphant,
considéré comme bénéfique, tuait plus que le tigre^. Ce
changement de mentalités est également imputable à la mise en contact
avec les chasseurs Européens, qui sont responsables en grande partie


En raison de sa disparition, McNeely et Wachtel considèrent qu'« à
l'origine symbole du pouvoir de la nature, le Tigre de Java est à
présent celui du paradis perdu ». Ce félin est pris comme symbole
de la disparition de la nature sauvage d'Asie, plus encore que celle




Marionnette du tigre Si Harimau pour le théâtre d'ombres


Le tigre, avec le buffle, est un animal dont la signification
mystique et politique est très forte dans la littérature
aristocratique javanaise^. Le thème du tigre-garou est
notamment traité dans le long poème du premier tiers du XIX^e siècle :


Le cycle de Sang Kancil est une série de comptines^
qui se retrouve dans l'ensemble du monde malais, y compris à Java. Ce
cycle folklorique raconte les aventures de Sang Kancil, un
Chevrotain malais (Tragulus javanicus) malicieux et rusé, qui
berne de nombreux animaux plus puissants. Il est comparé par
Romain Bertrand au Roman de Renart ou aux Fables de La
Fontaine. Dans cette littérature populaire, le tigre Si Harimau y est
dépeint comme un roi hautain qui maltraite ses sujets, berné par Sang


Dans la littérature européenne du XVII^e siècle, les écrits sont encore
très empreints du mythe de l'Asie sauvage. Le Tigre de Java est souvent
décrit comme un animal sanguinaire aussi grand qu'un cheval. En 1628,
un écrit relate une histoire d'un félin mesurant neuf mètres de long


Le Tigre de Java est largement utilisée dans la publicité de Java,
où il est associé aux idées de force et de vitesse. Par ailleurs, la
présence d'amulettes ou d'emblème de tigre dans les voitures ou aux
fenêtres des échoppes est fréquente^. Dans le théâtre
d'ombre wayang, très populaire dans les îles de Bali et
Java, le tigre figure parmi les rares animaux ayant leur
marionnette^. Par ailleurs, chaque représentation s'ouvre et
se termine sur une image de l'univers : une montagne surmontée
d'un arbre de vie, avec au pied de ceux-ci un tigre et un Banteng


