
L'étymologie du substantif neutre latin templum est discutée. Ou
bien, comme le suggère Varron, il serait dérivé de la racine

serait dérivé de la racine *temp- (« tendre, tirer un fil »), au sens


souligner que le latin templum (« espace circonscrit, orienté et
inauguré ») et le français temple (« construction religieuse ») ne sont
pas coextensifs. En effet, certains temples ne sont pas des templa :
c'est notamment le cas de tous les édifices ronds, tel l'aedes
Vestae (« temple de Vénus ») à Rome^^,^ ; et il existe
des templa qui ne sont dédiés à aucune divinité : c'est notamment le
cas où celui où le peuple se réunit pour élire les magistrats^.

Bien que le terme actuel « temple » soit un emprunt direct au latin
templum^, il peut servir également à traduire d'autres termes
latins comme aedes et aedicula, delubrum, fanum, sacellum, sacrarium,



Le templum désigne un espace terrestre, généralement rectangulaire,
correspondant à l'espace délimité dans le ciel par un augure à
l'aide de son bâton^ et approuvé par les auspices^
espace que ce prêtre effectue ses observations (comptage des oiseaux,
passage de nuages, etc.)^. Seul ce qui peut survenir ou être
visible depuis ce périmètre, séparé des alentours profanes^, est
pris en considération par l'augure et a une signification religieuse.
Cette pratique des augures est désignée comme la contemplatio, d'où le
mot français « contemplation » tire son origine. Les signes étant
toujours considérés comme les manifestations d'un dieu, les Romains
estiment que le dieu qui s'est manifesté souhaite que l'espace désigné
par les augures lui soit réservé. Le templum est à l'origine de



Selon Varron, l'aedes diffère du templum parce qu'il n'est consacré
que par un pontife, alors que le templum est en plus
inauguré^. Généralement, chez les auteurs antiques, l'aedes
désigne le bâtiment où réside une divinité^^,^. La
matérialisation du templum originel en un bâtiment découle
vraisemblablement de la nécessité de séparer physiquement l'espace
sacré, ou le sanctuaire (sacrarium), des profanes, c'est-à-dire du
monde extérieur. L'aedes conserve l'orientation du templum à
l'intérieur duquel il est construit^. Il arrive que des aedes
ne soient pas construits dans une enceinte consacrée comme c'est le cas
pour le temple de Vesta. Au contraire, des édifices bâtis dans les
limites d'un templum ne sont pas toujours considérés comme des aedes


Le terme aedicula, « édicule » en français, dérive du terme aedes.
Il s'agit souvent d'un sanctuaire de petite taille inclus dans un



Le delubrum désigne l'ensemble de l'aire sacrée, contenant le templum,
souvent pavée pour marquer la délimitation ou entourée de


Le terme fanum est un terme très générique qui peut être utilisé par
les auteurs antiques pour désigner indifféremment tout type d'aire ou
d'édifice sacrés, du petit bois au grand temple^. Il est
souvent employé par les archéologues pour qualifier les lieux de cultes


Le sacellum est un lieu consacré à une divinité à ciel ouvert (sine


Selon Ulpien, le sacrarium correspond à la salle d'un édifice lié à
un templum dans laquelle sont déposés les objets sacrés (locus in quo


La curia correspond à un templum contenant un autel pour procéder aux


Enfin, un lucus est un lieu sacré, souvent matérialisé par un petit
bois, compris dans les limites du templum auquel il est lié. Il s'agit
généralement de sanctuaires situés en dehors des limites des



La construction d'un temple doit respecter cinq étapes : le votum
initial, la locatio, l'inauguratio, la consecratio et enfin la



Il s'agit de la formulation d'un vœu ou d'une promesse de construire un
temple dédié à une divinité. Ce vœu est généralement formulé par un
général à l'occasion d'une bataille (avant pour obtenir les faveurs
d'un dieu, pendant pour en appeler à un dieu et faire basculer l'issue
des combats ou après en remerciement à un dieu pour la victoire
obtenue), durant des luttes politiques internes ou lors d'une



La locatio proprement dite (« localisation ») consiste dans le choix du
site sur lequel le temple sera édifié, avec l'indication de ses limites
relatives^. Mais la locatio s'entend également plus

faciendi (« contrat de louage d'ouvrage ») avec le conductor

Une fois le vœu formulé, il faut choisir le lieu de construction et
temple romain doit être tournée autant que possible vers le couchant
orientation de sorte qu'un fidèle en prière ou offrant un sacrifice
devant l'autel est tourné vers le levant (vers l'est)^[a
5]^,. Lorsqu'il n'est pas possible d'édifier le temple selon
cette orientation, il est fait en sorte qu'on puisse, de cet édifice,
embrasser du regard la plus grande partie de la ville. Enfin, lorsqu'un
temple est construit le long d'une rue ou au bord d'une route, on fait
en sorte que les passants puissent voir l'intérieur et ainsi saluer la



Représentation d'un augure tenant le lituus (Nordisk familjebok,
reprod. d'un bas-relief conservé au Musée des Offices).

L'inauguratio (« inauguration ») suit la locatio ou accompagne
celle-ci^. Le rite de l'inauguratio est toujours célébré par
un augur (« augure ») qui officie seul, à la demande expresse d'un
magistrat ou d'un prêtre^. À la suite des travaux d'Isaac
Marinus Josué Valeton, les auteurs admettent que la cérémonie comporte
quatre phases successives^. La première est l'auspicatio
Jupiter^. La deuxième est la liberatio par laquelle
l'augure élimine les servitudes qui grèvent le sol^. La
troisième est la délimitation rituelle du lieu^. La dernière
est l'effatio par laquelle l'augure énonce les limites du


Avant le début de la construction à proprement dite, le lieu choisi est
délimité et consacré par un augure qui délivre l'espace de toute
servitude divine^. L'augure prend les auspices afin de
s'assurer l'aval de Jupiter^. L'endroit choisi est ensuite
nettoyé et aplani. Puis l'augure, tenant son lituus de la main droite,
invoque les dieux et marque dans le ciel l'espace sacré en traçant une
ligne d'est en ouest. Ainsi, l'augure sépare l'espace de ce qui
l'environne, le purifie et le sanctifie^ (effatum et
liberatum^). Le bornage du futur temple avec des cippes est
effectué à l'aide d'équerres et de cordeaux en suivant les mouvements
de l'augure^. Enfin, ce dernier prononce la déclaration
inaugurale qui rend l'espace délimité inviolable^.


La consecratio (« consécration ») du temple précède sa dedicatio. Le
rituel de la consecratio est célébré par un pontife^. Celui-ci
récite une formule des libri pontificales. Elle consiste en la création
d'un aedes. À l'origine, l'acte de consecratio consiste à tracer avec
une charrue deux axes perpendiculaires qui définissent l'orientation du



La dedicatio (« dédicace ») conclut la fondation du temple^.
Lorsque le sanctuaire est public, le rituel de la dedicatio est
d'ordinaire célébré par un magistrat cum imperio^ en
exercice : effet, un magistrat cum imperio sorti de charge, un
magistrat sine imperio ou encore un privatus ne peut célébrer la
dedicatio d'un tel sanctuaire qu'après y avoir été expressément
autorisé par les comices^. Il consiste en la répétition, par
le magistrat, de la formule dédicatoire solennelle que le pontife a


Une fois la construction achevée et les lieux consacrés, les prêtres
peuvent procéder à l'inauguration officielle du temple (la dédicace).
Ce jour (dies natalis) est célébré par des cérémonies annuelles.


L'administration des temples de Rome, et de tout ce qui s'y rapporte,
est un privilège du collège des pontifes. On appelle aeditui les
hommes chargés de la gestion directe et quotidienne des temples.

En ce qui concerne la propriété des temples, il est établi que dans les
temps anciens, un domaine foncier est attribué à chaque temple. Mais
ces terres ne servent qu'à assurer la subsistance des prêtres. Les
rituels publics (sacra publica) sont assurés aux frais de l'État. Ainsi
on suppose que lorsque les dépenses pour l'entretien des temples,
compensées par les tarifs sacerdotaux et les amendes, sont trop
importantes pour permettre d'entreprendre des réparations, l'État se
porte caution, à moins qu'un particulier n'assure la couverture de ces




Maquette du temple de Jupiter Capitolin archaïque.

Il semble n'y avoir eu que très peu de temples dans les premiers temps
de Rome et, si dans bien des cas la vénération d'une idole y est
attestée de temps immémorial, le premier temple de cette même idole
n'est construit qu'à une époque historique relativement récente. Les
lieux d'adoration des premiers Latins ne devaient être le plus souvent


L'apparition des premiers sanctuaires monumentaux dans le second quart
du VI^e siècle av. J.-C. représente une étape importante dans
l'évolution de l'architecture romaine religieuse pour laquelle les
influences étrusques se mêlent aux influences grecques^. Les
premiers temples romains suivent le modèle étrusque. Les temples
jumeaux de style toscan de l'aire de Sant'Omobono sont parmi les
premiers à être construits à Rome, à proximité du port
fluvial^. Le VI^e siècle av. J.-C. voit la mise en
chantier d'un temple d'importance, le temple dédié à Jupiter sur
le Capitole dont la construction débute en 580 av.
J.-C.^. Les grandes dimensions du temple (54 mètres sur 74
mètres à la base) en font le plus grand monument de la péninsule
italienne à cette époque^. De nombreuses modifications sont
introduites à cette occasion dans le plan type du temple romain :
apparition d'un pronaos, des antae qui agrandissent la cella et d'un
haut podium qui fait office de fondation et qui renforce l'axialité du
temple^ : contrairement aux temples grecs qui peuvent être
approchés de toutes les directions, le temple romain possède une
orientation précise dans l'espace qui découle des rites
auguraux^. Vu de l'extérieur, le temple romain se caractérise
par l'importance donnée à la façade alors que le mur du fond est
souvent dépourvu de colonnade. Les premiers temples romains suivent un
plan rectangulaire, un escalier extérieur en façade mène au porche
précède la cella intérieure, souvent tripartite. Le toit est à
deux pans peu inclinés (« toit rampant ») couvert de tuiles en terre


Progressivement, l'influence architecturale grecque se fait davantage
ressentir, surtout après l'invasion de la Grèce au II^e siècle av.
J.-C., et devient prépondérante sur le modèle des temples romains. Peu
grecs et italiques. Les plans au sol s'étirent en longueur, la cella
est plus étendue que le porche, les trois pièces de la cella cèdent la
place à une grande salle unique qui abrite la ou les statues de culte



Restitution 3D du Grand Temple de Tarse en Cilicie, sans doute le



Le temple romain a généralement un plan rectangulaire ou circulaire.
Pour les temples rectangulaires, si l'entrée est située sur un côté
court, le temple est dit « oblong ». Mais il arrive plus rarement que
l'entrée se situe sur un côté long, le temple est alors qualifié de
de Véiovis ou le temple de Castor et Pollux in Circo)^.
Les temples romains combinent parfois la forme rectangulaire et


Le temple romain est constitué d'un corps central ceint de mur qui
abrite la cella. Le toit couvrant cette cella est appelé testudo,
mais selon Varron, il peut ne pas y en avoir afin de laisser
passer la lumière du jour pour illuminer l'effigie divine.
Le corps du temple peut-être entouré d'une ou plusieurs colonnades et
est précédé d'un pronaos, un porche ou vestibule délimité par les
colonnes en façade et les premières colonnes latérales^.

Les proportions et la décoration du temple dépendent de l'ordre
architectural utilisé. Ce dernier a évolué tout au long de l'histoire
romaine. À l'origine, les Romains utilisent principalement l'ordre
toscan puis adoptent les différents ordres grecs, l'ordre corinthien
devenant le plus répandu. Les Romains introduisent un nouvel ordre
architectural, l'ordre composite, qui mélange des éléments de l'ordre
ionique et de l'ordre corinthien. Ils introduisent également de
nouveaux éléments architecturaux comme la voûte, le dôme et
l'arche^. À l'époque archaïque, les matériaux principalement
utilisés lors de la construction des temples sont le bois, la brique et
le tuf. À l'époque républicaine, les architectes utilisent le tuf et le
travertin, auxquels s'ajoute le marbre sous l'Empire.





Différents plans au sol possibles montrant la disposition des
colonnades par rapport au corps central pour les temples grecs et

Articles connexes : Colonne (architecture) et Ordre


Les colonnes sont plus souvent lisses lorsqu'elles supportent des
chapiteaux toscans ou doriques et plus souvent cannelées pour les


Le plan architectural des temples peut varier sensiblement en fonction
de la disposition des colonnes extérieures par rapport aux murs de la



colonnes ou pilastres des portiques latéraux et postérieurs sont à

trois côtés (la façade et les côtés latéraux), le mur postérieur du
temple est prolongé de chaque côté d'une longueur correspondant à

double colonnade sur les quatre côtés, la colonnade proche des murs
et le passage qu'ils forment sont dits « intérieurs » tandis que la
deuxième colonnade et le passage qu'elle forme avec la première

de colonnes latérales est à demi-engagée dans les murs de la cella.

De plus, un temple peut être caractérisé par le nombre de colonnes
qu'il comporte en façade et sur les longs côtés^, les colonnes
d'angle étant comptées deux fois^. Pour les colonnes en


Enfin, on peut mesurer l'entrecolonnement, c'est-à-dire la distance








Bien que très exceptionnellement conservés en original, les décors
tympanaux des temples se laissent également étudier grâce à leurs
reproductions fidèles sur les reliefs historiques et sur quelques rares
monnaies impériales . Des mentions chez les auteurs antiques complètent

Articles détaillés : Fronton (architecture) et Tympan





















