
Le loup est une espèce de mammifère carnivore qui fait partie de la famille des canidés, comme le chien, le renard, le coyote ou le dingo.
Cette espèce est protégée dans de nombreux pays, car ses populations se sont beaucoup réduites.
Le mot loup vient du latin lupus, qu'on retrouve dans le nom scientifique du loup : Canis lupus.
La femelle de l'espèce est la louve, et le « petit » est couramment appelé « louveteau », mais il existe en fait différentes façons de le nommer selon son âge.


Le loup est un animal quadrupède, couvert de fourrure habituellement grise (d'où son nom), mais en fait de couleur très variable. Il ressemble beaucoup au chien.
Il existe de nombreuses sous-espèces de loups, vivant à différents endroits dans le monde et dont l'apparence peut varier.
Les loups vivent en meutes, lesquelles sont dirigées par un couple de loups, qui est le seul à avoir le droit de se reproduire. Les loups sont des carnivores qui chassent toujours collectivement. Leurs dents déchirent efficacement la chair de leurs proies. Ses prédateurs sont les ours, l'homme, les lynx et parfois les grands rapaces comme les pygargues à tête blanche.


Selon les sous-espèces, les loups du grand Nord américain sont les plus grands, ils mesurent 70 à 90 cm de haut à l'épaule (80 cm en moyenne). Les loups d'Espagne (Ibérique) sont les plus petits et ne dépassent pas 50 à 70 cm de haut (60 cm en moyenne). Quant aux loups d'Europe, ils sont à l’intermédiaire et mesurent de 60 à 80 cm au garrot (70 cm en moyenne) ainsi qu'un crâne de 23 cm à 28 cm de long et de 13 à 15 cm de large. Ses crocs mesurent 6 cm dont 2 cm sous la gencive.




Le loup est un quadrupède (il marche à quatre pattes). C'est un très bon coureur : il court vite (il atteint 50 km/h en course avec des pointes de 60-65 km/h) et, surtout, très longtemps. Il est capable de parcourir jusqu'à 50 km par jour !



Le loup a 42 dents exerçant une pression de 150 kg par cm² contre 60 pour le chien. Il mange de la viande crue ; c'est un carnivore. Comme il a des griffes, des dents et un système digestif qui lui permettent de se nourrir de chair crue, on dit aussi que c'est un carnassier. Avec ses mâchoires puissantes, il peut manger des oiseaux, des lapins, de petits rongeurs, des moutons, des cerfs, des sangliers, etc.. Bien qu'ils soient carnivores, les loups se nourrissent aussi parfois de raisin, qui leur apporte du sucre et des vitamines. Un loup affamé peut également manger des insectes ou encore des champignons. Il peut être à l'occasion charognard (il mange des animaux morts).
Ce sont des animaux opportunistes: ils mangent ce qu'ils trouvent.
Pour se nourrir, ils chassent en meute sur le territoire de leur clan.
Petit, le louveteau tète sa mère. À 2 mois, il mange de la viande mâchée pour lui par les adultes puis il apprend à chasser pour s'alimenter : d'abord des animaux de la taille de petits rongeurs, apportés au début par sa mère pour qu'il s'entraîne.


Le loup s'exprime de façon proche de celle du chien : il grogne et gronde. Il hurle1 aussi, mais lui, essentiellement avec ses congénères2 ; le hurlement des loups peut prendre de nombreuses formes et avoir différentes fonctions au sein de la meute: il sert principalement à communiquer la position de chacun, à permettre le regroupement pour la chasse, à assurer la cohésion du groupe et à permettre la reconnaissance vocale de chaque individu3.


Le loup n'est pas solitaire. Il vit et chasse en groupe (dit clan ou meute). La meute est composée de 8 à 20 loups.
La meute est composée de deux parents, et de leurs petits. On a longtemps cru que la meute formait un groupe hiérarchisé, car les deux parents sont les seuls à se reproduire, et ce sont les jeunes qui mangent en priorité. En réalité, ces règles sont là pour assurer la survie de l'espèce et empêcher l'inceste. Les études qui avaient aboutis à la conclusion d'une hiérarchie dans la meute était en réalité réalisées par des nazis, qui calquaient leur idéologie sur ce qu'ils pensaient être le mode de vie des loups. Le terme de "loup alpha" avait été introduit par le Dr. David L Mech, mais celui-ci est revenu dessus, avouant que le terme était inexact. Une vidéo Youtube est disponible à ce sujet : "Alpha" Wolf ?
Les signaux dits de "soumission" (lorsque les loups en lèchent d'autres sur les joues, ou se mettent sur le dos) sont des signaux utilisés pour éviter les conflits. La dominance, chez les loups (comme chez les chiens), ne résulte pas d'une hiérarchie stable, mais plutôt d'une situation (un loup A peut dominer B, et C peut dominer A dans une certaine situation, mais dans une autre cette boucle ne sera pas forcément la même). C'est pourquoi le terme de hiérarchie est erroné chez les loups. 
Le père loup et sa femelle restent en couple toute leur vie ; eux seuls ont des petits. Un louveteau devenu adulte peut choisir de quitter la meute, pour en fonder une nouvelle. 
Le clan a aussi un territoire : les loups le délimitent avec leur odeur et leurs excréments4 et aucun loup extérieur au clan n'a le droit d'y pénétrer. La nécessité de ce territoire très vaste explique ses conflits avec l'homme.


Les loups sont en âge de se reproduire vers 22 mois.
Après avoir préparé l'arrivée de ses louveteaux au début du printemps (aménagement d'une tanière avec des poils et des herbes sèches), la louve en met au monde de quatre à sept. Les louves plus jeunes ne font naître que de un à trois louveteaux.
Le petit du loup, communément appelé « louveteau », porte plus précisément différents noms en fonction du stade de sa croissance : 



Tout fragile, il naît sourd et aveugle. C'est sa mère qui chasse et lui rapporte de la viande. 
Adulte, il a la taille d'un gros chien et pèse environ 40 kg. Il vit entre 8 et 16 ans ; le record de longévité en captivité est de 20 ans.


Il y a des loups essentiellement dans l'hémisphère Nord. Selon la région, les loups sont quelque peu différents.
Par exemple, au nord, le pelage des loups est blanc. Il est adapté à son environnement : le loup peut mieux se camoufler dans un paysage de neige, surtout quand il souhaite chasser.
Si l'on compte aussi tous les loups domestiques, qui sont plus connues sous le nom de... chien, l'espèce est présente dans presque tous les pays du monde, partout où vit l'homme.
Le dingo est une sous-espèce de loup qui vit en Australie. C'est la seule sous-espèce sauvage de loup de l'hémisphère sud. Les dingos sont les descendants d'anciens chiens domestiques qui accompagnaient des hommes ayant abordé l'Australie il y a environ 4 000 ans, et qui depuis sont redevenus sauvages.


Longtemps, les loups ont été très craints des hommes. Ils étaient très nombreux à vivre au sein de grandes forêts, et il est arrivé qu'ils s'attaquent également à l'homme. Les jeunes enfants, qui à la campagne, étaient souvent chargés de garder les troupeaux, en étaient les principales victimes. Du coup, les hommes inventèrent de redoutables pièges, les chassèrent au fusil et en tuèrent beaucoup.
De nos jours, les loups ne sont pas très dangereux pour l'homme. Ils font encore parfois peur parce qu'avec leurs grandes gueules, ils peuvent dévorer du petit au gros ; aussi à cause de l'existence de contes anciens (comme le célèbre Petit Chaperon rouge), etc.. 
En revanche, ils ont bien failli disparaître totalement d'Europe et on a décidé de les protéger en 1990. Les loups sont revenus en France dans certains massifs de montagne comme les Alpes ou les Vosges et le massif central. Ils n'ont pas été réintroduits mais ont recolonisé ces régions à partir de populations de loups d'Italie. De nombreux éleveurs de brebis se plaignent maintenant d'attaques sur leurs troupeaux et souhaitent voir leur population maîtrisée.


C'est probablement une race de loup qui fut l'un des premiers animaux domestiqués par l'homme à la Préhistoire. Les descendants de ces premiers loups apprivoisés sont devenus les différentes sous-espèces d'animaux de compagnie communément appelées « races de chiens ».


Le loup est représenté par un assez grand nombre de sous-espèces selon les régions.
Ce sont notamment En Eurasie le loup d'Arabie, le loup des steppes, le loup de Mongolie, le loup de Sibérie, le loup des Abruzzes, le loup ibérique, et en Amérique du Nord le loup arctique (Canis lupus arctos), le loup d'Alberta, le loup des plaines et le loup du Mexique.
le chien (Canis lupus familiaris) n'est pas vraiment une espèce différente du loup : ce sont en fait les différentes races d'une sous-espèce de loup qui ont été domestiquées il y a environ 50 000 ans. Les loups et les chiens font toujours partie de la même espèce et peuvent se reproduire entre eux. Le chien peut donc être considéré aussi comme une sous-espèce du loup.
C'est aussi le cas du dingo (Canis lupus dingo), un animal sauvage dont les ancêtres sont des chiens, eux-même issus du loup !


Le loup est un mammifère qui fait partie de la famille des Canidés, et, plus précisément, du genre Lupus. D'autres espèces appartenant également à ce genre sont assez proches du loup :
- Certains animaux appelés « loups » ne font pas partie de la même espèce que le loup commun (ou loup gris), bien qu'ils en soient très proches : c'est le cas par exemple du loup roux (ou loup rouge), dont le nom scientifique est Canis rufus.
- Le coyote (nom scientifique : Canis latrans) est une espèce très proche du loup. Certains scientifiques pensent d'ailleurs que le loup roux serait en fait un hybride de loup et de coyote. Dans ce cas, le loup roux ne serait pas une espèce proprement dite, et son nom scientifique deviendrait Canis lupus x latrans.
- Certaines populations de loup, considérées comme des sous-espèces, tels le loup des Indes ou le loup de l'Est, pourraient en fait être des espèces distinctes, très proches du loup.
- Le loup terrible (nom scientifique Canis dirus, Dire Wolf en anglais) est une espèce de canidé préhistorique, très proche du loup, qui a disparu. Il était beaucoup plus grand que le loup actuel.




Le loup est présent dans les mythes d'un grand nombre de cultures et a toujours eu une place importante dans les légendes des hommes et dans la littérature.
- Dans la mythologie scandinave, Fenrir est un loup géant, fils du dieu Loki, qui doit tuer tous les dieux lors du Ragnarok (la fin du monde). Hati et Skoll sont les Managarn, des loups géants descendants de Fenrir. Ils poursuivent sans relâche la lune et le soleil pour les dévorer. On leur attribuait les éclipses. Geri et Freki, eux, sont les deux loups domestiques du dieu Odin, le roi des dieux.
- Dans la Rome antique, Rome a été fondée par Rémus et Romulus, deux jumeaux nouveaux-nés qui auraient été élevés et nourris par une louve, attaquant normalement les élevages.
- En Grèce antique, les lois de Solon instituent des primes pour tous ceux qui abattent les loups dangereux, poursuivant le mythe de la bête féroce.
- Dans l'Antiquité, voir un loup avant une bataille était un présage de victoire, le loup étant l’animal symbolique de la guerre et de la chasse.
- Chez les Indiens d’Amérique (Amérindiens), le loup est mieux vu : il incarne l’esprit, la réincarnation, la liberté.
- Le loup-garou est une créature légendaire : il s'agit d'un être humain qui se transforme en loup à la pleine lune. Dans certaines versions, il s'agit d'un loup monstrueux, ou d'une créature mi-loup, mi-homme.



La figure du loup est présente dans la littérature à travers Le Roman de Renart. Le baron Ysengrin, oncle de Renart, est souvent la victime du renard malicieux. Il est décrit comme un seigneur brutal (ou comme une bête féroce, selon le cas), et assez bête.

Le loup apparaît comme un prédateur sanguinaire et féroce à travers :- des légendes : les loups-garous, la Bête du Gévaudan (monstre-loup mangeant les hommes),- des contes européens comme ceux de Charles Perrault ou des Frères Grimm. C'est, par exemple, le grand méchant loup qui attaque le Petit Chaperon Rouge et sa mère-grand, et est finalement tué par le chasseur ; ou le loup des Trois Petits Cochons, conte du XVIIIe siècle, qui souffle sur les maisons pour essayer de les tuer. - les fables de Jean de La Fontaine comme Le Loup et l'agneau, ou Le Loup et le chien. Attention, dans ces fables, le loup peut aussi être trompé (Le loup et la Cigogne), ou être pauvre, faible mais libre (Le loup et le chien).
- Beaucoup plus récemment, dans Le Livre de la Jungle, de Rudyard Kipling, l'enfant Mowgli est recueilli et élevé par une famille de loups.
- L'Œil du loup est un roman pour la jeunesse de Daniel Pennac
D'autres livres dont le personnage principal et un loup :









- Se jeter dans la gueule du loup : aller droit vers le danger. L'expression du loup est apparue au XVe siècle après J.C.
- Marcher à pas de loup : marcher sans faire de bruit.
- Marcher à la queue-leu-leu : cette expression, qui avec le temps s'est déformée, signifie « marcher comme les loups en meute, à la suite les uns des autres, chacun le museau dans la queue de son prédécesseur » (c'est ainsi que leurs traces se confondent et qu'il peuvent faire croire qu'ils sont moins nombreux qu'en réalité …).
- Être connu comme le loup blanc : être connu de tout le monde.
- Entre chien et loup : entre l'heure des chiens et celle des loups, entre jour et nuit.
- C'est là que le loup mange sa mère : désigne un endroit perdu, généralement très boisé et très sombre, donc effrayant.
- Mettre (ou faire entrer) le loup dans la bergerie : placer dans une structure une personne qui nuira de l'intérieur.
- L'Homme est un loup pour l'Homme : il n'y a pas plus grand danger pour l'humain que ses congénères.

- Hurler avec les loups : dénoncer quelqu'un en groupe pour éviter de prendre le risque de le faire seul.
- Quand on parle du loup (on en voit la queue) : se dit quand une personne dont on parle apparaît.
- Un jeune loup (aux dents longues) : désigne un ambitieux opportuniste.
