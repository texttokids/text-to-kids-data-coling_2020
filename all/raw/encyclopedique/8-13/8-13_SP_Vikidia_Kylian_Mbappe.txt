
Kylian Mbappé, de son nom complet Kylian Mbappé Lottin (parfois surnommé "le prince de Bondy", « Donatello » ou « Kiki »1) est un footballeur international français né le 20 décembre 1998 dans le 19e arrondissement de Paris. Il évolue au poste d'attaquant et actuellement au Paris Saint-Germain, qui l'a arraché à l'AS Monaco pour 180 millions d'euros, faisant de lui le deuxième joueur de football le plus cher de l'histoire après Neymar, signé contre 222 millions d'euros par le... Paris Saint-Germain. Les deux joueurs les plus onéreux de l'histoire évoluent donc ensemble en attaque. Il est considéré comme étant le meilleur buteur de Ligue 1 cette saison.
Il dispute son premier match de Ligue 1 en décembre 2015 sous les couleurs monégasques et inscrit son premier but deux mois plus tard, devenant le plus jeune buteur de l'histoire de la Ligue 1, devant Thierry Henry.
Il remporte ensuite la Coupe Gambardella avec l'AS Monaco U-19, puis le championnat d'Europe 2016 des moins de 19 ans sous le maillot de l'équipe de France. La saison suivante, Kylian Mbappé explose en devenant le plus jeune buteur de l'histoire des demi-finales de Ligue des champions2, le meilleur espoir de la Ligue 1 2016-2017 et champion de France avec l'AS Monaco.
Il remporte la Ligue 1 2017-2018, sa deuxième consécutive cette fois avec le Paris Saint-Germain devant... son ancien club, l'AS Monaco. Le 15 juillet 2018, il devient champion du monde avec la France, battant au passage de nouveaux records de précocité. Titularisé dans tous les matches des Bleus en Coupe du monde, il y inscrit quatre buts et est élu meilleur jeune de la compétition.
Les qualification de l'Euro 2020 on été dur pour Kylian Mbappe mais les bleus ont réussi à se qualifier pour cette Euro 2020.




Il naît d'une mère algérienne3, la handballeuse professionnelle Fayza Lamari, et d'un père camerounais, Wilfried Mbappé, qui a évolué comme joueur de football au niveau régional avant de devenir éducateur dans le club de l'AS Bondy, où a justement débuté son fils4.
Enfant vif et considéré comme surdoué dans la plupart de ses projets, il ne s'en montre pas moins difficile5. Il supplie déjà son père à 2 ou 3 ans de l'emmener avec lui au foot et de l'inscrire, et Wilfried finit par céder trois ans plus tard6.
Commence alors l'ascension fulgurante du prodige qui n'en finit déjà plus d'affoler son monde et d'être sans cesse classé dans les catégories supérieures, dans une tentative d'effacer les gigantesques différences avec les enfants de son âge. Peine perdue, malgré l'apparente fragilité physique de Mbappé, il continuait à faire la différence. Il est déjà considéré comme un crack à 11-12 ans, et ses matches en championnat départemental sont observés attentivement par plusieurs clubs.
Kylian débute à l'AS Monaco à 14 ans, deux ans après avoir rejoint le prestigieux Institut National de Football de Clairefontaine, malgré des négociations avancées avec le SM Caen et les tentations du RC Lens, des Girondins de Bordeaux, de Chelsea ou encore du Real Madrid.


Lors de sa première année avec le club monégasque, Kylian Mbappé vit dans un appartement avec son père, qui a pris une année sabbatique pour le soutenir dans ses débuts avec Monaco, alors tout juste promu en Ligue 1. Il est toutefois écarté à plusieurs reprises de l'équipe U17 de Monaco, son entraîneur Bruno Irles lui reprochant de ne pas participer suffisamment aux tâches défensives.
Cependant, des joueurs meilleurs arrivent : à 16 ans et 11 mois, l'équipe première monégasque lance Mbappé dans un match de Ligue 1 contre le SM Caen, le faisant devenir le plus jeune joueur de l'histoire de l'AS Monaco A, et deux mois plus tard, il inscrit son premier but en championnat national contre l'ESTAC Troyes7, battant deux records de précocité de Thierry Henry.


Maintenant habitué à jouer en équipe première, Kylian Mbappé est appelé chez les U19 pour disputer la Coupe Gamberdella 2015-2016, équivalent de la Coupe de France pour les moins de 19 ans. Le prodige de Bondy répond aux attentes et inscrit notamment un doublé contre le RC Lens dans une finale à sens unique, remportée 3-08.
Au terme d'une superbe saison 2016-100 en Ligue 1, où il inscrit quinze but0 et devient le plus jeune joueur de l'histoire de la Ligue 1 à atteindre ce chiffre, Kylian Mbappé reçoit le trophée UNFP du meilleur espoir de la Ligue 19. Il marque également ses deux premiers triplés, en huitièmes de finale de la Coupe de la Ligue contre le Stade rennais et en Ligue 1 contre le FC Metz.
Kylian Mbappé marque avec l'AS Monaco pas moins de six buts dans la Ligue des champions 2016-17, autant qu'Antoine Griezmann10, devenant à cette occasion le plus jeune quintuple buteur de la compétition et le deuxième plus jeune buteur français après Karim Benzema. Parmi ces buts, quatre sont marqués dans les quatre premiers matchs à élimination directe, ce qui est également historique11 : l'aller et le retour du huitième de finale contre Manchester City, et les deux quarts de finale contre le Borussia Dortmund.


Le 31 août 2017, Kylian Mbappé quitte officiellement Monaco pour rejoindre le Paris Saint-Germain, dans un premier temps sous forme de prêt avec option d'achat à 180 millions d'euros12 devenant obligatoire en cas de maintien du club parisien au sein de l'élite, ce qui est effectué après une victoire 5-2 contre le RC Strasbourg13 en février 2018. Mbappé y inscrit 13 buts et délivre 7 passes décisives en 27 matchs avec le PSG en Ligue 114, dont un de chacun dès son deuxième match par exemple, contre les Girondins de Bordeaux (6-2).
Il se montre une nouvelle fois décisif en Ligue des champions 2017-18 en inscrivant quatre des 25 buts parisiens en phase de groupes, un record15, et délivre quatre passes décisives en cinq matchs16, dont deux lors du match remporté par le PSG contre le Bayern Munich par trois buts à zéro17. Le 23 octobre 2017, Mbappé est élu Golden Boy 20179, c'est-à-dire qu'il a été désigné par le quotidien italien Tuttosport comme le meilleur joueur européen de moins de 21 ans. Lors de la saison 2018-2019 il  choisit d'évoluer avec le numéro 7 étant fan de Cristiano Ronaldo.




En 2016, alors qu'il n'a que 17 ans, Mbappé est intégré à l'équipe de France des moins de 19 ans, zappant les U18. Il évoque alors la « fantaisie de Neymar », son futur coéquipier au Paris SG. il aidera grandement les Bleuets à remporter l'Euro 2016 U19 en inscrivant cinq buts en autant de matchs, notamment un doublé contre les Pays-Bas18 dans un match couperet du Groupe B et surtout deux autres buts et une passe décisive en demi-finale contre le Portugal (3-1)19.


Le 25 mars 2017, lors d'un match de qualification pour la Coupe du monde 201820 contre le Luxembourg, remporté 3-121, Kylian Mbappé est titularisé pour la première fois en équipe de France, devenant le premier joueur né après la victoire française de 1998 à être sélectionné par cette nation, puis est à nouveau titularisé dans un match amical, perdu 0-2, contre l'Espagne. Il marque son premier but en Bleu le jour de son transfert au PSG lors d'un match d'éliminatoires de la Coupe du monde 2018 contre les Pays-Bas, contribuant à la large victoire française 4-022.
En mars 2018, face à la Russie en match amical de préparation pour la Coupe du monde 2018, Mbappé inscrit un doublé et permet à une équipe globalement médiocre de s'imposer 3-123. Pour son dernier match de préparation à la coupe du monde, contre les États-Unis, la France se montre inefficace et stérile et n'évite la défaite que grâce à Mbappé (1-1)24.
Après un terne premier match en phase de groupes face à l'Australie poussivement remporté 2-1, où Mbappé multiplia les mauvais choix25, ce dernier marque l'unique but Tricolore contre le Pérou et, à 19 ans et 183 jours, devient le neuvième plus jeune buteur d'une Coupe du monde26.
Après la qualification française issue d'un match sans histoire, si ce n'est l'interruption d'une série record de 37 matchs de Coupe avec au moins un but marqué27, contre le Danemark, Kylian Mbappé livre une prestation complète face à l'Argentine en huitième de finale, faisant totalement déjouer la défense argentine pour marquer un doublé28. Face à un Uruguay privé d'Edinson Cavani en quarts, les accélérations de Mbappé mettent encore à mal la défense adverse29, mais il n'inscrit pas de but et se démarque surtout en tombant sur un léger contact avec un uruguayen, ce qui provoque une embrouille générale entre les 22 joueurs30.
Face à la Belgique en demi-finale, Kylian Mbappé n'améliore pas ses statistiques mais n'en réalise pas moins une prestation remarquable, mettant au supplice les défenseurs belges avec un beau centre pour Olivier Giroud, qui a loupé son face-à-face avec Courtois, et un décalage pour Pavard lui aussi tenu en échec par le gardien belge31.
En finale, face à la Croatie, Mbappé place à nouveau des accélérations redoutables et distribue de bons ballons au reste de l'attaque française. Il centre sur Griezmann, qui trouve Paul Pogba qui marque le troisième but des Français. Enfin, Mbappé est servi aux seize mètres et, d'une frappe pleine de sang froid, marque le quatrième but bleu de la finale qui se termine sur le score de 4-232. Il se voit alors décerner le trophée du meilleur jeune de la Coupe du monde.


Kylian Mbappé est un attaquant excentré, qui peut éliminer ses vis-à-vis et semer la pagaille dans la moitié de terrain adverse33. Cet excellent passeur pratique un beau jeu balle au pied. Fortement loué pour sa vitesse, qu'il produit grâce à des mouvements similaires à ceux de coureurs professionnels, Mbappé est capable de trouver le bon timing contrairement à beaucoup d'autres joueurs rapides34. Il se montre également capable d'analyser rapidement son adversaire et de produire les appels adéquats pour le battre.
Mbappé est un bon dribbleur, capable de changer de rythme en pleine course ou à l'arrêt. Il fait généralement une feinte puis place une accélération pour prendre l'adversaire au dépourvu. Aimant jouer en passes courtes35, il est décrit comme un joueur complet, très rapide à l'exécution mais également doué techniquement, deux qualités qui ne sont réunies que par quelques autres comme le légendaire Brésilien Ronaldo. 
Mbappé peut s'adapter à beaucoup de situations sur le terrain, même qu'il n'a jamais vécues auparavant, mais aussi à l'équipe qui l'entoure, comme en témoigne une passe vers Edinson Cavani alors que ce dernier, Mbappé et Neymar étaient tous trois présents dans la surface du Bayern Munich. Quand ses coéquipiers sont plus individualistes, Mbappé veille à souder le collectif de l'équipe.
Kylian Mbappé est fréquemment loué pour son étonnante maturité et sens de l'auto-critique36. Ainsi, alors que la totalité de ses coéquipiers avaient sorti leur téléphone portable en célébrant le titre de l'AS Monaco en Ligue 1, Mbappé ne l'a pas fait, n'y voyant pas l'utilité et préférant vivre la fête telle qu'elle est. Capable d'utiliser une double personnalité de joueur professionnel mature et de gamin de banlieue, il célèbre sobrement son titre de champion du monde 2018, tenant notamment à ne pas s'arrêter à ce titre et à se remettre au travail pour glaner d'autres succès37.




Le tableau ci-dessous résume les statistiques de Kylian Mbappé en club professionnel mises à jour le 26 septembre 201814. 


Kylian Mbappé a disputé deux matches avec la France U17 contre l'Ukraine38 et marqué sept buts en onze sélections avec la France U1939. Voici un tableau résumant les statistiques de Mbappé en équipe de France senior d'après la Fédération française de football :




Le premier titre de Kylian Mbappé est la Coupe Gambardella 2016, remportée avec l'AS Monaco U1939. Il termine ensuite premier de la Ligue 1 2016-2017 avec les Monégasques, auteurs d'une saison record avec 95 points engrangés et 107 buts étalés sur 37 des 38 matchs40, et réalise également une belle aventure en Ligue des champions stoppée en demi-finales par la Juventus de Turin41.
Arrivé au Paris Saint-Germain, Mbappé étoffe son palmarès d'une deuxième Ligue 1, consécutive à celle remporte avec Monaco, mais aussi d'une Coupe de la Ligue et d'une Coupe de France.


En équipe de France des moins de 19 ans, Kylian Mbappé est sacré champion d'Europe en 2016. Il fait partie de l'équipe de France A qui remporte la Coupe du monde 2018 et totalise 22 sélections pour 8 buts avec cette équipe14.


Kylian Mbappé a déjà reçu de nombreuses distinctions individuelles :
- Meilleur espoir de la Ligue 1 2016-2017 et 2017-2018
- Joueur du mois de Ligue 1 en avril 2017, mars 201842 et août 2018.


- Quatrième Meilleur footballeur de l'année FIFA.


-Vainqueur du 1er trophée Kopa en 2018 du meilleur U21 
-Meilleur joueur français de l'année 2018 d'après France Football


- Plus jeune Français à disputer un match de Coupe du monde (19 ans et 5 mois)43
- Plus jeune Français à marquer en Coupe du monde (19 ans et 6 mois)
- Premier joueur né après la victoire de la France dans la Coupe du monde 1998 à être sélectionné par cette équipe
- Premier joueur né après la victoire de la France en 1998 à marquer en Coupe du monde
- Deuxième plus jeune joueur à marquer quatre buts en Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Deuxième plus jeune joueur à marquer un doublé en Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Joueur Français ayant tenté le plus de dribbles en un match de Coupe du monde depuis 1966 (15, sept réussis)
- Deuxième plus jeune buteur d'une finale de Coupe du monde (19 ans et 6 mois), derrière Pelé (17 ans et 8 mois)
- Deuxième plus jeune buteur Français en Ligue des champions44 (18 ans et 5 mois)
- Deuxième joueur de football le plus cher de l'histoire (180 millions d'euros)
- Plus jeune joueur à marquer dix buts en Ligue des champions (18 ans et 11 mois)
- Plus jeune buteur de l'histoire des demi-finales de Ligue des champions45 (18 ans et 4 mois)
- Plus jeune joueur à marquer un but de chaque pied en quart de finale de Ligue des champions (18 ans et 3 mois)
- Plus jeune buteur du PSG en Ligue des champions (18 ans et 8 mois)
- Deuxième plus jeune buteur du PSG en Ligue 1 (18 ans et 8 mois)
- Plus jeune buteur pour deux clubs différents en Ligue des champions (18 ans et 8 mois)


Kylian Mbappé est un personnage jouable dans le jeu vidéo FIFA 18, dans le mode de jeu Ultimate Team46. En 2012, alors qu'il n'est âgé que de 14 ans, Mbappé signe son premier contrat et touche une prime de 400 000 euros47. Lors de sa saison 2016-2017 avec l'AS Monaco, le salaire annuel de Kylian Mbappé était d'environ 1 million d'euros48. Avec le PSG, ces revenus explosent pour atteindre une somme vertigineuse de 17,5 millions d'euros gagnés sur la saison 2017-201849. Malgré de nombreuses propositions, Kylian Mbappé n'a choisi qu'un unique sponsor : son équipementier Nike.
