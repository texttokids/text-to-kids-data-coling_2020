
Quand Vidocq entre dans la police, en 1810, les rues de Paris sont de vrais coupe-gorge. Mais il connaît cet univers pour l'avoir fréquenté. Cela fera de lui un policier pas comme les autres!

La police se modernise Après Vidocq, le criminologue Alphonse Bertillon (1853-1914) a développé des techniques scientifiques qui ont fait progresser police.
* 1882 : identification des criminels par leurs mensurations (mesures de la tête, de l'oreille droite...). On commence à recueillir les empreintes digitales.
* 1888 : les photographies de face et de profil se développent. Autre avancée, au XXe siècle cette fois ci, l'analyse de l'empreinte génétique par l'ADN (acide désoxyribonucléique), unique pour chaque individu.
Au début du XIXe siècle, avec l'essor de l'industrie, les paysans partent travailler en ville. La vie y est dure et la misère favorise les crimes et les délits. On dénombre alors à Paris 2000 voleurs et autant d'anciens bagnards. Après un an de collaboration avec la police, Vidocq obtient carte blanche pour créer son service, la brigade de sûreté, et le gérer à sa manière.
Il va donc recruter d'anciens forçats qui ont connu "de l'intérieur" le monde du crime. Mais, attention, Vidocq affiche un règlement dans ses bureaux: interdiction de boire de l'alcool à l'excès, de jouer aux jeux d'argent... Ses succès lui permettront d'élargir son service : en un an, son équipe de 12 agents procède
à 800 arrestations, dont 5 meurtriers et 108 cambrioleurs. A son départ, en 1827, la brigade de sûreté compte presque 30 agents ! Vidocq crée un solide réseau d'indicateurs, des personnes qui le renseignent contre de l'argent. Déguisements, filatures, infiltrations des milieux louches, les méthodes de la " Rousse " (le surnom de la bande de Vidocq) font mouche. Pour les historiens, Vidocq a créé les bases de la police moderne!

