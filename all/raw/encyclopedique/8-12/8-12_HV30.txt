Un empire au cinéma
Qu'ont en commun Mickey, Nemo, Captain America et Dark Vador? Ils sont tous membres de la grande famille des héros Disney.
Marvel, la renaissance des super-héros C'est peut-être l'une des décisions les plus rentables de Disney. En achetant tous les droits d'adaptation
de l'univers Marvel, les studios ont mis la main sur une véritable poule aux oeufs d'or. Captain America, Avengers, Les Gardiens de la galaxie... ces films de super-héros ont été d'énormes
succès au cinéma et ont élargi le public
Prochains rendez-vous : de Disney avec de nouveaux spectateurs :
Les Gardiens de la galaxie 2, en avril 2017 les amateurs d'action.
Walt Disney Pictures, la magie des contes Si les films d'animation Disney empruntent aujourd'hui à tous les genres (Les supers-héros avec Les nouveaux héros, l'action avec Zootopie), les contes de fées restent leur marque de fabrique. Ils continuent de faire rêver, comme le montre le succès de La Reine des neiges
en 2013, tiré d'un conte d'Andersen, et qui aura sa suite fin 2017. Disney mise aussi sur l'adaptation en films de ses dessins animés avec de vrais acteurs, comme Cendrillon et Le Livre de la jungle.
Pixar, le petit grain de folie
Entre Disney et les studios Pixar, l'histoire démarre en 1995 avec Toy Story : le premier film réalisé par ordinateur est une collaboration entre les deux studios. En 2006, Disney a racheté les studios Pixar qui ont produit bon nombre de chefs-d'oeuvre : Le Monde de Nemo et Le Monde de Dory, Ratatouille (ci-contre), Là-haut, Rebelle et Vice-versa. La spécialité de Pixar? Des films d'animation modernes avec des héros étonnants : des poissons, un rat, ou encore un papi!
Star Wars, une nouvelle génération Cette saga culte plaît aux enfants, succès de l'épisode VII : Le réveil aux parents et même aux papys et de la force. Une nouvelle génération mamies! Normal, le premier épisode - celle des enfants de ton âge - est sorti en 1977. Six films existaient découvre à son tour cette galaxie déjà lorsque Disney a racheté les très, très lointaine! studios de George Lucas, le créateur de star wars. 

