Léa adore passer son dimanche chez sa grand-mère.
Lorsqu'elle arrive dans le joli petit pavillon à la campagne, sa première visite est pour le potager au fond du jardin.
Là, cachée au milieu de plants de tomates, des salades et des jolies fleurs de courgettes, elle prend son petit râteau, sa pelle et enfile les jolis gants en coton fleuris que lui a offert son grand-père. Puis elle ratisse, retourne la terre, plante les graines qu'elle a apportées dans sa poche en suivant sagement les conseils de papi et mamie.
Souvent, elle travaille sous l'oeil attendri de ses grands-parents et sait que ce soir, quand papa et maman viendront la chercher, elle sera toute remplie de terre mais aussi du bonheur d'avoir bien joué.
Un peu avant de déguster la bonne tartine de confiture faite par sa mamie avec les fraises de l'an passé, elle va remplir son petit arrosoir joliment décoré et arrose ses plantations.
Elle sait que dans quelques temps, peut-être même le prochain week-end, si le soleil a bien voulu réchauffer la terre, elle verra des bébé-pousses bien vertes qui grandiront au fil des jours jusqu'à devenir un beau pied de basilic.
Bien plus tard, un autre dimanche, elle cueillera les feuilles de "son" basilic et, très fière, elle demandera à sa maman en rentrant chez elle de préparer une succulente sauce "pesto" pour assaisonner les pâtes.
Pour l'occasion, cette fois, ce sont papi et mamie qui viendront chez Léa et ses parents pour déjeuner. Mamie n'aura pas oublié d'apporter une bonne tarte faite avec la rhubarbe de son potager.
