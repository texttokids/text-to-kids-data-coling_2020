Pourquoi je ne dois pas avoir peur du noir ?
J’ai peur des monstres cachés sous mon lit.
J’ai peur qu’on vienne me chatouiller les pieds.
Ou qu’on vienne me voler mon Doudou.
J'ai peur de me réveiller dans la nuit et de ne rien voir.
J’ai peur de faire des cauchemars !
Tu ne dois pas avoir peur du noir. Il n’y a pas de danger dans ta chambre.
Je vais mettre une petite lumière près de toi, ainsi quand tu auras peur, tu pourras la regarder, et tu sauras que tout va bien.
Et si j’ai peur cette nuit, tu seras là Maman ? - Oui, tu pourras m’appeler et je viendrai te faire un câlin.
Bonne nuit Maman.
