Que fabrique le père Noël lorsqu’il n’est pas en train de préparer les jouets ou de les distribuer? Il a un grand nombre d’amis qui vivent près de sa maison au pôle Nord. Les bonhommes de neige, avec lesquels il invente plein de jeux.
Il y a aussi ses rennes, pour lesquels il fait des gâteaux à la châtaigne .
Il adore les films et regarde souvent ses cassettes préférées durant les tempêtes de neige
Le matin de Noël, il prend une bonne douche. C’est important pour sentir bon quand même !
Puis il prend un solide petit-déjeuner, mais il fait attention à ce que son repas soit bien équilibré.
Il essaye une dernière fois tous les jouets, pour être sûr qu’ils marchent bien. Un enfant ne doit pas être déçu par son cadeau.
Et c’est parti pour le grand soir ! Santa ou le père Noël est lancé pour la distribution annuel de cadeaux à tous les petits enfants du monde.
