A 7 ans, Bruno est un petit garçon très sûr de lui. Enfin, sûr de ce qu’il aime faire. Aller à l’école n’est vraiment pas son activité préférée.
Ce que préfère Bruno, c’est faire le pitre, raconter des blagues et écouter les autres. Un jour, en discutant avec ses copains, une idée lumineuse traverse l’esprit de Bruno.
« Hé les gars ! », dit-il, "Si on faisait une émission de radio en musique ?" « Mais on ne sait pas jouer de la musique", répondent ses amis, très étonnés.
"Attendez, je vous explique. Pipo, tu prends les barils qui nous servent de cibles pour jouer au foot. On va les utiliser pour créer le rythme. Tu pourras taper avec tes pieds, tes mains. Vas-y mon Pipo, joue !
Pipo se met à battre la mesure. Les copains l’imitent, Eric prend les plots d’entraînement et se met à chanter. Sa voix résonne et porte comme avec un micro. Chacun prend un objet qui lui sert d’instrument. Leurs amis Manu et Camille chantonnent. Tandis que le petit Jonathan entame un pas de danse.
Bruno prend à son tour un plot et commence à jouer les chefs d’orchestre. L’équipe s’amuse bien, tout le monde rit. Mais Bruno trouve quand même que l’organisation est difficile à mettre en place.
Un jour de cours de théâtre, Bruno se dit que ça serait quand même plus simple de faire danser les gens. Il propose alors au professeur, Delphine, d’animer un stand. « Je vais prendre les enceintes de la salle, et je vais mettre de l’animation. Vous pourrez passer vos annonces, écouter de la musique… » « Quelle bonne idée !", lui répond la maîtresse, "Comme à la radio. »
« Oui, on dira Bruno dans la radio » répond-il. "Je vais prendre mon lecteur de musique et on va s’éclater."
Le jour de la kermesse, tout le monde est impressionné par ce petit garçon si vif. Très vite, il est rejoint par ses copains, Manu, Camille et Jonathan. Les gens dansent et rient avec les petites têtes.
La fin de l’après-midi arrive et tous les professeurs vont féliciter le petit Bruno. Christopher, Jacques, Gérald, Rémy, David et Delphine offrent pour l’occasion un micro à Bruno.
"Merci, merci !", dit le petit garçon tout ému. "Un jour, je serais peut-être l’homme qui passera les plus grands tubes à la radio !" s’écrie-t-il.
