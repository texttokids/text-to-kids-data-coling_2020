Dans ma forêt, je regarde les flocons tomber.
Je longe la lisière à la recherche de quelques baies.
À l'abri dans les sous-bois, je déguste des glands et des champignons.
Puis, je cherche un lit bien chaud.
La nuit est tombée, je vais me coucher !
