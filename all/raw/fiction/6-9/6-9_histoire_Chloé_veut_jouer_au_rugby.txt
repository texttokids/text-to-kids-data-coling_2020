Depuis toute petite, Chloé est une petite casse-cou. Pour le plus grand malheur de sa maman, elle passe son temps à grimper aux arbres et à se rouler dans l’herbe, en jouant à la bagarre avec ses copains de classe.
Ses parents ont tout essayé: Le judo, l’équitation, la natation, mais rien ne suffisait à canaliser toute son énergie.
Mais il y a quelques mois, avec l’école, Chloé a découvert le rugby. Depuis, elle passe son temps à en parler.
A l’école au début, ses copains lui disaient : « Mais le rugby c’est un sport de garçon ! » Mais Chloé leur répondait : « Si les garçons peuvent faire de la danse classique pourquoi je ne pourrais pas faire du rugby !? »
Après de longues négociations, le papa de Chloé a réussi sa convaincre sa maman de l’inscrire dans un club. Depuis, elle s’entraine tous les mercredis après-midi et fait même des matchs le weekend avec son équipe.
Si le Papa de la petite fille est ravi, sa maman, en revanche, a très peur. Mais Chloé, elle, elle n’a pas peur de rien! Parfois elle a des petits bobos, mais comme elle dit toujours : « Un bisou de Maman et je n’ai plus mal! »
Et maintenant avec son Papa, ils ne loupent plus aucun match ! Chloé est une grande supportrice l’ASM de Clermont-Ferrand alors que son papa supporte le Stade Toulousain. Quand les deux équipes jouent, dans le salon, il y a beaucoup d’ambiance !
La maman de Chloé, elle aussi, s’est mise à regarder les matchs de Clermont-Ferrand. Mais La petite fille et son papa la suspectent de ne les regarder que pour apercevoir Wesley Fofana !
