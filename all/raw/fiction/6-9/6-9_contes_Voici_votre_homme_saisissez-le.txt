On raconte qu’un homme vint à Sulaymân fils de Dâoud et lui confia:
-« Ô Messager de Dieu, j’ai des voisins qui me volent mes oies. »
Sulaymân appela alors tous les fidèles à la prière ; ensuite il les exhorta, disant :
-« Ô croyants, l’un d’entre vous vole les oies de son voisin, puis il entre dans ce lieu de recueillement avec les plumes sur la tête. »
Un homme porta sa main à sa tête ; Sulaymân dit alors :
-« Voici votre homme, saisissez-le. ».
