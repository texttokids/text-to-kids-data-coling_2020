Un jour, alors que Julian se baladait dans la forêt avec ses parents, il se perdit et se retrouva seul.
Alerté par des pleurs, Jude le petit monstre s’approcha de Julian pour l’aider.
Julian avait un peu peur de Jude. Pour lui prouver sa bonne foi, Jude le petit monstre lui offrit la clé de son royaume. - J’aimerais bien visiter ton monde, et si on y allait ensemble ? dit Julian avec beaucoup d’entrain. Jude le petit monstre n’avait jamais eu d’amis et vivait seul dans son royaume, il était alors vraiment très heureux que Julian vienne avec lui.
- Mets la clé dans le trou de cet arbre pour entrer dans mon royaume. dit le petit monstre. Julian s’exécuta.
Une porte s’ouvrit dans l’arbre, et ils entrèrent tous les deux. Julian se crut en plein rêve. Le royaume de Jude était haut en couleurs, plein de sucreries et de jouets merveilleux.
Julian et Jude s’amusèrent comme des fous toute la journée, d’abord dans un parc d’attractions.
Puis dans une cascade d’eau.
En faisant du trampoline et plein d’autres activités.
Mais à la fin de la journée Julian était de nouveau triste car il voulait retourner chez lui auprès de sa famille. Touché par le chagrin de son nouvel ami, Jude décida de l’aider à retourner chez lui.
Après quelques heures de marche, Julian et Jude finirent par retrouver la maison.
Julian était heureux, il allait revoir ses parents. Mais Jude était triste, encore une fois il allait se retrouver seul dans son royaume, sans aucun ami.
Julian, triste de se séparer de son ami, lui proposa alors de rester chez lui pour toujours.
