
La forêt s’éveillait paisiblement pour saluer le matin naissant, et le lapin flânait sur les berges de la rivière, émerveillé par les fleurs qui l’entouraient.
Il sautillait et gambadait lorsque, tout à coup, un énorme bruit retentit : «Plouf!»		
Terrifié, le petit lapin détala en criant à tue-tête : « Au secours, voilà le Plouf !»		
Dans sa course, il croisa un renard.		
Pourquoi cours-tu ainsi?		
Le lapin, sans s’arrêter, répondit entre deux bonds :	
Sauve-toi vite,	voilà le Plouf !		
Le renard, pris de peur, déguerpit en hurlant avec lui :		
Sauvons-nous, voilà le Plouf !		
Dans leur fuite, le lapin et le	renard rencontrèrent un singe.		
Pourquoi courez-vous de la sorte ?		
Le lapin et le renard crièrent en poursuivant leur course :		
Sauve-toi vite,	voilà le Plouf !		
Affolé,	le singe partit	avec eux.		
Le lapin, le renard et le singe bousculèrent ensuite un zèbre.	

Pourquoi courez-vous de cette façon ?		
Les fuyards, sans même s’arrêter, crièrent en chœur :			
Sauve-toi vite,	voilà le Plouf	!	
Effaré,	le zèbre se mit	à courir lui aussi, en répétant comme les autres :		
Sauvons-nous, voilà le Plouf !		
Dans leur déroute, le lapin, le renard,	le singe et le zèbre percutèrent un éléphant.	
Pourquoi courez-vous de	cette manière ?		
Les quatre peureux répliquèrent	d’une seule voix :			
Sauve-toi vite,	voilà le Plouf !		
L’éléphant, pris de panique, s’enfuit à	son tour derrière les autres.		
Se joignirent à	eux la girafe, l’ours et la panthère.		
Puis le daim, l’hippopotame, le	lion...		
Tous suivaient le lapin	en hurlant :	
Sauvons-nous, voilà le Plouf !		
Soudain, ils tombèrent nez à nez avec le tigre,	qui leur barra la route.		
Que se passe-t-il ?
Qu’avez-vous donc à bondir dans tous les sens ?		
Le Plouf arrive, sauve-toi vite	!
répondit alors le lion,	haletant.		
Le Plouf ?
Mais qui est-ce	?	
Je l’ignore, dit le lion, c’est l’hippopotame qui m’a mis en garde.
Moi?
Je ne l’ai jamais vu, c’est la girafe qui m’en a parlé !		
Le tigre, intrigué, questionna les animaux un à un, jusqu’au lapin qui raconta tout tremblant sa mésaventure :			
J’ai entendu le Plouf au bord de la rivière, c’était absolument	terrifiant !	
Nous ferions mieux de voir à quoi il ressemble, proposa alors le tigre, avant de décider si nous devons fuir à son approche.		
Le lapin guida alors les animaux jusqu’à la berge où il avait entendu l’épouvantable Plouf.
Au même instant, une papaye tomba dans l’eau :
«Plouf!»
