
Chris	était	astronaute.	Un	astronaute	important	et	très	occupé.	
	
Quand	arrivait	l’heure	du	bain,	il	disait	à	sa	mère	:	
«	J’aimerais	bien,	mais	je	dois	sauver	la	planète	des	extraterrestres.	»	
	
Quand	arrivait	l’heure	de	sortir	du	bain	et	d’aller	se	coucher,	il	disait	à	son	père		-	
poliment,	parce	que	les	astronautes	sont	toujours	polis	:		
«	Désolé,	impossible,	je	suis	en	route	pour	Mars.	»	
	
Le	travail	d’un	astronaute	n’est	jamais	fini,	alors	les	astronautes	n’aiment	pas	
dormir.	
	
Mais	leurs	parents,	si.	
	
«	Tu	es	un	grand	garçon,	maintenant,	disait	le	père	de	Chris.	Tu	dois	dormir	dans	
ton	lit.	»	
	
Et	Chris	essayait,	il	essayait	vraiment,	mais	sa	chambre	était	noire.	Tellement,	
tellement	noire.	
	
Le	genre	de	noir	qui	attire	les	pires	extraterrestres.	
	
Mais	ses	parents	ne	plaisantaient	pas.	
Chris.	Allait.	Dormir.	Dans.	Son.	Lit.	Cette	nuit.	
	
Sa	mère	et	son	père	inspectèrent	le	dessous	de	son	lit,	dans	sa	commode,	et	
même	dans	ses	tiroirs.	Ils	déclarèrent	qu’il	n’y	avait	aucun	extraterrestre	dans	sa	
chambre.		
	
Ils	bordèrent	Chris	dans	son	lit.	Ils	allumèrent	la	veilleuse.	Ils	lui	donnèrent	
même	une	petite	clochette	pour	le	cas	où	il	aurait	peur.		
	
Ils	confisquèrent	la	clochette.	
	
Puis	son	père	dit	quelque	chose	qui	inquiéta	Chris	encore	plus	que	le	noir	de	la	
nuit	:	«	Encore	un	bruit,	jeune	homme,	et	on	sera	tous	trop	fatigués	pour	aller	
chez	les	voisins	demain.	»	Or	demain	était	un	jour	spécial.	Un	jour	très	spécial.	
Chris	devait	absolument	aller	chez	les	voisins.	Sa	vie	en	dépendait.		
	

Alors	Chris	resta	dans	son	lit.	Sans	un	bruit.	Il	mit	longtemps	à	s’endormir	mais,	
quand	il	y	parvint	enfin,	il	fit	son	rêve	favori…	
	
Il	pilotait	sa	fusée	jusqu’à	la	Lune.	
	
Le	jour	suivant	semblait	sans	fin.	Alors	que	la	Lune	brillait	au-dessus	du	lac	et	que	
le	vent	d’été	caressait	les	feuilles	des	arbres,	Chris	put	courir	chez	les	voisins.		
La	maison	était	déjà	pleine	de	monde,	attroupé	autour	de	la	télévision	–	la	seule	
et	unique	télévision	de	toute	l’île.			
Chris	se	trouva	une	place	d’où	il	pouvait	voir	quelque	chose.	Et	ce	qu’il	vit,	
c’était...		
	
Des	astronautes.	De	véritables	astronautes.	En	direct	de	la	lointaine	Lune.	Ils	
portaient	de	grosses	tenues	blanches	et	ils	sautaient	de	joie	haut,	très	haut,	du	fait	
de	la	faible	gravité.		
Les	adultes	regroupés	devant	la	télévision	étaient	ébahis.	De	toute	leur	vie,	ils	
n’avaient	jamais	imaginé	voir	une	chose	pareille.	Même	Chris	(qui	était	
pourtant	allé	sur	la	Lune	la	nuit	d’avant)	était	ébahi.	Il	n’avait	jamais	vraiment	
remarqué	à	quel	point	il	faisait	noir	tout	là-bas.		
	
L’espace	était	d’un	noir	absolu.		
	
Cette	nuit-là,	Chris	fit	une	petite	expérience.	Il	éteignit	toutes	les	lumières	de	sa	
chambre,	 même	 la	 veilleuse.	 Il	 faisait	 toujours	 noir.	 Très,	 très	 noir.	 Il	 y	 avait	
toujours	 des	 ombres	 qui	 avaient	 l’air	 un	 petit	 peu,	 disons,	 extraterrestres.	 Rien	
n’avait	changé.		
Mais	Chris	avait	changé.		
	
Il	 avait	 vu	 combien	 le	 noir	 de	 l’univers	était	 plus	 profond	 que	 le	 noir	 de	 sa	
chambre,	mais	il	n’avait	plus	peur.	Il	voulait	explorer	chaque	recoin	du	ciel.		
	
Pour	 la	 première	 fois,	 Chris	 comprenait	 la	 puissance,	 le	 mystère	 et	 la	 beauté	
veloutée	du	noir	de	la	nuit.	
	
Il	 réalisa	 qu’on	 n’y	 était	 jamais	 vraiment	 seul.	 Que	 nos	 rêves	 nous	
accompagnaient	partout.	De	grands	rêves,	à	propos	de	qui	on	voudrait	être.		
	
Des	rêves	merveilleux	à	propos	de	la	vie	qu’on	aura.		
	
Des	rêves	qui	deviennent	réalité.	

	

	
	
	
	


