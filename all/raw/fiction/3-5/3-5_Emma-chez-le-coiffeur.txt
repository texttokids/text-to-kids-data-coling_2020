Emma chez le coiffeur


Emma dessine ses deux grand-mères : l’une est petite et ronde et l’autre est grande et ronde. L’une est blonde et l’autre est brune. Emma est déjà allée chez le coiffeur avec chacune. L’une s’appelle Mamie et l’autre Grand-mère.
Avant d’y aller, les grand-mères ont les cheveux plutôt blancs. On applique un mélange gluant sur la tête et on attend. Emma adore aller chez le coiffeur parce qu’il a une fausse tête avec une perruque et Emma a le droit de la coiffer pendant que l’une ou l’autre grand-mère devient blonde ou brune.
Le mercredi, Emma passe la journée avec Mamie. 
« Tes cheveux sont trop longs ! » dit-elle à Emma. « On va aller toutes les deux chez le coiffeur. » Emma ne veut pas qu’on lui coiffe, c’est elle qui veut coiffer. Mais mamie l’attend et elles y vont.
Le coiffeur est de bonne humeur. « On va prendre cette grande demoiselle d’abord ! » dit-il à mamie. Il met deux annuaires téléphoniques sur le siège et Emma monte dessus.
Quand elle voit les ciseaux dans la main du coiffeur, elle couvre sa tête de ses mains pour se protéger.
« Mais non, » dit-il, « N’aies pas peur. Ça ne fait pas mal. Je ne vais pas couper beaucoup ! » Emma remet ses mains sur les genoux comme une parfaite petite fille sage.
Le coiffeur coupe en sifflant « petit papa Noël ». Emma voit tomber des mèches par terre. Le coiffeur coupe et chante, chante et coupe. Les ciseaux dansent à travers son crâne avec des clics et des clacs. Clic, clac et clip, clop.
Le coiffeur dit « J’adore couper les cheveux. » Emma pense qu’il exagère, qu’il s’emporte, qu’il coupe trop. Elle regarde Mamie. « Ça va les alléger ! » dit Mamie. Puis elle ajoute « Ça repousse ! »
Emma lève la tête et se regarde. Elle ne connaît pas la fille dans le miroir. Cette fille est terriblement moche !
« T’en fais pas ! Ça repousse ! » répète Mamie. Mais qu’est-ce qu’on fait jusqu’au jour où ça repousse, pense Emma. Elle veut se cacher dans les W.C. pendant trois mois ! 
« C’est fini ! » déclare le coiffeur.
Emma le hait ! Mais elle est si bien élevée qu’elle dit « merci ». Merci de m’avoir scalpé. Merci de m’avoir complètement abîmée ! Merci beaucoup !
« Qu’est-ce qu’on fait aujourd’hui ? » demande-t-il à sa Mamie, « une couleur et une coupe ? »
« Uhhhhh, non, pas de coupe aujourd’hui ! » Sa Mamie a compris que ce n’était pas le jour pour les coupes.
Pendant que Mamie reste coincée avec le mélange affreux sur la tête, Emma coiffe la perruque. Oh comme c’est bien les longs cheveux. Elle y met des barrettes et des nœuds. Elle lui met son serre-tête qui ne lui sert plus à rien.
Et puis elle a une idée. Elle enlève tous les accessoires de la perruque. Elle se glisse vers le tiroir du coiffeur et retire tout doucement l’objet. Mamie somnole sous son mélange. Le coiffeur fait un brushing à une autre dame. Ça fait un bruit d’enfer.
Et Emma va tout doucement et clip et clap et clic et clac. Elle coupe tous les cheveux de la pauvre perruque. Elle coupe et elle coupe. Les mèches tombent en pluie douce, des petits chuchotements.
Le coiffeur finit de coiffer Mamie. Ils regardent tous les deux la perruque.
« Mon Dieu ! » dit Mamie.
Le coiffeur dit un gros, un très gros mot !
Et Emma répond à leurs expressions d’horreur : « Ne vous en faites pas ! Ça va repousser ! »

