Emma et la Fête des mères


La maîtresse d’Emma a dit qu’ils pouvaient choisir l’objet à peindre pour leur mère. Il y a des cendriers, des porte-clefs, des cocotiers, des boîtes de Kleenex, des presse-papiers, des porte-crayons, des ronds de serviettes et des vases.

Emma est embêtée. Sa maman ne fume pas, elle a toutes ses clefs dans son sac à main, ne mange jamais d’œuf à la coque, préfère les mouchoirs en tissu, a déjà un porte-crayon et n’aime pas les fleurs coupées. Elle donc choisi le rond de serviette bien qu’ils n’en utilisent pas à la maison. Antonin choisit le vase en disant « Je vais aussi cueillir des fleurs et ça me fera DEUX cadeaux. Emma est un peu jalouse. Elle aime beaucoup les fleurs, mais sa mère a horreur quand elles se fanent et qu’il faut les jeter.

Elle décide de peindre cinq cœurs sur son rond de serviette, mais après ses deux premiers cœurs, il n’y a la place que pour une moitié de cœur en plus.  Du coin de l’œil, elle voit qu’Antonin a eu la place pour dix cœurs. Elle en veut à sa mère de ne pas aimer les fleurs.

Les objets en bois sont abandonnés sur le papier journal pour sécher pendant quelques jours avant d’appliquer le vernis. Puis ils se reposent de nouveau. Oubliés.

Le vendredi avant la Fête des mères, ils font des paquets cadeaux. Emma pense que son paquet est plus joli que ce qu’il contient. Quand sa mère vient la chercher Emma meurt d’envie de lui offrir son cadeau tout de suite. 

- Tiens Maman. Bonne fête ! 
- Mais Emma … il faut attendre dimanche.
Emma trépigne d’impatience. En plus elle ne veut plus tenir ce paquet.
- Je veux te le donner tout de suite !
- On donne le cadeau de la fête des mères le jour de la fête des mères. C’est comme les anniversaires. On ne donne jamais le cadeau avant.
- C’est moi qui l’ai fait et je veux te le donner tout de suite.
- Tu vas en parler avec ton père.

Le papa d’Emma rentre du travail fatigué et distrait. Il entend à peine ce qu’elle lui demande. Pour en finir avec elle, il dit « Oui, oui, vas-y !

Emma triomphe ! Elle porte le cadeau au salon et proclame « Bonne fête Maman. »

Maman n’a plus la force. Elle ouvre le cadeau et voit la moitié du cœur. Elle se lève et enfile une serviette en papier dans le rond. 
- J’espère que tu en feras un autre pour la fête des pères. Ça fera un couple ! Merci.

Emma sait que sa mère n’est pas ravie du cadeau.

- Tu ne l’aimes pas, Maman ?
- Oui, mais maintenant je n’aurai pas de cadeau le jour de la fête des mères.

Emma se rend compte qu’elle a fait une bêtise. Maintenant qu’elle a remis le cadeau, elle n’a plus de cadeau !

Elle se met à réfléchir. Elle tourne en rond. Elle aimerait trouver une idée d’un nouveau cadeau.

Dimanche, alors que sa mère n’attend plus rien, Emma saute dans le lit de ses parents.

- Bonne fête Maman !  J’ai un cadeau pour toi !
- Quoi ?
- MOI !
- C’est le plus beau cadeau Emma. Merci ! Sans toi, je ne serais pas maman !
Une maman est maman tous les jours et pas seulement le jour de la fête des mères.
 


