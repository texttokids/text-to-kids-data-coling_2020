On raconte que quand la souris du logis vit la souris du désert dans la gêne et la peine ; elle lui dit :
« Que fais-tu ici ? viens avec moi au logis car il y a toutes sortes d’opulence et d’abondance ». Alors la souris du désert vint avec elle.
Mais voici que le propriétaire du logis qu’elle habitait lui tendit un piège, constitué par une brique au-dessous de laquelle il avait placé un bout de graisse.
Elle se précipita pour prendre le gras, la brique lui tomba dessus et l’écrasa.
La souris du désert s’enfuit, hochant la tête et, étonnée, elle dit : -« Certes, je vois une grande abondance, mais aussi une grande affliction ; par conséquent, la santé avec la pauvreté me sont plus douces que la richesse qui conduit à ma perte. »
Puis elle s’enfuit vers le désert.
