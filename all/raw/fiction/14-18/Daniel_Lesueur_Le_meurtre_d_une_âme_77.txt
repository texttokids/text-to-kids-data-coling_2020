
Finirait-elle, cette nuit-là, pour la pauvre fille?... Qui sait?...

Cigale imprudente, qui chantait et stridulait dans l'ornière du mauvais chemin, sans entendre grincer, pour l'écraser dans l'ombre, les lourdes roues de la destinée.
-«Figure-toi, Mimi,» disait-elle, «voilà que j'ai faim, maintenant! Tu m'as empêchée de dîner, vilain! Mais ça m'ennuie de retourner à table.

On va faire apporter ici quelque chose de bon. Qu'en dis-tu? Du poulet froid, des gâteaux, du champagne. Je vais envoyer acheter un pâté, là, tout près. Il y a une spécialité épatante!» 

Le souper fut vite organisé. La femme de chambre et le domestique apportèrent une petite table volante toute servie.
-«Vous n'avez rien oublié?» dit Lina en parcourant d'un coup d'œil les deux couverts, les friandises, le champagne dans le seau à glace.-«Non?... Eh bien, c'est bon. Je vous donne campos. Remontez dans vos chambres, ou allez au diable! Je ne veux plus vous entendre tourner dans l'appartement, ni avoir vos oreilles au trou de la serrure.

Bonsoir!» 

Comme ils s'empressaient de déguerpir, elle les rappela.
-«Fermez bien les portes, au moins. Je n'ai pas envie d'être cambriolée cette nuit.» 

Quelques heures plus tard, le silence le plus absolu régnait dans l'appartement de la demi-mondaine. Almado et elle se reposaient de leur orageuse soirée. Et ce n'était pas un repos sans cauchemars vagues ni sursauts nerveux, car leur tendresse, succédant aux reproches et à la violence, avait été trouble, fiévreuse, secrètement amère.

Lina de Cardeville s'éveilla. Ses yeux s'enfoncèrent dans la pénombre de la chambre. L'ameublement luxueux, plus lourd, moins fanfreluché que dans le cabinet de toilette, chatoyait en teintes douces dans une lumière assourdie. Toute la nuit l'électricité brûlait en veilleuse, tamisée par les gros pétales translucides d'une fabuleuse fleur de verre, à la rosace du plafond.

Celle qui jouissait de ces raffinements, qui promenait ses regards parmi ces choses coûteuses, se représenta tout à coup une misérable demeure dans la cour de l'Épée-de-Bois. C'étaient deux chambres au rez-de-chaussée d'une chancelante bicoque. Quatre femmes y vivaient: sa grand'mère, sa mère, ses deux sœurs. Le logis ne contenait que bien juste les objets indispensables, car le moindre signe d'aisance attirait la convoitise du père, vieil ouvrier noceur, qui, de par son droit de maître légal, raflait aussitôt ce qui pouvait se vendre et se boire. Mais, si toute beauté extérieure était absente de ce pauvre asile, la beauté des âmes y fleurissait, sublime. C'était la résignation et la patience laborieuse de l'aïeule qui, après s'être rendue aussi utile qu'elle le pouvait dans la famille, trouvait le temps de tricoter des bas pour les petits miséreux de la cité. C'était la vaillance de la mère, se rendant chaque jour à un lointain labeur maigrement rétribué. C'était la gaieté méritoire de la sœur infirme, fauvette chanteuse, dont les roulades mettaient de la joie au cœur du rude voisinage, tandis que ses doigts agiles exerçaient un ingrat et fastidieux travail. C'était la vertu douloureuse de l'autre sœur, prête à écraser en elle-même son chaste amour plutôt que de faillir ou de mettre en lutte avec un père inexorable celui qu'elle aimait. Avec quelle vivacité, en ce moment, ces quatre figures, dans leur humble décor, surgissaient devant la vision intérieure de Lina!

Lina?... Est-ce là le nom qu'elle entendait sortir de leurs lèvres?

Lina de Cardeville?... Les honnêtes créatures prononçaient-elles jamais ces syllabes effrontées, dont elles devaient rougir? Non... Mélina...

La fraîche appellation de son enfance... C'est ce nom-là qui lui tintait aux oreilles.

Une autre bouche le prononçait aussi. Sa jeune maîtresse affectueuse, cette Régine si haute, si pure, qui l'aimait... Celle-là aussi élevait la voix dans la nuit et l'appelait avec bonté: «Mélina!» 

Une émotion inconnue étreignit la pécheresse. Trop d'abominations se mêlaient depuis quelque temps au luxe et au plaisir pour lesquels elle avait abandonné, piétiné ces êtres et ces choses. Son amour recouvré n'étouffait pas la nostalgie qui s'emparait d'elle. Tout à l'heure, dans l'affolement de la jalousie, elle ne pensait qu'à reconquérir l'être qui la tenait par des liens honteux et terribles.

Mais maintenant qu'il sommeillait à ses côtés, des frayeurs et des hantises montaient pour elle de ce sommeil, de cette belle tête brune et séduisante, de ce visage que jadis, en une nuit d'horreur, elle avait vu taché de sang.

Une angoisse indescriptible, une sensation d'isolement presque terrifiante saisirent la malheureuse. Qui avait-elle désormais au monde pour s'inquiéter d'elle? A qui pouvait-elle dévoiler la misère de sa pauvre âme puérile, fragile, mais parfois secouée d'épouvantes obscures, de remords confus et déchirants?... Celui qui dormait là, dans ce lit, n'avait jamais partagé avec elle l'intimité d'un sentiment. La passion les avait unis passagèrement, dans l'ignorance l'un de l'autre. Qui était-il au juste, ce Miguel Almado? Elle ne savait rien de lui, rien... sinon l'action effroyable qu'elle lui avait vu commettre, là-bas, dans le train de Marseille. Quel souvenir!... Et il ne l'aimait plus... Elle le sentait bien, malgré la réconciliation de ce soir. Pourtant il représentait tout pour elle. En dehors de lui, c'était l'aridité atroce d'une existence de courtisane: le méprisant caprice de ceux qui la payaient, la haine sournoise des serviteurs, la rosserie des camarades envieuses.
