
Tous les jours je me mettais à la fenêtre. J'ai eu bien vite deviné ce qu'il faisait. Il donnait des leçons de piano à ces deux longues demoiselles si maigres, que nous rencontrons quelquefois. Pauvre garçon!... J'épiais son arrivée et aussi sa sortie. Si tu savais, père, comme il avait l'air malheureux!... Il y avait des jours où il était si pâle, où il se traînait si péniblement que je me demandais s'il avait mangé. Te fais-tu une idée de cela? Lui!... souffrir la faim, lorsque moi je suis riche! Car nous sommes riches, n'est-ce pas? J'avais fini par connaître toutes les expressions de sa physionomie. Tiens, quand il était content, il faisait comme cela avec son bras...

Elle imitait en même temps un geste de Paul, geste qui lui était familier quand il lui arrivait quelque chose d'heureux.
-Mais, hélas!... continuait Flavie, un jour il a disparu... Pendant une semaine je suis restée à la fenêtre, attendant, espérant... En vain.

C'est alors que je suis tombée malade, et que je t'ai tout avoué, et que je t'ai dit: Celui-là est mon mari, je l'aime!...

C'est d'un air sombre et avec une visible contrainte que M. Martin-Rigal écoutait ce récit que Flavie lui répétait pour la centième fois, au moins, depuis trois mois.
-Oui, murmurait-il, c'est bien ainsi que tout s'est passé. Tu étais malade, je te voyais déjà mourante, je t'ai promis que ce jeune homme, cet inconnu dont tu ne savais même pas le nom, serait ton mari...

Dans un élan de reconnaissance, la jeune fille jeta ses bras autour du cou de son père, et couvrit son front de baisers sonores.
-Et aussitôt, reprit-elle, j'ai été guérie. Et tu tiendras ta parole, n'est-ce pas? Ah!... père chéri, je t'aime pour cela plus que pour tout le reste. Dire que le jour même, rien qu'avec les renseignements que je te donnais, tu t'es mis en quête de mon mystérieux artiste.
-Hélas!... je suis sans forces contre tes volontés.

Flavie se redressa, menaçant gaîment son père, d'un mouvement mutin.
-Que signifie cet hélas! monsieur? demanda-t-elle. En seriez-vous par hasard à regretter votre bonté parfaite, votre obéissance?

Il ne répondit pas. Il regrettait en effet.
-Par exemple, reprit Flavie, je donnerais bien mon beau collier pour savoir comment tu t'y es pris pour le découvrir. Pourquoi ne m'as-tu jamais conté le plus petit détail? Voyons, ne me cache rien, qu'as-tu imaginé pour arriver jusqu'à lui, d'abord, et ensuite pour l'amener jusqu'à nous sans éveiller ses soupçons.

M. Martin-Rigal sourit bonnement.
-Ceci, répondit-il, est mon secret.
-Soit, garde-le. Au fait, que m'importent les moyens employés, puisque tu as réussi! Car tu as réussi, n'est-ce pas, je ne rêve pas, je ne deviens pas insensée! Ce soir, avant une heure, dans quelques instants peut-être le docteur Hortebize va nous le présenter. Et il s'assoiera à notre table, je le regarderai à mon aise, j'entendrai le son de sa voix...
-Folle!.... interrompit le banquier, malheureuse enfant!...

Elle ne pouvait pas ne pas protester.
-Oh!... répondit-elle vivement, folle?... peut-être. Mais malheureuse?

pourquoi?
-Tu l'aimes trop, répondit le banquier, avec l'accent d'une conviction profonde, il abusera.
-Lui!... fit la jeune fille avec la certitude admirable de la passion, lui, jamais?...
-Fasse Dieu, pauvre chère adorée, que mes pressentiments me trompent.

Mais que veux-tu? ce n'est point là l'homme que je rêvais pour toi. Un artiste...

Flavie, sérieusement fâchée cette fois, quitta les genoux de son père.

[Illustration: Son père doucement l'attira sur ses genoux.]
-Et voilà donc, s'écria-t-elle, tout ce que tu trouves contre lui. Il est artiste. Serait-ce un crime! Que ne lui reproches-tu aussi sa misère? Oui, il est artiste mais il a du génie, je l'ai lu sur son font.

Oui, il est affreusement pauvre, mai je suis assez riche pour deux. Il me devra tout, tant mieux! Quand il aura de la fortune, il ne sera pas forcé de s'épuiser à donner des leçons de piano; il lui sera permis d'utiliser son talent. Il écrira des opéras comme ceux de Félicien David, plus beaux que ceux de Gounod. On les représentera dans les théâtres et les salles crouleront sous les applaudissements. Moi, cependant, toute seule au fond d'une loge fermée, je m'enivrerai de la gloire de l'élu de mon coeur. Le monde aura la poésie, moi j'aurai le poète, et, quand je le voudrai, c'est pour moi seule que chanteront ses divines mélodies...

Elle parlait avec une exaltation extraordinaire, si pénétrée de son rêve, qu'elle ressentait, dans toute leur intensité, les sensations exactes de la réalité.

Mais elle dut s'arrêter, une quinte de toux lui coupait la parole.

Et pendant que les efforts secouaient sa poitrine et que le sang affluait à ses pommettes, M. Martin-Rigal la contemplait avec une expression navrante.

La mère de Flavie avait été emportée à vingt-quatre ans par cette implacable maladie qu'on nomme la «phtisie galopante,» qui ne pardonne pas, qui est le désespoir de la science impuissante, et qui, en quinze jours, d'une fille rayonnante de vie et de santé, fait un cadavre.
-Tu souffres, Flavie? demanda le banquier d'un ton qui trahissait une inquiétude trop poignante pour pouvoir être complétement dissimulée.
-Moi! souffrir? répondit-elle avec un regard extatique, ce serait donc de joie?
