
Ceci à l'adresse de Hawkins, et le gros homme sourit débonnairement.
- Votre frère est un officier de premier ordre, William, dit-il, et je lui ai fait l'honneur de le traiter comme il le mérite. Rappelez-vous que c'est moi qui rédige les rapports confidentiels.
- Alors, il faut dire que William vaut son pesant d'or, dit Mrs Jim. Je ne sais pas ce que nous aurions fait sans elle.

Elle posa sa main sur celle de William, toute durcie à force de tenir des rênes, et William la caressa doucement. Jim rayonnait sur toute la compagnie. Les choses marchaient bien en son univers. Trois de ses auxiliaires parmi les plus incompétents étaient morts, et de plus capables occupaient leurs places. Chaque jour maintenant rapprochait la saison des pluies. On était venu à bout de la famine dans cinq des districts sur huit, et, en somme, la mortalité n'avait pas été trop grande, toutes proportions gardées. Il inspectait Scott de la tête aux pieds, comme un ogre une proie, jubilant a constater la dureté de ses muscles que l'entraînement avait faits d'acier.
- À peine s'il s'est tassé un brin, murmurait Jim, mais il peut faire encore l'ouvrage de deux hommes.

Il se rendit compte à ce moment que Mrs Jim lui télégraphiait par signes quelque chose, et, suivant le code du ménage, c'était :
- Pas de doute. Regardez-les !

Il regarda, il écouta.

William disait seulement :

Et Scott ne répondait pas autre chose que :
- Je serai rudement content de rentrer au club. Je vous retiens une valse pour le bal de Noël, n'est-ce pas ?
- Il y a encore loin d'ici au Lawrence Hall, dit Jim. Il faut se coucher de bonne heure, Scott. Chariots de riz à la clef demain ; on devra commencer le chargement à cinq heures.
- Vous n'allez pas même donner à M. Scott un jour de repos ?
- Demanderais pas mieux, Lizzie, mais pas moyen, j'ai peur. Tant qu'il tiendra debout, il faut l'employer.
- Eh bien ! J'aurais eu au moins une soirée à l'européenne… Par Jupiter, j'allais oublier ! Qu'est-ce que je fais de mes gosses ?
- Laissez-les ici, dit William, c'est nous que cela regarde ; ici, avec autant de chèvres que vous pouvez. Il faut que j'apprenne à traire maintenant.
- Si vous vous souciez de vous lever tôt demain, je vous montrerai. J'aurai à traire en tout cas. À propos, la moitié des petits ont des verroteries ou des machines au cou. Il faudra faire attention à ne pas les enlever, pour le cas où les mères reparaîtraient.
- Vous oubliez que j'ai l'habitude, depuis le temps que je suis ici.
- J'espère, pour l'amour de Dieu, que vous n'en faites pas trop.

Scott ne surveillait pas le ton de sa voix.
- J'aurai soin d'elle, dit Mrs Jim, télégraphiant des messages de cent mots à la minute.

Elle confisqua William, tandis que Jim donnait à Scott ses ordres pour la nouvelle campagne. Il était très tard - presque neuf heures.
- Jim, vous êtes une brute, dit sa femme cette nuit-là.

Et le chef de la famine pouffa.
- Pas le moins du monde, ma chère. Je me rappelle avoir établi le premier cadastre de Jandiala pour les beaux yeux d'une jeune personne en crinoline, et elle avait une taille, alors, Lizzie, à prendre entre les doigts. Je n'ai jamais fait si bonne besogne depuis. Lui, il va travailler comme un démon.
- Mais vous auriez dû lui donner un jour.
- Et laisser la chose éclater maintenant ? Non, ma chère ; c'est leur meilleur temps.
- Je parie - les amours ! - que ni l'un ni l'autre ne se doute de ce qui se passe. Est-ce beau ! Est-ce adorable !
- Elle va se lever à trois heures du matin pour apprendre à traire ; le bon Dieu la bénisse ! Hélas ! Pourquoi faut-il vieillir et prendre du ventre !
- C'est une perle. Elle a fait plus de travail sous ma direction…
- Votre direction ! Le lendemain de son arrivée elle dirigeait tout et vous étiez en sous-ordre, où vous êtes encore. Elle vous mène presque aussi bien que vous me menez, moi.
- Eh bien ! Oui, justement, et c'est pourquoi je l'aime. Elle est droite comme un homme - comme son frère.
- Son frère est plus mou qu'elle. Il vient toujours me demander des ordres ; mais il est franc du collier, et un vrai bourreau de travail.

J'avoue que j'ai un faible pour William, et si j'avais une fille…

Le colloque s'arrêta court. Là-bas, dans le pays du Derajas, il y avait une tombe d'enfant, vieille déjà de vingt années, et ni Jim ni sa femme n'en reparlaient jamais.
- Tout de même, vous êtes responsable, ajouta Jim, après un moment de silence.
- Dieu les bénisse, dit Mrs Jim qui s'endormait.

Les étoiles n'avaient pas commencé de pâlir lorsque Scott, qui couchait dans une charrette vide, s'éveilla et se mit en silence à la besogne ; il semblait inhumain d'éveiller à cette heure Faiz Ullah et l'interprète. Courbé, la tête près du sol, il n'entendit pas venir William. Il l'aperçut soudain debout à ses côtés dans sa vieille amazone roussâtre, les yeux encore lourds de sommeil et une tasse de thé avec une rôtie dans les mains. Il y avait par terre un bébé qui piaulait, couché sur un morceau de couverture, et un enfant de six ans risquait un œil par-dessus l'épaule de Scott.
- Allons, petit vaurien, dit Scott, comment diable veux-tu avoir ta part si tu ne restes pas tranquille ?
