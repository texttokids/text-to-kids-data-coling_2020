
Javert demeura quelques minutes immobile, regardant cette ouverture de ténèbres ; il considérait l'invisible avec une fixité qui ressemblait à de l'attention. L'eau bruissait. Tout à coup, il ôta son chapeau et le posa sur le rebord du quai. Un moment après, une figure haute et noire, que de loin quelque passant attardé eût pu prendre pour un fantôme, apparut debout sur le parapet, se courba vers la Seine, puis se redressa, et tomba droite dans les ténèbres ; il y eut un clapotement sourd, et l'ombre seule fut dans le secret des convulsions de cette forme obscure disparue sous l'eau.

Quelque temps après les événements que nous venons de raconter, le sieur Boulatruelle eut une émotion vive.

Boulatruelle, on s'en souvient peut-être, était un homme occupé de choses troubles et diverses. Il cassait des pierres et endommageait des voyageurs sur la grande route. Terrassier et voleur, il avait un rêve ; il croyait aux trésors enfouis dans la forêt de Montfermeil. Il espérait quelque jour trouver de l'argent dans la terre au pied d'un arbre ; en attendant, il en cherchait volontiers dans les poches des passants.

Néanmoins, pour l'instant, il était prudent. Il venait de l'échapper belle. Il avait été, on le sait

, ramassé dans le galetas Jondrette avec les autres bandits. Utilité d'un vice : son ivrognerie l'avait sauvé. On n'avait jamais pu éclaircir s'il était là comme voleur ou comme volé. Une ordonnance de non-lieu, fondée sur son état d'ivresse bien constaté dans la soirée du guet-apens, l'avait mis en liberté. Il avait repris la clef des bois. Il était revenu à son chemin de Gagny à Lagny faire, sous la surveillance administrative, de l'empierrement pour le compte de l'état, la mine basse, fort pensif, un peu refroidi pour le vol, qui avait failli le perdre, mais ne se tournant qu'avec plus d'attendrissement vers le vin, qui venait de le sauver.

Quant à l'émotion vive qu'il eut peu de temps après sa rentrée sous le toit de gazon de sa hutte de cantonnier, la voici :

Un matin, Boulatruelle, en se rendant comme d'habitude à son travail, et à son affût peut-être, un peu avant le point du jour, aperçut parmi les branches un homme dont il ne vit que le dos, mais dont l'encolure, à ce qui lui sembla, à travers la distance et le crépuscule, ne lui était pas tout à fait inconnue. Boulatruelle, quoique ivrogne, avait une mémoire correcte et lucide, arme défensive indispensable à quiconque est un peu en lutte avec l'ordre légal.
- Où diable ai-je vu quelque chose comme cet homme-là ? se demanda-t-il.

Mais il ne put rien se répondre, sinon que cela ressemblait à quelqu'un dont il avait confusément la trace dans l'esprit.

Boulatruelle, du reste, en dehors de l'identité qu'il ne réussissait point à ressaisir, fit des rapprochements et des calculs. Cet homme n'était pas du pays. Il y arrivait. À pied, évidemment. Aucune voiture publique ne passe à ces heures-là à Montfermeil. Il avait marché toute la nuit. D'où venait-il ? De pas loin. Car il n'avait ni havre-sac, ni paquet. De Paris sans doute. Pourquoi était-il dans ce bois ? pourquoi y était-il à pareille heure ? qu'y venait-il faire ?

Boulatruelle songea au trésor. À force de creuser dans sa mémoire, il se rappela vaguement avoir eu déjà, plusieurs années auparavant, une semblable alerte au sujet d'un homme qui lui faisait bien l'effet de pouvoir être cet homme-là.

Tout en méditant, il avait, sous le poids même de sa méditation, baissé la tête, chose naturelle, mais peu habile. Quand il la releva, il n'y avait plus rien. L'homme s'était effacé dans la forêt et dans le crépuscule.
- Par le diantre, dit Boulatruelle, je le retrouverai. Je découvrirai la paroisse de ce paroissien-là. Ce promeneur de patron-minette a un pourquoi, je le saurai. On n'a pas de secret dans mon bois sans que je m'en mêle.

Il prit sa pioche qui était fort aiguë.
- Voilà, grommela-t-il, de quoi fouiller la terre et un homme.

Et, comme on rattache un fil à un autre fil, emboîtant le pas de son mieux dans l'itinéraire que l'homme avait dû suivre, il se mit en marche à travers le taillis.

Quand il eut fait une centaine d'enjambées, le jour, qui commençait à se lever, l'aida. Des semelles empreintes sur le sable çà et là, des herbes foulées, des bruyères écrasées, de jeunes branches pliées dans les broussailles et se redressant avec une gracieuse lenteur comme les bras d'une jolie femme qui s'étire en se réveillant, lui indiquèrent une sorte de piste. Il la suivit puis il la perdit. Le temps s'écoulait. Il entra plus avant dans le bois et parvint sur une espèce d'éminence. Un chasseur matinal qui passait au loin sur un sentier en sifflant l'air de Guillery lui donna l'idée de grimper dans un arbre. Quoique vieux, il était agile. Il y avait là un hêtre de grande taille, digne de Tityre et de Boulatruelle. Boulatruelle monta sur le hêtre, le plus haut qu'il put.

L'idée était bonne. En explorant la solitude du côté où le bois est tout à fait enchevêtré et farouche, Boulatruelle aperçut tout à coup l'homme.
