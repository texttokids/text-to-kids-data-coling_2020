
« Alors M. de Villeparisis ne va pas tarder à descendre.

Depuis un mois qu'ils sont ici ils n'ont mangé qu'une fois l'un sans l'autre », dit le garçon.

Je me demandais quel était celui de ses parents avec lequel elle voyageait et qu'on appelait M. de Villeparisis, quand je vis, au bout de quelques instants, s'avancer vers la table et s'asseoir à côté d'elle son vieil amant, M. de Norpois.

Son grand âge avait affaibli la sonorité de sa voix, mais donné en revanche à son langage, jadis si plein de réserve, une véritable intempérance. Peut-être fallait-il en chercher la cause dans des ambitions qu'il sentait ne plus avoir grand temps pour réaliser et qui le remplissaient d'autant plus de véhémence et de fougue ; peut-être dans le fait que, laissé à l'écart d'une politique où il brûlait de rentrer, il croyait, dans la naïveté de son désir, faire mettre à la retraite, par les sanglantes critiques qu'il dirigeait contre eux, ceux qu'il se faisait fort de remplacer. Ainsi voit-on des politiciens assurés que le cabinet dont ils ne font pas partie n'en a pas pour trois jours. Il serait, d'ailleurs, exagéré de croire que M. de Norpois avait perdu entièrement les traditions du langage diplomatique. Dès qu'il était question de « grandes affaires » il se retrouvait, on va le voir, l'homme que nous avons connu, mais le reste du temps il s'épanchait sur l'un et sur l'autre avec cette violence sénile de certains octogénaires qui les jette sur des femmes à qui ils ne peuvent plus faire grand mal.

me de Villeparisis garda, pendant quelques minutes, le silence d'une vieille femme à qui la fatigue de la vieillesse a rendu difficile de remonter du ressouvenir du passé au présent.

Puis, dans ces questions toutes pratiques où s'empreint le prolongement d'un mutuel amour : - Êtes-vous passé chez Salviati ?
- Oui.
- Enverront-ils demain ?
- J'ai rapporté moi-même la coupe. Je vous la montrerai après le dîner. Voyons le menu.
- Avez-vous donné l'ordre de bourse pour mes Suez ?
- Non, l'attention de la Bourse est retenue en ce moment par les valeurs de pétrole. Mais il n'y a pas lieu de se presser étant donné les excellentes dispositions du marché. Voilà le menu. Il y a comme entrée des rougets. Voulez-vous que nous en prenions ?
- Moi, oui, mais vous, cela vous est défendu. Demandez à la place du risotto. Mais ils ne savent pas le faire.
- Cela ne fait rien. Garçon, apportez-nous d'abord des rougets pour Madame et un risotto pour moi.

Un nouveau et long silence.

« Tenez, je vous apporte des journaux, le Corriere della Sera , la Gazzetta del Popolo , etc. Est-ce que vous savez qu'il est fortement question d'un mouvement diplomatique dont le premier bouc émissaire serait Paléologue, notoirement insuffisant en Serbie ? Il serait peut-être remplacé par Lozé et il y aurait à pourvoir au poste de Constantinople. Mais, s'empressa d'ajouter avec âcreté M. de Norpois, pour une ambassade d'une telle envergure et où il est de toute évidence que la Grande-Bretagne devra toujours, quoi qu'il arrive, avoir la première place à la table des délibérations, il serait prudent de s'adresser à des hommes d'expérience mieux outillés pour résister aux embûches des ennemis de notre alliée britannique que des diplomates de la jeune école qui donneraient tête baissée dans le panneau. » La volubilité irritée avec laquelle M. de Norpois prononça ces dernières paroles venait surtout de ce que les journaux, au lieu de prononcer son nom comme il leur avait recommandé de le faire, donnaient comme « grand favori » un jeune ministre des Affaires étrangères. « Dieu sait si les hommes d'âge sont éloignés de se mettre, à la suite de je ne sais quelles manœuvres tortueuses, aux lieu et place de plus ou moins incapables recrues ! J'en ai beaucoup connu de tous ces prétendus diplomates de la méthode empirique, qui mettaient tout leur espoir dans un ballon d'essai que je ne tardais pas à dégonfler. Il est hors de doute, si le gouvernement a le manque de sagesse de remettre les rênes de l'État en des mains turbulentes, qu'à l'appel du devoir un conscrit répondra toujours : présent. Mais qui sait (et M. de Norpois avait l'air de très bien savoir de qui il parlait) s'il n'en serait pas de même le jour où l'on irait chercher quelque vétéran plein de savoir et d'adresse ? À mon sens, chacun peut avoir sa manière de voir, le poste de Constantinople ne devrait être accepté qu'après un règlement de nos difficultés pendantes avec l'Allemagne. Nous ne devons rien à personne, et il est inadmissible que tous les six mois on vienne nous réclamer, par des manœuvres dolosives et à notre corps défendant, je ne sais quel quitus, toujours mis en avant par une presse de sportulaires. Il faut que cela finisse, et naturellement un homme de haute valeur et qui a fait ses preuves, un homme qui aurait, si je puis dire, l'oreille de l'empereur, jouirait de plus d'autorité que quiconque pour mettre le point final au conflit. » Un monsieur qui finissait de dîner salua M. de Norpois.
- Ah ! mais c'est le prince Foggi, dit le marquis.
- Ah ! je ne sais pas au juste qui vous voulez dire, soupira Mme de Villeparisis.
- Mais parfaitement si. C'est le prince Odon. C'est le propre beau-frère de votre cousine Doudeauville. Vous vous rappelez bien que j'ai chassé avec lui à Bonnétable ?
- Ah ! Odon, c'est celui qui faisait de la peinture ?
- Mais pas du tout, c'est celui qui a épousé la sœur du grand-duc N…
