
« J'assiste, en effet, à une chose terrible.

« Un homme honorable et honoré, un vieux soldat dont le sang a coulé sur tous nos grands champs de bataille pour celui qui était à la fois son compatriote, son maître et son ami ; un homme dont jamais une pensée mauvaise n'a souillé le cœur, dont jamais une action honteuse n'a taché la main ; cet homme, venu ici le front haut afin de répondre à une de ces accusations qui parfois sont une gloire pour ceux qu'elles atteignent ; cet homme, qui vient vous dire : “J'ai joué ma tête à ce grand jeu des conspirations qui renverse les trônes, change les dynasties, bouleverse les empires ; j'ai perdu : prenez-la !” cet homme s'entend dire : “Taisez-vous ! vous n'êtes point un conspirateur ; vous êtes un voleur, vous êtes un ravisseur, vous êtes un assassin !”

« Ah ! messieurs, il faut être bien fort, vous en conviendrez, pour rester la tête haute devant cette triple accusation. En effet, nous sommes fort ! car, à cette triple accusation, nous répondrons purement et simplement ceci : “Si nous étions ce que vous dites, l'homme aux yeux d'aigle et aux regards de flamme, qui savait si bien lire dans les cœurs, ne nous aurait pas serré la main, ne nous aurait pas appelé son ami, ne nous aurait pas dit : Va !...
- Pardon, maître Emmanuel Richard, dit le président ; mais de quel homme parlez-vous donc ainsi ?

, sacré en 1804, à Paris, empereur des Français ; couronné en 1805, à Milan, roi d'Italie, et mort prisonnier à Sainte-Hélène, le 5 mai 1821, répondit à haute et intelligible voix le jeune avocat.

Il est impossible de dire quel frisson étrange courut dans l'assemblée.

À cette époque, on appelait Napoléon l'usurpateur, le tyran, l'ogre de Corse, et, depuis treize ans, c'est-à-dire depuis le jour de sa chute, personne, à coup sûr, n'avait prononcé tout haut, en face de son meilleur et de son plus intime ami, ce qu'Emmanuel Richard venait de prononcer en face de la cour, des jurés et de l'auditoire.

Les gendarmes qui étaient assis à la droite et à la gauche de M. Sarranti se levèrent et interrogèrent des yeux et du geste le président, pour savoir ce qu'il y avait à faire, et s'ils ne devaient pas, séance tenante, mettre la main sur l'audacieux avocat.

L'excès de son audace même le sauva ; le tribunal resta atterré.

M. Sarranti saisit la main du jeune homme.
- Assez, lui dit-il, assez ! au nom de votre père, ne vous compromettez pas.
- Au nom de votre père et du mien, continuez ! s'écria Dominique.
- Vous avez vu, messieurs, continua Emmanuel, des procès dans lesquels les accusés venaient démentir les témoins, dénier des preuves évidentes, chicaner leur vie au procureur du roi ; vous avez vu cela quelquefois, souvent, presque toujours... Eh bien, nous, messieurs, nous vous réservons un spectacle plus curieux.

« Nous venons vous dire :

« Oui, nous sommes coupable, et en voilà les preuves ; oui, nous avons conspiré contre la sûreté intérieure de l'État, et en voilà les preuves ; oui, nous avons voulu changer la forme du gouvernement, et en voilà les preuves ; oui, nous avons tramé un complot contre le roi et sa famille, et en voilà les preuves ; oui, nous sommes criminel de lèse-majesté, et en voilà la preuves ; oui, nous avons mérité la peine des parricides, et en voilà la preuve ; oui, nous demandons à marcher à l'échafaud les pieds nus et le voile noir sur la tête, comme c'est notre droit, comme c'est notre désir, comme c'est notre vœu... »

Un cri de terreur s'échappa de toutes les bouches.
- Taisez-vous ! taisez-vous ! cria-t-on de tous côtés au jeune fanatique, vous le perdez !
- Parlez, parlez ! s'écria Sarranti, c'est comme cela que je veux être défendu.

Des applaudissements éclatèrent sur tous les points de l'auditoire.
- Gendarmes, faites évacuer la salle ! s'écria le président.

Puis, se tournant vers l'avocat :
- Maître Emmanuel Richard, dit-il, je vous ôte la parole.
- Peu m'importe à cette heure, répondit l'avocat, j'ai rempli le mandat qui m'avait été confié ; j'ai dit tout ce que j'avais à dire. Puis, se retournant vers M. Sarranti :
- Êtes-vous content, monsieur, et sont-ce bien vos propres paroles que j'ai répétées ?

Pour toute réponse, M. Sarranti se jeta dans les bras de son défenseur.

Les gendarmes se mirent en mesure d'exécuter l'ordre du président ; mais un tel rugissement courut à l'instant même dans la multitude, que le président comprit qu'il entreprenait une œuvre non seulement difficile, mais encore dangereuse. Une émeute pouvait éclater, et, pendant le tumulte, M. Sarranti pouvait être enlevé.

Un des juges se pencha et prononça tout bas quelques mots à l'oreille du président.
- Gendarmes, dit celui-ci, reprenez vos places. La Cour en appelle à la dignité de l'auditoire.
- Silence ! dit une voix au milieu de la foule.

Et, comme si la foule était habituée à obéir à cette voix, elle se tut.

Suite de l'affaire Sarranti.

Dès lors, la question était nettement posée : d'un côté, la conspiration, réfugiée dans sa foi impériale, dans la religion de son serment, se faisant, non pas un bouclier, mais une palme de son crime lui-même ; de l'autre, le ministère public, décidé à poursuivre dans M. Sarranti, non le criminel de haute trahison, le coupable de lèse-majesté, mais le voleur de cent mille écus, le ravisseur des enfants, l'assassin d'Orsola.
