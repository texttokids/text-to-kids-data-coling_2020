
Dans cette position, il découvrait admirablement la cellule. Il apercevait la porte, le lit, la table, la chaise. Un seul petit espace près de la fenêtre, et la fenêtre elle-même, échappaient à ses regards.

Il terminait à peine sa reconnaissance, quand les verroux grincèrent.

Le prévenu revenait de sa promenade.

Il était très-gai, et terminait une histoire fort intéressante sans doute, puisque le gardien resta un moment pour en attendre la fin.

Le jeune policier fut ravi de l'épreuve. Il entendait aussi bien qu'il voyait. Les sons arrivaient à son oreille aussi distinctement que s'il y eussent été apportés par un cornet acoustique. Il ne perdit pas un mot du récit, qui était légèrement graveleux.

Le surveillant parti, Mai fit quelques pas de ci et de là dans sa cellule; puis il s'assit, ouvrit son volume de Béranger, et pendant une heure parut absorbé par l'étude d'une chanson. Finalement il se jeta sur son lit.

Au moment du repas du soir, seulement, il se leva pour manger de bon appétit. Il se remit ensuite à son chansonnier et ne se coucha qu'à l'extinction des feux.

Lecoq savait bien que la nuit ses yeux ne lui serviraient de rien; mais c'est alors qu'il espérait surprendre quelques exclamations révélatrices.

Son attente fut trompée, Mai se tourna et se retourna douloureusement sur ses matelas, il geignit par moments; on eût dit qu'il sanglotait, mais il n'articula pas une syllabe.

Le prévenu resta couché fort tard le lendemain. Mais en entendant sonner l'heure de la pitance du matin, onze heures, il se leva d'un bond, et après quelques entrechats dans sa cellule, il entonna à pleine voix une vieille chanson: 

Diogène, Sous ton manteau, Libre et content, je ris, je bois sans gêne...

C'est seulement lorsque les gardiens entrèrent qu'il cessa de chanter...

Telle s'était écoulée la journée de la veille, telle s'écoula celle-ci; celle du lendemain fut pareille, les suivantes furent toutes semblables...

Chanter, manger, dormir, soigner ses mains et ses ongles, telle était la vie de ce soi-disant saltimbanque. Son attitude, toujours la même, était celle d'un homme d'un heureux naturel profondément ennuyé.

Telle était la perfection de la comédie soutenue par cet énigmatique personnage, que Lecoq, après six nuits et six jours passés à plat ventre dans son grenier, n'avait rien surpris de décisif.

Pourtant il était loin de désespérer. Il avait observé que tous les matins, à l'heure où la distribution des vivres met en mouvement les employés de la prison, le prévenu ne manquait pas de répéter sa chanson de Diogène.
-Évidemment, se disait le jeune policier, cette chanson est un signal. Que se passe-t-il alors, du côté de cette fenêtre que je ne vois pas?... Je le saurai demain.

Le lendemain, en effet, il obtint que Mai serait conduit à la promenade à dix heures et demie, et il entraîna le directeur à la cellule du prisonnier.

Le digne fonctionnaire n'était pas content du dérangement.
-Que prétendez-vous me montrer? répétait-il, qu'y a-t-il de si curieux?...
-Peut-être rien, répondait Lecoq, peut-être quelque chose de bien grave...

Et onze heures sonnant peu après, il entonna la chanson du prévenu: 

Diogène, Sous ton manteau...

Il venait d'entamer le second couplet, quand une boulette de mie de pain de la grosseur d'une balle, adroitement lancée par dessus la hotte de la fenêtre, vint rouler à ses pieds.

La foudre tombant dans la cellule de Mai n'eût pas terrifié le directeur autant que cet inoffensif projectile.

Il demeura stupide d'étonnement, la bouche béante, les yeux écarquillés, comme s'il eût douté du témoignage de ses sens.

Quelle disgrâce! L'instant d'avant il eût répondu sur sa tête chauve de l'inviolabilité des secrets. Il vit sa prison déshonorée, bafouée, ridiculisée...
-Un billet, répétait-il d'un air consterné, un billet!...

Prompt comme l'éclair, Lecoq avait ramassé ce message et il le retournait triomphalement entre ses doigts.
-J'avais bien dit, murmurait-il, que nos gens s'entendaient!

Cette joie du jeune policier devait changer en furie la stupeur du directeur.
-Ah!... mes détenus s'écrivent!... s'écria-t-il bégayant de colère.

Ah! mes surveillants font l'office de facteurs! Par le saint nom de Dieu!... cela ne se passera pas ainsi!

Il se dirigeait vers la porte; Lecoq l'arrêta.
-Qu'allez-vous faire, monsieur! dit-il.
-Moi! je vais rassembler tous les employés de ma maison, et leur déclarer qu'il y a un traître parmi eux, et qu'il faut qu'on me le livre. Je veux faire un exemple. Et si d'ici vingt-quatre heures le coupable n'est pas découvert, tout le personnel du Dépôt sera renouvelé.

De nouveau, il voulut sortir, et le jeune policier, cette fois, dut presque employer la violence pour le retenir.
-Du calme, monsieur, lui disait-il, du calme, modérez-vous...
-Je veux punir!
-Je comprends cela, mais attendez d'avoir tout votre sang-froid. Il se peut que le coupable soit, non un de vos gardiens, mais un de ces détenus dont vous utilisez la bonne volonté, et qui aident tous les matins à la distribution...
-Eh! qu'importe...
-Pardon!... Il importe beaucoup. Si vous faites du bruit, si vous dites un seul mot de ceci, jamais nous ne découvrirons la vérité. Le traître ne sera pas si fou que de se livrer, mais il sera assez sage pour ne plus recommencer. Sachons nous taire, dissimuler et attendre.
