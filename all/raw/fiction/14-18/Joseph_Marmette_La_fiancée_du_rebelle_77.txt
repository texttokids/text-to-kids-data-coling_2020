
Gauthier désarma son mousquet, le jeta sur son épaule, et tous deux dirigèrent leurs pas vers l'endroit de la forêt d'où les Américains étaient sortis.

Voici, pendant ce temps, ce qui se passait au corps-de-garde où Tranquille avait été retenu prisonnier. Réjouis de leur victoire tous les soldate étaient en liesse. Les officiers venaient de leur faire distribuer une double ration d'eau-de-vie.

C'était l'heure du dîner. De l'étroit et sombre cachot où il était enfermé, Tranquille pouvait ouïr les joyeux propos et le cliquetis des fourchettes et des verres. A plusieurs reprises, il avait eu connaissance que des camarades du soldat en faction lui avaient apporté à boire. Bientôt même il entendit la sentinelle, un peu excitée par ses libations répétées, fredonner une chanson joyeuse.
-Mon homme me semble de bonne humeur, voici le moment de l'appeler, pensa Tranquille.

Il frappa trois coups dans la porte. Le soldat qui marchait de long en large, s'approcha et vint ouvrir.
-J'ai soif! lui dit Tranquille.

L'autre, qui ne comprenait pas le français et que l'obscurité qui régnait dans le caveau empêchait de bien apercevoir le prisonnier, se pencha en dedans de la porte entrebâillée.

Cinq doigts d'acier se cramponnèrent à son cou, tandis qu'une main le tirait dans le caveau. Sans lâcher la gorge du soldat, Tranquille lui asséna de son autre main fermée deux formidables coups de poings dans la poitrine, et un dernier, vrai coup de massue, en plein sur le crâne. Le malheureux tomba tout d'une pièce et perdit connaissance avant d'avoir pu jeter un cri. Tranquille lui enleva sa giberne pleine de cartouches, la passa à son cou, saisit le fusil de factionnaire, et s'élança hors du caveau.

La porte de sortie donnait sur la salle à dîner du corps-de-garde. Il ne fallait pas songer à s'en aller par-là. Et pourtant il n'y avait pas de temps à perdre, au même instant on l'apercevait de la salle. Il avisa une fenêtre, reconnut d'un coup d'oeil qu'elle n'était point garnie de barreaux de fer, l'enfonça d'un coup de pied, et, au milieu des éclats de verre et des débris de toutes sortes, qui volaient autour de lui, au bruit des clameurs forcenées des soldats, il sauta dans la rue. D'abord il courut quelques pas droit devant lui, puis s'orienta, pris ses longues jambes à son cou et s'élança du côté de la campagne.

Une dizaine de soldats s'étaient jetés à sa poursuite sans avoir eu le temps de prendre leurs armes, espérant le devancer à la course. Mais les pauvres diables ne savaient point qu'ils avaient affaire au plus agile coureur des bois qui ait chassé l'orignal de nos forêts.

A chaque bond qu'il faisait, Tranquille gagnait un pas sur ses poursuivants. Enfin il atteignit l'extrémité de la ville, sauta dans les champs où il secoua joyeusement la tête en aspirant l'air libre. Ses bondissements étaient joyeux et puissants comme ceux d'un fauve qui a rompu les barreaux de sa cage et dévore l'espace qui le sépare de la liberté.

Lassés bientôt de leur inutile poursuite, les soldats s'arrêtèrent, et ahuris, le virent grimper au haut du coteau Sainte-Marguerite et s'enfoncer dans la forêt.

Maintenant il faut nous reporter au jour précédent, aussitôt après la bataille. Comme ils s'enfuyaient tous deux en remontant le versant du coteau, Alice remarqua plusieurs fois que son mari chancelait.
-Es-tu blessé, dis-moi? lui demanda-t-elle à plusieurs reprises.

Mais lui, qui tenait sa main crispée autour du bras de la jeune femme, fuyait toujours, tout en la maintenant à son côté pour l'empêcher de le regarder en face.
-Non, non! disait-il avec énergie.

Ce fut ainsi qu'ils gagnèrent le bois où ils s'enfoncèrent en courant toujours. Ils firent plusieurs arpents, sans s'arrêter une minute. Mais peu à peu Marc semblait perdre de sa vigueur. Plus incertain son pas se ralentissait. Alice sentit enfin que les doigts qui retenaient se desserraient brusquement, et s'aperçut qu'il allait tomber. Elle voulut le retenir; mais Evrard ploya sur ses jambes sans force, et s'abattit lourdement sur le sol en entraînant sa femme avec lui.
-Mon Dieu! Marc, qu'as-tu donc? s'écria-t-elle.

En le regardant, elle jeta un cri de terreur et appuya ses doigts fermés sur la poitrine du jeune homme, d'où s'échappait un flot de sang.

L'épée du capitaine Evil avait percé le sein d'Evrard en pénétrant dans le poumon droit.
-Il va mourir mon Dieu! fit-elle avec un cri de désespoir qui retentit sous le dôme des grands arbres-Marc! je t'en prie, réponds-moi!

criait-elle affolée. Tout ce sang... Sa vie qui s'en va, Seigneur Dieu!

A l'aide! Au secours!... Mais ses clameurs se perdaient sous les bois et l'écho désespérant répondait seul à sa voix.

Après une faiblesse de quelques minutes, Marc un peu soulagé par l'hémorragie et ranimé par les accents déchirants d'Alice, ouvrit des yeux hagards. En reprenant peu à peu ses sens, il arrêta ses regards sur sa femme avec un sentiment indicible d'angoisse.

Elle dévorais ses gestes et aspirait chacun de ses soupirs.
-Oh! ne meurs pas, je t'en prie, Marc! Sauvez-le, mon Dieu! Tuez-moi, mais qu'il vive lui, Seigneur!
-Alice, soupira le blessé, je t'en prie... ne te désespère pas ainsi!... Tâche plutôt... d'arrêter mon sang...
