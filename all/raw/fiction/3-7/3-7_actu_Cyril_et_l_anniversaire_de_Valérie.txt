Ce matin, comme tous les matins, Cyril arrive à l’école.
Il retrouve tous ses copains dans la cour. Jean-Luc, son meilleur ami est là aussi.
Jean-Luc est très chic aujourd'hui. Il a une jolie chemise, il est peigné et a même un bouquet de fleurs.
Cyril éclate de rire : "Pourquoi tu es habillé en pingouin aujourd'hui ?"
Jean-Luc lui répond : "C'est parce que c'est l'anniversaire de Valérie. Je lui ai apporté des fleurs. Et toi ?"
Cyril repense à Valérie. Comme elle est jolie, c'est la plus jolie fille de la classe. Il est très amoureux d'elle.
Mais Cyril avait oublié que c’était l'anniversaire de Valérie ! Il ne veut pas que Jean-Luc lui fasse un plus beau cadeau que lui !
Alors Cyril réfléchit pendant que tout le monde rentre en classe. "Pense, pense, Cyril, tu dois trouver une super idée qui fera que Valérie tombera amoureuse de toi !"
C'est l'heure du goûter. Toute la classe a préparé des dessins, des bonbons pour Valérie. Chacun vient la voir et lui fait un cadeau. Valérie est très contente.
Mais c'est le tour de Cyril et il ne sait pas quoi faire ! Tremblant, il s’approche de Valérie et se met à danser ! Il fait la danse de l’épaule en tournant sur lui-même pour faire sourire la jolie Valérie.
Tout le monde est surpris, mais Valérie éclate de rire, applaudit et danse avec lui. Cyril est vraiment un drôle de garçon, mais il lui a fait le cadeau le plus rigolo, Valérie l'aime beaucoup !
