﻿C’est l’heure de la récréation, et Lily a envie de s’amuser. Elle sort dans la cour avec ses copines et décide de jouer à la marelle. Les petites filles se mettent à la queue leu-leu pour passer chacune leur tour.
C’est au tour de Lily ! La petite fille jette son caillou et saute.
Quand tout à coup, sa chaussure glisse et Lily tombe sur le genou. “Aïe aïe aïe !” crie Lily en pleurant et tenant son genou blessé.
Ses copines appellent la maîtresse qui prend la fillette dans ses bras et la conduit à l’infirmerie.
En arrivant à l’infirmerie, la maîtresse assoit Lily sur une chaise. L’infirmière regarde le genou de la petite fille et lui demande : “Et bien alors, comment t’es-tu fait ça ?”
“En jouant à la marelle.” répond Lily en sanglotant. “Ne t’en fais pas Lily, on va désinfecter ton bobo et mettre un pansement” explique l’infirmière.
“Ça veut dire quoi désinfecter ? Est ce que ça va faire mal ?” demande la fillette inquiète. “Ça va picoter un petit peu, mais rien de méchant !” réconforte l’infirmière. “Je vais prendre de l’eau et du savon pour nettoyer ton genou et enlever le sang et les petits graviers.” L’infirmière imbibe un coton d’eau savonneuse qu’elle vient poser sur le genou blessé. La petite fille frémit en sentant le coton tout froid. Mais pas le temps de crier que c’est déjà fini !
Le genou est nettoyé et ne saigne plus. Lily a même le droit à un joli pansement avec des papillons dessus.
Et pour retrouver le sourire, quoi de mieux qu’une bonne sucette !
Lily est rassurée. Elle n’a plus mal au genou et va pouvoir montrer son joli pansement à ses copines. Sans oublier la sucette de la gentille infirmière qu’elle va pouvoir grignoter. Avec tout ça, le petit accident est vite oublié !
