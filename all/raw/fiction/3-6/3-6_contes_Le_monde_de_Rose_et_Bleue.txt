Il était une fois un monde ou le ciel était jaune et les nuages orange. Dans ce monde vivaient deux princesses , l’une s’appelait Rose et l’autre Bleue.
Les deux princesses se disputaient sans cesse.
Un jour, pendant une dispute, Bleue tomba à travers les nuages.
Bleue atterrit dans un monde où elle était minuscule.
Elle chercha un endroit confortable où se poser pour la nuit.
Le soir elle regardait les étoiles en pensant à Rose qui lui manquait terriblement.
Pendant ce temps Rose pleurait, elle aussi, le départ de Bleue. Elle pleurait tellement que l’eau de ses larmes faisait déborder les nuages.
L’eau monta, monta, monta jusqu’à submerger le monde où se trouvait Bleue la brebis. Bleue se retrouva à flotter sur sa cuillère au même niveau que les nuages.
Enfin, les deux brebis furent réunies. Elles se promirent de ne plus jamais se disputer.
