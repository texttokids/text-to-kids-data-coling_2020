Aujourd’hui, c’est sortie à la piscine avec l’école. Lily a hâte de pouvoir se baigner, même si elle appréhende un peu car elle ne sait pas nager.
Arrivés à la piscine, tous les enfants ont envie de se baigner. Mais le maître nageur est là pour rappeler les règles de sécurité.
“Avant tout les enfants”, explique-t-il, "vous devez enfiler des bouées qui vous aideront à nager sans couler”. Le maitre nageur montre aux enfants comment enfiler un gilet de sauvetage et des brassards.
Lily écoute avec attention car elle sait qu’il est important de bien faire les choses pour pouvoir aller se baigner.
Une fois le tout enfilé, les enfants sont prêts. Un pied, deux pieds, et les voilà qui barbotent dans la grande piscine.
Et même si Lily ne sait pas nager, le gilet et les brassards la font flotter.
Le maître nageur et la maîtresse aident les enfants à se déplacer et leur expliquent comment arriver à nager. “Est-ce que quelqu’un peut m’expliquer comment fait la grenouille pour nager ?” demande la maîtresse.
“Elle fait des mouvements avec ses pattes” répond Lily. “C’est exact, et pour apprendre à nager, vous allez devoir faire les mêmes mouvements qu’une grenouille.”
Le maitre nageur montre alors les mouvements des bras et des jambes en répétant : ”On plie, on déplie et on étire.”
Lily essaie de faire pareil, et arrive à se déplacer dans l’eau. La petite fille est heureuse.
La sortie à la piscine est terminée, et même si Lily ne sait pas encore bien nager, elle est heureuse de s’être amusée dans l’eau.
