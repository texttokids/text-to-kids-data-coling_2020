Pourquoi il part tous les jours Papa ?
Il va sauver des gens comme Superman ?
Il va jouer avec d'autres enfants que moi ?
Il va faire le tour de la terre et il revient ?
Il va voir d'autres Papas et ils parlent de leurs maisons ?
Papa s'en va la journée car il va construire et fabriquer des choses pour d'autres gens. Ça s'appelle le travail. En échange, ils lui donnent des sous pour pouvoir acheter le repas pour la maison, des jouets et pour partir en vacances.
Mais il y a des jours où il est à la maison avec nous !
Papa est fatigué parce qu'il va beaucoup au travail, et tu lui manques aussi. 2 jours par semaine, il peut rester avec nous à la maison pour se reposer et profiter de nous.
Moi aussi je peux aller au travail avec Papa ? - Quand tu seras plus grand, tu auras aussi un travail, et tu pourras acheter des choses aussi.
Comme Papa est au travail, il peut m’acheter mon nouveau robot en rentrant alors !
