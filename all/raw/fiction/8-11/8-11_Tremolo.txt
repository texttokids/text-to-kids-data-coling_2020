Trémolo était musicien dans l’âme.
Il jouait de ses instruments jour et nuit.
Mais ce qui était de la musique à ses oreilles était du bruite pour ses voisins. Un jour à minuit, Madame Abrah Kadabrah, la voyante extralucide de dessus, fit irruption chez lui.
« Infernal kasse-tympan ! Les ultra zondes de ton tapage ont fait éclater ma boule de Kristal. J’en ai perdu l’avenir. Sois maudit, trois fois maudit ! »
Et l’ayant foudroyé de son mauvais œil, elle disparut.
Le lendemain matin, Trémolo se mit à jouer du tuba. Sa musique sonnait de plus en plus sourde et étouffée. Le tuba se remplissait de notes, grosses et noires comme des olives.
Il en fut de même avec les autres instruments.
Cet après-midi-là, Trémolo devait jouer à un enterrement. Quand il quitta son appartement, il pataugeait dans les notes jusqu’au genoux.
Ce fut une catastrophe !
Le cortège funèbre dérapait, glissait, trébuchait sur les petites boules noires, et finit sur le pavé.
Trémolo s’enfuit sous les injures.
La concierge l’attendait sur son palier.
« Ouste ! Dehors ! » rugit-elle. « Et fissa illico ! » 
« D’abord le boucan, et maintenant toute cette saleté ! »
Ainsi chassé, le musicien ramassa toutes ses affaires, les chargea dans son vieux tacot et se mit en route vers une cabane isolée, héritée de sa grand-mère.
Là, tout seul, perché dans la montagne, il pouvait enfin faire de la musique. Il se mit à jouer en pleine nature. Éberlués, émerveillés, charmés, les animaux sortirent de leur cachette. Et commencèrent à grignoter les notes de sa musique.
Le soir même, après son dîner, Trémolo décida de goûter les notes, lui aussi. 
« Si ça plaît aux animaux, ça pourrait me plaire…  Et que oui ! » Les notes étaient succulentes et de goûts très variés, aussi variés que la musique qu’il jouait.
La semaine suivante, il s’installa sur le marché de la ville.  Ses notes savoureuses eurent un succès immédiat.
Tout excité, Trémolo se mit à l’œuvre. Il apprit à jouer sans boucher ses instruments. Il recueillait et triait les notes.
Puis il se lança dans différentes expériences. Il mijotait et touillait, mixait et distillait, mélangeait et goûtait : les airs d’amour fondaient sur la langue, les marches vous donnaient de l’énergie et les berceuses vous plongeaient dans la somnolence.
Lorsqu’il découvrit que sa musique enregistrée sur bande magnétique faisait jaillit les notes des haut-parleurs, il décida de se lancer dans la production alimentaire.
Trémolo devient célèbre et fut invité au show télévisé du fameux animateur Pivo Méli-Mélo.
Son passage à la télévision provoqua un cataclysme inimaginable. Les téléviseurs de tout le pays avalèrent les notes de travers et explosèrent, déversant une glu nauséabonde qui puait le vomi calciné.
Le studio ressemblait à un cloaque. Désemparé, Trémolo s’en alla.
Et alors. Plus de télé.
La vie reprit son cours, paisible comme jadis.
On racontait des histoires, on lisait des livres, on jouait en famille, on faisait de la musique.
Le soir les gens se réunissaient joyeusement.
La violence n’étant plus à la mode, on put voir, devant les commissariats de police, de longues files d’attente de voleurs, de kidnappeurs et de malfaiteurs en tout genre qui, contrits, venaient rendre leur butin et leurs armes.
Mais un jour, sans s’être fait annoncer, Madame Abrah Kadabrah débarqua dans le bureau de Trémolo.
« Espèce de tordu, d’abord tu me kasses ma boule de Kristal, et maintenant tu hexploses mon poste de téléfission. Ce n’est pas pour être ton porte-bonheur que je suis ton porte-poisse ! Je te retire ma malédiction. Hoc volo, sic jubeo ! »
Plus de sortilège, plus de production. Mais Trémolo s’en moquait. Il vendit l’usine et se fit construire une somptueuse salle de concert, en se disant : « La musique, en faim de compte, c’est quand même mieux dans les oreilles que dans l’estomac. »
