Joyeux anniversaire, Chien Pourri !
Mouche l’école des loisirs 11, rue de Sèvres, Paris 6e Dans sa poubelle, Chien Pourri s’ennuie et compte les mouches sur un vieux gâteau. Lorsqu’il s’apprête à dévoiler le résultat de son calcul de débile mental, il tombe sur un objet non identifié, le renifle, et le mange aussitôt.
- Chien Pourri, je crois que tu es en train d’avaler une bougie, le prévient Chaplapla.
- Oui, à la fraise ! se régale-t-il.
Mais soudain, le pauvre toutou se sent aussi seul qu’une bougie sans allumette : il réalise que personne ne lui a jamais souhaité son anniversaire, lui le chien préféré des chats d’égout et des rats dégoûtants.
- Chaplapla, tu es né quand, toi ?
- Le 13 mars.
- Comment le sais-tu ?
- Avant de m’abandonner, ma maman avait pris soin de m’envelopper dans du papier journal, la date était indiquée dessus. Et toi, Chien Pourri ?
- Je ne sais pas, ma maman m’a jeté directement dans une poubelle, sans emballage.
Pauvre Chien Pourri, il ne sait toujours pas s’il a sept ans ou deux cents ans, lorsqu’il reconnaît une voix familière.
- Alors Chien Pourri, il était bon, mon gâteau ? Désolé, je ne t’ai pas invité à mon anniversaire, ma poubelle était déjà pleine ! ricane le caniche à frange.
- En tout cas, préviens-nous quand tu feras le tien, qu’on ne vienne pas ! ajoute le basset à petit manteau.
- Ce sera un pourriversaire ! s’esclaffe le caniche à frange.
« Quelle bonne idée, pense Chien Pourri. Je vais fêter mon anniversaire.»
- Tu ne comptes tout de même pas inviter dans notre poubelle ? demande Chaplapla.
- Pourquoi pas ? Elle n’est pas plus sale qu’une autre !
- Mais tu ne connais même pas la date.
- On n’a qu’à le faire un dimanche, les anniversaires tombent souvent les week-ends.
Sur une boîte de pizza en carton, Chien Pourri écrit avec un reste de ketchup qui lui gicle dans l’œil : Chien Pourri t’invite dimanche à son anniversaire.
Viens déguisé si tu veux.
Mais soudain il ressent l’inquiétude de tout toutou qui célèbre pour la première fois le jour de sa naissance.
- J’ai peur, Chaplapla.
- De quoi, Chien Pourri ?
- Que personne ne vienne à mon anniversaire.
- Ne t’inquiète pas, nous allons faire la liste des invités : Jimmy le pigeon, Hector le rat, Sophie la girafe ?
« Ce n’est pas beaucoup, pense Chien Pourri, mon anniversaire risque d’être vraiment pourri ! » Mais devant eux, une petite fille qui attend pour jeter ses ordures, a tout entendu :
- Moi, je veux bien venir à ton anniversaire, mon toutou. D’habitude personne ne m’invite jamais.
- C’est gentil, comment t’appelles-tu ? demande Chien Pourri.
- Jeanne Défaite.
- Quel drôle de nom, remarque Chaplapla.
- J’ai été trouvée devant un stade de foot, le soir d’une grande défaite.
C’est pour cette raison. Mais je te préviens, je ne suis pas un cadeau !
- Pourquoi ? demande Chaplapla.
11
- Je ne suis pas populaire, voilà tout.
- Ça veut dire quoi ? demande Chien Pourri.
- Eh bien, par exemple, la Reine des Nouilles, elle, est très populaire. Il y a des gommes La Reine des Nouilles, des taille-crayons La Reine des Nouilles, des assiettes La Reine des Nouilles.
« Pauvre petite fille, elle veut devenir une gomme », pense Chien Pourri.
- Tu seras la bienvenue dans notre poubelle, la rassure Chaplapla. Je suis certain que tu seras très populaire.
À dimanche !
Chien Pourri complète sa liste d’anniversaire : Jimmy le pigeon, Hector le rat, Sophie la girafe ?
+ une gomme. e carton de pizza d’invitation Pour faire plaisir à son ami et pour qu’il y ait plus que deux tondus et trois pelés à sa fête, Chaplapla suggère de lancer des cartons d’invitation.
- Sur qui ? demande Chien Pourri.
- Mais non, idiot ! Lancer des invitations veut dire inviter du monde.
- Ah oui, bien sûr, je le savais un peu, dit Chien Pourri.
La bêtise peu commune de ce chien hors du commun, donne une idée originale à Chaplapla.
- Laissons flotter quelques invitations dans le caniveau. Avec le courant, des enfants les ramasseront.
Malheureusement, sitôt le premier carton de pizza d’invitation jeté dans le caniveau, il revient comme un boomerang !
- Alors, Chien Pourri, tu vas te déguiser en serpillière ou en paillasson ? demande un pitbull.
- Et comme cadeau, tu veux une boîte de mégots ? rigole un foxterrier.
- Ou une poubelle télécommandée ? ricane un chihuahua.
Mais les moqueries des petits chiens sont interrompues par la présence de l’enfant le plus populaire du quartier, celui que tout le monde voudrait avoir pour son anniversaire, le bien nommé : Jean Lafaite.
- Il n’a jamais voulu venir au mien, regrette le basset.
- Il n’aime pas trop les fêtes, depuis que sa maman l’a oublié à une soirée, confie le caniche à frange.
- Comment le sais-tu ?
- C’est mon coiffeur qui me l’a dit.
Mais le carton de pizza d’invitation de Chien Pourri intrigue le jeune Lafaite.
- Un anniversaire dans une poubelle ? Hum, pourquoi pas ? Ça me changera des anniversaires pourris gâtés.
- Oui, celui-ci sera pourri tout court ! l’encourage à venir Chaplapla.
« Il faut que je note ma date d’anniversaire pour m’en rappeler », pense Chien Pourri.
- Quel jour serons-nous dimanche, Chaplapla ?
- Dimanche.
- Mais le numéro ?
- Tu veux dire la date ? Le 2, je crois.
« C’est un bon chiffre, pense Chien Pourri, comme les deux doigts de la main, les deux cheveux d’un chauve, les deux parents d’un chien.» ussitôt, l’humeur de Chien Pourri redevient aussi sombre que la tasse de café qu’il se renverse sur les pattes. Lui qui commençait à se faire une joie de fêter son anniversaire, le voici aussi triste qu’un chien abandonné sur un trottoir en hiver, sous la pluie et dans le brouillard. Car même pour un anniversaire pourri, il faut une famille pour que la fête soit réussie. Alors, quand Chaplapla lui demande ce qu’il préférerait entre un déguisement de doberman ou une carte cocker-morne, Chien Pourri lui répond simplement :
- J’aimerais que tu m’aides à retrouver ma maman. a chercher le chat-chat !
La tête dans sa poubelle, Chien Pourri pense à sa maman :
- Elle devait être tellement moche, Chaplapla ! Elle me manque.
Chaplapla a beau être aussi plat qu’un œuf, son cœur est gros comme une pastèque.
Dès les premiers camions-poubelles, il colle des affichettes sur tous les arbres, les bancs et les réverbères de la ville.
- Tu es sûr pour la récompense ? s’inquiète Chien Pourri.
- Bien sûr ! Retrouver son petit, n’est-ce pas la plus belle des récompenses ?
- Qui est petit ?
- Toi, gros bêta !
« Je suis petit ou gros ? Faudrait savoir », pense Chien Pourri.
L’annonce de Chaplapla porte ses fruits, de nombreux parents pourris se présentent devant leur poubelle.
- J’ai enfermé mon enfant dans un placard, se vante une dame.
- J’oblige le mien à faire la vaisselle, à descendre les poubelles et à me lire le journal, dit une autre.
- Je mange tous les bonbons de ma fille, confie un monsieur.
Malheureusement, aucun chien n’est assez moche, aucune chienne n’est assez bête pour être de la famille de Chien Pourri. Une dalmatienne, à une seule tache, albinos, borgne et sourde, essaie de se faire passer pour une vieille tante.
- J’ai bien connu ta maman ! Si tu me donnes des croquettes, je te dirai où elle est.
- Ne te fais pas avoir Chien Pourri. C’est une vache déguisée en chienne !
Chaplapla veille sur son ami et doit bien se résoudre à lui avouer la vérité :
- Retrouver tes parents revient à chercher une arête dans un poisson surgelé.C’est une mission impossible !
Pauvre Chien Pourri, l’espoir de revoir sa maman s’efface comme une tache de dalmatienne balayée par la pluie. Mais il ne se doute pas que non loin de sa poubelle, Jean Lafaite fait la connaissance d’une famille particulièrement hideuse et qu’il s’écrie :
- Si c’est pas une chance, ça !
À moi la forte récompense ! ue Lafaite commence !
Le grand jour est arrivé. Chaplapla attend les premiers invités qui tardent à arriver. Sophie la girafe a décommandé, elle a eu une visite au zoo de dernière minute. Hector le rat, lui, a préféré se rendre à un goûter dans les égouts.
Seul Jimmy le pigeon a réussi à sortir de son nid et a apporté de la mie de pain ramassée sur le trottoir.
- On devrait annuler, dit tristement Chien Pourri.
- Non, tant qu’il y a de la mie, il y a de l’espoir ! Regarde qui nous fait le plaisir de venir : la petite fille impopulaire.
Jeanne Défaite est à l’heure, mais elle a la mine défaite.
- Je n’ai pas le droit de manger de bonbon, j’ai des caries, soupiret-elle tristement.
- Ne t’inquiète pas, la rassure Chaplapla, ici, il n’y a pas de sucreries, c’est un anniversaire pourri !
- Même pas un su-sucre ? s’inquiète Chien Pourri.
Le populaire Jean Lafaite, fait aussi son entrée et traîne derrière lui trois énormes sacs-poubelles.
- Ce sont des cadeaux ? demande Chien Pourri.
- Si on te le demande…
- Ah bon ? Ce n’est pas moi qui le demande ? ! s’étonne-t-il, ahuri.
La venue de Jean Lafaite est un événement. La nouvelle se propage aussi vite qu’un feu dans une poubelle. Soudainement, tous les chiens, les rats et les pigeons du quartier souhaitent célébrer la naissance du plus pourri des toutous.
Jean Lafaite en connaisseur des fêtes, recherche les attractions.
- Où est la pêche aux canards ?
- On peut en faire une dans le caniveau, propose Chaplapla.
- Si j’attrape un canard, je pourrai le manger ? demande Chien Pourri.
Chaplapla n’a pas le temps d’expliquer les règles de ce jeu si apprécié des enfants, Jeanne Défaite vient de décrocher le gros lot !
- Je l’ai, je l’ai ! hurle-t-elle.
- Qu’est-ce que c’est ? Un canard laqué ? demande Chien Pourri.
- Non, une baguette de fée La Reine des Nouilles ! Je suis une fée, je suis une fée !
- Qui est cette nouille ? ! demande Jean Lafaite à Chaplapla.
- Je suis Jeanne Défaite, j’ai été trouvée dans un stade, un soir de grande défaite.
- Et moi, j’ai été oublié aux vestiaires le soir d’une grande fête, mais je ne raconte pas ma vie !
Chien Pourri, lui, est ravi d’avoir une fée pour son anniversaire.
- Vous pouvez faire apparaître ma maman ? demande-t-il.
- Pour ça, il faudrait plutôt un magicien, répond la petite fée, embêtée.
- Ça tombe bien, vous en avez un devant vous ! claironne Jean Lafaite.
« J’ai demandé un magicien et il est apparu, je suis une vraie fée ! » se dit Jeanne Défaite.
« Elle est complètement nouille », pense Jean Lafaite.
« Je vais revoir ma maman », espère Chien Pourri. e magichien d’Auge Jean Lafaite grimpe sur la poubelle de Chien Pourri avec ses grands sacs plastique.
« Il pourrait attendre pour jeter ses ordures », pense Chien Pourri.
Mais le brave chien ne sait pas qu’une surprise de taille se prépare.
- Ce n’est pas un spectacle pour enfants, prévient Jean Lafaite.
- Il va faire apparaître un manteau sans manche ? se demande horrifié, le basset.
La petite fée joue les assistantes.
Sitôt le premier sac-poubelle ouvert, des cris surgissent parmi les invités.
- C’est un pigeon écrasé ? demande Jimmy le pigeon.
- Le yeti ? s’interroge le caniche à frange.
- Je sais ! C’est une peluche Monster ! crie le basset.
- Non, mesdames et meschiens, cette chose infâme, n’est ni une peluche, ni un minable homme des neiges, c’est une chienne en chair et en nonos ! Je dirai même plus, c’est une maman ! indique Jean Lafaite.
« J’espère que ce n’est pas la mienne, pense Chien Pourri. Elle est vraiment horrible.»
- Seule une mère reconnaîtra son petit, prédit le jeune magicien.
Alors : à qui est le chienchien de cette mémère ?
Tous les visages se détournent au passage de cette bête immonde qui se faufile entre les pattes des invités, lorsque tout à coup, l’affreuse créature s’arrête net devant Chien Pourri.
- Pupuce ! Mon fils ! Je t’ai reconnu à l’odeur !
- Maman ? Tu es encore plus moche que je ne l’avais imaginé !
- Vous êtes vraiment sa mère ? demande Chaplapla.
- Ben oui, je ne suis pas son père ! Viens Pupuce que je te présente le reste de ta famille.
La petite fée détache les deux autres sacs-poubelles. Un papa et un frère viennent compléter l’immonde famille de Chien Pourri.
- Je suis Affreux, dit son père.
- C’est vrai, dit Chien Pourri.
- Et moi je suis Sale, ajoute sa mère.
- J’avais remarqué.
- Non, Sale, c’est mon prénom, banane !
- Je suis Méchant, grogne son frère.
- Ah ? Alors, tout doux le chien ! fait Chien Pourri.
« Affreux, Sale et Méchant ! J’ai déjà entendu ça quelque part, pense Chaplapla. C’est louche.» Chien Pourri, lui, louche avec amour sur sa maman.
- Et maintenant Pupuce, faisnous visiter ta poubelle, commande Sale.
- Et donne tes cadeaux à ton frère ! menace Affreux.
- Mais, vous êtes mes cadeaux ! dit Chien Pourri.
- Tu vis avec un chat ? s’étonne Sale, en découvrant Chaplapla.
- Oui, mais il ne prend pas beaucoup de place, il ne gêne pas.
- Je n’aime pas les chats, ça met des poils partout.
- Surtout sur les saucisses, affirme Affreux.
- Je peux le mordre ? demande Méchant.
« La famille de Chien Pourri est vraiment trop pourrie ! » pense Chaplapla.
’est toi le chat !
Chien Pourri n’est pas le seul à profiter de ses parents. Les petits invités se jettent sur eux comme autant d’attractions.
- Je veux jouer avec l’Affreux papa ! dit le basset.
- Et moi avec la Sale maman, dit le caniche à frange.
- Je veux essayer le Méchant frère, clame le pitbull.
- Calmez-vous, je vais distribuer des tickets. Vous allez jouer chacun votre tour, propose Jean Lafaite.
Rapidement l’attraction de l’Affreux papa fait merveille. Le basset qui n’a pas l’habitude qu’on lui hurle dessus demande trois tours d’Affreux papa.
- Mets ton manteau et que ça saute ! crie Affreux.
- Oui papa, dit le basset.
« Je ne comprends pas. Mon papa est aussi le papa du basset ? » se demande Chien Pourri.
- Maintenant, enlève ton manteau et plus vite que ça ! hurle l’Affreux papa.
- Oui papa.
- Je ne suis pas ton père, vieux cabot !
« Ah ! je me disais aussi », pense Chien Pourri, soulagé.
D’autres attractions sont prévues, Chaplapla organise une chasse aux trésors pour son ami. Chacun des invités repartira avec un petit sacpoubelle.
- J’espère trouver une gomme La Reine des Nouilles, dit la petite Jeanne en agitant sa baguette de fée.
- Moi aussi ! dit Jean Lafaite.
- Ah oui ?
- Bien sûr, pour pouvoir t’effacer tellement t’es nouille ! ricane-t-il.
Mais Jean Lafaite veut repartir avec bien davantage qu’un sacpoubelle rempli de petits trucs et de mini-bidules, il veut sa forte récompense.
- Mi-nou, mi-nou, où es-tu ?
« Je n’ai rien à lui offrir », s’inquiète Chaplapla.
Pour détourner son attention il propose de jouer à chat, mais le caniche refuse qu’on lui touche la frange, le basset qu’on lui tire le manteau, le pitbull préfère mordre que toucher et le fayot de labrador dit que si quelqu’un le touche, il le dira à sa maman.
- On peut faire une baballe au prisonnier ? propose Chien Pourri.
Mais Jean Lafaite n’est pas venu pour jouer.
- Le chat, ou tu me donnes ma récompense ou je te transforme en piniachat !
Chaplapla fouille dans la poubelle et fait le tri des objets collectés depuis des années. Trois lampes sans ampoule, une petite voiture sans roues, un pot de moutarde vide et une clé sans coffre-fort, constituent son seul trésor.
- Tu peux prendre ce que tu veux, propose-t-il.
- Tu te fiches de moi ! répond Jean Lafaite.
- Je n’ai rien d’autre.
- Alors, je reprends les parents de Chien Pourri, à moins…
- À moins ? répète Chaplapla.
- J’ai toujours rêvé d’avoir un chat !
Chaplapla est fait comme un rat !
« Chien Pourri a l’air tellement content, je ne peux pas le priver de sa famille », pense-t-il.
- Seule une fée pourrait m’aider, dit-il à voix haute.
- Je suis une fée, je suis une fée, sautille de joie Jeanne Défaite. Que puis-je faire pour toi, petit chat ?
- Un miracle, soupire Chaplapla en traînant la patte.
- Tu veux un gâteau à la sardine ?
- Non, je souhaite rester toute ma vie avec Chien Pourri, dit-il tristement. a sardine sur le gâteau En attendant l’heure du gâteau, Chien Pourri rattrape le temps perdu avec ses parents.
- Dis, maman, tu me fais un câlin ?
- Plutôt crever ! s’exclame-t-elle.
- Ah bon ? Plus tard alors.
- C’est ça, le plus tard possible !
Chien Pourri profite de la présence de son affreux papa, pour lui demander ce que tout toutou rêve de demander un jour à son père :
- Dis papa, il y a le caniche qui m’embête-eu, il dit que ma frange-eu est moche-eu !
- Qu’est-ce que tu veux que ça me fasse ? Débrouille-toi avec tes cheveux !
« Une éducation à la dure, j’adore !
Mes parents sont pourris mais justes.» Vient alors l’heure des souvenirs délicats, autour d’un verre de jus de pomme, apporté par le Saint-Bernard.
- Dis maman, raconte-moi encore comment tu m’as abandonné !
- Euh, je ne sais plus Pupuce, je t’ai laissé sur le bord de la route ?
- Non, c’était dans une poubelle.
- Ah oui, parce que tu sentais trop des pieds ?
- Je ne crois pas.
- Parce que tu étais trop moche ?
- Mais non maman, rappelle-toi !
- Parce qu’il n’y avait pas de videordures ?
Pauvre Chien Pourri. Une vraie maman se serait souvenue de la raison.
C’est parce qu’il sentait trop la sardine que sa maman l’avait abandonné, mais Chien Pourri est si naïf qu’il n’en veut pas à la chienne immonde qui l’a mise au monde.
Soudain, une nouvelle peur étreint Chien Pourri.
- J’espère que j’aurai assez de souffle pour éteindre mes bougies.
- Si tu fais un vœu et que tu les éteins toutes en une fois, il se réalisera, dit la petite Jeanne.
- Demande un nouveau cerveau, conseille le caniche à frange.
- Ou bien une gomme La Reine des Nouilles, suggère la petite Jeanne.
Chaplapla, lui, n’a plus le choix.
Pour que Chien Pourri garde sa maman, il doit se sacrifier. Alors, pour la première fois de sa vie, il joue un tour de cochon à son compagnon, en lui proposant de jouer à colinmaillard dans sa poubelle.
Chien Pourri se cache les yeux avec une vieille chaussette.
- Hou ! Hou ! les copains, où êtes-vous ?
Mais Chaplapla reste dehors et referme aussitôt le couvercle et dit :
- Toi Pitbull, s’il sort, tu le mords ! Jean Lafaite, emmène-moi avant que je ne change d’avis.
- Merci pour la récompense ! répond celui-ci.
- Hou ! Hou ! Chaplapla, je sais que c’est toi ! Ah ! non, c’est un dessous-de-plat, dit Chien Pourri.
- Adieu mon Chien, chuchote Chaplapla, en s’en allant.
- Chaplapla, où es-tu ?
- Je suis dans une cage, répond-il.
« Chaplapla a dû boire trop de jus de pomme, il se prend pour un lion », pense Chien Pourri.
Mais c’est plutôt lui, le lion en cage, prisonnier de sa poubelle.
- Quelqu’un peut allumer un lampadaire ? Je ne vois rien ! crie-t-il.
Mais personne ne vient à son secours.
À peine Chaplapla est-il parti, qu’Affreux, Sale et Méchant décident de s’en aller à leur tour.
- Ce n’est pas qu’on s’ennuie, mais on n’a pas que ton anniversaire à piller, euh… à souhaiter, dit Affreux.
- Oh maman, si toi aussi tu m’abandonnes…
- Tout de suite les grands mots, reste bien au chaud dans ta poubelle Pupuce !
- On reviendra un jour, dit Affreux.
- Jeter nos ordures ! ajoute Méchant.
Quand Chien Pourri réussit enfin à s’extraire de sa poubelle, en proposant au pitbull de mordre une boîte de conserve plutôt que sa jambe, il ne peut que constater que la fête est finie. Chaplapla n’est plus là, sa maman est partie et il n’a même pas eu son gâteau. appy birthday toutou !
Les derniers invités quittent les lieux. Quelques parents retardataires repartent avec leurs petits.
- Ta frange est toute décoiffée ! râle la maman du caniche.
- C’était nul, il n’y avait pas de portemanteau, se plaint le basset à petit manteau.
- C’était super, dit le labrador.
- Fayot ! dit le caniche.
Seule la petite fée accepte de rester au côté de Chien Pourri pour l’aider à ranger.
Car, dans une vie de chien, il n’y a pas que des baguettes de pain trempées dans le caniveau, il y a aussi des baguettes de fée dans les mains d’enfants merveilleux.
- Abracadabra, que ta poubelle s’ouvre à toi ! lance la petite fée.
« C’est vrai qu’elle est un peu nouille », pense Chien Pourri.
- Peut-être trouveras-tu de nouveaux cadeaux, mon toutou.
- Je n’ai vu personne jeter de nouvelles ordures, remarque Chien Pourri.
«Tiens, un lacet c’est bizarre, il me rappelle quelque chose, je connaissais une petite fille aux souliers rouges qui avaient les mêmes ».
- Oui, Chien Pourri, et c’est toi qui m’avais aidée à retrouver ma maman, dit une voix derrière sa poubelle.
- Un lacet qui parle ? Tu es un lacet magique ? demande-t-il au lacet.
Mais comme celui-ci reste coi, Chien Pourri sort la tête de sa poubelle et aperçoit la petite fille aux souliers rouges.
- Que fais-tu ici ?
- J’ai trouvé ton carton d’invitation dans le caniveau. Je ne pouvais pas laisser passer ton anniversaire.
- Oh, que c’est gentil petite fille !
- Moi, je suis venue avec ma poussette et des salsifis, dit une autre petite fille. Tous les caniveaux mènent à Chien Pourri.
- Poucette, c’est toi ? demande Chien Pourri.
- Oui, je t’ai aussi apporté une Vache qui rit.
Chien Pourri n’est pas au bout des surprises, car dans sa poubelle une PSB l’attend.
- Une poupée sans bras ? !
Marie-Noëlle et Jean-Noël entourent sa poubelle.
- Joyeux Noël, euh… joyeux anniversaire !
Chien Pourri n’en croit pas ses puces, tous ses amis sont venus à sa fête.
Short Bleu est accompagné de son papa, le CRS.
- Tu m’as sauvé la vie, vieux toutou, je ne pouvais pas rater ce jour, dit Short Bleu.
- J’espère que tu as mis tes brassards pour plonger dans ta poubelle, s’inquiète le CRS.
Même la maîtresse de l’école Royal clebs est au rendez-vous.
- J’ai copié sur les autres, confiet-elle.
Sanchichi est rentrée de sa tournée américaine, sans Podvache. La colonie des oubliés, elle, est au complet.
- Oh, le basset, toi aussi, tu es revenu ? s’étonne Chien Pourri.
- Non moi, je suis venu rechercher mon manteau, je l’avais oublié sur ta poubelle.
Mais pendant que le célèbre Aristide Bruyant qui lui avait fait visiter Paris en camion-poubelle, braille aux passants : «  Venez visiter la fameuse poubelle de Chien Pourri ! », le brave toutou a un nouveau coup de mou et ne pense qu’à son ami disparu.
- Un seul chat vous manque et tout est dépeuplé, dit-il tristement à la petite fée.
- Je ne pense pas que ma baguette puisse faire quoi que ce soit pour toi.
Et pourtant, au coin d’un réverbère apparaît une forme reconnaissable entre toutes : un œuf sur le plat, autrement dit un Chaplapla.
- Mon chat ! crie Chien Pourri.
- Mon chien ! hurle Chaplapla.
- Je te le rends, dit Jean Lafaite.
Ce chat ne vaut rien, il ne ronronne pas !
Chien Pourri remue la queue, Jeanne Défaite agite sa baguette :
- C’est le plus beau jour de ma vie, avec celui où j’ai trouvé ma baguette dans le caniveau, d’ailleurs c’est le même jour ! Hi, hi ! Je suis un peu nouille.
Mais elle ne sait pas qu’une plus grande surprise et qu’une plus grande nouille, l’attend encore. Une dame l’air égaré, tient dans sa main une petite affichette.
- C’est ici l’anniversaire de Chien Pourri ? demande-t-elle.
- Oui, mais vous arrivez un peu tard, remarque Chaplapla. Qui êtesvous ?
- J’ai trouvé cette annonce sur une poubelle, Recherche parents pourris, alors, je suis venue. Je suis la plus pourrie des mamans ! Un jour, j’ai oublié ma fille dans un stade de foot, et mon fils à une fête. Depuis, je les cherche. Je suis la reine des nouilles !
- Maman ! crie la petite fée.
- Maman ! hurle le petit magicien.
- Maman ? ! demande Chien Pourri.
- Mes enfants ! dit la reine des nouilles.
Pour célébrer leurs retrouvailles, la maman offre à ses deux enfants une gomme La Reine des nouilles.
- J’en ai toujours une sur moi pour les grandes occasions !
- Jamais je n’effacerai ce moment, dit Jean Lafaite. Pardon Jeanne, j’ai été nouille de te traiter ainsi.
- Bah, les nouilles c’est de famille, dit leur maman. D’ailleurs ce soir, je vais vous faire des pâtes !
« Cette histoire va me rendre très populaire dans mon école », se réjouit la petite Jeanne.
Chaplapla est impatient d’offrir son cadeau à son meilleur ami, le journal de sa naissance :
- La date exacte a été effacée mais tu es bien né un dimanche Chien Pourri !
’immonde
- Merci Chaplapla, quel beau cadeau !
Mais Chaplapla doit maintenant avouer la triste vérité à son ami, celle qu’il a lue dans le journal du jour, oublié sur un banc.
Le Parichien Une famille de Rapetoutou est recherchée par la pouliche.
Si vous les apercevez, appelez le 0800 12 12
- Chien Pourri, il faut que je t’avoue quelque chose…
- Ne te fatigue pas, Chaplapla, j’ai compris depuis longtemps.
- Que ta maman n’était pas ta maman ?
- Non, que les saucisses étaient surgelées. Mais maintenant que tu me le dis, ma maman m’appelait toujours Pupuce, tout le monde sait que mon surnom c’est Poupou ! Ça aurait dû me mettre la puce à l’oreille.
- Tu n’es pas trop déçu ?
- Non, Chaplapla, une maman même pour un jour, c’est mieux que rien. Et puis, ma vraie famille, c’est toi !
Chien Pourri peut enfin compter ses bougies d’anniversaire :
- Trois bougies plus quatre mouches moins cinq croquettes plus deux chaussettes égale ?
- Chaplapla, ce calcul est trop compliqué. Il faut être un polytechnichien, je ne connaîtrai jamais mon âge !
- Bah, l’âge c’est dans la tête, vieux toutou.
- Ah bon, ce n’est pas le cerveau qu’il y a à cet endroit ?
En soufflant sur ses bougies et ses chaussettes, Chien Pourri fait le vœu de ne plus jamais quitter Chaplapla. Les invités promettent de revenir chaque année, toujours un peu plus nombreux ! Ils peuvent désormais chanter tous en chœur et dans la joie :
- Joyeux anniversaire, Chien Pourri, nos vœux les plus pourris !
79