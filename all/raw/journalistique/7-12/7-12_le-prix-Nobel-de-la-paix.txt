N°26 - 6 au 12 octobre 2017
le prix Nobel de la paix
Le prix Nobel de la paix a été remis ce vendredi 6 octobre à Oslo, en Norvège. Il récompense chaque année des gens qui participent à faire la paix dans le monde. Mais qu’est-ce que c’est, le prix Nobel de la paix, et d’où ça vient ? Qui sont les gagnants les plus célèbres ? Et pourquoi le résultat ne fait-il pas toujours plaisir à tout le monde ? Je t’explique tout.


Kehkashan, 17 ans, a reçu un prix de la paix

Kehkashan vient des Emirats arabes unis, à plus de 5 000 kilomètres de la France. Aujourd’hui, elle vit au Canada et est élève en terminale. En 2016, elle a reçu le Prix international de la paix des enfants, pour ses actions en faveur de l’environnement. Une sorte de prix Nobel de la paix pour les jeunes.
Kehkashan a commencé à protéger l’environnement très jeune, à 8 ans seulement. Après avoir vu la photo d’un oiseau mort à cause de la pollution (il avait mangé du plastique), elle a décidé de défendre la nature en plantant un arbre. Puis elle a encouragé les gens autour d’elle à protéger la planète.
Ses actions ont été remarquées, et pas seulement à Dubaï, la ville où elle habitait : à l’âge de 11 ans, elle a été invitée à un grand événement organisé par le programme des Nations unies pour l’environnement. A 12 ans, elle a pris la parole lors d’une grande réunion internationale au Brésil. Elle était la plus jeune. «Notre futur était en train de se décider et même si nous, les enfants, étions les principaux intéressés, nous n’étions que des observateurs. J’ai décidé que ça devait changer», raconte-t-elle.
La jeune Emiratie a ensuite créé sa propre association, Green Hope, désormais présente dans plusieurs pays, comme les Emirats arabes unis, Oman, le Népal, l’Inde et le Canada. «Je suis convaincue que les enfants sont tout à fait capables de faire ce qu’un adulte peut faire, assure-t-elle. J’ai l’impression que mon travail ne fait que commencer.»
Elle a même écrit un livre pour les enfants, The Tree of Hope (ça veut dire «l’arbre de l’espoir» en français). Il raconte l’histoire d’une petite fille qui plante des arbres dans le désert pour le transformer en oasis.
Kehkashan vient des Emirats arabes unis et habite au Canada


Qui a gagné le prix Nobel de la paix ?

Le prix Nobel de la paix a été remis vendredi 6 octobre à un groupe de gens qui veulent que les armes nucléaires disparaissent. Ce groupe s’appelle la Campagne internationale pour l’abolition des armes nucléaires.
Ces armes sont très particulières. Il s’agit de très grosses bombes qui peuvent tuer énormément de gens d’un coup. Dans toute l’histoire, elles n’ont été utilisées que deux fois. C’était pendant la Seconde Guerre mondiale, en 1945. Cette année-là, les Etats-Unis s’en sont servis contre deux villes japonaises, Hiroshima et Nagasaki. Les bombes ont explosé dans le ciel et elles étaient tellement puissantes qu’elles ont tué des dizaines de milliers de personnes.
Lorsqu’une bombe nucléaire explose, il fait extrêmement chaud, donc des gens sont brûlés et il y a des incendies. En plus, ce type de bombe contient des produits dangereux qui ont des conséquences sur la santé pendant très longtemps. Donc des Japonais qui avaient survécu aux bombes en 1945 mais qui habitaient dans la zone touchée ont eu des maladies graves, comme des cancers, même des années plus tard.
Mais alors, si ces armes n’ont été utilisées que deux fois, il y a plus de soixante-dix ans, pourquoi en parle-t-on encore ? Parce que, aujourd’hui encore, elles ne sont pas interdites. Et certains menacent de s’en servir.
C’est le cas de la Corée du Nord. Ce pays d’Asie n’a pas encore l’arme nucléaire, mais il aimerait bien. Son chef, qui s’appelle Kim Jong-un, fait plein d’expériences avec des scientifiques et son gouvernement pour réussir à en créer une. Il dit qu’il s’en servira contre d’autres pays, comme les Etats-Unis, si la Corée du Nord est attaquée.
La Campagne internationale pour l’abolition des armes nucléaires a obtenu le prix Nobel de la paix parce qu’elle demande qu’on interdise ce type de bombes. Au mois de juillet, elle a réussi à convaincre 122 pays de s’y opposer.


Qu’est-ce que le prix Nobel de la paix ?

Le prix Nobel de la paix est une récompense donnée chaque année à des gens qui ont mené une action en faveur de la paix dans le monde. Ça peut être une ou plusieurs personnes ou une organisation.
Il porte le nom de l’homme qui l’a créé, Alfred Nobel. C’était un scientifique suédois qui a inventé la dynamite. A l’origine, on utilisait cet explosif pour récupérer des matériaux dans le sol, comme le charbon, mais beaucoup de gens sont morts parce que la dynamite explosait à des moments où ils ne s’y attendaient pas. En plus, certaines personnes s’en servaient comme d’une arme. Grâce à son invention, Alfred Nobel est devenu très riche mais il n’avait pas une bonne réputation aux yeux des gens.
Alors, pour changer son image de «marchand de la mort», comme on l’a surnommé, Alfred Nobel a demandé qu’après sa mort on crée un prix pour récompenser les personnes qui aident à faire le bien sur la planète. Il voulait qu’une partie de son argent soit distribuée à ces hommes et ces femmes chaque année. Voilà comment le prix Nobel de la paix est né, en 1901.
D’autres prix Nobel ont été créés en même temps, pour récompenser des gens qui font des choses positives en littérature (les livres), physique, chimie et médecine.
Chaque année, le nom du vainqueur du prix Nobel de la paix est annoncé au début du mois d’octobre. Il ou elle récupère son prix le 10 décembre à Oslo, en Norvège. Il y a une cérémonie et le gagnant prononce un discours de remerciement pour expliquer ses actions. Il reçoit une médaille, un diplôme et de l’argent (environ 900 000 euros).
On ne peut pas envoyer soi-même une candidature pour espérer gagner le prix Nobel de la paix, il faut que quelqu’un d’autre le fasse. Et pas n’importe qui : ça ne peut venir que de ministres, chefs d’Etat, professeurs d’université ou d’anciens gagnants du prix. Ils envoient un courrier au comité Nobel norvégien pour expliquer pourquoi une personne mérite la récompense.
Cette année, 318 candidatures ont été envoyées. C’est beaucoup, mais c’est moins qu’en 2016, le record, avec 376 dossiers. Le comité met plusieurs mois à décider à qui donner le prix. Les cinq personnes qui en font partie étudient toutes les propositions et sélectionnent seulement une dizaine de finalistes.
Il arrive parfois que personne ne reçoive le prix Nobel de la paix. Cela s’est produit pendant la Première Guerre mondiale (entre 1914 et 1918) et la Seconde Guerre mondiale (1939-1945), à cause des combats. D’autres fois, le prix n’a pas été donné car le comité pensait qu’aucun candidat ne le méritait.


Quelles personnalités ont marqué le Nobel de la paix ?

Depuis sa création en 1901, le prix Nobel de la paix a été remis 97 fois. Parmi les vainqueurs, on ne compte que 16 femmes. Treize fois, la récompense est allée à une organisation, comme la Croix-Rouge ou l’Unicef.
Voici quatre lauréats très célèbres.
Martin Luther King était un pasteur américain. Il a lutté, dans les années 1950 et 1960, pour que les Noirs aient les mêmes droits que les Blancs aux Etats-Unis. Il a prononcé un discours très connu, «I have a dream» («j’ai un rêve», en anglais), où il raconte qu’il rêve d’un monde où toutes les femmes et les hommes sont égaux et se respectent. Quatre ans après avoir eu le prix Nobel de la paix, en 1968, il a été tué par un homme opposé à ses idées.
Mère Teresa est née en Albanie, mais elle est devenue indienne. Elle avait choisi de vivre en respectant les règles de l’Eglise catholique. Elle a consacré toute sa vie à aider des gens pauvres et malades en Inde. Grâce à elle, des milliers de personnes, dont des enfants, ont pu être soignées. Mère Teresa est décédée en 1997.
En Afrique du Sud, Nelson Mandela était considéré comme le père du peuple. Dès son plus jeune âge, cet homme noir a lutté contre le racisme. Dans son pays, où vivaient surtout des Noirs, des Blancs étaient au pouvoir. Ces derniers avaient mis en place l’apartheid, un système qui séparait les Noirs et les Blancs, considérait que les Blancs étaient supérieurs et leur donnait plus de droits.
Pour s’être opposé à ça, Nelson Mandela a été emprisonné pendant 27 ans, jusqu’en 1990. L’année suivante, l’apartheid a été abandonné. Nelson Mandela est devenu président de l’Afrique du Sud en 1994. Il a reçu le prix Nobel de la paix en 1993 pour avoir aidé à mettre fin à l’apartheid.
Malala Yousafzai (on l’appelle souvent seulement par son prénom, Malala) est la plus jeune à avoir reçu le prix : elle n’avait que 17 ans ! A l’âge de 10 ans, cette Pakistanaise s’est mise à raconter à quoi ressemblait la vie avec les talibans, qui ont envahi sa région. Les talibans sont des hommes qui imposent des règles de vie très dures à la population, en disant que c’est ce que l’islam veut. Ils avaient interdit aux gens de regarder la télévision et d’écouter de la musique et les petites filles n’avaient pas le droit d’aller à l’école. Malala n’était pas d’accord avec tout ça et a décidé de le dire.
Son histoire a fait le tour du monde, ce qui n’a pas plu aux talibans, qui ont tenté de la tuer. Depuis, Malala vit en Angleterre. Elle a créé une association pour que les petites filles à travers le monde aillent à l’école.
Les pays dans lesquels ces quatre Prix Nobel de la paix ont agi


Pourquoi le résultat ne plaît-il pas toujours ?

Parfois, il arrive que l’annonce du gagnant du prix Nobel de la Paix fasse polémique. C’est ce qu’il s’est passé en 2009, lorsque Barack Obama a été récompensé. Cela faisait seulement quelques mois qu’il était président des Etats-Unis. Le comité l’a choisi pour avoir voulu aider les peuples du Moyen-Orient à mieux s’entendre.
Mais beaucoup de gens étaient contre ce choix car les Etats-Unis faisaient à ce moment-là encore la guerre en Irak et en Afghanistan. Certains ont même dit que Barack Obama avait plutôt gagné le prix Nobel de la guerre que celui de la paix. Le comité Nobel, chargé de choisir le lauréat, a dû donner des explications pour défendre son choix.
En 2012, c’est l’Union européenne qui a gagné le prix, pour avoir travaillé pendant plus de soixante ans à faire en sorte qu’il y ait la paix et la liberté en Europe. Ce qui a étonné, c’est que les 27 pays de l’Union européenne n’étaient pas très solidaires entre eux et ne montraient pas vraiment la fraternité pour laquelle ils avaient été récompensés.
Il arrive aussi que le lauréat du Nobel de la paix ne soit pas critiqué sur le moment mais plusieurs années après. C’est le cas de la Birmane Aung San Suu Kyi. Pendant des années, son pays, situé en Asie, était dirigé par des militaires qui faisaient beaucoup de mal aux habitants. Aung San Suu Kyi a été la porte-parole des opposants à ces militaires, ce qui lui a valu d’obtenir le Nobel de la paix en 1991.
Mais des centaines de milliers de personnes ont signé une pétition pour qu’on lui retire son prix. Pourquoi ? Parce qu’elles pensent qu’elle ne le mérite plus. Aung San Suu Kyi est aujourd’hui cheffe du gouvernement de Birmanie, un pays où les gens sont majoritairement bouddhistes. Elle est accusée de ne rien faire pour aider les Rohingyas, des musulmans qui habitent le pays mais qui sont victimes de violences à cause de leur religion, et sont obligés de fuir.
Le comité qui donne le prix a fait savoir qu’il était impossible de reprendre la récompense, car Alfred Nobel n’a pas laissé de consignes à ce sujet.


