N°64 - 29 juin au 5 juillet 2018
la sécurité routière
Dès dimanche, les voitures vont devoir rouler moins vite sur certaines routes en France. Le but : diminuer le nombre de personnes tuées dans des accidents. Cette nouvelle règle s’ajoute à d’autres qui doivent être respectées sur les routes pour que chacun soit en sécurité. Mais pourquoi limite-t-on la vitesse ? Comment protège-t-on les gens et à quoi doit-on faire attention en voiture, à pied ou à vélo ? Prends la route avec moi !


Jordan, 24 ans, a eu un grave accident de moto

Jordan avait 2 ans et demi quand il a fait du ski pour la première fois. Très vite, ce sport est devenu une passion.
Mais à l’âge de 15 ans, il a eu un grave accident alors qu’il conduisait une moto : «Je ne me suis pas arrêté à un panneau stop, raconte-t-il. J’ai cogné une voiture et j’ai fait un vol de 17 mètres. Ma jambe s’est coincée entre la moto et la voiture.» Les médecins ont dû couper la jambe gauche de Jordan en dessous du genou.
«Quand j’ai appris à l’hôpital que j’avais été amputé, c’était un peu bizarre», se souvient-il. «La première chose qui m’est venue à l’esprit, c’est de demander si je pourrais skier à nouveau un jour.» Son médecin lui avait répondu qu’il pourrait skier sur ses deux jambes, grâce à une prothèse. «Ça m’a réconforté. Grâce à ça, j’ai très vite accepté mon handicap.»
Plus tard, il s’est inscrit dans un club de ski réservé aux sportifs handicapés, en équipant sa prothèse d’une chaussure adaptée. Puis il a rejoint l’équipe de France de ski paralympique et a participé aux Jeux paralympiques de PyeongChang, en Corée du Sud, en mars dernier. «J’y ai passé des moments extraordinaires, magiques. Je suis vraiment content d’avoir réalisé ce rêve.»
Malheureusement, il n’a pas remporté de médaille. Mais il s’entraîne déjà pour en gagner une aux prochains JO, en 2022. «Je n’aurais jamais pensé être un sportif de haut niveau sans mon accident. Avec du recul, cet accident a changé beaucoup de choses dans ma vie. Et je ne regrette pas du tout la voie que j’ai choisie», assure Jordan.
Jordan habite à Caluire-et-Cuire (Métropole de Lyon) et s’entraîne au ski à Gruffy (Haute-Savoie)


Que va-t-il se passer sur les routes ?

A partir du dimanche 1er juillet, les voitures devront rouler moins vite sur certaines routes de France. La vitesse maximum sera de 80 kilomètres par heure (km/h) au lieu 90 km/h sur ces routes situées, pour la plupart, en dehors des villes, à la campagne.
Cette nouvelle règle s’applique plus précisément sur les routes à double sens, sur lesquelles deux voitures peuvent se croiser. Celles dont les voies ne sont pas séparées par un muret ou une barrière mais seulement par une ligne blanche.
C’est le gouvernement qui a pris cette décision. Grâce à ça, il espère réduire le nombre de morts. «Ça va permettre de sauver des vies car la vitesse provoque des accidents et peut rendre ces accidents plus graves», se réjouit Emmanuel Renard, de l’association Prévention routière.
D’autres pensent au contraire que ça ne changera rien. C’est par exemple le cas des associations d’automobilistes ou de motards, qui manifestent depuis plusieurs mois. Certains ruraux trouvent cette décision injuste parce qu’ils doivent utiliser ces routes tous les jours, pour aller au travail ou à l’école. Et ils n’ont pas envie de rouler moins vite, sinon leurs trajets dureront plus longtemps.
«Les personnes qui vivent à la campagne n’ont que leur voiture pour se déplacer car il y a très peu de transports en commun sur ces territoires», explique Christophe Jerretie, député de la Corrèze, un département rural.
Pour lui, les accidents ont aussi lieu parce que certaines routes sont trop abîmées. Pour réduire le nombre d’accidents, il pense qu’il faudrait surtout que les routes soient mieux entretenues.


Pourquoi limite-t-on la vitesse sur les routes ?

Parce que la vitesse est la première cause des accidents mortels en France. Rouler trop vite est en effet dangereux. D’abord, on ne voit pas les choses de la même façon quand on va plus vite. Plus un conducteur accélère, plus son regard se fixe sur ce qu’il y a droit devant lui, au loin. Sur les côtés, sa vision devient floue : il voit donc moins bien les dangers.
Ensuite, quand on va vite, la voiture met plus de temps à s’arrêter en cas de danger. Il faut compter le temps de réaction du conducteur (environ une seconde) et ajouter à ça le temps d’arrêt de la voiture (plusieurs mètres).
Une voiture qui roule à 50 kilomètres par heure (km/h) s’arrête au bout de 28 mètres. A cette vitesse-là, le choc avec un obstacle peut être très violent : ça correspond à une chute de 3 étages. Si la voiture va deux fois plus vite, à 100 km/h, elle mettra 4 fois plus de temps avant de s’arrêter, la distance parcourue sera de 80 mètres et le choc sera donc encore plus violent.
Limiter la vitesse permet donc de diminuer le nombre d’accidents. Rouler trop lentement peut aussi être dangereux : «Ça peut provoquer des accidents si le conducteur se sent en sécurité sur une route et qu’il fait moins attention à ce qu’il se passe, avertit Philippe Chrétien, qui étudie les accidents au Centre européen d’études de sécurité et analyse des risques. Le plus important est de rester attentif en conduisant.»


Comment protège-t-on les gens sur la route ?

Ce n’est pas la première fois que la vitesse est réduite sur les routes en France. En 50 ans, plusieurs mesures ont été prises pour mieux protéger les gens. Il y a par exemple eu l’obligation de porter la ceinture de sécurité ou l’interdiction de boire trop de l’alcool si on prend le volant.
On ne peut donc pas faire ce qu’on veut sur les routes et dans la rue. Les voitures, les camions, les deux roues mais aussi les piétons : tout le monde doit respecter des règles pour être en sécurité. On appelle ça le code de la route. Il faut le connaître par coeur avant d'apprendre à conduire. Il y a un examen à passer pour ça.
Comme il y a beaucoup de règles, des panneaux sont installés le long des routes pour indiquer ce qui est autorisé ou interdit, ce qui est dangereux ou conseillé. Il y a quatre grandes catégories de panneaux :
Ils sont ronds et rouges. Il y a l'interdiction de garer sa voiture à un endroit, de dépasser les voitures quelque part, ou encore d'aller dans un sens dans une rue avec le panneau sens interdit (comme sur le dessin).
Ils sont ronds et bleus. Si un automobiliste voit le panneau qui lui dit de tourner à droite (comme sur le dessin), il est obligé de le faire. Même chose si c’est un piéton qui doit traverser à un endroit précis.
Ils sont triangulaires et entourés de rouge. Ils préviennent d’un danger qui peut arriver là où ils sont installés : une route qui tourne beaucoup, un animal sauvage qui peut passer par là (comme sur le dessin), des enfants qui traversent parce qu’on est près d’une école…
Ils sont carrés ou rectangulaires. Ils sont soit bleus et entourés de blanc, soit blancs et entourés de bleu. Ils indiquent où on se trouve et ce qu’il y a dans une direction : une impasse (comme sur le dessin), un espace de jeu pour les enfants sur une aire d’autoroute…
Si on ne respecte pas les règles, on est puni. Il faut payer une amende : plus la faute est grave, plus elle est élevée. On peut aussi perdre des points sur son permis de conduire : au départ, on en a 12. Si on brûle un feu rouge, par exemple, on perd 4 points. Si on perd tous ses points, il faut repasser le permis. Un piéton ou un cycliste n’a pas de permis de conduire mais il doit payer une amende s’il fait une bêtise.
A toi de jouer !
Regarde bien l’image ci-dessous et trouve les P’tit Libé qui ne respectent pas le code de la route.
Ça commence bien : le scooter A roule dans un sens interdit (le panneau rond et rouge avec un trait blanc), il ne respecte donc pas le code de la route. Même chose pour la voiture D : s’il n’y a pas de panneau sens interdit cette fois, on voit un panneau d’obligation (un rond bleu) avec une flèche qui oblige les voitures à tourner à droite. Or il a déjà mis son clignotant orange… pour tourner à gauche.
La voiture F risque aussi une amende parce qu’elle roule trop vite. Un panneau de danger (triangle entouré de rouge) indique pourtant qu’il y a une école tout près (avec le dessin d’un enfant qui tient la main à un adulte) : ça veut dire qu’il faut ralentir. Et on voit que le P’tit Libé qui traverse avec un enfant gronde le conducteur.
Enfin, regarde le personnage I : il traverse la rue en courant, en dehors du passage piéton et tout près d’un carrefour. Les autres (B, C, E et H) n’ont rien à se reprocher. Il fallait donc répondre A, D, F et I.


A quoi doit-on faire attention sur la route ?

Qu’on soit piéton, cycliste ou en voiture, il faut respecter les règles du code de la route pour notre sécurité et celle des autres. Voici quelques consignes à suivre avec l'exemple de P'tit Libé, qui part demain en vacances avec sa famille. Il a oublié son vélo chez ses grands-parents et veut absolument le prendre avec lui. Il part donc le chercher.
En sortant de la maison, P’tit Libé tourne à droite pour aller au bout de la rue. Il marche sur le trottoir, destiné aux piétons. S’il n’y avait pas eu de trottoir, P’tit Libé aurait dû marcher sur la route. Le mieux dans ce cas, c’est de se mettre du côté gauche, face aux voitures. Ça permet d’être vu plus facilement et de voir les voitures qui arrivent.
Arrivé au bout de la rue, P’tit Libé doit traverser la route. Il s’arrête devant un passage piéton et attend que le feu pour les piétons soit vert. Il regarde à droite et à gauche, et traverse sans courir.
Quand on est à pied, le plus important est d’être bien vu par les automobilistes. Il faut traverser sur les passages piétons. S’il n’y en a pas, il faut traverser à un endroit où la vue est dégagée, pour être vu par les voitures. Là où il n’y a pas de virage, par exemple.
Sur le trottoir d’en face, des enfants font du roller. L’un d’entre eux va trop vite : P’tit Libé s’écarte de justesse pour le laisser passer.
A roller ou à trottinette, on peut rouler sur le trottoir. Mais il faut faire attention à ne pas gêner les autres piétons. Heureusement, P’tit Libé était attentif. Dans la rue, il faut toujours faire attention aux bruits autour de soi : il vaut mieux ne pas avoir d’écouteurs dans les oreilles.
P’tit Libé est arrivé chez ses grands-parents. Après avoir pris un bon goûter, il met son casque et repart sur son vélo. Il roule à droite, sur la piste cyclable, jusque chez lui.
A vélo, il est obligatoire d’avoir une sonnette et de porter un casque jusqu’à l’âge de 12 ans (qu’on soit conducteur ou passager). Quand il n’y a pas de piste cyclable, on peut rouler sur le trottoir si on a moins de 8 ans. Si ce n’est pas le cas, il faut rouler sur la route, bien à droite, en faisant très attention aux voitures.
Le lendemain, c’est le grand départ ! P’tit Libé monte à l’arrière de la voiture entre son frère et sa sœur. Il attache bien sa ceinture de sécurité. Cette fois, tout est prêt.


