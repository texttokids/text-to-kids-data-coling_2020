N°53 - 13 au 19 avril 2018
les grèves
Des grèves à la SNCF ou à Air France, des salariés en colère, des syndicats qui négocient… Mais qu’est-ce que ça veut dire tout ça ? Qu’est-ce qu’une grève exactement ? A quoi ça sert d’arrêter de travailler quand il n’y a plus de dialogue avec son patron ? Cette semaine, je t’explique tout sur cette action dont on entend régulièrement parler dans l’actualité.


Robin, 24 ans, est en grève

Robin est un conducteur de train de la SNCF qui vit à Marseille, dans le Sud-Est de la France. Depuis un an et demi, il transporte les voyageurs dans toute la région provençale.
Le 4 avril, il s’est mis en grève pour la première fois de sa vie avec d’autres collègues. Ça veut dire qu’il a décidé d’arrêter de travailler pour montrer qu’il n’est pas d’accord avec des décisions prises pour son entreprise. Robin a dû prévenir ses chefs «deux jours avant, pour qu’ils puissent s’organiser», explique-t-il.
Même s’ils ne travaillent pas, les grévistes se retrouvent très souvent sur leur lieu de travail, tôt le matin. Ils organisent un «piquet de grève», c’est-à-dire qu’ils se rassemblent tous ensemble pour discuter et rendre la grève visible, ce qui ne serait pas le cas s’ils restaient chez eux. «On peut par exemple organiser une manifestation ou distribuer des tracts aux gens pour expliquer pourquoi on fait grève», détaille Robin. Pour lui, c’est important de se retrouver avec ses collègues. «C’est un moment stressant, alors c’est sympa d’être ensemble pour se serrer les coudes.»
Chaque fin de matinée, les grévistes organisent aussi une assemblée générale. C’est une sorte de réunion géante où les représentants des grévistes, que l’on appelle les syndicalistes, donnent des informations. Tout le monde discute, et à la fin, il y a un vote pour décider si on continue la grève ou pas.
Parfois, ce n’est pas facile de continuer : les grévistes ne sont pas payés les jours où ils ne travaillent pas. «J’avais mis des sous de côté pour partir en vacances, je vais devoir y renoncer, confie Robin. Mais j’ai de la chance, car ma famille m’aide.» Robin espère que la grève sera utile. Il a décidé d’aller jusqu’au bout.
Marseille, la ville où habite Robin


Que se passe-t-il en France en ce moment ?

Depuis quelques jours, on entend beaucoup parler de la grève à la SNCF. Une partie de ses salariés, qu’on appelle les cheminots, a en effet décidé de ne pas travailler deux jours sur cinq… jusqu’à la fin du mois de juin au moins. Pourquoi ? Pour protester contre le gouvernement, qui veut modifier des choses dans cette entreprise qui s’occupe des trains en France.
De nombreux trains ne peuvent pas circuler les jours de grève parce que ceux qui les font rouler ne travaillent pas.
Les cheminots ne sont pas les seuls à être en colère. Des salariés d’Air France, la principale compagnie aérienne française, sont aussi régulièrement en grève en ce moment. Ils demandent à être mieux payés. Des éboueurs ont aussi décidé d’arrêter de travailler dans plusieurs villes : ils demandent notamment à partir plus tôt à la retraite.
Il n’y a pas que les salariés en grève : des étudiants et des lycéens se mobilisent aussi en ce moment dans toute la France pour protester contre la modification des conditions d'accès à l'université.


Qu’est-ce qu’une grève ?

Quand des salariés font grève, ça veut dire qu’ils arrêtent de travailler pour dire qu’ils ne sont pas d’accord avec leur employeur ou pour lui réclamer des choses. On dit que ce sont des grévistes. Ils demandent en général à être mieux payés, à avoir de meilleures conditions de travail ou défendent tout simplement leur emploi si leur patron dit qu’il va tous les licencier par exemple.
Personne ne fait grève par plaisir. Les salariés ne sont pas payés quand ils font grève. Mais quand ils n’arrivent vraiment pas à se mettre d’accord avec leur employeur, ils utilisent ce moyen de pression pour obtenir des choses. Parce qu’une entreprise ne peut pas fonctionner sans ses salariés.
Comme tout le monde perd de l’argent, «les grèves sont, dans la très grande majorité des cas, de très courte durée en France, elles durent très souvent moins d’un jour», remarque Baptiste Giraud, enseignant chercheur à l’université d’Aix-Marseille. Lorsqu’elles durent longtemps, plusieurs semaines ou plusieurs mois, les gens peuvent soutenir les grévistes en leur donnant de l’argent via des cagnottes prévues à cet effet.
Les salariés qui font grève sont protégés par la loi : leur employeur ne peut pas les punir pour ça, en les renvoyant par exemple. Il y a quand même des conditions :
Lors d’une grève, les salariés se retrouvent souvent pour discuter, pour fabriquer des banderoles, réfléchir à ce qu’il faut faire pour convaincre les autres salariés de faire grève ou encore pour manifester dans la rue.
Les grèves sont, le plus souvent, organisées par des syndicats, des organisations de salariés chargées de défendre les autres travailleurs. Pendant la grève, les syndicats cherchent à trouver un accord avec la direction. On appelle ça des négociations.
Les syndicats disent ensuite aux salariés ce qu’ils ont obtenu ou non. Si la majorité des salariés n’est pas d’accord avec ces conditions, la grève continue et les négociations recommencent. Si un accord est trouvé, tout le monde reprend le travail.


D’où vient la grève ?

Le mot «grève» vient de la place de Grève, à Paris, devenue aujourd’hui la place de l'Hôtel de ville. Au début du XIXe (19e) siècle, les ouvriers se rassemblaient là pour chercher du travail. «Quand ils n’étaient pas contents de leurs conditions de travail, ils décidaient de ne pas se faire embaucher et disaient "On fait grève". C’est comme ça que le mot est né», raconte l’historien spécialiste des grèves Stéphane Sirot.
En 1946, le droit de faire grève a été inscrit dans la Constitution, un texte très important qui précise toutes les règles du pays. Depuis, les grévistes sont protégés par la loi et ne peuvent plus être renvoyés de leur travail à cause de ça.
Dans l’histoire, deux grandes grèves ont marqué la France, celle de 1936 et celle de 1968.
Que s’est-il passé ? En mai 1936, les ouvriers ont fait grève pour réclamer de meilleures conditions de travail. Des salariés d’autres entreprises ont aussi arrêté de travailler. Il y a eu plus de 2 millions de grévistes dans toute la France. Ça a duré jusqu’au mois de juin.
Qu’ont obtenu les salariés ? Beaucoup de choses ! D’abord, les salaires ont été augmentés. Ensuite, le temps de travail est passé à 40 heures par semaine alors qu’avant les gens travaillaient 48 heures (aujourd’hui c’est 35 heures). Et enfin, les congés payés ont été créés : les travailleurs ont eu le droit de partir en vacances deux semaines par an tout en recevant de l’argent (aujourd’hui c’est cinq semaines).
Que s’est-il passé ? En mai 1968, des étudiants ont lancé un grand mouvement de protestation. Ils souhaitaient moins d’inégalités et plus de libertés notamment. Les ouvriers ont ensuite bloqué leurs usines pour réclamer de meilleures conditions de travail. Dans quasiment toutes les professions, des salariés ont arrêté de travailler. Il y a eu près de 10 millions de grévistes, soit 1 personne sur 5 en France ! Ça a duré jusqu’en juin, et même plus longtemps dans certains endroits.
Qu’ont obtenu les salariés ? Les salariés ont obtenu des choses, mais beaucoup moins qu’en 1936. Le salaire minimum a été augmenté de 35% (il passe de 2,20 à 3 francs par heure), et tous les autres salaires de 7% environ. Pour la première fois, les syndicats ont été reconnus à l’intérieur des entreprises.


Qui peut arrêter de travailler ?

Quasiment tous les salariés peuvent cesser le travail pour protester. Mais certains travailleurs n’ont pas le droit de faire grève du fait de leur profession particulière. Ça concerne ceux qui ont une profession d’autorité ou de défense, comme les policiers, les militaires, les juges ou les surveillants de prison. Ils doivent donc trouver d’autres moyens de protestation pour montrer leur désaccord.
D’autres salariés ont droit de faire grève mais pas tous en même temps. C’est parce qu’ils s’occupent des urgences ou de la sécurité des gens. S’il y a un incendie et que tous les pompiers sont en grève, par exemple, ce serait dangereux. Ça concerne l’hôpital et les pompiers, donc, mais aussi ceux qui contrôlent la circulation des avions dans le ciel.
Si les enseignants peuvent tous faire grève en même temps, les communes qui gèrent les écoles sont, elles, obligées d’accueillir les enfants.
D’ailleurs, les enfants ont-ils le droit de faire grève pour avoir moins de devoirs ou pour obtenir une récré plus longue ? «La loi ne l’interdit pas, remarque Baptiste Giraud, enseignant chercheur à l’université d’Aix-Marseille. Mais comme ce n’est pas reconnu comme un droit pour eux par la loi, ça ne les protège pas non plus contre le risque de punition…»


