N°59 - 25 au 31 mai 2018
Roland-Garros
Comme chaque année au printemps, les meilleures joueuses et les meilleurs joueurs de tennis du monde se retrouvent à Paris pour le tournoi de Roland-Garros. Du 27 mai au 10 juin, ils vont s'affronter pour devenir les nouveaux champions de la terre battue. A quoi ressemble ce tournoi ? Pourquoi est-il particulier ? Comment marche un match de tennis ? Enfile tes baskets et suis-moi sur le court.


Paul, 14 ans, est ramasseur de balles

Paul, 14 ans, participe cette année à Roland-Garros. Il fait partie des 250 ramasseurs de balles, ces personnes qui distribuent et ramassent les balles de tennis pour les joueuses et joueurs pendant les matchs. Les ramasseurs de balles leur apportent aussi leur serviette quand ils en ont besoin, parce qu’on transpire beaucoup lorsqu’on joue.
Pour être sélectionné, Paul a dû passer plusieurs étapes. La première était au mois d’octobre. Il y avait 3 000 candidats ! C’était beaucoup, alors il a fallu passer des épreuves pour savoir qui étaient les meilleurs. «Il y avait plein d’exercices, on devait courir, ramasser la balle le plus vite possible», explique Paul. A la fin de ce test, il ne restait plus que 370 ramasseurs de balles.
La seconde étape a eu lieu au mois de février. Pendant une semaine, Paul a été testé pour savoir s’il était «vraiment très bon pour ramasser les balles», raconte-t-il. Il a appris qu’il irait à Roland-Garros à la fin de cette semaine de stage. «J’ai eu du mal à y croire. C’est presque un rêve qui se réalise car j’ai toujours aimé le tennis.»
Paul joue au tennis depuis l’âge de 9 ans, mais il a commencé à s’entraîner il y a deux ans pour être ramasseur de balles. Il a voulu participer aux sélections de Roland-Garros pour «vivre de beaux moments et être au plus près des joueurs».
L’adolescent est un peu stressé : «J’ai peur de mal faire les choses parce que Roland-Garros est un grand tournoi.» Il rêve de se retrouver sur le même terrain de tennis que son joueur préféré, l’Espagnol Rafael Nadal. Ce serait un grand moment pour lui.
Neuville-aux-Bois, la ville où habite Paul, et Paris, où a lieu le tournoi de Roland-Garros


A quoi ressemble ce tournoi ?

Dans l’univers du tennis, les tournois les plus importants sont réunis dans ce qu’on appelle «le Grand Chelem». Roland-Garros en fait partie, avec trois autres compétitions :
Les tournois du Grand Chelem
A Roland-Garros, il y a deux tournois : un pour les femmes et un pour les hommes. La gagnante remporte la coupe Suzanne-Lenglen (c’est le nom d’une ancienne joueuse très connue dans les années 1920). Le gagnant soulève la coupe des Mousquetaires (en hommage à quatre joueurs de tennis français qui étaient très forts dans les années 1920 et 1930).
La particularité du tournoi de Roland-Garros ? Les terrains de tennis sont en terre battue. C’est un mélange de brique, de calcaire et de cailloux. L’intérêt, c’est que la balle rebondit plus haut. Mais le jeu est aussi plus lent.
Le plus grand terrain s’appelle le court Philippe-Chatrier (cet homme a été le président de la Fédération française de tennis, organisation qui dirige le tennis en France). On l’appelle aussi le court central. C’est là que les deux finales (hommes et femmes) ont lieu. Les gradins peuvent accueillir jusqu’à 15 000 personnes.
Le tournoi de Roland-Garros se déroule en différentes étapes. Il démarre avec 128 joueurs. La moitié est éliminée à chaque tour. A la quatrième étape, qu’on appelle les huitièmes de finale, ils ne sont plus que 16.
Chaque année, il y a beaucoup de monde à Roland-Garros. Au printemps 2017, plus de 470 000 spectateurs sont venus voir des matchs. C’était un record.


Comment marche un match de tennis ?

Le tennis peut se jouer à deux ou à quatre. Quand il y a deux joueurs sur le terrain, ça s’appelle un simple. Quand il y a quatre joueurs, ça s’appelle un double.
Un match de tennis se déroule en plusieurs parties, qu’on appelle des sets. Ça peut durer une heure, deux heures, trois heures… Tout dépend du moment où le gagnant obtient le nombre de sets demandé. Le record est onze heures et cinq minutes ! A Roland-Garros, pour qu’un match se termine, il faut que les hommes gagnent trois sets et les femmes deux sets.
Au tennis, on ne compte pas les points un par un. Quand on remporte un échange, on a 15 points, puis 30 points, puis 40 points et enfin on gagne un «jeu». Cette façon de compter vient du jeu de paume, un sport très ancien qui est l’ancêtre du tennis. Et quand on gagne suffisamment de jeux, on remporte un set.
Pour bien comprendre les règles, imaginons un match entre Madame P’tit Libé et Monsieur P’tit Libé.
Le premier set est fini. Les joueurs font une pause de quelques minutes.
L’arbitre dit «reprise» : ça veut dire que le match reprend, pour un deuxième set. Les deux adversaires sont très forts. Ils arrivent à aller jusqu’à 6-6 ! Quand il y a 6-6, on dit qu’il y a un jeu décisif, ou un tie-break en anglais. Madame P’tit Libé gagne le jeu décisif, et donc le set… mais aussi ce match en deux sets gagnants !
L’arbitre dit «jeu, set et match», le match de Monsieur P’tit Libé et Madame P’tit Libé est terminé. Les deux joueurs se serrent la main.
On peut faire un petit résumé. Au tennis, il y a donc :


Pourquoi le tournoi de Roland-Garros est-il particulier ?

Après la Première Guerre mondiale (1914-1918), la France n’avait plus beaucoup d’argent. «Le seul moyen de garder sa puissance était de gagner des compétitions sportives internationales», raconte Patrick Clastres, historien spécialisé dans le sport à l’université de Lausanne, en Suisse.
Dans les années 1920 et 1930, il y a près de cent ans, quatre joueurs français, surnommés les Quatre Mousquetaires, ont justement gagné plusieurs grandes compétitions, comme la Coupe Davis (un tournoi de tennis qui réunit les pays les plus forts) et ont permis au tennis français de se faire remarquer.
La joueuse française Suzanne Lenglen a été la première star du tennis. Elle a même été championne du monde à l’âge de 15 ans. Entre 1927 et 1933, elle a gagné beaucoup de matchs, ce qui a fait venir de très nombreux spectateurs à Roland-Garros. «Toute la ville de Paris se déplaçait pour regarder ses matchs. Il y avait même Gaston Doumergue, le président de la République de l’époque», raconte Patrick Clastres.
C’est avec Suzanne Lenglen que Roland-Garros est devenu un tournoi «élégant et glamour», c’est-à-dire un lieu où il y a des personnes bien habillées, bien maquillées. «Les plus grands couturiers voulaient habiller Suzanne Lenglen», explique Patrick Clastres. Elle était très souvent prise en photo pour les journaux. En plus, le tournoi était à Paris, ville qui est considérée comme la capitale de la mode.
Grâce à ses joueurs, «Roland Garros est devenu populaire», c’est-à-dire connu par beaucoup de monde. Dans les années 1950, les stars ont commencé à venir à Roland-Garros. «Elles allaient d’abord au Festival de Cannes [un grand festival de cinéma qui a lieu en mai] puis à Roland-Garros», dit Patrick Clastres.
Dans les années 1970, le tennis a commencé à passer à la télévision. Ce qui a amené encore plus de célébrités, car elles voulaient qu’on les voie à la télé. Aujourd’hui encore, de nombreux acteurs, chanteurs et sportifs se rendent à Roland-Garros pour voir les matchs.
Pourquoi ce tournoi s’appelle-t-il comme ça ?Roland Garros était un aviateur, c’est-à-dire un pilote d’avion. Il est la première personne à avoir traversé la mer Méditerranée en avion. C’était en 1913, il y a plus de cent ans. Quand il était jeune, Roland Garros a fait ses études dans une grande école de commerce. Là-bas, il a rencontré un autre étudiant, Emile Lesieur. Les deux hommes sont devenus très copains.En 1918, pendant la Première Guerre mondiale, Roland Garros est mort dans un accident d’avion. Dix ans plus tard, un stade de tennis a été construit pour accueillir le tournoi français du Grand Chelem. Emile Lesieur, qui était alors président d’un club de sport appelé le Stade Français, a demandé qu’on donne le nom de Roland Garros à ce stade, pour rendre hommage à son ami décédé. Puisque c’est dans ce stade qu’avait lieu le tournoi, on l’a appelé le tournoi de Roland-Garros.


Quels joueurs ont marqué Roland-Garros ?

Le P’tit Libé te présente quatre joueurs, deux hommes et deux femmes, qui sont importants dans l’histoire de Roland-Garros.
Rafael Nadal (Espagne, 31 ans) C’est le plus grand champion de Roland-Garros. Il a gagné dix fois le tournoi ! Il est le seul joueur à avoir réussi cet exploit. On dit souvent que Rafael Nadal est un extraterrestre quand il joue sur terre battue, parce que ses coups sont encore plus puissants sur ce genre de terrain. Il n’avait que 19 ans quand il a gagné son premier Roland-Garros.
Björn Borg (Suède, 61 ans) Avant Rafael Nadal, c’est Björn Borg qui détenait le plus grand nombre de victoires à Roland-Garros. Il a remporté le tournoi six fois, entre 1974 et 1981. Il était connu pour être un joueur très concentré à chaque match. On ne pouvait pas voir sur son visage s’il avait peur de perdre, s’il était stressé ou en colère.
Serena Williams (Etats-Unis, 36 ans) C’est la plus grande championne de tennis : elle a gagné 23 titres du Grand Chelem (le nom qu’on donne aux tournois de tennis les plus importants), dont trois fois Roland-Garros. Elle participe cette année au tournoi parisien, mais est un peu moins forte : la joueuse a arrêté de jouer pendant un an parce qu’elle a accouché en septembre.
Chris Evert (Etats-Unis, 63 ans) Elle a gagné sept fois Roland-Garros, entre 1974 et 1986. Un record chez les joueuses de tennis. Chris Evert est connue pour avoir inventé un nouveau de style pour les femmes : elle a été la première à jouer très longtemps en fond de court, c’est-à-dire sans avancer vers le filet. Chris Evert est une grande star, notamment aux Etats-Unis.
Qui va gagner Roland-Garros cette année ?Chez les hommes, Rafael Nadal est le grand favori. Les joueurs français, eux, ne sont pas très forts en ce moment. Certains ont été blessés, comme Gaël Monfils.Chez les femmes, il y a plusieurs favorites, comme la Roumaine Simona Halep ou l’Espagnole Garbine Muguruza. La Française Caroline Garcia fait partie des outsiders, c’est-à-dire de celles qui ne sont pas favorites mais pourraient quand même gagner. Elle est la septième joueuse mondiale.


