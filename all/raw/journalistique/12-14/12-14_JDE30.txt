Jeudi 15 février 2018
L'hiver se termine dans un mois, mais le froid est toujours là. Comment le corps réagit-il pour s'adapter au froid et jusqu'où peut-il résister ?
Zoom
Premiers bébés pour la nouvelle tortue géante
Un sac à dos pour entendre la musique
Faut-il se méfier du froid ?
L'archipel (Ensemble d'îles) des Galapagos au large de l'Equateur (Amérique centrale) est connu pour être le repère de tortues géantes. Douze espèces différentes y vivent. La dernière a été découverte en 2015 : la "Donfaustoi". À l'âge adulte, ces tortues mesurent près de 2 mètres et pèsent jusqu'à 450 kg. Elles peuvent vivre
jusqu'à 150 ans. Mais il n'en reste plus beaucoup. Pour leur permettre de se reproduire tranquillement, elles ont été recueillies dans un centre d'élevage. Les premiers bébés viennent tout juste de naître. ils seront relâchés à l'âge de quatre ou cinq ans, lorsqu'ils seront assez grands pour se débrouiller.
Face à froid intense, le corps ne résiste pas longtemps. Par exemple, à partir de -28 degrés de température ressentie, la peau exposée peut geler en 10 à 30 minutes. Quand on est à l'extérieur par grand froid, il est important de couvrir ses extrémités. La tête, qui perd beaucoup de chaleur, mais aussi les mains et les pieds. En effet, une réaction du corps va permettre de rediriger le sang vers les principaux organes (foie, coeur, poumons...) qui sont situés à l'intérieur. Les parties les plus éloignées du coeur (les mains et les pieds) reçoivent alors moins de sang. C'est un peu comme si le corps les sacrifiait.
PyeongChang 2018 À Pyeongchang (Corée du Sud), où se déroulent les Jeux Olympiques, les températures sont glaciales.

Le froid peut tuer, mais aussi soigner. Il peut stopper une hémorragie [quand beaucoup de sang coule d'une plaie]. Il peut réduire la douleur ou apaiser des muscles ou des articulations qui ont trop travaillé, comme chez les sportifs par exemple. Certaines personnes qui ont eu un arrêt cardiaque qu'on a pu soigner sont placées en hypothermie. On fait descendre la température de leur corps entre 32 et 34 °C pour leur permettre de récupérer.
Le zéro absolu

La bibliothèque se déplace
Il dessine sur les murs des hôpitaux
Pour tout savoir sur le froid, une exposition simplement intitulée "Froid" se tient actellement à la Cité des Sciens à Paris. Promis, il y fait bon.
Au Nigeria (Afrique), il n'est pas toujours facile de trouver des livres pour apprendre quand on habite dans les quartiers les plus pauvres.
L'alpiniste sauvée des glaces
Comment le corps se protege du froid?
Funmi llori a donc décidé de remplir une camionnette qu'elle a surnommée I Read (je lis en anglais) de livres et d'aller a la rencontre des enfants dans les écoles. Son but : leur donner goût à la lecture.
Peau : les vaisseaux sanguins (tuyaux qui transportent le sang] se resserrent et envoient le sang vers l'intérieur du corps.
Elle possède aujourd'hui 1 900 livres et quatre camionnettes.
Elle soigne les pattes des animaux brûlés.
Le 20 janvier dernier, l'alpiniste Tançaise Elisabet Revol et son compagnon de cordée polonais Tomasz Mackiewicz, entament l'ascension Tu Nanga Parbat. Cette montagne le 8125 m, située au Pakistan, dans une chaîne de l'Himalaya, est surnommée "la montagne tueuse" car de nombreux alpinistes y ont deja perdu la vie.
Le 25 janvier, Elisabeth lance un appel de détresse. Tomasz va très mal. Une opération de sauvetage est lancée, mais les secouristes ne font récupérer que la Française. Elisabeth Revol a eu les mains et le pieds gauche gelés. 
Revenue en France, Elisabeth Revol souffre de très graves gelures pieds aux et aux mains. Elles sont survenues à cause d'un épisode d'hallucination. C'est comme un rêve fabriqué par le cerveau, quelque chose de très réel. 
Jamie Peyton est une vétérinaire américain récemment, elle a soigné des animaux sauva y avaient été victimes d'un grave incendie en Californie et avaient eu les pattes brûlées. Elle a notamment pris en charge deux ourses noireset un lion des montagnes qui avaient été récu après l'incendie et ne pouvaient plus se débrouiller seuls.
Frissons : les muscles se contractent pour créer de la chaleur.
Poils : ils se hérissent [se dressent] pour créer une couche d'air qui isole la peau. Coeur : il bat plus vite pour amener le sang vers les organes vitaux. Digestion : elle s'accélère pour absorber glucides, protéines et graisses indispensables.
Son projet nommé "Graff mon hôpital" a pour but de donner un peu de fantaisie aux murs blancs et pas toujours très accueillants des établissements hospitaliers.
Pour les aider, elle a utilisé une nouvelle technologie, un pansement à base de peau de poisson tilapia. Sa peau contient du collagène, qui permet à la peau de cicatriser (se réparer). Le tilap l'un des poissons les plus élevés au monde. S'est donc disponible en grande quantité. La technique a déjà été utilisée sur des humains. L'opération a réussi et les trois animaux ont pu partir dans la nature avec des pattes toute neuves.
