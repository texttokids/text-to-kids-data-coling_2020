Jeudi 5 octobre 2017
Le monde compte aujourd'hui 1,1 milliard de filles. Elles sont pleines de talent et d'ambition, mais trop souvent confrontées à des situation
de violence ou à des interdictions auxquelles ne sont pas soumis les garçons.

Pourquoi les filles sont-elles plus vulnérables

Ne pas aller à l'école enferme les filles dans la pauvreté.
Apprendre à lire et à écrire les aide à en sortir. 
Mais pour aller à l'école, les filles ont besoin d'être en sécurité et respectées.
Vous êtes peut-être assis tranquillement dans votre chambre, ou sur votre chaise à l'école ou à la bibliothèque.
Vous venez de lire le titre de cet article et vous vous demandez ce que vulnérable peut vouloir dire ici.
A l'occasion de la 5e journée internationale des filles, célébrée chaque année le 11 octobre, le JDE a voulu faire le point sur la situation des filles sur la planète.
Un très grand nombre d'entre elles est vulnérable, c'est-à-dire qu'elles peuvent être facilement atteintes par quelque chose de mauvais.
Elles peuvent être maltraitées, forcées à faire des choses dont elles n'ont pas envie.

Construire son avenir

Dans les pays en développement,1 fille sur 3 est mariée de force avant ses 18 ans.
116 millions de filles dans le monde sont exploitées et obligées de travailler.
Si une fille va à l'école, elle peut grandir normalement.
Elle n'est pas obligée de faire les travaux de la maison, ou pire, de se marier et d'avoir des enfants très jeune.
En apprenant à lire et écrire, elle comprend qu'elle a des droits.
En apprenant un métier, elle gagne de l'argent.
Et plus longtemps elle reste à l'école, plus élevé sera son salaire.
Gofran, 17 ans, vit dans un camp.
Elle aura moins d'enfants, et il sera plus facile d'élever et nourrir sa famille.
Elle produira des richesses p son pays, et pourra le faire part de ses talents.
Elle change l'avenir de son pays, et le sien.
Sala, une jeune réfugiée âgée de 6 ans, fait un exercice de maths dans la salle de classe à Bosso, Niger (Afrique).
Pourquoi sont-elles si vulnérables ? Parce que dans de nombreux pays, notamment les plus pauvres, les femmes, et encore moins les filles n'ont pas la même importance que les garçons.
Elles ne peuvent pas rapporter assez d'argent et sont une bouche à nourrir dont il faut se débarrasser, le plus souvent en les mariant ou en les vendant.
Envoyer les filles à l'école n'est souvent pas une priorité.
Résultat, 2 personnes sur 3 qui ne savent ni lire ni écrire dans le monde sont des femmes.

L'importance des filles, de leur éducation et leur rôle dans le futur d'un pays commencent à être de mieux en mieux compris.
Pour les filles, il est temps que les choses changent.
Bipendu, Nyayang et les autres...
Dans de nombreux endroits du monde, les jeunes filles sont confrontées à des situations de violence.
Malgré tout, elles veulent construire leur avenir et aller à l'école est leur priorité.
Zoom
Quelles menaces pèsent sur elles ?
chanceuse, car elle a sa cousine Nahid.
Si elle avait été seule, elle : n'aurait certainement pas le droit de sortir dans le camp.
Chaque fille a sa propre histoire.
Après avoir fui leur village occupé par l'armée, elles vivent depuis 5 ans dans le camp de réfugiés d'Akçakale. Elles illustrent bien les difficultés auxquelles font plus particulièrement face les filles. Gofran rêvait de devenir médecin, elle voudrait continuer ses études. Mais ce n'est plus possible. Alors quand elle peut, elle travaille avec l'Unicef (organisation chargée de protéger les enfants dans le monde)
Le reste du temps, elle s'occupe des tâches ménagères dans la tente qu'elle partage avec sa maman et son frère. 
Dans le monde, 8,6 millions d'enfants ont dû fuir leur pays à cause de la guerre ou de la pauvreté. Plus de 3,5 millions d'enfants réfugiés âgés de 5 à 17 ans n'ont pas pu aller à l'école lors de la dernière année scolaire.
Parmi eux, la majorité sont des filles.
Dans les camps, elles sont soit obligées de s'occuper du ménage ou de leurs frères : et soeurs, soit mariées très jeunes pour ne plus être une charge pour leur famille.
Elles sont victimes de violence et doivent être particulièrement protégées.
Le mariage forcé : cette pratique, interdite dans de nombreux pays, existe pourtant toujours.
15 millions de filles dans le monde sont mariées de force avant l'âge de 18 ans.
Elles sont maintenues dans une situation inférieure à celle de leur mari, ont des enfants trop jeunes, ne vont pas à l'école et n'apprennent aucun métier.
Le travail forcé : 116 millions de filles de 5 à 17 ans sont exploitées dans le monde.
Souvent, elles sont vendues comme esclaves domestiques pour faire le ménage, les repas...).
La maltraitance : les filles sont victimes de violences physiques et sexuelles.
Et avoir un bébé trop jeune est très dangereux, pour la maman comme pour l'enfant.
Bipendu a 8 ans.
Elle vit en République Démocratique du Congo (Afrique).
Il y a un an, sa région, le Kascii, a plongé dans la guerre à cause d'un conflit entre un chef de tribu et l'armée.
Bipendu a dû fuir avec sa maman.
Elles ont marché deux mois dans la brousse avant d'arriver dans un camp de réfugiés.
Là, là con de Bipendulg Inscrite à l'école. Pour évaluer son niveau, le directeur lui présente un petit test de mathématiques et d'écriture.
La petite fille doit faire de gros efforts pour réaliser le calcul simple qu'elle a devant les yeux.
Elle s'aide de ses doigts pour compter et, après plusieurs minutes, écrit la réponse : 5+2 = 7. Le test d'écriture est beaucoup plus facile, Le directeur annonce que Bipendu peut aller en troisième année. Elle n'a pas été à l'école pendant un an. C'est normal qu'elle ait oublié des choses. Ça va revenir rapidement.
Le seau n'est pas vraiment confortable mais ça va aller. J'aimerais quand même avoir une vraie chaise. Ma famille est venue ici à cause de la guerre. Le bruit des fusil me faisait vraiment peur et j'ai vu beaucoup de mauvaises choses. Quand je serai grande, je veux être professeur et aider à éduquer mon propre peuple.
Nyayang, 10 ans, porte un vieux seau cassé. Dans son école située dans un camp de réfugiés du Soudan du Sud (Afrique).

