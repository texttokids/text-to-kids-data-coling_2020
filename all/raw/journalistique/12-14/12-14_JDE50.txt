Jeudi 28 septembre 2017
Pour l'homme, les arbres servent à produire de l'oxygène et du bois. Ce qu'il ne sait pas forcément, c'est qu'ils prennent soin les uns des autres.
Les 6 caractéristiques les plus étonnantes des arbres : 
- ils ont un langage codé
- ils se défendent contre l'ennemi
- ils font équipe pour survivre
- ils sont très prévoyants
- ils ont chacun leur caractère
- ils ont de la mémoire.
ikea souhaite donner une deuxième vie à ses emballages en carton. Elle a fait appel aux étudiants de la Miami Ad School (une école de publicité aux Etats-Unis). Ces derniers ont proposé leurs idées au géant du meuble suédois.

Quoi de mieux que de transformer les vieilles· boîtes en fusée ?
Baptisée Space ship, l'idée consiste à habiller les boîtes de motifs spatiaux et à y ajouter : une notice afin que les enfants (et les adultes) puissent la transformer en fusée.
« Dans le cadre d'une éducation bienveillante, Je lance aujourd'hui une gamme de stickers #Éthiques Responsables ». C'est ce que l'on peut lire sur le compte Twitter et Facebook de Monsieur Le Prof (plus de 200 000 abonnés).
Avec l'illustratrice Claude Combacau, le prof d'anglais a concocté 24 autocollants, la plupart écrits en français, pour récompenser les bons élèves... Et donner un coup de pouce aux moins bons. Le tout avec humour !
L'Allemand Peter Wohlleben a exercé comme forestier pendant 20 ans.
Il dirige maintenant une forêt naturelle de hêtres située dans une chaîne de montagnes de l'ouest de l'Allemagne.
Son livre "La Vie secrète des arbres" (Ed. Les Arènes) a été vendu à plus d'un million d'exemplaires. Le 27 septembre, sort le film documentaire "L'Intelligence des arbres" tiré de son best-seller [livre à succès].
Livraison de couches réutilisables.
Puiser de l'eau en s'amusant.
En France, 4 milliards de couches sont jetées chaque année.
Entre sa naissance et ses 2 ans et demi, un enfant a besoin de près de 4 500 couches et produit 800 kg de déchets.

L'entreprise "Ma petite couche" offre un service de location, livraison à domicile et nettoyage de couches lavables fabriquées en France. Il est destiné aux parents qui souhaitent se convertir au zéro déchet.

«Pourquoi certaines personnes sont dégoûtées devant un fromage qui pue ? » Des chercheurs français ont travaillé sur les mécanismes du cerveau.
Son activité serait plus intense chez ceux qui auraient un dégoût pour le fromage.
Cette balançoire révolutionnaire s'appelle "See: saw Well".
Le concept est relativement simple : Il suffit de placer un enfant à chaque extrémité.
Avec un simple système de piston qui s'abaisse et remonte grâce au poids des enfants, l'eau est entraînée, ce qui permet de la pomper.
Pour protéger le sabot des chevaux de l'usure et des blessures, on place des fers.
Mais ils sont lourds, s'amortissent pas les chocs et diminuent parfois les mouvements du cheval.
Sans parler de l'opération nécessaire pour les poser qui peut abîmer la corne du sabot.
Une société autrichienne a créé les premières chaussures de course pour cheval.
Appelées Megasus Horserunners, ces chaussures sont simples à mettre en place et à retirer.
Elles offrent tous les avantages du fer à cheval, sans les inconvénients.
Légères, elles peuvent être ajustées à chaque cheval et offrent une protection qui n'a pas besoin d'être entretenue tous les jours: Elles permettent au sabot de rester souple en absorbant [amortissant] au maximum les chocs.
Le cheval peut alors galoper en toute tranquillité.

Une équipe internationale travaille depuis 10 ans sur la diminution des ronflements. Sa conclusion : jouer régulièrement du didgeridoo [instrument à vent venu d'Australie] améliore le sommeil du patient ainsi que celui de son conjoint.

