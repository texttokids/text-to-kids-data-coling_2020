directeur général de Texas Pacific Group (TPG) pour l'Europe et vice-président de Gemplus, Abel Halpern est sorti de sa réserve en déclarant au Monde, jeudi 23 août, être en désaccord avec « ceux qui disent qu'il y a une crise chez Gemplus ». « Nous sommes des investisseurs minoritaires sans contrôle unilatéral ou autorité sur la direction de Gemplus. Nous sommes au conseil d'administration avec des administrateurs indépendants et des représentants d'autres actionnaires. (...) Le conseil d'administration soutient unanimement l'équipe de management. » A propos des « changements structurels » en cours, M. Halpern estime que « dans toutes les situations de changement, certaines parties sont mécontentes et ont le droit de le signifier. Mais le bruit fait autour des dissonances ne signifie pas qu'il ya une crise. (...) En tant que vice-président et membre du conseil d'administration, j'ai confiance dans la prise en compte par la direction des intérêts des multiples parties prenantes de l'entreprise (...) ». Ce faisant, M. Halpern revendique comme son « approche philosophique » la théorie de la stakeholder value, qui veut que toutes les parties prenantes de l'entreprise en tirent bénéfice, au contraire de la shareholder value (maximisation du profit de l'actionnaire). Mais il estime qu' « en période de changement, aucune des parties prenantes ne peut voir tous ses besoins satisfaits », et que « le progrès est une question de compromis » dans les temps difficiles. De même, il rappelle que TPG investit à long terme. Pourtant, le goût du secret de TPG a toujours alimenté les polémiques. Ce fonds gère 10 milliards de dollars (10,95 milliards d'euros), mais il n'a de comptes à rendre qu'aux investisseurs institutionnels qui lui ont confié leur argent, lesquels seraient « certains des plus grands fonds de pensions publics et privés, des banques et des compagnies d'assurances », précise le porte-parole de TPG, Owen Blicksilver. Le fonds ne divulgue pas, notamment, la rentabilité annuelle de ses investissements. DIX INVESTISSEMENTS EN EUROPE Depuis 1996, TPG a réalisé dix investissements en Europe, au Royaume-Uni, en Irlande, en Suisse, en Allemagne et en Italie, en plus de son investissement dans Gemplus. Ses « coups » les plus connus sont le rachat du constructeur de motos italien Ducati, de la chaîne de pubs Punch Tavern au Royaume-Uni ou du chausseur suisse Bally. Le montant moyen investi dans chaque opération serait de 150 millions de dollars. Si Texas Pacific Group, créé en 1993, n'a jamais répugné à investir dans la technologie, son site Web (www.texaspacific.com) est encore en construction ! Le fondateur de TPG, David Bonderman, cinquante-huit ans, cultive lui-même le secret. Il a débuté en tant qu'avocat spécialisé dans les faillites, et a mis son expérience au service de Robert Bass, héritier d'une famille de pétroliers texans, qui investissait dans le rachat de créances immobilières décotées. Puis M. Bonderman a levé des fonds pour créer TPG, basé à Forth Worth, au Texas, afin de reprendre la compagnie aérienne Continental Airlines, en redressement judiciaire, payée alors 66 millions de dollars. Selon Worth Magazine, la compagnie restructurée aurait été revendue cinq ans plus tard pour 700 millions de dollars à Northwest Airlines. En 1996, David Bonderman s'est retrouvé mêlé, très indirectement, aux polémiques sur le financement de la campagne électorale de Bill Clinton. Il a offert 64 500 dollars au Parti démocrate, et a participé, le 15 décembre 1995, à un petit-déjeuner à la Maison Blanche en compagnie du vice-président Al Gore. On a suspecté ces petits-déjeuners d'être des récompenses pour les donations faites au Parti démocrate, et un club de lobbying pour les hommes d'affaires invités. Richard Blum, mari de la sénatrice de Californie, Diane Fenstein, assistait au même petit-déjeuner. En 1994, M. Blum avait créé avec M. Bonderman Newbridge Capital, un fonds d'investissement spécialisé sur l'Asie. Son directeur général, Peter Kwok, a par la suite été cité dans la polémique sur le financement, par des hommes d'affaires proches de Pékin, de la campagne électorale de M. Clinton. 