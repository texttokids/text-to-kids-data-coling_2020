#!/usr/bin/fish

# rename "s/.description//" {train,dev,test,test.expert}/csv/*/*.csv

# for type in autre encyclopedique journalistique fiction ; for token in words sentences ; sed -i -e "s/^\([0-9]\)/$type\/\1/" {train,dev,test,test.expert}/csv/$type/$token.csv ; end ; end

for token in sentences words
    head -n 1 train/csv/encyclopedique/$token.csv > /tmp/header.$token.csv
    for s in train dev test test.expert
        tail -q -n +2 $s/csv/*/$token.csv > /tmp/$s.$token.csv
        cat /tmp/header.$token.csv /tmp/$s.$token.csv > $s/csv/$s.$token.csv
    end
    cat /tmp/header.$token.csv /tmp/{train,dev,test}.$token.csv > all/csv/all.$token.csv
end
