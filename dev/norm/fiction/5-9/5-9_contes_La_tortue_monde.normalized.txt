au tout début de tout , se déplaçait une tortue .
elle avançait lentement , traversant les temps et les saisons , soleil et pluie , nuit et jour , au rythme de son coeur .
un coeur chaud , un coeur fort , qui battait calmement , paisiblement .
sur sa carapace habitaient de petits êtres .
ils ne manquaient de rien , même en période aride , ayant appris à écouter le rythme du coeur et des temps .
ils entendaient quelquefois la voix sourde et chaude de la tortue , qui les rassurait lorsque tout semblait difficile .
à chaque temps , de nouvelles semences se déposaient sur la carapace que les petits êtres cultivaient avec soin .
la récolte achevée , ils lançaient les grains dont ils n' avaient pas besoin dans le vent , donnant ainsi la vie sur le passage de la tortue .
mais un jour , un puceron atterrit sur le dos de la tortue .
d' habitude , les petits êtres détruisaient les pucerons car ils abimaient les récoltes .
sauf que celui ci était un peu plus malin que les autres et déclara : mes chers amis , comme je vous plains !
les petits êtres se regardèrent surpris .
oui , mes amis , je vous plains !
que vous devez être à l' étroit ici sur cette pierre immobile !
mais ce n' est pas une pierre , répliqua un des petits êtres , c' est la tortue , quelque chose de bien vivant !
peuh , répondit le puceron , une tortue , ça ?

vous vous trompez les amis !
il y a bien des choses vivantes et intéressantes à voir en dehors !
les petits êtres murmurèrent entre eux .
et si le puceron disait vrai ?
ils se mirent à l' écouter plus attentivement , ne prêtant pas attention au doux grondement de la tortue qui murmurait sous leurs pieds .
le puceron les incita à sortir au dehors , à accumuler ce qu' ils trouvaient , à se construire toutes sortes de choses dont ils n' avaient pas besoin .
bientôt , les petits êtres furent rongés par la soif de posséder et bâtir sur le dos de la tortue des maisons de plus en plus grandes , de plus en plus hautes .
ils creusèrent aussi des tunnels dans la carapace pour se déplacer plus vite sur celle ci .
ils n' écoutaient plus la tortue , ils ne respectaient plus son rythme .
ils ne l' entendaient pas suffoquer avec peine sous le poids de leurs envies , ils ne l' entendaient pas gémir à cause des blessures de leur folle vitesse .
et le monde autour d' eux se desséchait , privé de semences , des richesses que les petits êtres avaient jusque là partagées .
un jour la tortue s' affaissa , épuisée , malmenée .
la carapace trembla , des maisons s' écroulèrent .
les petits êtres s' écrièrent , paniqués .
la tortue , nous ne l' avons pas écouté !
voilà qu' elle meure !
d' autres répliquèrent : vous croyez encore en ces légendes !

non , cela est fini .
les uns restèrent en larmes sur le dos de la tortue , d' autres s' en allèrent le coeur sec et vide .
il faisait nuit .
ceux qui étaient restés déblayèrent la carapace de tout ce qui l' avait encombré .
et attendirent sans trop savoir quoi faire d' autre .
alors les mains qui guérissent , les mains qui réparent déposèrent une petite graine d' espoir , une petite graine d' amour dans les creux et les failles de la carapace .
les petits êtres continuaient de pleurer , et leurs larmes coulèrent et glissèrent à leur tour dans ces creux .
alors les grains germèrent .
la tortue reprit des forces et se remit en marche .
alors la vie reprit ...