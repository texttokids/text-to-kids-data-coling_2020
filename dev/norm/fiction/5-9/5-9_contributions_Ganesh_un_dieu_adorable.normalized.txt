Ganesh est , parmi les divinités hindouistes , l' une des plus populaires .
c' est un dieu bénéfique qui protège les foyers , bénit les mariages , écarte les malheurs et soutient les projets .
c' est le dieu de la famille , du savoir et de l' intelligence , des arts , de la sagesse et du bonheur , du commerce et de la réussite .
il est surtout lié aux activités quotidiennes des Hommes .
il est très largement honoré .
son culte est étendu surtout dans l' Etat de Maharashtra .
chaque année , à Bombay , un festival lui est consacré .
on trouve jusqu'à huit mille représentations du dieu , de toutes tailles ( jusqu'à huit mètres de hauteur ) qui sont immergées dans le fleuve , à la fin des festivités .
les hindous l' invoquent au début de toute entreprise : voyage , construction d' une maison , écriture d' une lettre ou d' un livre .
ce culte s' appuie sur des légendes : Ganesh est considéré comme un bon scribe et comme un spécialiste des textes sacrés .
sous la dictée du sage Vyasa , il écrivit le " Maharabharata " , la Bible de l' hindouisme .
comment peut on expliquer l' arrivée de cette tête d' éléphant sur un corps d' homme ?
il y a plusieurs versions mais , dans toutes , Ganesh est l' enfant du couple divin Parvati Shiva .
dans certaines légendes , il a été conçu par Pârvatî ou même créé à partir de pellicules de sa propre peau mélangées à des huiles et à des onguents jusqu'à obtenir une représentation humaine .
la déesse lui a alors donné vie en l' éclaboussant avec de l' eau du Gange .
dans ce cas , c' est Shiva qui , par jalousie , a décapité ce fils qui n' était pas le sien .
dans d' autres légendes , c' est Shiva qui met au monde Ganesh .
on raconte même qu' il jaillit de son front sous les traits d' un homme d' une radieuse beauté .
Pârvatî , jalouse , lui jette un sort : Ganesh sera laid et ventru .
dans les deux cas , Ganesh ne peut survivre qu' avec la première tête disponible : celle d' un éléphant !
pour compenser ce handicap , il fut divinisé et déclaré chef des armées de Shiva .
il empêche les mauvaises actions de se réaliser .
une autre légende parle de son mariage avec deux jeunes filles .
il était en rivalité avec son frère et , pour savoir qui épouserait les promises , une grande course autour du monde fut organisée .
son frère S épuisa dans un long voyage et , à son retour , il trouve Ganesh marié avec les deux jeunes filles !

ce dernier , affirma avoir effectué un tour du monde entier durant l' étude des textes sacrés et , grâce à la logique , avait réalisé ce périple bien avant le retour de son frère .
on représente toujours Ganesh avec un corps d' homme , petit et bedonnant .
il a la peau jaune , quatre bras et une tête d' éléphant avec une seule défense .
il tient dans ses mains un coquillage , un disque , une massue ( ou un aiguillon à éléphant ) et un nénuphar .
un rat se trouve sous son pied gauche .
il a une prédilection pour les offrandes de nourriture , surtout des fruits , qu' il reçoit quotidiennement .
c' est ce qui explique son gros ventre !
et chaque foyer hindou possède une représentation de Ganesh .
à l' instar de Dumbo et de Babar , Ganesh est un éléphant merveilleux et drôlement sympathique , vous ne trouvez pas ?