Dublin , treize octobre mille neuf cent trente deuxIl est dix neuf heures trente , la journée est presque terminée .
les Dublinois quittent peu à peu leur travail pour retourner chez eux et rejoindre leur famille .
Amelia O' Neill , elle , ne veut pas rentrer .
journaliste au Irish Independent depuis trois ans , elle s' active pour terminer ce qu' elle considère être son article de référence , celui qui la fera connaître et fera d' elle une journaliste respectée .
cet article , qu' elle prépare depuis maintenant trois mois , est un immense travail d' investigation portant sur l' étude des mystères de l' île de Pâques .
Amelia a passé la nuit à la rédaction du Irish Independent pour terminer son article à temps afin que celui ci puisse être publié dans l' édition du week end , consacrée aux grands articles d' investigation .
fière de son enquête , elle se rend au bureau de son directeur Colin Murphy pour lui demander de publier son article .
quelle n' est pas sa stupeur lorsqu' il lui annonce que son article ne sera pas retenu !
" je te remercie pour ton travail Amelia , mais ce n' est pas un sujet que nous pouvons nous permettre de publier . celui ci n' intéressera personne ! quand est ce que tu comprendras que notre objectif est de vendre un maximum de papiers ? il nous faut du clinquant , de l' extraordinaire , quelque chose de vraiment sensationnel ! cet article , à l' image de tous ceux que tu as écris avant , ne répond à aucun de ces critères . je suis désolé Amelia , mais tu ne respectes pas notre ligne éditoriale . j' en ai marre de te le répéter . tu es virée ! "
c' est la douche froide , un véritable ascenseur émotionnel pour Amelia qui était si sure de son coup .
dépitée , elle ne prend même pas la peine de récupérer les affaires de son bureau avant de s' en aller et file directement dans son lieu de refuge , l' immense bibliothèque de sa maison familiale .
c' est là qu' elle s' évade en lisant de grands récits d' aventure qui la font rêver .
le problème , c' est qu' Amelia vient de réaliser qu' elle les a déjà tous lus , plus rien ne pourra lui faire oublier ses soucis .
frustrée et folle de rage , elle donne un violent coup de talon sur le sol en bois de la bibliothèque .
au moment de l' impact , elle sent une dalle se dérober sous son pied et entend un " clic " métallique " .
prise par surprise , elle tombe en arrière , sur le dos .
en se relevant , elle voit la bibliothèque devant elle pivoter lentement pour laisser place à un long tunnel sombre ...
guidée par son sens inné de l' aventure , Amelia s' engouffre dans le tunnel , une lampe à huile à la main .
le passage est très humide et Amelia a la mauvaise surprise d' y rencontrer des rats et des chauves souris .
mais il en faut beaucoup plus pour arrêter cette jeune femme courageuse .
l' envie de découvrir ce que cache ce mystérieux tunnel l' emporte sur tout le reste .
au bout de celui ci , Amelia découvre une pièce quasiment vide avec pour seule ornement une table .
intriguée , elle s' en approche et y découvre un grand sac de voyage en cuir .
ni une ni deux , elle ouvre le sac et sort tout ce qu' il contient : une boussole , une carte du monde , un compas et un mystérieux carnet ...
le coeur d' Amelia bat à toute vitesse , elle est exaltée par la découverte qu' elle vient de faire .
les mains tremblantes d' excitation , elle ouvre le vieux carnet duquel s' échappe un épais nuage de fumée qui la fait toussoter .
Amelia se demande à qui peuvent bien appartenir ces objets , et elle éclaircit très rapidement ce mystère en feuilletant les pages délavées du carnet : elle y reconnait l' écriture très particulière de son grand père paternel Murphy O' Neill , grand explorateur du dix neuvième siècle !
Amelia ne l' avait rencontré que lorsqu' elle était enfant , mais elle avait appris à le connaître grâce aux lettres qu' il envoyait à ses parents et qu' elle lisait en cachette , à la lumière de la bougie .
ses lettres restaient brèves et mystérieuses , mais laissaient deviner le récit d' un homme qui parcourait le monde à la recherche d' aventures .
Amelia était impressionnée par le train de vie de cet homme , à tel point qu' elle décida de devenir journaliste pour vivre des aventures aussi palpitantes que les siennes .
et ce qu' elle allait découvrir dans ce carnet allait lui permettre de mener à bien cette extraordinaire ambition ...