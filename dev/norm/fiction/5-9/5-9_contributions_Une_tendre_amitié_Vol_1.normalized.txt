
quelle belle région !
c' est ici , dans le Roussillon , que Bérangère , jeune femme aux immenses yeux noirs , a passé toute son enfance .
à l' âge de dix ans , longs cheveux clairs et petit nez couvert de taches de rousseur , elle va y vivre le début d' une amitié qui dure encore aujourd'hui .
c' était la dernière année de primaire et il fallait bien travailler pour passer en sixième au collège l' année suivante .
les enfants s' y rendaient en autobus car , dans ce petit village roussillonnais qu' à peine quelques centaines de gens habitaient , seules se trouvaient une école maternelle et une école primaire .
en attendant , pour l' année de son CM deux , les quelques pas séparant sa maison de l' école se cheminaient à pied .
la vieille bâtisse
c' est précisément sur ce chemin que quelques jours seulement après la rentrée des classes , l' étonnement , puis l' intrigue s' emparèrent de la fillette .
en effet , un après midi après les cours , elle s' aperçut que les volets de l' ancienne et grande bâtisse à la façade de pierre rose étaient béants , la demeure était abandonnée depuis si longtemps que Bérangère n' y avait jamais vu le moindre occupant .
petite curieuse de nature et quelque peu fouineuse , elle se demanda alors quelle âme avait pu ranimer aujourd'hui cette maison laissée si longtemps sans vie .
elle aurait bien voulu distinguer tout nouvel habitant .
elle n' eut pas à attendre longtemps , car ce même jour , par la balustrade aux barreaux largement espacés du premier étage , elle entrevit une porte fenêtre restée ouverte , elle l' observait toujours tout en s' approchant et discerna derrière celle ci un garçonnet de son âge ou presque , qui se tenait assis .
elle poursuivit son chemin et rentra chez elle .
le jeune inconnu Quelques journées passèrent sans que Bérangère osât s' arrêter , le fait que le garçon ne se rendait pas en classe comme tous les autres enfants du village tourmentait un peu son esprit et son envie d' en savoir plus long sur ce nouveau venu grandissait un peu plus chaque jour .
pourtant , un soir après l' étude , rentrant chez elle , elle s' approcha de la bâtisse , décidée à s' informer un peu plus sur la vie du garçon qu' elle avait aperçu quelques jours auparavant .
mais l' enfant n' y était pas .
après cette journée un peu fraiche , peut être avait il trouvé que le soleil ne réchauffait plus assez , le crépuscule allant bientôt recouvrir tout le paysage .
ce soir là , avant de s' endormir , Bérangère se demanda si le garçon serait toujours là le lendemain et si elle pourrait enfin faire sa connaissance .
une foule de pensées dansèrent dans sa tête autour du même sujet : le garçon , la bâtisse ...
finissant par la faire sombrer dans un sommeil chargé de rêves .
les présentations
au matin suivant , elle frôla à nouveau les murs de la vieille demeure et aperçut l' enfant encore en robe de chambre , un livre à la main .
fixant bien le garçon , elle remarqua qu' il était assis dans un fauteuil un peu différent de ceux qu' elle avait coutume de voir .
" bonjour !
lui cria t elle , tentant un espoir de réponse qui ne tarda pas à se faire entendre .
en effet , le garçonnet , à la fois surpris et étonné , redressa la tête et vit la petite fille .
bonjour !
répliqua t il en se penchant légèrement en avant de son fauteuil .
je m' appelle Bérangère , enchaina aussitôt la fillette tout à fait satisfaite d' avoir enfin pu échanger quelques paroles avec ce jeune inconnu .
moi , c' est Tanguy , rétorqua t il .
tu ne vas pas à l' école , tu es en vacances ?
questionna Bérangère .
euh ...
non !
répondit le garçon un peu gêné , je ne peux pas , j' étudie à la maison " , lui montrant simultanément le livre de français qu' il reposa aussitôt sur ses genoux .
Bérangère pensa alors qu' il devait avoir un quelconque problème , certainement de santé , pour ne pas pouvoir se rendre normalement à l' école .
l' an passé , une de ses camarades , Virginie , après une mauvaise chute de cheval , était dans ce cas : immobilisée pendant un mois pour blessure à la nuque et à la jambe droite , elle étudiait à la maison .
probablement en était il de même pour ce garçon , songea t elle .
je me dépêche car je vais être en retard , s' exclama t elle en regardant la montre offerte par sa tante pour son anniversaire , objet dont elle était si fière .
seras tu là ce soir après la sortie des classes ?
rajouta t elle .
sans doute , dit Tanguy , si le temps est assez clair pour que je puisse encore lire dehors sur la terrasse .
de toute façon , je repasserai te voir !
" lança la gamine en commençant à courir pour rattraper le temps perdu à bavarder .
en fin d' après midi , elle n' oublia pas le rendez vous qu' elle même avait fixé et , dans une précipitation fort enthousiaste , laissa son blouson dans la classe .
Tanguy , de son côté , espérait qu' elle viendrait , la compagnie lui faisait en effet défaut et il éprouvait depuis le matin un certain bien être à la pensée que quelqu' un , et précisément un enfant de son âge , s' intéressait à lui , dans ce village .
la demeure qu' il habitait depuis peu était érigée dans une rue éloignée de toute animation .
" bonsoir ! " s' exclamèrent ensemble les enfants en s' apercevant .
cette fois ci , Tanguy était en tenue de jour , son tee shirt à rayures clair contrastant avec ses cheveux bruns très foncés .
les yeux de Tanguy n' étaient pas noirs comme ceux de Bérangère mais d' un vert tirant sur l' émeraude .
" as tu le temps de monter un instant ?
lança t il en sa direction .
je ne sais pas trop , dit elle timidement , ma maman m' attend et s' inquiéterait .
ne t' inquiète pas , je suis juste avec ma gouvernante , Marthe .
elle va descendre avec le téléphone et tu pourras demander à ta maman la permission de rester un petit instant avec moi .
Marthe aussi lui parlera .
bon , alors , d' accord si ma mère veut bien .

une dame d' un certain âge , assez rondelette , descendit avec un portable , et ensemble elles se mirent d' accord au téléphone .
Marthe ouvrit à Bérangère avec un grand sourire et l' accompagna à l' étage où le jeune garçon se trouvait .
la rencontre
parvenue à l' étage , la petite fille semblait émerveillée devant l' ameublement aussi stylé et encaustiqué , le parquet n' en avait d' ailleurs pas moins d' éclat .
chez elle , c' était plutôt moderne , bien que vivant à la campagne , ses parents encore très jeunes avaient opté pour un genre pratique et fonctionnel , moins chargé .
Tanguy avança vers elle en manipulant son fauteuil .
la fillette fixa les roues de ce siège particulier permettant au garçon de circuler aisément dans la maison .
ils se regardèrent un moment sans oser se parler , ne sachant qui des deux devait entamer la conversation .
Bérangère se décida et lui demanda son âge :
" onze ans , déclara t il , et toi ?
dix ans !
elle continua : tes parents habitent ils ici avec toi ?
non , je vis avec Marthe , la dame qui t' a fait entrer .
elle est en quelque sorte la maîtresse de maison , mon père exerce un métier qui l' oblige à se déplacer souvent , maman l' assistant la plupart du temps .
j' ai une petite soeur de six ans , Chloé , qui est en pension cette année .
la ville ne me convient guère depuis que j' ai eu cet accident ne me permettant plus l' usage normal de mes jambes .
mes parents ont alors décidé un grand oncle de nous céder cette ancienne demeure afin que je puisse bénéficier de l' air pur de la campagne et étudier en toute sérénité .
d' ailleurs , à Paris , d' où je viens , j' habitais un appartement qui était bien sobre et assez bruyant .

Bérangère l' écoutait attentivement et commençait à ressentir l' impression que sa présence , de temps à autre , pourrait être bénéfique au moral de Tanguy , privé malgré lui de la compagnie d' autres enfants .
elle enchaina :
" penses tu que tes parents viendront te voir bientôt ?
je ne sais pas pour l' instant , mais pour Noël , certainement , avec Chloé , ma petite soeur .
cela fait il longtemps que tu as eu cet accident ?
questionna peut être un peu indiscrètement la fillette .
quatre mois exactement ...

le garçon s' interrompit garda un instant le silence , comme songeur puis finalement poursuivit de lui même , décidé à se confier : " nous rentrions de week end , maman , Chloé ma petite soeur , Marthe et moi , je riais à l' arrière avec Chloé . soudain , j' entendis maman crier un choc ! puis je me suis évanoui sur une douleur envahissante . je ne me suis réveillé qu' à l' hôpital . maman avait voulu éviter une voiture qui arrivait droit devant nous et dont le chauffeur avait doublé sans aucune visibilité . heureusement , personne d' autre n' a été gravement blessé . mais depuis , je ne me sers plus de mes jambes ... "
à nouveau , après un court silence , il enchaina : " as tu des frères et soeurs ?
, détournant ainsi une conversation qui ne pouvait que lui remémorer un trop mauvais souvenir .
oui , j' ai un petit frère , un bout de chou de deux ans , Grégoire , il ne va pas encore à l' école .
maman s' occupe de lui car elle ne travaille pas en ce moment , papa est commercial et ...

regardant sa montre , elle fit une moue qui amusa Tanguy .
elle avait trop bavardé et ne s' était pas rendu compte de l' heure tardive .
" pardonne moi , dit elle , mais je suis très en retard , je dois rentrer chez moi . je reviendrai te voir demain si tu veux bien , car c' est mercredi . enfin , si maman m' y autorise . si elle dit oui , j' apporterai des jeux , d' accord ? d' accord ! " répondit il .
après les " au revoir " d' usage , Marthe raccompagna à la porte l' enfant qui se hâta dans le soir naissant .
le début d' une amitié Au cours de la soirée , à table , Bérangère raconta à ses parents la rencontre avec Tanguy .
" il est très gentil et poli , tu sais , maman , c' est dommage qu' il ne puisse venir lui même à la maison .
oui , c' est un handicap que les enfants ne devraient jamais connaître , dit le père , les adultes non plus , d' ailleurs !
ajouta t il .
si tu veux , dit la maman , je préparerai un gâteau pour votre gouter de demain .
chic , bonne idée !
répondit de façon quasi incompréhensible Bérangère , tout en mastiquant sa salade .
par contre , renchérit la mère , je préfère t' accompagner pour me rendre compte de l' endroit où tu vas passer l' après midi .
d' accord " , fit Bérangère à mi voix , un peu déçue , mais elle avait suffisamment de maturité pour comprendre que plus qu' un manque de confiance en sa fille , il s' agissait pour la maman d' un principe de sécurité destiné à la rassurer .
le lendemain , les deux enfants , ainsi que Marthe qui ne manqua pas d' y gouter , apprécièrent la saveur de la tarte aux pommes que Bérangère apporta .
les gamins jouèrent à divers jeux de société de lettres et de cartes , l' après midi s' écoulant avec une extrême rapidité .
Tanguy avait laissé tomber , pour l' occasion , ses livres et ses cahiers .
Molière attendrait ...
il essayait d' être gai et de ne pas laisser percevoir sa frustration , mais quelques faits inévitables lui rappelaient sa situation inconfortable .
ainsi , un pion tombant du jeu et roulant sous un meuble , suffisait à l' attrister car il était incapable de pouvoir le ramasser , et n' avait même plus le réflexe de se pencher .
bien que Bérangère ne lui montrait pas qu' elle s' apercevait de la difficulté de son handicap dans un cas comme celui ci , il se doutait qu' elle ne pouvait pas rester totalement indifférente à cet état .
" tu sais , un docteur doit venir m' examiner la semaine prochaine ! " s' exclama t il d' un coup .
la fillette parut toute surprise de cette phrase si spontanée et inattendue , elle sentit que Tanguy avait besoin de justifier sa situation .
sans doute voulait il ainsi lui démontrer que tout espoir de recouvrer l' usage normal de ses jambes n' était pas perdu .
" ah , bien ! fit elle , tu vas entreprendre une rééducation ? je ne sais pas encore , mes premiers efforts n' ont pas été concluants , mais j' espère que cela sera à nouveau possible . je suis encore très jeune , avec le temps et de la patience , j' arriverai peut être à marcher à l' aide de béquilles ou d' un appareil . je l' espère beaucoup pour toi " , affirma t elle .
un quart d' heure plus tard , les enfants se quittèrent , Bérangère devant réviser ses leçons .
" je suis très content de cet après midi , dit Tanguy d' une voix enjouée , quand reviendras tu ? samedi après midi , si tu veux bien ! entendu et merci encore pour le gâteau et les jeux ! "