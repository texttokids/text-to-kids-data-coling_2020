le grand jour est enfin arrivé !
pierre va enfin réaliser son rêve : participer au Tour de France !
pierre est prêt : il a son maillot , ses gants , son casque , ses lunettes de soleil , une bouteille d' eau sans oublier son vélo de course .
une immense foule acclame les coureurs .
plusieurs équipes de télévision sont venues filmer l' événement .
pierre se sent confiant et remercie la foule .
la course commence !
pierre pédale le plus vite possible mais le trajet reste long à faire .
il passe par plusieurs communes .
à chaque étape , une foule de supporters les encourage .
le soleil tape fort , Pierre n' oublie pas de boire de l' eau pour s' hydrater .
avant la fin du tour , Pierre doit monter une impressionnante pente .
après un long effort , il réussit à arriver en haut de la pente !
soudain Pierre voit la ligne d' arrivée !
prenant son courage à deux mains , il réussit à dépasser les autres coureurs et arrive premier !
il a du mal y croire , il est arrivé vainqueur du Tour de France !
les journalistes se précipitent pour l' interviewer tandis que les supporters lui demandent des autographes .
pierre se place sur le podium et enfile le maillot jaune du meilleur coureur .
il est si heureux !
toute sa famille et ses amis sont fiers de lui .