depuis toute petite , la princesse Li rêve de découvrir ce qui se cache dans la mystérieuse forêt près du château .
ma fille , il ne faut jamais aller dans la forêt car vous risquerez un grand danger !
lui répète souvent l' Empereur .
un jour , alors que l' Empereur s' absente pour aller à la chasse , la princesse s' aventure dans la forêt .
au bout de quelques heures , la princesse s' aperçoit qu' elle s' est perdue !
elle tourne en rond depuis plusieurs heures déjà .
soudain , La princesse aperçoit une grande tour rouge au loin .
elle décide de s' y rendre et ouvre la porte de la tour .
qui est là ?
demande une vieille dame .
je suis Li , la princesse du royaume de Wei .
répond la princesse .
mais quel honneur !
s' exclame la vieille dame .
je vous en prie , asseyez vous princesse , que je vous offre le thé .
puis la vieille dame apporte un mystérieux objet .
permettez moi de vous offrir un présent .
affirme la vieille dame .
la vielle dame tend à la jeune princesse un talisman rouge et doré .
il est magnifique !
s' exclame la princesse .
la princesse enfile le talisman autour de son cou mais à peine a t elle le temps de se contempler devant le miroir qu' elle s' évanouit !
la vieille dame enferme alors la princesse dans la tour surveillée par un immense dragon .
de retour au château , l' Empereur s' inquiète de ne plus voir sa fille .
il demande de l' aide à ses guerriers chinois .
un des guerriers s' engouffre dans la forêt et découvre la tour .
mais lorsqu' il ouvre la porte , il tombe nez à nez avec un immense dragon !
le guerrier prend son sabre et plante la lame dans le coeur du dragon .
puis le guerrier se précipite vers la princesse endormie et lui enlève le talisman rouge qui pend à son cou .
la princesse est sauvée !
l' Empereur remercie son guerrier et décide de jeter le talisman dans le fleuve .
ainsi , le dragon et le talisman magique ne seront plus qu' un mauvais souvenir !
déclare l' Empereur .