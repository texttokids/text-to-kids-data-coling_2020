( Note soixante quatre : le prince de Bade est frère de l' impératrice de Russie .

je n' ai point encore dit que , parmi les plaisirs qu' on se donnait quelquefois à cette cour , il faut compter ceux de la comédie , qu' on jouait à la Malmaison .
cela avait été assez fréquent dans la première année du consulat .
le prince Eugène et sa soeur avaient de vrais talents , et cela les amusait beaucoup .
à cette époque , Bonaparte s' intéressait assez à ces représentations , données devant une assemblée peu nombreuse .
on bâtit une jolie salle à la Malmaison , et nous y jouâmes plusieurs fois .
mais , peu à peu , le rang où la famille se trouva montée ne permit plus guère ce genre de plaisir , et on finit par ne se le permettre qu' à certaines occasions , comme à la fête de l' impératrice .
quand l' empereur revint de Vienne , madame Louis Bonaparte imagina de faire faire un petit vaudeville de circonstance , où nous jouâmes tous et chantâmes des couplets .
on avait invité assez de monde , et la Malmaison fut illuminée d' une manière charmante .
c' était quelque chose d' imposant que de paraitre en scène devant un pareil auditoire , mais l' empereur se montra assez bien disposé .
nous jouâmes bien , madame Louis eut et devait avoir un grand succès , les couplets étaient jolis , les louanges assez délicates , la soirée réussit parfaitement .
( Note soixante cinq : cette représentation pourrait bien avoir été donnée un peu plus tard que cela n' est dit ici .
du moins , quand Barré , Radet et Desfontaines , les grands vaudevillistes du temps , firent jouer devant le public de Paris la pièce dont il s' agit , ils l' appelèrent la Colonne de Rosbach .
ils semblaient l' avoir faite en l' honneur de la campagne d' Iéna .
il est vrai que les auteurs pouvaient , sans travail , transporter leur à propos de la guerre de dix huit cent cinq à la campagne de Prusse .
ni les courtisans ni les vaudevillistes n' y regardent de si près .
ce qui est certain , c' est que le rôle de la vieille Alsacienne est bien tel que ma grand mère le raconte .
les princesses étaient ses filles , ou ses nièces .
cette Alsacienne se montrait pleine d' enthousiasme pour l' empereur , et chantait ce couplet , que la merveilleuse mémoire de mon père ne lui permettait pas d' oublier , et que je retiens après lui :
air : j' ai vu partout dans mes voyages .
ce qui dans le jour m' intéresse , La nuit occupe mon repos .
ainsi donc je rêve sans cesse À la gloire de mon héros .
les songes , dit on , sont des fables , Mais , quand c' est de lui qu' il s' agit , J' en fais que l' on trouve incroyables , Et sa valeur les accomplit .
on peut trouver dans les Mémoires de Bourrienne des détails sur les représentations de la Malmaison .
le vaudeville était fort à la mode à cette cour .
c' était toute la littérature de la jeunesse de beaucoup de personnages du temps .
( P R ) )
il était assez curieux de voir de quel ton chacun se disait le soir : " l' empereur a ri , l' empereur a applaudi ... " et comme nous nous en félicitions !
moi , particulièrement , qui ne l' abordais plus qu' avec une certaine réserve , je me retrouvai tout à coup dans une meilleure position vis à vis de lui , par la manière dont j' avais rempli le rôle d' une vieille paysanne qui rêvait toujours que son héros ferait des choses incroyables , et qui voyait les évènements surpasser ce qu' elle avait rêvé .
après le spectacle , il me fit quelques compliments , nous avions tous joué de coeur , et il semblait un peu ému .
quand il m' arrivait de le voir ainsi , saisi comme à l' improviste par une sorte de détente et d' attendrissement , il me prenait des envies de lui dire : " eh bien , laissez vous faire et consentez quelquefois à sentir et à penser comme un autre . " j' éprouvais , dans ces occasions trop rares , un vrai soulagement , il semblait qu' une espérance nouvelle vînt tout à coup se raviver en moi .
ah !
que les grands sont facilement maîtres de nous , et par combien peu de frais ils pourraient se faire aimer !
peut être cette réflexion m' est elle déjà échappée , mais je l' ai faite si souvent pendant douze années de ma vie , elle me presse encore tellement aujourd'hui , quand j' interroge mes souvenirs , qu' il n' est pas extraordinaire qu' elle m' échappe plus d' une fois .
la cour de l' empereur .
maison ecclésiastique .
maison militaire .
les maréchaux .
les femmes .
delille .
chateaubriand .
madame de Staël .
madame de Genlis .
les romans .
la littérature .
les arts .
avant de reprendre la suite des évènements , j' ai envie de m' arrêter un peu sur les noms des personnages qui , dans ce temps , composaient la cour , ou qui occupaient quelque rang distingué dans l' État .
je ne pourrais pas cependant prétendre à faire une suite de portraits qui eussent des différences bien piquantes .
on sait que le despotisme est le plus grand des niveleurs .
il impose à la pensée , il détermine les actions et les paroles , et , par lui , la règle à laquelle chacun est soumis se trouve si bien observée , qu' elle appareille tous les extérieurs , et peut être même quelques unes des impressions .
je me souviens que , durant l' hiver de mille huit cent quatorze , l' impératrice Marie Louise recevait tous les soirs un grand nombre de personnes .
on venait s' informer chez elle des nouvelles de l' armée , dont chacun était vivement occupé .
au moment où l' empereur , poursuivant le général prussien Bluecher du côté de Château Thierry , laissa à l' armée autrichienne le loisir de s' avancer jusque sur Fontainebleau , on se crut , à Paris , près de tomber au pouvoir des étrangers .
beaucoup de gens s' étaient réunis chez l' impératrice , on s' y interrogeait avec anxiété .