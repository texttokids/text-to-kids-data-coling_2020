nous venons de voir , au sujet des Cerceris , des Sphex et des Ammophiles , comment la mère résout le problème des conserves alimentaires , le problème qui consiste à déposer par avance dans la cellule la quantité nécessaire de gibier et à le maintenir des semaines entières dans un parfait état de fraicheur , que dis je , presque à l' état de vie , bien que les victimes soient immobiles ainsi que l' exige la sécurité du vermisseau qui en fait pâture .
les ressources les plus savantes de la physiologie accomplissent cette merveille .
le stylet à venin est dardé dans les centres nerveux une seule fois , ou bien à diverses reprises , suivant la structure de l' appareil d' innervation .
ainsi opérée , la victime conserve les attributs de la vie , moins l' aptitude de se mouvoir .
examinons si les Bembex font usage de cette profonde science du meurtre .
les Diptères retirés d' entre les pattes du ravisseur entrant dans son terrier ont , pour la plupart , toutes les apparences de la mort .
ils sont immobiles , rarement , sur quelques
uns , peut on constater de légères convulsions des tarses , derniers vestiges d' une vie qui s' éteint .
les mêmes apparences de mort complète se retrouvent habituellement chez les insectes non tués en réalité , mais paralysés par l' habile coup de dard des Cerceris et des Sphégiens .
la question de vie ou de mort ne peut alors se décider que d' après la manière dont se conservent les victimes .
mis dans de petits cornets de papier ou dans des tubes de verre , les Orthoptères des Sphex , les Chenilles des Ammophiles , les Coléoptères des Cerceris gardent la flexibilité de leurs membres , la fraicheur de leur coloration et l' état normal de leurs viscères pendant des semaines et des mois entiers .
ce ne sont pas des cadavres , mais des corps plongés dans une torpeur qui n' aura pas de réveil .
les Diptères des Bembex se comportent tout autrement .
les Éristales , les Syrphes , tous ceux enfin dont la livrée présente quelque vive coloration , perdent en peu de temps l' éclat de leur parure .
les yeux de certains Taons , magnifiquement dorés avec trois bandes pourpres , pâlissent vite et se ternissent comme le fait le regard d' un mourant .
tous ces Diptères , grands et petits , enfouis dans des cornets où l' air circule , se dessèchent en deux ou trois jours et deviennent cassants , tous , préservés de l' évaporation dans des tubes de verre où l' air est stagnant , se moisissent et se corrompent .
ils sont donc morts , bien réellement morts lorsque l' Hyménoptère les apporte à la larve .
si quelques
uns conservent encore un reste de vie , peu de jours , peu d' heures terminent leur agonie .
ainsi , par défaut de talent dans l' emploi de son stylet ou pour tout autre motif , l' assassin tue à fond ses victimes .
étant connue cette mort complète du gibier au moment où il est saisi , qui n' admirerait la logique des manoeuvres des Bembex ?
comme tout se suit méthodiquement , comme tout s' enchaîne dans les actes de l' Hyménoptère avisé !
les vivres ne pouvant se conserver sans pourriture au delà de deux ou trois jours , ne doivent pas être emmagasinés au grand complet dès le début d' une éducation qui durera pour le moins une quinzaine , forcément la chasse et la distribution doivent se faire au jour le jour , peu à peu , à mesure que le ver grandit .
la première ration , celle qui reçoit l' oeuf , durera plus longtemps que les autres , il faudra plusieurs jours au naissant vermisseau pour en manger les chairs .
il la faut par conséquent de petite taille , sinon la corruption gagnerait la pièce avant qu' elle fut consommée .
cette pièce ne sera donc pas un Taon volumineux , un corpulent Bombyle , mais bien une menue Sphérophorie , ou quelque chose de semblable , tendre repas pour un ver si délicat encore .
viendront après et par ordre croissant les pièces de haute venaison .
en l' absence de la mère , le terrier doit être clos pour éviter à la larve de fâcheuses invasions , l' entrée néanmoins doit pouvoir s' ouvrir très fréquemment , à la hâte , sans difficulté sérieuse , lorsque l' Hyménoptère rentre , chargé de son gibier et guetté par d' audacieux parasites .
ces conditions feraient défaut dans un sol consistant , tel que celui où d' habitude s' établissent les Hyménoptères fouisseurs : la porte , béante par elle même , demanderait chaque fois un travail pénible et long , soit pour être obstruée avec de la terre et du gravier , soit pour être désobstruée .
le domicile sera , par conséquent , creusé dans un terrain très mobile à la surface , dans un sable fin et sec , qui cèdera aussitôt au moindre effort de la mère et , en s' éboulant , fermera de lui même la porte , ainsi qu' une tapisserie flottante qui , repoussée de la main , livre passage et se remet en place .
tel est l' enchaînement des actes que déduit la raison de l' homme et que met en pratique la sapience des Bembex .
pour quel motif le ravisseur met il à mort le gibier saisi , au lieu de le paralyser simplement ?
est ce défaut d' habileté dans l' emploi de son dard ?
est ce difficulté provenant soit de l' organisation des Diptères , soit des manoeuvres usitées pour la chasse ?
je dois avouer tout d' abord que mes tentatives ont échoué pour mettre un Diptère , sans le tuer , dans cet état d' immobilité complète où il est si facile de plonger un Bupreste , un Charançon , un Scarabée , en inoculant , avec la pointe d' une aiguille , une gouttelette d' ammoniaque dans la région ganglionnaire du thorax .