pourquoi ce monsieur n' a pas de maison ?
parce qu' il est puni ?
parce que sa maison s' est envolée ?
parce que quelqu' un lui a volé ?
parce qu' elle est trop petite ?
le monsieur n' a pas de maison car il a vécu des malheurs .
il n' a plus d' argent pour payer sa maison , alors , en attendant , il dort dans la rue .
combien de temps encore ?
je ne sais pas , mais il retrouvera du travail et il pourra acheter une nouvelle maison , et il sera à nouveau heureux .
il n' a personne pour l' aider ?
sa famille et ses amis ne peuvent peut être pas l' aider pour le moment .
on l' invite à la maison pour l' aider ?
non , on ne peut pas , mais tu peux l' aider en lui donnant une pièce , ou juste en lui disant bonjour avec un sourire .
et nous ça peut nous arriver de ne plus avoir de maison ?
non , ça ne nous arrivera pas , car Papa et Maman ont du travail , et que nous avons une famille qui pourra nous aider .