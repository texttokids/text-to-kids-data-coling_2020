" dis maman , pourquoi est ce que tu fermes les volets ? ce n' est pourtant pas l' heure d' aller se coucher ! " " non , Theo , mais aujourd'hui il va faire très chaud . en fermant les volets et les fenêtres , j' évite à la chaleur de rentrer dans la maison . "
Theo est tout content : " chouette ! s' il fait chaud c' est qu' il y a du soleil ! je vais pouvoir aller jouer dans le jardin ! " il se précipite dans sa chambre pour prendre son chapeau et ses lunettes de soleil .
il remplit sa gourde et se tartine de crème solaire .
mais quand il veut sortir dans le jardin , sa maman l' en empêche .
" mon chéri , c' est très bien de te protéger du soleil mais les jours de canicule , cela ne suffit pas . " " j' ai aussi pris de l' eau ! " dit Theo , en brandissant avec fierté sa gourde rouge .
" tu as bien raison car il faut beaucoup boire quand il fait chaud . mais aujourd'hui , tu vas devoir rester à l' abri dans la maison . c' est plus raisonnable . car si tu restais trop longtemps au soleil , tu pourrais avoir une insolation . après , on ne se sent pas bien du tout , comme quand on a la grippe . "
le petit garçon est déçu .
il retourne dans sa chambre ranger son chapeau et ses lunettes .
mais une surprise de taille l' attend à son retour .
sa maman tient une glace à la fraise dans la main .
c' est la préférée de Theo !
il retrouve bien vite le sourire !
" tu sais , ce soir , quand il fera moins chaud , tu pourras aller jouer dans le jardin . j' ai même une idée ... mais , chut ! c' est un secret ! "
la journée se termine .
la chaleur tombe un peu .
le papa de Theo vient de rentrer .
la maman et le papa de Theo discutent ensemble , tout bas .
ils ont l' air de bien s' amuser .
" de quoi peuvent ils bien parler ? " se demande Theo .
" allez Theo ! " dit son papa .
" enfile ton maillot de bain et rejoins moi dans le jardin ! " Theo file dans sa chambre .
son joli maillot bleu et vert est posé sur son lit !
il se déshabille et , quelques instants plus tard , il sort dans le jardin .
son papa est là .
sa maman aussi .
ils sont en maillot , comme Theo .
sur la table du jardin , il y a trois superbes pistolets pleins d' eau .
et à côté , la maman de Theo a rempli un seau d' eau fraiche pour pouvoir les remplir à nouveau .
Theo a juste le temps de les apercevoir que son papa se met à crier : " que la bataille d' eau commence ! " chacun attrape un pistolet et c' est parti !
quelle rigolade !
que c' est bon de jouer à s' arroser quand il fait chaud dehors !