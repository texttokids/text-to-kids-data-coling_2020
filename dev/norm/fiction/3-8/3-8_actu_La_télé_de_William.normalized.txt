depuis qu' il est tout petit , William rêve de devenir journaliste .
il est toujours partant pour faire rire les autres et adore faire le " gugusse " .
en colonie de vacances , il propose à tout le monde un nouveau jeu .
faire une émission en classe .
le lendemain matin , William se réveille très tôt .
il fait encore nuit lorsqu' il réveille son copain Vincent pour aller cueillir des légumes pour cuisiner .
" Vincent , hop hop hop , il est quatre heures trente , on se dépêche , c' est l' heure de faire à manger ! " .
laura , les yeux encore plein de sommeil , demande à William ce qu' il se passe .
" ah bah Laura tu tombes bien , trouve la meilleure recette que Vincent pourrait réaliser pour le petit déjeuner . après , on prend la caméra et on se filme " .
à sept heures , tout le camp est debout .
William a réuni tous les copains autour de lui .
" Laurent c' est toi qui nous présente la météo de la journée . au moins comme ça , les moniteurs sauront s' il faut jouer à l' intérieur ou à l' extérieur . laura dit à William : " la caméra est branchée , on diffuse sur Internet " .
Vincent arrive avec une cagette remplie de champignons et William vient à son secours pour porter les oeufs .
l' émission démarre et se déroule parfaitement .
tous les copains applaudissent et participent même .
Vincent fait des heureux avec ses formidables oeufs brouillés aux champignons .
" encore ! encore ! " , dit le petit Laurent !
je veux en profiter car il n' y aura sans doute plus de champignons pendant longtemps .

" ah bon , pourquoi " lui demande Laura ?
" parce qu' il ne va pas pleuvoir , la chaleur arrive et quand il fait chaud , les champignons ne poussent plus . "
William dit alors " un jour cette émission sera vue par tout le monde ! " " on parlera de tout , même des histoires de champignons . "
bien des années plus tard , le petit William présentera une émission matinale , que toute la France regardera et aussi dans bien d' autres pays !