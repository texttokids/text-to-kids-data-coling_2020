c' est quoi les quatre saisons ?
l' hiver , où il fait très froid .
les feuilles des arbres tombent , il y a de la neige partout .
certains animaux dorment , on dit qu' ils hibernent .
le soleil se couche très tôt , les journées sont plus courtes .
le printemps , où tout le monde se réveille .
les fleurs poussent dans la prairie .
les feuilles poussent dans les arbres .
les animaux sortent de leur tanière et il fait plus chaud .
l' été , où il fait très beau , et très chaud , et il ne pleut presque plus .
le soleil se couche très tard et les journées sont plus longues .
c' est aussi le moment des grandes vacances .
l' automne , les feuilles changent de couleurs et tombent des arbres .
il commence à faire un peu froid .
la pluie est de retour .
puis l' hiver revient et ainsi de suite .
les saisons se partagent l' année , chacune à la même place depuis toujours .
moi j' aime l' hiver car je peux faire des bonhommes de neige .
au printemps les fleurs sentent particulèrement bons .
j' aime l' été car je peux me baigner à la mer .
et l' automne pour ramasser les feuilles , des champignons .
j' aime toutes les saisons !