Emma et la lune de Noël
ce n' est pas au Père Noël qu' Emma donne sa liste .
quand Emma voit la lune si loin dans le ciel et si près d' elle , elle sait que sa liste va aller tout droit vers elle .
elle sait que la lune est son amie .
elle pense même que la lune lui fait un grand sourire .
la lune n' est pas toujours là haut , perchée dans la nuit , mais quand elle est là , Emma lui parle : " je ne t' écris pas , maman lune , parce que je ne sais pas encore écrire , mais je vais te dire ma liste de cadeaux : ma liste est courte parce que tout ce que je veux c' est toi , c' est la lune . "
Emma a l' impression que la lune lui fait un clin d' oeil et lui répond : " voilà ! c' est fait ! je me donne à toi ! "
elle ne se fait pas de soucis .
c' est promis .
Emma aura la lune .
Emma voit les paquets que le Père Noël a déposé dans l' armoire de l' entrée parce que sa hutte est trop lourde le soir de Noël .
et Emma sait que dans l' un de ces paquets il y aura un grand ballon blanc tout doux , tout mou , qui sera son doudou magique .
sa maman fait du pain d' épices en forme de bonhommes et Emma aimerait croquer la lune .
nono lui répète sa liste tous les jours .
c' est une liste qui n' est qu' un long bouchon de camions , de voitures , d' avions , de trains .
comment peut on désirer des camions quand on peut avoir la lune ?
Emma compte les jours .
nono n' arrête pas de lui parler des camions rouges et verts , grands et petits .
et le jour de Noël arrive avec sa dinde et ses marrons et Emma est confiante .
elle aura la lune .
la lune le lui a dit .
elle commence par les plus petits paquets .
pas de lune .
c' est normal .
les paquets grandissent avec son espoir .
l' avant dernier paquet n' est que l' avion de Barbie .
il ne reste plus qu' un .
Emma voit que c' est rond .
elle sait que maman lune tient ses promesses .
elle regarde le ciel et elle voit que la lune n' est plus là , disparue du ciel pour atterrir chez elle .
elle ouvre le paquet .
Emma ne peut pas cacher sa déception .
elle fait une grimace .
" c' est quoi ça ? "
" c' est un globe . avec ça , tu sais où tu te trouves parmi tous les pays . "
" mais ce n' est pas lune ... "
" non , c' est la terre ! "
Emma pense qu' il faut y aller petit à petit , d' abord la terre et après la lune .
ce sera pour l' an prochain la lune !