ont boosté le PAF .
et l' attribution
de fréquences est loin d' être terminée ,
que ce soit pour les chaines TNT ou pour les autres supports comme la haute définition ( TVHD ) ou la télévision mobile personnelle ( TMP ) .
W La TNT .
le CSA a sélectionné
deux cent huit nouvelles zones géographiques
où la télévision numérique terrestre devra être reçue avant la fin deux mille huit .
ces nouvelles zones s' inscrivent dans le cadre de l' extension de la couverture visant à atteindre quatre vingt cinq pour cents
de la population métropolitaine d' ici la fin
de l' année .
l' extension se poursuivra
pour atteindre quatre vingt quinze pour cents fin vingt cent onze .
W La HD .
le ministre de la Culture , Christine Albanel , a sélectionné la chaîne publique France deux , en concurrence avec la chaîne franco allemande Arte , pour bénéficier
du canal dédié à la HD , préempté
par le gouvernement .
d' ici la mi novembre ,
le CSA doit attribuer deux autres canaux
à des chaines privées .
tf1 et M6 ont toutes les chances de se les voir attribuer .
W La TMP .
selon le président du CSA ,
un appel à candidatures pour des chaînes
sur la télévision mobile personnelle ( TMP ) sera lancé " dans la première quinzaine
de novembre " .
cet appel porterait sur quatorze
à dix sept fréquences de télévision .
" je souhaite que nous continuions d' avancer sur
les aspects économiques de la TMP , afin que son lancement ouvre de façon durable une nouvelle page dans l' histoire de la télévision " , a dit Michel Boyon , pour qui " une offre
de chaines enrichie est l' occasion de faire davantage vivre le pluralisme des médias " .
fe .
N
idées
Deleuze et Guattari à la croisée des concepts
François Dosse montre dans une biographie toute la force créative de la " conversation " engagée , vingt ans durant , par les deux intellectuels .
gilles Deleuze
Félix Guattari ,
biographie croisée ,
de François Dosse , Éditions La Découverte , six cent quarante huit pages , vingt neuf , cinquante euros .
" la preuve du pudding , c' est qu' on le mange " , disait Engels .
la preuve de l' existence matérielle d' un objet , c' est la pratique que l' on met en oeuvre à son égard .
la preuve de l' existence d' une pensée , c' est sa manifestation orale ou écrite .
écrire à quatre mains oblige donc à un difficile " agencement " .
cette biographie croisée de Gilles Deleuze et Félix Guattari que nous offre l' historien de la philosophie , François Dosse , ne juxtapose ni ne mélange l' itinéraire singulier de chacun des deux réputés amis .
le premier , philosophe , disait du second , psychanalyste , " ce qui compliquera les choses , c' est que je veux prendre à Félix quelque chose qu' il ne voudra jamais me donner , et lui , m' entraîner quelque part où je ne voudrais jamais aller " .
gilles avait besoin de l' espace de la " folie " et Félix de concepts rénovés .
le " château de La Borde " , clinique psychiatrique dans le Loir et Cher et utopie réalisée , fit l' affaire .
mélange ultrahétérogène .
ils écriront ensemble : " créer des concepts est toujours lié à un devenir révolutionnaire , la seule chose qui soit universelle dans le capitalisme , c' est le marché " .
Deleuze qui avait toujours eu horreur des discussions relevant , selon lui , de l' échange stérile d' opinions , oppose la pratique des conversations qui engagent une véritable polémique intérieure à l' énonciation , à ce qui était jusque là ses " armes lourdes philosophiques " et son " intendance bibliographique " .
chacun tour à tour parle et formule des concepts : c' est l' essence même de la nouvelle réflexion philosophique fondée sur l' interdisciplinarité .
l' auteur de ce " croisement " cite Deleuze lui même : " nous n' avons pas collaboré comme deux personnes . nous étions plutôt comme deux ruisseaux qui se rejoignent pour en faire un troisième qui aurait été nous ( ... ) . si on fait quelque chose ensemble , c' est que ça marche et qu' on est porté par quelque chose qui nous dépasse . gilles est mon ami , non mon copain " .
l' idée
d' " agencement " est fondamentale pour comprendre la singularité
de ce dispositif de recherche .
avec son concept de " machine " et sa proposition de le substituer à la notion de " structure " qui avait envahi les idées européennes de l' époque , Guattari offre à Deleuze une possible porte de sortie de
la pensée structurale : " je travaillais alors uniquement dans les concepts , et encore de façon très timide . Félix m' a parlé de ce qu' il appelait déjà les machines délirantes : toute une conception théorique et pratique de l' inconscient machine , de l' inconscient schizophrénique . alors j' ai eu l' impression que c' était lui qui était en avance sur moi ... " .
les deux intellectuels ont ainsi formé , pendant vingt ans , une sorte de laboratoire de mise à l' épreuve des concepts dans leur efficacité , grâce
au caractère transversal
de la démarche reliant psychanalyse et philosophie .
l' originalité de ce croisement qui commence en dix neuf cent soixante neuf et s' achève au cours de l' été dix neuf cent quatre vingt onze est manifeste dès leur première oeuvre commune l' Anti OEdipe un , premier tome de Capitalisme et schizophrénie .
à d' autres moments , c' est Deleuze qui , reprenant la démarche de Nietszche trainant après lui une masse de mythes antiques , faisait montre d' une indéniable avance philosophique .
à la fin d' un livre et de sa publication , l' agencement marque un temps d' arrêt .
de nouveaux moyens d' expression philosophiques sont alors à disposition pour poursuivre la tentative systématique de décodage .
les deux auteurs cherchant à échapper à toute forme de " codage " en se laissant interpeller par les forces du dehors pour défaire les formes convenues .
cet " horizon nomade " sera atteint dans le second volume de Capitalisme et schizophrénie publié en dix neuf cent quatre vingts sous le titre Mille plateaux .
Guattari est devenu un prodigieux inventeur de " concepts sauvages " , jetant ainsi les bases de leur dernière oeuvre commune en dix neuf cent quatre vingt onze , Qu' est ce que la philosophie : le cheminement des concepts dans l' historicité des civilisations .