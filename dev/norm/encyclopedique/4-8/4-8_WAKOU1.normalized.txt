
histoire en image
petites oies du Canada
ils viennent de naitre ici , dans ce nid .
dès qu' ils ont tous été prêts , leur mère les a emmenés prendre leur premier bain : plouf !
les oisons ont encore leur duvet jaune de naissance et leurs ailes sont minuscules !
qui sont ces oisons qui suivent leur maman sur l' eau ?
ce sont les petits de l' oie bernache du Canada .

en quelques semaines , l' oison a bien , grandi et ses petites ailes ...
aussi !
les poussins restent groupés quand leur maman s' éloigne .
et dès qu' elle revient ils se précipitent sous ses plumes : hop , à l' abri !
bientôt , ces jeunes oies auront leur beau collier blanc sur les joues et de grandes ailes pour parcourir le ciel .