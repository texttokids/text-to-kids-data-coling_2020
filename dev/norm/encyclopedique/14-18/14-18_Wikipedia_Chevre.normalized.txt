les termes chèvre , Capra et caprin dérivent du latin capra
l' épithète spécifique dérive du latin hircus , nom désignant le
pourrait aussi être rattaché les mots de langues régionales désignant
on a donné aux caprins domestiques le nom scientifique de Capra hircus
au XVIII E siècle , avant le développement de la biologie de
l' évolution .
cette dernière a mis en lumière l' étroite relation
existant entre les races domestiques et sauvages .
dans ce contexte , le
statut scientifique des " espèces " domestiques a été remis en cause ,
et beaucoup de biologistes ne les considèrent plus désormais que comme
des formes domestiquées des espèces sauvages originelles .
une espèce est en effet constituée de " groupes de populations
naturelles , effectivement ou potentiellement interfécondes , qui sont
génétiquement isolées d' autres groupes similaires " .
or , les
elles en ont l' occasion .
" vu que , du moins en ce qui concerne les
races d' animaux domestiques primitives , celles ci constitueraient , en
règle générale , une entité de reproduction avec leur espèce ancestrale ,
si elles en avaient la possibilité , la classification d' animaux
domestiques en tant qu' espèces propres n' est pas acceptable .
c' est
pourquoi on a essayé de les définir comme sous espèces " .
on donne alors à la nouvelle sous espèce le nom de l' espèce d' origine ,
complété par le nom de sous espèce ( qui reprend l' ancienne
plus en plus la désignation " forma " , abrégée " F " , qui exprime
clairement qu' il s' agit d' une forme d' animal domestique qui peut
vraisemblablement d' abord pour son lait , puis pour sa laine ,
la chèvre ( Capra aegagrus ) semble avoir été domestiquée une
première fois il y a environ dix ans ( fin de la dernière glaciation )
dans les monts Zagros et sur les plateaux d' Iran .
l' autre
centre de domestication connu , le plus important quantitativement , est
la Bible mentionne , dans le livre de la Genèse , que
Rebecca prépare à son mari Isaac deux chevreaux pour qu' Isaac
les analyses génétiques d' ADN fossile laissent penser que les hommes
ont d' abord protégé des populations de chèvres sauvages en tuant leurs
prédateurs .
puis les tribus ont commencé à les élever pour avoir plus
facilement sous la main du lait conservé sous forme de fromage , des
poils , de la viande et des peaux .
les chèvres domestiques
les collines ou sur d' autres domaines de pâturage analogues .
les
chevriers qui les soignaient étaient souvent des enfants ou des
adolescents , pareils à l' image que nous nous faisons du berger .
ces
méthodes de garde se rencontrent encore aujourd'hui .
la domestication des chèvres a probablement engendré des modifications
significatives des paysages et des écosystèmes ( recul des zones
arborées au profit des buissons et " maquis " ) .
la peau de chèvre est utilisée pour le transport de l' eau , du lait
caillé ou du vin .
historiquement , elle servait aussi à produire le
parchemin , qui était le support le plus employé pour écrire en
Europe jusqu'à l' invention de l' imprimerie et la vulgarisation du
anatomie de la chèvre : un .
mufle deux .
chanfrein trois .
front quatre .
cornes cinq .
encolure six .
garrot sept .
épaule huit .
dos neuf .
reins dix .
flanc onze .
croupe douze .
cuisse treize .
grasset quatorze .
mamelle quinze .
jarret seize .
canon dix sept .
pied dix huit .
trayons dix neuf .
abdomen vingt .
coude vingt et un .
boulet vingt deux .
onglon vingt trois .
boulet vingt quatre .
la chèvre est un animal d' assez petite taille , à cornes arquées ou
sans corne ( " motte " , polled pour les anglo saxons ) , très
agile , particulièrement adapté au saut .
sa température interne normale
est assez élevée ( de trente huit à trente neuf , cinq degrees Celsius ) .
on la trouve dans toutes les
régions du globe , particulièrement en montagne .
les mâles sont
appelés boucs et les petits sont des chevreaux ou chevrettes
les yeux de la chèvre ont une particularité , leur pupille est
rectangulaire et horizontale , ce qui lui donne un regard étrange , cela
lui permet en fait d' avoir un plus large champ de vision .
la chèvre adulte a trente deux dents : huit incisives inférieures qui s' appuient
sur la gencive supérieure qui forme un bourrelet résistant ( elle n' a
pas d' incisives supérieures ) .
le fond de la bouche est garni de
les chèvres ont toutes soixante chromosomes par cellule .
la
chèvre mesure entre quatre vingts et cent centimètres , et pèse , selon ses origines , entre quinze
cette section est vide , insuffisamment détaillée ou incomplète .
la chèvre est un animal relativement intelligent , s' attachant
volontiers au soigneur .
c' est une grimpeuse adaptée aux escarpements
rocheux , aux murailles ou aux arbres si leur feuillage est convoité ,
poussée par son instinct d' exploratrice , elle se retrouve parfois dans
les chèvres raffolent de l' herbe à la puce , de l' armoise
tridentée , de l' euphorbe ésule et du kudzu .
voilà pourquoi
elles sont les stars du désherbage sélectif .
on a recours à ces
brouteuses pour éliminer les végétaux indésirables , telles les espèces
invasives et les broussailles alimentant les feux de forêt .
cette
pratique est populaire en Amérique du Nord et en Australie .
la location de troupeaux a commencé à prendre de l' essor il y a une
dizaine d' années , selon John Walker , écologue à l' université
Texas A&M .
on utilise à la fois des moutons et des chèvres , mais ces
dernières sont plus appréciées en raison de leurs gouts éclectiques , de
leur bon équilibre sur un terrain pentu et de leur capacité à brouter
plus haut , en appui sur leurs pattes arrière .
les chèvres dont un
troupeau de cent têtes peut se louer cent cinquante euros la journée arrachent
aussi les feuilles avec précision .
de nombreux clients tels que les
parcs , les ranchs et ou encore des particuliers font appel à leurs
déforestation , voire à la désertification par surpâturage
certaines races de chèvre , parmi les moins rustiques ( alpine , saanen ,
et caetera ) ne peuvent rester en plein air pendant les mois d' hiver .
comme
beaucoup d' animaux d' élevage , elles doivent avoir accès à de l' eau en
beaucoup d' espèces de chèvres sont victimes d' infestations
parasitaires .
les vers intestinaux peuvent être éradiqués par
vermifugation .
il convient de contenir également l' infestation du
pelage par les puces , tiques ou autres parasites .
la chèvre est un ruminant : elle possède quatre estomacs .
dans un
premier temps , elle avale grossièrement ses aliments puis les régurgite
lorsqu' elle est au calme pour les mâcher , c' est ce que l' on appelle la
rumination .
elle les avale de nouveau ensuite dans un autre estomac où
elle se nourrit de toutes sortes de végétaux sauvages ou cultivés .
ces
qu' elle pâturera ou d' un foin composé de légumineuses ( vesce ,
lotier , luzerne , et caetera ) et de graminées ( dactyle , ray grass , et caetera ) ,
la météorisation est une affection qui peut tuer une chèvre en
quelques heures .
elle est en général provoquée par la consommation de
repousses d' herbes , ou d' herbe trop mouillée de rosée , ou d' un brusque
refroidissement .
la digestion étant brutalement arrêtée , l' herbe
fermente dans l' appareil digestif , occasionnant le dégagement de gaz .
au printemps , pour les troupeaux faisant la transition entre les
chèvreries et les pâtures ( " mise à l' herbe " ) , les éleveurs continuent
cette section est vide , insuffisamment détaillée ou incomplète .
la chèvre peut se reproduire dès l' âge de sept mois .
en général , les
chaleurs ont lieu à la fin de l' été ( environ soixante jours après que les
jours commencent à décliner ) .
la gestation dure cinq mois , au terme de
laquelle la chèvre met bas un ou plusieurs chevreaux .
on
procède au sevrage des petits à environ deux mois ( entre quatorze kilogrammes et seize kilogrammes ) .
la chèvre est parfois utilisée comme organisme modèle ou
animal de laboratoire .
des chèvres transgéniques ont été
puis utilisées pour produire des molécules chimiques complexes
attendait puis modifiées en intégrant des gènes humains dès
le début des années dix neuf cent quatre vingt dix ) , pour produire des hormones
plus récemment ( vingt cent cinq vingt cent six ) , des chercheurs de l' industrie des
biotechnologies ont ainsi produit des chèvres transgéniques qui
synthétisent dans leurs glandes mammaires des molécules qu' on peut
ensuite extraire , dont le lysozyme ( point lz ) humain qui a
donnée à des cochons ( il semble améliorer le fonctionnement de leur
articles connexes : liste de races caprines et Liste des
le genre Capra comprend des espèces comme la chèvre domestique , la
chèvre sauvage ( Capra aegagrus ) , le bouquetin , ou le
chèvres alpines posant pour les photographes au hameau Les
l' Alpine est originaire du massif alpin suisse et français .
le
berceau de la race se situe en suisse où elle conserve un cheptel
notable .
c' est la race la plus répandue en France .
morphologie : poil
ras , robe de couleur variée passant du blanc pur au blanc tacheté de
brun , de fauve , de gris , de noir , de pie ou de roux .
les troupeaux
sélectionnés génétiquement présentent une couleur plus homogène , marron
avec les extrémités et la ligne dorsale noire .
la poitrine est
profonde , le bassin large et peu incliné .
les membres sont solides , les
articulations sèches et les aplombs corrects .
la mamelle est
volumineuse , bien attachée , se rétractant bien après la traite .
les
trayons sont distincts de la mamelle , sont dirigés vers l' avant et
sensiblement parallèles .
c' est une chèvre de format moyen : cinquante kilogrammes à
soixante dix kilogrammes pour la femelle , quatre vingts kilogrammes à cent kilogrammes pour le mâle .
rustique , très
appréciée pour ses qualités laitières et d' élevage , la race Alpine
s' adapte aussi bien aux systèmes d' élevages stabulatoires qu' aux
la Saanen ( ou chèvre de Gessenay ) , blanche à poils courts ,
généralement dépourvue de cornes ( mais certaines ont une paire de
cornes de taille moyenne tournées vers l' arrière ) , calme et excellente
laitière .
elle est originaire de la haute vallée de la Sarine
entrainé l' implantation de la race dans de nombreux pays .
on peut
considérer aujourd'hui que la Saanen est la race la plus répandue
mondialement parmi les races laitières caprines .
c' est un animal trapu ,
solide et paisible , aux qualités très laitières qui s' adapte très bien
aux différents modes d' élevage notamment intensifs .
morphologie : poil
court , dense et soyeux , robe uniformément blanche , tête avec un
profil droit , poitrine profonde , large et longue , caractérisant une
grande capacité thoracique , épaule large et bien attachée , garrot bien
en viande , aplombs sont corrects et allures régulières , mamelle
globuleuse , bien attachée et larges à la base .
c' est une race à fort
développement : cinquante kilogrammes à quatre vingt dix kilogrammes pour la femelle , quatre vingts kilogrammes à cent vingt kilogrammes pour le
la Toggenbourg , excellente laitière , de pelage fauve à sombre ,
caractérisée par ses deux bandes blanches de l' oreille à la bouche .
souvent utilisé durant le haut Moyen Âge comme animal sacrificiel pour
les rituels de fertilité des fêtes de la Saint Abondance , elle était
ensuite dépecée , et sa peau utilisée pour fabriquer des pagnes qui
la chèvre poitevine est une race originaire des alentours des
sources de la Sèvre niortaise , dans le centre ouest de la France .
plus de quarante au début du XX E siècle .
en dix neuf cent vingt cinq , une épizootie de
fièvre aphteuse a décimé les troupeaux poitevins .
c' est à partir de
souches prélevées dans les Alpes que le troupeau fut progressivement
reconstitué .
on dénombre aujourd'hui environ deux huit cents femelles
principalement élevées dans le berceau de la race en
poitou charentes .
voici sa morphologie : robe de couleur brune ,
plus ou moins foncée , parfois presque noire dite " en cap de maure " ,
poils semi longs sur le corps et sur les cuisses , face inférieure des
membres , dessous du ventre et de la queue blancs ou très clairs , face
comportant une raie blanche de chaque côté du chanfrein encadrant une
tête fine , triangulaire .
la chèvre poitevine est avec ou sans cornes ,
avec ou sans barbiches ou pampilles .
la poitevine est une chèvre de
format moyen à grand , d' aspect longiligne .
les mâles peuvent atteindre
soixante quinze kilogrammes .
la chèvre pèse entre quarante kilogrammes et soixante cinq kilogrammes .
sa taille au garrot est de
soixante dix quatre vingts centimètres .
rustique et de caractère paisible , la chèvre poitevine est
appréciée pour son lait typique aux grandes qualités fromagères .
elle
présente de bonnes capacités pour valoriser les pâturages et les
la chèvre provençale est une race française de Provence .
elle
fait partie des races à petit effectif , puisqu' on compte environ
un chèvres en deux mille treize .
la couleur de la robe peut être très
diversifiée , c' est une chèvre aux oreilles longues et tombantes ,
parfois recourbées .
elle a le poils long en particulier sur les
bouches du rhône , faisant partie des races à petit effectif , le
cheptel compte environ huit bêtes en deux mille treize .
la robe est généralement de
couleur marron rouge et parfois tachetée de blanc .
elle possède des
cornes de section triangulaires et torsadées de taille importante qui
permette de la reconnaitre facilement .
avec son lait on fabrique un
fromage typique appelé la Brousse .
c' est une race mixte puisqu' elle est
utilisé pour la production de lait et la transformation fromagère , mais
la chèvre Boer améliorée est apparue au début des années mille neuf cent
lorsque des éleveurs d' Afrique du Sud ont commencé à sélectionner
pour une chèvre démontrant une bonne conformation bouchère , une
croissance et une prolificité élevées , une bonne qualité de carcasse ,
un pelage court blanc sur le corps et rouge ( variant de cannelle à
presque noir ) sur la tête et le cou .
en mille neuf cent quatre vingt treize , la chèvre Boer est
apparue au Canada et elle a été importée pour la boucherie .
ses
oreilles sont pendantes et son nez est assez arrondi ou busqué .
ses
cornes sont rondes et courbées vers l' arrière .
aux États Unis , on retrouve la chèvre Tennessee ou
chèvre myotonique .
cette espèce de chèvre est présente
principalement au Tennessee et au Texas .
cette race remonte à
la fin des années mille huit cent .
cette variété de chèvres a la
particularité de se tétaniser littéralement quand elle est stressée ou
surprise : l' animal tombe sans pouvoir bouger pendant environ une
vingtaine de secondes .
cette chèvre est affectée par la myotonie
d' un stress ) , ce qui expliquerait la tendreté de la viande et la forte
musculature de la carcasse de cette race .
l' origine de la chose serait
une mutation génétique héréditaire .
cette chèvre est surtout élevée
pour son potentiel en boucherie .
il existe trois différents types dont
un seul est potentiellement intéressant pour la production de
boucherie .
seul un type de grande taille à forte musculature et
myotonique peut être qualifié de chèvre Tennessee .
ces lignées
produisent une chèvre désaisonnalisée , très résistante aux parasites ,
très maternelle , à facilité de mise bas , très facile à garder et à
manipuler puisqu' elle ne saute pas , probablement le meilleur rendement
de carcasse , mais à croissance très lente .
il existe des lignées
anciennes avec un potentiel de boucherie très intéressant .
à propos de
cette chèvre , on dit qu' elle feint la mort , ce qu' on appelle la
thanatose ( du grec ancien alpha , thanatos , " mort " ) , et
consiste à simuler la mort , afin d' échapper à un prédateur .
de nombreux
animaux ont recours à cette technique , comme l' opossum , la
la chèvre nubienne est surtout une chèvre laitière , mais peut également
même chose que pour la Nubienne , la Mancha est surtout une chèvre
laitière , mais peut également être élevée pour sa viande .
elle est
des chèvres d' espèces naines sont également élevées comme animaux
certaines races sont élevées spécialement pour leur pelage : tel
est le cas de la chèvre angora , originaire de Turquie ( Angora
est l' ancien nom d' Ankara ) dont le poil sert à produire le
d' autres le sont uniquement pour leur viande , à l' instar de la race
le mot " kiko " a été utilisé traditionnellement en
nouvelle zélande par les Maoris pour décrire les animaux de
boucherie .
la chèvre Kiko a été développée en Nouvelle Zélande par la
sélection des meilleures et des plus fertiles chèvres férales de ce
pays au niveau de leur capacité de production de viande améliorée dans
des conditions de pâturages naturels où le broutement arbustif est
important .
le taux de croissance est probablement la caractéristique
qui définit le mieux la race Kiko .
les chevreaux présentent une vigueur
impressionnante .
elle est aussi très rustique .
la chèvre Kiko peut être
maintenue sous des conditions d' élevage extensif dans les milieux
ouverts broussailleux .
elle n' est pas que présente en Nouvelle Zélande ,
mais elle a aussi été introduite au Canada , dans la province de Québec .
les boucs matures possèdent des cornes distinctives en spirale et de
grande envergure .
les oreilles de la Kiko sont placées assez hautes , de
largeur moyenne et longueur modérée , non pendantes et non dressées .
son
museau est bien proportionné , ni convexe ni concave .
la densité de son
pelage peut varier en fonction des conditions climatiques et il y a une
variation marquée entre le pelage d' été et d' hiver .
la couleur
prédominante de sa robe est le blanc , mais toute autre couleur est
au Canada , la production caprine est divisée en trois productions ,
soit la production laitière , la production de viande et la production
au Québec , le nombre de chèvres représente environ dix sept pour cents
du cheptel canadien .
la production laitière est la principale
production caprine .
le lait est surtout transformé en fromage ,
mais aussi en yogourt et en beurre .
l' élevage de la chèvre de
boucherie est une production en plein essor .
la viande de chèvre ne
fait pas partie des habitudes alimentaires des Québécois , mais la
demande provient principalement des différentes ethnies présentes au
Québec .
par ailleurs , la chèvre Boer est la race de
boucherie que l' on retrouve le plus dans les cheptels du Québec , mais
d' autres races sont aussi utilisées pour la production de chevreau de
boucherie , notamment la Nubienne et la Kiko .
la viande provenant des
mâles de toutes races et des chèvres laitières de réforme peut aussi
consommation , cependant le chevreau demeure la viande la plus
les trois productions caprines sont regroupées dans un seul plan
conjoint .
le plan conjoint est utilisé par plusieurs
productions agricoles du Québec , il permet d' améliorer les conditions
de mise en marché et la structure d' offre des produits agricoles , tout
en améliorant l' approvisionnement des transformateurs et en stabilisant
les revenus des producteurs .
la mise en marché du lait de
chèvre est plutôt bien établie , cependant la mise en marché des
productions de viande et de mohair est à développer .
en deux mille dix sept , la mise
en marché de la viande caprine du Québec est individuelle , c' est à dire
que chaque producteur fait sa propre mise en marché .
par exemple , en
deux mille onze , plusieurs producteurs de chèvre de boucherie vendaient
leurs produits directement à la ferme , dans des restaurants et à des
commerçants .
d' autres producteurs vendent tout simplement à
l' encan et certains vendent des sujets de reproduction .
en France , la chèvre est élevée surtout pour son lait , qui
sert à la fabrication de fromages mais les chèvres laitières de
réformes ainsi que les chevreaux , issus des inséminations annuelles
pour la production de lait , fournissent tout de même de la viande .
en
deux mille treize , sur un cheptel total de un .
deux million de caprins , cent trente cinq caprins
de réforme et six cent quarante quatre chevreaux ont été abattus .
les races caprines : en France deux races dominent : l' alpine ,
environ cinquante cinq pour cents du cheptel , la saanen , environ un quart .
le
des passages de cette section sont obsolètes ou annoncent des
les principaux pays par l' importance de leur cheptel caprin sont les
la bonne digestibilité des laits de chèvre pourrait s' expliquer en
partie par leur teneur en acides gras courts , par la petite taille
des globules gras qui les composent , mais également par leur richesse
en triglycérides à chaîne moyenne et courte .
la réputation
d' innocuité du lait de chèvre en matière d' allergie mérite en
le lait de chèvre est aussi utilisé pour fabriquer des fromages
très populaires comme le cabécou et la féta , bien qu' il
puisse aussi être transformé en n' importe quel type de fromage .
on
appelle souvent le fromage de chèvre simplement " chèvre " .
exemples de fromages de chèvre de Provence .
exemples de
fromages de chèvre de Provence .
exemples de fromages de chèvre de
chèvre sont protégées par le système AOP : banon un ,
chabichou du Poitou un , charolais un , chevrotin
pélardon un , picodon un , pouligny saint pierre
sainte maure de touraine un , selles sur cher un ,
les chevreaux mâles sont utilisés au moment des fêtes de Pâques en
il existe également ce que l' on appelle le chevreau lourd .
le syndicat
caprin de la Drôme a annoncé en vingt cent dix huit travailler en la création d' un
la peau de chèvre est principalement utilisée dans la confection
d' instruments à percussion , comme le djembé , le bendir et le
sabar , et aussi à cordes , comme la kora .
elle peut aussi
servir dans la fabrication de certains vêtements et accessoires .
article connexe : histoire de la laine et du drap .
la toison de la chèvre angora sert à fabriquer le mohair , une
sorte de laine .
sa laine est non seulement un très bon isolant
thermique , mais les vêtements fabriqués avec cette matière sont très
légers à porter .
on peut aussi en faire des couvertures .
de ventôse , est officiellement dénommé jour de la Chèvre .
nourrit Zeus enfant , entre autres avec de l' ambroisie et du
nectar contenus dans ses cornes .
l' une de ses cornes , brisée par le
jeune dieu , fut transformée en Corne d' abondance .
une autre
version dit que Zeus , à la mort de la chèvre , fit un bouclier de sa
peau ( l' Égide ) , ce qui le rendait invincible , car celle ci ne
l' attention des hommes de Delphes vers le lieu où des fumées
sortaient des entrailles de la terre .
prises de vertige , elles
dansaient .
intrigués par ces danses , des hommes auraient compris le
sens des vapeurs émanant de la terre : il leur fallait interpréter
dans l' Hymne homérique , il porte barbe , cornes et pieds de chèvre ,
mais dans l' art figuré , il est parfois représenté sous les traits
d' un jeune homme à tête de chèvre avec une courte queue de chèvre .
nourricière : Heidrun , la chèvre qui donne son lait composé
d' hydromel aux guerriers d' Odin .
Heidrun ( ou heithrun , clair
ruisseau ?
) est une chèvre vivant au Valhalla et broutant les
chez les Scandinaves , la chèvre incarnait " l' esprit du blé "
sous le nom de " Kornbocke " .
le julbock ou chêvre de Noël est
une figurine de chêvre en paille utilisée comme décoration de Noël .
ses origines remontent aux deux boucs Tanngrisnir et
chèvre est le symbole de la substance primordiale non manifestée .
c' est la mère du monde Prakriti .
les trois couleurs qui lui
sont attribuées , le rouge , le blanc et le noir correspondent aux
trois guna ou qualités primordiales , respectivement
de la terre et même plus précisément de l' agriculture et de
l' élevage .
dans la religion primitive tibétaine , la divinité en
question avait les traits d' un caprin à poils longs .
par ailleurs ,
certaines peuplades de la Chine mettent la chèvre en rapport avec
le dieu de la foudre : la tête de la chèvre sacrifiée lui sert
d' enclume .
toujours au Tibet , des chèvres blanches auraient
contribué à l' édification du palais du Potala à Lhassa
négociant sans relâche toutes les sinuosités des chemins escarpés
dans l' Ancien Testament , on voit que la chèvre est souvent offerte
en sacrifice lors des rituels juifs .
dans le Cantique des
cantiques , les cheveux de l' amoureuse sont comparés à un troupeau de
chèvres suspendues aux flancs de Galaad ( Ct quatre .
un ) .
inoubliable héroïne des célèbres Lettres de mon moulin ,
souvent une arme redoutable dans les combats dans les Johan et
porte pas de nom ) délivre Tintin de deux bandits qui ont
l' intention de le tuer en l' obligeant à sauter du haut d' une
falaise .
cette chèvre est délivrée par Milou du pieu auquel elle
est attachée .
en poursuivant Milou qui se dirige sur les deux
hommes , la chèvre les déstabilise et sauve Tintin de la mort .
du côté de la sculpture , Jean Cocteau a sculpté plus d' une fois
des têtes de chèvres .
en dix neuf cent cinquante huit , il a sculpté une tête de chèvre orange .
il a aussi sculpté une tête de chèvre verte en bronze .
Gé Pellini
Picasso a aussi constitué une sculpture de chèvre très originale
pour sa propre chèvre , Esmeralda .
le ventre est constitué d' un panier ,
ses deux pis sont des pots à lait en céramique , les cornes ont été
taillées dans des ceps de vigne , le tout assemblé avec du plâtre .
elle
est aussi faite d' une boite de conserve et pour son dos , une feuille de
palmier .
en dix neuf cent soixante treize , le français Jean Marais a sculpté une tête de
cabri orange , qui est exposée au musée Jean Marais à Vallauris .
en
mille neuf cent soixante trois , le sculpteur animalier Joseph Constant a réalisé un groupe de
chèvres en bronze , installée sur une place à La Courneuve .
peinture qui représente Zeus enfant nourri par la chèvre
Almathée .
elle est exposée au Musée du Louvre à Paris .
accompagné de chèvres , intitulé Grange avec des chèvre .
la jeune fille à la chèvre , qui est conservée au Musée Lambinet à
femme avec des chèvres dans les dunes .
caméra .
qu' il aimait beaucoup .
l' une de ses plus belles peintures caprines ,
parmi les mieux connues , est sans doute une peinture à l' huile
qu' il a peint en mille neuf cent six , intitulé Jeune fille avec une chèvre , et qui
a été reproduite de nombreuses fois .
il s' agit d' une jeune femme
nue jouant dans ses cheveux et un garçonnet nu portant un vase sur
sa tête accompagnés d' une chèvre lors de leur marche .
pour lui , les
chèvres semblaient représenter la joie de vivre .