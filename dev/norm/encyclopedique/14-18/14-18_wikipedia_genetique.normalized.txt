d' après le dictionnaire en ligne Littré , le mot génétique est un adjectif , c' est un terme didactique qui a rapport aux fonctions de génération .
il provient du grecque Genete , qui signifie engendrement .
on trouve également comme définition du mot génétique dans le dictionnaire en ligne Larousse en tant que nom féminin provenant du grecque genos race : la partie de la biologie qui étudie les lois de l' hérédité .
une de ses branches , la génétique formelle , ou mendélienne , s' intéresse à la transmission des caractères héréditaires entre des géniteurs et leur descendance .
l' invention du terme " génétique " revient au biologiste anglais William Bateson ( dix huit cent soixante et un dix neuf cent vingt six ) , qui l' utilise pour la première fois en dix neuf cent cinq .
la génétique moderne est souvent datée de la mise en évidence de la structure en double hélice de l' ADN effectuée par James Watson et Francis Crick en dix neuf cent cinquante trois .
très tôt , la génétique s' est diversifiée en plusieurs branches différentes :
la génétique du développement étudie les acteurs moléculaires ( et les gènes qui les codent ) impliqués dans la formation de l' organisme à partir du stade unicellulaire d' oeuf fécondé .
elle se focalise tout particulièrement sur la mise en place de la symétrie bilatérale et les mécanismes qui permettent de passer d' un système biologique simple ( unicellulaire , symétrie radiaire ) à un organisme complexe ( pluricellulaire , souvent métamérisé , et construit en organes spécialisés ) .
elle utilise souvent des espèces modèles pour étudier les mécanismes de formation de l' organisme ( la drosophile , le nématode Caenorhabditis elegans , le poisson zèbre , une plante du genre Arabidopsis ) ,
la génétique médicale étudie l' hérédité des maladies génétiques humaines , leur ségrégation dans les familles de malades .
elle cherche à identifier par ce biais les mutations responsables des maladies , afin de mettre au point des traitements pour les soigner ,
la génomique étudie la structure , la composition et l' évolution des génomes ( la totalité de l' ADN , trois milliards de paires de bases chez l' être humain , organisées en chromosomes ) , et tente d' identifier des motifs dans l' ADN pouvant avoir un sens biologique ( gènes , unités transcrites non traduites , miRNAs , unités de régulations , promoteurs , CNGs , et caetera ) ,
la génétique quantitative étudie la composante génétique expliquant la variation de caractères quantitatifs ( la taille , la couleur du pelage , la vitesse de croissance , la concentration d' une molécule , et caetera ) et leur héritabilité ,
la génétique de l' évolution étudie les signatures de la sélection naturelle sur le génome des espèces , et tente d' identifier les gènes qui ont joué un rôle essentiel dans l' adaptation et la survie des espèces dans des environnements changeants ,
la génétique des populations étudie les forces ( et leurs effets ) qui influencent la diversité génétique des populations et des espèces ( mutation , dérive , sélection ) par ( entre autres ) le développement de modèles mathématiques et statistiques .
la génétique chronologique étudie l' âge de la séparation des espèces en se fiant à la différence génétique entre elles et à la vitesse d' augmentation de la différence génétique , calibrée par d' autre méthode de chronologie , du groupe d' espèces dont elles font partie .
l' hérédité , qui étudie le phénotype et tente de déterminer le génotype sous jacent se fonde toujours sur les lois de Mendel .
la biologie cellulaire et la biologie moléculaire étudient les gènes et leur support matériel ( ADN ou ARN ) au sein de la cellule , la biologie cellulaire pour leur expression .
les progrès de la branche ingénierie de la génétique , le génie génétique , ont permis de passer le stade de la simple étude en réussissant à modifier le génome , à implanter , supprimer ou modifier de nouveaux gènes dans des organismes vivants : il s' agit des organismes génétiquement modifiés ( OGM ) .
les mêmes progrès ont ouvert une nouvelle voie d' approche thérapeutique : la " thérapie génique " .
il s' agit d' introduire de nouveaux gènes dans l' organisme afin de pallier une déficience héréditaire .
l' évolution sans cesse croissante de la connaissance en génétique pose plusieurs problèmes éthiques liés au clonage , aux divers types d' eugénismes possibles , à la propriété intellectuelle de gènes et aux possibles risques environnementaux dus aux OGM .
la compréhension du fonctionnement de la machinerie cellulaire est ainsi rendue plus complexe : en effet , plus on l' étudie , plus les acteurs sont nombreux ( ADN , ARN messager , de transfert , microARN , et caetera ) et le nombre de rétroactions ( épissage , édition , et caetera ) entre ces acteurs grandit .
en mille huit cent soixante deux , Charles Naudin est primé par l' Académie des sciences pour son Mémoire sur les hybrides du règne végétal .
en mille huit cent soixante cinq , passionné de sciences naturelles , le moine autrichien Gregor Mendel , dans le jardin de la cour de son monastère , décide de travailler sur des pois comestibles présentant sept caractères ( forme et couleur de la graine , couleur de l' enveloppe , et caetera ) , dont chacun peut se retrouver sous deux formes différentes .
à partir de ses expériences , il publie , en mille huit cent soixante six sous l' autorité de la Société des sciences naturelles de Bruenn , un article où il énonce les lois de transmission de certains caractères héréditaires .
cet article , " recherche sur les hybrides végétaux " , est envoyé aux scientifiques des quatre coins du monde : les réactions sont mitigées , voire inexistantes .
ce n' est qu' en mille neuf cent sept que son article fut reconnu et traduit en français .
en mille huit cent soixante neuf l' ADN est isolé par Friedrich Miescher , un médecin suisse .
il récupère les bandages ayant servi à soigner des plaies infectées et il isole une substance riche en phosphore dans le pus .
il nomme cette substance nucléine .
il trouve la nucléine dans toutes les cellules et dans le sperme de saumon .
en mille huit cent soixante dix neuf , Walther Flemming décrit pour la première fois une mitose .
la mitose avait déjà été décrite quarante ans avant par Carl Nageli mais celui ci avait interprété la mitose comme une anomalie .
Walter Flemming invente les termes prophase , métaphase , et anaphase pour décrire la division cellulaire .
son travail est publié en mille huit cent quatre vingt deux .
en dix huit cent quatre vingts , Oskar Hertwig et Eduard Strasburger découvrent que la fusion du noyau de l' ovule et du spermatozoïde est l' élément essentiel de la fécondation .
en dix huit cent quatre vingt onze , Theodor Boveri démontre et affirme que les chromosomes sont indispensables à la vie .
en dix neuf cent zéro , redécouverte des lois de l' hérédité : Hugo de Vries , Carl Correns et Erich von Tschermak Seysenegg redécouvrent de façon indépendante les lois de Mendel .
en dix neuf cent deux , Walter Sutton observe pour la première fois une méiose , propose la théorie chromosomique de l' hérédité , c' est à dire que les chromosomes seraient les supports des gènes .
il remarque que le modèle de séparation des chromosomes supporte tout à fait la théorie de Mendel .
il publie son travail la même année .
sa théorie sera démontrée par les travaux de Thomas Morgan .
première description d' une maladie humaine héréditaire par Archibald Garrod : l' alcaptonurie .
en mille neuf cent neuf , Wilhelm Johannsen crée le terme gène et fait la différence entre l' aspect d' un être ( phénotype ) et son gène ( génotype ) .
William Bateson , quatre ans avant , utilisait le terme génétique dans un article et la nécessité de nommer les variations héréditaires .
en mille neuf cent onze , Thomas Morgan démontre l' existence de mutations en conduisant des expériences sur des drosophiles mutantes aux yeux blancs ( mouches du vinaigre ) .
il montre que les chromosomes sont les supports des gènes , grâce à la découverte des liaisons génétiques ( genetic linkage ) et des recombinaisons génétiques .
il travaille avec Alfred Sturtevant , Hermann Muller , et Calvin Bridges .
il reçoit le prix Nobel de Médecine en mille neuf cent trente trois .
ses expériences permettront de consolider la théorie chromosomique de l' hérédité .
en dix neuf cent treize , Morgan et Alfred Sturtevant publient la première carte génétique du chromosome X de la drosophile , montrant l' ordre et la succession des gènes le long du chromosome .
en dix neuf cent vingt huit , Fred Griffith découvre la transformation génétique des bactéries , grâce à des expériences sur le pneumocoque .
la transformation permet un transfert d' information génétique entre deux cellules .
il ne connait pas la nature de ce principe transformant .
en dix neuf cent quarante et un , George Beadle et Edward Tatum émettent l' hypothèse qu' un gène code une ( et uniquement une ) enzyme en étudiant Neurospora crassa .
en mille neuf cent quarante trois , la diffraction au rayon X de l' ADN par William Astbury permet d' émettre la première hypothèse concernant la structure de la molécule : une structure régulière et périodique qu' il décrit comme une pile de pièces de monnaie ( like a pile of pennies ) .
en mille neuf cent quarante quatre , Oswald Avery , Colin MacLeod , et Maclyn McCarty démontrent que l' ADN est une molécule associée à une information héréditaire et peut transformer une cellule .
Barbara McClintock montre que les gènes peuvent se déplacer et que le génome est beaucoup moins statique que prévu .
elle reçoit le prix Nobel de Médecine en mille neuf cent quatre vingt trois .
en mille neuf cent cinquante , la structure chimique de l' ADN a été définie par Phoebus Levene ( post mortem ) et Alexander Robert Todd .
en mille neuf cent cinquante deux , Alfred Hershey et Martha Chase découvrent que seul l' ADN d' un virus a besoin de pénétrer dans une cellule pour l' infecter .
leurs travaux renforcent considérablement l' hypothèse que les gènes sont faits d' ADN .
en mille neuf cent cinquante trois , simultanément aux travaux de recherche de Maurice Wilkins et Rosalind Franklin qui réalisèrent un cliché d' une molécule d' ADN , James Watson et Francis Crick présentent le modèle en double hélice de l' ADN , expliquant ainsi que l' information génétique puisse être portée par cette molécule .
Watson , Crick et Wilkins recevront en mille neuf cent soixante deux le prix Nobel de médecine pour cette découverte .
en dix neuf cent cinquante cinq , Joe Hin Tjio fait le premier compte exact des chromosomes humains : quarante six .
Arthur Kornberg découvre l' ADN polymérase , une enzyme permettant la réplication de l' ADN .
en mille neuf cent cinquante sept , le mécanisme de réplication de l' ADN est mis en évidence .
en mille neuf cent cinquante huit , le Professeur Raymond Turpin de l' hôpital Trousseau , Marthe Gautier et Jérôme Lejeune réalisent une étude des chromosomes d' un enfant dit " mongolien " et découvre l' existence d' un chromosome en trop sur la vingt et unième paire .
pour la première fois au monde est établi un lien entre un handicap mental et une anomalie chromosomique .
par la suite , Jérôme Lejeune et ses collaborateurs découvrent le mécanisme de bien d' autres maladies chromosomiques , ouvrant ainsi la voie à la cytogénétique et à la génétique moderne .
dans les années dix neuf cent soixante , François Jacob et Jacques Monod élucident le mécanisme de la biosynthèse des protéines .
introduisant la distinction entre " gènes structuraux " et " gènes régulateurs " , ils montrent que la régulation de cette synthèse fait appel à des protéines et mettent en évidence l' existence de séquences d' ADN non traduites mais jouant un rôle dans l' expression des gènes .
le principe de code génétique est admis .
en dix neuf cent soixante et un , François Jacob , Jacques Monod et André Lwoff avancent conjointement l' idée de programme génétique .
en dix neuf cent soixante deux , Crick , Watson et Wilkins reçoivent le prix Nobel de médecine pour avoir établi que les triplets de bases étaient des codes .
le comité Nobel évoquera " la plus grande réussite scientifique de notre siècle " .
en mille neuf cent soixante six , J L Hubby et Richard C Lewontin ouvrent la voie au domaine de la recherche sur l' évolution moléculaire en introduisant les techniques de la biologie moléculaire comme l' électrophorèse sur gel dans la recherche sur la génétique des populations .
mille neuf cent soixante huit : prix Nobel décerné pour le déchiffrage du code génétique .
mille neuf cent soixante quinze : autre prix Nobel pour la découverte du mécanisme de fonctionnement des virus .
la génomique devient dès lors l' objet d' intérêts économiques importants .
dans le même temps , la sociobiologie et la psychologie évolutionniste d' Edward O Wilson se fondent sur l' idéologie du déterminisme génétique que génère l' idée devenue fausse de programme génétique .
de la sorte , c' est à dire selon une conception évolutionniste ( linéaire et réductionniste ) générée par le néodarwinisme et le mythe du Graal de la génétique , ces deux domaines débordent sur la sphère sociale et politique .
c' est ainsi que , tout en apportant une conception scientifique selon une pensée " dialectique " , Stephen Jay Gould , Richard C Lewontin et quelques autres membres du groupe de Science for the People ont démarré la polémique encore en cours sur la sociobiologie et la psychologie évolutionniste .
en mille neuf cent quatre vingts , la Cour Suprême des États Unis admet pour la première fois au monde le principe de brevetabilité du vivant pour une bactérie génétiquement modifiée ( oil eating bacteria ) .
cette décision juridique est confirmée en mille neuf cent quatre vingt sept par l' Office américain des Brevets , qui reconnait la brevetabilité du vivant , à l' exception notable de l' être humain .
en mille neuf cent quatre vingt six , est réalisé le premier essai en champ de plante transgénique ( un tabac résistant à un antibiotique ) .
en mille neuf cent quatre vingt neuf , il est décidé de décoder les trois milliards de paires de bases du génome humain pour identifier les gènes afin de comprendre , dépister et prévenir les maladies génétiques et tenter de les soigner .
une première équipe se lance dans la course : le Human Génome Project , coordonné par le NIH ( National Institutes of Health ) et composé de dix huit pays dont la France avec le Génoscope d' Évry qui sera chargée de séquencer le chromosome quatorze .
dans les années dix neuf cent quatre vingt dix , à Évry , des méthodologies utilisant des robots sont mises au point pour gérer toute l' information issue de la génomique .
en dix neuf cent quatre vingt douze , l' Union européenne reconnait à son tour la brevetabilité du vivant et accorde un brevet pour la création d' une souris transgénique .
elle adopte en dix neuf cent quatre vingt dix huit la directive sur la brevetabilité des inventions biotechnologiques : sont désormais brevetables les inventions sur des végétaux et animaux , ainsi que les séquences de gènes .
en dix neuf cent quatre vingt dix huit , l' Europe adopte une Directive fondamentale relative à la protection des inventions biotechnologiques : sont désormais brevetables les inventions sur des végétaux et animaux , ainsi que les séquences de gènes .
dans le même temps les premiers Mouvement anti OGM se forment contre le lobby du " complexe génético industriel " dans le domaine de l' OGM .
les OGM , organismes génétiquement modifiés sont en réalité pour le généticien Richard C Lewontin et Jean Pierre Berlan des CCB , clones chimériques brevetés .
cela ouvre de nombreux débats politiques et médiatiques , divers et variés , sur l' OGM conduisant à des règlementations .
en dix neuf cent quatre vingt douze dix neuf cent quatre vingt seize , les premières cartes génétiques du génome humain sont publiées par J Weissenbach et D Cohen dans un laboratoire du Généthon .
en dix neuf cent quatre vingt dix huit , créée par Craig Venter et Perkin Elmer ( leader dans le domaine des séquenceurs automatiques ) , la société privée Celera Genomics commence elle aussi le séquençage du génome humain en utilisant une autre technique que celle utilisée par le NIH .
en dix neuf cent quatre vingt dix neuf , un premier chromosome humain , le vingt deux , est séquencé par une équipe coordonnée par le centre Sanger , au Royaume Uni .
en juin vingt cent zéro, le NIH et Celera Genomics annoncent chacun l' obtention de quatre vingt dix neuf pour cents de la séquence du génome humain .
les publications suivront en vingt cent un dans les journaux Nature pour le NIH et Science pour Celera Genomics .
en juillet vingt cent deux, des chercheurs japonais de l' Université de Tokyo ont introduit deux nouvelles bases , S et Y , aux quatre déjà existantes ( À , T , G , C ) sur une bactérie de type Escherichia coli , ils l' ont donc dotée d' un patrimoine génétique n' ayant rien de commun avec celui des autres êtres vivants et lui ont fait produire une protéine encore inconnue dans la nature .
certains n' hésitent pas à parler de nouvelle genèse , puisque d' aucuns y voient une nouvelle grammaire autorisant la création d' êtres vivants qui non seulement étaient inimaginables avant mais qui , surtout , n' auraient jamais pu voir le jour .
le quatorze avril vingt cent trois, la fin du séquençage du génome humain est annoncée .
les années vingt cent dix vont vers la fin du " tout gène " et du réductionnisme de la génétique moléculaire des quarante dernières années avec la découverte de phénomènes épigénétiques liés à l' influence de l' environnement sur le gène .
un gène est une unité d' information génétique , constitué par plusieurs nucléotides ( un nucléotide est constitué par un groupement phosphate , un sucre et une base azotée ) .
les gènes sont soit codant et leur information génétique est utilisée : un pour la biosynthèse des protéines deux lors de la formation d' un embryon , ou bien sont de l' ADN non codant et dont l' information génétique ne sera pas traduite directement en protéine mais assurera toute une série d' autres fonctions , comme l' activation et la désactivation de l' expression de certains gènes .
plus largement dans une définition prenant en compte les découvertes récentes , notamment sur les microARN , on peut dire qu' un gène est " l' ensemble des séquences d' ADN qui concourent à la production régulée d' un ou plusieurs ARN , ou d' une ou plusieurs protéines " .
l' information génétique est portée par l' acide désoxyribonucléique , ou ADN .
l' ADN est une macromolécule formée par l' enchaînement de nombreux nucléotides .
chaque nucléotide est formé d' un groupement phosphate , d' un glucide , le désoxyribose , et d' une base azotée .
il existe quatre bases azotées différentes donc quatre nucléotides différents dans l' ADN : l' adénine , la cytosine , la guanine et la thymine .
la molécule d' ADN est formée de deux chaines de nucléotides enroulées en double hélice .
les nucléotides sont complémentaires deux à deux : en face d' une cytosine se trouve toujours une guanine , en face d' une adénine se trouve toujours une thymine .
c' est la séquence , c' est à dire l' ordre et le nombre des nucléotides d' un gène , qui porte l' information génétique .
l' ADN sert de support pour la synthèse des protéines .
l' information génétique portée par l' ADN est " reportée " dans une molécule d' ARNm ( acide ribonucléique " messager " ) lors de la transcription , puis l' ARNm sert de support pour la synthèse d' une protéine lors de la traduction .
chaque triplet de nucléotide ( ou codon ) de l' ARNm " code " un acide aminé ( cela signifie que chaque triplet " appelle " un acide aminé précis ) , selon la correspondance établie par le code génétique .
ainsi la séquence en acides aminés de la protéine dépend directement de la séquence en nucléotides de l' ADN .
or les protéines forment le phénotype moléculaire de la cellule ou de l' individu .
le phénotype moléculaire conditionne le phénotype cellulaire et finalement le phénotype de l' organisme .
tous les organismes vivants : animaux , végétaux , sont constitués de cellules .
ainsi , un être humain est composé de , selon les auteurs , cinquante milliards à cent milliards de cellules .
toutes les cellules d' un être vivant proviennent de la même cellule initiale qui s' est divisée un très grand nombre de fois , au cours de l' embryogenèse puis du développement foetal .
au cours d' un cycle cellulaire ( succession des étapes de la vie de la cellule ) , la cellule réplique son ADN , c' est à dire que toute l' information génétique est dupliquée à l' identique : elle se retrouve avec deux " copies " complètes de son information génétique , ses chromosomes sont constitués de deux chromatides identiques .
lors de la division cellulaire , ou mitose , les deux chromatides de chaque chromosome se séparent pour former deux lots identiques de chromosomes ( à une seule chromatide ) .
chaque cellule fille reçoit un de ces lots .
ainsi , au terme d' une mitose , les deux cellules filles issues de la cellule mère possèdent exactement le même patrimoine génétique : elles sont des copies conformes l' une de l' autre .
les débuts de la génétique ont été influencés par deux idéologies dominantes et hégémoniques opposées et exacerbées dans les années mille neuf cent trente :
dans les pays occidentaux , la plupart des généticiens ont adhéré à l' eugénisme .
l' eugénisme est une invention du néoconservateur Francis Galton à la fin du XIXe siècle sur la base d' une sélection artificielle des individus .
cette idéologie fut amalgamée à l' idée spencériste .
ce dernier se fonde sur la sélection naturelle en société par analogie à la sélection naturelle existante dans la nature .
avec le malthusianisme , elles sont , selon le point de vue de la bourgeoisie , en adéquation avec la société capitaliste du XIXe siècle .
ces pratiques sont , par la suite , soutenues par la fondation Rockefeller dès mille neuf cent douze et l' UNESCO par son premier président Julian Huxley en mille neuf cent quarante cinq ,
dans le bloc soviétique jusque dans les années mille neuf cent soixante , la génétique a été interdite par l' intervention du transformiste Trofim Denissovitch Lyssenko , qui s' oppose à la génétique " mendelo morgannienne " .
Staline va placer sa confiance dans Lyssenko dans les années de troubles des années mille neuf cent trente , parce que les principes transformistes de Lyssenko sont en adéquation avec le lamarkisme social de la société soviétique .
cette confiance a conduit Lyssenko à faire faire condamner Nikolai Vavilov et ses collaborateurs darwino mendéliens qui sont ainsi envoyés au goulag en mille neuf cent trente neuf .
mais , c' est surtout pendant l' ère post stalinienne , la période Nikita Khrouchtchev que Lyssenko aura plus de soutiens .
ce soutien vient non de la part de scientifiques , comme le biologiste Jacques Monod proche du PCF ou encore le généticien , marxiste et communiste , JBS .
Haldane , mais de militants non scientifique comme le philosophe , communiste Jean Paul Sartre .
dans les années mille neuf cent cinquante , Staline , quant à lui , voit un non sens dans la terminologie de " science prolétarienne " inventée par Lyssenko qui veut l' opposer à une science bourgeoise .
Lyssenko est limogé en mille neuf cent soixante cinq après la chute de son protecteur Nikita Khrouchtchev .
ces oppositions idéologiques s' ouvrent sur la question philosophique de l' inné ou de l' acquis de l' acquisition des connaissances des individus et du développement de la culture humaine bien qu' elle fût résolue scientifiquement par Charles Darwin dès dix huit cent soixante et onze dans le passé inaperçu La Filiation de l' homme .
il n' y a pas d' opposition entre l' inné ( génétique ) et l' acquis ( l' environnement ) .
cependant , pour les néo darwinistes ou sociobiologistes , la question se pose encore sur le comportement de l' homme .
mais , pour le généticien Richard C Lewontin , " il n' y a pas de " part " respective des gènes et de l' environnement , pas plus qu' il n' y a de " part " de la longueur et de la largeur dans la surface d' un rectangle , pour reprendre une métaphore classique . l' exposition à l' environnement commence d' ailleurs dans le ventre maternel , et inclut des évènements biologiques comme la qualité de l' alimentation ou l' exposition aux virus . génétique et milieu ne sont pas en compétition , mais en constante interaction : on dit qu' ils sont covariants . le comportement d' un individu serait donc à la fois cent pour cents génétique et cent pour cents environnemental " .
lors de l' ouverture de la quête du Graal que fut le Projet génome humain pour les généticiens le laboratoire Celera Genomics dirigée par Craig Venter conduit une course contre le consortium international public pour obtenir le premier des séquences génétiques dans le but de les breveter et de les vendre aux sociétés pharmaceutiques .
une étude de deux mille cinq révèle que vingt pour cents des gènes humains font l' objet d' un brevet : soixante trois pour cents de ces brevets appartiennent à des firmes privées , vingt huit pour cents à des universités .
le brevetage d' une partie des gènes constitue donc un frein à la découverte de leur fonction .
par exemple la compagnie Myriad dépose un brevet sur l' utilisation des gènes BRCA un et BRCA deux séquencé en mille neuf cent quatre vingt quatorze mille neuf cent quatre vingt quinze comme indicateurs de risques pour le cancer du sein et de l' ovaire , maladie dont les gènes ont été associés .
le test coute d' abord un six cents dollars US et son prix est passé à trois deux cents dollars en deux mille neuf .
ce brevet représente l' essentiel des revenus annuels de la compagnie .
ainsi , le brevet qui donne le droit de propriété exclusif sur la séquence , empêche complètement d' autres compagnies de développer des tests alternatifs utilisant ces mêmes gènes .
cependant en vingt cent dix , ce brevet est annulé car le caractère inventif du test est contesté par le bureau européen des brevets .
en effet , le séquençage ne constitue pas une invention , mais une découverte .
la méthode se limite à comparer une séquence de l' échantillon à une séquence de référence , et n' est pas brevetable en soi ( Directive sur la brevetabilité des inventions biotechnologiques ) .
ainsi , pour une plus grande liberté de la recherche alternative la cour a donné son verdict : " les produits naturels et propriétés naturelles des objets vivants sont légalement distincts d' objets manufacturés en usant d' une ingéniosité substantielle . "