médaillon central restauré d' une mosaïque romaine découverte à
son histoire est contée dans l' Iliade par Glaucos , fils
d' Hippoloque , à qui Diomède a demandé sa lignée .
donnèrent la beauté et la vigueur charmante .
mais Proétos , qui était
le plus puissant des Argiens , car Zeus les avait soumis à son
sceptre , eut contre lui de mauvaises pensées et le chassa de son
peuple .
car la femme de Proétos , la divine Antéia , désira ardemment
s' unir au fils de Glaucos par un amour secret , mais elle ne persuada
point le sage et prudent Bellérophon et , pleine de mensonge , elle
parla ainsi au roi Proétos : meurs , Proétos , ou tue Bellérophon qui ,
elle parla ainsi , et , à ces paroles , la colère saisit le Roi .
et il ne
tua point Bellérophon , redoutant pieusement ce meurtre dans son
esprit , mais il l' envoya en Lycie avec des tablettes où il avait
tracé des signes de mort , afin qu' il les remît à son beau père et que
celui ci le tuât .
et Bellérophon alla en Lycie sous les heureux
auspices des Dieux .
sur la route il dompta le Pégase , roi des
chevaux .
et quand il y fut arrivé , sur les bords du rapide Xanthos ,
le roi de la grande Lycie le reçut avec honneur , lui fut
hospitalier pendant neuf jours et sacrifia neuf boeufs .
mais quand Éos
aux doigts rosés reparut pour la dixième fois , alors il l' interrogea et
demanda à voir les signes envoyés par son gendre Proétos .
et , quand il
les eut vus , il lui ordonna d' abord de tuer l' indomptable Chimère .
celle ci était née des Dieux et non des hommes , lion par devant , dragon
par l' arrière , et chèvre par le milieu du corps .
et elle soufflait des
flammes violentes .
mais il la tua , s' étant fié aux prodiges des Dieux .
puis , il combattit les Solymes illustres , et il disait avoir
entrepris là le plus rude combat des guerriers .
enfin il tua les
amazones viriles .
comme il revenait , le Roi lui tendit un piège rusé ,
ayant choisi et placé en embuscade les plus braves guerriers de la
grande Lycie .
mais nul d' entre eux ne revit sa demeure , car
l' irréprochable Bellérophon les tua tous .
et le Roi connut alors que
cet homme était de la race illustre d' un Dieu , et il le retint et lui
donna sa fille et la moitié de sa domination royale .
et les Lyciens lui
choisirent un domaine , le meilleur de tous , plein d' arbres et de
champs , afin qu' il le cultivât .
et sa femme donna trois enfants au
brave Bellérophon : Isandros , Hippolochos et Laodamie .
et
le sage Zeus s' unit à Laodamie , et elle enfanta le divin
Sarpédon couvert d' airain .
mais quand Bellérophon fut en haine aux
dieux , il errait seul dans le désert d' Aléios : " objet de
haine pour les dieux , Il errait seul dans la plaine d' Alcion , Le coeur
dévoré de chagrins , évitant les traces des hommes " .
les Olympiques de Pindare , composées dans le cadre des jeux
ses efforts furent inutiles jusqu' au moment où la chaste Pallas
lui apporta un frein enrichi de rênes d' or .
réveillé en
sursaut d' un sommeil profond , il la voit apparaître à ses yeux et
l' entend prononcer ces paroles : " tu dors , roi , descendant d' Éole !
prends ce philtre , seul capable de rendre les coursiers dociles ,
après l' avoir offert à Poséidon , ton père , immole un superbe
taureau à ce dieu si habile à dompter les coursiers " .
la déesse à
la noire égide ne lui en dit pas davantage au milieu du silence de
la nuit .
Bellérophon se lève aussitôt , et , saisissant le frein
merveilleux , le porte au fils de Coeramus , le devin de ces contrées .
il lui raconte la vision qu' il a eue , comment , docile à ses oracles ,
il s' est endormi pendant la nuit sur l' autel de la déesse , et
comment cette fille du dieu , à qui la foudre sert de lance lui a
donné elle même ce frein d' or sous lequel doit plier Pégase .
le
devin lui ordonne d' obéir sans retard à ce songe et d' élever un
autel à Minerve Équestre après avoir immolé un taureau au
dieu , qui de ses ondes environne la terre .
c' est ainsi que la
puissance des dieux rend facile ce que les mortels jureraient être
impossible et désespèreraient même d' exécuter jamais .
tressaillant
d' allégresse , l' intrépide Bellérophon saisit le cheval ailé : tel
qu' un breuvage calmant , le frein dont il presse sa bouche modère sa
fougue impétueuse , alors , s' élançant sur son dos , Bellérophon ,
revêtu de ses armes , le dresse au combat en se jouant .
bientôt ,
transporté avec lui dans le vide des airs sous un ciel glacé , il
accable de ses traits les Amazones , habiles à tirer de l' arc ,
tue la Chimère qui vomissait des flammes et défait les
Solymes .
je ne parlerai point de la mort de Bellérophon : je
dirai seulement que Pégase fut reçu dans les étables de l' immortel
olympiques , treize , traduction de Monsieur Al .
perrault maynand
Bellérophon naquit à Ephyre sous le nom d' Hipponoos .
il était
officiellement le fils de Glaucos et le petit fils de Sisyphe ,
mais une rumeur faisait de lui le fils du dieu de la mer Poséidon .
il fut rebaptisé " Bellérophon " après avoir tué involontairement son
frère Déliadès lors d' un lancer de disque , ou , selon d' autres
récits par une flèche dans le dos ( il visait une biche ) , avoir tué
involontairement un noble corinthien tyran de son état , nommé
Belléros .
il dut s' expatrier et fuir à Tirynthe pour
que le roi Proétos le purifie de son crime .
mais la femme de ce
dernier , Sthénébée , s' éprit du jeune homme .
Bellérophon était très
timide avec les femmes et la repoussa .
elle l' accusa faussement devant
le roi d' avoir tenté de la séduire .
Proétos décida de tuer le jeune
homme .
ne pouvant mettre à mort son hôte lui même sans s' attirer le
courroux des Érinyes , il l' envoya à la cour de son beau père
Iobatès , le roi de Lycie et père de Sthénébée , avec une
tablette scellée sur laquelle figurait un message ordonnant de tuer le
Iobatès fit grand accueil à Bellérophon et le laissa manger et boire à
sa table une semaine durant avant de lire le message .
il lui demanda
alors d' éliminer la Chimère , un monstre qui causait de grands
ravages dans son pays , persuadé que le jeune homme y trouverait la
désemparé , Bellérophon consulta un devin , Polyidos , qui lui
conseilla de sacrifier un taureau à Poséidon en le noyant et de
passer une nuit dans le temple d' Athéna , ce qu' il fit .
la déesse
apparut dans ses rêves pour lui parler de Pégase , seule créature
assez rapide pour lui permettre d' échapper aux flammes de la Chimère .
elle lui remit une bride d' or et lui dit où trouver le coursier ailé .

son réveil , Bellérophon trouva l' objet bien réel à côté de lui .
il
réussit à apprivoiser Pégase près de la fontaine de Pirène où le
Bellérophon vint à bout de la Chimère : selon une version
du mythe en volant au dessus d' elle , il la cribla de flèches , selon
une autre version il utilisa une lance garnie de plomb , que le souffle
ardent de la créature fit fondre et qui lui brula les entrailles .
Iobatès , loin de le récompenser , l' envoya combattre les belliqueux
Solymes , peuple montagnard de Lycie .
lorsque le guerrier et
sa monture revinrent , le roi les renvoya affronter les Amazones ,
alliées des Solymes .
quand Bellérophon revint pour la troisième fois à
la cour de Lycie , Iobatès posta secrètement des combattants en
embuscade et demanda à Bellérophon de contacter un certain Acrisios , de
nuit et sans armes .
Bellérophon triompha une fois de plus .
comme
Iobatès avait envoyé sa garde royale contre lui , Bellérophon mit pied à
terre et demanda à Poséidon d' inonder la plaine à mesure qu' il
avançait .
les hommes n' ayant pas réussi à l' arrêter , les femmes de la
région relevèrent leur tunique par dessus leur tête et marchèrent vers
lui .
Bellérophon était si pudique qu' il fit demi tour , entrainant les
Iobatès , impressionné après de tels exploits , fut convaincu de
l' innocence de son invité et renonça à le mettre à mort .
il lui donna
sa fille Philonoé en mariage ainsi que la moitié de son royaume en
succession .
Bellérophon eut plusieurs enfants : Isandros ,
Hippoloque et Laodamie , la mère du héros Sarpédon .
peu à
peu , Bellérophon devint victime de son orgueil .
pour se venger de la
reine Sthénébée ( ou Antéia ) , il revint à Argos et fit
semblant de succomber à ses charmes .
il lui proposa un petit voyage
aérien sur le dos de Pégase et quand il fut assez haut , il la
précipita dans les flots .
au sommet de sa gloire , il entreprit de voler
vers l' Olympe grâce à Pégase , s' estimant digne de séjourner
avec les dieux .
mais Zeus , furieux , envoya un taon qui piqua
pégase sous la queue .
Bellérophon tomba dans un buisson d' épines ,
devint aveugle et erra sur la terre jusqu'à sa mort après avoir vu son
fils Isandros tué par les Solymes , et sa fille Laodamie , qui meurt
par la volonté d' Artémis , de maladie soudaine et
selon le quatrième Livre de l' Histoire d' Héraclée par l' historien
Nymphis , Bellérophon tua dans les campagnes de Xanthos
un sanglier qui ravageait les cultures et élevages de la région .
sans
aucune reconnaissance de la part des habitants , Bellérophon les maudit ,
et obtint de Poséidon qu' il sortît du sein de la terre des
exhalaisons salées dont l' amertume corrompait tous les fruits .
le fléau
ne cessa que lorsque les femmes vinrent lui demander grâce .
cette section est vide , insuffisamment détaillée ou incomplète .
les habitants de Corinthe lui rendaient un culte héroïque .
selon les Histoires incroyables de Palaiphatos , Bellérophon
dans une nef rapide , et Pégase était le nom de son navire .
Amisodaros ,
un roi voisin du fleuve Xanthos et de la forêt , habitait entre des
escarpements sur une montagne du nom de Chimère , le long d' une route et
une cité le long d' une autre route .
d' un côté se trouve un vaste ravin ,
où de la terre jaillissent des flammes .
un lion vivait près de l' accès
principal , et un serpent non loin de là , dévorant les bucherons et les
bergers .
Bellérophon mit le feu à la forêt de Telmissa , qui brula , les
jean Haudry voit dans le récit de Bellérophon et de son châtiment
un ancien mythe lunaire répandu dans le domaine indo européen , où Lune ,
les récits ) .
Antée se plaint à Proétos , mais le châtiment n' est
pas suivi d' effets .
comme Yama , Bellérophon est puni une seconde
fois , et " il est réduit à errer solitaire comme la Lune dans la
daina " , comme Yima privé de son pouvoir et de ses trois
une mosaïque de pavement du II E siècle fut découverte à
Reims en dix neuf cent trente huit lors de travaux dans la rue Jadart .
au centre ,
Bellérophon chevauche Pégase et terrasse la Chimère
représentée ici comme un monstre bicéphale crachant des flammes .
le
reste de la mosaïque est composée de losanges et de triangles encadrées
de tresses dans une grande variété de coloris , .
la légende a inspiré la tragédie lyrique homonyme de
jean de La Fontaine fait référence également à ce personnage
mythologique dans L' Ours et l' Amateur des jardins , fable dix du
un rapprochement peut aisément être effectué avec La Tempête de
Shakespeare où une vengeance n' empêche pas un roi de confier sa
dans le film Planète interdite de Fred McLeod Wilcox , sorti
en dix neuf cent cinquante six , le vaisseau des scientifiques naufragés porte le nom de
Bellérophon , préfigurant ainsi sous forme d' indice l' attaque de la
chimère née des cauchemars du seul scientifique survivant .
dans le film Mission Impossible second John Woo , sorti en vingt cent zéro ,
l' équipe est aux prises avec une organisation ayant créé un virus
exterminateur , la " chimère " , dont le seul remède connu s' appelle le
la planète extrasolaire cinquante et un Pegasi B est également surnommée
tradition d' attribuer un nom issu de la mythologie aux planètes de
Bellérophon est le nom du navire britannique sur lequel
napoléon embarqua après Waterloo en se plaçant sous la
bienveillance de son plus constant ennemi , le Royaume Uni .
c' est
sur ce navire qu' en mille huit cent quinze , il quitta définitivement le territoire
l' ouvrage de Federico Grisone , fondateur de l' école d' équitation
napolitaine , est publié la première fois en français sous le titre
guillaume Auvray , rue Jean de Beauvais , et parait chez le libraire
Charles Périer à l' enseigne du Bellérophon couronné .
ce dernier est
alors l' un des quatre grands libraires jurés de la ville et de
l' université de Paris .
il avait repris une affaire qui appartenait aux
Wechel , dynastie de libraires originaires de Bâle et dont
l' officine se trouva libre après le départ du fils André qui acheta en
mille cinq cent soixante le fonds de l' imprimerie d' Henri Estienne .
la marque utilisée
par les Wechel sur leurs pages de titre représentait deux mains
soutenant un double caducée que surmonte Pégase .
le Belléphoron de
la marque d' imprimeur des Périer est chevauché par ce même Pégase .
tous
les textes équestres produits sous l' enseigne du Belléphoron utilisent
du matériel typographique des éditions Wecheliennes , des lettrines en
bois taillées d' après Holbein et trois xylographies hippiques