Une heure après sa naissance, le petit veau tient debout et peut aller téter le lait de sa maman. e boit que du lait pendant plusieurs semaines.
Le veau mâle devient un taureau fort et trapu vers l'âge de 18 mois. Mais ce n'est pas lui le chef, car c'est une vache qui dirige ce troupeau. Eh oui !
Lorsqu'ils grandissent, les veaux aiment rester entre eux pour jouer. Amusez-vous bien dans le pré !
Vers 4 mois, il commence à manger de l'herbe, mais continue à téter sa maman et à lui faire des câlins.

