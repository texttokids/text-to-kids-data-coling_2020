
Youssef, en ce moment, il doit se faire interroger dans un bureau gris qui sent le renfermé. Moi, je sais que Youssef, c'est un mec gentil. C'est pas juste. Quand Maman a raccroché, on a un peu parlé mais des fois même les mots, ils suffisent pas. Juste, on regardait par la fenêtre et ça voulait tout dire. Dehors, il faisait gris comme la couleur du béton des immeubles et il pleuvait à très fines gouttes, comme si Dieu nous crachait dessus.

Ça fait déjà plusieurs nuits que je fais le même rêve, un de ces rêves chelous dont on se souvient parfaitement au réveil et qu'on est capable de raconter au détail près.

J'ouvrais la fenêtre et j'avais le soleil qui me tapait fort dans le visage. J'arrivais même plus à ouvrir les yeux. J'ai passé mes jambes par-dessus la fenêtre jusqu'à me retrouver assise sur le rebord, puis, d'un élan, je me suis envolée. J'allais de plus en plus haut, je voyais les HLM qui s'éloignaient et devenaient de plus en plus petits. Je battais des ailes, enfin des bras, et puis à force de les secouer pour continuer à monter, je me suis réellement cognée au mur à ma droite et ça m'a fait un énorme bleu. C'est ce qui m'a réveillée et je dois dire que c'était plutôt dur de revenir à la réalité de cette façon.

J'ai raconté mon rêve à Mme Burlaud. Elle m'a regardée en clignant des yeux et elle a dit : 
- Oui, bien sûr, évidemment… Ça rejoint aussi l'épisode de l'atlas…

Ah bon. Elle appelle carrément ça un épisode. Si ça se trouve Mme Burlaud, elle est pas vraiment psy. Elle travaille peut-être dans le cinéma et s'inspire des foutaises que je lui raconte pour écrire un sitcom. Burlaud, je suis certaine que c'est un pseudo, son vrai nom, ça doit être un truc du style Laurence Bouchard. Elle fait partie de l'équipe de scénaristes qui bosse pour AB Productions. C'est ça la vérité… Le concept a peut-être déjà été lancé et la série, elle cartonnerait et commencerait à être diffusée dans le monde entier. Elle serait même doublée en japonais. Et puis moi, je toucherais aucuns droits dessus, je ferais seulement partie des millions de fans, anonyme et couillonnée, comme tous les autres.

L'épisode de l'atlas, je sais même pas pourquoi je lui ai raconté. Je sais pas non plus pourquoi je lui raconte tout le reste d'ailleurs… C'était un jour où je m'ennuyais vraiment comme un rat mort. Je suis allée au débarras récupérer l'atlas que j'avais eu en livre de prix à la fin d'année de CM

Un débarras, c'est comme un grenier, mais en plus petit, situé en général dans le couloir. Ça sert à entreposer des conneries dont on se sert jamais.

Bref, j'ai ouvert mon atlas au planisphère, là où le monde tient en une seule page. Et comme je galérais pas mal, j'ai tracé un itinéraire sur la carte pour partir. C'était le chemin que j'allais faire plus tard, en passant par les endroits les plus beaux du monde. Bon, j'ai dessiné le chemin au crayon de papier parce que Maman m'aurait engueulée si elle avait vu que je gribouillais au stylo sur un livre neuf. En tout cas, je me suis tracé un pur itinéraire, même si je suis encore au point de départ et que le point de départ c'est Livry-Gargan.

De toute façon, je sais pas si Maman serait d'accord pour que je me casse. Il n'y aurait personne pour lui enregistrer Les Feux de l'amour . Et puis personne non plus pour aller chercher Sarah au centre, et Lila, elle serait emmerdée pour trouver une autre baby-sitter. Ça me rappelle que quand même, y a des gens qu'ont besoin de moi, et ça fait du bien.

Parce que des fois, j'aimerais trop être quelqu'un d'autre, ailleurs et peut-être même à une autre époque. Souvent, je m'imagine que je fais partie de la famille Ingalls dans La Petite Maison dans la prairie 

J'explique le plan : 

Le papa, la maman, les enfants, le chien qui mord pas, la grange et les rubans dans les cheveux pour aller à l'église le dimanche matin. Le bonheur quoi… L'histoire, elle se passe dans des ambiances, genre avant 1900, avec la lampe à pétrole, l'arrivée du chemin de fer, des vêtements préhistoriques et d'autres trucs vieux comme ça… Ce que j'aime bien chez eux, c'est que dès qu'il arrive un drame, ils font le signe de croix, pleurent un bon petit coup, et à l'épisode d'après on a tout oublié… C'est du pur cinéma.

C'est la honte parce que je trouve que dans cette série, ils sont mieux habillés que moi. Alors qu'ils habitent un microvillage tout pourri et que leur père c'est un gros fermier. Rien que le sweat que je porte en ce moment, même l'abbé Pierre il en voudrait pas. Une fois, j'ai mis un pull mauve avec des étoiles et un truc en anglais écrit dessus. Ma mère, elle l'avait acheté dans une friperie qui pue le vieux. Elle avait réussi à l'avoir pour un euro. Elle en était toute fière. Comme je voulais pas la vexer, je l'ai porté au lycée mais, je sais pas, j'avais un mauvais pressentiment, je le trouvais suspect ce pull. Il l'était. Les poufiasses du lycée, la bande de décolorées, surmaquillées avec leurs soutiens-gorge rembourrés et leurs chaussures compensées, elles se sont bien foutues de ma gueule. Le truc écrit en anglais sur le pull, c'était « sweet dreams  ». Ça veut dire « fais de beaux rêves ». Cette saloperie de pull mauve, c'était un haut de pyjama. Je savais que j'aurais dû être plus attentive pendant les cours de miss Baker en sixième.
