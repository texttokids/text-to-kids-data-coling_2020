
Schmucke avait repris la main de Pons et la tenait avec joie, en croyant la santé revenue.
- Allons-nous-en, monsieur l'abbé, dit le docteur, je vais envoyer promptement madame Cantinet ; je m'y connais : elle ne trouvera peut-être pas monsieur Pons vivant.

Pendant que l'abbé Duplanty déterminait le moribond à prendre pour garde madame Cantinet, Fraisier avait fait venir chez lui la loueuse de chaises, et la soumettait à sa conversation corruptrice, aux ruses de sa puissance chicanière, à laquelle il était difficile de résister. Aussi madame Cantinet, femme sèche et jeune, à grandes dents, à lèvres froides, hébétée par le malheur, comme beaucoup de femmes du peuple, et arrivée à voir le bonheur dans les plus légers profits journaliers, eut-elle bientôt consenti à prendre avec elle madame Sauvage comme femme de ménage. La bonne de Fraisier avait déjà reçu le mot d'ordre. Elle avait promis de tramer une toile en fil de fer autour des deux musiciens, et de veiller sur eux comme l'araignée veille sur une mouche prise. Madame Sauvage devait avoir pour loyer de ses peines un débit de tabac : Fraisier trouvait ainsi le moyen de se débarrasser de sa prétendue nourrice, et mettait auprès de madame Cantinet un espion et un gendarme dans la personne de la Sauvage. Comme il dépendait de l'appartement des deux amis une chambre de domestique et une petite cuisine, la Sauvage pouvait coucher sur un lit de sangle et faire la cuisine de Schmucke. Au moment où les femmes se présentèrent, amenées par le docteur Poulain, Pons venait de rendre le dernier soupir, sans que Schmucke s'en fût aperçu. L'Allemand tenait encore dans ses mains la main de son ami, dont la chaleur s'en allait par degrés. Il fit signe à madame Cantinet de ne pas parler ; mais la soldatesque madame Sauvage le surprit tellement par sa tournure, qu'il laissa échapper un mouvement de frayeur, à laquelle cette femme mâle était habituée.
- Madame, dit madame Cantinet, est une dame de qui répond monsieur Duplanty ; elle a été cuisinière chez un évêque, elle est la probité même, elle fera la cuisine.
- Ah ! vous pouvez parler haut !  s'écria la puissante et asthmatique Sauvage, le pauvre monsieur est mort !... il vient de passer. Schmucke jeta un cri perçant, il sentit la main de Pons glacée qui se raidissait, et il resta les yeux fixes, arrêtés sur ceux de Pons, dont l'expression l'eût rendu fou, sans madame Sauvage, qui, sans doute accoutumée à ces sortes de scènes, alla vers le lit en tenant un miroir, elle le présenta devant les lèvres du mort, et comme aucune respiration ne vint ternir la glace, elle sépara vivement la main de Schmucke de la main du mort.
- Quittez-la donc, monsieur, vous ne pourriez plus l'ôter ; vous ne savez pas comme les os vont se durcir !  Ça va vite le refroidissement des morts. Si l'on n'apprête pas un mort pendant qu'il est encore tiède, il faut plus tard lui casser les membres...

Ce fut donc cette terrible femme qui ferma les yeux au pauvre musicien expiré ; puis, avec cette habitude des garde-malades, métier qu'elle avait exercé pendant dix ans, elle déshabilla Pons, l'étendit, lui colla les mains de chaque côté du corps, et lui ramena la couverture sur le nez, absolument comme un commis fait un paquet dans un magasin.
- Il faut un drap pour l'ensevelir ; où donc en prendre un ?... demanda-t-elle à Schmucke, que ce spectacle frappa de terreur.

Après avoir vu la Religion procédant avec son profond respect de la créature destinée à un si grand avenir dans le ciel, ce fut une douleur à dissoudre les éléments de la pensée, que cette espèce d'emballage où son ami était traité comme une chose.
- Vaides gomme fus fitrez !

... répondit machinalement Schmucke.

Cette innocente créature voyait mourir un homme pour la première fois. Et cet homme était Pons, le seul ami, le seul être qui l'eût compris et aimé !...
- Je vais aller demander à madame Cibot où sont les draps, dit la Sauvage.
- Il va falloir un lit de sangle pour coucher cette dame, dit madame Cantinet à Schmucke.

Schmucke fit un signe de tête et fondit en larmes. Madame Cantinet laissa ce malheureux tranquille ; mais, au bout d'une heure, elle revint et lui dit :
- Monsieur, avez-vous de l'argent à nous donner pour acheter ? Schmucke tourna sur madame Cantinet un regard à désarmer les haines les plus féroces ; il montra le visage blanc, sec et pointu du mort, comme une raison qui répondait à tout.

, dit-il en s'agenouillant.

Madame Sauvage était allée annoncer la mort de Pons à Fraisier, qui courut en cabriolet chez la présidente lui demander, pour le lendemain, la procuration qui lui donnait le droit de représenter les héritiers.
- Monsieur, dit à Schmucke madame Cantinet, une heure après sa dernière question, je suis allée trouver madame Cibot, qui est donc au fait de votre ménage, afin qu'elle me dise où sont les choses ; mais, comme elle vient de perdre monsieur Cibot, elle m'a presque agonie

de sottises... Monsieur, écoutez-moi donc...

Schmucke regarda cette femme, qui ne se doutait pas de sa barbarie ; car les gens du peuple sont habitués à subir passivement les plus grandes douleurs morales.
- Monsieur, il faut du linge pour un linceul, il faut de l'argent pour un lit de sangle, afin de coucher cette dame ; il en faut pour acheter de la batterie de cuisine, des plats, des assiettes, des verres, car il va venir un prêtre pour passer la nuit, et cette dame ne trouve absolument rien dans la cuisine.
- Mais, monsieur, répéta la Sauvage, il me faut cependant du bois, du charbon, pour apprêter le dîner, et je ne vois rien ! Ce n'est d'ailleurs pas bien étonnant, puisque la Cibot vous fournissait tout...
- Mais, ma chère dame, dit madame Cantinet en montrant Schmucke qui gisait aux pieds du mort dans un état d'insensibilité complète, vous ne voulez pas me croire, il ne répond à rien.
- Eh bien ! ma petite, dit la Sauvage, je vais vous montrer comment l'on fait dans ces cas-là.
