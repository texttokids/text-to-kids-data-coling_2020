
Mais le matin, en se levant, l'idée lui était venue brusquement qu'après tout, il ne savait pas combien de temps cela pouvait durer. Il avait décidé de partir. Comme il était recommandé (dans son métier, on a des facilités), il avait pu toucher le directeur du cabinet préfectoral et lui avait dit qu'il n'avait pas de rapport avec Oran, que ce n'était pas son affaire d'y rester, qu'il se trouvait là par accident et qu'il était juste qu'on lui permît de s'en aller, même si, une fois dehors, on devait lui faire subir une quarantaine. Le directeur lui avait dit qu'il comprenait très bien, mais qu'on ne pouvait pas faire d'exception, qu'il allait voir, mais qu'en somme la situation était grave et que l'on ne pouvait rien décider.
- Mais enfin, avait dit Rambert, je suis étranger à cette ville.
- Sans doute, mais après tout, espérons que l'épidémie ne durera pas.

Pour finir, il avait essayé de consoler Rambert en lui faisant remarquer qu'il pouvait trouver à Oran la matière d'un reportage intéressant et qu'il n'était pas d'événement, tout bien considéré, qui n'eût son bon côté. Rambert haussait les épaules. On arrivait au centre de la ville :
- C'est stupide, docteur, vous comprenez. Je n'ai pas été mis au monde pour faire des reportages. Mais peut-être ai-je été mis au monde pour vivre avec une femme. Cela n'est-il pas dans l'ordre ?

Rieux dit qu'en tout cas cela paraissait raisonnable.

Sur les boulevards du centre, ce n'était pas la foule ordinaire. Quelques passants se hâtaient vers des demeures lointaines. Aucun ne souriait. Rieux pensa que c'était le résultat de l'annonce Ransdoc qui se faisait ce jour-là. Au bout de vingt-quatre heures, nos concitoyens recommençaient à espérer. Mais le jour même, les chiffres étaient encore trop frais dans les mémoires.
- C'est que, dit Rambert sans crier gare, elle et moi nous sommes rencontrés depuis peu et nous nous entendons bien.

Rieux ne disait rien.
- Mais je vous ennuie, reprit Rambert. Je voulais simplement vous demander si vous ne pouvez pas me faire un certificat où il serait affirmé que je n'ai pas cette sacrée maladie. Je crois que cela pourrait me servir.

Rieux approuva de la tête, il reçut un petit garçon qui se jetait dans ses jambes et le remit doucement sur ses pieds. Ils repartirent et arrivèrent sur la place d'Armes. Les branches des ficus et des palmiers pendaient, immobiles, grises de poussière, autour d'une statue de la République, poudreuse et sale. Ils s'arrêtèrent sous le monument. Rieux frappa contre le sol, l'un après l'autre, ses pieds couverts d'un enduit blanchâtre. Il regarda Rambert. Le feutre un peu en arrière, le col de chemise déboutonné sous la cravate, mal rasé, le journaliste avait un air buté et boudeur.
- Soyez sûr que je vous comprends, dit enfin Rieux, mais votre raisonnement n'est pas bon. Je ne peux pas vous faire ce certificat parce qu'en fait, j'ignore si vous avez ou non cette maladie et parce que, même dans ce cas, je ne puis pas certifier qu'entre la seconde où vous sortirez de mon bureau et celle où vous entrerez à la préfecture, vous ne serez pas infecté. Et puis même…
- Et puis même ? dit Rambert.
- Et puis, même si je vous donnais ce certificat, il ne vous servirait de rien.
- Pourquoi ?
- Parce qu'il y a dans cette ville des milliers d'hommes dans votre cas et qu'on ne peut cependant pas les laisser sortir.
- Mais s'ils n'ont pas la peste eux-mêmes ?
- Ce n'est pas une raison suffisante. Cette histoire est stupide, je sais bien, mais elle nous concerne tous. Il faut la prendre comme elle est.
- Mais je ne suis pas d'ici !
- À partir de maintenant, hélas ! vous serez d'ici comme tout le monde.

L'autre s'animait :
- C'est une question d'humanité, je vous le jure. Peut-être ne vous rendez-vous pas compte de ce que signifie une séparation comme celle-ci pour deux personnes qui s'entendent bien.

Rieux ne répondit pas tout de suite. Puis il dit qu'il croyait qu'il s'en rendait compte. De toutes ses forces, il désirait que Rambert retrouvât sa femme et que tous ceux qui s'aimaient fussent réunis, mais il y avait des arrêtés et des lois, il y avait la peste, son rôle à lui était de faire ce qu'il fallait.
- Non, dit Rambert avec amertume, vous ne pouvez pas comprendre. Vous parlez le langage de la raison, vous êtes dans l'abstraction.

Le docteur leva les yeux sur la République et dit qu'il ne savait pas s'il parlait le langage de la raison, mais il parlait le langage de l'évidence et ce n'était pas forcément la même chose. Le journaliste rajustait sa cravate :
- Alors, cela signifie qu'il faut que je me débrouille autrement ? Mais, reprit-il avec une sorte de défi, je quitterai cette ville.

Le docteur dit qu'il le comprenait encore, mais que cela ne le regardait pas.
- Si, cela vous regarde, fit Rambert avec un éclat soudain. Je suis venu vers vous parce qu'on m'a dit que vous aviez eu une grande part dans les décisions prises. J'ai pensé alors que, pour un cas au moins, vous pourriez défaire ce que vous aviez contribué à faire. Mais cela vous est égal. Vous n'avez pensé à personne. Vous n'avez pas tenu compte de ceux qui étaient séparés.
