
Rémonencq pria le courtier de soulever Schmucke, qui restait sur son banc comme une masse inerte ; ils le menèrent à la balustrade derrière laquelle le rédacteur des actes de décès s'abrite contre les douleurs publiques.  Rémonencq, la providence de Schmucke, fut aidé par le docteur Poulain, qui vint donner les renseignements nécessaires sur l'âge et le lieu de naissance de Pons. L'Allemand ne savait qu'une seule chose, c'est que Pons était son ami. Une fois les signatures données, Rémonencq et le docteur, suivis du courtier, mirent le pauvre Allemand en voiture, dans laquelle se glissa l'enragé courtier, qui voulait avoir une solution pour sa commande. La Sauvage, en observation sur le pas de la porte cochère, monta Schmucke presque évanoui dans ses bras, aidée par Rémonencq et par le courtier de la maison Sonet.
- Il va se trouver mal !... s'écria le courtier, qui voulait terminer l'affaire qu'il disait commencée.
- Je le crois bien ! répondit madame Sauvage ; il pleure depuis vingt-quatre heures, et il n'a rien voulu prendre. Rien ne creuse l'estomac comme le chagrin.
- Mais, mon cher client, lui dit le courtier de la maison Sonet, prenez donc un bouillon. Vous avez tant de choses à faire : il faut aller à l'Hôtel-de-Ville, acheter le terrain nécessaire pour le monument que vous voulez élever à la mémoire de cet ami des Arts, et qui doit témoigner de votre reconnaissance.
- Mais cela n'a pas de bon sens, dit madame Cantinet à Schmucke en arrivant avec un bouillon et du pain.
- Songez, mon cher monsieur, si vous êtes si faible que cela, reprit Rémonencq, songez à vous faire représenter par quelqu'un, car vous avez bien des affaires sur les bras : il faut commander le convoi ! vous ne voulez pas qu'on enterre votre ami comme un pauvre.
- Allons, allons, mon cher monsieur ! dit la Sauvage en saisissant un moment où Schmucke avait la tête inclinée sur le dos du fauteuil.

Elle entonna dans la bouche de Schmucke une cuillerée de potage, et lui donna presque malgré lui à manger comme à un enfant.
- Maintenant, si vous étiez sage, monsieur, puisque vous voulez vous livrer tranquillement à votre douleur, vous prendriez quelqu'un pour vous représenter...
- Puisque monsieur, dit le courtier, a l'intention d'élever un magnifique monument à la mémoire de son ami, il n'a qu'à me charger de toutes les démarches, je les ferai...
- Qu'est-ce que c'est ? qu'est-ce que c'est ? dit la Sauvage. Monsieur vous a commandé quelque chose ! Qui donc êtes-vous ?
- L'un des courtiers de la maison Sonet, ma chère dame, les plus forts entrepreneurs de monuments funéraires... dit-il en tirant une carte et la présentant à la puissante Sauvage.
- Eh bien ! c'est bon, c'est bon !...  On ira chez vous quand on le jugera convenable ; mais ne faut pas abuser de l'état dans lequel se trouve monsieur. Vous voyez bien que monsieur n'a pas sa tête...
- Si vous voulez vous arranger pour nous faire avoir la commande, dit le courtier de la maison Sonet à l'oreille de madame Sauvage en l'amenant sur le palier, j'ai pouvoir de vous offrir quarante francs...
- Eh bien ! donnez-moi votre adresse, dit madame Sauvage en s'humanisant.

Schmucke, en se voyant seul et se trouvant mieux par cette ingestion d'un potage au pain, retourna promptement dans la chambre de Pons, où il se mit en prières. Il était perdu dans les abîmes de la douleur, lorsqu'il fut tiré de son profond anéantissement par un jeune homme vêtu de noir qui lui dit pour la onzième fois un : - Monsieur ?... que le pauvre martyr entendit d'autant mieux, qu'il se sentit secoué par la manche de son habit.
- Qu'y a-d-il engore ?
- Monsieur, nous devons au docteur Gannal une découverte sublime ; nous ne contestons pas sa gloire, il a renouvelé les miracles de l'Égypte ; mais il y a eu des perfectionnements, et nous avons obtenu des résultats surprenants. Donc, si vous voulez revoir votre ami, tel qu'il était de son vivant...
- Le refoir !

... s'écria Schmucke ; me barlera-d-il !
- Pas absolument !... Il ne lui manquera que la parole, reprit le courtier d'embaumement ; mais il restera pour l'éternité comme l'embaumement vous le montrera.  L'opération exige peu d'instants. Une incision dans la carotide et l'injection suffisent ; mais il est grand temps... Si vous attendiez encore un quart d'heure, vous ne pourriez plus avoir la douce satisfaction d'avoir conservé le corps...
- Hâlis-fis-en au tiaple !... Bons est une âme !... et cedde ame est au ciel
- Cet homme est sans aucune reconnaissance, dit le jeune courtier d'un des rivaux du célèbre Gannal en passant sous la porte cochère ; il refuse de faire embaumer son ami !
- Que voulez-vous, monsieur ! dit la Cibot, qui venait de faire embaumer son chéri. C'est un héritier, un légataire. Une fois son affaire faite, le défunt n'est plus rien pour eux.

Une heure après, Schmucke vit venir dans la chambre madame Sauvage suivie d'un homme vêtu de noir et qui paraissait être un ouvrier.
- Monsieur, dit-elle, Cantinet a eu la complaisance de vous envoyer monsieur, qui est le fournisseur des bières de la paroisse.
