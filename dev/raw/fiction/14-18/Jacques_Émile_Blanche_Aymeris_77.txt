
Tu le sais, dis que tu le sais?

Georges lui avait donné un bon baiser par-dessus la table. Les clients du bistro faisaient des plaisanteries. Rosie l'écarta rageusement: 
-Je ne suis pas une de ces filles qui courent les ateliers, comme celles-là! Si je suis avec toi, c'est que j'ai à gagner mon pain, j'ai pas envie de me le procurer autrement qu'en posant, puisque je suis modèle; je fais mon besogne pour les dix francs par jour, ce que tu me payes; c'est comme si je servais dans une café ou dans un maison particulière; croyez-vous que je me laisserais peloter par la cliente!

Je suis une «lady», moi, et aussi fière que tu l'es de ta famille, j'ai des parents aussi chics que les tiens: mais on fait ce qu'on peut, n'est-ce pas, quand on est pauvre?

Georges me regarda comme pour que j'attestasse qu'il disait vrai: 
-Dis donc à Rosie que je n'ai aucune prévention de ce genre.

Rosemary! Je vois toute l'humanité sur le même plan. Je ne méprise que les hypocrites. Les méchants? Il y a des êtres cruels...... mais il s'agirait de savoir s'ils n'ont pas une excuse.....

Rosie interrompit:-Ça est vrai!-Et Georges encouragé: 
-On a ses raisons, tu crois aussi? Je suis sûr que nous nous entendrions... Tu dissimules, à quoi bon? J'ai souffert aussi... Parle donc, parle! ma vieille, je t'en conjure.
-Taisez voo, imbécile! N'est-ce pas que Georges est stioupide, monsieur?

Je m'aperçus que Georges en était encore aux préliminaires d'un nouvel amour. La suite n'allait plus être du service commandé par Darius Marcellot. Rosie renversa son verre qui se brisa. Elle avala une autre absinthe, et ordonna à Georges, encore une fois, de se taire. Il s'excusait: 
-J'ai une trop longue habitude de me renfermer, pour commettre l'imprudence de la perdre, ainsi, et devant un ami à moi... Plus qu'un mot: alors, je ne suis pas ton copain, Rosie, ton ami à toi?...

réponds... je suis si sincère, Rosie!
-Aoh immbécile! sincerity! Cela est pour les pauvres!

Je commençais à regretter d'être venu; cette scène aurait-elle eu lieu, sans moi? Georges était de ceux qui parlent devant un tiers et ne se livrent pas dans le tête-à-tête avec la femme aimée; il profitait de ma présence, eût voulu que je me portasse garant de sa sincérité, et il m'expliqua Rosie comme un jeune auteur fait d'un manuscrit dont il n'est pas sûr, en le lisant à des amis plus expérimentés, et qui défend surtout les parties qu'il sait les plus faibles.

Elle se leva: 
-Un copain? Aymeris, tu seras toujours, pour moi, le patron. I know what my duty is! Chacun à sa place! Je ne suis pas une oie! (I am not a goose). Moi, j'ai mon honneur. Tu as toujours été chic avec moi. Tiens, voilà ma main, shake hands! Vous entendez, monsieur? Allez donc dîner avec Georges au boulevard! Moi j'ai trimé, je prendrai un bouillon et je me coucherai. A demain matin, Georges: je serai «punctual»! half past eight, sharp.

Rosemary tendait à Georges ses mains osseuses, il les baisa. Elle perla un rire enfantin, qui dut persuader Georges de l'innocence de cette âme primitive, selon lui, mais non point pour moi, qui la devinais complexe; et je tâchai d'emmener le pauvre garçon vers de la lumière et de la gaîté. Il m'accompagna quelques minutes.

Il fit avec moi quelques pas, puis: 
-Ah! non! un autre soir, reviens-dit-il-je suis trop fatigué, je rentre aussi.

Dès ce moment, c'en était fait! Ces deux êtres si disparates, que rien n'eût jamais dû rapprocher, et si éloignés, peut-être encore, quand je les avais vus à la terrasse du bistro, je les avais unis pour leur mutuelle punition. J'entrevis tout de suite les mornes tableaux d'un lamentable collage, l'ennui, le brouillard, le gris d'une existence médiocre, désaccordée, et dont cette scène n'était que le prélude: un de ceux où le compositeur pose les premières notes des thèmes qu'il entremêlera au cours de l'ouvrage, qui éclateront ensemble, de tout leur pathétique, avant la chute du rideau, pour la mort du héros.

Le sentiment de Georges s'était formé comme une tumeur interne; depuis des mois atteint, il l'ignorait. Sa passion avait grandi, cruelle, violente et haineuse; je crus à un de ces amours dont nous guérissons quand ils ne nous tuent pas. D'abord, et c'en fut une peut-être, je crus à une crise d'altruisme, aggravée de littérature-bien de l'époque d'Un amateur d'âmes; à une soif de sympathie qu'aurait mon ami, pour un être plus à plaindre qu'il ne l'était lui-même à ses yeux (et aux miens). Rosie avait la bizarrerie que goûtaient tant les artistes d'alors; elle n'était point parmi les «heureux de ce monde», et de quels abîmes, de quelle fange n'était-elle pas sortie, humiliée et vengeresse, pour venir, de Hong-Kong, s'échouer dans un bouge de Montparnasse! Georges, en fils de bourgeois, était subjugué par le mélange de cynisme et d'innocence, par la verdeur de langue, toute métisse, comme par la vision des choses, directe, disait-on, qu'avait cette fille garçonnière et si féminine, dont la peau, telle qu'un camélia, se duvetait de reflets verts, ou se colorait d'un rose métallique, sous une crinière violette et jaune, réservoir d'effluves que les plus durables parfums échouaient à dominer. Quand elle arrivait avant son peintre à l'atelier, et que nous revenions ensemble de chez Lavenue, Georges me disait, dès le seuil: Je suis en retard, je sens sa peau!
