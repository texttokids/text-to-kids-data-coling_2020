N°4 - décembre 2015
Les régionales 
On en parle peu mais ce sont des élections très importantes. Les dimanches 6 et 13 décembre, les Français sont appelés à voter aux élections régionales. Et cette année, il y a de grands changements puisque ce ne sont plus les mêmes régions qu'avant. Alors à quoi ressemble ta région aujourd'hui ? Et au fait, à quoi ça sert exactement ?


Gabin, 16 ans, conseiller régional des jeunes

Gabin a 16 ans et il habite au Mans, dans l’ouest de la France. Depuis un an, ce lycéen est membre du conseil régional des jeunes des Pays de la Loire.
C’est un groupe de personnes de 15 à 30 ans qui réfléchit à des projets à mettre en place dans la région. Par exemple, en ce moment, Gabin imagine un système de covoiturage dans les collèges et les lycées. Cela permettrait aux élèves, aux enseignants et à toutes les personnes qui travaillent dans les établissements scolaires de faire des trajets en voiture ensemble. Si tout le monde partage sa voiture, cela réduit la pollution, évite d’attendre trop longtemps le bus et coûte moins cher.
Pour mettre ce système en place, les membres du conseil régional des jeunes font des réunions. Parfois, ils se réunissent seulement entre eux. D’autres fois, ils invitent des adultes qui connaissent bien les lois et qui peuvent les conseiller pour que le projet fonctionne bien.
Ils ont présenté leur projet au conseil régional, qui est un groupe de personnes élues par les citoyens pour prendre des décisions pour la région. Et le conseil a accepté de mettre en place le système de covoiturage. Ce ne sont pas les jeunes qui prennent directement les décisions, mais au moins ils peuvent dire aux adultes de quoi ils ont envie ou besoin.
Gabin aime beaucoup faire partie du conseil régional des jeunes, parce qu’il se sent utile. Grâce à ça, il s’intéresse maintenant à l’écologie, alors qu’il n’y connaissait rien avant. Et puis, dit-il, « ça permet de découvrir la citoyenneté et les différentes institutions. Je n’avais aucune idée de ce qu’était un conseil régional avant. La région s’occupe de notre quotidien, de notre vie. On ne s’en aperçoit pas. »
Le Mans, ville de Gabin


Qu'est-ce qu'une région ?

La France est comme un grand puzzle : elle est découpée en plusieurs morceaux. D’abord, il y a les régions, à l’intérieur desquelles on trouve des départements, qui eux-mêmes sont divisés en communes, c’est-à-dire en villes. Chaque région a une capitale, qu’on appelle un chef-lieu.
Régions, départements, communes : on les appelle des collectivités territoriales. Le rôle des élus est de prendre des décisions adaptées à leur territoire, parce qu’ils sont tous différents. Plutôt que de décider à Paris, on décide sur place, à Lyon, à Strasbourg ou à Marseille, par exemple.
On ne le sait pas forcément, mais la région s’occupe de choses qu’on utilise au quotidien pour se déplacer, travailler ou faire du sport.
Elle s’occupe par exemple des lycées publics. Elle décide de les construire et finance les travaux. Au quotidien, elle paye les personnes qui font le ménage, qui travaillent à la cantine et qui surveillent les élèves (mais ce n’est pas elle qui paye les professeurs, c’est l’Etat).
Ce sont les départements qui s’occupent des collèges et les communes qui prennent en charge les écoles maternelles et primaires.
La région donne aussi de l’argent aux structures qui aident les jeunes à trouver un travail.
Elle s’occupe des trains et des cars régionaux : elle achète les wagons, fixe le prix des billets et les horaires. En 2017, elle s’occupera aussi des transports scolaires. Pour l’instant, ceux-ci sont gérés par les départements.
La région a aussi beaucoup d’autres missions, par exemple aider les entreprises à s’installer sur son territoire, encourager les gens à trier leurs déchets ou donner de l’argent aux médecins qui s’installent dans des endroits où il en manque. Elle soutient aussi des festivals de musique, des tournages de cinéma ou des tournois de football.
Aujourd’hui, il y a 22 régions en France métropolitaine (c’est la partie de la France qui est en Europe) et cinq dans les départements et régions d’outre-mer (qui ne sont pas en Europe). A partir du 1er janvier 2016, ça va changer : les 22 régions actuelles seront regroupées pour n’en former plus que 13. Par exemple, la Bourgogne et la Franche-Comté vont être réunies.
Puisqu’on change la forme des régions, c’est comme si on redécoupait la carte de France. On appelle ça le redécoupage. Il a été imaginé par le gouvernement et tout le monde n’était pas d’accord. Plusieurs régions ne voulaient pas être associées à d’autres, notamment parce qu’elles ont peur d’être isolées ou craignent que leurs traditions n’existent plus, perdues au milieu de celles des autres. Certains sont en revanche contentes : elles n’avaient pas l’impression d’exister toutes seules.
Pour l’instant, les régions ont accolé leur ancien nom pour créer les nouveaux, comme Alsace-Champagne-Ardenne-Lorraine : c’est très long et pas très pratique. Après les élections, les régions devront se choisir un nouveau nom.
Si le gouvernement a voulu redécouper les régions, c’est pour faire des économies : passer de 22 à 13 est censé permettre de réduire le nombre de bureaux et de papiers à faire, ce qui coûte normalement moins cher. Mais tout le monde n’est pas d’accord avec cet argument.
L’autre objectif avec des régions plus grandes, c’est qu’elles aient moins de différences entre elles, par exemple qu’elles aient une richesse et un nombre d’habitants qui se ressemblent d’une région à l’autre.
Six régions n’ont pas changé avec le redécoupage : la Bretagne, le Centre Val-de-Loire, la Corse, l’Ile-de-France, les Pays de la Loire et Provence-Alpes-Côte-d’Azur.


Pourquoi vote-t-on dimanche ?

Les régions sont gérées par ceux qu’on appelle des conseillers régionaux. Ce sont des hommes et des femmes qui ont été élus pour six ans par les citoyens, pour les représenter. Les 6 et 13 décembre, c’est le moment de retourner voter. Dans toute la France seront organisées les élections régionales : les Français de 18 ans et plus vont aller désigner le nouveau conseil régional. Toutes les régions n’ont pas le même nombre d’élus, car cela dépend du nombre d’habitants. Par exemple, il y en a 204 en Auvergne-Rhône-Alpes et 83 en Bretagne. Au total, il y en a 1 757 dans toute la France.
Les électeurs de chaque région devront voter pour une liste de candidats. En général, chaque liste correspond à un parti politique. Mais parfois, plusieurs partis s’associent. Sur la liste, il doit y avoir autant d’hommes que de femmes : c’est le principe de la parité.
Dans chaque région, on élit parmi les conseillers régionaux le président du conseil régional, c’est le plus souvent le chef de la liste qui a obtenu le plus gros score.
Depuis les élections de 2010, la plupart des régions sont gérées par le Parti socialiste (le PS, c’est le parti du Président François Hollande). Il est parfois allié à d’autres partis, comme le Parti des radicaux de gauche (PRG), Europe Ecologie-Les Verts (EE-LV) ou le Front de gauche (FG). L’Alsace est gérée par le parti qui s’appelle Les Républicains (c’est celui de l’ancien Président, Nicolas Sarkozy), qui s’est allié à l’Union des démocrates et indépendants (UDI).
D’autres partis présentent des candidats aux élections, comme le Front national (FN), Lutte ouvrière (LO) ou l’Union populaire républicaine (UPR). Il y a aussi des partis qui ne se présentent que dans une région, comme le Parti pirate en Ile-de-France.
Si dès le premier jour de l’élection, qu’on appelle le premier tour, une liste obtient plus de la moitié des voix, elle a gagné et comptera le plus d’élus au conseil régional. Si ce n’est pas le cas, il y aura un deuxième tour : les électeurs voteront une nouvelle fois, la semaine suivante.


Comment se déroule une élection ?

Les Français vont voter aux élections régionales le 6 et le 13 décembre. Pour avoir le droit de voter, il faut être majeur (avoir au moins 18 ans) et être inscrit sur les listes électorales, c’est-à-dire les listes sur lesquelles sont écrits tous les noms des citoyens qui ont le droit de voter.
L’élection se passe dans un bureau de vote, généralement installé dans une mairie ou une école. Chaque électeur doit aller dans celui qui est indiqué sur sa carte d’électeur. Dans chaque ville, le nombre de bureaux de vote dépend du nombre d’habitants. Par exemple, il y en a 136 à Bordeaux et 71 à Amiens, qui est une plus petite ville.
Il faut d’abord montrer sa carte d’identité et sa carte d’électeur. Ça permet de vérifier qu’on est bien inscrit sur les listes électorales et qu’on a bien le droit de voter.
On prend une enveloppe et des bulletins de vote. Ce sont les papiers sur lesquels sont écrits les noms des candidats. On n’est pas obligé de prendre tous les bulletins, mais il vaut mieux en prendre au moins deux, car le vote doit être secret : si on n’en prenait qu’un, tout le monde saurait pour qui on vote.
On se rend dans l’isoloir, une sorte de petite cabine fermée par un rideau. Une fois qu’on est dedans, à l’abri des regards, on met le bulletin du candidat pour qui on vote dans l’enveloppe.
On montre une nouvelle fois sa carte d’identité et sa carte d’électeur. Puis on glisse l’enveloppe contenant le bulletin dans une boîte, qu’on appelle l’urne.
Il faut signer pour prouver qu’on a bien voté. Ça permettra ensuite de vérifier que le nombre de bulletins correspond au nombre de votants.
Dans certains bureaux de vote, il n’y a ni bulletins en papier, ni enveloppes, ni urne car le vote est électronique : les listes de candidats sont présentées sur un écran et il faut appuyer sur celle pour qui on veut voter.
Les personnes qui ne peuvent pas aller voter, par exemple parce qu’elles sont parties en vacances, ont le droit de faire une procuration, c’est-à-dire autoriser quelqu’un d’autre à aller voter pour elles.
Le soir, quand le bureau de vote ferme, commence le dépouillement, c’est-à-dire l’ouverture de toutes les enveloppes pour compter le nombre de voix que chaque candidat a obtenu. N’importe quel électeur qui sait lire et écrire peut dépouiller.
Certains bulletins ne sont pas valables. On dit alors qu’ils sont nuls. C’est par exemple le cas quand quelqu’un a écrit sur le bulletin ou quand il a mis plusieurs bulletins dans l’enveloppe.
Un électeur peut décider de ne pas mettre de papier dans l’enveloppe : on dit qu’il vote blanc, c’est-à-dire pour personne. C’est une façon de dire qu’aucun candidat ne lui plaît, mais qu’il s’intéresse quand même à l’élection.
Quand tous les bulletins ont été dépouillés, le président du bureau de vote annonce les résultats.


La carte d'identité de ma région

Clique sur chaque nom de région pour les découvrir
Auvergne-Rhône-Alpes
Bourgogne-Franche-Comté
Bretagne
Centre-Val de Loire
Corse
Alsace-Champagne-Ardenne-Lorraine (Grand Est)
Nord-Pas-de-Calais-Picardie (Hauts-de-France)
Île-de-France
Normandie
Aquitaine-Limousin-Poitou-Charentes (Nouvelle-Aquitaine)
Languedoc-Roussillon-Midi-Pyrénées (Occitanie)
Pays de la Loire
Provence-Alpes-Côte d’Azur
Guadeloupe
Martinique
Guyane
La Réunion
Mayotte


