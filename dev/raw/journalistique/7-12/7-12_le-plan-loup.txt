N°45 - 16 au 22 février 2018
le plan loup
Plusieurs dizaines de loups vivent en liberté en France. Il s’agit d’un animal protégé, qu’on n’a pas le droit de tuer exprès. Mais les loups mangent parfois des brebis et les agriculteurs qui les élèvent ne sont pas contents. Il est donc possible d'en tuer dans certains cas, pour protéger les troupeaux.


Mélia, 11 ans, habite en Lozère

Mélia habite dans le parc national des Cévennes, dans le sud-est de la France. C’est un grand parc réparti sur plusieurs départements, situé en moyenne montagne, à plusieurs centaines de mètres d’altitude. Elle vit entourée de forêts et d’animaux sauvages, comme des renards, des chevreuils, des blaireaux ou des vautours. Depuis neuf ans, des loups vivent aussi dans ce parc.
«C’est un bel animal, qui est protégé. S’il est protégé, c’est que c’est important, estime la jeune fille. Mais il y a aussi un mauvais côté, les éleveurs perdent beaucoup de bêtes.» Dans cette région, de nombreuses personnes élèvent des moutons et des brebis. «Il y a eu des troupeaux attaqués. On n’est pas sûrs que ce soit tout le temps à cause des loups, c’est peut-être des chiens», note Mélia.
Les agriculteurs font pâturer leurs bêtes dans la montagne, c’est-à-dire qu’ils les laissent manger les herbes et les plantes qui poussent naturellement. L’été, quand il fait chaud, ils les font pâturer la nuit, une fois que les températures ont baissé. Pendant ce temps-là, les éleveurs dorment, et ne peuvent pas surveiller leurs troupeaux. Il est alors plus facile pour les loups de venir manger leurs bêtes.
«Il y a eu des manifestations contre le loup l’année dernière», se souvient Mélia. Les agriculteurs voudraient que cet animal sauvage disparaisse du parc national des Cévennes et, pour cela, avoir le droit de le tuer. Mais d’autres personnes veulent au contraire le protéger et disent que c’est aux agriculteurs de mettre en place plus de mesures de protection de leurs troupeaux. C’est le rôle du gouvernement de choisir les règles.
Ispagnac, la ville où habite Mélia


Qu’est-ce que le plan loup ?

Tous les cinq ans, le gouvernement français choisit des règles concernant le loup. Elles sont réunies dans un document appelé «plan loup». Là-dedans, il décide :
comment on protège cet animal, car il n’y en a pas beaucoup dans le pays
dans quelles conditions on a le droit de le tuer, parce qu’il attaque les troupeaux de moutons
Cette année, mi-février, un nouveau «plan» va être mis en place. Ses règles devront être respectées pendant cinq ans, jusqu’en 2023. Ce plan explique par exemple que le nombre de loups doit augmenter en France : aujourd’hui ils sont environ 360 en liberté et l’objectif est qu’ils soient 500. A partir de 500 bêtes, l’espèce peut survivre.
Depuis près de 40 ans, le loup est une espèce protégée en France. Ça veut dire qu’on n’a pas le droit de le tuer exprès. Mais il y a une exception à cette loi : si les loups menacent les troupeaux et s’il n’existe pas d’autre solution pour protéger les bêtes, le gouvernement peut autoriser certaines personnes, qui travaillent auprès des animaux sauvages, à les tuer.
Entre 40 et 50 loups (10% de leur nombre total) peuvent être abattus chaque année. Ce sont les ministres de l’Ecologie et de l’Agriculture qui décident. Entre l’été 2017 et l’été 2018, par exemple, c’est 40.
Au mois de janvier, un document contenant les propositions du nouveau plan loup a été publié sur Internet et les citoyens ont pu écrire leurs remarques. On a ainsi pu noter que ce projet ne convenait pas à tout le monde.
Des éleveurs de brebis et de moutons ainsi que des responsables politiques des zones de montagne trouvent que ce plan prend trop la défense des loups. D’abord parce qu’il est prévu que, si les éleveurs n’ont pas protégé leurs troupeaux correctement, ils ne toucheront pas d’argent en cas d’attaque. Et, de toute façon, les éleveurs disent qu’il est impossible que le loup vive au même endroit que les bêtes d’élevage sans les attaquer. Ils souhaiteraient donc que l’animal sauvage soit moins protégé et que le nombre de tirs autorisés pour le tuer soit plus élevé.
Mais là, ce sont les défenseurs des animaux sauvages qui refusent : dans le plan, il est déjà possible de tuer les loups à condition d’avoir bien protégé son troupeau, rappellent-ils. Selon ces associations qui veulent protéger le loup, aucun tir ne devrait être autorisé car c’est inutile si les moutons sont surveillés par des hommes et des chiens.


Où trouve-t-on des loups en France ?

Durant plus de 50 ans, les loups avaient disparu de France. Les humains faisaient tout pour les tuer, afin qu’ils n’attaquent pas leurs troupeaux, et avaient réussi à s’en débarrasser. Ainsi, on en a vu jusqu’en 1939 et puis… plus rien.
Mais en 1992, il y a plus de 25 ans, des hommes ont aperçu deux animaux dans le parc du Mercantour, dans le sud-est de la France. C’était le grand retour des loups dans le pays ! Ils venaient d’Italie, où l’espèce était protégée depuis plusieurs années, et avaient traversé les Alpes.
Des loups qui vivaient en Italie sont venus s'installer en France
En France, «il y avait beaucoup de zones boisées avec des chevreuils, des cerfs, des chamois, des sangliers», dont se nourrissent les loups, explique Pierre Rigaux, spécialiste des mammifères sauvages. Résultat, les loups ont trouvé de quoi vivre, ils se sont installés et reproduits. Aujourd’hui, on compte au moins 360 loups en France, dans des zones de montagne, surtout dans le Sud-Est.
Les zones où les loups vivent en France
Les loups vivent en meute, c’est-à-dire en groupe, de 3 à 8 animaux. «Ils ne peuvent pas être trop nombreux sur un territoire parce qu’il n’y a pas assez de proies», précise Pierre Rigaux. Du coup, certains quittent leur meute pour s’installer ailleurs, chercher un autre animal avec qui s’accoupler et créer leur propre clan. C’est pour ça que les populations de loups s’installent sur de nouveaux territoires au fil des années.
Aujourd’hui, on en trouve dans le Massif central, dans les Vosges et dans les Pyrénées, et les scientifiques ont prouvé qu’il s’agissait de loups venant à l’origine des Alpes.
Les loups du Massif central, des Vosges et des Pyrénées viennent des Alpes
Il n’est pas toujours simple pour les loups de se reproduire dans une zone qu’ils ne connaissent pas. D’abord, il faut trouver un mâle ou une femelle avec qui faire des petits. Et puis «un loup seul dans un territoire qu’il ne connaît pas a une faible espérance de vie. C’est dur pour lui de chasser et il n’est pas protégé par la meute», affirme Pierre Rigaux. Dans les Vosges, par exemple, les loups ont mis plusieurs années avant de commencer à se reproduire.
A quoi les loups servent-ils dans la nature ?Quand les cerfs ou les sangliers, par exemple, sont trop nombreux quelque part, ils font des dégâts. En les mangeant, les loups sont donc utiles pour l’environnement. En plus, ils s’attaquent d’abord aux animaux malades ou blessés, qui auraient de toute façon du mal à survivre.


Pourquoi les éleveurs en veulent-ils aux loups ?

Quand ils sont adultes, les loups mangent entre 2 et 4 kilos de viande par jour. Au menu de leurs repas : du cerf, du chamois, du mouflon, du sanglier… Parfois, ils se laissent tenter par un lièvre, un rongeur ou un oiseau. Mais, même s’ils aiment les animaux sauvages, ils n’ont rien contre ceux qui sont élevés par les humains. Quand ils croisent un troupeau de moutons, ça les fait saliver… C’est bien ça qui embête les éleveurs. L’an dernier, sur les 7 millions de brebis élevées en France, 10 000 ont été tuées par des loups.
«C’est invivable», lâche François Monge. Cet éleveur de 52 ans possède un troupeau de 450 brebis dans la Drôme, dans le sud-est de la France. L’an dernier, alors que ses bêtes mangeaient, un agneau a disparu. Il l’a retrouvé mort quelque temps après et s’est rendu compte qu’une de ses brebis avait été mordue et griffée. Les deux animaux avaient été attaqués par un loup. Pourtant, François Monge avait mis un filet électrique autour de son troupeau pour le protéger.
En septembre dernier, ses brebis mangeaient avec d’autres troupeaux. Les animaux étaient gardés par des bergers : un le jour, un la nuit. Il y avait aussi un patou, un chien dont le rôle est de protéger les bêtes et d’empêcher les attaques. «Tous les soirs il y avait des loups, il fallait tirer», raconte François Monge. Les trois loups qui rôdaient ont réussi à tuer trois ou quatre brebis, ainsi que le patou.
«Quand le loup attaque, ça stresse les bêtes, précise l’éleveur. J’ai eu 100 agneaux de moins que prévu parce que les brebis [qui étaient enceintes] ont perdu leur bébé. A cause de ça, il va me manquer 20 000 euros.» François Monge élève ses animaux pour faire de la viande.
François Monge pense qu’il n’est pas possible d’élever des animaux dans la montagne tant qu’il y a des loups et voudrait en être débarrassé.


Pourquoi le loup fait-il peur ?

Les Trois Petits Cochons, le Petit Chaperon rouge, la Chèvre de monsieur Seguin, le Loup et l’Agneau… L’animal est souvent utilisé dans les histoires et les contes pour faire peur. Depuis des centaines d’années, il représente la bête qui rôde dans la forêt à la recherche d’un enfant à croquer, d’une grand-mère à dévorer. C’est l’animal qu’on a peur de croiser derrière un arbre, qui pourrait faire du mal aux personnes innocentes.
Pourtant, les loups ont généralement peur des humains. Depuis le retour de l’animal en France, il y a plus de 25 ans, on n’a compté aucune attaque de loup sur l’homme. Alors d’où vient sa mauvaise réputation ?
A l’époque où les humains chassaient, ils s’entendaient plutôt bien avec le loup. Chacun attrapait les bêtes qu’il voulait, parfois même les loups mangeaient les restes des animaux tués par les humains. Mais au Moyen Age, les hommes se sont mis à l’élevage. Dès lors, hors de question de laisser les loups manger leurs troupeaux ! L’humain s’est mis à se méfier du loup et à s’en protéger…
Il faut dire aussi qu’à une époque, les loups attaquaient les humains. Ça se produisait surtout dans des périodes de guerre ou de famine, quand il était difficile de trouver à manger. Les bêtes s’en prenaient souvent aux enfants, car à l’époque les enfants n’allaient pas à l’école et gardaient les troupeaux.
Au XVIIIe (18e) siècle, il y avait entre 15 000 et 20 000 loups en France, c’est-à-dire entre 40 et 50 fois plus qu’aujourd’hui ! On en trouvait partout, même dans les grandes villes. Une toute petite partie d’entre eux s’en prenait aux humains, mais ça a suffi pour faire peur. Surtout que le loup a des crocs très pointus et, quand il attaque, il mord la gorge, c’est très violent.
C’est à cette époque qu’a eu lieu l’histoire de la Bête du Gévaudan : entre 1764 et 1767, un animal a tué des dizaines d’humains dans le Gévaudan (ça correspond aujourd’hui à la Lozère, où habite Mélia, qui parle au début de ce numéro). On n’a jamais vraiment su ce qu’était cette bête mais beaucoup de personnes ont dit qu’il s’agissait d’un loup. Les gens ont alors eu encore plus peur de l’animal.
Aujourd’hui encore, le loup est très présent dans les histoires pour enfants. Mais de plus en plus, on se moque de lui, pour lui enlever son image effrayante. Difficile en effet d’avoir peur du Loup en slip, le héros de la BD du même nom qui hurle et montre ses crocs pointus parce qu’il a froid aux fesses !


