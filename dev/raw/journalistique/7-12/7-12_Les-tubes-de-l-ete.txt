N°23 - juillet 2017
Les tubes de l'été
Chaque été, on aime écouter en boucle la même chanson, la chanter à tue-tête, danser dessus… Et quand on retourne à l’école, on la réécoute en repensant aux vacances. C’est ça, la magie des tubes de l’été, ces chansons qui connaissent un grand succès. Quel sera le morceau des grandes vacances cette année ? D’où viennent les tubes de l’été ? Comment fait-on un album et comment se fabrique un tube aujourd'hui ? «Le P’tit Libé» t’emmène en musique sur la route des vacances.


Erza, 11 ans, chanteuse des Kids United

Erza a 11 ans et elle est déjà une star ! Elle fait partie des Kids United, avec quatre autres enfants âgés de 10 à 17 ans. Depuis son premier album sorti il y a deux ans, le groupe a beaucoup de succès et a déjà vendu plus de 1,5 million de disques. En juin, les Kids United ont sorti une chanson, Mama Africa, qui pourrait bien devenir un tube cet été. «Je l’aime beaucoup parce qu’elle apporte de la joie et donne envie de danser, explique Erza. En plus, on a tourné le clip au Sénégal pendant quatre jours et on s’est vraiment amusés.»
La jeune chanteuse habite à Sarreguemines, une ville située dans le nord-est de la France. Elle termine son année de sixième avec des félicitations. Elle va à l’école la semaine comme tous les autres enfants. Mais le soir, après avoir mangé et fait ses devoirs, elle répète souvent de nouveaux morceaux. Ça ne la dérange pas parce qu’elle «chante tout le temps», partout où elle va et ça depuis l’âge de 2 ans !
Chaque week-end et parfois le mercredi, elle retrouve les Kids United pour chanter dans des concerts, poser pour des séances photo, répondre à des interviews… «Je ne pensais avoir ce succès au début. On me demandait des photos, des autographes, on me reconnaissait dans la rue... Maintenant je me suis habituée et ça me fait toujours plaisir.»
Tout le mois de juillet, Erza se produit devant des milliers de spectateurs lors d’une dizaine de concerts en France, en Belgique et en Suisse. En août, elle pourra enfin se reposer et partira en vacances au Kosovo, le pays d’origine de sa famille, où habitent ses grands-parents et ses cousins. Plus tard, Erza souhaite «toujours chanter tout en faisant des études pour devenir dentiste parce que j’aime bien ce métier».
Sarreguemines, la ville où habite Erza


Quel est le tube de cet été ?

Tout au long de l’année, des chansons connaissent un très grand succès et sont diffusées en boucle, un peu partout. On appelle ça des tubes. L’été, c’est un peu particulier : ces morceaux accompagnent nos vacances, nos soirées au camping, nos barbecues, nos séjours en colonie… Ils sont associés à la détente et aux loisirs donc, souvent, on en garde un souvenir plaisant.
Cet été, on devrait beaucoup entendre Despacito, qui est ou a été numéro 1 des ventes dans près de 80 pays. Au mois de juin, le clip a dépassé les 2 milliards de vues sur YouTube. C’est très rare, car seules une douzaine de chansons ont atteint un tel score. Ce titre est chanté par Luis Fonsi et Daddy Yankee, qui viennent tous les deux de Porto Rico, une île au sud des Etats-Unis. Mais c’est Justin Bieber qui l’a rendu très célèbre car il en a fait une reprise.
Le groupe Kids United espère que sa chanson Mama Africa sera un tube de l’été. «On y trouve tout ce qui rappelle le soleil, la plage, avec des rythmes africains très dansants, plusieurs moments marquants comme un refrain qu’on n’oublie pas. La chanson doit mettre tout le monde de bonne humeur», explique Jérémy Chapron, le directeur artistique et coach vocal du groupe. Une chorégraphie très simple accompagne aussi le morceau dans le clip «comme ça tout le monde peut la reprendre facilement», ajoute Jérémy Chapron.
Cette chanson est d’ailleurs sortie au mois de juin alors qu’elle fait partie de l’album Forever United qui sort en août. «Ça permet au public de l’entendre avant pour bien la connaître et danser dessus cet été.» Et pour que le plus de monde possible la connaisse, la chanson est diffusée plusieurs fois par jour sur la chaîne M6, qui est partenaire des Kids United.
D’autres chansons vont peut-être devenir des tubes de l’été, mais ce n’est pas toujours simple de le savoir à l’avance. Le P’tit Libé en a fait une sélection.
«That’s What I Like» de Bruno Mars
«Chocolat» de Lartiste avec Awa Imani
«I’m the One» de DJ Khaled avec Justin Bieber, Quavo, Chance the Rapper, Lil Wayne
«Wild Thoughts» de DJ Khaled avec Rihanna, Bryson Tiller
«Shape of You» d’Ed Sheeran
Au mois de juin, une chanson française a fait parler d’elle : Claquettes chaussettes, du rappeur Alrima. Tout à coup, à cause de cette chanson, des adolescents se sont mis à aller en cours en portant des claquettes et des chaussettes, un style vestimentaire qui était jusqu’alors souvent jugé ridicule.
Avec ses paroles très simples, qui parlent de barbecue, de transats, de piscine, et qui restent dans la tête, ce titre a de quoi être un tube de l’été. Mais pas sûr que tout le monde ait très envie de chanter à la gloire des claquettes-chaussettes !


D’où viennent les tubes de l'été ?

A l’origine, le mot «tube» désignait l’objet sur lequel on enregistrait les chansons, qui avait la forme d’un cylindre. C’était l’ancêtre des CD et des disques. Dans les années 1950, l’écrivain Boris Vian a décidé d’utiliser le mot «tube» pour parler des chansons à succès parce qu’il trouvait qu’elles étaient creuses comme des tubes en plastique.
Pour reconnaître un tube, c’est assez simple : on l’entend beaucoup à la radio, mais aussi au supermarché, dans les cafés, à la kermesse de l’école… Bref, un peu partout. Il s’agit d’une chanson avec une mélodie facile à retenir, qu’on aime bien fredonner. Elle plaît à des gens très différents, des petits comme des grands, des riches comme des pauvres, etc.
Selon les époques, les tubes de l’été sont différents. Dans les années 1960, c’étaient surtout des slows, c’est-à-dire des musiques lentes et romantiques. Dans les années 1970, les gens aimaient beaucoup le disco, une musique sur laquelle on pouvait danser de manière énergique. A partir des années 1980, les chansons très festives, avec des chorégraphies et des filles dénudées, ont pris le dessus.
Plusieurs chansons sont devenues des tubes de l’été grâce à des chaînes de télévision, et en particulier TF1. A une époque, la chaîne choisissait la chanson qu’elle voulait transformer en tube de l’été et en diffusait le clip à longueur de journée. Le meilleur exemple est la Lambada, sortie en 1989.
Cet été-là, le clip était diffusé au moins une fois par heure sur TF1. En plus, la pub pour Orangina utilisait la musique de la Lambada. La marque de soda avait en effet passé un accord avec TF1. C’est pour ça qu’on voit les enfants boire de l’Orangina dans le clip. Résultat, entre le clip et les pubs pour la boisson, les téléspectateurs de TF1 n’avaient aucune chance d’échapper à cette chanson !
Plus récemment, en 2011, TF1 a mis en avant la chanson A nos actes manqués, de M. Pokora, qui a été très diffusée sur la chaîne.
A l’étranger, des tubes à d’autres moments de l’annéeAux Etats-Unis, les succès musicaux liés à la détente et aux loisirs sortent au printemps. C’est en effet à ce moment qu’a lieu le «spring break», une période de vacances pendant laquelle les étudiants font beaucoup la fête. Au Brésil, c’est en février, au moment du carnaval, car c’est une fête très importante dans ce pays.


Quels sont les 10 plus grands tubes de l'été ?

Les tubes de l’été se suivent mais ne se ressemblent pas forcément. Entre les années 1960 et les années 2010, les styles de musique qui plaisent au public ont évolué. Voici une sélection de dix tubes de l’été qui ont marqué l’histoire.
1965 : «Aline», de Christophe
Christophe a écrit cette chanson en quelques minutes, chez sa grand-mère, alors qu’elle l’appelait pour qu’il vienne déjeuner. Au départ, la fille dont il parle dans sa chanson n’avait pas de prénom, puis il a opté pour Aline, car c’était le nom d’une petite amie qu’il a eue à l’époque.
1970 : «In the Summertime», de Mungo Jerry
30 millions d’exemplaires de ce morceau ont été vendus dans le monde, ce qui en fait l’un des singles les plus vendus de l’histoire. Venu du Royaume-Uni, il a connu un énorme succès en France, aux Pays-Bas, en Norvège, aux Etats-Unis, en Australie… Ses paroles évoquent la joie d’être en été, le plaisir de boire un verre, de rencontrer des filles, de se baigner… Une légèreté qui plaît au public.
1974 : «Waterloo», d’Abba
Avec cette chanson, le groupe suédois Abba a remporté l’Eurovision, un grand concours européen de chansons. Waterloo est le nom d’une ville belge où a eu lieu une célèbre bataille, au cours de laquelle l’armée française de Napoléon Ier fut battue. Mais le morceau parle d’amour, pas de guerre : c’est l’histoire d’une femme qui s’engage à aimer un homme toute sa vie.
1979 : «Born to Be Alive», de Patrick Hernandez
Au début, personne en France ne voulait de cette chanson disco. Alors elle est sortie en Italie, et ça a été un succès immédiat. Très vite, les radios d’autres pays, dont la France, se sont mises à la passer en boucle. Aujourd’hui encore, Born to Be Alive rapporte 1 000 à 1 500 euros par jour à Patrick Hernandez, car elle est toujours très diffusée.
1989 : «Lambada», de Kaoma
Quand on parle de tubes de l’été, on pense immédiatement à la Lambada. Le problème, c’est que la chanson a été copiée : à l’origine, elle a été créée par des musiciens boliviens, et un Français a décidé de la récupérer et de la changer un peu, sans demander l’autorisation. Il a gagné beaucoup d’argent alors que ce n’était pas son idée. Plus tard, les auteurs ont porté plainte et récupéré l’argent.
1996 : «Macarena», de Los del Rio
Une des raisons du succès de ce morceau est sa chorégraphie : grâce à ses gestes très simples, imaginés par une chorégraphe française, petits et grands l’ont apprise très vite et se sont mis à la danser partout, tout le temps. Cette année-là, l’ancien président américain Bill Clinton a même fait de Macarena la musique de sa campagne électorale !
1998 : «I Will Survive», de Hermes House Band
Ce titre est d’abord sorti en 1979, chanté par Gloria Gaynor. Vingt ans plus tard, le groupe Hermes House Band l’a repris, et l’équipe de France de foot en a fait sa chanson officielle pour la Coupe du monde, organisée à Paris. Les Bleus ont gagné et I Will Survive a eu un énorme succès, qui reste associé à cet événement.
2002 : «Asereje», de Las Ketchup
Le refrain de ce morceau… ne veut rien dire ! Les chanteuses ont en effet repris «en yaourt» et avec leur accent espagnol une célèbre chanson de rap américaine. «I said a hip hop» est ainsi devenu «Asereje». Avec sa chorégraphie facile à apprendre, les gens ont dansé sur cette chanson tout l’été.
2012 : «Gangnam Style», de Psy
Le clip de cette chanson est la vidéo la plus regardée sur YouTube, avec près de 3 milliards de vues. Dans le monde entier, on refait la célèbre danse du cheval de ce chanteur sud-coréen, et tant pis si on n’arrive pas à chanter les paroles !
2013 : «Papaoutai», de Stromae
Avec près de 450 millions de vues sur YouTube, le clip de Papaoutai est la vidéo francophone la plus regardée au monde. Contrairement à d’autres tubes de l’été aux paroles légères, cette chanson parle de la tristesse du chanteur d’avoir grandi sans son papa.


Comment fait-on un album ?

Un album, c’est un disque (qu’on peut aussi écouter sur Internet) sur lequel il y a en moyenne dix chansons et qui dure entre 40 et 45 minutes. Mais il n’y a pas de règles, certains ont moins de chansons, d’autres en ont plus de vingt. On peut en faire un chez soi ou avec une entreprise qui s’occupe de tout. Ça s’appelle une maison de disques (les trois plus importantes dans le monde sont Universal Music, Sony Music et Warner) ou un label indépendant.
Il existe des chansons sans textes, avec seulement de la musique. La personne qui crée une musique est un compositeur. Il existe aussi des chansons avec des paroles, qui sont écrites par des auteurs. Certains artistes composent et écrivent des chansons. On dit qu’ils sont auteurs-compositeurs. D’autres chantent des paroles écrites par d’autres personnes, on dit alors qu’ils sont interprètes. C’est par exemple le cas de la chanteuse Louane.
Il peut se faire chez soi. Pour ça, il faut des micros pour enregistrer sa voix et des instruments. On peut aussi faire un album sans instruments, avec un ordinateur, grâce auquel on peut créer des sons de batterie, de guitare ou encore de violon. Ça se fait notamment beaucoup dans la musique électronique. Un jeune de 16 ans, Petit Biscuit, a par exemple enregistré toutes ses chansons seul dans sa chambre et il joue maintenant devant des milliers de personnes !
Enregistrer des chansons peut aussi se faire dans des studios d’enregistrement. Ça coûte cher, donc certains artistes qui n’ont pas beaucoup d’argent enregistrent leurs chansons en quelques jours ou même en une journée.
Parfois, les groupes enregistrent en live pour que les gens entendent ensuite toute l’énergie qu’il y a eue à ce moment-là, et tant pis si ce n’est pas parfait. D’autres fois, ils enregistrent chacun de leur côté : la guitare, la voix, la flûte, etc.
Dans le studio d’enregistrement, les artistes se placent derrière une vitre. De l’autre côté, un ingénieur du son augmente ou diminue les volumes des instruments et de la voix. Il fait aussi du montage quand les artistes enregistrent chacun leur tour.
C’est un peu comme du repassage : comme si les chansons étaient froissées et qu’il fallait enlever les gros plis. Il faut égaliser les sons, s’il y a trop d’aigus ou de graves, si la voix est trop haute par rapport à la musique, par exemple. Une fois que c’est fait, si on est tout seul, on grave soi-même ses CD, sinon ils sont faits dans des usines.
Faire de la promotion, c’est faire parler de quelque chose. Quand un artiste est seul, il peut parler de ses morceaux sur les réseaux sociaux, envoyer ses chansons à des journalistes spécialistes de la musique ou vendre ses CD après ses concerts. S’il a signé un contrat avec une maison de disques ou un label, ce sont eux qui font la promo. Ils contactent les journalistes, payent pour la création du single et du clip…
C’est la plupart du temps un tourneur qui s’en occupe : il s’agit d’une entreprise chargée de réserver des dates dans des salles de concert pour les artistes. Quand il y a plusieurs dates de concert, en France et/ou à l’étranger, on parle de tournée.
Les artistes ont aussi un manager : ça peut être un professionnel, un ami ou quelqu’un de leur famille. Cette personne se charge de faire le lien avec le tourneur, la maison de disques, s’occupe des papiers, des repas et boissons pendant la tournée, des transports…


Quelles sont les recettes pour faire un tube ?

Ce qui est étrange avec les tubes, c’est qu’on ne les aime pas forcément dès la première écoute. Ils devraient donc nous agacer sérieusement quand on les entend à la radio pour la dixième fois dès qu’on allume le poste. Eh bien, c’est souvent l’inverse : on se met à les aimer et à les fredonner. En fait, ce n’est pas un hasard.
Aujourd’hui, de très nombreux tubes sont fabriqués par quelques producteurs très doués qui veulent vendre un produit : la chanson. Pour la créer, ces producteurs font appel à des personnes qui vont inventer l’introduction et le refrain, par exemple. Tout ça se fait par ordinateur, avec des logiciels de montage. Ensuite, des artistes sont chargés d’écrire des mélodies et doivent surtout inventer des petits bouts de mélodies qu’on va tout de suite garder en tête. C’est par exemple le cas quand la star Rihanna chante «ella ella ella eh eh» dans son tube Umbrella, sorti en 2007.
C’est comme quand on mange des chips : quand on commence, on a du mal à s’arrêter. Là, nos oreilles s’habituent à ces courtes mélodies et en redemandent. Du coup, il y en a de plus en plus dans les tubes. Résultat : les paroles n’ont pas beaucoup d’importance et ne sont souvent pas de grands poèmes.
Pour faire un tube, il faut aussi un chanteur ou une chanteuse qui a beaucoup de charisme. Sa voix n’a pas besoin d’être exceptionnelle parce qu’elle peut être modifiée sur ordinateur. Rihanna a justement été choisie parce qu’elle avait beaucoup de personnalité.
Evidemment, il n’y a pas de formule magique. Certaines chansons avaient tout pour être des tubes et n’ont pas du tout eu de succès et d’autres sont devenues des tubes «par accident», c’est-à-dire que rien n’avait été prévu !
Mais un seul tube, ça ne suffit pas. Il faut en créer toujours plus parce qu’on ne consomme plus la musique comme avant. On peut écouter une chanson en streaming,sans même la télécharger. Comme les albums, qu’ils soient sur CD ou sur Internet, se vendent de moins en moins, les producteurs veulent toujours trouver une nouvelle chanson. Rihanna en sort très souvent. Ceux qui travaillent avec elle organisent souvent des grandes réunions avec des auteurs et des compositeurs du monde entier qui se creusent la tête pendant une semaine pour lui trouver un tube, comme ça se fait aussi avec d’autres artistes.
Ensuite, la chanson ne peut devenir un tube que si on l’entend partout. La maison de disques chargée de s’occuper de Rihanna a donc tout fait pour qu’Umbrella soit diffusée le plus possible sur les plus grosses stations de radio américaines. Voilà comment la chanson est devenue numéro 1 pendant sept semaines aux Etats-Unis.
Aujourd’hui, il n’y a plus de grands médias écoutés par tout le monde. Mais, même si chacun peut choisir et écouter la musique qu’il souhaite sur son téléphone portable, il y a encore des tubes que (presque) tout le monde connaît.
Ce dossier a été réalisé avec l’aide du journaliste Bertrand Dicale et des livres Grande histoire et petits secrets des tubes de l’été, de Jean-Marie Potiez et Alain Pozzuoli, et Hits, enquête sur la fabrique des tubes planétaires, de John Seabrook.


